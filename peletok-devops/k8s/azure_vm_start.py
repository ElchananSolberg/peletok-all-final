import os
from azure.common.credentials import ServicePrincipalCredentials
from azure.mgmt.compute import ComputeManagementClient


class AzureVmController:
    credentials = None

    def __init__(self, subscription_id, client_id, secret, tenant):
        self.subscription_id = subscription_id
        self.client_id = client_id
        self.secret = secret
        self.tenant = tenant
        self.credentials = ServicePrincipalCredentials(
            client_id=client_id,
            secret=secret,
            tenant=tenant
        )
        self.compute_client = ComputeManagementClient(self.credentials, self.subscription_id)

    def start_vm(self, resource_group_name, vm_name):
        self.compute_client.virtual_machines.start(resource_group_name, vm_name)


subscription_id = os.environ.get("subscription_id")
client_id = os.environ.get("client_id")
secret = os.environ.get("secret")
tenant = os.environ.get("tenant")


azure_controller = AzureVmController(subscription_id, client_id, secret, tenant)

azure_controller.start_vm("peletok", "build-vm")
