import os
import subprocess

os.system("pwd")


def filter_trellis_env():
    arr_env = []
    all_envs_secret_arr = read_global_env_file()
    all_environment = subprocess.check_output("env").decode()
    all_environment_split = all_environment.split()
    for env in all_environment_split:
        if env.startswith("TRELLIS"):
            arr_env.append(env)
    return arr_env + all_envs_secret_arr


def read_global_env_file(path="/var/lib/jenkins/env/.env-k8s"):
    with open(path, "r")as f:
        envs_str = f.read().split("\n")
        return envs_str


def create_env_file(env_arr):
    with open("/var/lib/jenkins/env/.env", "w")as f:
        for elm in env_arr:
            f.write(elm + "\n")


def main():
    env_arr = filter_trellis_env()
    create_env_file(env_arr)

main()
