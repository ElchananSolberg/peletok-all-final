import subprocess
import argparse
import ast


def get_args():
    parser = argparse.ArgumentParser("get arg for get last version")
    parser.add_argument("-rn", dest="repository_name")
    args = parser.parse_args()
    repository_name = args.repository_name
    return repository_name


def get_last_version():
    repository_name = get_args()
    str_all_tags = subprocess.check_output("az acr repository show-tags -n trellis --repository {}".format(repository_name),shell=True).decode('utf-8')
    list_all_versions = ast.literal_eval(str_all_tags)
    last_version = list_all_versions[-1]
    print(last_version)

def main():
    get_last_version()


if __name__ == "__main__":
    main()
