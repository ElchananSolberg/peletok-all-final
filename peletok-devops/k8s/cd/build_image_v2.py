import argparse
import os
import re
import subprocess
from _decimal import Decimal

app_url = os.environ.get("APP_HOSTNAME")
react_app_env = os.environ.get("REACT_APP_ENV")
class BuildImage:
    def __init__(self, registry_name, service_name, env="prod", namespace: str = None):
        self.service_name = service_name
        self.registry = registry_name
        self.env = env
        self.image = self.service_name
        if namespace:
            self.namespace = namespace
            self.image = service_name + '-' + namespace
            # self.namespace_dir = namespace.replace('-', '_')
        self.full_image = f'{self.registry}.azurecr.io/{self.image}'
        if service_name == "server":
            self.service_dir = "peletok_"+service_name+"/server"
        elif service_name == "django":
            self.service_dir = "peletok-"+service_name
        elif service_name == "client":
            self.service_dir = "peletok_"+service_name
        if os.path.isdir(self.service_dir):
            self.Dockerfile_name = f"{self.service_dir}/docker/{self.find_dockerfile_of_service()}"
        else:
            raise Exception('service directory does not exist')

    def get_last_version(self) -> dict:
        """
        Get the last tag, from the repository of Azure.
        :return dict {last_tag: '', next tag: ''}:
        """
        print('Get the last tag')
        try:
            str_tags = subprocess.check_output(
                f"az acr repository show-tags -n {self.registry} --repository {self.image}",
                shell=True).decode('utf-8')

            list_tags = re.findall(r'"(.*)"', str_tags)
            str_last_tag = max(list_tags)
            last_tag_flout = Decimal(re.findall("\d+\.\d+", str_last_tag)[0])
            str_next_tag = f"v{last_tag_flout + Decimal('0.01')}"
            return {"last_tag": str_last_tag, "next_tag": str_next_tag}

        except subprocess.CalledProcessError:
            # use for new image
            return {"last_tag": 'v0.00', "next_tag": 'v1.01'}

    def build_image(self):
        versions = self.get_last_version()
        last_version = versions.get('last_tag')
        new_version = versions.get('next_tag')

        print(f'last_tag: {last_version}\nnext_tag {new_version}\n\n')
        print('Build Image')

        # remove old image from local
        os.system(f"docker rmi {self.full_image}:{last_version}")
        os.system(f"docker rmi {self.full_image}:latest")

        # remove latest tag from azure
        os.system(f"az acr repository untag -n {self.registry} --image {self.image}:latest")

        # docker build image
        print(self.service_dir, self.Dockerfile_name)
        subprocess.check_call(f"docker build --build-arg APP_HOSTNAME={app_url} --build-arg REACT_APP_ENV={react_app_env} -f {self.Dockerfile_name} -t {self.full_image} .".split())
        print("finish build #####")
        # add tag to image
        os.system(f"docker tag {self.full_image} {self.full_image}:{new_version}")
        print("add tagg $$$")

        # push image to azure repository
        os.system(f"docker push {self.full_image}:{new_version}")
        os.system(f"docker push {self.full_image}:latest")
        print("push success")

    def find_dockerfile_of_service(self):
        docker_service_dir = f"{self.service_dir}/docker"
        for file_name in os.listdir(docker_service_dir):
            if "Dockerfile-k8s" in file_name or "Dockerfilenode" in file_name or "Dockerfile-django" in file_name:
                return file_name
      

class Main:

    @staticmethod
    def get_args():
        """
        get args from command line.
        :return: args
        """
        print("\nget args\n")

        # initiate the parser
        parser = argparse.ArgumentParser()
        parser.add_argument("-n", "--namespace", help="namespace of k8s")
        parser.add_argument("-s", "--service", help="service of deployment", required=True)
        parser.add_argument("-r", "--registry", help="registry name", required=True)
        parser.add_argument("-e", "--env", help="which environment to create prod, staging, k8s ...", default="prod")

        # read arguments from the command line
        command_line_args = parser.parse_args()
        return command_line_args

    @staticmethod
    def main():
        # os.system("pwd")
        # os.system("env")
        args = Main.get_args()
        build_image = BuildImage(registry_name=args.registry, service_name=args.service, env=args.env,
                                 namespace=args.namespace)
        build_image.build_image()


if __name__ == '__main__':
    Main.main()
