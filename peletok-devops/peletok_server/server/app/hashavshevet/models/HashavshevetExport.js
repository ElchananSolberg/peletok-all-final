const BaseModel = require('../../BaseModel');
const DataTypes = require("sequelize").DataTypes;

/**
 * HashavshevetExport model
 */
class HashavshevetExport extends BaseModel{
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            timestamp_export: {
                type: DataTypes.DATE,
                allowNull: true
            },
            file_name: {
                type: DataTypes.STRING(256),
                allowNull: true
            },
            meta: {
                type: DataTypes.TEXT,
                allowNull: true
            },
            max_order_number: {
                type: DataTypes.INTEGER,
                allowNull: false
            },
            start_date: {
                type: DataTypes.DATE,
                allowNull: true
            },
            end_date: {
                type: DataTypes.DATE,
                allowNull: true
            }
        };
        this.prefix = "HASH";
        this.author = true;
    }
    /**
     * creates associate for HashavshevetExport model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.hasMany(models.transaction.model, {as: 'Transaction' });
        this.model.belongsTo(models.business.model, { as: 'Business' });

    }
}

module.exports = new HashavshevetExport();
