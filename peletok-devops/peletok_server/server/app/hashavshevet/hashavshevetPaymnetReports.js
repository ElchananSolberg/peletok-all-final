const models = require('../index')
const Op = require('sequelize').Op
/**
 * create db query filters according to req filters
 * @param {*} data 
 * @param {*} req 
 */
function createWhereData(data, req) {
    let whereObj = {}

    if (data['startDate'] && data['endDate']) {
        whereObj['date'] = { [Op.between]: [data.startDate, data.endDate] }
    }

    if (data['business_id']) {
        whereObj['business_id'] = data['business_id']
    }

    return whereObj
}
/**
 * get 'BusinessPayment' table data with associate business data according to req filters
 */
async function getBusinessPaymentReports(req, res) {
    whereObj = createWhereData(req.body)
    let currentBusinessPayment = await models.businessPayment.model.findAll({
        where: whereObj, attributes: ['date', 'sum', 'invoice_number'], include: {
            model: models.business.model,
            as: 'Business',
            attributes: [['name', 'business_name'], 'business_identifier']
        }
    })

    res.send(currentBusinessPayment)
}

module.exports.getBusinessPaymentReports = getBusinessPaymentReports