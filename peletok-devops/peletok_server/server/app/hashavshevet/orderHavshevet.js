const reportView = require("../transaction/view/ReportView");
// const hashavshevetExport = require("./models/HashavshevetExport")
const transaction = require("../transaction/models/Transaction")
const Op = require("sequelize").Op;
const path = require("path");
const sequelize = require("sequelize");
const defaultPath = path.join(__dirname, "../rest/restData/hashavshevet");
let fs = require('fs');
const moment = require('moment')
const models = require("../index")
const priceProd = "price"
const priceNotVat = "price_not_including_vat"
const { getTypeNum } = require('../utils.js')

/* check if  business with artivle 20  */
async function checkBusiness_use_article_20(businessId) {

    let business = await models.business.model.findByPk(businessId)

    if (business["use_article_20"]) { return priceProd }
    else { return priceNotVat }

}

function bulidWhereObject(params){
    if(params.exportedOrdertId){
        // if exportedOrdertId we filter by exportedOrdertId and business if exist
        return Object.assign({
            hashavshevet_export_id: params.exportedOrdertId
        }, params.businessId || params.businessIds ? {business_id: params.businessId || params.businessIds} : {})
    }
    let where = {
        use_article_20 : { [Op.eq]: !!params.section20 },
        hashavshevet_export_id: { [!!params.isReportOnly ? Op.ne : Op.eq]: null },
        product_type: { [Op.or]: !!params.isCommissionExport ?  [getTypeNum('bills_payment','product')] : [getTypeNum('virtual','product'), getTypeNum('manual','product')] },
    }
    if(params.businessId != 0){
        where.business_id =  params.businessId || params.businessIds;
    }

    if(params.expoertId){
        where.hashavshevet_export_id = params.expoertId;
    }

    if(params.agentId){
        where.agent_id = params.agentId;
    }
    if(params.customerNumber){
        where.business_identifier = params.customerNumber;
    }

   

    return {
                transaction_start_timestamp: { [Op.between]: [params.startDate, params.endDate] },
                ...where
    }
}


/*(step1) Get data from DB order Hashavshevet  */
async function getOrderHashavshevetView(params) {
    // let priceProd = await checkBusiness_use_article_20(businessId)
    const where = bulidWhereObject(params)

    return await reportView.model.findAll(
        {
            where,
            group: ["business_id", "business_name", 'business_identifier'],
            attributes: ["business_id",  //TODO add vat
                ['business_name', 'customerName'],
                ['business_identifier', 'customerNumber'],
                ['business_identifier', 'hashavshevetCustomerCode'],// TODO add delek
                sequelize.literal(`(select sum(${!params.section20 ? 'price_not_including_vat' : 'price'}) as sum from "TRNSC_Transaction" where id in (SELECT unnest(ARRAY_AGG("ReportView"."id")))) as "totalPayments"`), 
                // [sequelize.fn('sum', sequelize.col('price_not_including_vat')), 'totalPayments'],//TODO handle section 20
                [sequelize.fn('max', sequelize.col('business_balance')), 'balance'],//TODO get business's current balance
                [sequelize.fn('to_char',sequelize.fn('max', sequelize.col('transaction_start_timestamp')), 'YYYY-MM-DD HH:MI:SS'), 'latestDateInOrder'],
                [sequelize.fn('to_char', sequelize.fn('min', sequelize.col('transaction_start_timestamp')), 'YYYY-MM-DD HH:MI:SS'), 'earliestDateInOrder']
            ],
            order: ["business_name"],

            raw: true
        })

}

/* Get data from DB by business ID one  only (step2) 


  */
async function getOrderHashavshevetPrivate(params) {
    const where = bulidWhereObject(params)

    return await reportView.model.findAll(
        {
            where,
            group: ["business_id", "product_id","product_name" , !params.section20 ? 'price_not_including_vat' : 'price'], // TODO hndle vat change
            attributes: [
                ["product_name", 'itemName'], "product_id",
                // if isCommissionExport we need sum else we need price per unit
                !!params.isCommissionExport ? 
                sequelize.literal(`(select sum(${!params.section20 ? 'price_not_including_vat' : 'price'}) as sum from "TRNSC_Transaction" where id in (SELECT unnest(ARRAY_AGG("ReportView"."id")))) as "price"`) : 
                [!params.section20 ? 'price_not_including_vat' : 'price', 'price'], 
                sequelize.literal(`COUNT(distinct("ReportView"."id")) as quantity`), 

            ],
            raw: true
        })
}

/* Get data from DB for  XML */
async function getOrderHashavshevetXml(params, hashavshvetNumbersMap) {

    const where = bulidWhereObject(params);
    return await reportView.model.findAll(
        {
            where,
            group: ['business_id', "product_id", 'barcode', !params.section20 ? 'price_not_including_vat' : 'price', 'business_identifier'],
            attributes: [
                ["business_identifier", 'customerId'],
                ["barcode", 'itemId'],
                sequelize.literal("to_char(now(), 'DD/MM/YYYY') as date "),
                // if isCommissionExport we need sum else we need price per unit
                !!params.isCommissionExport? 
                sequelize.literal(`(select sum(${!params.section20 ? 'price_not_including_vat' : 'price'}) as sum from "TRNSC_Transaction" where id in (SELECT unnest(ARRAY_AGG("ReportView"."id")))) as "price"`) : 
                [!params.section20 ? 'price_not_including_vat' : 'price', 'price'],
                sequelize.literal(`COUNT(distinct("ReportView"."id")) as amount`),
                sequelize.literal(`${[...new Set(params.businessIds)].reduce((n,id)=>n + ' WHEN "ReportView"."business_id" = ' + id + ' THEN ' + hashavshvetNumbersMap[id] , ' CASE ')} END as "orderNo"`), 
                [sequelize.fn('ARRAY_TO_STRING',sequelize.fn('ARRAY_AGG', sequelize.col('id')), ','), 'transactions']
                // [sequelize.fn("to_char", sequelize.col("transaction_start_timestamp"), 'YYYY-MM-DD'), "date"]
            ],
            order: ["business_id"],
            raw: true
        })

}



/* Convert the list to xml using map to receive the data  */
async function convertObjectToXmlFormat(xmlName, itemName, itemsList) {
    let constants = await { docNo: 11, dollar: 1, money: 'ש"ח', };
    
    let  xmlString = itemsList.reduce((n, i)=>{
         let mergedData = {...i, ...constants}
         return n + getXmlRow(itemName, Object.keys(mergedData).reduce((n, k)=>n + getXmlRow(k,mergedData[k]),''))
         },'') 
    return getXmlRow(xmlName, xmlString);
}

/*  covert key and value to xml format */
function getXmlRow(rowName, value) {
    return `<${rowName}>${value}</${rowName}>`;
}

/* write xml to file */
async function writeXmlToFileAndUpdateTransaction(xml, date, transactionIds,  dirPath = null, name, maxNo, meta, exportedOrdertId) {
   
    name = name || "ExportOrdersToHashavshevetReport_" + Date.now()
    pathExportFile = path.join(dirPath || defaultPath, name + ".xml");
    
    
    await fs.writeFileSync(pathExportFile, xml)

    let hashavshevetExport = await models.hashavshevetExport.model.create({ 
        timestamp_export: date,
        file_name: name,
        max_order_number: maxNo,
        meta: meta ?  JSON.stringify(meta) : '{}'
    })
    await models.transaction.model.update({
        hashavshevet_export_id: hashavshevetExport.id
    },{
        where: {id: transactionIds}
    })
    // if we update export we should reset all last export to be null 
    // if they didnt overided
    if(exportedOrdertId){
        await models.transaction.model.update({
            hashavshevet_export_id: null
        },{
            where: {hashavshevet_export_id: exportedOrdertId}
        })
    }

    return pathExportFile
}

/* export data to Xml && create orderNum  */
async function exportToXml(params) {
    let maxNo = (await models.hashavshevetExport.model.max('max_order_number')) || 1
    let hashavshvetNumbersMap = [...new Set(params.businessIds)].reduce((n, id)=>Object.assign(n, {[id]: maxNo++}), {})
    
    let data = await getOrderHashavshevetXml(params, hashavshvetNumbersMap, params.exportedOrdertId)
    let date = moment().toDate()

    
    
   
    
    let {transactions, restData} = data.reduce((n, d)=>{
        let {transactions, ...rest} = d;
        return {transactions: n.transactions + (transactions ? ',' : '') + transactions, restData: [rest, ...n.restData] }
    },{transactions:'', restData: []})
     
    const xml = await convertObjectToXmlFormat("orders", "row", restData);


    return await writeXmlToFileAndUpdateTransaction(xml, date, transactions.split(',').filter(t=>t), null, params.fileNam, maxNo - 1, {...hashavshvetNumbersMap, isCommission: !!params.isCommissionExport}, params.exportedOrdertId );
}





/*Get   hashavshevet  created by date 
(return date,fileName)*/
async function getHashavshevetByDate(startDate, endDate) {
    return await models.hashavshevetExport.model.findAll(
        {
            where: {
                timestamp_export: { [Op.between]: [startDate, endDate] },
            },
            attributes: ["id", [sequelize.fn('to_char', sequelize.col('timestamp_export'), 'YYYY-MM-DD HH:MI:SS'), 'date'], ["file_name", 'fileName'],
            ],

            raw: true

        })
}

/*Get  order by hashavshevet --  created by date (step2)
(return date,fileName)*/
async function getOrderNoByHashavshevet(hashavshevetName) {
    return await models.hashavshevetExport.model.findAll(
        {
            where: {
                file_name: hashavshevetName,lastOrderNo :{[Op.ne]: null}
            },
            attributes: ["lastOrderNo",
            ],

            raw: true

        })
}

/*Get   hashavshevet  created by  businessId
TODO  add  מע"ם  .  businessIDHashavshevet
*/
async function getHashavshevetById(startDate, endDate, businessId) {
    return await reportView.model.findAll(
        {
            where: {
                business_id: businessId,
                transaction_start_timestamp: { [Op.between]: [startDate, endDate] },

            },
            group: ["business_id", "business_name", "order_timestamp", "order_No", , current_business_balance],
            attributes: ["order_No", "business_id", "order_timestamp", "business_name", current_business_balance,
                [sequelize.fn('sum', sequelize.col('price')), 'sumPrice'],
                [sequelize.fn('max', sequelize.col('transaction_start_timestamp')), 'dateMax'],
                [sequelize.fn('min', sequelize.col('transaction_start_timestamp')), 'dateMin']
            ],
            raw: true

        })
}

/* Get data from DB by orderId one  only 
*/
async function getHashavshevetByOrder(orderNo) {
    return await reportView.model.findAll(
        {
            where: {
                order_No: orderNo,

            },
            group: ["order_No", "product_name", "price", "product_id"],
            attributes: ["product_name", "product_id", "price",
                [sequelize.fn('COUNT', 'id'), 'amount'],
                [sequelize.fn('sum', sequelize.col('price')), 'sumPrice'],
            ],
            raw: true
        })

}


module.exports.getOrderHashavshevetView = getOrderHashavshevetView
module.exports.getOrderHashavshevetViewPrivate = getOrderHashavshevetPrivate
module.exports.exportToXml = exportToXml
module.exports.getHashavshevetByDate = getHashavshevetByDate
module.exports.getOrderNoByHashavshevet = getOrderNoByHashavshevet
module.exports.getHashavshevetById = getHashavshevetById
module.exports.getHashavshevetByOrder = getHashavshevetByOrder

