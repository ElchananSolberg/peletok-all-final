const reportView = require("../transaction/view/ReportView");
const hashavshevetExport = require("../hashavshevet/models/HashavshevetExport")
const transaction = require("../transaction/models/Transaction")
const Op = require("sequelize").Op;
const path = require("path");
const sequelize = require("sequelize");
const defaultPath = path.join(__dirname, "../rest/restData/hashavshevet");
const moment = require('moment')
const models = require("../index")
const   businessPr ="business_profit"
const distributorPr="distributor_profit"
const peletokId=1
let fs = require('fs');

/* check if  distributor  is Peletok  */
async function checkDistributorIsPeletok(businessId){

    let business=await models.business.model.findByPk(businessId[0])
   
    if (business["distributor_id"]=peletokId){ return businessPr}
    else{return distributorPr }
 
  

}

/*(step1) Get data from DB order Hashavshevet  */
async function getOrderHashavshevetView(startDate, endDate, businessId) {
    let priceProd = await checkDistributorIsPeletok(businessId)

    return await reportView.model.findAll(
        {
            where: {
                order_No: null,
                business_id: businessId,
                transaction_start_timestamp: { [Op.between]: [startDate, endDate] },
                product_type: 3
            },
            group: ["business_id","current_business_balance", "business_name"],
            attributes: ["business_id","current_business_balance","business_name",

                [sequelize.fn('sum', sequelize.col(priceProd)), 'sumPrice'],
                [sequelize.fn('max', sequelize.col('transaction_start_timestamp')), 'dateMax'],
                [sequelize.fn('min', sequelize.col('transaction_start_timestamp')), 'dateMin']
            ],
            order: ["business_id"],

            raw: true
        })

}

/* Get data from DB by business ID one  only (step2) 

TODO add balance to  businessId

  */
 async function getOrderHashavshevetPrivate(startDate, endDate, businessId) {
    let priceProd = await checkDistributorIsPeletok(businessId)
    return await reportView.model.findAll(
        {
            where: {
                order_No: null,
                business_id: businessId,
                transaction_start_timestamp: { [Op.between]: [startDate, endDate] },
                product_type: 3
            },
            group: ["business_id", "product_name", priceProd, "product_id",current_business_balance],
            attributes: ["product_name", "product_id", priceProd,current_business_balance,
                [sequelize.fn('COUNT', 'id'), 'amount'],
                [sequelize.fn('sum', sequelize.col(priceProd)), 'sumPrice'],
            ],
            raw: true
        })
}

/* Get data from DB for  XML */
async function getOrderHashavshevetXml(startDate, endDate, businessId) {
    let priceProd = await checkDistributorIsPeletok(businessId)
    return await reportView.model.findAll(
        {
            where: {
                order_No: null,
                business_id: businessId,
                transaction_start_timestamp: { [Op.between]: [startDate, endDate] },
                product_type:3
            },
            group: ["supplier_id", "business_id", "product_id", priceProd, "date"],
            attributes: ["supplier_id", "business_id", "product_id", priceProd,
                [sequelize.fn('COUNT', 'id'), 'amount'],
                [sequelize.fn("to_char", sequelize.col("transaction_start_timestamp"), 'YYYY-MM-DD'), "date"]
            ],
            order: ["business_id"],
            raw: true
        })

}



/* Convert the list to xml using map to receive the data  */
async function convertObjectToXmlFormat(xmlName, itemName, itemsList,businessId) {
    let priceProd = await checkDistributorIsPeletok(businessId)
    let constants =await { docNo: 11, dollar: 1, money: 'ש"ח', };
    const map =await { amount: "amount", customerId: "business_id", price: priceProd, itemId: "product_id", date: "date", orderNo: 'orderNo' };
    
    let xmlString = "";
    const mapKeys =await Object.keys(map);
    const constantsKeys =await Object.keys(constants);

    for (let i = 0; i < itemsList.length; i++) {
        let objectItem = itemsList[i];
      
        let rowString = "";

        for (let j = 0; j < mapKeys.length; j++) {
            let mapKey = mapKeys[j];
            let valueKey = map[mapKey];
            rowString += await getXmlRow(mapKey, objectItem[valueKey]);
        }

        for (let j = 0; j < constantsKeys.length; j++) {
            let constantsKey = constantsKeys[j];
            rowString += await getXmlRow(constantsKey, constants[constantsKey]);
        }

        xmlString += getXmlRow(itemName, rowString);
    }
    return  getXmlRow(xmlName, xmlString);
}

/*  covert key and value to xml format */
function getXmlRow(rowName, itemName) {
  return `<${rowName}>${itemName}</${rowName}>`;
}

/* write xml to file */
async function writeXmlToFile(xml, date, orderNumbers, dirPath = null, name = "ExportOrdersToHashavshevetReport" + new Date(new Date().toString().split('GMT')[0] + ' UTC').toISOString().replace(/:/g, '-')) {

    pathExportFile = path.join(dirPath || defaultPath, name + ".xml");
    for (var i = 0; i < orderNumbers.length; i++) {
        if (orderNumbers[i] != orderNumbers[i - 1]) {
            await hashavshevetExport.model.create({ lastOrderNo: orderNumbers[i], file_name: name })
        }

    }


    await hashavshevetExport.model.create({ timestamp_export: date, file_name: name })
    await fs.writeFileSync(pathExportFile, xml)

    return pathExportFile
}

/* export data to Xml && create orderNum  */
async function exportToXml(startDate, endDate, businessId) {
    let data = await getOrderHashavshevetXml(startDate, endDate, businessId)
    let date = moment().toDate()


    let startNum = await getOrderStartNum()
    let previousBusinessId = -1;
    let orderNo = null
    let orderNumbers = []
    await Object.values(data).forEach(obj => {
        if (obj.business_id !== previousBusinessId)
            orderNo = startNum++; obj["orderNo"] = orderNo;
        previousBusinessId = obj.business_id

        updateTransactionOrderNo(obj, date)
        orderNumbers.push(orderNo)
    })
    

    const xml = await convertObjectToXmlFormat("orders", "row", data, businessId);


    return await writeXmlToFile(xml, date, orderNumbers);
}
/* get  last OrderNumber from   DB for create  in new new Order  startNum 
TODO
 delete  if (!hashavshevetExport.model["lastOrderNo"])
        await hashavshevetExport.model.create({ lastOrderNo: 1000 }) - this  only  demo */
async function getOrderStartNum() {
    if (!hashavshevetExport.model["lastOrderNo"])
        await hashavshevetExport.model.create({ lastOrderNo: 1000 })
    if (hashavshevetExport.model.max("lastOrderNo")) { return await hashavshevetExport.model.max("lastOrderNo") + 1 }
}


/* update in  every Transaction orderNumber  of  hashavshevet and  timestamp of order  */
async function updateTransactionOrderNo(obj, date) {


    let dateAfter = new Date(obj.date);
    dateAfter.setDate(dateAfter.getDate() + 1);
    await transaction.model.update({ order_No: obj.orderNo, order_timestamp: date }, {
        where: {
            business_id: obj.business_id,
            product_id: obj.product_id,
            transaction_start_timestamp: { [Op.between]: [obj.date, dateAfter] }

        }
    })

}

module.exports.getOrderHashavshevetView = getOrderHashavshevetView
module.exports.getOrderHashavshevetViewPrivate = getOrderHashavshevetPrivate
module.exports.exportToXml = exportToXml


