const { parseString } = require('xml2js')
const xml2js = require('xml2js')
const fs = require('fs')
const models = require('../index.js')
const path = require('path')
const { uploadFile } = require('../utils')
const publicFilesDir = path.normalize(`${__dirname}/../rest/restData/local/`)
let hashavshevetImportDir = './rest/restData/local/hashavshevetImport'
const moment = require('moment')
var parser = new xml2js.Parser();
const util = require('util')
const Op = require('sequelize').Op
const currency=require('currency.js')

/**
 * show user uploaded hashavshevet file
 * @param {*} req 
 * @param {*} res 
 */
async function showReport(req, res) {
    let businesses = []
    let reqData = await uploadFile(req, hashavshevetImportDir, 'import', ['xml'])
    // we should add h becouse of the splite error . TODO fix uploadFile and remove 'h'
    let filePath = path.normalize(`${publicFilesDir}h${reqData.hashavshevet}`)
    fs.readFile(filePath, function (err, data) {
        parser.parseString(data, async function (err, result) {
            
            let businessesWithBalance = await models.business.model.findAll({
                where: { [reqData.isHashavshevetId != 'true' ? 'business_identifier' : 'delek_store_identifier']: {
                    [Op.in]: result['קבלת_מזומן']['Row'].map(r => reqData.isHashavshevetId != 'true' ? r['מפתח_חשבון'][0] : parseInt(r['מפתח_חשבון'][0], 10))
                } },
                include: {
                    model: models.businessFinance.model,
                    as: "BusinessFinance",
                }
            });
            for (let i = 0; i < result['קבלת_מזומן']['Row'].length; i++) {
                const payment = result['קבלת_מזומן']['Row'][i];
                let currentBusinessPayment = businessesWithBalance.find(bp => reqData.isHashavshevetId != 'true' ? (bp.business_identifier == payment['מפתח_חשבון'][0])  : (bp.delek_store_identifier == parseInt(payment['מפתח_חשבון'][0], 10)))
                if (!currentBusinessPayment) {
                    continue
                }
                let modifiedBusiness = {
                    businessName: currentBusinessPayment.name,
                    receiptNumber: payment['מספר_קבלה'][0],
                    date: payment['תאריך_הפקה_בפועל'][0],
                    hour: payment['שעת_הפקה_בפועל'][0],
                    amount: payment['סכום'][0]
                }
                businesses.push(modifiedBusiness)
            }
            //TODO fix uploadFile and remove 'h'
            let fileName = 'h' + reqData['nameAndDate']
            res.send({ file: fileName, payments: businesses })
        })
    })
}


/**
 * handles import of hashavshevet files and update all relevant fields in DB
 * @param {*} req 
 * @param {*} res 
 */
async function hashavshevetImport(req, res, ) {
    // TODO - insert action to DB (credit, refund, payment, etc..) 
    let dates = []
    let isHashavshevetId = false
    let businessPayments = []
    if (req.body.isHashavshevetId) isHashavshevetId = true
    let hashavshevetImport = await models.hashavshevetImport.model.create({
        timestamp_import: moment().toDate(),
        file_name: req.body.fileName.slice(req.body.fileName.lastIndexOf("/") + 1, -1),
        is_hashavshevet_id: isHashavshevetId,
        author_id: req.user.id
    })
    fs.readFile(`${publicFilesDir}${req.body.fileName}`, function (err, data) {
        parser.parseString(data, async function (err, result) {
            let businessesWithBalance = await models.business.model.findAll({
                where: { [!isHashavshevetId ? 'business_identifier' : 'delek_store_identifier']: {
                    [Op.in]: result['קבלת_מזומן']['Row'].map(r => !isHashavshevetId ? r['מפתח_חשבון'][0] : parseInt(r['מפתח_חשבון'][0], 10))
                } },
                include: {
                    model: models.businessFinance.model,
                    as: "BusinessFinance",
                }
            });
            for (let i = 0; i < result['קבלת_מזומן']['Row'].length; i++) {
                const payment = result['קבלת_מזומן']['Row'][i];
                if (i < 1) {
                    result['קבלת_מזומן']['Row'].map((paymentRow) => {
                        let date = moment(paymentRow['תאריך_הפקה_בפועל'][0])
                        dates.push(date)
                    })
                    let maxDate = moment.max(dates)
                    let minDate = moment.min(dates)
                    hashavshevetImport['start_date'] = minDate
                    hashavshevetImport['end_date'] = maxDate
                }
                hashavshevetImport.save()
                let currentBusinessWithBalance = businessesWithBalance.find(bp => !isHashavshevetId ? (bp.business_identifier == payment['מפתח_חשבון'][0]) : (bp.delek_store_identifier == parseInt(payment['מפתח_חשבון'][0], 10)))

                if (!currentBusinessWithBalance) {
                    continue
                }
                let currentBalance = currentBusinessWithBalance.BusinessFinance[0].dataValues
                currentBusinessWithBalance['BusinessFinance'].balance = currency(currentBalance['balance']).add(payment['סכום'][0]);
                let businessPaymentRow = {
                    date: payment['תאריך_הפקה_בפועל'][0], hour: payment['שעת_הפקה_בפועל'],
                    sum: payment['סכום'][0], hashavshevet_import_id: hashavshevetImport.id, author_id: req.user.id,
                    user_id: req.user.id, business_credit: currentBalance.balance,
                    is_credit_from_supplier: req.body.isCredit,
                    business_id: currentBusinessWithBalance.id,
                    invoice_number: payment['מספר_קבלה'][0],
                    author_id: req.user.id,
                    author_name: `${req.user['first_name']} ${req.user['last_name']}`,
                    business_identifier: currentBusinessWithBalance.business_identifier
                }
                businessPayments.push(businessPaymentRow)
                // await models.businessFinance.model.update({ balance: parseFloat(currentBalance['balance']) + parseFloat(payment['סכום'][0]) }, { where: { business_id: currentBusinessWithBalance.id } })
                await models.businessFinance.model.update({ balance:currency(currentBalance['balance']).add(payment['סכום'][0]) }, { where: { business_id: currentBusinessWithBalance.id } })

            }

            await models.businessPayment.model.bulkCreate(businessPayments)
        });
    });

    res.send('success')
}

module.exports.hashavshevetImport = hashavshevetImport
module.exports.showReport = showReport
