const BaseModel = require("../../BaseModel");
const DataTypes = require("sequelize").DataTypes;
const User = require('../../user_management/models/User').model

/**
 * Role model
 */
class Role extends BaseModel {


    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            name: {
                type: DataTypes.STRING(256),
                allowNull: false
            },
        };

        this.scopes = {
            userRoles: (userId) => {
                 return {include: [{ model: User, where: {
                            id: userId
                         }}]}
            } 
        }

        this.prefix = "PRM";
        this.author = true;
        this.status = true;
        this.statusAuthor = true;
    }

    /**
     * creates associate for Role model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {

        this.model.belongsToMany(models.permission.model, {through: "PermissionRole"});
        this.model.belongsToMany(models.user.model, {through: "RoleUser"});
        this.model.belongsToMany(models.business.model, {through: "BusinessRole"});
        this.model.belongsToMany(models.role.model, {through: "RoleAllowedRole", as: "Role", foreignKey: 'role_id'});
        this.model.belongsToMany(models.role.model, {through: "RoleAllowedRole", as: "AllowedRoles", foreignKey: 'allowed_role_id'});

    }
}

module.exports = new Role();