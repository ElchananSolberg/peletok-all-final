const BaseModel = require("../../BaseModel");
const DataTypes = require("sequelize").DataTypes;
const Op = require("sequelize").Op
const moment = require('moment');
/**
 * UiPermissionRole model
 */
class PermissionRole extends BaseModel {


    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            id: {
               type: DataTypes.INTEGER,
               primaryKey: true,
               autoIncrement: true 
            },
            action: {
                type: DataTypes.ENUM("show", "edit"),
                allowNull: false
            },
            expiry_date: {
                type: DataTypes.DATEONLY,
                allowNull: true  // null means: Not limited in time
            }
        };
        this.scopes = {
            validPermissionRoles:{
                where: {
                    expiry_date: {
                        [Op.or]: {
                           [Op.gte]: moment().toDate(),  
                           [Op.eq]: null 
                        }
                    }

                   
                },
            }
        }
        this.prefix = "PRM";
        this.author = true;
        this.status = true;
        this.statusAuthor = true;
    }

    /**
     * creates associate for UiPermissionRole model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.permission.model, {as: "Permission"});
        this.model.belongsTo(models.role.model, {as: "Role"});
    }
}

module.exports = new PermissionRole();