const BaseModel = require("../../BaseModel");
const DataTypes = require("sequelize").DataTypes;

/**
 * UiPermissionGroup model
 */
class PermissionGroup extends BaseModel {


    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            name: {
                type: DataTypes.STRING(256),
                allowNull: false
            },
        };
        this.prefix = "PRM";
        this.author = true;
    }

    /**
     * creates associate for UiPermissionGroup model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {

        this.model.hasMany(models.permission.model, {as: "Permission"});
    }
}

module.exports = new PermissionGroup();