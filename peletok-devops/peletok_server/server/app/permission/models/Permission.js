const BaseModel = require("../../BaseModel");
const DataTypes = require("sequelize").DataTypes;
const Op = require("sequelize").Op
const moment = require('moment');
const Role = require('./Role').model
/**
 * UiPermission model
 */
class Permission extends BaseModel {


    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            name: {
                type: DataTypes.STRING(256),
                allowNull: true
            },
            path: {
                type: DataTypes.STRING(256),
                allowNull: false
            },
            type: {
                type: DataTypes.ENUM("ui", "api"),
                allowNull: false
            },
        };

        this.scopes = {
            rolesValidPermissions(roleIds){
                return {include: [{
                            model: Role,
                            where: {id:  roleIds},
                            through: {                   
                              where: {
                                    expiry_date: {
                                        [Op.or]: {
                                           [Op.gte]: moment().toDate(),  
                                           [Op.eq]: null 
                                        }
                                    }
                                }
                            }
                          }]
                      }
            }
        }

        this.indexes = [
            {
              unique: true,
              fields: ['path']
            },
         ]

        this.prefix = "PRM";
        this.author = true;
    }

    /**
     * creates associate for UiPermission model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsToMany(models.role.model, {through: "PermissionRole"});
        this.model.belongsTo(models.permissionGroup.model, {as: "PermissionGroup"});
        this.model.belongsToMany(models.permission.model, {through: "ApiPermissionPermission", as: "Permission", foreignKey: 'api_permission_id'});
        this.model.belongsToMany(models.permission.model, {through: "ApiPermissionPermission", as: "ApiPermissions", foreignKey: 'permission_id'});
    }
}

module.exports = new Permission();