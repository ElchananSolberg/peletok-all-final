const BaseModel = require('../../BaseModel');
const DataTypes = require("sequelize").DataTypes;

/**
 * Tag MessageBusinessTag
 */
class MessageBusinessTag extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            mark: {
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: false
            }
        };
        this.prefix = "MSG";
        this.author = true;
    }
    /**
     * creates associate for MessageBusinessTag model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.tagOfBusinessMessage.model, { as: 'TagOfBusinessMessage' });
        this.model.belongsTo(models.messageBusiness.model, { as: 'MessageBusiness' });
    }
}

module.exports = new MessageBusinessTag();