const DataTypes = require('sequelize').DataTypes;
const {Op} = require("sequelize");
const BaseModel = require('../../BaseModel');
const {createMessageBusiness} = require("../message_utils");

/**
 *  Message model
 */
class Message extends BaseModel {

    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            message: {
                type: DataTypes.STRING(1024),
                allowNull: true
            },
            timestamp_sending: {
                type: DataTypes.DATE,
                allowNull: true
            },
        };
        this.prefix = 'MSG';
        this.author = true;
        this.statusField = true;
        this.statusAuthor = true;
        this.statusTimestampField = true;
    }

    /**
     * creates associate for role model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.user.model, {as: 'User', foreignKey: 'from_user_id'});
        this.model.belongsTo(models.user.model, {as: 'ReceiptUser', foreignKey: 'to_user_id'});
        this.model.belongsTo(models.business.model, {as: 'Business', foreignKey: 'from_business_id'});
        this.model.belongsTo(models.user.model, {as: 'ReceiptBusiness', foreignKey: 'to_business_id'});
        this.model.hasMany(models.messageTag.model, {as: 'MessageTag'});
        this.model.hasMany(models.messageBusiness.model, {as: 'MessageBusiness'});
        // this.model.hasMany(models.favoriteMessage.model, {as: 'FavoriteMessage'});
    }

}

let messageInstance = new Message();

messageInstance.model.afterCreate(async (message, options) => {
    const models = require("../../index");
    const tagOfMessageModel = models.tagOfMessage.model;
    const messageTagModel = models.messageTag.model;
    let explicitMessageTag = message.MessageTag||[];
    const tags = await tagOfMessageModel.findAll({
        where: {
            id: {
                [Op.notIn]: explicitMessageTag.map(mt => {
                    return mt.tag_of_message_id
                })
            }
        }
    });
    let messagesTagsToCreate = tags.map(t => {
        return {tag_of_message_id: t.id, message_id: message.id, mark: t.default_value}
    });
    await messageTagModel.bulkCreate(messagesTagsToCreate);

    const tagOfBusinessMessageModel = models.tagOfBusinessMessage.model;
    const messageBusinessTagModel = models.messageBusinessTag.model;
    let explicitMessageBusinessTag = message.MessageBusinessTag||[];
    const businessTags = await tagOfBusinessMessageModel.findAll({
        where: {
            id: {
                [Op.notIn]: explicitMessageBusinessTag.map(messageBusTag => {
                    return messageBusTag.tag_of_business_message_id
                })
            }
        }
    });
    if(message.MessageBusiness)return;
    let businessMessagesTagsToCreate = [];
    let messageBusiness = await createMessageBusiness(message.id, message.to_business_id);
    for (const mb of messageBusiness) {
        for (const t of businessTags) {
            businessMessagesTagsToCreate.push({
                tag_of_business_message_id: t.id,
                mark: t.default_value,
                message_business_id: mb
            })
        }
        }
    await messageBusinessTagModel.bulkCreate(businessMessagesTagsToCreate);
});

module.exports = messageInstance;