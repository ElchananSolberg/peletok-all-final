const BaseModel = require('../../BaseModel');
const DataTypes = require("sequelize").DataTypes;

/**
 * Tag MessageBusiness
 */
class MessageBusiness extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.prefix = "MSG";
        this.author = true;
    }
    /**
     * creates associate for MessageBusiness model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.hasMany(models.messageBusinessTag.model, { as: 'MessageBusinessTag' });
        this.model.belongsTo(models.business.model, { as: 'Business' });
        this.model.belongsTo(models.message.model, { as: 'Message' });
    }
}

module.exports = new MessageBusiness();