const BaseModel = require('../../BaseModel');
const DataTypes = require("sequelize").DataTypes;

/**
 * TagOfMessage model
 */
class TagOfMessage extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            tag_name: {
                type: DataTypes.STRING(128),
                allowNull: true
            },
            order: {
                type: DataTypes.STRING,
                allowNull: true
            },
            default_value: {
                type: DataTypes.BOOLEAN,
                defaultValue: false,
                allowNull: false
            }
        };
        this.prefix = "MSG";
        this.author = true;
    }
    /**
     * creates associate for TagOfMessage model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.hasMany(models.messageTag.model, { as: 'MessageTag' });
        this.model.hasMany(models.businessNotificationByTag.model, { as: 'BusinessNotificationByTag' });
    }
}

let tagOfMessage = new TagOfMessage();
tagOfMessage.model.afterCreate(async (tag, options) => {
    const models = require("../../index");
    const messageModel = models.message.model;
    const messageTagModel = models.messageTag.model;
    const messages = await messageModel.findAll();
    let messagesTagsToCreate = messages.map(m => {
        return {tag_of_message_id: tag.id, m: message.id, mark: tag.default_value}
    });
    await messageTagModel.bulkCreate(messagesTagsToCreate);
});



module.exports = tagOfMessage;