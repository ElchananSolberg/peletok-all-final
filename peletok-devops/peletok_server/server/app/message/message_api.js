const express = require('express');
const {Op} = require("sequelize");
const moment = require('moment')

const {Info} = require("../mapping/crudMiddleware");
const {LM} = require('../config/LanguageManager')
const models = require("../index");

const Message = models.message.model;
const MessageTag = models.messageTag.model;
const MessageBusiness = models.messageBusiness.model;
const MessageBusinessTag = models.messageBusinessTag.model;
const TagOfMessage = models.tagOfMessage.model;
const TagOfBusinessMessage = models.tagOfBusinessMessage.model;
const User = models.user.model;
const Business = models.business.model;

const messageRouter = express.Router();
const messageTagRouter = express.Router();
const messageManagementRouter = express.Router();

//-----------------------------Methods--------------------------------
/***
 * Fixes json result
 * @param messages array of messages
 */
function fixMessageObject(messages) {
    messages.map(m => {
        m.dataValues.sender_name = m.Business.dataValues.sender_name;
        delete m.dataValues.Business;
        delete m.Business;
        if (m.MessageTag.length > 0) {
            m.dataValues.tags = {};
        }
        m.MessageTag.map(t => {
            let tag = t.TagOfMessage;
            if (tag && tag.dataValues.tag_name) {
                m.dataValues.tags[tag.dataValues.tag_name] = t.dataValues.mark;
                delete m.dataValues.MessageTag;
                delete m.MessageTag;
            }
        });
        m.MessageBusinessTag = m.dataValues.MessageBusiness[0].dataValues.MessageBusinessTag;
        delete m.dataValues.MessageBusiness;
        delete m.MessageBusiness;
        if (m.MessageBusinessTag.length > 0) {
            m.dataValues.business_tags = {};
        }
        m.MessageBusinessTag.map(t => {
            let tag = t.TagOfBusinessMessage;
            if (tag && tag.dataValues.t_name) {
                m.dataValues.business_tags[tag.dataValues.t_name] = t.dataValues.mark;
            }
        });

        delete m.dataValues.MessageBusinessTag;
        delete m.MessageBusinessTag;
    });

}

/***
 * Returns Promise of DB query
 * @param businessId {Number} connected business
 * @returns {Promise<Array<Model>>}
 */
function getMessages(businessId) {
    return Message.findAll({
        where: {to_business_id: {[Op.or]: [businessId, null]}},
        include: [{
            model: Business,
            as: "Business",
            attributes: [["name", "sender_name"]]
        }, {
            model: MessageTag,
            as: "MessageTag",
            include: {model: TagOfMessage, as: "TagOfMessage", attributes: ["tag_name"]}
        },
            {
                model: MessageBusiness,
                as: "MessageBusiness",
                where: {business_id: businessId},
                required: true,
                include: {
                    model: MessageBusinessTag,
                    as: "MessageBusinessTag",
                    required: false,
                    attributes: ["id", "mark"],
                    include: {
                        model: TagOfBusinessMessage,
                        as: "TagOfBusinessMessage",
                        attributes: [["tag_name", "t_name"]]
                    }
                }
            }
        ]
    });
}

/***
 * Updates tags of message
 * @param tagsInfo {Object} format {[tag id]: Boolean}
 * @param messageId {Number} message to mark
 * @returns {Promise<void>}
 */
async function updateMessageTag(tagsInfo, messageId) {
    if (!tagsInfo) return;
    let markTrueTags = [];
    let markFalseTags = [];
    Object.keys(tagsInfo).forEach(k => {
        if (tagsInfo[k]) markTrueTags.push(k);
        else markFalseTags.push(k);
    });
    if (markTrueTags[0]) {
        await MessageTag.update({mark: true}, {where: {tag_of_message_id: markTrueTags, message_id: messageId}});
    }
    if (markFalseTags[0]) {
        await MessageTag.update({mark: false}, {where: {tag_of_message_id: markFalseTags, message_id: messageId}});
    }

}

/**
 * Updates tags of messageBusiness (for example: read, favorite)
 * @param tagsBusinessInfo {Object} format {[tag id]: Boolean}
 * @param messageId {Number} message to mark for businessId
 * @param businessId {Number} business to mark for messageId
 * @returns {Promise<void>}
 */
async function updateMessageBusinessTag(tagsBusinessInfo, messageId, businessId) {
    if (!tagsBusinessInfo) return;
    let markTrueBusinessTags = [];
    let markFalseBusinessTags = [];
    Object.keys(tagsBusinessInfo).forEach(k => {
        if (tagsBusinessInfo[k]) markTrueBusinessTags.push(k);
        else markFalseBusinessTags.push(k);
    });

    if (markTrueBusinessTags[0] || markFalseBusinessTags[0]) {
        let messageBusiness = await MessageBusiness.findOne({
            attributes: ["id"],
            where: {message_id: messageId, business_id: businessId}
        });
        if (markTrueBusinessTags[0]) {
            await MessageBusinessTag.update({mark: true}, {
                where: {
                    tag_of_business_message_id: markTrueBusinessTags,
                    message_business_id: messageBusiness.id
                }
            });
        }
        if (markFalseBusinessTags[0]) {
            await MessageBusinessTag.update({mark: false}, {
                where: {
                    tag_of_business_message_id: markFalseBusinessTags,
                    message_business_id: messageBusiness.id
                }
            });
        }
    }

}


//------------------------------------API--------------------------------
//update message
messageRouter.put("/:message_id/", async (req, res) => {
        const info = new Info();
        const businessId = req.user.business_id;
        const messsageText = req.body.message;
        const messageId = req.params.message_id;
        if(req.user.isPtAdmin)await updateMessageTag(req.body.tags, messageId);
        if (businessId)
            await updateMessageBusinessTag(req.body.business_tags, messageId, businessId);
        if (messsageText) {
            await Message.update({
                message: messsageText,
                author_id: req.user.id
            }, {where: {id: messageId}}).catch(e => {
                info.errors.push(e)
            })
        }
        res.customSend(info);
    }
);

// get messages of user
messageRouter.get("/", async (req, res) => {
    const info = new Info();
    // to_business_id null means to all users
    await getMessages(req.user.business_id).then(r => {
        fixMessageObject(r);
        info.result = r;
    }).catch(e => {
        info.errors.push(e)
    });

    res.customSend(info);
});


//Create new message (include tags)
messageRouter.post("/", async (req, res) => {
    let message = null;
    let info = new Info();
    const tags = req.body.tags;  // expected an array of tags id's
    const messageBusinessTags = req.body.business_tags;  // expected an array of tags id's
    let data = {
        message: req.body.message,
        timestamp_sending: moment().toDate(),
        to_business_id: req.body.to_business_id || null,
        from_business_id: req.user.business_id,
        author_id: req.user.id,
    };
    await Message.create(data).then(async newMessage => {
        message = newMessage;
        if (tags) {
            await updateMessageTag(tags, message.id)
        }
        if (messageBusinessTags && req.body.to_business_id) {
            await updateMessageBusinessTag(messageBusinessTags, message.id, req.body.to_business_id)
        }
    }).catch(err => {
        info.errors.push(err);
    });
    res.customSend(info);
});


//delete message for connected business
messageRouter.delete("/:message_id", async (req, res) => {
    let info = new Info();
    await MessageBusiness.destroy({
        where: {
            business_id: req.user.business_id,
            message_id: req.params.message_id,
        }
    }).catch(e=>{info.errors.push(e)});
    res.customSend(info);
});

// delete message (allowed only to admin)
messageManagementRouter.delete("/:message_id", async (req, res) => {
    let info = new Info();
    if(!req.user.isPtAdmin){
        info.errors.push(LM.getString("unauthorizedAction"));
    }
    await Message.destroy({
        where: {
            id: req.params.message_id,
            from_business_id: req.user.business_id
        }
    }).catch(e => {
        info.errors.push(e)
    });
    res.customSend(info);
});

//get tags of messages
messageTagRouter.get("/tag", async (req, res) => {
    let info = new Info();
    await TagOfMessage.findAll({attributes: ["id", "tag_name", "order"]}).then(r => {
        info.result = r
    }).catch(e => info.errors.push(e));
    res.customSend(info);
});

//get business tags of messages
messageTagRouter.get("/business_tag", async (req, res) => {
    let info = new Info();
    await TagOfBusinessMessage.findAll({attributes: ["id", "tag_name", "order"]}).then(r => {
        info.result = r
    }).catch(e => info.errors.push(e));
    res.customSend(info);
});


module.exports.messageRouter = messageRouter;
module.exports.messageTagRouter = messageTagRouter;
module.exports.messageManagementRouter = messageManagementRouter;
