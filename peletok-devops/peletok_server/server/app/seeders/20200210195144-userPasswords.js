'use strict';
const moment = require('moment');

module.exports = {
  up: (queryInterface, Sequelize) => {
    let dateNow = moment().toDate();
    let passes = [
    // {
    //   id: 1,
    //   password: '$2a$10$dkK73QbGj8CfRSDYe4Zsm.jrzSJEn8uiJBciyk9R29wwE7VywcpEy',
    //   user_id: 1,
    //   created_at: dateNow,
    //   updated_at: dateNow
    // },
    // {
    //   id: 2,
    //   password: '$2a$10$Y9bcOHR2FDkSwtB/eaYul.hTm4rFMQPgqod7Esp5d9mlL6Qf0FoMS',
    //   user_id: 2,
    //   created_at: dateNow,
    //   updated_at: dateNow
    // },
    // {
    //   id: 3,
    //   password: '$2a$10$ROSguZCgSBI1GltUImYwX.gB2nXw4z4Oqp7dL.LbAN7WzO4tLj0vG',
    //   user_id: 3,
    //   created_at: dateNow,
    //   updated_at: dateNow
    // },
    {
      id: 4,
      password: '$2a$10$zYCODKgPACgcvY8vAWfAxuLE/Xe9uFRfUOPfs82S0iY7Qh1mGW/hy',
      user_id: 4,
      created_at: dateNow,
      updated_at: dateNow
    },
    // {
    //   id: 5,
    //   password: '$2a$10$2gkycBRcFnp.CO65IRwmM.8buUMIjSUP0IhvFRjnYWEHhT2lB1uHa',
    //   user_id: 5,
    //   created_at: dateNow,
    //   updated_at: dateNow
    // },
    // {
    //   id: 6,
    //   password: '$2a$10$A1t5N1/8CrqcM454bJIIUukuSwq2W7L7psmzdQd4n3lE3GQiCwYm.',
    //   user_id: 6,
    //   created_at: dateNow,
    //   updated_at: dateNow
    // },
    // {
    //   id: 7,
    //   password: '$2a$10$gM5pJNq6ric8PpKXUQGejOoXBehhwwhSPhLq3RTp78Od7LQFQVUxa',
    //   user_id: 7,
    //   created_at: dateNow,
    //   updated_at: dateNow
    // },
    // {
    //   id: 8,
    //   password: '$2a$10$yuj3xxk1y74JlIZel0wOnOBAJJpyq/AvYseuyVL.BxOBoWGwTuhvi',
    //   user_id: 8,
    //   created_at: dateNow,
    //   updated_at: dateNow
    // },
    // {
    //   id: 9,
    //   password: '$2a$10$TPUS.Wi1DquyymFfUTTSJO0oJyGyiHya5NjRKwIPM2QDvZFK.poMy',
    //   user_id: 9,
    //   created_at: dateNow,
    //   updated_at: dateNow
    // },
    // {
    //   id: 10,
    //   password: '$2a$10$5b3VfA16ZUN5W8SmQ.RgauQmBc.tNlMPb5SHWcAESQquG4ZAbI312',
    //   user_id: 10,
    //   created_at: dateNow,
    //   updated_at: dateNow
    // },
    // {
    //   id: 11,
    //   password: '$2a$10$QLRK.uUAfQWYwq0R2mahguDgxVnu5ihFctBuSheORwlUj18m0kb3C',
    //   user_id: 11,
    //   created_at: dateNow,
    //   updated_at: dateNow
    // },
    // {
    //   id: 12,
    //   password: '$2a$10$EZCBd35jg9qx200pA7IzpeTSBQXX4O0nuB/5wb6WZLs3LGyiDzRoO',
    //   user_id: 12,
    //   created_at: dateNow,
    //   updated_at: dateNow
    // },
    // {
    //   id: 13,
    //   password: '$2a$10$MdL51wdNAjDXs8gIZXzC4.I1Jdzex09ZIULUazV6l1UFXHacqY2C.',
    //   user_id: 13,
    //   created_at: dateNow,
    //   updated_at: dateNow
    // },
    ]

    return queryInterface.bulkInsert('USR_UserPassword', passes, {});

  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('USR_UserPassword', null, {});
  }
};
