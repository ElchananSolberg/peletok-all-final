'use strict';
const moment = require('moment');

module.exports = {
  up: (queryInterface, Sequelize) => {
    let dateNow = moment().toDate();
    let roles = [
      {
        id: 1, name: 'מנהל',
        created_at: dateNow,
        updated_at: dateNow
      },
      {
        id: 2, name: 'סוכן',
        created_at: dateNow,
        updated_at: dateNow
      },
      {
        id: 3, name: 'משווק',
        created_at: dateNow,
        updated_at: dateNow
      },
      {
        id: 4, name: 'מזכירה',
        created_at: dateNow,
        updated_at: dateNow
      },
    ]
    return queryInterface.bulkInsert('PRM_Role', roles, {});

  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('PRM_Role', null, {});

  }
};
