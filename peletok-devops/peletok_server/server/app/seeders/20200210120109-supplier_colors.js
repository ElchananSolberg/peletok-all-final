'use strict';
const moment = require('moment');

module.exports = {
  up: (queryInterface, Sequelize) => {

    let dateNow = moment().toDate();
    let supplierColors = [{
      id: 1,
      logo_background: '#0072b6',
      font: '#0072b6',
      product_background: '#0072B6',
      chosen_background: '#009FD3',
      favorite_star_color: '#F8E71C',
      supplier_id: 95,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 2,
      logo_background: '#6d216f',
      font: '#6d216f',
      product_background: '#6D216F',
      chosen_background: '#982E9B',
      favorite_star_color: '#F8E71C',
      supplier_id: 94,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 3,
      logo_background: '#2dd5c4',
      font: '#1bad9f',
      product_background: '#2BCDBD',
      chosen_background: '#2BCDBD',
      favorite_star_color: '#F8E71C',
      supplier_id: 127,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 4,
      logo_background: '#715ce6 ',
      font: '#715ce6',
      product_background: '',
      chosen_background: '',
      favorite_star_color: '#F8E71C',
      supplier_id: 196,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 5,
      logo_background: '#626a7c',
      font: '#626a7c',
      product_background: '',
      chosen_background: '',
      favorite_star_color: '#F8E71C',
      supplier_id: 124,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 6,
      logo_background: '#074080',
      font: '#626a7c',
      product_background: '',
      chosen_background: '',
      favorite_star_color: '#F8E71C',
      supplier_id: 203,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 7,
      logo_background: '#1e5495',
      font: '#626a7c',
      product_background: '',
      chosen_background: '',
      favorite_star_color: '#F8E71C',
      supplier_id: 205,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 8,
      logo_background: '#f8a100',
      font: '#f8a100',
      product_background: '#fcd213d9',
      chosen_background: '#fcd213',
      favorite_star_color: '#139bfcd9',
      supplier_id: 201,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 9,
      logo_background: '#fffff',
      font: '#0072b6',
      product_background: '#5ad0f8',
      chosen_background: '#1ebdf5',
      favorite_star_color: '#F8E71C',
      supplier_id: 202,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 10,
      logo_background: '#124e1f',
      font: '#0072b6',
      product_background: '#176b29eb',
      chosen_background: '#176b29',
      favorite_star_color: '#F8E71C',
      supplier_id: 210,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 11,
      logo_background: '#690f18',
      font: '#0072b6',
      product_background: '#b75c64',
      chosen_background: '#9a474f',
      favorite_star_color: '#F8E71C',
      supplier_id: 209,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 12,
      logo_background: '#0f4a8ad6',
      font: '#626a7c',
      product_background: '#c40401',
      chosen_background: '#c40401d4',
      favorite_star_color: '#F8E71C',
      supplier_id: 211,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 13,
      logo_background: '#e05d00',
      font: '',
      product_background: '',
      chosen_background: '',
      favorite_star_color: '',
      supplier_id: 185,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 14,
      logo_background: '#e05d00',
      font: '',
      product_background: '',
      chosen_background: '',
      favorite_star_color: '',
      supplier_id: 186,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 15,
      logo_background: '#02517d',
      font: '',
      product_background: '',
      chosen_background: '',
      favorite_star_color: '',
      supplier_id: 187,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 16,
      logo_background: '#1e5495',
      font: '',
      product_background: '',
      chosen_background: '',
      favorite_star_color: '',
      supplier_id: 194,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 17,
      logo_background: '#cad624',
      font: '',
      product_background: '',
      chosen_background: '',
      favorite_star_color: '',
      supplier_id: 999,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 18,
      logo_background: '#23cf5f',
      font: '',
      product_background: '',
      chosen_background: '',
      favorite_star_color: '',
      supplier_id: 192,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 19,
      logo_background: '#626a7c',
      font: '',
      product_background: '',
      chosen_background: '',
      favorite_star_color: '',
      supplier_id: 193,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 20,
      logo_background: '#626a7c',
      font: '',
      product_background: '',
      chosen_background: '',
      favorite_star_color: '',
      supplier_id: 1,
      created_at: dateNow,
      updated_at: dateNow
    },]
    return queryInterface.bulkInsert('SUPL_SupplierColor', supplierColors, {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
    return queryInterface.bulkDelete('SUPL_SupplierColor', null, {});
  }
};
