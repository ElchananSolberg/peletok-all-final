'use strict';
const moment = require('moment');
var Sequelize = require('sequelize');
var Permission = require('../permission/models/Permission').model;
var Op = Sequelize.Op;

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const permissions = await queryInterface.select(Permission, 'PRM_Permission', {
      attributes: ['id', 'path'],
      // where: {
      //   path: {[Op.in]: ['/config/general','/main/charge']},
      // },
    });

    let rolePermissionMap = {
      1: permissions.map(p=>p.path),
      2: ['/main/gifts/pointUsage', '/main/gifts/cancelGift', '/main/charge'],
      3: ['/main/charge'],
      4: ['/main/charge']
    }

    
    let permissionRoles = []
    Object.keys(rolePermissionMap).forEach((key) => {
       let tempPermissionRoles = permissions.filter(p=>rolePermissionMap[key].indexOf(p.path) != -1).map((p) => {
           return {
              action: 'edit',
              role_id: key,
              permission_id: p.id,
              created_at: moment().toDate(),
              updated_at: moment().toDate(),
            }
           
        } )
        
       permissionRoles = permissionRoles.concat(tempPermissionRoles)
    } )
    
   if(permissionRoles.length > 0){
     return queryInterface.bulkInsert('PRM_PermissionRole', permissionRoles, {});
   }
 
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('PRM_PermissionRole', null, {});
  }
};
