'use strict';
const moment = require('moment');

module.exports = {
  up: (queryInterface, Sequelize) => {
    let dateNow = moment().toDate();
    let businesses = [
    {
      id: 1,
      name: 'peletok',
      business_identifier: 1,
      account_management: null,
      account_number: null,
      account_number_external: null,
      business_license_number: null,
      phone_number: '',
      use_article_20: false,
      payment_method: null,
      opening_hours: '',
      is_distributor: true,
      use_point: false,
      is_delek: false,
      delek_num: null,
      invoice_email: null,
      is_admin: false,
      use_distribution_fee: null,
      status: null,
      distributor_id: 1,
      agent_id: null,
      created_at: dateNow,
      updated_at: dateNow
    },
    // {
    //   id: 4,
    //   name: 'שופרסל',
    //   business_identifier: 34545,
    //   account_management: null,
    //   account_number: null,
    //   account_number_external: null,
    //   business_license_number: 123456789,
    //   phone_number: '0508735890',
    //   use_article_20: false,
    //   payment_method: 'הוראת קבע',
    //   opening_hours: '08:00 - 16:00',
    //   is_distributor: false,
    //   use_point: false,
    //   num_ip_allowed: 1,
    //   num_device_allowed: null,
    //   is_delek: false,
    //   delek_num: null,
    //   invoice_email: null,
    //   is_admin: false,
    //   use_distribution_fee: null,
    //   status: 1,
    //   distributor_id: 1,
    //   agent_id: null,
    //   created_at: dateNow,
    //   updated_at: dateNow
    // },
    // {
    //   id: 6,
    //   name: 'זול ובגדול',
    //   business_identifier: 67466,
    //   account_management: null,
    //   account_number: null,
    //   account_number_external: null,
    //   business_license_number: 133456789,
    //   phone_number: '0508735892',
    //   use_article_20: false,
    //   payment_method: 'תשלום לסוכן',
    //   opening_hours: '08:00 - 17:00',
    //   is_distributor: false,
    //   use_point: true,
    //   num_ip_allowed: null,
    //   num_device_allowed: 5,
    //   is_delek: false,
    //   delek_num: null,
    //   invoice_email: null,
    //   is_admin: false,
    //   use_distribution_fee: null,
    //   status: 3,
    //   distributor_id: 3,
    //   agent_id: null,
    //   created_at: dateNow,
    //   updated_at: dateNow
    // },
    // {
    //   id: 7,
    //   name: 'כל זול',
    //   business_identifier: 43536,
    //   account_management: null,
    //   account_number: null,
    //   account_number_external: null,
    //   business_license_number: 988654321,
    //   phone_number: '0508735893',
    //   use_article_20: false,
    //   payment_method: 'תשלום לסוכן',
    //   opening_hours: '09:00 - 21:00',
    //   is_distributor: false,
    //   use_point: false,
    //   num_ip_allowed: 4,
    //   num_device_allowed: 1,
    //   is_delek: false,
    //   delek_num: null,
    //   invoice_email: null,
    //   is_admin: false,
    //   use_distribution_fee: null,
    //   status: 1,
    //   distributor_id: 1,
    //   agent_id: null,
    //   created_at: dateNow,
    //   updated_at: dateNow
    // },
    // {
    //   id: 8,
    //   name: 'מעיין 2000',
    //   business_identifier: 65632,
    //   account_management: null,
    //   account_number: null,
    //   account_number_external: null,
    //   business_license_number: 123446789,
    //   phone_number: '0508735894',
    //   use_article_20: false,
    //   payment_method: 'תשלום לסוכן',
    //   opening_hours: '08:00 - 12:30',
    //   is_distributor: false,
    //   use_point: true,
    //   num_ip_allowed: 8,
    //   num_device_allowed: 4,
    //   is_delek: false,
    //   delek_num: null,
    //   invoice_email: null,
    //   is_admin: false,
    //   use_distribution_fee: null,
    //   status: 2,
    //   distributor_id: 3,
    //   agent_id: null,
    //   created_at: dateNow,
    //   updated_at: dateNow
    // },
    // {
    //   id: 9,
    //   name: 'יינות ביתן',
    //   business_identifier: 63478,
    //   account_management: null,
    //   account_number: null,
    //   account_number_external: null,
    //   business_license_number: 987654311,
    //   phone_number: '0508735895',
    //   use_article_20: false,
    //   payment_method: 'תשלום מראש',
    //   opening_hours: '08:00 - 16:00',
    //   is_distributor: false,
    //   use_point: true,
    //   num_ip_allowed: 2,
    //   num_device_allowed: 4,
    //   is_delek: false,
    //   delek_num: null,
    //   invoice_email: null,
    //   is_admin: false,
    //   use_distribution_fee: null,
    //   status: 3,
    //   distributor_id: 3,
    //   agent_id: null,
    //   created_at: dateNow,
    //   updated_at: dateNow
    // },
    // {
    //   id: 10,
    //   name: 'יש',
    //   business_identifier: 48756,
    //   account_management: null,
    //   account_number: null,
    //   account_number_external: null,
    //   business_license_number: 113456789,
    //   phone_number: '0508735896',
    //   use_article_20: false,
    //   payment_method: 'הוראת קבע',
    //   opening_hours: '07:00 - 21:00',
    //   is_distributor: false,
    //   use_point: true,
    //   num_ip_allowed: null,
    //   num_device_allowed: 2,
    //   is_delek: false,
    //   delek_num: null,
    //   invoice_email: null,
    //   is_admin: false,
    //   use_distribution_fee: null,
    //   status: 1,
    //   distributor_id: 1,
    //   agent_id: null,
    //   created_at: dateNow,
    //   updated_at: dateNow
    // },
    // {
    //   id: 11,
    //   name: 'יש חסד',
    //   business_identifier: 53656,
    //   account_management: null,
    //   account_number: null,
    //   account_number_external: null,
    //   business_license_number: 997654321,
    //   phone_number: '0508735897',
    //   use_article_20: false,
    //   payment_method: 'תשלום לסוכן',
    //   opening_hours: '08:00 - 17:00',
    //   is_distributor: false,
    //   use_point: false,
    //   num_ip_allowed: 2,
    //   num_device_allowed: null,
    //   is_delek: false,
    //   delek_num: null,
    //   invoice_email: null,
    //   is_admin: false,
    //   use_distribution_fee: null,
    //   status: 2,
    //   distributor_id: 1,
    //   agent_id: null,
    //   created_at: dateNow,
    //   updated_at: dateNow
    // },
    // {
    //   id: 12,
    //   name: 'מגה',
    //   business_identifier: 45612,
    //   account_management: null,
    //   account_number: null,
    //   account_number_external: null,
    //   business_license_number: 123456779,
    //   phone_number: '0508735898',
    //   use_article_20: false,
    //   payment_method: 'הוראת קבע',
    //   opening_hours: '09:00 - 21:00',
    //   is_distributor: false,
    //   use_point: true,
    //   num_ip_allowed: 1,
    //   num_device_allowed: 8,
    //   is_delek: false,
    //   delek_num: null,
    //   invoice_email: null,
    //   is_admin: false,
    //   use_distribution_fee: null,
    //   status: 3,
    //   distributor_id: 1,
    //   agent_id: null,
    //   created_at: dateNow,
    //   updated_at: dateNow
    // },
    // {
    //   id: 13,
    //   name: 'מגה בעיר',
    //   business_identifier: 88795,
    //   account_management: null,
    //   account_number: null,
    //   account_number_external: null,
    //   business_license_number: 987664321,
    //   phone_number: '0508735899',
    //   use_article_20: false,
    //   payment_method: 'הוראת קבע',
    //   opening_hours: '08:00 - 12:30',
    //   is_distributor: false,
    //   use_point: true,
    //   num_ip_allowed: null,
    //   num_device_allowed: null,
    //   is_delek: false,
    //   delek_num: null,
    //   invoice_email: null,
    //   is_admin: false,
    //   use_distribution_fee: null,
    //   status: 1,
    //   distributor_id: 1,
    //   agent_id: null,
    //   created_at: dateNow,
    //   updated_at: dateNow
    // },
    // {
    //   id: 2,
    //   name: 'אושר עד',
    //   business_identifier: 112568,
    //   account_management: null,
    //   account_number: null,
    //   account_number_external: null,
    //   business_license_number: null,
    //   phone_number: '',
    //   use_article_20: false,
    //   payment_method: null,
    //   opening_hours: '',
    //   is_distributor: true,
    //   use_point: false,
    //   num_ip_allowed: 14,
    //   num_device_allowed: 14,
    //   is_delek: false,
    //   delek_num: null,
    //   invoice_email: null,
    //   is_admin: false,
    //   use_distribution_fee: true,
    //   status: null,
    //   distributor_id: 2,
    //   agent_id: null,
    //   created_at: dateNow,
    //   updated_at: dateNow
    // },
    // {
    //   id: 5,
    //   name: 'רמי לוי',
    //   business_identifier: 53666,
    //   account_management: null,
    //   account_number: null,
    //   account_number_external: null,
    //   business_license_number: 987654321,
    //   phone_number: '0508735891',
    //   use_article_20: false,
    //   payment_method: 'הוראת קבע',
    //   opening_hours: '07:00 - 21:00',
    //   is_distributor: false,
    //   use_point: false,
    //   num_ip_allowed: 4,
    //   num_device_allowed: 3,
    //   is_delek: false,
    //   delek_num: null,
    //   invoice_email: null,
    //   is_admin: false,
    //   use_distribution_fee: true,
    //   status: 2,
    //   distributor_id: 2,
    //   agent_id: null,
    //   created_at: dateNow,
    //   updated_at: dateNow
    // },
    // {
    //   id: 3,
    //   name: 'קופיקס',
    //   business_identifier: 112268,
    //   account_management: null,
    //   account_number: null,
    //   account_number_external: null,
    //   business_license_number: null,
    //   phone_number: '',
    //   use_article_20: true,
    //   payment_method: null,
    //   opening_hours: '',
    //   is_distributor: true,
    //   use_point: false,
    //   num_ip_allowed: null,
    //   num_device_allowed: null,
    //   is_delek: false,
    //   delek_num: null,
    //   invoice_email: null,
    //   is_admin: false,
    //   use_distribution_fee: true,
    //   status: null,
    //   distributor_id: 3,
    //   agent_id: null,
    //   created_at: dateNow,
    //   updated_at: dateNow
    // }
    ];

    return queryInterface.bulkInsert('BSN_Business', businesses, {});

  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('BSN_Business', null, {});

  }
};
