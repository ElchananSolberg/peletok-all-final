'use strict';
const moment = require('moment');

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
   let dateNow = moment().toDate();
   let productDocs = [
    {
      id: 29,
      url: "/productsImages/textVoice/textVoice-29-prodouct.png",
      type: 1,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 30,
      url: "/productsImages/textVoice/textVoice-164-prodouct.png",
      type: 1,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 31,
      url: "/productsImages/pelephone/pelephone-59-product.png",
      type: 1,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 32,
      url: "/productsImages/pelephone/pelephone-79-product.png",
      type: 1,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 33,
      url: "/productsImages/pelephone/pelephone-99-product.png",
      type: 1,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 34,
      url: "/productsImages/partner/partner-60-product.png",
      type: 1,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 35,
      url: "/productsImages/partner/partner-29-product.png",
      type: 1,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 36,
      url: "/productsImages/partner/partner-50-product.png",
      type: 1,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 37,
      url: "/productsImages/partner/partner-29-product.png",
      type: 1,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 38,
      url: "/productsImages/partner/partner-50-product.png",
      type: 1,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 39,
      url: "/productsImages/partner/partner-59-product.png",
      type: 1,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 40,
      url: "/productsImages/benleumi/benleumi-13-product.png",
      type: 1,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 41,
      url: "/productsImages/benleumi/benleumi-17-product.png",
      type: 1,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 42,
      url: "/productsImages/benleumi/benleumi-40-product.png",
      type: 1,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 43,
      url: "/productsImages/cellcom/cellcom-29-product.jpg",
      type: 1,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 44,
      url: "/productsImages/cellcom/cellcom-49-product.jpg",
      type: 1,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 45,
      url: "/productsImages/cellcom/cellcom-59-product.jpg",
      type: 1,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 46,
      url: "/productsImages/free_phone/freePhone-25.png",
      type: 1,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 47,
      url: "/productsImages/free_phone/freePhone-50.png",
      type: 1,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 48,
      url: "/productsImages/free_phone/freePhone-100.png",
      type: 1,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 49,
      url: "/productsImages/jawal/jawal-10.png",
      type: 1,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 50,
      url: "/productsImages/jawal/jawal-20.png",
      type: 1,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 51,
      url: "/productsImages/jawal/jawal-30.png",
      type: 1,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 52,
      url: "/productsImages/wataniya/wataniya-10.png",
      type: 1,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 53,
      url: "/productsImages/wataniya/wataniya-15.png",
      type: 1,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 54,
      url: "/productsImages/wataniya/wataniya-21.png",
      type: 1,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 55,
      url: "/productsImages/home_card/home_card-4u.png",
      type: 1,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 56,
      url: "/productsImages/home_card/home_card-20.png",
      type: 1,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 57,
      url: "/productsImages/home_card/home_card-30.png",
      type: 1,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 58,
      url: "/productsImages/mobile012/mobile012-25-product",
      type: 1,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 59,
      url: "/productsImages/mobile012/mobile012-45-product",
      type: 1,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 60,
      url: "/productsImages/mobile012/mobile012-60-product",
      type: 1,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 61,
      url: "/productsImages/mobile012/mobile012-60-product",
      type: 1,
      created_at: dateNow,
      updated_at: dateNow
    }
  ]
  return queryInterface.bulkInsert('DOC_Document', productDocs, {});
  },


  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
   return queryInterface.bulkDelete('DOC_Document', null, {});
  }
};
