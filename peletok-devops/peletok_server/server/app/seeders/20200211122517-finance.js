'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    let finances = [{id:1,frame:1100,temp_frame:0,frame:1100,balance:20000,created_at:"2020-02-11 12:23:39.206+00",updated_at:"2020-02-11 12:23:39.206+00",business_id:1,},
    // {id:2,frame:5000,temp_frame:400,frame:5000,balance:10500,created_at:"2020-02-11 12:23:39.276+00",updated_at:"2020-02-11 12:23:39.276+00",business_id:2,},
    // {id:3,frame:55,temp_frame:10,frame:55,balance:100000,created_at:"2020-02-11 12:23:39.399+00",updated_at:"2020-02-11 12:23:39.399+00",business_id:3,},
    // {id:4,frame:100000,temp_frame:100000,frame:100000,balance:100000,created_at:"2020-02-11 12:23:39.461+00",updated_at:"2020-02-11 12:23:39.461+00",business_id:4,},
    // {id:5,frame:200,temp_frame:100,frame:200,balance:-100,created_at:"2020-02-11 12:23:39.54+00",updated_at:"2020-02-11 12:23:39.54+00",business_id:5,},
    // {id:6,frame:300,temp_frame:200,frame:300,balance:-70,created_at:"2020-02-11 12:23:39.627+00",updated_at:"2020-02-11 12:23:39.627+00",business_id:6,},
    // {id:7,frame:400,temp_frame:300,frame:400,balance:231,created_at:"2020-02-11 12:23:39.716+00",updated_at:"2020-02-11 12:23:39.716+00",business_id:7,},
    // {id:8,frame:500,temp_frame:400,frame:500,balance:-122,created_at:"2020-02-11 12:23:39.772+00",updated_at:"2020-02-11 12:23:39.772+00",business_id:8,},
    // {id:9,frame:600,temp_frame:500,frame:600,balance:111,created_at:"2020-02-11 12:23:39.82+00",updated_at:"2020-02-11 12:23:39.82+00",business_id:9,},
    // {id:10,frame:700,temp_frame:600,frame:700,balance:-625,created_at:"2020-02-11 12:23:39.866+00",updated_at:"2020-02-11 12:23:39.866+00",business_id:10,},
    // {id:11,frame:800,temp_frame:700,frame:800,balance:255,created_at:"2020-02-11 12:23:39.913+00",updated_at:"2020-02-11 12:23:39.913+00",business_id:11,},
    // {id:12,frame:100000,temp_frame:800,frame:100000,balance:-562,created_at:"2020-02-11 12:23:39.966+00",updated_at:"2020-02-11 12:23:39.966+00",business_id:12,},
    // {id:13,frame:100000,temp_frame:900,frame:100000,balance:-700,created_at:"2020-02-11 12:23:40.027+00",updated_at:"2020-02-11 12:23:40.027+00",business_id:13,},
    ]
    return queryInterface.bulkInsert('BNK_BusinessFinance', finances, {});
    
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('BNK_BusinessFinance', null, {});
    
  }
};
