'use strict';
const moment = require('moment');

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    let dateNow = moment().toDate();
    let productDocs = [
      { 
        product_id: 1, 
        document_id: 29,
        created_at: dateNow,
        updated_at: dateNow 
      },
      {
        product_id: 3,
        document_id: 30,
        created_at: dateNow,
        updated_at: dateNow
      },
      {
        product_id: 11,
        document_id: 31,
        created_at: dateNow,
        updated_at: dateNow
      },
      {
        product_id: 12,
        document_id: 32,
        created_at: dateNow,
        updated_at: dateNow
      },
      {
        product_id: 13,
        document_id: 33,
        created_at: dateNow,
        updated_at: dateNow
      },
      {
        product_id: 994,
        document_id: 34,
        created_at: dateNow,
        updated_at: dateNow
      },
      {
        product_id: 995,
        document_id: 35,
        created_at: dateNow,
        updated_at: dateNow
      },
      {
        product_id: 996,
        document_id: 36,
        created_at: dateNow,
        updated_at: dateNow
      },
      {
        product_id: 1024,
        document_id: 37,
        created_at: dateNow,
        updated_at: dateNow
      },
      {
        product_id: 1025,
        document_id: 38,
        created_at: dateNow,
        updated_at: dateNow
      },
      {
        product_id: 1026,
        document_id: 39,
        created_at: dateNow,
        updated_at: dateNow
      },
      {
        product_id: 103,
        document_id: 40,
        created_at: dateNow,
        updated_at: dateNow
      },
      {
        product_id: 101,
        document_id: 41,
        created_at: dateNow,
        updated_at: dateNow
      },
      {
        product_id: 102,
        document_id: 42,
        created_at: dateNow,
        updated_at: dateNow
      },
      {
        product_id: 1030,
        document_id: 43,
        created_at: dateNow,
        updated_at: dateNow
      },
      {
        product_id: 1031,
        document_id: 44,
        created_at: dateNow,
        updated_at: dateNow
      },
      {
        product_id: 1032,
        document_id: 45,
        created_at: dateNow,
        updated_at: dateNow
      },
      {
        product_id: 25,
        document_id: 46,
        created_at: dateNow,
        updated_at: dateNow
      },
      {
        product_id: 50,
        document_id: 47,
        created_at: dateNow,
        updated_at: dateNow
      },
      {
        product_id: 100,
        document_id: 48,
        created_at: dateNow,
        updated_at: dateNow
      },
      {
        product_id: 10,
        document_id: 49,
        created_at: dateNow,
        updated_at: dateNow
      },
      {
        product_id: 20,
        document_id: 50,
        created_at: dateNow,
        updated_at: dateNow
      },
      {
        product_id: 30,
        document_id: 51,
        created_at: dateNow,
        updated_at: dateNow
      },
      {
        product_id: 1020,
        document_id: 52,
        created_at: dateNow,
        updated_at: dateNow
      },
      {
        product_id: 1021,
        document_id: 53,
        created_at: dateNow,
        updated_at: dateNow
      },
      {
        product_id: 1022,
        document_id: 54,
        created_at: dateNow,
        updated_at: dateNow
      },
      {
        product_id: 2026,
        document_id: 55,
        created_at: dateNow,
        updated_at: dateNow
      },
      {
        product_id: 2002,
        document_id: 56,
        created_at: dateNow,
        updated_at: dateNow
      },
      {
        product_id: 2003,
        document_id: 57,
        created_at: dateNow,
        updated_at: dateNow
      },
      {
        product_id: 1506,
        document_id: 58,
        created_at: dateNow,
        updated_at: dateNow
      },
      {
        product_id: 1500,
        document_id: 59,
        created_at: dateNow,
        updated_at: dateNow
      },
      {
        product_id: 1501,
        document_id: 60,
        created_at: dateNow,
        updated_at: dateNow
      },
      {
        product_id: 1600,
        document_id: 61,
        created_at: dateNow,
        updated_at: dateNow
      }

    ]
    return queryInterface.bulkInsert('DOC_ProductDocument', productDocs, {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
   return queryInterface.bulkDelete('DOC_ProductDocument', null, {});
  }
};
