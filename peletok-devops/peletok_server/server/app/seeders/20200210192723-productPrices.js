'use strict';
const moment = require('moment');

module.exports = {
  up: (queryInterface, Sequelize) => {
    let dateNow = moment().toDate();
    let productPrices = [{
      id:1,
      fixed_price:29.9,
      buying_price:null,
      distribution_fee:0,
      timestamp_start:'2020-02-10 16:21:13.076+00',
      timestamp_end:'2099-12-30 22:00:00+00',
      product_id:1,
      author_id:4,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id:2,
      fixed_price:164.45,
      buying_price:null,
      distribution_fee:0,
      timestamp_start:'2020-02-10 16:21:13.076+00',
      timestamp_end:'2099-12-30 22:00:00+00',
      product_id:3,
      author_id:4,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id:4,
      fixed_price:75,
      buying_price:null,
      distribution_fee:0,
      timestamp_start:'2020-02-10 16:21:13.074+00',
      timestamp_end:'2099-12-30 22:00:00+00',
      product_id:12,
      author_id:4,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id:5,
      fixed_price:99,
      buying_price:null,
      distribution_fee:0,
      timestamp_start:'2020-02-10 16:21:13.075+00',
      timestamp_end:'2099-12-30 22:00:00+00',
      product_id:13,
      author_id:4,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id:7,
      fixed_price:29,
      buying_price:null,
      distribution_fee:0,
      timestamp_start:'2020-02-10 16:21:13.075+00',
      timestamp_end:'2099-12-30 22:00:00+00',
      product_id:995,
      author_id:4,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id:8,
      fixed_price:50,
      buying_price:null,
      distribution_fee:0,
      timestamp_start:'2020-02-10 16:21:13.075+00',
      timestamp_end:'2099-12-30 22:00:00+00',
      product_id:996,
      author_id:4,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id:9,
      fixed_price:29,
      buying_price:null,
      distribution_fee:0,
      timestamp_start:'2020-02-10 16:21:13.075+00',
      timestamp_end:'2099-12-30 22:00:00+00',
      product_id:1024,
      author_id:4,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id:10,
      fixed_price:50,
      buying_price:null,
      distribution_fee:0,
      timestamp_start:'2020-02-10 16:21:13.075+00',
      timestamp_end:'2099-12-30 22:00:00+00',
      product_id:1025,
      author_id:4,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id:11,
      fixed_price:59,
      buying_price:null,
      distribution_fee:0,
      timestamp_start:'2020-02-10 16:21:13.075+00',
      timestamp_end:'2099-12-30 22:00:00+00',
      product_id:1026,
      author_id:4,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id:12,
      fixed_price:13,
      buying_price:null,
      distribution_fee:0,
      timestamp_start:'2020-02-10 16:21:13.074+00',
      timestamp_end:'2099-12-30 22:00:00+00',
      product_id:103,
      author_id:4,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id:14,
      fixed_price:40,
      buying_price:null,
      distribution_fee:0,
      timestamp_start:'2020-02-10 16:21:13.074+00',
      timestamp_end:'2099-12-30 22:00:00+00',
      product_id:102,
      author_id:4,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id:15,
      fixed_price:25,
      buying_price:null,
      distribution_fee:0,
      timestamp_start:'2020-02-10 16:21:13.076+00',
      timestamp_end:'2099-12-30 22:00:00+00',
      product_id:1030,
      author_id:4,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id:16,
      fixed_price:49,
      buying_price:null,
      distribution_fee:0,
      timestamp_start:'2020-02-10 16:21:13.076+00',
      timestamp_end:'2099-12-30 22:00:00+00',
      product_id:1031,
      author_id:4,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id:17,
      fixed_price:59,
      buying_price:null,
      distribution_fee:0,
      timestamp_start:'2020-02-10 16:21:13.076+00',
      timestamp_end:'2099-12-30 22:00:00+00',
      product_id:1032,
      author_id:4,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id:18,
      fixed_price:25,
      buying_price:null,
      distribution_fee:0,
      timestamp_start:'2020-02-10 16:21:13.076+00',
      timestamp_end:'2099-12-30 22:00:00+00',
      product_id:25,
      author_id:4,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id:19,
      fixed_price:50,
      buying_price:null,
      distribution_fee:0,
      timestamp_start:'2020-02-10 16:21:13.076+00',
      timestamp_end:'2099-12-30 22:00:00+00',
      product_id:50,
      author_id:4,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id:20,
      fixed_price:100,
      buying_price:null,
      distribution_fee:0,
      timestamp_start:'2020-02-10 16:21:13.076+00',
      timestamp_end:'2099-12-30 22:00:00+00',
      product_id:100,
      author_id:4,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id:21,
      fixed_price:12,
      buying_price:null,
      distribution_fee:0,
      timestamp_start:'2020-02-10 16:21:13.076+00',
      timestamp_end:'2099-12-30 22:00:00+00',
      product_id:10,
      author_id:4,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id:22,
      fixed_price:22,
      buying_price:null,
      distribution_fee:0,
      timestamp_start:'2020-02-10 16:21:13.076+00',
      timestamp_end:'2099-12-30 22:00:00+00',
      product_id:20,
      author_id:4,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id:23,
      fixed_price:33,
      buying_price:null,
      distribution_fee:0,
      timestamp_start:'2020-02-10 16:21:13.076+00',
      timestamp_end:'2099-12-30 22:00:00+00',
      product_id:30,
      author_id:4,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id:24,
      fixed_price:13,
      buying_price:null,
      distribution_fee:0,
      timestamp_start:'2020-02-10 16:21:13.076+00',
      timestamp_end:'2099-12-30 22:00:00+00',
      product_id:1020,
      author_id:4,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id:25,
      fixed_price:17,
      buying_price:null,
      distribution_fee:0,
      timestamp_start:'2020-02-10 16:21:13.076+00',
      timestamp_end:'2099-12-30 22:00:00+00',
      product_id:1021,
      author_id:4,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id:26,
      fixed_price:25,
      buying_price:null,
      distribution_fee:0,
      timestamp_start:'2020-02-10 16:21:13.076+00',
      timestamp_end:'2099-12-30 22:00:00+00',
      product_id:1022,
      author_id:4,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id:27,
      fixed_price:74,
      buying_price:null,
      distribution_fee:0,
      timestamp_start:'2020-02-10 16:21:13.076+00',
      timestamp_end:'2099-12-30 22:00:00+00',
      product_id:2026,
      author_id:4,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id:28,
      fixed_price:20,
      buying_price:null,
      distribution_fee:0,
      timestamp_start:'2020-02-10 16:21:13.076+00',
      timestamp_end:'2099-12-30 22:00:00+00',
      product_id:2002,
      author_id:4,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id:29,
      fixed_price:30,
      buying_price:null,
      distribution_fee:0,
      timestamp_start:'2020-02-10 16:21:13.076+00',
      timestamp_end:'2099-12-30 22:00:00+00',
      product_id:2003,
      author_id:4,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id:30,
      fixed_price:null,
      buying_price:null,
      distribution_fee:0,
      timestamp_start:'2020-02-10 16:21:13.077+00',
      timestamp_end:'2099-12-30 22:00:00+00',
      product_id:1002,
      author_id:4,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id:31,
      fixed_price:null,
      buying_price:null,
      distribution_fee:0,
      timestamp_start:'2020-02-10 16:21:13.077+00',
      timestamp_end:'2099-12-30 22:00:00+00',
      product_id:1003,
      author_id:4,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id:32,
      fixed_price:null,
      buying_price:null,
      distribution_fee:0,
      timestamp_start:'2020-02-10 16:21:13.077+00',
      timestamp_end:'2099-12-30 22:00:00+00',
      product_id:1004,
      author_id:4,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id:33,
      fixed_price:null,
      buying_price:null,
      distribution_fee:0,
      timestamp_start:'2020-02-10 16:21:13.077+00',
      timestamp_end:'2099-12-30 22:00:00+00',
      product_id:1005,
      author_id:4,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id:34,
      fixed_price:null,
      buying_price:null,
      distribution_fee:0,
      timestamp_start:'2020-02-10 16:21:13.077+00',
      timestamp_end:'2099-12-30 22:00:00+00',
      product_id:1006,
      author_id:4,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id:35,
      fixed_price:null,
      buying_price:null,
      distribution_fee:0,
      timestamp_start:'2020-02-10 16:21:13.078+00',
      timestamp_end:'2099-12-30 22:00:00+00',
      product_id:1007,
      author_id:4,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id:36,
      fixed_price:25,
      buying_price:null,
      distribution_fee:0,
      timestamp_start:'2020-02-10 16:21:13.078+00',
      timestamp_end:'2099-12-30 22:00:00+00',
      product_id:1506,
      author_id:4,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id:37,
      fixed_price:45,
      buying_price:null,
      distribution_fee:0,
      timestamp_start:'2020-02-10 16:21:13.078+00',
      timestamp_end:'2099-12-30 22:00:00+00',
      product_id:1500,
      author_id:4,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id:38,
      fixed_price:50,
      buying_price:null,
      distribution_fee:0,
      timestamp_start:'2020-02-10 16:21:13.078+00',
      timestamp_end:'2099-12-30 22:00:00+00',
      product_id:1501,
      author_id:4,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id:39,
      fixed_price:50,
      buying_price:null,
      distribution_fee:0,
      timestamp_start:'2020-02-10 16:21:13.078+00',
      timestamp_end:'2099-12-30 22:00:00+00',
      product_id:1600,
      author_id:4,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id:3,
      fixed_price:59,
      buying_price:null,
      distribution_fee:10,
      timestamp_start:'2020-02-10 16:21:13.074+00',
      timestamp_end:'2099-12-30 22:00:00+00',
      product_id:11,
      author_id:4,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id:6,
      fixed_price:60,
      buying_price:null,
      distribution_fee:10,
      timestamp_start:'2020-02-10 16:21:13.075+00',
      timestamp_end:'2099-12-30 22:00:00+00',
      product_id:994,
      author_id:4,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id:13,
      fixed_price:17,
      buying_price:null,
      distribution_fee:10,
      timestamp_start:'2020-02-10 16:21:13.074+00',
      timestamp_end:'2099-12-30 22:00:00+00',
      product_id:101,
      author_id:4,
      created_at: dateNow,
      updated_at: dateNow
    },]
    return queryInterface.bulkInsert('SUPL_ProductPrice', productPrices, {});
    
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
   return queryInterface.bulkDelete('SUPL_ProductPrice', null, {});
  }
};
