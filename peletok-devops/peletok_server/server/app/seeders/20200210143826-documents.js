'use strict';
const moment = require('moment');

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    let dateNow = moment().toDate();
    let images = [{
      id: 1,
      url: 'suppliersLogos/pelephone-logo.svg',
      type: 3,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 2,
      url: 'SuppliersBackgrounds/pelephone-texture.png',
      type: 4,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 3,
      url: 'suppliersLogos/cellcom-logo.svg',
      type: 3,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 4,
      url: 'SuppliersBackgrounds/cellcom-texture.png',
      type: 4,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 5,
      url: 'suppliersLogos/partner-logo.svg',
      type: 3,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 6,
      url: 'SuppliersBackgrounds/partner-texture.png',
      type: 4,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 7,
      url: 'suppliersLogos/text_voice_logo.png',
      type: 3,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 8,
      url: 'SuppliersBackgrounds/textVoice-texture.png',
      type: 4,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 9,
      url: 'suppliersLogos/ben_leumi_logo.png',
      type: 3,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 10,
      url: 'SuppliersBackgrounds/benLeumi-texture.png',
      type: 4,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 11,
      url: 'suppliersLogos/global_sim_logo.png',
      type: 3,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 12,
      url: 'SuppliersBackgrounds/globalSim-texture.png',
      type: 4,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 13,
      url: 'suppliersLogos/bezeq-binleumi-logo.svg',
      type: 3,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 14,
      url: 'SuppliersBackgrounds/bezeqBenLeumi-texture.png',
      type: 4,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 15,
      url: 'SuppliersBackgrounds/mobile012-texture.png',
      type: 4,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 16,
      url: 'suppliersLogos/012-mobile-logo.svg',
      type: 3,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 17,
      url: 'suppliersLogos/free_phone_logo.svg',
      type: 3,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 18,
      url: 'suppliersLogos/jawal_logo.svg',
      type: 3,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 19,
      url: 'suppliersLogos/wataniya_logo.svg',
      type: 3,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 20,
      url: 'suppliersLogos/home_card_logo.svg',
      type: 3,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 21,
      url: 'suppliersLogos/israel-electric.svg',
      type: 3,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 22,
      url: 'suppliersLogos/israel-electric.svg',
      type: 3,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 23,
      url: 'suppliersLogos/kevish-6.svg',
      type: 3,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 24,
      url: 'suppliersLogos/bezeq_online.svg',
      type: 3,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 25,
      url: 'suppliersLogos/ravkav_logo.png',
      type: 3,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 26,
      url: 'suppliersLogos/carmel_tunels.svg',
      type: 3,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 27,
      url: 'suppliersLogos/authorities_payment.svg',
      type: 3,
      created_at: dateNow,
      updated_at: dateNow
    },
    {
      id: 28,
      url: 'suppliersLogos/authorities_payment.svg',
      type: 3,
      created_at: dateNow,
      updated_at: dateNow
    },]
    return queryInterface.bulkInsert('DOC_Document', images, {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
    return queryInterface.bulkDelete('DOC_Document', null, {});
  }
};
