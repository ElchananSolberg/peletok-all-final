const express = require('express');
const router = express.Router();
const {suppliers} = require('./restData/local/localDemoData.js')
const {suppliersList} = require('./restData/local/realSuppliersData.js')


//TODO - get real suppliers data from DB including images and textures 
//TODO - delete logos and textures folders and upload them from DB dynamically


router.get('/suppliersList/', async function (req, res) {
    res.send(suppliersList)
});
 


module.exports.suppliersRouter = router;