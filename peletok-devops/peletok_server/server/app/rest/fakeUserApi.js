const express = require('express');
const router = express.Router();

var userBalanceStatus = [{strRepr :'מסגרת', value: 12000.56},{strRepr :'יתרה', value: 50000.2},{strRepr :'יתרה לניצול', value: 62000.21}]
var userDetails= {id:'1',permission:'manager'};
var userGeneralDetails= {balance:userBalanceStatus, details:userDetails};

// get user id,balance and permissions right after login
router.get('/generalInformation/:userId', async function (req, res) {
    res.send(userGeneralDetails)
});

module.exports.fakeUserRouter = router;
 