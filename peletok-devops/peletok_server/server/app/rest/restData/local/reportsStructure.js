const LM = require('../../../config/LanguageManager').LM;


let reportData0 = {
    reportName: LM.getString('actionReport'), // Display
    reportID: "0", // will serve as id for backend
    fields: [ // each inner array represents a list in display
        [ // each object in a line represents a field to be created
            {
                title: LM.getString('selectVendorList'),  // Display
                id: "provider",  // will serve as id for backend
                fieldType: "supplierList",
                // options: this will be filed on client side
                default: LM.getString('all'),
            },
            {
                title: LM.getString('phoneNumber'),
                id: "phoneNumber",
                fieldType: "Number",
                tooltip: LM.getString('phoneNumberTooltip'),
                minLength: 9, // -1 for no limit
                maxLength: 13,
            },
        ],
        [
            {
                title: LM.getString("selectServiceList"),
                id: "service_type",
                fieldType: "RadioButton",
                options: [LM.getString('all'), "כרטיס ידני", "כרטיס וירטואלי", "תשלום חשבון"],
            }
        ],
        [
            {
                title: LM.getString('startDate'),
                id: "startDate",
                fieldType: "Date",
            },
            {
                default: "00:00",
                title: LM.getString('time'),
                id: "startDateTime",
                fieldType: "Time"
            }
        ],
        [
            {
                title: LM.getString('endDate'),
                id: "endDate",
                fieldType: "Date",
            },
            {
                default: "23:59",
                title: LM.getString('time'),
                id: "endDateTime",
                fieldType: "Time"
            }
        ]
    ],
    // is_cancelable determines for each report whether to display the cancellation request column
    // In addition, in the reportsApi file for each report data request, it appears for each row whether it can be undone
    is_cancelable: true,
    // is_printable determines for each report whether to display the print column
    // In addition, in the reportsApi file for each report data request, is_printable = false for a non-printable row
    is_printable: true,
    // sum_is_displayed determines whether to display a summary line in this report
    sum_business_price_is_displayed: true,
    sum_consumer_price_is_displayed: true,
    sum_profit_is_displayed: true,
    sum_payment_is_displayed: false,
    sum_consumer_price_is_displayed_in_head: true,
    current_balance_is_displayed: false

};


const reportData1 = {
    reportName: "דוח תשלומים", // Display
    reportID: "1", // will serve as id for backend
    fields: [ // each inner array represents a list in display
        [
            {
                title: LM.getString('startDate'),
                id: "startDate",
                fieldType: "Date",
            },
            {
                default: "00:00",
                title: LM.getString('time'),
                id: "startDateTime",
                fieldType: "Time"
            }
        ],
        [
            {
                title: LM.getString('endDate'),
                id: "endDate",
                fieldType: "Date",
            },
            {
                default: "23:59",
                title: LM.getString('time'),
                id: "endDateTime",
                fieldType: "Time"
            }
        ]
    ],
    is_cancelable: false,
    is_printable: false,
    sum_business_price_is_displayed: false,
    sum_consumer_price_is_displayed: false,
    sum_profit_is_displayed: false,
    sum_payment_is_displayed: true,
    sum_consumer_price_is_displayed_in_head: false,
    current_balance_is_displayed: false
};

const reportData2 = {
    reportName: "דוח תשלומים ופעילויות", // Display
    reportID: "2", // will serve as id for backend
    fields: [ // each inner array represents a list in display
        [
            {
                title: LM.getString('startDate'),
                id: "startDate",
                fieldType: "Date",
            },
            {
                default: "00:00",
                title: LM.getString('time'),
                id: "startDateTime",
                fieldType: "Time"
            }
        ],
        [
            {
                title: LM.getString('endDate'),
                id: "endDate",
                fieldType: "Date",
            },
            {
                default: "23:59",
                title: LM.getString('time'),
                id: "endDateTime",
                fieldType: "Time"
            }
        ]
    ],
    is_cancelable: false,
    is_printable: false,
    sum_business_price_is_displayed: false,
    sum_consumer_price_is_displayed: false,
    sum_profit_is_displayed: false,
    sum_payment_is_displayed: false,
    sum_consumer_price_is_displayed_in_head: false,
    current_balance_is_displayed: true
};

const reportData3 = {
    reportName: "דוח כרטיסים ידניים", // Display
    reportID: "3", // will serve as id for backend
    fields: [ // each inner array represents a list in display
        [
            {
                title: LM.getString('selectVendorList'),  // Display
                id: "provider",  // will serve as id for backend
                fieldType: "perpaidList",
                // options: this will be filled on client side
                default: LM.getString('all'),
            },
        ],
        [
            {
                title: LM.getString('startDate'),
                id: "startDate",
                fieldType: "Date",
            },
            {
                default: "00:00",
                title: LM.getString('time'),
                id: "startDateTime",
                fieldType: "Time"
            }
        ],
        [
            {
                title: LM.getString('endDate'),
                id: "endDate",
                fieldType: "Date",
            },
            {
                default: "23:59",
                title: LM.getString('time'),
                id: "endDateTime",
                fieldType: "Time"
            }
        ]
    ],
    is_cancelable: false,
    is_printable: false,
    sum_business_price_is_displayed: false,
    sum_consumer_price_is_displayed: false,
    sum_profit_is_displayed: false,
    sum_payment_is_displayed: false,
    sum_consumer_price_is_displayed_in_head: false,
    current_balance_is_displayed: false
};

const reportData4 = {
    reportName: "דוח ריכוז כרטיסים", // Display
    reportID: "4", // will serve as id for backend
    fields: [ // each inner array represents a list in display
        [
            {
                title: LM.getString('selectVendorList'),  // Display
                id: "provider",  // will serve as id for backend
                fieldType: "supplierList",
                // options: this will be filled on client side
                default: LM.getString('all'),
            },
        ],
        [
            {
                title: LM.getString("selectServiceList"),
                id: "service_type",
                fieldType: "RadioButton",
                options: [LM.getString('all'), "כרטיס ידני", "כרטיס וירטואלי", "תשלום חשבון"],
            }
        ],
        [
            {
                title: LM.getString('startDate'),
                id: "startDate",
                fieldType: "Date",
            },
            {
                default: "00:00",
                title: LM.getString('time'),
                id: "startDateTime",
                fieldType: "Time"
            }
        ],
        [
            {
                title: LM.getString('endDate'),
                id: "endDate",
                fieldType: "Date",
            },
            {
                default: "23:59",
                title: LM.getString('time'),
                id: "endDateTime",
                fieldType: "Time"
            }
        ]
    ],
    is_cancelable: false,
    is_printable: false,
    sum_business_price_is_displayed: true,
    sum_consumer_price_is_displayed: true,
    sum_profit_is_displayed: true,
    sum_payment_is_displayed: false,
    sum_consumer_price_is_displayed_in_head: true,
    current_balance_is_displayed: false
};

const reportData5 = {
    reportName: "דוח תשלום לכביש 6", // Display
    reportID: "5", // will serve as id for backend
    supplierId: 187,
    fields: [ // each inner array represents a list in display
        [
            {
                title: LM.getString('startDate'),
                id: "startDate",
                fieldType: "Date",
            },
            {
                default: "00:00",
                title: LM.getString('time'),
                id: "startDateTime",
                fieldType: "Time"
            }
        ],
        [
            {
                title: LM.getString('endDate'),
                id: "endDate",
                fieldType: "Date",
            },
            {
                default: "23:59",
                title: LM.getString('time'),
                id: "endDateTime",
                fieldType: "Time"
            }
        ]
    ],
    is_cancelable: false,
    is_printable: false,
    sum_business_price_is_displayed: false,
    sum_consumer_price_is_displayed: true,
    sum_profit_is_displayed: false,
    sum_payment_is_displayed: false,
    sum_consumer_price_is_displayed_in_head: true,
    current_balance_is_displayed: false
};

const reportData6 = {
    reportName: "דוח מנהרות הכרמל", // Display
    reportID: "6", // will serve as id for backend
    supplierId: 192,
    fields: [ // each inner array represents a list in display
        [
            {
                title: LM.getString('startDate'),
                id: "startDate",
                fieldType: "Date",
            },
            {
                default: "00:00",
                title: LM.getString('time'),
                id: "startDateTime",
                fieldType: "Time"
            }
        ],
        [
            {
                title: LM.getString('endDate'),
                id: "endDate",
                fieldType: "Date",
            },
            {
                default: "23:59",
                title: LM.getString('time'),
                id: "endDateTime",
                fieldType: "Time"
            }
        ]
    ],
    is_cancelable: false,
    is_printable: false,
    sum_business_price_is_displayed: false,
    sum_consumer_price_is_displayed: true,
    sum_profit_is_displayed: false,
    sum_payment_is_displayed: false,
    sum_consumer_price_is_displayed_in_head: true,
    current_balance_is_displayed: false
};

const reportData7 = {
    reportName: "דוח תשלום לבזק", // Display
    reportID: "7", // will serve as id for backend
    supplierId: 194,
    fields: [ // each inner array represents a list in display
        [
            {
                title: LM.getString('phoneNumber'),
                id: "phone",
                fieldType: "Number",
                tooltip: LM.getString('phoneNumberTooltip'),
                minLength: 9, // -1 for no limit
                maxLength: 13,
            },
            {
                title: LM.getString('transactionNumber'),
                id: "transaction",
                fieldType: "Number",
                minLength: -1, // -1 for no limit
                // maxLength: 13,
            },
            {
                title: LM.getString('BezeqPhoneNumber'),
                id: "bezeq",
                fieldType: "Number",
                tooltip: LM.getString('phoneNumberTooltip'),
                minLength: 9, // -1 for no limit
                maxLength: 13,
            },
        ],
        [
            {
                title: LM.getString('startDate'),
                id: "startDate",
                fieldType: "Date",
            },
            {
                default: "00:00",
                title: LM.getString('time'),
                id: "startDateTime",
                fieldType: "Time"
            }
        ],
        [
            {
                title: LM.getString('endDate'),
                id: "endDate",
                fieldType: "Date",
            },
            {
                default: "23:59",
                title: LM.getString('time'),
                id: "endDateTime",
                fieldType: "Time"
            }
        ]
    ],
    is_cancelable: false,
    is_printable: false,
    sum_business_price_is_displayed: false,
    sum_consumer_price_is_displayed: true,
    sum_profit_is_displayed: false,
    sum_payment_is_displayed: false,
    sum_consumer_price_is_displayed_in_head: true,
    current_balance_is_displayed: false
};

const reportData8 = {
    reportName: "דוח תשלום לחברת חשמל לישראל", // Display
    reportID: "8", // will serve as id for backend
    supplierId: 185,
    fields: [ // each inner array represents a list in display
        // [
        //     {
        //         title: LM.getString('transactionNumber'),
        //         id: "transaction",
        //         fieldType: "Number",
        //         minLength: -1, // -1 for no limit
        //         // maxLength: 13,
        //     },
        //     {
        //         title: LM.getString('contractNumber'),
        //         id: "contract",
        //         fieldType: "Number",
        //         minLength: -1, // -1 for no limit
        //         // maxLength: 13,
        //     },
        // ],
        [
            {
                title: LM.getString('startDate'),
                id: "startDate",
                fieldType: "Date",
            },
            {
                default: "00:00",
                title: LM.getString('time'),
                id: "startDateTime",
                fieldType: "Time"
            }
        ],
        [
            {
                title: LM.getString('endDate'),
                id: "endDate",
                fieldType: "Date",
            },
            {
                default: "23:59",
                title: LM.getString('time'),
                id: "endDateTime",
                fieldType: "Time"
            }
        ]
    ],
    is_cancelable: false,
    is_printable: false,
    sum_business_price_is_displayed: false,
    sum_consumer_price_is_displayed: true,
    sum_profit_is_displayed: false,
    sum_payment_is_displayed: false,
    sum_consumer_price_is_displayed_in_head: true,
    current_balance_is_displayed: false
};

const reportData9 = {
    reportName: "דוח טעינה לחברת חשמל לישראל", // Display
    reportID: "9", // will serve as id for backend
    supplierId: 186,
    fields: [ // each inner array represents a list in display
        // [
        //     {
        //         title: LM.getString('transactionNumber'),
        //         id: "transaction",
        //         fieldType: "Number",
        //         minLength: -1, // -1 for no limit
        //         // maxLength: 13,
        //     },
        //     {
        //         title: LM.getString('contractNumber'),
        //         id: "contract",
        //         fieldType: "Number",
        //         minLength: -1, // -1 for no limit
        //         // maxLength: 13,
        //     },
        // ],
        [
            {
                title: LM.getString('startDate'),
                id: "startDate",
                fieldType: "Date",
            },
            {
                default: "00:00",
                title: LM.getString('time'),
                id: "startDateTime",
                fieldType: "Time"
            }
        ],
        [
            {
                title: LM.getString('endDate'),
                id: "endDate",
                fieldType: "Date",
            },
            {
                default: "23:59",
                title: LM.getString('time'),
                id: "endDateTime",
                fieldType: "Time"
            }
        ]
    ],
    is_cancelable: false,
    is_printable: false,
    sum_business_price_is_displayed: false,
    sum_consumer_price_is_displayed: true,
    sum_profit_is_displayed: false,
    sum_payment_is_displayed: false,
    sum_consumer_price_is_displayed_in_head: true,
    current_balance_is_displayed: false
};

const reportData10 = {
    reportName: "דוח לרשויות", // Display
    reportID: "10", // will serve as id for backend
    supplierId: 193,
    fields: [ // each inner array represents a list in display
        [
            {
                title: LM.getString('startDate'),
                id: "startDate",
                fieldType: "Date",
            },
            {
                default: "00:00",
                title: LM.getString('time'),
                id: "startDateTime",
                fieldType: "Time"
            }
        ],
        [
            {
                title: LM.getString('endDate'),
                id: "endDate",
                fieldType: "Date",
            },
            {
                default: "23:59",
                title: LM.getString('time'),
                id: "endDateTime",
                fieldType: "Time"
            }
        ]
    ],
    is_cancelable: false,
    is_printable: false,
    sum_business_price_is_displayed: false,
    sum_consumer_price_is_displayed: true,
    sum_profit_is_displayed: false,
    sum_payment_is_displayed: false,
    sum_consumer_price_is_displayed_in_head: true,
    current_balance_is_displayed: false
};
let reports = [{
    id: reportData0.reportID,
    reportName: reportData0.reportName,
    data: reportData0
}, {
    id: reportData1.reportID,
    reportName: reportData1.reportName,
    data: reportData1
}, {
    id: reportData2.reportID,
    reportName: reportData2.reportName,
    data: reportData2
}, {
    id: reportData3.reportID,
    reportName: reportData3.reportName,
    data: reportData3
}, {
    id: reportData4.reportID,
    reportName: reportData4.reportName,
    data: reportData4
}, {
    id: reportData5.reportID,
    reportName: reportData5.reportName,
    data: reportData5
}, {
    id: reportData6.reportID,
    reportName: reportData6.reportName,
    data: reportData6
}, {
    id: reportData7.reportID,
    reportName: reportData7.reportName,
    data: reportData7
}, {
    id: reportData8.reportID,
    reportName: reportData8.reportName,
    data: reportData8
}, {
    id: reportData9.reportID,
    reportName: reportData9.reportName,
    data: reportData9
}, {
    id: reportData10.reportID,
    reportName: reportData10.reportName,
    data: reportData10
}]


async function getReports(req) {
    const models = require("../../../index");
    const busSupplierModel = models.businessSupplier.model;
    let busSupplier = await busSupplierModel.findAll({
        where: {business_id: req.user.business_id, is_authorized: true},
        attributes: ["supplier_id"],
        raw: true
    });
    let supAuthorizedList = busSupplier.map(bs => bs.supplier_id);
    return reports.filter(r => {
        return (!r.data.supplierId) || (supAuthorizedList.indexOf(r.data.supplierId) !== -1)
    })
}

module.exports.getReports = getReports;
