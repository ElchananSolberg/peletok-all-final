const csvParser = require("csv-parser");
const path = require('path');
const fs = require("fs");
const logger = require("../../../../logger");

const models = require("../../../../index");
const DEFAULT_AREA_ID = 1;


/**
 * Converts city name to city id if exist
 * @param csvData data
 * @return {Promise<void>}
 */
async function fixCity(csvData) {
    const citiesFromTable = await models.city.model.findAll();
    for (const c of csvData.values()) {
        for (const cft of citiesFromTable.values()) {
            if (cft.name_HE === c.city) {
                c.city_id = cft.id;
                break;
            }
        }
        if (!c.city_id) {
            c.area_id = DEFAULT_AREA_ID || c.area_id;
            c.city_name = c.city;
        }
    }
}

/**
 * Converts city name to city id if exist
 * @param csvData data
 * @param cityPaymentTypeId id of pay option
 * @return {Promise<void>}
 */
async function fixLink(csvData, cityPaymentTypeId) {
    const sitesFromTable = await models.site.model.findAll();
    for (const row of csvData.values()) {
        logger.debug("row: ");
        logger.debug(row);
        for (const sft of sitesFromTable.values()){
            if (row.link.indexOf(sft.site_url) === 0) {
                row.site_id = sft.id;
                row.params = row.link.replace(sft.site_url, "");
                break;
            }
        }
        if (!row.site_id) {
            throw Error(`${row.link} not valid, please check that site is exist in the "Site" table`);
        }
        row.city_payment_type_id = cityPaymentTypeId;
    }
}

/**
 * Returns the data from csv
 * @param fileName name of the csv file
 * @return {Promise<any>}
 */
function getCsvData(fileName) {
    const pathFile = path.join(__dirname, fileName);
    let csvData = [];
    return new Promise(function (resolve, reject) {
        let i = fs.createReadStream(pathFile, {encoding: "utf8"}).pipe(csvParser());
        i.on('data', (data) => {
            csvData.push(data)
        });
        i.on('end', () => {
            resolve(csvData)
        });
    })
}

/**
 * Fills data of city payment from csv file
 * @param fileName csv file name (The file must exist in the current folder)
 * @param cityPaymentTypeId id of pay option
 * @return {Promise<Array>}
 */
async function createDataFromCsv(fileName, cityPaymentTypeId) {
    let csvData = await getCsvData(fileName);
    await fixCity(csvData);
    await fixLink(csvData, cityPaymentTypeId);
    const {createDemoFromObjects} = require("../../../../mapping/createDemo");
    await createDemoFromObjects("link", csvData);
}

module.exports.createDataFromCsv = createDataFromCsv;


