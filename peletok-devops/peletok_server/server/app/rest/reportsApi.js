const path = require('path');
const express = require('express');
const router = express.Router();
const hashavshevetRouter = express.Router();
const { getReports } = require('./restData/local/reportsStructure');
const { results } = require('.//restData/local/reportsResultsDemoData.js');
const reportView = require("../transaction/view/ReportView.js");
const sequelizeInstance = require('../setupSequelize');
const { QueryTypes } = require('sequelize');
const LM = require('../config/LanguageManager').LM;
const { Info } = require("../mapping/crudMiddleware");
const logger = require("../logger");
const moment = require('moment');
let { cleanRawKeys } = require('../utils')

const reportViewModel = reportView.model;
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const getType = require('../utils').getTypeNum;
const actionsReportId = '0';
const paymentsReportId = '1';
const paymentsAndActionsReportId = '2';
const manualCardsReportId = '3';
const allCardsReportId = '4';
const roadSixReportId = '5';
const carmelTunnelsReportId = '6';
const bezeqReportId = '7';
const electricityPaymentReportId = '8';
const electricityChargeReportId = '9';
const authoritiesReportId = '10';

const managerBalanceByDateReportId = 'remainderByDate';

const hashavshevetDir = path.join(__dirname, '/restData/hashavshevet/');
const managerTransactionsReportId = '11';
const managerPaymentsReportId = '12';

const managerActionsReportId = '13';
const managerPaymentsAndActionReportId = '14';
const managerActionsDistributionReportId = '15';
const managerManualCardsReportId = '17';
const managerAllCardsReportId = '18';
const managerObligoReportId = '16';
const managerInvoiceMailReportReportId = '19';
const sequelize = require('sequelize');
const getStatus = require('../utils').getNumStatus;
const iecPaymentId = '185';
const iecChargeId = '186';
const authoritiesId = '193';
const bezekId = '194';
const models = require('../index.js');
const businessPaymentModel = models.businessPayment;
const businessFinanceLogModel = models.businessFinanceLog;
const business = models.business;
const businessFinanceModel = models.businessFinance.model;
const roadSixId = '187';
const carmelTunnelsId = '192';

const succeedStatus = getStatus("succeeded", "Transaction");

const DATE_FORMAT = 'YYYY-MM-DD';
const TIME_FORMAT = 'HH24:MI:SS';
let DAYS = ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"];
let ALPHA_BET_DAYS = {
    sunday: "א",
    monday: "ב",
    tuesday: "ג",
    wednesday: "ד",
    thursday: "ה",
    friday: "ו",
    saturday: "ז"
}
    ;
const goodSuppliersIds = '(186,187,192)'
const prepaidSuppliersTypes = '(1,3)'
const paymentsSuppliersType = getType('bills', 'supplier')
const virtualAndManualProductTypes = '(1,2)'


/**
 * Returns string filter to filtering by date (or true if not filtering)
 * @param data {Object} to filter data by datetime
 * @param modelName {string} model name for query
 * @returns {boolean||string}
 */
function getDateFilter(data, modelName) {
    let dateFilter = true;

    if (data && data['startDate'] && data['endDate']) {
        dateFilter = `
       "${modelName}"."created_at" BETWEEN '${moment(data.startDate).toISOString()}' AND '${moment(data.endDate).toISOString()}'
       `
    }
    return dateFilter;
}

function setDateFilter(data) {
    if (data.startDate && data.startDateTime && data.endDate && data.endDateTime) {
        if (data.endDateTime == "23:59") {
            data.endDateTime = data.endDateTime + ":59"
        }
        data["startDate"] = data.startDate + " " + data.startDateTime;
        data["endDate"] = data.endDate + " " + data.endDateTime;
    }
    if (data.date && data.time) {
        data["date"] = data.date + " " + data.time;
    }
    return data;
}

/**
 * Returns sequelize promise with payment and action report
 * @param data {Object} to filter data by datetime
 * @param businessId {number} id of business for report
 * @returns {Promise}
 */
function getPaymentAndActionReport(data, businessId) {
    let payment = LM.getString("payment");
    let query = `SELECT id, to_char("created_at", ?) AS "date",
       to_char("created_at", ?) AS "time",
       "business_name",
       "business_identifier" AS "customerNumber",
       "supplier_name"                     AS "supplier",
       "action",
       "product_name"                      AS "product",
       "phone_number",
       "price" as consumer_price,
       "business_price",
       "transaction_type",
       "is_cancelable",
       "cancel_process_status",
       "is_canceled",
       to_char("business_balance", 'FM999999999.00') as balance2,
       true as is_printable
FROM "REP_ReportView" AS "ReportView"
WHERE (("ReportView"."deleted_at" > '${moment().toISOString()}' OR "ReportView"."deleted_at" IS NULL) AND (${getDateFilter(data, 'ReportView')} AND
        "ReportView"."business_id" = ?) AND status=${succeedStatus})
UNION
SELECT "BusinessPayment".id, to_char("date", ?) AS "date",
       "BusinessPayment"."hour"      AS "time",
       "Business"."name"             AS "Business.business_name",
       "Business"."business_identifier" AS "customerNumber",
       '',
       '${payment}',
       '',
       '',
       null,
       "BusinessPayment"."sum"       AS business_price,
       null,
       false,
       'NO',
       false,
       to_char("business_credit", 'FM999999999.00') as balance2,
       false as is_printable
                 FROM "BNK_BusinessPayment" AS "BusinessPayment"
         LEFT OUTER JOIN "BSN_Business" AS "Business" ON "BusinessPayment"."business_id" = "Business"."id" AND
                                                         ("Business"."deleted_at" > '${moment().toISOString()}' OR
                                                          "Business"."deleted_at" IS NULL)
WHERE (("BusinessPayment"."deleted_at" > '${moment().toISOString()}' OR "BusinessPayment"."deleted_at" IS NULL) AND
       ("BusinessPayment"."business_id" = ? AND
        ${getDateFilter(data, 'BusinessPayment')} ))
order by date, time, id limit ? offset ?;`;
    return sequelizeInstance.query(query, {
        replacements: [DATE_FORMAT, TIME_FORMAT, businessId, DATE_FORMAT, businessId, data.limit ? data.limit : null, data.offset ? data.offset : 0],
        type: QueryTypes.SELECT
    });
}


/**
 * crates object that contains req filters for manager DB query
 * @param {*} data
 */

function managerCreateWhereData(data) {
    //TODO - add payment type filter, add agent filter

    let whereObj = {};

    if (data['startDate'] && data['endDate']) {
        whereObj['created_at'] = { [Op.between]: [data.startDate, data.endDate] }
    }

    if (data['business_id']) {
        whereObj['business_id'] = data['business_id']
    }
    if (data['business_identifier']) {
        whereObj['business_identifier'] = data['business_identifier']
    }
    if (data['distributor_id']) {
        whereObj['distributor_id'] = data['distributor_id']
    }
    if (data['status']) {
        if (data['status'].length > 1) {
            data['status'] = data['status'].split(',');
        }
        whereObj['status'] = data['status']
    }
    if (data['phone_number']) {
        whereObj['phone_number'] = data['phone_number']
    }
    if (data['supplier_id']) {
        whereObj['supplier_id'] = data['supplier_id']
    }
    if (data['business_frozen'] == true || data['business_frozen'] == 'true') {
        whereObj['business_status'] = getStatus('frozen', 'Business')
    }
    if (data['business_blocked'] == true || data['business_blocked'] == 'true') {
        whereObj['business_status'] = getStatus('blocked', 'Business')
    } else if (data['business_deleted'] == true || data['business_deleted'] == 'true') {
        whereObj['is_business_deleted'] = { [Op.not]: null }
    }

    if (data['invoice_number']) {
        whereObj['invoice_number'] = data['invoice_number']
    }

    if (data['product_id']) {
        whereObj['product_id'] = data['product_id']
    }
    if (data['agent_id']) {
        whereObj['agent_id'] = data['agent_id']
    }
    if (data['transaction_status']) {
        whereObj['transaction_status'] = data['transaction_status']
    }
    if (data['business_status']) {
        if (data['business_status'].length > 1) {
            data['business_status'] = data['business_status'].split(',');
        }
        whereObj['business_status'] = data['business_status']
    }
    if (data['business_tags_id']) {
        whereObj['business_tags_id'] = data['business_tags_id']
    }
    if (data['action']) {
        whereObj['action'] = data['action']
    }
    if (data['manual'] == 'true' || data['manual'] === true) {
        whereObj['product_type'] = getType('manual', 'product')
    } else if (data['virtual'] == 'true' || data['virtual'] === true) {
        whereObj['product_type'] = getType('virtual', 'product')
    }
    if (data['bills'] == 'true' || data['bills'] === true) {
        whereObj['supplier_type'] = getType('bills', 'supplier')
    }

    return whereObj
}


/**
 * crates object that contains req filters for DB query
 * @param {*} data
 */

function createWhereData(data, businessId) {
    let whereObj = {};

    if (data['startDate'] && data['endDate']) {
        whereObj['created_at'] = { [Op.between]: [data.startDate, data.endDate] }
    }

    if (data['phoneNumber']) {
        whereObj['phone_number'] = data['phoneNumber']
    }

    if (data['supplier_id']) {
        whereObj['supplier_id'] = data['supplier_id']
    }

    if (data['manual'] == 'true' || data['manual'] === true) {
        whereObj['product_type'] = getType('manual', 'product')
    } else if (data['virtual'] == 'true' || data['virtual'] === true) {
        whereObj['product_type'] = getType('virtual', 'product')
    }
    if (data['bills'] == 'true' || data['bills'] === true) {
        whereObj['supplier_type'] = getType('bills', 'supplier')
    }
    whereObj['business_id'] = businessId;

    return whereObj


}

/**
 * Returns sum business price and sum consumer price in concentration report
 * @param reports {Array} concentration report
 * @returns {{consumer_price: string, business_price: string}}
 */
function sumGroupTransactions(reports) {
    let business_price = 0;
    let consumer_price = 0;
    for (let i = 0; i < reports.length; i++) {
        let report = reports[i];
        business_price += report.dataValues.business_price * report.dataValues['sumCharges'];
        consumer_price += report.dataValues.consumer_price * report.dataValues['sumCharges'];
    }

    return { business_price: business_price.toFixed(2), consumer_price: consumer_price.toFixed(2) };
}

function createWhereDataForBalanceByDate(data) {
    let whereObj = { "is_abstract": false };
    if (data.business_id) {
        whereObj.id = data.business_id;
    }
    if (data.distributor_id) {
        whereObj["distributor_business_identifier"] = data.distributor_id;
    }
    if (data.payment_method) {
        whereObj["payment_method"] = data.payment_method;
    }
    return whereObj;
}

/**
 * Gets reports from view according to req args
 */
async function dynamicReportsGetter(businessId, reportId, data, countOnly = false, req, res) {

    const returnData = !countOnly;

    let defaultModel = reportViewModel;
    let options = {};
    let paymentsFlag = false;

    const dateColFromCreated = [sequelize.fn("to_char",
        sequelize.col("created_at"), DATE_FORMAT), "date"];
    const dateColFromDate = [sequelize.fn("to_char",
        sequelize.col("date"), DATE_FORMAT), "date"];
    const timeCol = [sequelize.fn("to_char",
        sequelize.col("created_at"), TIME_FORMAT), "time"];
    const hourAsTime = ['hour', "time"];

    const businessName = 'business_name';
    const distributorName = 'distributor_name'
    const busId = ['business_identifier', 'customerNumber'];

    const action = 'action';
    const phoneNumber = 'phone_number';

    const price = 'price';
    const consumerPrice = [price, "consumer_price"];
    const invoiceAmount = [price, 'invoiceAmount'];
    const payAmount = [price, "paymentAmount"];
    const priceAsAmount = [price, 'amount'];
    const businessPrice = 'business_price';
    const sumAsPay = ['sum', 'payment'];
    const prices = [consumerPrice, businessPrice];

    const transactionType = "transaction_type";
    const cancelable = "is_cancelable";
    const cancelProcessStatus = "cancel_process_status";
    const canceled = "is_canceled";
    const transactionStatusFields = [transactionType, cancelable, cancelProcessStatus, canceled];

    const supplierAndProd = [['supplier_name', 'supplier'], ['product_name', 'product']];

    const city = 'city';
    const cityName = ['city_pay_city', city];
    const cityPayType = ['city_pay_type', 'paymentType'];

    const clientName = 'client_name';
    const billNumber = 'bill_number';
    const invoiceNum = ['invoice_number', 'realInvoiceNumber'];
    const customerId = 'customer_id';
    const invoiceDetails = [customerId, billNumber, invoiceAmount];
    const contractNum = ['contract_number', 'contractNumber'];
    const confirmation = ['confirmation', 'confirmationNumber'];
    const clientddress = ['client_address', 'address'];
    const clientDetails = [clientName, clientddress, 'contact_identification'];
    const externalId = 'external_transaction_id';
    const comment = 'comment';

    const cardNum = ['card_number', 'cardNumber'];
    const cardCode = ['card_code', 'code'];
    const cardDetails = [cardNum, cardCode];

    const bezeqDetails = ['bezek_phone_number', 'bezek_client_id'];

    let phoneNumberColFlag = true;
    let dateColFlag = true;
    let timeColFlag = true;
    let idColFlag = true;
    let baseAttributesFlag = true;

    let baseAttributes = [];
    let attributes = [];
    if (returnData) {
        //TODO: Add agent
        baseAttributes = [businessName, busId, ...transactionStatusFields]
    }

    switch (reportId) {
        case actionsReportId:
            options.where = createWhereData(data, businessId);
            if (returnData) {
                attributes = [city, ...supplierAndProd, action, ...prices];
            }
            break;
        case roadSixReportId:
            options.where = {
                created_at: { [Op.between]: [data.startDate, data.endDate] },
                business_id: businessId,
                supplier_id: roadSixId
            };
            if (returnData) {
                attributes = invoiceDetails;
            }
            break;

        case carmelTunnelsReportId:
            options.where = {
                created_at: { [Op.between]: [data.startDate, data.endDate] },
                business_id: businessId,
                supplier_id: carmelTunnelsId
            };
            if (returnData) {
                attributes = invoiceDetails;
            }
            break;

        case manualCardsReportId:
            options.where = {
                product_type: getType('manual', 'product'),
                created_at: { [Op.between]: [data.startDate, data.endDate] },
                business_id: businessId
            };
            if (data.supplier_id) {
                options.where.supplier_id = data.supplier_id;
            }
            if (returnData) {
                phoneNumberColFlag = false;
                attributes = [...supplierAndProd, ...cardDetails]
            }
            break;
        case electricityPaymentReportId:
            options.where = {
                supplier_id: iecPaymentId,
                created_at: { [Op.between]: [data.startDate, data.endDate] },
                business_id: businessId,
            };
            if (returnData) {
                attributes = [payAmount, contractNum, billNumber, clientName, clientddress, comment, externalId];
            }
            break;
        case electricityChargeReportId:
            options.where = {
                supplier_id: iecChargeId,
                created_at: { [Op.between]: [data.startDate, data.endDate] },
                business_id: businessId,
            };
            if (returnData) {
                attributes = [payAmount, contractNum, billNumber, action, ...clientDetails, confirmation];
            }
            break;
        case authoritiesReportId:
            options.where = {
                supplier_id: authoritiesId,
                created_at: { [Op.between]: [data.startDate, data.endDate] },
                business_id: businessId
            };
            if (data.phoneNumber) {
                options.where.phoneNumber = data.phoneNumber
            }
            if (returnData) {
                attributes = [payAmount, contractNum, cityName, cityPayType, clientName, confirmation];
            }
            break;
        case bezeqReportId:
            options.attributes = [];
            options.where = {
                created_at: { [Op.between]: [data.startDate, data.endDate] },
                supplier_id: bezekId,
                business_id: businessId,
            };
            if (returnData) {
                attributes = [priceAsAmount, clientName, customerId, ...bezeqDetails];
            }
            break;
        case allCardsReportId:
            options.where = createWhereData(data, businessId);
            options.group = ["product_id", businessName,
                'business_identifier', 'supplier_name', action, 'product_name',
                price, businessPrice, ...transactionStatusFields];
            options.order = options.group;
            const sumCharge = [sequelize.fn('count', sequelize.col(businessName)), 'sumCharges'];
            phoneNumberColFlag = false;
            dateColFlag = false;
            timeColFlag = false;
            idColFlag = false;
            if (countOnly) {
                options.attributes = [sumCharge, ...prices]
            } else {
                if (returnData) {
                    attributes = [...supplierAndProd, [action, 'cardType'], sumCharge, ...prices]
                }
            }
            break;

        case managerTransactionsReportId:
            options.where = managerCreateWhereData(data);
            if (returnData) attributes = ['ip', 'supplier_balance', 'distributor_balance', 'business_balance', 'peletok_price', 'distributor_price',
                'business_price', consumerPrice, phoneNumber, 'status', ...supplierAndProd, timeCol, dateColFromCreated,
                city, distributorName, 'distributor_id', busId, businessName]
            break;
        case paymentsReportId:
            paymentsFlag = true;
            phoneNumberColFlag = false;
            dateColFlag = false;
            timeColFlag = false;
            baseAttributesFlag = false;
            //TODO - add agent attribute
            options.where = { business_id: businessId, created_at: { [Op.between]: [data.startDate, data.endDate] } };
            if (returnData) {
                attributes = [dateColFromDate, hourAsTime, sumAsPay, invoiceNum];
                options.include = {
                    model: models.business.model,
                    as: 'Business',
                    attributes: [['name', businessName]]
                }
            }
            options.raw = true;
            defaultModel = businessPaymentModel.model;
            break;

        case managerPaymentsReportId:
            return await getManagerPaymentReports(req, res)
        case managerActionsReportId:

            options.where = managerCreateWhereData(data);
            if (returnData) {
                attributes = [action, phoneNumber, consumerPrice, 'business_price', 'distributor_price', 'peletok_price', 'business_balance', 'distributor_balance', 'ip', 'business_points', [Sequelize.fn('ARRAY_TO_STRING', sequelize.fn('ARRAY_AGG', sequelize.col('business_tags_id')), ','), 'business_tags_id']];
                options.group = ['transaction_id', action, phoneNumber, 'price', 'business_price', 'distributor_price', 'peletok_price', 'business_balance', 'distributor_balance', 'ip', 'business_points', 'business_name', 'business_identifier', 'transaction_type', 'is_cancelable', 'cancel_process_status', 'is_canceled', 'created_at', 'updated_at', 'id']
            }
            options.where.status = getStatus('succeeded', 'Transaction')

            break;

        case managerManualCardsReportId:
            options.where = {
                product_type: getType('manual', 'product'),
                created_at: { [Op.between]: [data.startDate, data.endDate] },
            };

            if (data.supplier_id) {
                options.where.supplier_id = data.supplier_id;
            }
            if (data.business_identifier) {
                options.where.business_identifier = data.business_identifier;
            }
            if (data.agent_id) {
                options.where.agent_id = data.agent_id;
            }
            if (data.business_status) {
                let businessStatusArray = data.business_status.split(',')
                options.where.business_status = businessStatusArray;
            }
            if (returnData) {
                phoneNumberColFlag = false;
                attributes = [...supplierAndProd, ...cardDetails, 'business_status', 'agent_id']
            }
            break;

        case managerBalanceByDateReportId:
            if (countOnly) { // error
                return { reports: null, paymentsFlag: null };
            }
            options.where = createWhereDataForBalanceByDate(data);
            paymentsFlag = true;
            phoneNumberColFlag = false;
            dateColFlag = false;
            timeColFlag = false;
            baseAttributesFlag = false;
            idColFlag = false;
            options.order = ['id'];
            attributes = [['name', businessName], busId, 'distributor_id', 'payment_method', "id"];
            options.include = [{
                model: businessFinanceLogModel.model,
                as: "BusinessFinanceLog",
                attributes: ["balance", "business_id"],
                where: { timestamp_start: { [Op.lte]: data.date }, timestamp_end: { [Op.or]: [{ [Op.gt]: data.date }, null] } }
            }, {
                model: models.collectionDay.model,
                as: 'CollectionDay',
                attributes: DAYS,
            }, {
                model: models.businessPointLog.model,
                as: 'BusinessPointLog',
                attributes: [["sum", "points"]],
                where: { timestamp_start: { [Op.lte]: data.date }, timestamp_end: { [Op.or]: [{ [Op.gt]: data.date }, null] } }
            }];
            options.raw = true;
            defaultModel = business.model;
            break;
        default:
            return { reports: null, paymentsFlag: null };

    }
    if (data['transactionId'] && options.where) {
        options.where['id'] = data['transactionId']
    }
    if (returnData) {
        options.attributes = [];
        if (baseAttributesFlag) {
            options.attributes = [...baseAttributes];
        }
        options.attributes = options.attributes.concat(attributes);
        if (phoneNumberColFlag) {
            options.attributes.push(phoneNumber)
        }
        if (dateColFlag) {
            options.attributes.push(dateColFromCreated)
        }
        if (timeColFlag) {
            options.attributes.push(timeCol)
        }
        if (idColFlag) {
            options.attributes.push(["id", "transactionId"]);
            options.order = ['created_at', 'id'];
        }
        if (data.limit && (data.limit !== 'NaN')) {
            options.limit = data.limit
        }
        if (data.offset && (data.offset !== 'NaN')) {
            options.offset = data.offset
        }
    }
    if ([managerPaymentsReportId, managerTransactionsReportId, managerBalanceByDateReportId, paymentsReportId].indexOf(reportId) === -1) {
        options.where["status"] = succeedStatus;
    }
    if (countOnly && (reportId !== allCardsReportId) && reportId !== managerBalanceByDateReportId) {
        options.attributes = [[sequelize.fn('count', sequelize.col('id')), 'length']];
        if (paymentsFlag) {
            options.attributes.push([sequelize.fn('sum', sequelize.col('sum')), 'sum_payment'])
        } else {
            options.attributes.push([sequelize.fn('sum', sequelize.col(businessPrice)), businessPrice],
                [sequelize.fn('sum', sequelize.col(price)), 'consumer_price'])
        }
    }
    const reports = await defaultModel.findAll(options);
    return { reports: reports, paymentsFlag: paymentsFlag };
}

function fixBalanceReport(reports) {
    reports.map(r => {
        let collectionDayKey = "CollectionDay";
        delete r['BusinessFinanceLog.business_id'];
        delete r[collectionDayKey + '.id'];
        r.collectionDay = '';
        for (d of DAYS) {
            let dayKey = collectionDayKey + '.' + d;
            if (r[dayKey]) {
                r.collectionDay += ',' + ALPHA_BET_DAYS[d];
            }
            delete r[dayKey];

        }
        if (r.collectionDay[0] === ',') {
            r.collectionDay = r.collectionDay.substring(1);
        }
    })
}

/**
 * send incoming reports to client with relevant data
 * @param {*} req
 * @param {*} res
 */
async function reportSender(req, res) {
    let reports, paymentsFlag, priceAndPayment, sumProfit;
    const data = setDateFilter(req.query);
    let reportId = req.params.reportId;
    if (reportId == paymentsAndActionsReportId) {
        reports = await getPaymentAndActionReport(data, req.user.business_id).catch(e => {
            logger.error(e);
        });
        reports = reports ? reports : [];
        paymentsFlag = true;

    } else if (reportId == managerPaymentsAndActionReportId) {
        reports = await getManagerPaymentAndActionReport(data).catch(e => {
            logger.error(e);
        });
        reports = reports ? reports : [];
        paymentsFlag = true;

    } else if (reportId == managerObligoReportId) {
        reports = await getObligoReports(data).catch(e => {
            logger.error(e);
        });
        reports = reports ? reports : [];
        paymentsFlag = true;

    } else if (reportId == managerActionsDistributionReportId) {

        reports = await getManagerActionsDistributionReports(data).catch(e => {
            logger.error(e);
        });
        reports = reports ? reports : [];
        paymentsFlag = true;


    }
    else if (reportId == managerInvoiceMailReportReportId) {

        reports = await managerGetInvoiceMailReport(data).catch(e => {
            logger.error(e);
        });
        reports = reports ? reports : [];
        paymentsFlag = true;


    } else if (reportId == managerAllCardsReportId) {

        reports = await managerGatAllCardsReport(data).catch(e => {
            logger.error(e);
        });
        reports = reports ? reports : [];
        paymentsFlag = true;

    }
    else {
        const dynamicReport = await dynamicReportsGetter(req.user.business_id, reportId, data, false, req, res);
        reports = dynamicReport.reports;
        paymentsFlag = dynamicReport.paymentsFlag;
    }
    if (reportId === allCardsReportId) {
        priceAndPayment = sumGroupTransactions(reports);
        sumProfit = priceAndPayment['consumer_price'] - priceAndPayment['business_price']
    }
    if (reports) {
        if (!reports[0]) {
            res.send('No reports found')
        } else {
            if (reportId === managerBalanceByDateReportId) {
                fixBalanceReport(reports);
            }
            let headers = null;
            if (paymentsFlag) {
                headers = Object.keys(reports[0]);
            } else {
                headers = Object.keys(reports[0].dataValues);
            }
            headers = headers.filter(h => h !== "id" && h !== "is_cancelable" && h !== "cancel_process_status" && h !== "is_canceled" && h !== "transaction_type" && h !== "is_printable");
            let toSend = {
                headers: headers,
                data: reports,
                sum: { ...priceAndPayment }
            };
            if (sumProfit) {
                toSend.sum_profit = { sum_profit: sumProfit }

            }
            res.send(toSend)
        }
    } else {
        res.status(400).send('failed')
    }

}

async function lengthOfBalanceReport(data) {
    return await models.business.model.count({ where: createWhereDataForBalanceByDate(data) });
}

// get reports list with data
router.get('/', async function (req, res) {
    let info = new Info();
    getReports(req).then((r) =>
        info.result = r).catch(e => {
            info.errors.push(e)
        }).then(() => {
            res.customSend(info)
        });
}
);

// get report results
router.get('/result:reportId', function (req, res) {
    var path = `report${req.params.reportId}`;

    res.send(results[path])
}
);



router.get('/report_data/:reportId', reportSender);

/**
 * Returns length of report
 * @param req req object
 * @param res res object
 * @returns {Promise<void>}
 */
async function getReportLength(req, res) {
    let info = new Info();
    const reportId = req.params.reportId;
    const data = setDateFilter(req.query);
    if (reportId === managerBalanceByDateReportId) {
        const length = await lengthOfBalanceReport(data);
        info.result = { length: length };
    }

    else if (reportId === paymentsAndActionsReportId) {
        await dynamicReportsGetter(req.user.business_id, paymentsReportId, data, true, req, res).then(async r => {
            info.result = {};
            info.result.length = Number(r.reports[0].length);
            info.result.currentBalance = (await businessFinanceModel.findOne({ where: { business_id: req.user.business_id } })).balance;
        }).catch(e => {
            info.errors.push(e)
        });
        await dynamicReportsGetter(req.user.business_id, actionsReportId, data, true, req, res).then(r => {
            info.result.length += Number(r.reports[0].dataValues.length);
            info.result.consumer_price = r.reports[0].dataValues.consumer_price
        }).catch(e => {
            info.errors.push(e)
        });
    } else {
        await dynamicReportsGetter(req.user.business_id, reportId, data, true, req, res).then(async r => {
            if (reportId === paymentsReportId) {
                info.result = r.reports[0];
                info.result.sum_payment = Number(r.reports[0].sum_payment)
            } else if (reportId === allCardsReportId) {
                info.result = { length: r.reports.length };
                info.result = Object.assign(info.result, sumGroupTransactions(r.reports));
                info.result.sum_profit = info.result.consumer_price - info.result.business_price;
            } else {
                info.result = r.reports[0].dataValues;
                info.result.sum_profit = info.result.consumer_price - info.result.business_price;
            }
        }).catch(e => {
            info.errors.push(e)
        });
    }
    res.customSend(info);

}

router.get('/report_length/:reportId', getReportLength);

hashavshevetRouter.get('/:file_name/', (req, res) => {
    res.download(path.join(hashavshevetDir, req.params.file_name))
});

async function getManagerPaymentReports(req, res) {
    const data = setDateFilter(req.query);
    let options = {}
    let includeWhere = {}
    let baseWhere = { created_at: { [Op.between]: [data.startDate, data.endDate] } }
    options.where = Object.assign(data.startDate && data.endDate ? baseWhere : {})
    if (data['business_id']) {
        includeWhere['business_id'] = data['business_id']
    }
    if (data['distributor_id']) {
        includeWhere['distributor_id'] = data['distributor_id']
    }
    if (data['status']) {
        includeWhere['status'] = data['status']
    }
    if (data['agent_id']) {
        includeWhere['agent_id'] = data['agent_id']
    }
    options.attributes = [[sequelize.fn("to_char",
        sequelize.col("date"), DATE_FORMAT), "date"], ['hour', "time"], ['sum', 'payment'], ['invoice_number', 'realInvoiceNumber'], 'action', 'author_name'];
    options.include = [
        {
            model: models.business.model,
            as: 'Business',
            attributes: ['name', 'distributor_id', 'agent_id', 'business_identifier'],
            where: includeWhere,
            required: false,
            include: [
                {
                    model: models.businessAddress.model,
                    as: 'BusinessAddress',
                    required: false,
                    attributes: [],
                    include: [
                        {
                            model: models.address.model,
                            as: 'Address',
                            attributes: ['city'],
                            required: false,
                        }
                    ]
                }, {
                    model: models.user.model,
                    as: 'Agent',
                    attributes: [[sequelize.fn("concat", sequelize.col("first_name"), ' ', sequelize.col("last_name")), 'agent_name']],

                },
                {
                    model: models.distributor.model,
                    as: 'Distributor',
                    attributes: [],
                    include: [
                        {
                            model: models.business.model,
                            as: 'Business',
                            attributes: [['name', 'distributor_name']]
                        }
                    ]
                }
            ]
        }

    ]
    options.raw = true


    let reports = await models.businessPayment.model.findAll(options)
    if (reports) {
        if (!reports[0]) {
            return res.send('No reports found')
        } else {
            let sum = 0
            let arrangedReports = []
            for (let index = 0; index < reports.length; index++) {
                const report = reports[index];
                sum += report.payment

                let cleanReport = cleanRawKeys(report, false)
                arrangedReports.push(cleanReport)

            }
            let headers = Object.keys(arrangedReports[0])


            let response = { headers: headers, data: arrangedReports, sum: sum }

            res.send(response)
        }
    } else {
        res.status(400).send('Failed')
    }
}


function getManagerPaymentAndActionReport(data) {
    let payment = LM.getString("payment");
    let replacements = { dateFormat: DATE_FORMAT, timeFormat: TIME_FORMAT, dataLimit: data.limit ? data.limit : null, dataOffset: data.offset ? data.offset : 0 }
    let businessWhere = ''
    let distributorWhere = ''
    let agentWhere = ''
    let businessStatusWhere = ''
    let phoneNumberWhere = ''
    let transactionWhere = ''
    let productTypeWhere = ''
    let businessTagsWhere = ''
    let transactionStatusWhere = ''
    let businessPaymentWhere = ''
    let openingClause = ''
    let closingClause = ''

    for (let key in data) {

        if (data[key] == '') {
            delete data[key]
        }
    }

    if (data.business_identifier) {
        replacements.business_identifier = data.business_identifier
        businessWhere = `AND "ReportView"."business_identifier" = :business_identifier`
        businessPaymentWhere = `"BusinessPayment"."business_identifier" = :business_identifier AND`
    }
    if (data.distributor_id) {
        replacements.distributor_id = data.distributor_id
        distributorWhere = `AND "ReportView"."distributor_id" = :distributor_id`
    }
    if (data.agent_id) {
        replacements.agent_id = data.agent_id
        agentWhere = `AND "ReportView"."agent_id" = :agent_id`
    }
    if (data.business_status) {
        let businessStatusArray = data.business_status.split(',')
        let sqlReadyBusinessStatus = businessStatusArray.map(function (a) { return "'" + a.replace("'", "''") + "'"; }).join();
        businessStatusWhere = `AND "ReportView"."business_status" IN (${sqlReadyBusinessStatus})`
    }
    if (data.phone_number) {
        replacements.phone_number = data.phone_number
        phoneNumberWhere = `AND "ReportView"."phone_number" = :phone_number`
    }
    if (data.transaction_id) {
        replacements.transaction_id = data.transaction_id
        transactionWhere = `AND "ReportView"."id" = :transaction_id`
    }
    if (data.product_type) {
        replacements.product_type = data.product_type
        productTypeWhere = `AND "ReportView"."product_type" = :product_type`
    }
    if (data.business_tags_id) {
        let businessTagsArray = data.business_tags_id.split(',')
        let sqlReadyTags = businessTagsArray.map(function (a) { return "'" + a.replace("'", "''") + "'"; }).join();
        businessTagsWhere = `AND "ReportView"."business_tags_id" IN (${sqlReadyTags})`
    }
    if (data.transaction_status) {
        let transactionStatusArray = data.transaction_status.split(',')
        let sqlReadyTransactionStatuses = transactionStatusArray.map(function (a) { return "'" + a.replace("'", "''") + "'"; }).join();
        transactionStatusWhere = `AND "ReportView"."status" IN (${sqlReadyTransactionStatuses})`
    }
    let query = `SELECT id, to_char("created_at", :dateFormat) AS "date",
       to_char("created_at", :timeFormat ) AS "time",
       "business_name",
       "business_identifier" AS "customerNumber",
       "distributor_name",
       "distributor_id",
       "city",
       "supplier_name"                     AS "supplier",
       "action",
       "product_name"                      AS "product",
       "phone_number",
       "price" as consumer_price,
       "business_price",
       "distributor_price",
       "business_points",
       "transaction_type",
       "is_cancelable",
       "cancel_process_status",
       "is_canceled",
       "distributor_balance",
       to_char("business_balance", 'FM999999999.00') as business_balance,
       true as is_printable
FROM "REP_ReportView" AS "ReportView"
WHERE (("ReportView"."deleted_at" > '${moment().toISOString()}' OR "ReportView"."deleted_at" IS NULL) AND (${getDateFilter(data, 'ReportView')}) ${openingClause}${businessWhere} ${transactionStatusWhere} ${distributorWhere} ${agentWhere}
 ${businessStatusWhere} ${phoneNumberWhere} ${transactionWhere} ${productTypeWhere} ${businessTagsWhere}${closingClause}) 
UNION
SELECT "BusinessPayment".id, to_char("date", :dateFormat) AS "date",
       "BusinessPayment"."hour"      AS "time",
       "Business"."name"             AS "Business.business_name",
       "Business"."business_identifier" AS "customerNumber",
       '',
       null,
       '',
       '',
       '${payment}',
       '',
       '',
       null,
       "BusinessPayment"."sum"       AS business_price,
       null,
       null,
       null,
       false,
       'NO',
       false,
       null,
       to_char("business_credit", 'FM999999999.00') as balance2,
       false as is_printable
       FROM "BNK_BusinessPayment" AS "BusinessPayment"
       LEFT OUTER JOIN "BSN_Business" AS "Business" ON "BusinessPayment"."business_id" = "Business"."id" AND
       ("Business"."deleted_at" > '${moment().toISOString()}' OR
       "Business"."deleted_at" IS NULL)
        WHERE (("BusinessPayment"."deleted_at" > '${moment().toISOString()}' OR "BusinessPayment"."deleted_at" IS NULL) AND
        (${businessPaymentWhere}
        ${getDateFilter(data, 'BusinessPayment')} ))
        order by date, time, id limit :dataLimit offset :dataOffset;`;
    return sequelizeInstance.query(query, {
        replacements: replacements,
        type: QueryTypes.SELECT
    });
}

async function getObligoReports(data) {
    let whereObj = {}
    if (data['business_identifier']) {
        whereObj['business_identifier'] = data['business_identifier']
    }
    let businesses = await models.business.model.findAll({
        attributes: ['business_identifier', 'name', 'payment_method',],
        include: [{
            model: models.businessFinance.model,
            as: 'BusinessFinance',
            attributes: ['frame', 'temp_frame', 'balance', [sequelize.literal('"BusinessFinance"."frame" + "BusinessFinance"."temp_frame" + "BusinessFinance"."balance"'
            ), 'credit_for_use']]
        }, {
            model: models.businessAddress.model,
            as: 'BusinessAddress',
            attributes: ['id'],
            include: [{
                model: models.address.model,
                as: "Address",
                attributes: ['city']
            }]
        }, {
            model: models.collectionDay.model,
            as: 'CollectionDay',
            attributes: [['sunday', 'א'], ['monday', 'ב'], ['tuesday', 'ג'], ['wednesday', 'ד'], ["thursday", 'ה'], ["friday", 'ו'], ["saturday", 'ז'], ['monthly', "חודשי"]]
        }],
        where: whereObj,
        raw: true
    })
    cleanBusinesses = []
    businesses.map((business) => {
        cleanBusiness = cleanRawKeys(business, false)
        cleanBusiness.collectionDays = []
        for (let key in cleanBusiness) {
            if (key == 'א' || key == 'ב' || key == 'ג' || key == 'ד' || key == 'ה' || key == 'ו' || key == 'ז' || key == 'חודשי') {
                if (cleanBusiness[key]) {
                    cleanBusiness.collectionDays.push(key)
                    delete cleanBusiness[key]
                } else {
                    if (key != 'collectionDays') {
                        delete cleanBusiness[key]
                    }
                }
            }
        }
        cleanBusinesses.push(cleanBusiness)
    })
    return cleanBusinesses
}
function getManagerActionsDistributionReports(data) {
    //TODO - add agent id, agent name, area
    let replacements = {}
    let businessWhere = ''
    let distributorWhere = ''
    let agentWhere = ''
    let businessStatusWhere = ''
    let queryOptions = {}

    for (let key in data) {

        if (data[key] == '') {
            delete data[key]
        }
    }

    if (data.business_identifier) {
        ('inside if (data.business_identifier');
        replacements.business_identifier = data.business_identifier
        businessWhere = `AND "REP_ReportView"."business_identifier" = :business_identifier`
    }
    if (data.distributor_id) {
        ('inside if (data.distributor_id');
        replacements.distributor_id = data.distributor_id
        distributorWhere = `AND "REP_ReportView"."distributor_id" = :distributor_id`
    }
    if (data.agent_id) {
        ('inside if (data.agent_id');
        replacements.agent_id = data.agent_id
        agentWhere = `AND "REP_ReportView"."agent_id" = :agent_id`
    }
    if (data.business_status) {
        ('inside if (data.business_status');
        let businessStatusArray = data.business_status.split(',')
        let sqlReadyBusinessStatus = businessStatusArray.map(function (a) { return "'" + a.replace("'", "''") + "'"; }).join();
        businessStatusWhere = `AND "REP_ReportView"."business_status" IN (${sqlReadyBusinessStatus})`
    }



    let query = `SELECT business_identifier, business_name, distributor_id, distributor_name, city,
     MAX( to_char( created_at,'DD/MM/YYYY HH24'||CHR(58)||'MI'||CHR(58)||'SS')) AS last_action,
     SUM(good) + sum(bad) + sum(charge) AS overall_sum,
     SUM(good) + sum(bad) AS payments_sum,
     MAX(case When product_type = 1
     THEN to_char( created_at,'DD/MM/YYYY HH24'||CHR(58)||'MI'||CHR(58)||'SS') Else '1000-10-10' End) AS last_virtual_charge,
     COUNT(good) AS count_all,
     SUM(case When good > 0 THEN 1 Else 0 End) AS count_good,
     SUM(case When bad > 0 THEN 1 Else 0 End) AS count_bad,
     SUM(case When good > 0 THEN 1 Else 0 End) + SUM(case When bad > 0 THEN 1 Else 0 End) AS payment_count,
     SUM(case When charge > 0 THEN 1 Else 0 End) AS count_charge,
     SUM(case When good > 0 THEN 1 Else 0 End) + SUM(case When bad > 0 THEN 1 Else 0 End) + SUM(case When charge > 0 THEN 1 Else 0 End) AS overall_count,
     SUM(good) AS sum_good, sum(bad) AS sum_bad, sum(charge) AS sum_charge
     FROM (SELECT product_type, created_at, business_identifier, business_name, distributor_id, distributor_name, city,
     CASE WHEN supplier_id IN ${goodSuppliersIds} THEN price else 0 end AS good,
     CASE WHEN supplier_type = ${paymentsSuppliersType} and supplier_id not IN ${goodSuppliersIds} THEN price else 0 end AS bad,
     CASE WHEN supplier_type IN ${prepaidSuppliersTypes} THEN price else 0 end AS charge 
     FROM "REP_ReportView" WHERE ("REP_ReportView"."deleted_at" > '${moment().toISOString()}'
     OR "REP_ReportView"."deleted_at" IS NULL) AND (${getDateFilter(data, 'REP_ReportView')})
     AND status = ${succeedStatus} ${businessWhere} ${distributorWhere} ${agentWhere} ${businessStatusWhere} and price > 0) AS baseTable 
     GROUP BY business_identifier, business_name, distributor_id, distributor_name, city ;`

    return sequelizeInstance.query(query, {
        replacements: replacements,
        type: QueryTypes.SELECT
    });

}


async function managerGetInvoiceMailReport(data) {

    let arrangedResult = []

    let result = await models.invoice.model.findAll({
        where: {
            customer_number: data.business_identifier
        },
        attributes: [[sequelize.fn('to_char', sequelize.col('created_at'), 'DD/MM/YYYY HH24:MI:SS'), 'date'], 'path']
    })
    let currentBusiness = await models.business.model.findOne({
        where: {
            business_identifier: data.business_identifier
        },
        attributes: ['business_identifier', 'name', 'invoice_email']
    })

    for (let i = 0; i < result.length; i++) {
        const invoiceRow = result[i];
        let arrangedResultRow = {
            business_name: currentBusiness.name,
            business_identifier: currentBusiness.business_identifier,
            mail: currentBusiness.invoice_email,
            date: invoiceRow.dataValues.date,
            file_name: invoiceRow.path.substr(invoiceRow.path.lastIndexOf('/') + 1, invoiceRow.path.indexOf(-1))
        }
        arrangedResult.push(arrangedResultRow)
    }
    return arrangedResult
}
async function managerGatAllCardsReport(data) {
    let replacements = {}
    let businessWhere = ''
    let distributorWhere = ''
    let agentWhere = ''
    let businessStatusWhere = ''
    for (let key in data) {

        if (data[key] == '') {
            delete data[key]
        }
    }

    if (data.business_identifier) {
        ('inside if (data.business_identifier');
        replacements.business_identifier = data.business_identifier
        businessWhere = `AND "REP_ReportView"."business_identifier" = :business_identifier`
    }
    if (data.distributor_id) {
        ('inside if (data.distributor_id');
        replacements.distributor_id = data.distributor_id
        distributorWhere = `AND "REP_ReportView"."distributor_id" = :distributor_id`
    }
    if (data.agent_id) {
        ('inside if (data.agent_id');
        replacements.agent_id = data.agent_id
        agentWhere = `AND "REP_ReportView"."agent_id" = :agent_id`
    }
    if (data.business_status) {
        ('inside if (data.business_status');
        let businessStatusArray = data.business_status.split(',')
        let sqlReadyBusinessStatus = businessStatusArray.map(function (a) { return "'" + a.replace("'", "''") + "'"; }).join();
        businessStatusWhere = `AND "REP_ReportView"."business_status" IN (${sqlReadyBusinessStatus})`
    }
    let query = `SELECT business_id,business_identifier, product_id ,product_name, supplier_name, price, business_price, transaction_type , COUNT(*), price AS price, business_price AS business_price FROM "REP_ReportView"
    WHERE ("REP_ReportView"."deleted_at" > '${moment().toISOString()}'
    OR "REP_ReportView"."deleted_at" IS NULL) AND (${getDateFilter(data, 'REP_ReportView')}) AND product_type IN ${virtualAndManualProductTypes} ${businessWhere} ${distributorWhere} ${agentWhere} ${businessStatusWhere}  AND status = ${succeedStatus}
    GROUP BY business_id,business_identifier, product_id ,product_name, supplier_name, price, business_price, transaction_type ORDER BY business_id`


    let result = await sequelizeInstance.query(query, {
        replacements: replacements,
        type: QueryTypes.SELECT
    });

    console.log('result:', result);


    let prevBusinessId = null
    let newResults = []
    let overallProfitSum = 0
    let overallProductCountSum = 0
    for (i = 0; i < result.length; i++) {
        let currentResult = result[i];
        let productCountSum = currentResult.price * currentResult.count
        let profitSum = currentResult.business_price * currentResult.count
        if (!prevBusinessId) {
            ;
            prevBusinessId = currentResult.business_id
        }
        if (prevBusinessId == currentResult.business_id) {
            prevBusinessId = currentResult.business_id
            newResults.push(currentResult)
            overallProductCountSum += productCountSum
            overallProfitSum += profitSum
            if (i == result.length - 1) {
                newResults.push({ count: LM.getString('allReportsOverallSum'), price: overallProductCountSum })
                newResults.push({ count: LM.getString('allReportsProfitSum'), price: parseFloat(overallProductCountSum - overallProfitSum) })
            }

        } else {
            prevBusinessId = currentResult.business_id
            newResults.push({ count: LM.getString('allReportsOverallSum'), price: overallProductCountSum })
            newResults.push({ count: LM.getString('allReportsProfitSum'), price: parseFloat(overallProductCountSum - overallProfitSum) })

            newResults.push(currentResult)
            overallProductCountSum = productCountSum
            overallProfitSum = profitSum

        }
    }



    return newResults
}

module.exports.reportsRouter = router;
module.exports.hashavshevetRouter = hashavshevetRouter;
module.exports.getReportLength = getReportLength;
module.exports.reportSender = reportSender;