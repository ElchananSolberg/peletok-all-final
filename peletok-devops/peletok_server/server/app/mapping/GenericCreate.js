const GenericDBAction = require('./GenericDBAction');
const {Op} = require('sequelize');

const models = require('../index');
const logger = require("../logger");
const {isEmptyArr} = require("../utils");

const {table, path, mapPathQuery} = require("./map_tables");
const {isObjectFullyEmpty, capitalize} = require("../utils");

class GenericCreate extends GenericDBAction {
    /***
     * Constructor
     * @param actionPath path of action (by mapping tables)
     * @param objToCreate object with key val to create in DB
     */
    constructor(actionPath, objToCreate) {
        super(actionPath);
        this.baseWhere["creation"] = true;
        this.objToCreate = objToCreate;
        this.mapFields = {};
        this.newValByTable = {};
        this.attrKeyVal = {};
        this.include = {};
    }

    /**
     * Maps fields by tables
     * @param fields fields for select query from DB
     * @return {Promise<void>}
     */
    async map(fields) {
        Object.values(fields).forEach(v => {
            let tableName = v["MapPathQuery.Table.table_name"];
            this.mapFields[v.key_name] = {
                table: tableName,
                field: v.field_name
            };
            if (v["WhereTable.key_filter"]) {
                this.mapFields[v.key_name]["filter"] = {
                    key: [v["WhereTable.key_filter"]],
                    val: v["WhereTable.val_filter"]
                };
            }
            this.newValByTable[tableName] = [];
        });
    }

    //TODO: Add documentation
    /***
     * Saves new values by tables and fields
     * @returns {Promise<void>}
     */
    async mapNewVal() {
        Object.keys(this.objToCreate).forEach(
            k => {
                if (this.objToCreate[k] === null || this.objToCreate[k] === undefined || this.objToCreate[k] === "") {
                    return;
                }
                if (this.mapFields[k]) {
                    let tableToStore = this.mapFields[k]["table"];
                    let fieldToStore = this.mapFields[k]["field"];
                    if (this.mapFields[k]["filter"]) {
                        const mapKey = this.mapFields[k]["filter"]["key"];
                        const mapVal = this.mapFields[k]["filter"]["val"];
                        if (isEmptyArr(this.newValByTable[tableToStore])) {
                            this.newValByTable[tableToStore].push({});
                        }
                        for (const i of this.newValByTable[tableToStore]) {
                            if (i[mapKey] === mapVal) {
                                i[fieldToStore] = this.objToCreate[k];
                                return;
                            }
                            if (!i[mapKey]) {
                                i[fieldToStore] = this.objToCreate[k];
                                i[mapKey] = mapVal;
                                return;
                            }
                        }
                        let obj = {[mapKey]: mapVal, [fieldToStore]: this.objToCreate[k]};
                        this.newValByTable[tableToStore].push(obj);

                    } else {
                        if (this.objToCreate[k] instanceof Array) {
                            for (const fieldVal of this.objToCreate[k]) {
                                this.newValByTable[tableToStore].push({[fieldToStore]: fieldVal})
                            }
                        } else {
                            if (isEmptyArr(this.newValByTable[tableToStore])) {
                                this.newValByTable[tableToStore].push({});
                            }
                            this.newValByTable[tableToStore][0][fieldToStore] = this.objToCreate[k];
                        }
                    }
                } else {
                    logger.warn(`Warning: the '${k}' key is not valid key`);
                }
            });
    }

    /***
     * Builds objects for passing to the create method
     * @returns {Promise<void>}
     */
    async buildQuery() {
        // attr of base table
        this.attrKeyVal = Object.assign(this.attrKeyVal, this.newValByTable[this.asTagOfMainTable][0]);
        this.attrKeyVal["author_id"] = this.objToCreate["author_id"];
        //attr in other tables
        await this.buildIncludeAndAttr(this.startRow.id, this.include, this.attrKeyVal).catch(err => {
            //logging
            logger.error(JSON.stringify(this.include, null, 5));
            logger.error(JSON.stringify(this.attrKeyVal, null, 5));
        });
    }

    /***
     * Builds objects for passing to the create method (without attr of base table)
     * @param parentId id of parent map row
     * @param include object to save into it 'include'
     * @param attrKeyVal object to save into it attributes key and value to create
     * @returns {Promise<void>}
     */
    async buildIncludeAndAttr(parentId, include, attrKeyVal) {
        let findByParent = await mapPathQuery.findAll({
                where: {"parent_id": parentId},
                include:
                    [{model: table, as: "Table", attributes: ["table_name"]},
                        {model: path, as: "Path"}]
            }
        );
        for (let i = 0; i < findByParent.length; i++) {
            let tableName = findByParent[i]["Table"].table_name;
            let asTag = capitalize(tableName);
            let includeThis = {
                model: models[tableName.charAt(0).toLowerCase() + tableName.slice(1)].model,
                as: asTag
            };
            let thisAttrKeyVal = {[asTag]: {}};
            if (this.newValByTable[asTag]) {
                thisAttrKeyVal[asTag] = [...this.newValByTable[asTag]];
            }
            await this.buildIncludeAndAttr(
                findByParent[i].id, includeThis, thisAttrKeyVal[asTag][0] || thisAttrKeyVal[asTag]);

            if (!isObjectFullyEmpty(thisAttrKeyVal[asTag])) {
                if (!include["include"]) {
                    include["include"] = [];
                }
                include["include"].push(includeThis);
                if (!attrKeyVal) attrKeyVal = {};
                if (this.objToCreate["author_id"]) {
                    Object.values(thisAttrKeyVal[asTag]).forEach(i => {
                        i["author_id"] = this.objToCreate["author_id"];
                    });
                }
                attrKeyVal[asTag] = thisAttrKeyVal[asTag];
            }
        }
    }


    /***
     * Create new rows (with dependencies) in DB
     * @return {Promise<created object>}
     */
    async create() {
        await this.config();
        const allFields = await GenericDBAction.getFieldsToInclude(this.baseWhere);
        await this.map(allFields);
        await this.mapNewVal();
        await this.buildQuery();
        // console.log(JSON.stringify(this.attrKeyVal));
        return await models[this.lowerCaseNameOfMainTable].model.create(this.attrKeyVal, this.include);
    }


}

module.exports = GenericCreate;
