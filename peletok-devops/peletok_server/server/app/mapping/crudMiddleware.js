const GenericCreate = require("./GenericCreate");
const { GenericGet } = require("./GenericGet");
const { removeAbsolutePath } = require('../utils');
const logger = require("../logger")
const debugging = process.env["debug"];
const { GenericUpdate } = require('./GenericUpdate')

/***
 * Information about errors that occurred in the process
 */
class ErrorInfo {
    constructor(type, field, message) {
        this.type = type;
        this.field = field;
        this.message = message;
    }
}


/***
 * Information about process (include errors if having)
 */
class Info {
    constructor() {
        this.errors = [];
        this.message = "";
        this.result = null;
    }
}

/***
 * Returns of function that saves that errors into info object
 * @param info info object
 * @returns {Function}
 */
function saveErrors(info) {
    return errors => {
        logger.error(errors);
        try {
            for (const e of errors.errors) {
                info.errors.push(new ErrorInfo(e.type, e.path, e.message));
            }
        } catch (unKnownError) {
            const errorInfo = new ErrorInfo("unKnownError", "unKnown");
            if (debugging && debugging !== "false") {
                errorInfo.message = errors;
            }
            info.errors.push(errorInfo)
        }
    };
}

/***
 * Returns of function that saves that result into info object
 * @param info info object
 * @returns {Function}
 */
function saveResult(info) {
    return result => {
        Object.values(result).forEach(r => removeAbsolutePath(r, 0));
        info.result = result;

    };
}


/**
 * Returns mapping path by url request
 * @param req request object
 * @returns string of path
 */
function getMappingPath(req) {
    let baseUrl = req.originalUrl;
    
    if (baseUrl.lastIndexOf("?") !== -1) {
        baseUrl = baseUrl.slice(0, baseUrl.lastIndexOf("?"));
    }
    if (baseUrl[baseUrl.length - 1] === "/") {
        baseUrl = baseUrl.slice(0, baseUrl.length - 1);
    }
    let lastIndexOf = baseUrl.lastIndexOf("/");
    let paramsExist = Object.entries(req.params).length > 0;
    if (paramsExist) {
        baseUrl = baseUrl.slice(0, lastIndexOf);
        lastIndexOf = baseUrl.lastIndexOf("/");
    }
    return baseUrl.slice(lastIndexOf + 1);
}


/**
 * Creates new rows in DB
 * @param path path for mapping data to tables
 * @param data data to save in DB
 * @returns {Promise<Info>}
 */
async function create(path, data) {
    let info = new Info();
    await new GenericCreate(path, data).create().then(r => {
        info.message = "success";
    }).catch(saveErrors(info)
    );
    return info;
}

async function update(path, where, data) {
    let info = new Info();
    await new GenericUpdate(path, where, data).update().then(r => {
        info.message = "success";
    }).catch(saveErrors(info)
    );
    return info;
}

/**
 * Calls to create function and sends result
 * @param req request object
 * @param res response object
 * @param next {Object} next
 * @param data data to save in DB
 * @returns {Promise<void>}
 */
async function wrapperCreate(req, res, next = null, data = null) {
    let mappingPath = getMappingPath(req);
    data = data || req.body;
    data.author_id = req.user ? req.user.id : null;
    let info = await create(mappingPath, data);
    res.customSend(info);
}


/**
 * Returns information from DB
 * @param path mapping path
 * @param filter filter object for DB query
 * @param raw boolean flag for passing to sequelize query
 * @return {Promise<Info>}
 */
async function get(path, filter, raw) {
    let info = new Info();
    const getManager = await new GenericGet(path, filter);
    if (raw === true || raw === false) {
        getManager.include.raw = raw;
    }
    await getManager.get().then(saveResult(info)).catch(saveErrors(info))
    return info;
}


/**
 * Wraps the get method
 * @param req request object
 * @param res response object
 * @param next {Object} next
 * @param raw boolean flag for passing to sequelize query
 * @param filter {Object} filter DB
 * @return {Promise<void>}
 */
async function wrapperGet(req, res, next = null, raw = null, filter = null) {
    let mappingPath = getMappingPath(req);
    filter = filter || (req.params.id ? { id: req.params.id } : (req.query || {}));
    let info = await get(mappingPath, filter, raw);
    res.customSend(info);
}

async function wrapperUpdate(req, res, next = null, data = null, mappingPath = null) {
    mappingPath = mappingPath || getMappingPath(req);
    data = data || req.body;
    data.author_id = req.user ? req.user.id : null;
    const filter = req.params.id ? { id: req.params.id } : {};
    let info = await update(mappingPath, filter, data);
    res.customSend(info);
}

async function wrapperGetUser(req, res, next = null, raw = null, filter = null) {
    let mappingPath = getMappingPath(req);
    filter = filter || (req.params.id ? { id: req.params.id } : (req.query || {}));
    let info = await get(mappingPath, filter, raw);
    return info
}
async function wrapperCreateBusiness(path, data) {
    let info = new Info();
    await new GenericCreate(path, data).create().then(r => {
        info.message = { id: r.id, message: "success" };
    }).catch(saveErrors(info)
    );
    return info;
}

async function wrapperCreateUser(path, data) {
    let info = new Info();
    await new GenericCreate(path, data).create().then(r => {
        info.message = { id: r.id, message: "success" };
    }).catch(saveErrors(info)
    );
    return info;
}
async function wrapperCreateDistributor(path, data) {
    let info = new Info();
    await new GenericCreate(path, data).create().then(r => {
        info.message = { id: r.id, message: "success" };
    }).catch(saveErrors(info)
    );
    return info;
}




module.exports.wrapperCreate = wrapperCreate;
module.exports.create = create;
module.exports.wrapperGet = wrapperGet;
module.exports.get = get;
module.exports.getMappingPath = getMappingPath;
module.exports.update = update;
module.exports.wrapperUpdate = wrapperUpdate;
module.exports.Info = Info;
module.exports.wrapperGetUser = wrapperGetUser;
module.exports.wrapperCreateBusiness = wrapperCreateBusiness;
module.exports.wrapperCreateDistributor = wrapperCreateDistributor;
module.exports.wrapperCreateUser = wrapperCreateUser;
