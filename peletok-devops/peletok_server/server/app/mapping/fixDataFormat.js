const logger = require('../logger');

/**
 * Filters data by mapping file (for column filter such as phone number)
 * @param data data to filter
 * @param fieldsByTable all fields to getting from DB
 */
function filterResult(data, fieldsByTable) {
    const {filterSuffix, sortFieldSuffix} = require("./GenericGet");
    const {removeAbsolutePath} = require('../utils');
    Object.values(data).forEach(r => removeAbsolutePath(r, 1));
    let keyFilterInResult;
    Object.values(data).forEach(r => {
        Object.keys(fieldsByTable).forEach(k => {
            Object.values(fieldsByTable[k]).forEach(f => {
                let keyInResult = k + '.' + f.asName;
                if (f.filter) {
                    Object.keys(f.filter).forEach(fk => {
                        keyFilterInResult = `${k}.${fk}${filterSuffix}`;
                        // noinspection EqualityComparisonWithCoercionJS
                        if (r[keyFilterInResult] != f.filter[fk]) {
                            // logger.debug(
                            //     `keyFilterInResult: ${keyFilterInResult}
                            //              result[${keyFilterInResult}]:  ${r[keyFilterInResult]}
                            //              filterKey: ${fk}
                            //              filter_val: ${f.filter[fk]}
                            //              keyInResult: ${keyInResult}
                            //              r[${keyInResult}]: ${r[keyInResult]}`);
                            r[keyInResult] = null;
                        }
                    });
                }
                if (f.sort_by_field) {
                    let sortedKey = k + '.' + f.sort_by_field + sortFieldSuffix;
                    r[keyInResult + sortFieldSuffix] = {[r[sortedKey]]: r[keyInResult]};
                    delete r[keyInResult];
                    delete r[sortedKey];
                }
            })
        });
        delete r[keyFilterInResult];
    });
}


/***
 * Merges duplicated data to one object
 * @param data data to merging
 * @param idName name of id column
 * @returns merged object
 */
function mergeDuplicate(data, idName) {
    const {mergeObjects} = require("../utils");
    Object.keys(data).forEach(k1 => {
        if (!(data[k1])) return;
        if (!(data[k1][idName])) return;
        Object.keys(data).forEach(k2 => {
            if (k1 === k2) return;
            if (!(data[k2])) return;
            if (!(data[k1])) return;
            if (data[k1][idName] === data[k2][idName]) {
                data[k1] = mergeObjects(data[k1], data[k2]);
                data[k2] = null;
            }
        })
    });
    return data.filter(k => k !== null);
}

/**
 * merges multi objects,
 * for example:
 * arrayObjects: [{a: 1, b: 2}, {a: 2, b: null}]
 * result: {a: [1, 2], b: 2}
 * @param arrayObjects
 * @return Object which contains all date for any key
 */
function mergeObjects(arrayObjects) {
    const {sortFieldSuffix} = require("./GenericGet");
    let keys = [], sortedKeys = [];
    for (const k of Object.keys(arrayObjects[0])) {
        if (k.indexOf(sortFieldSuffix) !== -1) {
            let keyWithoutSuffix = k.slice(0, k.indexOf(sortFieldSuffix));
            sortedKeys.push(keyWithoutSuffix)
        } else {
            keys.push(k)
        }
    }
    let newObj = {};
    for (const k of keys) {
        newObj[k] = [];
        for (const o of arrayObjects) {
            if (o[k]!==undefined&&o[k]!==null)
                newObj[k].push(o[k])
        }
        if (newObj[k][0] && newObj[k][0] instanceof Date) {
            newObj[k] = [...new Set(newObj[k].map(x => x.toJSON()))];
        } else {
            newObj[k] = [...new Set(newObj[k])];
        }
        if (newObj[k].length === 1) {
            newObj[k] = newObj[k][0];
        }
        if (newObj[k].length === 0) {
            newObj[k] = null;
        }
    }
    for (const k of sortedKeys) {
        newObj[k] = {};
        for (const o of arrayObjects) {
            let keyWithSuffix = k + sortFieldSuffix;
            if (o[keyWithSuffix]) {
                let entries = Object.entries(o[keyWithSuffix])[0];
                if (entries[0] && entries[1]) {
                    if (newObj[k][entries[0]]) {
                        if (newObj[k][entries[0]] !== entries[1]) {
                            if (!(newObj[k][entries[0]] instanceof Array)) {
                                newObj[k][entries[0]] = [newObj[k][entries[0]]];
                            }
                            newObj[k][entries[0]].push(entries[1]);
                            newObj[k][entries[0]] = [...new Set(newObj[k][entries[0]])]
                        }
                    } else {
                        newObj[k][entries[0]] = entries[1];
                    }
                }
            }
        }
    }
    return newObj;

}

/***
 *
 /**
 * Divides an array of objects into different groups by value of an idName field
 * and merges the objects in each group
 * for example:
 * arrayObjects: [{a: 1, b: 2, id: 1}, {a: 2, b: null, id: 4}, {a: 3, b: null, id: 1}]
 * idNum: id
 * result: [{a: [1, 3], b: 2}, {a: 2, b: null}]
 * @param arrayObjects
 * @param idName
 * @return Object which contains all date as new format
 */
function mergeObjectsById(arrayObjects, idName) {
    let result = arrayObjects.reduce(function (map, obj) {
        if (map[obj[idName] + "_id"] === undefined) map[obj[idName] + "_id"] = [];
        map[obj[idName] + "_id"].push(obj);
        return map;
    }, {});
    for (const k of Object.keys(result)) {
        result[k] = mergeObjects(result[k])
    }
    return Object.values(result);
}

module.exports.filterResult = filterResult;
module.exports.mergeDuplicate = mergeObjectsById;