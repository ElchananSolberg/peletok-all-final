const GenericCreate = require("./GenericCreate");
const {GenericGet} = require("./GenericGet");
const {demo} = require("./demoData");
const objectsDemo = require('../rest/restData/local/localDemoData');
const models = require('../index');
const categoryModel = models["category"].model;
const categoryNameModel = models["categoryName"].model;

const favorite = "favorite";
const recommended = "recommended";
const internet = "internet";
const typeOfImage = 1;
const tempAuthorId = 2;
const awaitingForCancellationType = 3;
const moment = require('moment')

const logger = require('../logger');

/***
 * Script for creates and gets demo data
 */

/***
 * Creates demo data
 * @param pathRequest path of request
 * @returns {Promise<void>}
 */
async function createDemo(pathRequest) {
    let errors = [];
    for (let i = 0; i < 10; i++) {
        let objKeyVal = {};
        for (const k of Object.keys(demo[pathRequest])) {
            let length = demo[pathRequest][k].length;
            if (i < length)
                objKeyVal[k] = demo[pathRequest][k][i];
        }
        await new GenericCreate(pathRequest, objKeyVal).create().catch(err => {
            if (err) {
                logger.error(err);
                logger.error("objKeyVal: " + JSON.stringify(objKeyVal, null, 5));
                errors.push(err);
            }
        });
    }
    logger.info(errors.length ? `${errors.length} Errors found` : 'not found errors')
}

/***
 * Results demo data
 * @param pathRequest
 * @returns {Promise<void>}
 */
async function getDemo(pathRequest) {
    let r = await new GenericGet(pathRequest).get();
    logger.info("all:");
    logger.info(r);
    r = await new GenericGet(pathRequest, {id: r[0].id}).get();
    logger.info("one:");
    logger.info(r);
}

/**
 * Creates demo data from list of object
 * @param pathRequest path of request
 * @param obj key value for create in DB
 * @returns {Promise<void>}
 */
async function createDemoFromObjects(pathRequest, obj) {
    try{
        var errors = [];
        for (const item of Object.values(obj)) {
            item.author_id = tempAuthorId;
            await new GenericCreate(pathRequest, item).create().catch(err => {
                if (err) {
                    logger.error(err);
                    errors.push(err);
                }
            });
        }
        logger.info(errors.length ? `${errors.length} Errors found` : 'not found errors');
    }catch(e){
        console.log(e)
    }
}


//TODO: remove
/***
 * Inserts Category of products
 * @param productArr array of products
 * @return {Promise<void>}
 */
async function addProductCategory(productArr) {
    const productCategoryModel = models["productCategory"].model;

    const favoriteId = (await categoryNameModel.findOne({where: {name: favorite}})).category_id;
    const recommendId = (await categoryNameModel.findOne({where: {name: recommended}})).category_id;
    const internetId = (await categoryNameModel.findOne({where: {name: internet}})).category_id;

    for (const prod of productArr) {
        if (prod[favorite]) {
            await productCategoryModel.create({category_id: favoriteId, product_id: prod["id"]});
        }
        if (prod[recommended]) {
            await productCategoryModel.create({category_id: recommendId, product_id: prod["id"]});
        }
        if (prod[internet]) {
            await productCategoryModel.create({category_id: internetId, product_id: prod["id"]});
        }
    }
}

/***
 * Returns supplier and product and product price from DB
 * @param models {Object} all sequelize models
 * @returns {Promise<*>}
 */
async function getSuppliers(models) {
    const supplierModel = models.supplier.model;
    const productModel = models.product.model;
    const productPriceModel = models.productPrice.model;
    return await supplierModel.findAll({
        "include": {
            model: productModel,
            as: "Product",
            include: {model: productPriceModel, as: "ProductPrice"}
        }
    });
}

/***
 * Returns users with details (business and terminal) from DB
 * @param models {Object} all sequelize models
 * @returns {Promise<*>}
 */
async function getUsers(models) {
    const userModel = models.user.model;
    const userBusinessModel = models.userBusiness.model;
    const businessModel = models.business.model;
    const branchModel = models.branch.model;
    const terminalModel = models.terminal.model;

    return await userModel.findAll({
        include: {
            model: userBusinessModel,
            as: "UserBusiness",
            required: true,
            include: {
                model: businessModel,
                as: "Business",
                include: {
                    model: branchModel,
                    as: "Branch",
                    include: {
                        model: terminalModel,
                        as: "Terminal"
                    }
                }
            }
        }
    });
}

/**
 * Creates objects of demo data (for transaction)
 * @param suppliers {Array} all supplier and products
 * @param statuses {Array} all possible statuses
 * @param users {Array} all users
 * @param dates {Array} possible dates
 * @returns {[]}
 */
function makeTransactions(suppliers, statuses, users, dates) {
    const items = [1, 8, 11, 88, 1021, 57, 98];
    const defualtItem = 110;
    const phoneNumbers = ["0527640424", "0502649384", "0527640426", "0502649484"];
    const defualtphoneNumber = "0503040251";
    const contractNumbers = ["1800394747", "026009002", "026009808", "0776009087"];
    const defaultContractNumber = "0776009044";
    let invoiceNumber = "101";
    const actions = {
        "193": {action: 'תשלום מקומי',
            item_id: true,
            invoice_number: true
        },
        "94": {
            action: 'הטענת כרטיס וירטואלי',
            item_id: true,
            contract_number: true
        },
        "95": {
            action: 'הטענת כרטיס וירטואלי',
            phone_number: true,
            item_id: true,
            contract_number: true
        },
        "196": {
            action: 'הטענת כרטיס וירטואלי',
            item_id: true,
            contract_number: true
        },
        "127": {
            action: 'הטענת כרטיס וירטואלי',
            item_id: true,
            contract_number: true
        },
        "187": {
            action: 'תשלום כביש שש',
            invoice_number: true,
            phone_number: true
        },
        "192": {action: 'תשלום מהרות הכרמל',
            invoice_number: true
        },
        "185": {action: 'תשלום חשבון חשמל',
            phone_number: true,
            contract_number: true
        },
        "186": {action: 'הטענת חשמל',
            contract_number: true
        }
    };
    let dataList = [];
    for (const s of suppliers) {
        const products = s.Product[0]?s.Product: [{}];
        for (const p of products) {
            const price = p.ProductPrice && p.ProductPrice[0] ? p.ProductPrice[0].fixed_price : null;
            for (const status of statuses) {
                for (let i=0; i<users.length;i++) {
                    const u = users[i];
                    const userId = u.id;
                    const busId = u.UserBusiness[0].Business.id;
                    for (const d of dates) {
                        users[i].balance -= price;
                        const data = {
                            price: price,
                            status: status,
                            product_id: p.id,
                            supplier_id: s.id,
                            author_id: userId,
                            status_author_id: userId,
                            payment: price,
                            transaction_start_timestamp: d,
                            user_id: userId,
                            business_id: busId,
                            action: actions[s.id]?actions[s.id].action:null,
                            is_canceled:false,
                            business_balance: users[i].balance,
                            distributor_balance: -price

                            // terminal_id: this.req.user.terminal_id,
                            // transaction_parent_id: this.parent,
                            // price_not_including_vat: await VATReduction(this.price)
                        };
                        if(actions[s.id]){
                            if(actions[s.id].item_id){
                                data.item_id = items.pop()||defualtItem;
                            }
                            if(actions[s.id].phone_number){
                                data.phone_number = phoneNumbers.pop()||defualtphoneNumber;
                            }
                            if(actions[s.id].contract_number){
                                data.contract_number = contractNumbers.pop()||defaultContractNumber;
                            }
                            if(actions[s.id].invoice_number){
                                data.invoice_number = ++invoiceNumber;
                            }
                        }
                        dataList.push(data);
                        logger.info(data);
                    }
                }
            }
        }

    }
    return dataList;
}

/**
 * Returns demo dates
 * @returns {*[]}
 */
function getDates() {
    let date3 = new Date();
    let date1 = new Date();
    let date2 = new Date();
    let date4 = new Date();
    let date5 = new Date();
    let date6 = new Date();

    let dateNow = new Date().getDate();
    date3.setDate(dateNow - 1);
    date2.setDate(dateNow - 2);
    date1.setDate(dateNow - 3);
    date4.setDate(dateNow - 10);
    date5.setDate(dateNow - 11);
    date6.setDate(dateNow - 13);

    return [date1, date2, date3, date4, date5, date6];
}


/**
 * Returns all possible statuses
 * @returns {[number, number, number]}
 */
function getStatuses() {
    const transactionOpened = 1;
    const transactionFailed = 4;
    const transactionSucceeded = 3;

    return [transactionOpened, transactionFailed, transactionSucceeded];
}






/***
 * Create demo transaction on DB
 * @returns {Promise<void>}
 */
async function createTransactionDemo() {

    const statuses = getStatuses();
    const dates = getDates();

    // const manualCardId = "1";
    // const manualCardFlag = false;
    // const actions = { localPayment: 'תשלום מקומי', virtualCardCharge: 'הטענת כרטיס וירטואלי', roadSix: ',תשלום כביש שש', carmelTunnels: 'תשלום מהרות הכרמל', iecBill: 'תשלום חשבון חשמל', iecMatama: 'הטענת חשמל' };
    // const unpaidFlag = false;
    // const iecPay = false;
    //

    const models = require("../index");
    let suppliers = await getSuppliers(models);
    let users = await getUsers(models);
    users.map(u=>{u.balance=0;})
    let dataList = makeTransactions(suppliers, statuses, users, dates);

    const transactionModel = models.transaction.model;
    await transactionModel.bulkCreate(dataList);

}

async function createCancellationRequestsTransaction(){
    let randomIds = []
    for (let i = 0; i < 100; i++) {
        randomIds.push(Math.floor(Math.random() * 5000) + 1)
    }
    await models.transaction.model.update({ cancel_process_status: "AWAITING" }, { where: { id: randomIds } })

    let randomTransactions = await models.transaction.model.findAll({ where: { id: randomIds }, raw:true })
    randomTransactions.map((transaction) => {
        transaction.parent_transaction_id = transaction.id
        delete transaction.id;
        transaction.transaction_end_timestamp = moment().toDate();
        transaction.created_at = moment().toDate();
        transaction.transaction_start_timestamp = moment().toDate();
        transaction.transaction_type = awaitingForCancellationType;
        transaction.is_cancelable = false;
        transaction.user_id = 2;
    })
    

    await models.transaction.model.bulkCreate(randomTransactions)
    
}

/***
 * Creates demo data in multi tables
 * @returns {Promise<void>}
 */
async function createAllDemo() {
    //WARNING: Don't change the order of the function calls
    await createDemoFromObjects("language", objectsDemo["languages"]);
    await createDemoFromObjects("user", objectsDemo["distributor_user"]);
    await createDemoFromObjects("tag", objectsDemo["messagesTags"]);
    await createDemoFromObjects("tag", objectsDemo["demoTags"]);
    await createDemoFromObjects("distributor", objectsDemo["distributors"]);
    await createDemo("business");
    await createDemo("user");
    await createDemoFromObjects("country", objectsDemo["country"]);
    await createDemoFromObjects("banner_location", objectsDemo["banner_location"]);
    await createDemoFromObjects("branch", objectsDemo["branches"]);
    await createDemoFromObjects("terminal", objectsDemo["terminals"]);
    await createDemoFromObjects("banner", objectsDemo["banner"]);
    await createDemoFromObjects("area", objectsDemo["area"]);
    await createDemoFromObjects("site", objectsDemo["sites"]);
    await createDemoFromObjects("city_payment_type", objectsDemo["cityPaymentType"]);
    const {createDataFromCsv} = require("../rest/restData/local/cityPay/readCityPay");
    await createDataFromCsv("property_tax.csv", 1);
    await createDataFromCsv("parking.csv", 2);
    await createDataFromCsv("water.csv", 3);
    await createDemoFromObjects("link", objectsDemo["cities_sites"]);
    await createDemoFromObjects("credit_card", objectsDemo["creditCards"]);
    await createDemoFromObjects("gift", objectsDemo["gifts"]);
    await createDemoFromObjects("supplier", objectsDemo["suppliers"]["prepaid"]);
    await createDemoFromObjects("supplier", objectsDemo["suppliers"]["bills"]);
    await createDemoFromObjects("barcode_type", objectsDemo["barcodeTypes"]);
    await createDemoFromObjects("product", objectsDemo["textVoiceCards"]["virtualCards"]);
    await createDemoFromObjects("product", objectsDemo["pelephoneGeneralCards"]["virtualCards"]);
    await createDemoFromObjects("product", objectsDemo["partnerProducts"]);
    await createDemoFromObjects("product", objectsDemo["benleumiProducts"]);
    await createDemoFromObjects("product", objectsDemo["cellcomProducts"]);
    await createDemoFromObjects("product", objectsDemo["freePhoneProducts"]);
    await createDemoFromObjects("product", objectsDemo["JawalProducts"]);
    await createDemoFromObjects("product", objectsDemo["wataniyaProducts"]);
    await createDemoFromObjects("product", objectsDemo["homeCardProducts"]);
    await createDemoFromObjects("product", objectsDemo["iecPayProducts"]);
    await createDemoFromObjects("product", objectsDemo["iecChargeProducts"]);
    await createDemoFromObjects("product", objectsDemo["roadSixProducts"]);
    await createDemoFromObjects("product", objectsDemo["carmelTunnelsProducts"]);
    await createDemoFromObjects("product", objectsDemo["bezeqOnlineProducts"]);
    await createDemoFromObjects("product", objectsDemo["authoritiesPaymentProducts"]);
    await createDemoFromObjects("product", objectsDemo["mobile012Products"]);
    await createDemoFromObjects("product", objectsDemo["bezeqBenLeumiProducts"]);
    await createDemoFromObjects("manual_card", objectsDemo["pelephoneGeneralCards"]["manualCards"]);
    await createDemoFromObjects("manual_card", objectsDemo["partnerManualCards"]);
    await createDemoFromObjects("manual_card", objectsDemo["benleumiManualCards"]);
    await addBaseCategory();
    await addProductCategory(objectsDemo["textVoiceCards"]["virtualCards"]);
    await addProductCategory(objectsDemo["pelephoneGeneralCards"]["virtualCards"]);
    // await createDemoFromObjects("transaction", objectsDemo["transactions"]);
    await createDemoFromObjects("vat", objectsDemo["vat"]);
    await createDemoFromObjects("product_cost", objectsDemo["product_cost"]);
    await createDemoFromObjects("businessPayment", objectsDemo["businessPayments"]);
    await createTransactionDemo();


    
    await createPermissionsDemo();
    await createIPAndTokens();
    await createCancellationRequestsTransaction()
    const TagOfBusinessMessage = models.tagOfBusinessMessage.model;
    const TagOfMessage = models.tagOfMessage.model;
    await TagOfBusinessMessage.create({tag_name: "read"});
    await TagOfBusinessMessage.create({tag_name: "favorite"});
    await TagOfMessage.create({tag_name: "giftOrder"});
    await TagOfMessage.create({tag_name: "cancellation"});
    await TagOfMessage.create({tag_name: "credit"});
    const messageModel = models.message.model;
    for (m of objectsDemo["messages"]) {
        await messageModel.create(m);
    }
    const businessNotificationByTagModel = models.businessNotificationByTag.model;
    await businessNotificationByTagModel.create({tag_of_message_id: 1, business_id: 1});
    await businessNotificationByTagModel.create({tag_of_message_id: 2, business_id: 1});
    await businessNotificationByTagModel.create({tag_of_message_id: 3, business_id: 1});
    await businessNotificationByTagModel.create({tag_of_message_id: 1, business_id: 2});
    await businessNotificationByTagModel.create({tag_of_message_id: 2, business_id: 2});
    await businessNotificationByTagModel.create({tag_of_message_id: 3, business_id: 2});
await createProfitDemo()

}

// createSuperUser = async ()=>{
//     let business = await models.business.modle.findOne({
//         where: {id: 1},
//         include: [
//             {model: modlels.businessProduct.model, as: 'BusinessProduct'}
//         ]
//     })
//     let suppliers = models.supplier.modle.findAll({
//         attributes: ['min'],
//         include: [{
//             attributes: ['product_id'], 
//             model: modlels.product.model, as: 'Prodcut'
//         }],
//         raw:true
//     })
//     business.BusinessProduct.forEach(bp=>{
//         bp.update({})
//     })

//     modlels.businesSupplier.modle.update({
//         is_authorized: ture
//     }, {
//         where: {business_id: 1}
//     })
// }

createProfitDemo=async()=>{
   await models.business.model.update({use_article_20:true}, { where: { id: 3 } })
   await  models.business.model.update({use_distribution_fee:true}, { where: { id: [3,2,5] } })
   await  models.productPrice.model.update({distribution_fee:10}, { where: {product_id:[994,101,11]  } })
   await  models.product.model.update({profit_model:"commission"}, { where: { id: 102 } })
   await  models.businessProduct.model.update({percentage_profit:3}, { where: { business_id: 5,product_id:[994,101,11] } })
   await  models.businessProduct.model.update({ final_commission:3}, { where: { business_id:[3,2,5],product_id:102 } })
   await   models.businessProduct.model.update({percentage_profit:5}, { where: { business_id: [3,2],product_id:[994,101,11] } })
   await  models.businessProduct.model.update({business_commission:10}, { where: { business_id: [3,2],product_id:102 } })
   await  models.businessProduct.model.update({distribution_fee:true}, { where: { business_id: [3,2],product_id:[994,11]  } })

}

createPermissionsDemo = async () => {
    const PELETOK1_USER = 5;
    const PELETOK2_USER = 6;

    const Role = models.role.model
    const User = models.user.model

    const roles = objectsDemo['roles'];


   await roles.forEach(async (r) => {
        await Role.create(r).catch((err) => {
            console.log(err)
        } )
   } )



   User.findById(PELETOK1_USER).then((user) => {
       user.setRoles([1]);
   } )

   User.findById(PELETOK2_USER).then((user) => {
       user.setRoles([2]);
   } )


}

createIPAndTokens = async () => {
    await models.userIp.model.bulkCreate([
        {
            ip_address: '127.0.0.1',
            business_id: 2,
        },
        {
            ip_address: '127.0.0.2',
            business_id: 2,
        }
    ])
    await models.userToken.model.bulkCreate([
        {
            token: 'KDqn9YHZwFItVoRx',
            user_id: 2,
        },
        {
            token: 'MB8QyifKg8KUupJx',
            user_id: 2,
        }
    ])

    // await models.activitySession.model.bulkCreate([
    //     {
    //         pasport_session: '123454',
    //         user_id: 5,
    //         business_ip_id: 1,
    //         business_token_id: 1,
    //     },
    //     {
    //         pasport_session: '67894',
    //         user_id: 5,
    //         business_ip_id: 2,
    //         business_token_id: 2,
    //     },
    //     {
    //         pasport_session: '875544',
    //         user_id: 5,
    //         business_ip_id: 3,
    //         business_token_id: 3,
    //     }
    // ])
    // let to_delete = await models.activitySession.model.findOne({
    //     where: {
    //         business_token_id: 3
    //     }
    // })
    // await to_delete.destroy()
}


//TODO: remove
/***
 * Example using in generic get and generic get all and generic update
 * @returns {Promise<void>}
 */
async function example() {
    await new GenericGet("product", {"id": 5}).get();
    await getDemo("product");
}


//TODO: remove
/**
 * Inserts images url for supplier
 */
async function addImagesUrl(suppName, imageUrl, type) {
    const supModel = models["supplier"].model;
    const doc = models["document"].model;
    const userDoc = models["userDocument"].model;
    let sup = await supModel.findOne(
        {where: {name: suppName}});
    let supUser = await sup.getSupplierUser();
    let user = await supUser[0].getUser();
    let docId = (await doc.create({url: imageUrl, type: type})).id;
    return await userDoc.create({user_id: user.id, document_id: docId})
}

//TODO: remove
/**
 * Inserts images url for supplier
 */
async function addImageSuppliers() {
    for (const p of Object.values(objectsDemo["suppliers"]["prepaid"])) {
        await addImagesUrl(p.name, p.image, typeOfImage).then(r => logger.info(r));
    }
}

//TODO: remove
/***
 * Inserts base category (internet, recommended, favorite) into DB
 * @return {Promise<void>}
 */
async function addBaseCategory() {

    const favoriteId = (await categoryModel.create()).id;
    await categoryNameModel.create({category_id: favoriteId, name: favorite});

    const recommendId = (await categoryModel.create()).id;
    await categoryNameModel.create({category_id: recommendId, name: recommended});

    const internetId = (await categoryModel.create()).id;
    await categoryNameModel.create({category_id: internetId, name: internet});
}


module.exports.createAllDemo = createAllDemo;
module.exports.createDemoFromObjects = createDemoFromObjects;