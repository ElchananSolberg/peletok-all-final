const logger = require("../logger");
const {Op} = require('sequelize');

const {GenericGet, filterSuffix, sortFieldSuffix} = require("./GenericGet");
const GenericCreate = require("./GenericCreate");
const GenericDBAction = require("./GenericDBAction");

const models = require("../index");
const sequelize = require("sequelize");
const {isObjectEmpty} = require("../utils");
const {changeToArrIfOneIsArr, isEmptyArr} = require("../utils");

//TODO: check about docs tables and userContact table


/***
 * The create method of sequelize uses 2 parameters to save a nested object in DB,
 one is the fields and values for saving in DB and the other is the nested object hierarchy
 This function accepts the first parameter and builds the second parameter (hierarchy)
 * @param attrKeyValArray fields and values for saving in DB
 * @return {{include: Array}} object hierarchy
 */
function buildIncludeByAttrKeyVal(attrKeyValArray) {
    let includeObj = {include: []};
    attrKeyValArray = attrKeyValArray instanceof Array ? attrKeyValArray : [attrKeyValArray];
    for (const obj of Object.values(attrKeyValArray)) {
        for (const k of Object.keys(obj)) {
            if (obj[k] instanceof sequelize.Model) {
                const modelName = k.charAt(0).toLowerCase() + k.slice(1);
                includeObj["include"].push({
                        model: models[modelName].model,
                        as: k,
                        include: buildIncludeByAttrKeyVal(obj[k]).include
                    }
                );
            }
        }
    }
    return includeObj;
}

/**
 *When there is more than one row in the existing or new information or both,
 *  there must be one column that determines which line in the updated information we have
 *  to compare with which line in the current information
 *  This column appears in the current information with a filter suffix or sort field suffix
 *  and updated information without the extension
 *  Results name of the determining field and suffix of the determining field
 * @param obj {Object} First row of current data
 * @return {{filterField: *, suffix: *}}
 *  filterField: name of the determinet field, suffix: suffix of the determining field
 * and false if noe exist determining field
 */
function getFilterField(obj) {
    for (k of Object.keys(obj)) {
        if (k.indexOf(filterSuffix) > -1) {
            return {filterField: k.slice(0, k.lastIndexOf(filterSuffix)), suffix: filterSuffix};
        }
        if (k.indexOf(sortFieldSuffix) > -1) {
            return {
                filterField: k.slice(0, k.lastIndexOf(sortFieldSuffix)),
                suffix: sortFieldSuffix
            };
        }
    }
    return false;
}

/***
 * Saves the updated data and exist data according to their value in the determining column
 For each value, the existing information is stored against the updated information
 for comparison purposes
 * @param current {Array} exist data
 * @param updated {Array} updated data
 * @param fieldFilter {String} name of the determining field
 * @param suffix suffix of the determining field
 * @return {Object} updated data and exist data by value in the determining column
 */
function storeByFilterVal(current, updated, fieldFilter, suffix) {
    let result = {};
    for (const obj of Object.values(current)) {
        let currentFilterVal = obj[fieldFilter] ? String(obj[fieldFilter]) : String(obj.dataValues[fieldFilter + suffix]);
        let currentObj = result[currentFilterVal];
        if (currentObj) {
            currentObj["current"] = obj;
        } else result[currentFilterVal] = {"current": obj};
    }
    for (const dataValues of Object.values(updated)) {
        let currentFilterVal = String(dataValues[fieldFilter]);
        let currentObj = result[currentFilterVal];
        if (currentObj) {
            currentObj["updated"] = dataValues;
        } else result[currentFilterVal] = {"updated": dataValues};
    }
    return result;
}


/***
 * update DB data (generic, by map tables)
 */
class GenericUpdate {
    /***
     * constructor
     * @param actionPath {String} path of request (for getting the fields that access to update)
     * @param whereObject {Object} object for filtering rows the want you update
     * @param updatedData {Object} updated data
     */
    constructor(actionPath, whereObject, updatedData) {
        if (!whereObject || isObjectEmpty(whereObject)) {
            throw Error(`message: 'whereObject' must be object not empty 
            status: {updated: false}`)
        }
        this.actionPath = actionPath;
        this.whereObject = whereObject;
        this.updatedData = updatedData;
        this.baseWhere = {"updating": true};
    }

    /**
     * Returns the current data from DB
     * @return {Promise<info<Model>>}
     */
    async getCurrentData() {
        let getDataManager = new GenericGet(this.actionPath, this.whereObject, true);
        // updating true or updating null and getting true
        getDataManager.baseWhere[Op.or] = [this.baseWhere,
            {[Op.and]: [{getting: getDataManager.baseWhere.getting}, {updating: null}]}];
        delete getDataManager.baseWhere.getting;
        getDataManager.include.raw = false;
        return await getDataManager.get(false);
    }


    /***
     * Returns createManager As it were, the information is new (creation instead of update)
     to represent the information in the DB format and compare it to the existing information
     and create the missing and change the changed
     * @return {Promise<GenericCreate>}
     */
    async createPostNewDataManager() {
        const createDataManager = new GenericCreate(this.actionPath, this.updatedData);
        // updating true or updating null and creation true
        createDataManager.baseWhere[Op.or] = [this.baseWhere,
            {[Op.and]: [{creation: createDataManager.baseWhere.creation}, {updating: null}]}];
        delete createDataManager.baseWhere.creation;
        await createDataManager.config();
        const allFields = await GenericDBAction.getFieldsToInclude(createDataManager.baseWhere);
        await createDataManager.map(allFields);
        await createDataManager.mapNewVal();
        await createDataManager.buildQuery();
        return createDataManager;
    }

    /***
     * Checks the new and current object type and if available, to match the type
     and determine what to do (delete from DB, update to DB, or create new DB information)
     The method is called for each instance of a model
     (and by DB looking: for each table that contains information of
     the nested object we are updating)
     * @param currentInstanceOfModel {Object} instance with current DB data
     * @param newInstanceOfModel {Object} instance with updated data
     * @param parentObjOfCurrent {Object} parent (in object hierarchy)
     instance with current DB data
     * @param modelName {String} Name of the model whose parameters (new and current data) are its
     * instances
     * @return {Promise<void>}
     */
    async checkObj(currentInstanceOfModel, newInstanceOfModel, parentObjOfCurrent, modelName) {
        if (!newInstanceOfModel) return;
        if (isEmptyArr(newInstanceOfModel)) {
            //TODO: Remove DB data in loop
            return;
        }
        if (!currentInstanceOfModel || isEmptyArr(currentInstanceOfModel)) {
            newInstanceOfModel = newInstanceOfModel instanceof Array ? newInstanceOfModel : [newInstanceOfModel];
            for (const instance of Object.values(newInstanceOfModel)) {
                await this.createNewInstance(modelName, instance, parentObjOfCurrent);
            }
            return
        }

        // Saves both objects as an array when at least one of them is already array)
        let {obj1, obj2} = changeToArrIfOneIsArr(currentInstanceOfModel, newInstanceOfModel);
        currentInstanceOfModel = obj1;
        newInstanceOfModel = obj2;


        if (currentInstanceOfModel instanceof Array) {
            const {filterField, suffix} = getFilterField(currentInstanceOfModel[0].dataValues);
            if (filterField) {
                let byFilterField = storeByFilterVal(currentInstanceOfModel, newInstanceOfModel,
                    filterField, suffix);
                for (const filterVal of Object.keys(byFilterField)) {
                    byFilterField[filterVal].updated = byFilterField[filterVal].updated || [];
                    await this.checkObj(byFilterField[filterVal].current, byFilterField[filterVal].updated, parentObjOfCurrent, modelName)
                }

                return;
            }
            if (currentInstanceOfModel.length > 1 || newInstanceOfModel.length > 1) {
                throw Error(
                    "modelName: " + modelName +
                    "\nmessage: " +
                    "Can not update more than one row if there is no filter" +
                    "\ncurrentInstanceOfModel.length: " + currentInstanceOfModel.length +
                    "\nnewInstanceOfModel.length: " + newInstanceOfModel.length) +
                "\ncurrentInstanceOfModel: " +
                JSON.stringify(currentInstanceOfModel, null, 2);
            }
            currentInstanceOfModel = currentInstanceOfModel[0];
            newInstanceOfModel = newInstanceOfModel[0];
        }

        await this.updateInstanceOfModel(currentInstanceOfModel, newInstanceOfModel, modelName)
    }

    async createNewInstance(modelName, newInstanceOfModel, parentObjOfCurrent) {
        let info = `model name: ${modelName}
            updated data: ", ${JSON.stringify(newInstanceOfModel, null, 3)}`;
        const includeByAttrKeyVal = buildIncludeByAttrKeyVal(newInstanceOfModel);
        let message = `Failed to update information
                info: ${info}
                include object: ${JSON.stringify(includeByAttrKeyVal, null, 2)}
                attribute key value: ${JSON.stringify(newInstanceOfModel, null, 2)}
                parent object: ${JSON.stringify(parentObjOfCurrent, null, 2)}
                called function: parentObjOfCurrent.create${modelName}()
                `;
        try {
            await parentObjOfCurrent[`create${modelName}`](newInstanceOfModel,
                includeByAttrKeyVal).then(() => {
                // noinspection JSUnresolvedFunction
                logger.debug("successfully updated\n" + info);
            }).catch(e => {
                // noinspection JSUnresolvedFunction,SpellCheckingInspection
                logger.error(e);
                message += `\nerror: ${e}`;
                throw Error(message);
            });
        } catch (e) {
            // noinspection JSUnresolvedFunction,SpellCheckingInspection
            logger.error(e);
            message += `\nerror: ${e}`;
            throw Error(message);
        }
    }

    /***
     * Updates DB data,
     The method updates the specific instance and runs the checkObj method for each sub-object
     * The method is called for each instance of a model (and by looking at
     the DB for each table that contains information of the nested object we are updating)
     * @param currentInstanceOfModel {Object} instance with current DB data
     * @param newInstanceOfModel {Object} instance with updated data
     * @param modelName {String} Name of the model whose parameters (new and current data) are its
     * instances
     * @return {Promise<void>}
     */
    async updateInstanceOfModel(currentInstanceOfModel, newInstanceOfModel, modelName) {
        const dataValues = currentInstanceOfModel.dataValues;
        if (dataValues) {
            const attrArray = Object.keys(dataValues).filter(
                k => {
                    return dataValues[k] instanceof Date || !(dataValues[k] instanceof Object)
                        ? k : false
                });
            let info = "model name: " + modelName + "\n\told data\t\t\t\tupdated data\t";
            for (const k of attrArray) {
                if (newInstanceOfModel[k] !== undefined) {
                    info += "\n" + k + ": " + currentInstanceOfModel[k] +
                        "\t\t" + k + ": " + newInstanceOfModel[k];
                    currentInstanceOfModel[k] = newInstanceOfModel[k];
                    delete newInstanceOfModel[k];
                }
            }
            await currentInstanceOfModel.save().then((r) => {
                // noinspection JSUnresolvedFunction
                if (!isObjectEmpty(r._changed)) logger.debug("successfully updated\n" + info);
                else logger.debug("modelName: " + modelName +
                    "\nthe data are same to existing data\n");
            }).catch(e => {
                // noinspection JSUnresolvedFunction
                logger.error(e);
                throw Error(`
                    message: Failed to update information
                    info: ${info}
                    error: ${e}
                    updated data object: ${JSON.stringify(currentInstanceOfModel, null, 2)}
                    called function: currentInstanceOfModel.save()`
                )
            })
        }
        if (!currentInstanceOfModel._options.includeNames) return;
        for (const k of Object.values(currentInstanceOfModel._options.includeNames)) {
            await this.checkObj(currentInstanceOfModel[k], newInstanceOfModel[k], currentInstanceOfModel, k);
        }
    }

    /***
     * Updates DB data
     * @return {Promise<void>}
     */
    async update() {
        let currentData = await this.getCurrentData();
        if (!currentData || isEmptyArr(currentData)) {
            logger.warn(`
            filter query: ${this.whereObject},
            message: data not found
            status: {updated: false}`);
            return
        }
        // noinspection JSUnresolvedFunction
        logger.debug("currentData: ", JSON.stringify(currentData, null, 5));
        let postNewDataManager = await this.createPostNewDataManager();
        // noinspection JSUnresolvedFunction
        logger.debug("attribute key value object if this was new information: ",
            JSON.stringify(postNewDataManager.attrKeyVal, null, 5));
        // noinspection JSUnresolvedFunction
        logger.debug("include object if this was new information: ",
            JSON.stringify(postNewDataManager.include, null, 5));

        await this.checkObj(currentData, postNewDataManager.attrKeyVal, null, postNewDataManager.lowerCaseNameOfMainTable).catch(e => {
                logger.error(e)
            }
        );
    }
}

// noinspection JSUnresolvedVariable
module.exports.GenericUpdate = GenericUpdate;