const GenericCreate = require("./GenericCreate");

/***
 * Generic function of post request
 * @param req request object
 * @param res response object
 * @returns {Promise<void>}
 */
async function generalPost(req, res) {
    let err = null;
    let result = await new GenericCreate(req.params.requestPath, req.body).create().catch(e => err = e);
    if (err) {
        //TODO: Replace with logger
        console.log(err.toString());
        res.status(401).send(err.toString());
    } else {
        res.status(200).send(result);
    }
}


module.exports.generalPost = generalPost;