const moment = require("moment");

const models = require('../index.js');
const supplier = models.supplier;
const supplierModel = supplier.model;
const product = models.product;
const productModel = product.model;
const supplierUser = models.supplierUser;
const supplierUserModel = supplierUser.model;
const businessProduct = models.businessProduct;
const businessProductModel = businessProduct.model;
const user = models.user;
const userModel = user.model;
const branch = models.branch;
const branchModel = branch.model;
const productPrice = models.productPrice;
const productPriceModel = productPrice.model;
const statuses = require('../config/statuses.json');
const types = require('../config/types.json');
const getStatus = require('../utils').getNumStatus;


// product names:
const productNames = ['talkMan', '1200 Minutes', 'no limit', '100 minutes free', 'טוקמן 29 ש"ח 1000 דקות והודעות + 150 מגה גלישה. תקף ליומיים',
    'טוקמן 49 ש"ח ללא הגבלה בארץ לשבוע (1500 דקות והודעות + 3GB גלישה)', 'טוקמן אינטרנט 100GB בארץ בלבד(מופעל אוטומטי)',
    'טוקמן 1688 שח מקנה 5000 דקות, 5000 הודעות ו 15 גיגה גלישה בחודש למשך שנה', 'טוקמן חבילת גלישה בנפח 10GB +10GB בונוס סה"כ 20 GB בארץ בלבד, בעלות של 129 ₪ בלבד',
    '"טוקמן" טעינה 400 ש"ח לחיוג לכל הרשתות בארץ וגם לחו"ל עם 013 נטוויז', 'כרטיס 55 ש"ח לחיוג לכל הרשתות  +  40 ₪ בונוס  לשיחותו sms בארץ +5 גיגה +שיחות ללא הגבלה ברשת סלקום '];

// string numbers
const accountingCodes = ['1', '2', '3', '4', '6', '5', '7', '8', '9'];

const demoStatuses = [1, 2, 3,1, 2, 3,1, 2, 3,1]

// prices
const prices = [9, 19, 29, 39, 49, 59, 69, 79, 89, 99, 109, 119, 129, 139, 149, 159, 169, 179, 189, 199];

// suppliers names:
const supplierNames = ['סלקום', 'אורנג', 'פלאפון', 'בינלאומי', 'wataniya', 'jawwal',
    'תשלום חשבון חשמל', 'כביש 6', 'מובייל 012', 'בזק', 'מנהרות הכרמל', 'זיהוי שיחה חסויה', 'הוט מובייל', '013', 'תשלום לרשויות', 'דוחות משטרה'];

// native names
const hebrewNames = ["אבי", "עודד", "ניסים", "יהודה", "משה", "ישראל", "שלמה", "אליהו", "יוסי", "מוטי", "קובי", "אבהרהם", "מנדי", "ישי", "דוד"];
// last names
const lastNames = ["כהן", "לוי", "ישראל", "שטרן", "חגג", "עטיה", "וייסברג", "וייס", "קאופמן", "ביטון", "מזרחי", "דויטש", "פרבר", "בן דוד", "חן"];
const firstNames = ["מאיר", "משה", "זרחיה", "רבקה", "דנה", "גילה", "עינת", "מירב", "מנחם", "אברהם", "שמואל", "יצחק", "רחל", "שרה", "נחמיה"];
// passports examples
const passports = ['5520563', '5520564', "5520565", "5520566", "5520566", "5520568", "5520569", "5520562", "5520561", "5520560"];
// languages id's
const languagesID = ['HE', 'EN', "AR"];

// countries:
const countries = ["israel", "austria", "united states", "afghanistan", "argentina", "armenia", "canada", "cambodia", "croatia", "ethiopia"];

const payment_methods = ["הוראת קבע", "הוראת קבע", "תשלום לסוכן", "תשלום לסוכן", "תשלום לסוכן", "תשלום מראש", "הוראת קבע", "תשלום לסוכן", "הוראת קבע", "הוראת קבע"];

const use_point = ["false", "false", "true", "false", "true", "true", "true", "false", "true", "true"];

const num_ip_allowed = [1, 4, null, "4", "8", "2", null, "2", "1", null];

const num_device_allowed = [null, 3, "5", "1", "4", 4, "2", null, 8];

const cities = ["ירושלים", "תל-אביב", "מעלה אדומים", "בני-ברק", "נתניה", "אשדוד", "חולון", "הרצליה", "באר שבע", "דימונה", "ירוחם", "טבריה"];

const streets = ["תותים למשה", "שושנים לדוד", "מצפה נבו", "הר הבית", "דרך קדם", "סורוצקין", "יהודה המכבי", "רבי יוסף קארו", "רבי נחמן מברסלב", "חזון איש", "רשבם"];

const zipCodes = ["9841113", "9841111", "9841112", "9841114", "9841115", "9841115", "9841116", "9841117", "9841118", "9841119", "9841110"];

const englishNames = ['avi', 'oded', 'nisim', 'yehuda', 'moshe', 'israel', 'shlomo', 'eliyahu', 'yosi', 'moti', 'kobi', 'avraham', 'mendy', 'yishai', 'david'];

const businessesNames = ["שופרסל", "רמי לוי", "זול ובגדול", "כל זול", "מעיין 2000", "יינות ביתן", "יש", "יש חסד", "מגה", "מגה בעיר", "מיסטר זול", "קואופ שופ", "ליאור עדיקה"];
const businessesLoginNames = ["שופרסל", "רמי לוי", "זול ובגדול", "כל זול", "מעיין 2000", "יינות ביתן", "יש", "יש חסד", "מגה", "מגה בעיר", "מיסטר זול", "קואופ שופ", "ליאור עדיקה"];

const points = ['50', '50', '50', '50', '50', '50', '50', '50', '50', '50',]

const branchNames = ["המתוקים של ארז", "מתוק בכיכר", "לוטו נחלת שבעה", "יוסי בכפר", "נחמן בן פייגי", "מפגש הבוכרים", "אצל רני", "הבית של פיסטוק", "שום פלפל ושמן זית"];

const documentsTypes = ["type1", "type2", "type3", "type4", "type5", "type6", "type7", "type8", "type9", "type0"];
//TODO : Documents: [types?] Roles:[names?, permissions?] UserContact: [label?] Terminal: [mac_address_required?, cpu_id_required?] BusinessFinance : [limit?]

const roleNames = ['role name1', "name2", "name3", "name4", "name5", "name6", "name7", "name8", "name9", "name0"];

const UserContactLabels = ['label0', "label1", "label2", "label3", "label4", "label5", "label6", "label7", "label8", "label9"];

// urls for documents table:
const urls = ["https://www.lucidchart.com/documents/edit/062ce3b0-f3e4-4fd5-9037-f1f777125625/0?shared=true&existing=1&docId=062ce3b0-f3e4-4fd5-9037-f1f777125625", "https://gitlab.com/pletok/peletok_server", "https://gitlab.com/pletok/peletok_server", "https://gitlab.com/pletok/peletok_server", "https://gitlab.com/pletok/peletok_server", "https://gitlab.com/pletok/peletok_server", "https://www.lucidchart.com/documents/edit/062ce3b0-f3e4-4fd5-9037-f1f777125625/0?shared=true&existing=1&docId=062ce3b0-f3e4-4fd5-9037-f1f777125625"];

// phone numbers for UserContact table (in 'value' row )

const phoneNumbers = ['0508735890', '0508735891', "0508735892", "0508735893", "0508735894", "0508735895", "0508735896", "0508735897", "0508735898", "0508735899"];
const mails = ["peletok0@ravtech.co.il", 'peletok1@ravtech.co.il', 'peletok2@ravtech.co.il', "peletok3@ravtech.co.il", "peletok4@ravtech.co.il", "peletok5@ravtech.co.il", "peletok6@ravtech.co.il", "peletok7@ravtech.co.il", "peletok8@ravtech.co.il", "peletok9@ravtech.co.il"];

// for 'terminal' table:
const terminalNames = ['ADDS-CONSUL-980', 'ADDS-REGENT-100', 'ADDS-REGENT-20', "ADDS-REGENT-200", "ADDS-REGENT-25", "ADDS-REGENT-40", "ADDS-REGENT-60", "ADDS-VIEWPOINT", "ADDS-VIEWPOINT-60", "AED-512"];
const macAddresses = ['0a-1b-3c-4d-5e-6f', '34:34:45:79:34:54', "7a-8b-9c-4d-5e-6f", "3a-3b-3c-3d-3e-3f", "0g-1t-3c-4j-5n-6d", "0a-1c-3h-4p-5i-6q", "0a-1b-3c-4d-5e-6b", "0a-1b-3c-4d-2e-6s", "0a-1b-3c-4d-5e-6w", "0a-1b-3c-4d-5v-6x"];
const cpuIds = ['3febfbff00000f12', "3febfbff00000f10", "3febfbff00000f11", "3febfbff00000f13", "3febfbff00000f14", "3febfbff00000f15", "3febfbff00000f16", "3febfbff00000f17", "3febfbff00000f18", "3febfbff00000f19"];
const localTokens = ['token0', 'token1', "token2", "token3", "token4", "token5", "token6", "token7", "token8", "token9"];
//for UserStatusMessage table:
const messages = ["הטעינה בוצעה בהצלחה", "מספר מכשיר חייב להכיל 7 ספרות בלבד מעבר לקידומת", "מייל לביטול נשלח בהצלחה!", "המתן לאישור הביטול הסופי על ידי המנהל", "אנא בחר יעד למשלוח", "בטל מתנות שנבחרו", "שינוי סיסמא למשתמש", "הסיסמא שונתה בהצלחה!", "נא שמור את החשבונית עד להשלמת הטעינה", "תודה ולהתראות"];

const userNames = ['peletok0', "peletok1", "peletok2", "peletok3", "peletok4", "peletok5", "peletok6", "peletok7", "peletok8", "peletok9"];
const ptAdmins = [false, true, false, false, false, false, false, false, false, false];
const idValadities = [moment('04.05.2022', "MM-DD-YYYY"), moment("01.02.2028", "MM-DD-YYYY"),
moment("03.04.2029", "MM-DD-YYYY"), moment("06.07.2021", "MM-DD-YYYY"), moment("08.09.2024", "MM-DD-YYYY"),
moment("10.11.2033", "MM-DD-YYYY"), moment("11.12.2022", "MM-DD-YYYY"),
moment("05.13.2022", "MM-DD-YYYY"), moment("04.09.2026", "MM-DD-YYYY"),
moment("09.07.2024", "MM-DD-YYYY")];
const israeliId = ["000000018", "000000026", "000000034", "000000042", "000000059", "000000067", "000000075", "000000083", "000000091", "000000190"];
// for UserPassword table:
const passwords = ['pass0', "pass1", "pass2", "pass3", "pass4", "pass5", "pass6", "pass7", "pass8", "pass9"];

// for BusinessFinance table :
const limits = [29000.43, 1342, 436.54, 323, 100000, 89, 2456.8, 999999, 1024];

// for BankAccounts table:
const bankNames = ["לאומי", "יהב", "דיסקונט", "בנק הפועלים", "מרכנתיל", "פאגי", "מזרחי טפחות", "אוצר החייל", "הבנק למשכתנאות", "בינלאומי"];
const bankBranches = ["דרך קדם", "כיכר יהלום", "קניון אדומים", "מרכז העיר", "גאולה", "מאה שערים", "שמעון הצדיק", "הבוכרים", "המלך גורג", "תחנה מרכזית"];
const accountNumbers = ['01885680', '01885681', '01885682', "01885683", "018856804", "01885685", "01885686", "01885687", "01885688", "01885689"];

// for CreditCard table:

const cc_company_names = ["ויזה", "ישראכרט", "מאסטרקארד", "אמריקן אקספרס", "לאומי קארד"];

const creditFrames = [100000, 200, 300, 400, 500, 600, 700, 800, 100000, 100000]
const creditTempFrames = [100000, 100, 200, 300, 400, 500, 600, 700, 800, 900]
const balances = [100000, -100, -70, 231, -122, 111, -625, 255, -562, -700]

const business_identifiers = ['34545', '53666', '67466', '43536', '65632', '63478', '48756', '53656', '45612', '88795']

const authorized_dealer_numbers = ['123456789','987654321','133456789','988654321','123446789','987654311','113456789','997654321','123456779','987664321']

const opening_hours = ['08:00 - 16:00','07:00 - 21:00','08:00 - 17:00','09:00 - 21:00','08:00 - 12:30','08:00 - 16:00','07:00 - 21:00','08:00 - 17:00','09:00 - 21:00','08:00 - 12:30']





//TODO: Add for others tables
const distributorId = [1, 2, 3, 1, 3, 3, 1, 1, 1, 1];
module.exports.demo =
    {
        product: {
            product_name: productNames, price: prices,
            //TODO: Fix language in map.csv
            // Language_language_id: languagesID
        },
        business: {

            'country': countries, 'city': cities, 'zip_code': zipCodes, 'name': businessesNames, 'business_identifier': business_identifiers,
            'user_name': businessesLoginNames, 'points': points, 'status': demoStatuses ,'distributor_id': distributorId ,"tag_id": [1], 'frame': creditFrames,
            'temp_frame':creditTempFrames, 'balance' : balances, 'phone_number': phoneNumbers, 'business_license_number' : authorized_dealer_numbers,
            'opening_hours': opening_hours, 'payment_method': payment_methods,
            "use_point": use_point,
        },


        user: {
            'username': userNames,
            'pt_admin': ptAdmins,
            'password': passwords,
            'country': countries,
            'city': cities,
            'zip_code': zipCodes,
            'last_name': lastNames,
            'first_name': firstNames,
            'phone_number': phoneNumbers,
            'israeli_id': israeliId,
            'account_mail': mails,
            'id_validity': idValadities,
            'language_code': languagesID,
            'business_id': [1, 2, 3, 4, 5, 6, 7, 8, 9, 1],
            'points': points,
            "status":[1,1,1,1,1,1,1,1,1,1],
            'document_by_type': [
                {type: "תעודת זהות", url: "file1.png", expired_time: moment('18-9-2019', "DD-MM-YYYY")},
                {type: "תעודת זהות", url: "file2.png", expired_time: moment('18-6-2019', "DD-MM-YYYY")},
                {type: "תעודת זהות", url: "file3.png", expired_time: moment('5-2-2019', "DD-MM-YYYY")},
                {type: "תעודת זהות", url: "file4.png", expired_time: moment('14-10-2019', "DD-MM-YYYY")},
                {type: "תעודת זהות", url: "file5.png", expired_time: moment('1-2-2020', "DD-MM-YYYY")},
                {type: "תעודת זהות", url: "file6.png", expired_time: moment('12-5-2011', "DD-MM-YYYY")},
                {type: "תעודת זהות", url: "file7.png", expired_time: moment('12-7-2040', "DD-MM-YYYY")},
                {type: "תעודת זהות", url: "file8.png", expired_time: moment('6-9-2025', "DD-MM-YYYY")},
                {type: "תעודת זהות", url: "file9.png", expired_time: moment('18-2-2019', "DD-MM-YYYY")}]    
        }
    };



