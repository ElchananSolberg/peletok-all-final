const {capitalize} = require("../utils");
const {mapPathQuery, path, table, whereTable, mapFieldQuery} = require("./map_tables");


/***
 * Manage generic actions in DB by mapping tables
 * (Consider effectiveness vs. generality)
 */
class GenericDBActions {
    /***
     * Constructor
     * @param actionPath path of action (by mapping tables)
     *  (for example:
     *      if you want to create User with details (password, contact, etc) then actionPath="user"
     *      and:
     *      if you want to create seller with details then actionPath="business")
     */
    constructor(actionPath) {
        this.actionPath = actionPath;
        this.baseWhere = {'$MapPathQuery.Path.path$': this.actionPath};
    }

    /**
     * Configs attributes (Class constructor not be an async method)
     * @return {Promise<void>}
     */
    async config() {
        this.startRow = await GenericDBActions.getStartMapRow(this.actionPath);
        if (!this.startRow) {
            throw `the path '${this.actionPath}' is not valid`;
        }
        this.mainTable = this.startRow["Table"];
        this.nameOfMainTable = this.mainTable.table_name;
        this.asTagOfMainTable = capitalize(this.nameOfMainTable);
        this.lowerCaseNameOfMainTable = this.nameOfMainTable.charAt(0).toLowerCase() +
            this.nameOfMainTable.slice(1);
    }

    /***
     * Returns startRow attribute to row that links the path to the corresponding table
     *@param actionPath path of action (user, supplier, etc)
     * @returns mapPathQuery instance
     */
    static async getStartMapRow(actionPath) {
        return await mapPathQuery.findOne({
            where: {
                "$Path.path$": actionPath,
                parent_id: null
            }, include: [{model: path, as: "Path"}, {model: table, as: "Table"}]
        });
    }

    /***
     * Returns all fields to include in query
     * @param baseWhere where object for filter rows in mapFieldQuery table (by path etc)
     * @returns list of mapFieldQuery instances (with information about table of each of them)
     */
    static async getFieldsToInclude(baseWhere) {
        return await mapFieldQuery.findAll(
            {
                where: baseWhere,
                attributes: ["key_name", 'field_name', 'sort_by_field'],
                raw: true,
                include: [{
                    model: mapPathQuery,
                    attributes: [],
                    as: "MapPathQuery",
                    include: [{
                        attributes: [],
                        model: path,
                        as: 'Path'
                    }, {
                        model: table,
                        as: "Table",
                        attributes: ["table_name"]
                    }]}, {
                        model: whereTable,
                        as: "WhereTable",
                        attributes: {all: true}

                    }
                ]
            });
    }

}

module.exports = GenericDBActions;
