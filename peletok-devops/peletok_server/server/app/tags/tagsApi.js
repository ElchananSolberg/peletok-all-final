const models  = require('../index.js')
var Sequelize = require('sequelize');
const Op = Sequelize.Op;
const tagModel = models.tag.model


/**
 * Get tag by name or order
 * @param {*} req 
 * @param {*} res 
 * @param {*} nameOrOder  represents tag name or order
 */
async function getTagByNameOrOrder(req,res,nameOrOder){
    let tag = await tagModel.findOne({where:{[Op.or]: [{ tag_name:nameOrOder }, { order:nameOrOder}]}})
    if(tag){
        res.send(tag)
    }else{
        res.status(400).send('Tag not exist')
    }


}


/**
 * delete tag
 * @param {*} req 
 * @param {*} res 
 */
async function deleteTag(req, res) {
    await tagModel.findOne({
        where: { id: req.params.id }
    }).then(tag => {
        if (tag) {
            tag.destroy().then(info => {
                res.send(info)
            })
        } else {
            res.status(400).send('failed')
        }
    })
}




module.exports.getTagByNameOrOrder = getTagByNameOrOrder
module.exports.deleteTag = deleteTag
