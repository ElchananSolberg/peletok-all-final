const BaseModel = require('../../BaseModel');
const DataTypes = require("sequelize").DataTypes;

/**
 * Tag BusinessTag
 */
class BusinessTag extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true
            }
};
        this.prefix = "TAG";
        this.author = true;
    }
    /**
     * creates associate for BusinessTag model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.hasMany(models.bannerTag.model, {as: 'BannerTag' });
        this.model.belongsTo(models.tag.model, { as: 'Tag' });
        this.model.belongsTo(models.business.model, { as: 'Business' });
    }
}

module.exports = new BusinessTag();