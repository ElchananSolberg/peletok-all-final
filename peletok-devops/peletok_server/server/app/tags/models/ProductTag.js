const BaseModel = require('../../BaseModel');
const DataTypes = require("sequelize").DataTypes;

/**
 * Tag ProductTag
 */
class ProductTag extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {};
        this.prefix = "TAG";
        this.author = true;
    }
    /**
     * creates associate for ProductTag model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.tag.model, { as: 'Tags' });
        this.model.belongsTo(models.product.model, { as: 'Products' });
    }
}

module.exports = new ProductTag();