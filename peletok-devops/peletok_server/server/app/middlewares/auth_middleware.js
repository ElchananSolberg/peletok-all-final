const models = require('../index.js');
const whiteList = require('./path_white_list')

const Role = models.role.model;
const Permission = models.permission.model
const { getNumStatus } = require("../utils");


const AuthMiddleware = async (req, res, next) => {
    if(req.method == 'OPTIONS'){
        return next();
    }
    let uri = req.url.split('/api/v1')[1]
    if(!uri) {uri = req.url}
    // remove query params from uri
    uri = uri.split('?')[0]
    
    // filter last uri element if uri has params example "/user/:userId"
    uriArr = uri.split('/');
    uri = uriArr.filter(u=>!/^\d+$/.test(u)).join('/')
    
    let prefixUri = uri.split('/')[1]
    if (whiteList.fullMatch.indexOf(uri) > -1 || whiteList.matchBegin.indexOf(prefixUri) > -1) {
        return next();
    }


    else if (req.user) {
        let isLocked = (await models.business.model.count({
            where: {
                status: getNumStatus('frozen', 'Business')
            },
            include: [
                {model: models.userBusiness.model, as: 'UserBusiness', where: {
                    user_id: req.user.id,
                },
                    require: true
                }
            ]

        })) > 0;
        if(isLocked &&  /^payment/.test(prefixUri)){
            return res.status(401).send('Unauthorized')
        }
        let UserRoles = await Role.scope({ method: ['userRoles', req.user.id] })
        const roles = await UserRoles.findAll();

        let path = req.method + " " + (uri[uri.length - 1] == '/' ? uri.slice(0, -1) : uri)
        let hasPermission = await Permission.count({
            where: {
                path: path
            },
            include: [{ model: Role, where: { id: roles.map(r => r.id) } }],
        }) > 0
        if (hasPermission) {
            next()
        } else {
            if(process.env.NODE_ENV == 'development-vm'){
                const {PtMailer, peletokMail} = require('../ptMailer')
                if(PtMailer.pathErrorStack.indexOf(path) == -1){
                    PtMailer.pathErrorStack.push(path)
                    await PtMailer.send(peletokMail, 'peletokdev@ravtech.co.il', 'Usar hasn\'t permission', `User Id: ${req.user.id} Path: ${path}\n\n Full url ${req.url} \n\n Params: ${JSON.stringify(req.params)}`)
                }
            }
            console.log("Has no permission for: " + path);
            return res.status(401).send('Unauthorized')
        }
    } else {
            return res.status(401).send('Unauthorized')
    }
}

module.exports = AuthMiddleware
