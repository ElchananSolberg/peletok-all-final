var logs = require('../../logger')
var axios = require('axios')
const PaymentBasic = require('../paymentBasic');
const models = require('../../index');
const ResInfo = require('../../resInfo')
const LM = require('../../config/LanguageManager').LM
const productPrice = models.productPrice
const productPriceModel = productPrice.model
const {MongoLogger} = require('../../mongoLogger')

const basicUrl = "http://104.155.16.69/israel/api/freephone/api.php"
const SUCCESS_CODE = "SUCCESS"
const deposit = "Deposit"
const createUser = "CreateUser"
const name = "peletop"
const pass = "A34bbCTz41hh"

/**
  * class to   create new  user  for  Frephone @  to  pay (replenish)  a deposit of user 
  * 
  */

class Freephone extends PaymentBasic {
    constructor(contractNumber, phoneNumber, productId,req) {
        
        
        super()
        this.req=req
        this.phoneNumber = phoneNumber
        this.contractNumber = contractNumber
        this.productId = productId
        this.info = new ResInfo(req)
    }


    

    /**
     * function  to  pay   a deposit of user (if  have  number  of user ) a  create  new  user and pay  a deposit for  this  new user 
     * TODO change transactionId_test to transactionId
      */
    async  pay(transactionId) {
        let transactionId_test=Date.now()
        let reqPrice = await this.getPrice()
        let price = reqPrice.price
        let strQuery

        let userDemo=await this.isDemoTransactionUser()
        if(userDemo){
            return this.demoRespons()
        }else{
            if (!this.contractNumber) {
                strQuery = basicUrl + "?transaction_id=" + transactionId_test + "&amount="+price+"&seller_id=" + name + "&seller_pass=" + pass + "&action=" + createUser + "&phone" + this.phoneNumber
            }
            else{
                strQuery = basicUrl + "?transaction_id=" + transactionId_test + "&user_id=" +this.contractNumber + "&amount=" + price + "&seller_id=" + name + "&seller_pass=" + pass + "&action=" + deposit + "&phone" + this.phoneNumber
               
            }
            return this.fill_detailes(strQuery, price, transactionId);
        }
       

       
    }
    /**
     * to send request 
     * return user_id,price (and  if  this a  new  user  return password )
     * 
     */
    async fill_detailes(strQuery, price, transactionId) {
              
        return new Promise(resolve =>
            axios.get(strQuery)
                .then((response) => {
                     MongoLogger.logTransaction({
                            transactionId,
                            supplierId: this.req.params.supplierID,
                            requestParams: this.req.body,
                            request: strQuery,
                            response: response ,
                        })
                    
                    if (response.data["status"] == SUCCESS_CODE) {
                        this.info.isPaid = true
                        this.info.send = {
                            "success": {
                                price: price,
                                user_id: response.data["user_id"],
                                user_password: response.data["user_password"]
                            }
                        }
                        logs.info(this.info.send)
                        resolve(this.info)

                    }
                    else {
                        this.info.error.err_in_Payment = true
                        this.info.setErrMessage(LM.getErrorBasic(), response.data["error_message"]|| response.data["error_code"])
                        resolve(this.info)
                    }
                }
                )
                .catch(e => {
                    this.info.setErrMessage(LM.getErrorBasic(), "err_connection_site" + e.toString())
                    this.info.error.err_connection_site = true
                    resolve(this.info) 
                })

        )
    }


}

module.exports = Freephone;
