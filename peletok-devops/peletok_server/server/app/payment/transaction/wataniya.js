const axios = require('axios').default;
const logs = require('../../logger')
const PaymentBasic = require('../paymentBasic');
const models = require('../../index');
const Resinfo = require('../../resInfo')
const qs = require("querystring");
const urlLogin = "https://prod.palpay.ps/palpay/ar/api/Auth/login"
const urlTopup = "https://prod.palpay.ps/palpay/ar/api/MerchantApi/topup"
const urlBanance = "https://prod.palpay.ps/palpay/ar/api/MerchantApi/dealers"
const urlConfirmation = "https://prod.palpay.ps/palpay/ar/api/MerchantApi/transactions?limit=1&since=0"
const LM = require('../../config/LanguageManager').LM
const product = models.product
const productModel = product.model
const productPrice = models.productPrice
const productPriceModel = productPrice.model
const {MongoLogger} = require('../../mongoLogger')

class Wataniya extends PaymentBasic {
    constructor(contractNumber, productId,req) {
        super()
        this.contractNumber = contractNumber
        this.info = new Resinfo(req)
        this.req=req

        this.productId = productId
    }
    

    /***
        * Payment  wattaniya
        * TODO   to  add pass + user  to  evn_file
           */

    async pay(transactionId) {
        if ((await this.isDemoTransactionUser())) {
            return this.demoRespons()
        }
        else {

            let contractNumber = this.contractNumber
            const body = {
                code: "",
                param: "hi",
                password: 123456,
                username: "moeen13455"
            }
            /***
                     process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;  == deactivate SSL certificate  verification for watta
                       */

            process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;
            let res = await axios.post(urlLogin, qs.stringify(body), {
                withCredentials: true
            })
            let cookies = res.headers["set-cookie"]
            let resBalance = await axios.get(urlBanance, {
                withCredentials: true,
                headers: {
                    Cookie: cookies
                }
            })
            let balance = []
            balance = resBalance.data
            let item = await productModel.findOne(
                {
                    where: { id: this.productId }
                })

            let itemName = item["supplier_identity_number"]
            if (!itemName) {
                this.info.setErrMessage(LM.getErrorBasic(), "Error in supplier_identity_number ")
                return this.info
            }
            let date = await Date.now()
            let mobileNum = this.contractNumber.slice(3)
            const bodyPayment = { "mobileNumber": mobileNum, "amount": itemName, "dealerId": "297", "dealer": qs.stringify(balance[0]), "chosenCustomer": null, "isDebt": false, "param": "hi", "mobile": this.contractNumber, "status": 1, "date": date, "transIdx": 1 }

            let resPay = await axios.post(urlTopup, qs.stringify(bodyPayment), {
                withCredentials: true,
                headers: {
                    Cookie: cookies,


                }
            })

            /***
                rest.data   return "" ,it"s success but  don"t  have сonfirmation
                make  new post to  page  to get сonfirmation 
        
                   */

            if (resPay.data === "") {
                let reference = await new Promise(async (resolve, reject) => {

                    setTimeout(async () => {

                        let сonfirmation = await axios.get(urlConfirmation, {
                            withCredentials: true,
                            headers: {
                                Cookie: cookies
                            }
                        })
                        if (сonfirmation.data["transactions"][0]["status"] == 4) {
                            if (сonfirmation.data["transactions"][0]["mobile"] == contractNumber) {

                                resolve(сonfirmation.data["transactions"][0]["reference"])
                            }

                            else {
                                resolve("000000")
                                this.info.error.noConfirmation = true
                            }

                        }

                        else {
                            this.info.error["err_in_Payment"] = true
                            this.info.setErrMessage(LM.getErrorBasic(), "problems in  payment")
                            resolve(this.info)
                        }

                    }, 4000)

                })



                if (this.info.error.messageForLog) {
                    return this.info
                }


                this.info.isPaid = true
                this.info.send = { "success": reference }
                this.info.confirmation = reference
                MongoLogger.logTransaction({
                    transactionId,
                    supplierId: this.req.params.supplierID,
                    requestParams: this.req.body,
                    request: 'robot',
                    response: this.info ,
                })
                return this.info
            }

            else if (resPay.data["error"] = "true") {
                this.info.setErrMessage(LM.getErrorBasic(), resPay.data["errors"])
                return this.info
            }
            else {
                this.info.setErrMessage(LM.getErrorBasic(), "changes in  API wattamia")
                return this.info
            }
        }
    }
}

module.exports = Wataniya;

