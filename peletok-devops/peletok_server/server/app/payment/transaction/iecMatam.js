const  BasicIEC=require("./iecBasic")
const details = 1
const prePaidPay = 2
const approval = 3
const revert=4


/***
    * Payment  IECMatam
       */
    class IECMatam extends BasicIEC {

    constructor(contract, price,req) {
           super(contract, price)
           this.details=details
           this.prePay=prePaidPay
           this.approval=approval 
           this.revert=revert 
           this.req=req
    }
}
module.exports = IECMatam

