var soap = require('soap');
var logs = require('../../logger')
const path = require('path')
const PaymentBasic = require('../paymentBasic')
const models = require('../../index')
const Resinfo = require('../../resInfo')
const LM = require('../../config/LanguageManager').LM
const utils = require("../../utils")
const product = models.product
const productModel = product.model

const productPrice = models.productPrice
const productPriceModel = productPrice.model

var url = path.join(__dirname, "./wsdl/bezeq014_2.wsdl")

/***
 * Payment manager Bezeq014
 *
 */
class Bezeq014 extends PaymentBasic {


    constructor(contractNumber, productId, req) {
        super()
        this.req = req
        this.info = new Resinfo(req)
        this.productId = productId
        this.contractNumber = contractNumber
        this.timeout = 5000
    }

    /***
   * check  what price of  bil/product 
   * 
   */
    async getPrice() {
        const product = await productPriceModel.findOne(
            {
                where: { product_id: this.productId }
            })

        if (!product) {
            this.info.setErrMessage(LM.getErrorBasic(), " ProductId  is  null ")
            this.info.err_in_productId = true
            return this.info
        }

        this.price = product["fixed_price"]


        this.info.price = this.price
        logs.info("price  of  card  is ", this.price)
        return this.info

    }

    /***
            * Pay   
            @returns number  of   confirmation
            */
    async pay() {



            const args = {


                SourceSystem: 2,
                LoginId: 63,
                Terminal: 0,
                PrepaidSrviceDetails: {
                    CardCustPhone: "0526192666",
                    CardPkgCode: 101,
                },
                ServiceType: 0
            
            
            
            }


           await this.createSoapRequest(args)
       
           
      

    }

    /***
    * soapRequest 
    @return (resolve) number  of lXRefID
   */
    async createSoapRequest(args) {
        var util = require('util');

        // let client = await soap.createClientAsync(url,(err, result, rawResponse, soapHeader, rawRequest)=>{
        //     console.log('err:', err)
        //     console.log('res:', result)
        // });
        let info=this.info
        process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"
        soap.createClient(url, function (err, client) {
            client.setSecurity(new soap.BasicAuthSecurity('EAIPeletop', 'Ax48559p!'));
        
      client['PrepaidCardUpsert.Request'](args, function (err, result) {
      
console.log(result);
console.log(info);

              if (err){
                info.setErrMessage(LM.getErrorBasic(), err)
               info.error.err_connection_site = true
              
              }
              else {

                if(result.ErrorCode==0){
                    info.confirmation = result
                    info.send = { "success": result.TransactionId }
                    info.confirmation=result.
                    info.isPaid = true
                    info.externalId = result
                    console.log(result.TransactionId);
                    
                }
                  else{
                    info.setErrMessage(LM.getErrorBasic(),result.ErrorCode)
                  }  
              }
           
            });

            });
    


        // let client = await soap.createClientAsync(url, function(err, result) {
        //    cccccccccccccccccccccccccccccccccccccccc
        //     process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"

        //    client['PrepaidCardUpsert.Request'](args, { timeout: this.timeout })
        //     console.log('err:', err)
        //        console.log('res:', result)
        // })


        //    let a= client.describe()
        //   let b= util.inspect(a, { showHidden: false, depth: null })
        //    console.log(b);



        //     client.setSecurity(new soap.BasicAuthSecurity('EAIPeletop', 'Ax48559p!'));
        //     process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"

        //    client['PrepaidCardUpsert.Request'](args, { timeout: this.timeout })





        // .then(result => {
        //     console.log("result", result);

        // }).catch(err => {
        //     this.info.error.err_connection_site = true

        //     this.info.setErrMessage("Error in connection bezeq " + err)

        // })
        return this.info;
    }


}


module.exports = Bezeq014;
const args = {


    SourceSystem: 2,
    LoginId: 63,
    Terminal: 0,
    PrepaidSrviceDetails: {
        CardCustPhone: "0526192666",
        CardPkgCode: 101,
    },
    ServiceType: 0



}

n = new Bezeq014("222", "444").pay()

