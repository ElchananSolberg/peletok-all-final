const models = require('../../index.js')
const Op = require('sequelize').Op;
let Transaction = require('./transactions')
const connectionError = 7
const paymentError = 8
const transactionSucceeded = 3


/**
 * gat all Unpaid transactions in reports format
 * @param {*} req 
 * @param {*} res 
 */
async function getUnpaidTransactions(req, res) {
    let UnpaidList = []
    let UnpaidTransactions = await models.transaction.model.findAll({
        where: { [Op.or]: [{ failure_status:connectionError }, { failure_status:paymentError}], parent_transaction_id: null, ['$Child.id$']: null }, include: [{
            model: models.transaction.model,
            as: "Child",
            where: { "status": transactionSucceeded },
            required: false,

        }, {
            model: models.supplier.model,
            as: "Supplier",
        }]
    }
    )
    if (UnpaidTransactions) {
        for (let i = 0; i < UnpaidTransactions.length; i++) {
            let customizedTransaction = {}
            const currentTransaction = UnpaidTransactions[i];
            customizedTransaction['supplierName'] = currentTransaction['Supplier']['name']
            customizedTransaction['id'] = currentTransaction['id']
            customizedTransaction['date'] = currentTransaction['created_at'].toLocaleDateString();
            customizedTransaction['time'] = currentTransaction['created_at'].getHours() + ':' + (currentTransaction['created_at'].getMinutes() < 10 ? '0' : '') + currentTransaction['created_at'].getMinutes();
            customizedTransaction['price'] = currentTransaction['price']
            UnpaidList.push(customizedTransaction)
        }




        res.send(UnpaidList)
    } else {
        res.send(UnpaidList)
    }

}

/**
 * pay all transaction according to receive ids
 * @param {} req 
 * @param {*} res 
 * @param {*} id  transaction id (an array)
 */
async function payUnpaidTransactions(req, res, id) {
    let paymentList = []
    let errorsList = []
    let successList = []
    let transactionsData = await models.transactionData.model.findAll({
        where: { transaction_id: id }, include: {
            model: models.transaction.model,
            as: "Transaction",

        }
    })
    transactionsData.map((data) => {
        req.body['customerId'] = data['customer_id']
        req.body['billNumber'] = data['bill_number']
        req.body['price'] = data['price']
        req.body['phoneNumber'] = data['phone_number']
        req.body['item_id'] = data['item_id']
        req.body['transaction_id'] = data['Transaction']['id']
        req.body['itemId'] = data['Transaction']['product_id']
        req.params.supplierID = data['Transaction']['supplier_id']
        req['user'] = {}
        req['user']['business_id'] = data['Transaction']['business_id']
        let transaction = new Transaction(req, data['Transaction']['id']);
        paymentList.push(transaction)
    })

    for (let i = 0; i < paymentList.length; i++) {
        const transaction = paymentList[i];
        let info = await transaction.pay()

        if (info.error.message) {
            errorsList.push(({ "error": info.error.message, 'transaction_id': transaction['req']['body']['transaction_id'] }))
        } else {
            successList.push(info.send)
        }

    }

    res.send({ 'errors': errorsList, 'success': successList })



}



module.exports.getUnpaidTransactions = getUnpaidTransactions
module.exports.payUnpaidTransactions = payUnpaidTransactions 