const models = require('../../index.js')
const businessFinance = models.businessFinance
const businessFinanceModel = businessFinance.model
const business = models.business
const businessModel = business.model
const businessProduct = models.businessProduct
const businessProductModel = businessProduct.model
const businessPoint = models.businessPoint
const businessPointModel = businessPoint.model

const product = models.product
const productMode = product.model
const transaction = models.transaction
const transactionModel = transaction.model
const moment = require('moment')
const Confidential = require("./confidential")
const Highway6 = require("./highway6Crawler")
const CarmelTunnels = require("./carmelTunnelsCrawler")
const Pelephone = require("./pelephone")
const Partner = require("./partner")
const IECBill = require("./iecBill")
const CityPayment = require("./cityPayment")

const LM = require('../../config/LanguageManager').LM
const Wataniya = require("./wataniya")
const Jawwal = require("./jawwal")
const logger = require("../../logger")
const IECMatam = require("./iecMatam")
const ManualCard = require("./manualCard")
const Cellcom = require("./cellcom")
const Freephone = require("./freephone")
const Bezeq014 = require("./bezeq014")
const { VATReduction } = require('../../utils.js')
const ResInfo = require('../../resInfo')
const { is_charge_time_limited } = require('../../utils')
const currency = require('currency.js')
const { createApprovedCancelActionNotification, createRejectedCancelActionNotification } = require("../../message/message_utils");
const ProfitCalc = require("../../transactionCalc/profitCalk")
const CommissionCalc = require("../../transactionCalc/commissionCalc")
const cellcomId = "94"
const pelephoneId = "95"
const confidentialId = "196"
const partnerId = "127"
const highway6Id = "187"
const carmelTunnelsId = "192"
const iecBillId = "185"
const iecMatamId = "186"
const cityPayId = "193"
const freephoneId = "202"
const wataniyaId = "209"
const jawwalId = "210"
const mobile012Id = "201"
const bezeq014Id = "205"
const home013Id = "211"

const manualCardId = "1"
const transactionOpened = 1
const transactionFailed = 4
const transactionSucceeded = 3
const transactionDataModel = models.transactionData.model
var manualCardFlag = false
const charge = 1
const cancel = 2
const actions = { bezek014: "בזק 014", wattaniya: "וואטניה", jawwal: "ג'וואל", freephone: 'פריפון ', localPayment: 'תשלום מקומי', virtualCardCharge: 'הטענת כרטיס וירטואלי', roadSix: ',תשלום כביש שש', carmelTunnels: 'תשלום מהרות הכרמל', iecBill: 'תשלום חשבון חשמל', iecMatama: 'הטענת חשמל', paymentCancel: 'ביטול הטענת כרטיס וירטואלי' }
var unpaidFlag = false
var iecPay = false
var citiesPay = false
var road6 = false
const payPayment = 1
const payCancel = 2
let peletok = 1






/**
 *  transaction definition 
 */
class Transactions {
    constructor(req, parent) {
        this.req = req
        this.price = null;
        this.action = null;
        this.transactionData = null;
        this.parent = parent;
        this.info = new ResInfo(req)

    }

    async getManualCardSupplier() {
        let product = await models.product.model.findOne({ where: { id: this.req.body.itemId } })
        return product.supplier_id

    }
    /**
     * checks which supplier called by client and creates supplier payment handler instance
     * @returns payment instance
     */

    async getSupplierById(supplierId, itemId, cancelPaymentFlag) {
        logger.info('payment function invoked ,  with supplier id : ' + supplierId)
        let accessDenied = false
        switch (String(supplierId)) {

            case bezeq014Id:
                this.action = actions['bezek014']
                unpaidFlag = true
                this.transactionData = transactionDataModel.build({ contract_number: this.req.body.contractNumber, item_id: itemId })
                return new Bezeq014(this.req.body.contractNumber, itemId, this.req)




            case cellcomId:
                if (cancelPaymentFlag) {

                    this.action = actions['paymentCancel']
                }

                accessDenied = await is_charge_time_limited(this.req.body.contractNumber)
                if (accessDenied) {
                    throw new Error("Action already performed on this number in last minutes")
                }
                this.transactionData = transactionDataModel.build({ item_id: itemId, contractNumber: this.req.body.contractNumber })
                this.action = actions['virtualCardCharge']
                this.transactionData = transactionDataModel.build({ item_id: this.req.body.itemId, contract_number: this.req.body.contractNumber, business_id: this.req.user.business_id })

                return new Cellcom(this.req.body.contractNumber, itemId, this.req.user.business_id, this.req);

            case freephoneId:
                accessDenied = await is_charge_time_limited(this.req.body.contractNumber)
                if (accessDenied) {

                    throw new Error("Action already performed on this number in last minutes")
                }
                this.action = actions['freephone']
                this.transactionData = transactionDataModel.build({ contract_number: this.req.body.contractNumber, item_id: this.req.body.itemId, phone_number: this.req.body.phoneNumber })
                return this.payment = new Freephone(this.req.body.contractNumber, this.req.body.phoneNumber, itemId, this.req)



            case manualCardId:
                this.action = actions['manualCardCharge']
                this.req.params.supplierID = await this.getManualCardSupplier()
                this.transactionData = transactionDataModel.build({ item_id: this.req.body.itemId, })
                manualCardFlag = true
                return this.payment = new ManualCard(this.req.body.supplierId, itemId, this.req)


            case cityPayId:
                this.action = actions['localPayment']
                unpaidFlag = true
                this.transactionData = transactionDataModel.build({ contract_number: this.req.body.billNum, price: this.req.body.price })
                return new CityPayment(this.req.body.idItem, this.req.body.billNum, this.req.body.idClient, this.req.body.price, this.req)

            case wataniyaId:
                this.action = actions['localPayment']
                unpaidFlag = true
                this.transactionData = transactionDataModel.build({ contract_number: this.req.body.contractNumber, item_id: itemId })
                return new Wataniya(this.req.body.contractNumber, itemId, this.req)

            case jawwalId:
                this.action = actions['localPayment']
                unpaidFlag = true
                this.transactionData = transactionDataModel.build({ contract_number: this.req.body.contractNumber, item_id: itemId })
                return new Jawwal(this.req.body.contractNumber, itemId, this.req)

            case iecMatamId:
                this.action = actions['iecMatama']
                this.transactionData = transactionDataModel.build({ bill_number: this.req.body.contractNumber, price: this.req.body.price, contract_number: this.req.body.contractNumber })
                return new IECMatam(this.req.body.contractNumber, this.req.body.price, this.req)

            case iecBillId:
                this.action = actions['wattaniya']
                this.transactionData = transactionDataModel.build({ bill_number: this.req.body.contractNumber, price: this.req.body.price, contract_number: this.req.body.contractNumber, phone_number: this.req.body.phoneNumber })
                iecPay = true
                return new IECBill(this.req.body.contractNumber, this.req.body.price, this.req.body.idAppeal, this.req.body.businessId, this.req)

            case pelephoneId:
                accessDenied = await is_charge_time_limited(this.req.body.contractNumber)
                if (accessDenied) {
                    throw new Error("Action already performed on this number in last minutes")
                }
                this.action = actions['virtualCardCharge']
                this.transactionData = transactionDataModel.build({ contract_number: this.req.body.contractNumber, item_id: this.req.body.itemId })

                return new Pelephone(this.req.body.contractNumber, itemId, this.req.user.business_id, this.req);



            case confidentialId:
                accessDenied = await is_charge_time_limited(this.req.body.contractNumber)
                if (accessDenied) {
                    throw new Error("Action already performed on this number in last minutes")
                }
                this.transactionData = transactionDataModel.build({ contract_number: this.req.body.contractNumber, item_id: itemId })
                this.action = actions['virtualCardCharge']
                return new Confidential(this.req.body.contractNumber, itemId, this.req);

            case partnerId:
                accessDenied = await is_charge_time_limited(this.req.body.contractNumber)
                if (accessDenied) {
                    throw new Error("Action already performed on this number in last minutes")
                }
                this.transactionData = transactionDataModel.build({ contract_number: this.req.body.contractNumber, item_id: itemId })
                this.action = actions['virtualCardCharge']
                return new Partner(this.req.body.contractNumber, itemId, this.req.user.business_id, this.req);

            case mobile012Id:
                accessDenied = await is_charge_time_limited(this.req.body.contractNumber)
                if (accessDenied) {
                    throw new Error("Action already performed on this number in last minutes")
                }
                this.transactionData = transactionDataModel.build({ contract_number: this.req.body.contractNumber, item_id: itemId })
                this.action = actions['virtualCardCharge']
                return new Partner(this.req.body.contractNumber, itemId, this.req.user.business_id, this.req);

            case home013Id:
                accessDenied = await is_charge_time_limited(this.req.body.contractNumber)
                if (accessDenied) {
                    throw new Error("Action already performed on this number in last minutes")
                }
                this.transactionData = transactionDataModel.build({ contract_number: this.req.body.contractNumber, item_id: itemId })
                this.action = actions['virtualCardCharge']
                return new Cellcom(this.req.body.contractNumber, itemId, this.req.user.business_id, this.req);

            case highway6Id:
                road6 = true
                unpaidFlag = true
                this.transactionData = transactionDataModel.build({ customer_id: this.req.body.customerId, bill_number: this.req.body.billNumber, price: this.req.body.price, phone_number: this.req.body.phoneNumber })
                this.action = actions['roadSix']
                return new Highway6(this.req.body.customerId, this.req.body.billNumber, this.req.body.price, this.req.body.phoneNumber, this.req);

            case carmelTunnelsId:
                unpaidFlag = true
                road6 = true
                this.transactionData = transactionDataModel.build({ customer_id: this.req.body.customerId, bill_number: this.req.body.billNumber, price: this.req.body.price, phone_number: this.req.body.phoneNumber })
                this.action = actions['carmelTunnels']
                return new CarmelTunnels(this.req.body.customerId, this.req.body.billNumber, this.req.body.price, this.req.body.phoneNumber, this.req);

            default:
                throw new Error("supplier not found")
        }
    }

    /**
     * Checks if the user does not exceed credit limits
     */
    async isCreditFrameAuthorized() {
        let credit = await businessFinanceModel.findOne({ where: { business_id: this.req.user.business_id } })
        if (credit) {
            let frame = credit['frame']
            let tempFrame = credit['temp_frame']
            let balance = credit['balance']
            if (currency(frame).add(tempFrame).multiply(-1).value <= currency(balance).subtract(this.price).value) {
                let distributorCredit = (await businessModel.findOne({
                    where: {
                        distributor_id: this.req.user.distributor_id,
                        is_distributor: true
                    },
                    include: [
                        {model: models.businessFinance.model, as: 'BusinessFinance'}
                    ]
                })).BusinessFinance[0]
                return { result: true, credit, distributorCredit }
            }
        }




        return { result: false, credit: credit }
    }


    /**
     * to calculation a profit or commissions for business_price 
     */
    async calculation() {
        let profit = (await productMode.findOne({ where: { id: this.req.body.itemId }, attributes: ["profit_model"] })).profit_model
        let calculation
        if (profit == "profit") {
            calculation = new ProfitCalc(this.req, this.price)

        }
        else {
            calculation = new CommissionCalc(this.req, this.price)

        }


        return await calculation.calculate()


    }
    /**
     * creates transaction in DB with relevant data
     */

    async startTransaction() {

        this.currentTransaction = await transactionModel.create({
            transaction_start_timestamp: moment().toDate(), price: this.price, status: transactionOpened,
            product_id: this.req.body.itemId, supplier_id: this.req.params.supplierID,
            author_id: this.req.user.id, status_author_id: this.req.user.id,

            user_id: this.req.user.id, payment: this.price, phone_number: this.req.body['phoneNumber'] || this.req.body['contractNumber'], business_id: this.req.user.business_id,
            action: this.action, terminal_id: this.req.user.terminal_id, transaction_parent_id: this.parent, price_not_including_vat: await VATReduction(this.price),
            transaction_type: payPayment, is_canceled: false
        })
        // we insert the transaction into the req object for log porpuse
        this.req.transactionId = this.currentTransaction.id
    }

    async updatePoint(action) {
        let businessPoint = (await businessModel.findOne({ where: { id: this.req.user.business_id }, attributes: ["use_point"] })).use_point
        if (businessPoint) {
            let point = (await businessProductModel.findOne({ where: { business_id: this.req.user.business_id, product_id: this.req.body.itemId }, attributes: ["points"] })).points
            let pointsAmount = await businessPointModel.findOrCreate({ where: { business_id: this.req.user.business_id }, defaults: { sum: 0 } })

            let newPointBalance
            if (action == charge) { newPointBalance = currency(pointsAmount["sum"]).add(point).value }
            else { newPointBalance = currency(pointsAmount["sum"]).subtract(point).value }

            pointsAmount["sum"] = newPointBalance

            await pointsAmount[0].update({ sum: newPointBalance })
            return point
        }
    }

    async getVat() {
        let vat = await models.vATRate.model.findOne({
            order: [
                ['start_timestamp', 'DESC']
            ],
            attributes: ['value'],
            raw: true
        })
        return vat.value
    }


    async getBusinessIdentifier() {

        let BusinessIdentifier = (await models.business.model.findOne({ where: { id: this.req.user.business_id }, attributes: ["business_identifier"] })).business_identifier
        return BusinessIdentifier
    }

    /**
     * update transaction and frame details in the end of transaction
     */
    async endTransaction(info) {

        this.currentTransaction['transaction_end_timestamp'] = moment().toDate();
        this.currentTransaction['status_author_id'] = this.req.user.id


        if (info.isPaid) {
            this.currentTransaction['status'] = transactionSucceeded
            let vat = await this.getVat()
            let points = await this.updatePoint(charge)
            let ip = (this.req.headers['x-forwarded-for'] || this.req.connection.remoteAddress || '').split(":");
            ip = ip[ip.length - 1]
            let businessIdentifier = await this.getBusinessIdentifier()
            this.transactionData['ip'] = ip
            this.transactionData['vat'] = vat
            this.transactionData['points'] = points
            this.transactionData['business_identifier'] = businessIdentifier


            let calculation = await this.calculation()
            this.transactionData['confirmation'] = info.confirmation
            this.credit["balance"] = calculation["businessBalance"]
            this.distributorCredit["balance"] = calculation["distributorBalance"]

            this.currentTransaction.business_price = calculation["businessPrice"],
                this.currentTransaction.business_price_not_including_vat = await VATReduction(calculation["businessPrice"]),
                this.currentTransaction.business_profit = calculation["businessProfit"],
                this.currentTransaction.business_profit_not_including_vat = await VATReduction(calculation["businessProfit"]),
                this.currentTransaction.business_profit_vat = calculation["businessVat"],
                this.currentTransaction.business_balance = calculation["businessBalance"],
                this.currentTransaction.distributor_price = calculation["distributorPrice"],
                this.currentTransaction.distributor_price_not_including_vat = await VATReduction(calculation["distributorPrice"]),
                this.currentTransaction.distributor_profit = calculation["distributorProfit"],
                this.currentTransaction.distributor_profit_not_including_vat = await VATReduction(calculation["distributorProfit"]),
                this.currentTransaction.distributor_profit_vat = calculation["distributorVat"],
                this.currentTransaction.distributor_balance = calculation["distributorBalance"]




        }

        if (info.error.messageForLog) {
            this.currentTransaction['failure_status'] = (info.getStatus())[0]
            this.currentTransaction['status'] = transactionFailed
        }

        this.transactionData['transaction_id'] = this.currentTransaction['id']
        this.transactionData['author_id'] = this.req.user.id
        this.transactionData['phone_number'] = this.req.body.phoneNumber
        if (manualCardFlag && info.send) {
            this.transactionData['card_number'] = info.send.success.cardNumber
            this.transactionData['card_code'] = info.send.success.code
        }

        if (iecPay) {
            this.transactionData['client_name'] = info.details['client_name']
            this.transactionData['client_address'] = info.details['client_address']

            this.transactionData['confirmation'] = info.details['confirmation']
            this.transactionData['external_transaction_id'] = info.externalId
            this.transactionData['comment'] = info.comment
        }
        if (citiesPay) {
            let CityPaymentTypeSite = await models.cityPaymentTypeSite.model.findOne({
                where: { id: this.req.body.idItem },
                attributes: [],
                include: [{
                    model: models.city.model,
                    as: "City",
                    attributes: ['name_HE']
                }, {
                    model: models.cityPaymentType.model,
                    as: "CityPaymentType",
                    attributes: ['pay_name']
                }],
                raw: true
            })
            this.transactionData['city_pay_city'] = CityPaymentTypeSite['City.name_HE']
            this.transactionData['city_pay_type'] = CityPaymentTypeSite['CityPaymentType.pay_name']
            this.transactionData['confirmation'] = info.confirmation
            this.transactionData['phone_number'] = this.req.body.phoneNumber
        }

        this.transactionData['request_ip'] = this.req.ip


        await this.transactionData.save()
        await this.credit.save()
        await this.currentTransaction.save()

        let distributorID = (await businessModel.findOne({ where: { id: this.req.user.business_id }, attributes: ["distributor_id"] })).distributor_id
        if (distributorID != this.req.user.business_id && distributorID != peletok) {


            await this.distributorCredit.save()
        }
    }


    /**
      * approval payment 
      
      */
    async approval() {
        let payment = await this.getSupplierById(this.req.params.supplierID, this.req.body.itemId)
        if (!payment) {
            return {
                error: {
                    message: 'supplier not found'
                }
            }
        }
        const { result, credit } = (await this.isCreditFrameAuthorized())
        this.credit = credit
        let info = await payment.approvall()
        this.endTransaction(info);
        if (info.error.message) { return info }
        info.send["currentTransaction"] = this.currentTransaction.id;
        return info
    }

    /**
         * revert (cancel)payment/charging 
         */
    async cancelPayment() {
        let transaction = await models.transaction.model.findOne({
            where: {id: this.req.body.transactionId },
            include: {
                model: models.transactionData.model, as: 'transactionData'
            }
        })
        
        if (!transaction) {
            this.info.setErrMessage(LM.getString("transactionNotExist", this.req.user.language_code))
            return this.info
        }
        if (transaction.is_canceled == true) {
            this.info.setErrMessage(LM.getString("transactionHasCancelled", this.req.user.language_code))
            return this.info
        }
        else {
            let contractNum = transaction.transactionData["contract_number"]
            let itemId = transaction.transactionData["item_id"]
            let businessId = transaction.transactionData["business_identifier"]

            switch (String(this.req.params.supplierID)) {
                case cellcomId:
                    let payment = await new Cellcom(contractNum, itemId, businessId);
                    const infoPrice = await payment.getPrice()
                    if (infoPrice.error.message) { return infoPrice }
                    this.price = infoPrice.price
                    await this.startTransaction()
                    let info = await payment.cancelPayment(this.currentTransaction.id)
                    if (info.error.message) { return info }
                    await this.endTransactionCancled(true)
                    info.send = "success";
                    return info;
                // case confidentialId:

                default: return this.cancelPaymentInternal(true)
            }
        }
    }
    async endTransactionCancled(cancledByApi) {
        let errorFound = false;
        let updateResult = await transactionModel.update({ is_canceled: true, is_cancelable: false, cancel_process_status: "DONE" }, { where: { id: this.req.body.transactionId }, returning: true })
        await models.transactionData.model.update({ cancellation_note: this.req.body.cancellationNote }, { where: { transaction_id: this.req.body.transactionId } })
        //In postgres, length of updateResult = 2, see sequelize doc
        let updatedRows = updateResult[1];
        //updated rows is array
        let data = updatedRows[0];
        let price = this.req.body.cancelAmount ? this.req.body.cancelAmount : data.dataValues.business_price;
        await this.updateTransactionOfCancelRequest().catch(e => { errorFound = true; logger.error(e) });
        await this.updateBalance(data, price).catch(e => { errorFound = true; logger.error(e) });
        let cancelAsTransaction = await this.saveCancelAsTransaction(data, cancledByApi, price).catch(e => { errorFound = true; logger.error(e) });
        await this.cancelInTransactionDataTable(cancelAsTransaction).catch(e => { errorFound = true; logger.error(e) });
        await createApprovedCancelActionNotification(this.req.user.business_id, data.business_id, this.req.body.transactionId, this.req.body.cancellationNote);

    }
    /**
     * payment cancel /charging Internal (to DB)
       */
    async cancelPaymentInternal(cancledByApi) {

        let transaction = await models.transaction.model.findOne({
            where: { id: this.req.body.transactionId }
        })


        if (!transaction) {
            this.info.setErrMessage(LM.getString("transactionNotExist", this.req.user.language_code))

        }

        else {
            
            if (transaction.type == payCancel) {
                this.info.setErrMessage(LM.getString("transactionUnable to cancel", this.req.user.language_code))
                return this.info
            }


            if (transaction.supplier_id == this.req.params.supplierID || this.req.params.supplierID == manualCardId) {
                if (transaction["is_canceled"] == false || this.req.params.supplierID == manualCardId) {
                    await this.endTransactionCancled(cancledByApi)
                    this.info.send = { success: LM.getString("CancelltransactionSuccess", this.req.user.language_code) };
                }
                else {
                    this.info.setErrMessage(LM.getString("transactionHasCancelled", this.req.user.language_code))

                }

            }
            else {
                this.info.setErrMessage(LM.getString("transactionNotbelongSupplier", this.req.user.language_code))
            }
        }

        return this.info











    }

    async rejectCancelation() {
        let updateResult = await transactionModel.update({ is_canceled: false, is_cancelable: false, cancel_process_status: "REJECT" }, { where: { id: this.req.body.transactionId }, returning: true }).catch(e => { errorFound = true; logger.error(e) });
        //In postgres, length of updateResult = 2, see sequelize doc
        let updatedRows = updateResult[1];
        //updated rows is array
        let data = updatedRows[0];
        await this.rejectUpdateTransactionOfCancelRequest().catch(e => { errorFound = true; logger.error(e) });
        await createRejectedCancelActionNotification(this.req.user.business_id, data.business_id, this.req.body.transactionId).catch(e => { errorFound = true; logger.error(e) });
        delete data.dataValues.id;
        this.info.send = "success";
        return this.info
    }

    async updateBalance(data, price) {
        //what is cancelAmount?
        this.credit = await businessFinanceModel.findOne({ where: { business_id: data["business_id"] } })

        //TODO: Fix if price not equals to data.dataValues.price (parti cancel) (sub profite from price)
        this.credit["balance"] = currency(this.credit["balance"]).add(price).value

        //why not using this.credit?
        await this.credit.save()

    }

    saveCancelAsTransaction(data, cancledByApi, price) {
        delete data.dataValues.id;
        data.dataValues.transaction_type = payCancel;
        data.dataValues.parent_transaction_id = this.req.body.transactionId;
        data.dataValues.transaction_end_timestamp = moment().toDate();
        data.dataValues.created_at = moment().toDate();
        data.dataValues.transaction_start_timestamp = moment().toDate();
        data.dataValues.canceled_by_api = cancledByApi;
        //TODO: Change more fields to field * -1
        data.dataValues.price = currency(data.dataValues.price).multiply(-1).value;
        data.dataValues.business_price = currency(data.dataValues.business_price).multiply(-1).value;
        data.dataValues.business_balance = this.credit["balance"];
        data.dataValues.is_canceled = false;
        data.dataValues.is_cancelable = false;
        data.dataValues.cancel_process_status = "DONE";
        return transactionModel.create(data.dataValues);
    }

    async cancelInTransactionDataTable(newTransaction) {
        let currentTransactionData = await models.transactionData.model.findOne({
            where: { transaction_id: this.req.body.transactionId },
            raw: true
        });
        if (currentTransactionData) {
            delete currentTransactionData.id
            let newTransactionData = await models.transactionData.model.build(Object.assign({}, currentTransactionData));
            newTransactionData.transaction_id = newTransaction.id
            let points = await this.updatePoint(cancel)
            newTransactionData.points = points

            await newTransactionData.save()
        }
    }
    /**
       * checkCreditCardId for   payment  for each  supplier
       */
    async checkCreditCardId() {


        return (await models.supplier.model.findOne({ where: { id: this.req.params.supplierID }, attributes: ['credit_card_id'] })).credit_card_id


    }


    /**
     * Main function that handles whole transaction process
     */
    async pay() {
        let error = null;
        let payment;
        await this.getSupplierById(this.req.params.supplierID, this.req.body.itemId).then(p => {
            payment = p;
        }).catch(err => {
            error = { error: { message: err.message } }
        });
        if (error) return error;

        const infoPrice = await payment.getPrice()

        if (infoPrice.error.message) { return infoPrice }
        this.price = infoPrice.price

        const { result, credit, distributorCredit } = (await this.isCreditFrameAuthorized())

        if (!result) {
            return { error: { message: 'creditLimitExceeded' } }
        }
        this.distributorCredit = distributorCredit
        this.credit = credit;

        await this.startTransaction()
        let creditCardId = await this.checkCreditCardId()
        let info = await payment.pay(this.currentTransaction.id, creditCardId)
        await this.endTransaction(info);
        if (info.error.message) { return info }
        info.send["currentTransaction"] = this.currentTransaction.id;
        return info
    }

    async updateTransactionOfCancelRequest() {
        return await transactionModel.update({ cancel_process_status: "DONE" }, { where: { parent_transaction_id: this.req.body.transactionId } })
    }

    async rejectUpdateTransactionOfCancelRequest() {
        return await transactionModel.update({ cancel_process_status: "REJECT" }, { where: { parent_transaction_id: this.req.body.transactionId } })
    }
}

module.exports = Transactions
