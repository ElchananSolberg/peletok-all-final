const models = require('../../index');
const reportView = require("../../transaction/view/ReportView")
const reportViewModel = reportView.model
const sequelize = require('sequelize')
/**
 * 
 * @param {*} req 
 * @param {*} res 
 * @returns object with receipt details according to req.params.transaction_id
 */
async function getReceiptDetails(req, res) {
    let receipt = await reportViewModel.findOne({
        where: { id: req.params.transaction_id },
        attributes: [['id', 'transId'], [sequelize.fn("to_char",
            sequelize.col("transaction_start_timestamp"), ' HH24:MI:SS YYYY/MM/DD'), "transDateTime"], [sequelize.fn("concat",
                sequelize.col("business_name"), ' ', sequelize.col("business_identifier")), "operationPerformer"],
        ['phone_number', 'targetPhone'], ['supplier_name', 'provider'], ['product_name', 'product'], 'price']
    })
    res.send(receipt)
}

module.exports.getReceiptDetails = getReceiptDetails
