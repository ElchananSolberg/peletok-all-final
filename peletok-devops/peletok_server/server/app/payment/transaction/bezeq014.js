var soap = require('soap');
var logs = require('../../logger')
const path = require('path')
const PaymentBasic = require('../paymentBasic')
const models = require('../../index')
const Resinfo = require('../../resInfo')
const LM = require('../../config/LanguageManager').LM
const utils = require("../../utils")
const product = models.product
const productModel = product.model
const {MongoLogger} = require('../../mongoLogger')

const productPrice = models.productPrice
const productPriceModel = productPrice.model

var url = path.join(__dirname, "./wsdl/bezeq014_2.wsdl")

/***
 * Payment manager Bezeq014
 *
 */
class Bezeq014 extends PaymentBasic {


    constructor(contractNumber, productId, req) {
        super()
        this.req = req
        this.info = new Resinfo(req)
        this.productId = productId
        this.contractNumber = contractNumber
        this.timeout = 5000
        this.userName='EAIPeletop'
        this.userPass="Ax48559p!"
    }



    /***
            * Pay   
            @returns number  of   confirmation
            */
    async pay(transactionId) {

        if ((await this.isDemoTransactionUser())) {
            return this.demoRespons()
        }
        else {

            let item = await productModel.findOne(
                {
                    where: { id: this.productId }
                })

            let itemName = item["supplier_identity_number"]
            if (!itemName) {
                this.info.setErrMessage(LM.getErrorBasic(), "Error in supplier_identity_number ")
                return this.info
            }

            const args = {
                SourceSystem: 2,
                LoginId: 63,
                Terminal: 0,
                PrepaidSrviceDetails: {
                    CardCustPhone: this.contractNumber,
                    CardPkgCode:  itemName,
                },
                ServiceType: 0
            
            
            
            }
           await this.createSoapRequest(args, transactionId)
           return  this.info
        }

    }

    /***
    * soapRequest 
    @return (resolve) number  of lXRefID
   */
    createSoapRequest(args, transactionId) {

        process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"
        return new Promise((resolve, reject) => {
            soap.createClient(url,  (err, client) => {
                client.setSecurity(new soap.BasicAuthSecurity(this.userName, this.userPass));
                client['PrepaidCardUpsert.Request'](args,  (err, result) => {
                      MongoLogger.logTransaction({
                            transactionId,
                            supplierId: this.req.params.supplierID,
                            requestParams: this.req.body,
                            request: result.request && result.request.body ? result.request.body : args,
                            response: result.body || result ,
                        })
                      if (err){
                        this.info.setErrMessage(LM.getErrorBasic(), err)
                        this.info.error.err_connection_site = true
                        resolve()
                      
                      }
                      else {

                        if(result.ErrorCode==0){
                            // this.info.confirmation = result
                            this.info.send = { "success": result.TransactionId }
                            // this.info.confirmation=result
                            this.info.isPaid = true
                            // this.info.externalId = result
                            resolve()
                        }
                          else{
                            this.info.setErrMessage(LM.getErrorBasic(),result.ErrorCode)
                            resolve()
                          }  
                      }
               
                });
            });
        });


    }
      



}


module.exports = Bezeq014;


