const PaymentBasic = require('../paymentBasic');
const ResInfo = require('../../resInfo')
const jsdom = require("jsdom");
var httpRequest = require('./httpRequest')
var logs = require('..//../logger')
var models = require('../../index')
const LM = require('../../config/LanguageManager').LM
const { JSDOM } = jsdom;
const {MongoLogger} = require('../../mongoLogger')
const moment = require('moment')
var qs = require('querystring');



/***
 * Payment manager for road6/CarmelTunnel
 */
class PeletalkCrawler extends PaymentBasic {
    /***
     * Constructor
     * @param accountNumber ID number
     * @param invoiceNumberSuffix Last 6 digits of invoice
     */
    constructor(accountNumber, invoiceNumberSuffix, price, phoneNumber, req) {
        super()

        this.req=req
        this.info = new ResInfo(req)
        this.phoneNumber = phoneNumber
        this.price = price;
        this.accountNumber = accountNumber
        this.invoiceNumberSuffix = invoiceNumberSuffix
        this.loginData = {
            'selectedIdLoginType': '1',
            'generalAuthType': '1',
            'accountNumber': accountNumber,
            'invoiceNumberSuffix': invoiceNumberSuffix,
            'passId': 'false'
        };
        this.baseOptions = {
            'hostname': this.getHostname(),
            'headers': {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        };
        this.choiceData = { "selectedBillsIndexes": '0' };



        this.sessionId = null;
        this.error = false;

    }
    /***
   * choise what  url to use (or  road 6  or CarmelTunel )
   * @returns Object with options
   */
    getHostname() { return ""; }

    /***
     * Results request options for login page
     * @returns Object with options
     */
    loginOptions() {
        var options = this.baseOptions;
        options["method"] = "POST";
        options["path"] = '/AnonymousPaymentLogin.do';
        return options;
    }

    /***
     * Results request options for Invoices page
     * @returns Object with options
     */
    getInvoicesOptions() {
        var options = this.baseOptions;
        options["headers"]["Cookie"] = this.sessionId;
        options["method"] = "GET";
        options["path"] = '/FastBillPaymentDetails.do';
        return options;
    }
    /***
    * Results request options for ChoiceBill page
    * @returns Object with options
    */
    postChoiceBill() {
        var options = this.baseOptions;
        options["headers"]["Cookie"] = this.sessionId;
        options["method"] = "POST";
        options["path"] = '/FastPaymentsStage1Action.do';
        return options;
    }
    getChoiceBill() {
        var options = this.baseOptions;
        options["headers"]["Cookie"] = this.sessionId;
        options["method"] = "GET";
        options["path"] = '/FastPaymentsStage2.do';
        return options;
    }

    /***
   * Results request options for Payment page
   * @returns Object with options
   */
    getPayment() {
        var options = this.baseOptions;
        options["headers"]["Cookie"] = this.sessionId;
        options["method"] = "POST";
        options["path"] = '/FastPaymentsStage3Action.do';
        return options;
    }
    /***
        * Results request options for Confirmation page
        * @returns Object with options
        */
    getConfirmationOptions() {
        var options = this.baseOptions;
        options["method"] = "GET";
        options["path"] = "/FastPaymentRedirect.do?forward=FastPaymentsStage4";
        return options;
    }
    /***
     * Get accountNumber  of  bill &  number  or invoiceNum 
     *  Сheck  the amount and account number of  bill 
     * @returns IndexNumber  of  bill in array all bills
     */
    async fill_detailes() {
        this.info.billNumber = this.invoiceNumberSuffix
        this.info.customerId = this.accountNumber

        var invoices = [];
        logs.info("accountNumber  of  bill is " + JSON.stringify(this.loginData['accountNumber']) + " number  or invoiceNum " + this.loginData["invoiceNumberSuffix"] + " amuont is " + this.price + "phoneNumber is " + this.phoneNumber)
        if (!this.sessionId) {
            await httpRequest(this.loginOptions(), true, qs.stringify(this.loginData)).
                then(session => { this.sessionId = session[0] })
                .catch(e => {
                    this.info.setErrMessage(LM.getErrorBasic(), "err_connection_site" + e.toString())
                    this.info.error.err_connection_site = true
                    return this.info

                })
        }

        if (this.sessionId) {
            let invoiceAll

            try {
                invoiceAll = await httpRequest(this.getInvoicesOptions(), false)
            }

            catch (e) {

                this.info.setErrMessage(LM.getString("errorNumID"), "err_in_ID_numBill" + e.toString())
                this.info.error.logingError = true
                return this.info
            }


            invoices = await this.getDocData(invoiceAll)
            

            const forward = null
            for (let i = 0; i < invoices.length;  i++) {
                if (invoices[i]["invoiceNum"] == this.invoiceNumberSuffix) {
                    if (invoices[i]["includeVat"] == this.price && invoices[i]["invoiceNum"] == this.invoiceNumberSuffix) {
                        invoices.splice(i, 1);
                        forward = { "choice": i, "invoices": invoices }
                    }
                    else {
                        this.info.setErrMessage(LM.getString("err_priceBill"))
                    }
                }
            }
            if (forward) {
                return forward;
            } else if (this.info.error.message) {
                return this.info
            } else {
                this.info.setErrMessage(LM.getString("err_billPaid"), "this bill is paid")
                return this.info
            }
        }

    }

    /***
    * check  what price of  bil/product 
            */
    getPrice() {
        this.info.price = this.price
        return this.info
    }

    /***
        *To  pay  a  bill 
        @returns number  of   confirmation if  isPaid
        or    return  this.Info => this.info.error. setErrMessage() if NoPaid
                */
    async pay(transactionId, creditCardId) {

        if ((await this.isDemoTransactionUser())) {
            return this.demoRespons()
        }
        else {



        const phoneNumberTest = "1234567890"
        let confirmation = null
        const choice = await this.fill_detailes();
        if (this.info.error.message) {
            return this.info
        }

        this.choiceData["selectedBillsIndexes"] = choice["choice"]


        await httpRequest(this.postChoiceBill(), true, qs.stringify(this.choiceData))
        await httpRequest(this.getChoiceBill(), true).then(p =>
            logs.info("P1", p))
            
       
            if (this.phoneNumber == phoneNumberTest) {
                let confirmation = "test123"
                this.info.confirmation = confirmation
                this.info.send = { "success": confirmation, "bills": choice["invoices"] }
                return this.info

            }
            let creditCard = await models.creditCard.model.findOne({
                where: { id: creditCardId }
            });
            if(!creditCard){
                this.info.setErrMessage(LM.getString("err_cardNotExist"), "Supplier is not link to credit card")
            }
            
            let payment = {
                'page': '2',
                'creditOwnerFullName': creditCard.cc_holder_name,
                'idType.value': '1',
                'creditOwnerId': creditCard.cc_holder_id,
                'creditNumber': creditCard.cc_number,
                'creditYear.value': moment(creditCard.cc_validity).format('YYYY'),
                'creditMonth.value': moment(creditCard.cc_validity).format('MM'),
                'cvv':  creditCard.cc_cvv_number,
                'total': this.price
            }
            
            

            await httpRequest(this.getPayment(), false, qs.stringify(payment))

                .then(p => {
                    logs.info("p", p)
                    const err = this.getDocDataErr(p)
                    if (err) {
                        this.info.error.err_in_Payment = true
                        this.info.isPaid = true
                        this.info.send = { "success": LM.getString("errPayment"), "bills": choice["invoices"] }
                        MongoLogger.logTransaction({
                            transactionId,
                            supplierId: this.req.params.supplierID,
                            requestParams: this.req.body,
                            request: 'robot',
                            response: this.info ,
                        })
                        return this.info
                    }


                }).catch(e => {
                    this.info.setErrMessage(LM.getErrorBasic(), "err_connection_site" + e.toString())
                    this.info.error.err_connection_site = true
                    MongoLogger.logTransaction({
                        transactionId,
                        supplierId: this.req.params.supplierID,
                        requestParams: this.req.body,
                        request: 'robot',
                        response: this.info ,
                    })
                    return this.info
                })



            await httpRequest(this.getConfirmationOptions(), false, null)
                .then(p => {
                    confirmation = this.getDocDataConfirmation(p)
                })
                .catch(e => {
                    this.info.noConfirmation = true
                    this.info.isPaid = true

                    this.info.send = { "success": LM.getString("errPayment"), "bills": choice["invoices"] }
                    MongoLogger.logTransaction({
                        transactionId,
                        supplierId: this.req.params.supplierID,
                        requestParams: this.req.body,
                        request: 'robot',
                        response: this.info ,
                    })
                    return this.info
                })

            if (this.info.error.message) {
                MongoLogger.logTransaction({
                    transactionId,
                    supplierId: this.req.params.supplierID,
                    requestParams: this.req.body,
                    request: 'robot',
                    response: this.info ,
                })
                return this.info
            }
            logs.info(" num of confirmation is " + confirmation)
            this.info.isPaid = true
            this.info.confirmation = confirmation
            this.info.send = { "success": confirmation, "bills": choice["invoices"] }
            MongoLogger.logTransaction({
                transactionId,
                supplierId: this.req.params.supplierID,
                requestParams: this.req.body,
                request: 'robot',
                response: this.info ,
            })
            return this.info
        
    }
    }

    /**
     *
     * @param {string} htmlString html doc in string format
     * Gets an html doc in string format, parse it to an object,
     * collecting relevant client data and creating a 'bill' object containing the collected data
     * stores the bills object in a array and returns it.
     * if the html doc contains no bills the function will return an empty list
     * @returns  includeVat (amout of bill) & invoiceNum (number of bill ) $ accountNumber  with 
     */
    getDocData(htmlString) {

        var billsList = [];
        if (htmlString.includes('noBill')) {
            this.info.setErrMessage(LM.getString("noBill"), "No  bill  to  pay ")
            this.info.error.noBills = true
            return this.info
        } else {
            const dom = new JSDOM(htmlString);
            /**
                    *Check how much bull's have accountNumber by id 
                    */
            var invoiceNum = []
            let elements = dom.window.document.getElementsByTagName('td');

            Object.values(elements).forEach(element => {
                if (element != null) {
                    if (element.innerHTML.length == 10 && element.innerHTML.includes('/') == false) {
                        invoiceNum.push(element.innerHTML)
                    }
                }
            }
            );

            for (var i = 0; i < invoiceNum.length; i++) {
                let bill = {
                    includeVat: dom.window.document.getElementById('billForPaymentTotal_' + i).innerHTML.replace(/\s+/g, ''),
                    invoiceNum: invoiceNum[i].slice(4),
                    accountNumber: this.accountNumber
                };
                billsList.push(bill)
            }
        }


        return billsList
    }

    /**
     *
     * @param {string} htmlString html doc in string format
     * Gets an html doc in string format, parse it to an object,
     *  number  of Bill's Confirmation
     * @returns  number of confirmation
      */
    getDocDataConfirmation(htmlString) {

        const dom = new JSDOM(htmlString);
        let confirmation = dom.window.document.getElementsByTagName("h3")[0].innerHTML.replace(/\D+/g, "")
        return confirmation
    }

    /**
   *
   * @param {string} htmlString html doc in string format
   * Gets an html doc in string format, parse it to an object,
   *      * @returns  text  of  error in  payment 
   *    if   no error   return  null 
    */
    getDocDataErr(htmlString) {
        const dom = new JSDOM(htmlString);
        if (dom.window.document.getElementsByTagName("span")[5]) {
            let err = dom.window.document.getElementsByTagName("span")[5].innerHTML
            return err
        }
        else return null

    }

}




module.exports = PeletalkCrawler;

