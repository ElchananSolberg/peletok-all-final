var soap = require('soap');
const logs = require('../../logger')
const path = require('path');
const PaymentBasic = require('../paymentBasic');
const models = require('../../index');
const Resinfo = require('../../resInfo')
const LM = require('../../config/LanguageManager').LM
const {MongoLogger} = require('../../mongoLogger')

const productPrice = models.productPrice
const productPriceModel = productPrice.model
const product = models.product
const productModel = product.model
var wsdlPath = path.join(__dirname, "wsdl/partner.wsdl")
const dealerId = 12
const comNetId = 1
const paymentType = 1
const digitalSignature = "DIGI_SIGI"
const success=0
const ErrSubscriberNumber=32
/***
 * Payment manager Partner
 *
 */
class Partner extends PaymentBasic {
    constructor(contractNumber, productId, businessId,req) {
        super()
        this.req=req
        this.info = new Resinfo(req)
        this.timeout=3000
        this.productId = productId
        this.businessId = businessId
        this.contractNumber = contractNumber



    }


    

    /***
            * Pay  to partner    
           
            */
    async pay(transactionId) {
        if ((await this.isDemoTransactionUser())) {
            return this.demoRespons()
        }
        else {
        const resPrice = await this.getPrice()
        const price = resPrice.price * 100
        let item = await productModel.findOne(
            {
                where: {id: this.productId}
            })

        let itemName=item["supplier_identity_number"]
        if (!itemName){
            this.info.setErrMessage(LM.getErrorBasic(), "Error in supplier_identity_number ")
            return this.info
        }

        const args = {
            DealerId: dealerId,
            ComNetId: comNetId,
            EndUnitId: this.businessId,
            MSISDN: this.contractNumber,
            TransAmount: price,
            PaymentType: paymentType,
            DigitalSignature: digitalSignature,
            AccountName: itemName
        };



        return this.createSoapRequest(args, transactionId)

    }
}
    /***
* soapRequest
@return (resolve) number  of   confirmation
*/
    async createSoapRequest(args, transactionId) {
        let client = await soap.createClientAsync(wsdlPath);
        return await client.ChargeAsync(args, {timeout: this.timeout})
            .then(result => {
                MongoLogger.logTransaction({
                    transactionId,
                    supplierId: this.req.params.supplierID,
                    requestParams: this.req.body,
                    request:args,
                    response:result,
                })
                if(!(result&&result[0]&&result[0]["ChargeResult"])){
                    //    TODO: Add error to info
                    this.info.error.err_connection_site = true
                    this.info.setErrMessage(LM.getErrorBasic(),"Error in connection ")
                    return this.info;
                }

                const chargeResult = result[0]["ChargeResult"];
                const resultCode = chargeResult["ResultCode"];
                if (resultCode == success) {
                    var res = { "success": chargeResult["TransactionId"] }
                    this.info.confirmation=chargeResult["TransactionId"]
                    logs.info(res)
                    this.info.isPaid = true
                    this.info.send = res
                    return this.info
                }
                else if (resultCode == ErrSubscriberNumber) {
                    var resErr = chargeResult["ResultDesc"]
                    this.info.error. err_in_SubscriberNumber=true
                    this.info.setErrMessage(LM.getString("invalidSubscriber"), resErr)
                    return this.info
                }
                else {
                    var resErr = chargeResult["ResultDesc"]
                    this.info.setErrMessage(LM.getErrorBasic(),resErr)
                    return this.info

                }

            }).catch(err => {
                this.info.error.err_connection_site = true
                this.info.setErrMessage(LM.getErrorBasic(),"Error in connection " + err)
                return this.info;
                })
    
    }

}

module.exports = Partner;
