const BaseModel = require("../../BaseModel");
const DataTypes = require("sequelize").DataTypes;

/**
 * Site model
 */
class Site extends BaseModel {


    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            site_url: {
                type: DataTypes.STRING(256),
                allowNull: false
            },
            class_name: {
                type: DataTypes.STRING(256),
                allowNull: false
            }
        };
        this.prefix = "PAY";
        this.author = true;
    }

    /**
     * creates associate for city model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.hasMany(models.cityPaymentTypeSite.model, { as: "CityPaymentTypeSite"});
    }
}

module.exports = new Site();