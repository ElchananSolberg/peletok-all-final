const BaseModel = require("../../BaseModel");
const DataTypes = require("sequelize").DataTypes;

/**
 * cityPaymentType model
 */
class CityPaymentType extends BaseModel {


    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            pay_name: {
                type: DataTypes.STRING(256),
                allowNull: false
            }
        };
        this.prefix = "PAY";
        this.author = true;
    }

    /**
     * creates associate for cityPaymentType model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.hasMany(models.cityPaymentTypeSite.model, { as: "CityPaymentTypeSite"});
    }
}

module.exports = new CityPaymentType();