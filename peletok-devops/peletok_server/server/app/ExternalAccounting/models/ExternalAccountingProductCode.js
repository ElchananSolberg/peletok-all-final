const DataTypes = require('sequelize').DataTypes;
const BaseModel = require('../../BaseModel');

/**
 * ExternalAccountingProductCodes model
 */
class ExternalAccountingProductCode extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            code_value: {
                type: DataTypes.STRING(32),
                allowNull: true
            }
        }
        this.statusField = true;
        this.prefix = 'EXAC'
    }
    /**
     * creates associate for ExternalAccountingProductCodes model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.externalAccounting.model, { as: 'ExternalAccounting' })
        this.model.belongsTo(models.product.model, { as: 'Product' })

    }
}


module.exports = new ExternalAccountingProductCode();
