

const models = require('../index');
const business = models.business
const businessModel = business.model
const businessFinance = models.businessFinance
const businessFinanceModel = businessFinance.model
const product = models.product
const productMode = product.model
const businessProduct = models.businessProduct
const businessProductMode = businessProduct.model
const supplier = models.supplier
const supplierMode = supplier.model
const {PELETOK_DIST_ID} = require('../constants')

const currency = require('currency.js')
const { VATReduction } = require('../utils')

class BasicCalc {

    constructor(req, price) {
        this.price = price
        this.req = req
    }




    /**
     * check if  business  use_article_20  & calculation VAT of  profit 
     * return  only vat
     */

    async balance(businessId, businessPrice) {
        let article20 = (await businessModel.findOne({ where: { id: businessId }, attributes: ["use_article_20"] })).use_article_20
        let businessBalance = (await businessFinanceModel.findOne({ where: { business_id: businessId }, attributes: ["balance"] })).balance
        let balance
        if (article20) {
            balance = currency(businessBalance).subtract(businessPrice["price"]).subtract(businessPrice["profitVat"]).value

        }
        else {
            balance = currency(businessBalance).subtract(businessPrice["price"]).value
        }
        return balance
    }

    async commitBusinessProfit(profit) {
        let businessPrice = currency(this.price).subtract(profit).value
        let vatProfit = await VATReduction(profit)
        let profitVat = currency(profit).subtract(vatProfit).value
        return { "price": businessPrice, "profit": profit, "profitVat": profitVat }
    }

    

    /**
     * to  calculation   commissions  for   business_price 
     */
    async calculate() {

        let businessPrice = await this.calculateBusinessProfit(this.req.user.business_id)
        let businessBalance = await this.balance(this.req.user.business_id, businessPrice)



        if (!this.req.user.is_distributor && this.req.user.distributor_id != PELETOK_DIST_ID) {
            let distributor = await businessModel.findOne({
                where: {
                    distributor_id: this.req.user.distributor_id,
                    is_distributor: true
                }
            })
            let distributorPrice = await this.calculateBusinessProfit(distributor.id)
            let distributorBalance = await this.balance(distributor.id, distributorPrice)
            return { "businessPrice": businessPrice["price"], "businessProfit": businessPrice["profit"], "businessVat": businessPrice["profitVat"], "businessBalance": businessBalance, "distributorPrice": distributorPrice["price"], "distributorProfit": distributorPrice["profit"], "distributorVat": distributorPrice["profitVat"], "distributorBalance": distributorBalance }
        }
        return { "businessPrice": businessPrice["price"], "businessProfit": businessPrice["profit"], "businessVat": businessPrice["profitVat"], "businessBalance": businessBalance }

    }
}
module.exports = BasicCalc;
