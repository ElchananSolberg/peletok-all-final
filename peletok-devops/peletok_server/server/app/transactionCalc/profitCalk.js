const BasicCalc = require('./baseCalc')
const models = require('../index');

const businessProduct = models.businessProduct
const businessProductMode = businessProduct.model

const productPrice = models.productPrice
const productPriceMode = productPrice.model

const business = models.business
const businessMode = business.model
const currency = require('currency.js')
const Sequelize = require('sequelize');
const Op = Sequelize.Op
const moment = require('moment')

class ProfitCalk extends BasicCalc {
    

    async calculateBusinessProfit(businessId) {

        let profit
        let percent = (await businessProductMode.findOne({ where: { business_id: businessId, product_id: this.req.body.itemId }, attributes: ["percentage_profit"] })).percentage_profit
        let amountWithoutDistributerFee = await this.getDistributingFee(businessId)
        if (amountWithoutDistributerFee) {
            profit = currency(amountWithoutDistributerFee).multiply(percent / 100).value
        }
        else {
            profit = currency(this.price).multiply(percent / 100).value
        }

        return this.commitBusinessProfit(profit)
    }


    async getDistributingFee(businessId) {
        let distributionFee
        let product_fee = (await productPriceMode.findOne({ where: { product_id: this.req.body.itemId,timestamp_end:{
            [Op.gte]: moment()
        } }, attributes: ["distribution_fee"] })).distribution_fee
        if (product_fee > 0) {
            distributionFee = product_fee
            let businessDistFee = (await businessMode.findOne({ where: { id: businessId }, attributes: ["use_distribution_fee"] })).use_distribution_fee
            if (businessDistFee == true) {
                let businessProdDistFee = (await businessProductMode.findOne({ where: { business_id: businessId, product_id: this.req.body.itemId }, attributes: ["distribution_fee"] })).distribution_fee
                if (businessProdDistFee == true) {

                    return currency(this.price).subtract(distributionFee).value
                }
            }


        }

    }
}
module.exports = ProfitCalk;
