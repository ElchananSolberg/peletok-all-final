const models = require('../index.js')
const gift = models.gift
const giftModel = gift.model
const giftOrder = models.giftOrder
const giftOrderModel = giftOrder.model
const { getMappingPath, get, wrapperCreate, create } = require("../mapping/crudMiddleware");
const moment = require('moment')
const businessPoint = models.businessPoint
const businessPointModel = businessPoint.model
const waiting_for_delivery = 1
const in_delivery_processes = 2
const sent = 3
const canceled = 4
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const sequelizeInstance = require('../setupSequelize')
const {createCancelOrderNotification} = require("../message/message_utils");
let transaction;



/**
 * create gift order process in DB
 * @param {*} req 
 * @param {*} res 
 */
async function wrapperGiftOrder(req, res) {

    let mappingPath = getMappingPath(req);
    req.body.author_id = req.user ? req.user.id : null;
    let user = await businessPointModel.findOne({ where: { business_id: req.user.business_id } })
    let infoArray = [];
    let errFound = false;
    req.body['order_date'] = moment().toDate();
    req.body['user_id'] = req.user.id
    req.body['status'] = waiting_for_delivery
    let giftsIds = req.body.gift_id
    for (let i = 0; i < giftsIds.length; i++) {
        const id = giftsIds[i];
        req.body['gift_id'] = id
        let gift = await giftModel.findOne({ where: { id: id } })
        let giftUtilizingPoints = gift['points_required']
        req.body['utilizing_points'] = giftUtilizingPoints
        let info = await create(mappingPath, req.body);
        if (info.errors[0]) {
            errFound = true;
        }
        infoArray.push(info);
        user['sum'] -= req.body['utilizing_points']
    }
    await user.save()

    let status = errFound ? 400 : 200;
    res.status(status).send(infoArray);
}




/**
 * delete gift
 * @param {*} req 
 * @param {*} res 
 */
async function deleteGift(req, res) {
    await giftModel.findOne({
        where: { id: req.params.id }
    }).then(gift => {
        if (gift) {
            gift.destroy().then(info => {
                res.send(info)
            })
        } else {
            res.status(400).send('failed')
        }
    })
}
/**
 * cancel gift order by gift and author id
 * @param {*} req 
 * @param {*} res 
 */
async function cancelGiftOrder(req, res) {
    transaction = await sequelizeInstance.transaction();
    let error = null
    let giftsIds = req.body['gift_id']
    for (let i = 0; i < giftsIds.length; i++) {
        const id = giftsIds[i];
        try {
            let currentGiftOrder = await giftOrderModel.findOne({
                where: { gift_id: id, author_id: req.user.id, status: waiting_for_delivery }, include: [{
                    model:models.gift.model,
                    as: 'Gift' 
                }]
            }, { transaction });
            let giftUtilizing_points = currentGiftOrder.Gift.points_required
            let user = await businessPointModel.findOne({ where: { business_id: req.user.business_id } }, { transaction })
            user['sum'] += giftUtilizing_points
            currentGiftOrder['status'] = canceled;
            currentGiftOrder['status_author_id'] = req.user.id
            currentGiftOrder['cancel_date'] = moment().toDate();
            currentGiftOrder['status_timestamp'] = moment().toDate();
            await currentGiftOrder.save({ transaction })
            await user.save({ transaction })
            await createCancelOrderNotification(currentGiftOrder.user_id, currentGiftOrder.Gift.name, currentGiftOrder.id);
        } catch (err) {
            error = err
        }

    }

    if (error) {
        await transaction.rollback();
        res.status(401).send(error.toString())
    } else {
        await transaction.commit();
        res.send('success')
    }

}



module.exports.wrapperGiftOrder = wrapperGiftOrder
module.exports.deleteGift = deleteGift
module.exports.cancelGiftOrder = cancelGiftOrder

