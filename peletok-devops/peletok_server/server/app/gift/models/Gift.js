var BaseModel = require('../../BaseModel.js');
const DataTypes = require("sequelize").DataTypes;


/**
 * Gift address model
 */

class Gift extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            name: {
                type: DataTypes.STRING(150),
                allowNull: false
            },
            description: {
                type: DataTypes.STRING(150),
                allowNull: true
            },
            points_required: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            picture: {
                type: DataTypes.STRING(150),
                allowNull: true
            }
        };
        this.options["indexes"] = [
            {
                unique: false,
                fields: ['status']
            }
            ];

        this.author = true;
        this.statusField = true;
        this.statusAuthor = true;
        this.statusTimestampField = true;
        this.prefix = "GFT";
    }

    /**
     * creates associate for Gift model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.hasMany(models.giftOrder.model, { as: 'GiftOrder'});
    }
}

module.exports = new Gift();
