var BaseModel = require('../../BaseModel.js');
const {createOrderGiftNotification} = require("../../message/message_utils");
const DataTypes = require("sequelize").DataTypes;

const logger = require("../../logger");


/**
 * GiftOrder address model
 */

class GiftOrder extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            order_date: {
                type: DataTypes.DATE,
                allowNull: false
            },
            cancel_date: {
                type: DataTypes.DATE,
                allowNull: true
            },
            delivery_date: {
                type: DataTypes.DATE,
                allowNull: true
            },
            utilizing_points: {
                type: DataTypes.INTEGER,
                allowNull: false
            },
            address: {
                type: DataTypes.STRING,
                allowNull: false
            }
        };
        this.options["indexes"] = [
            {
                unique: false,
                fields: ['status']
            }
        ];
        this.author = true;
        this.statusField = true;
        this.statusAuthor = true;
        this.statusTimestampField = true;
        this.prefix = "GFT";
    }

    /**
     * creates associate for GiftOrder model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.gift.model, { as: 'Gift' });
        this.model.belongsTo(models.user.model, { as: 'User' });
    }
}

let giftOrder = new GiftOrder();

giftOrder.model.afterCreate(async (orderGift, options)=>{
    let giftName = orderGift.Gift?orderGift.Gift.name:(await orderGift.getGift({attributes: ["name"]})).name;
    await createOrderGiftNotification(orderGift.user_id, giftName, orderGift.id).catch(e=>logger.error(e));
});

module.exports = giftOrder;
