const BaseModel = require('../../BaseModel.js');
const DataTypes = require("sequelize").DataTypes;
const moment = require('moment');
    
/**
 * BusinessPoint model
 */

class BusinessPoint extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            sum: {
                type: DataTypes.INTEGER,
                allowNull: false
            },
            comment: {
                type: DataTypes.TEXT,
                allowNull: true
            }
        };
        this.author = true;
        this.prefix = "GFT";
    }

    /**
     * creates associate for BusinessPoint model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.business.model, { as: 'Business' });
    }
}

const businessPoint = new BusinessPoint();

businessPoint.model.afterSave(async (instance, options)=> {
    const businessPointLogModel = require("../../index").businessPointLog.model;
    let timestampStart = moment().toDate();
    const dataValues = {...instance.dataValues};
    delete dataValues.id;
    const optionsForOldRow = {
        where: {
            business_id: dataValues.business_id,
            timestamp_end: null
        },
    };
    if (options.transaction) {
        optionsForOldRow.transaction = options.transaction
    }
    await businessPointLogModel.update({timestamp_end: timestampStart}, optionsForOldRow).catch(async e => {
        if (options.transaction) {
            logger.error(e);
            await transaction.rollback();
        }
    });
    const optionsForNewRow = {};
    if (options.transaction) {
        optionsForNewRow.transaction = options.transaction
    }
    await businessPointLogModel.create({...dataValues, timestamp_start: timestampStart}, optionsForNewRow).catch(async e=>{
        if(options.transaction){
            logger.error(e);
            await transaction.rollback();
        }
    });
});

module.exports = businessPoint;
