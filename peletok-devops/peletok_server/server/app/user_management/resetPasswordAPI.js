var express = require('express');
var router = express.Router();
const LM = require('../config/LanguageManager').LM;
const { getPhoneNumber, isUserExist, checkVerificationCode, changePasswordWithRycoveryCode } = require('./user_management_utils');
const smsSender = require('../client_messaging/smsSender')
const { generateValidationCode } = require('../utils')
const suffixPhoneNumber = '/suffix_phone';
const checkPhoneNumber = '/check_phone';
const checkUser = '/check_user';
const checkVerificationCodePath = '/check_verification_code';
const resetWithVarificationCodePath = '/reset_with_verification_code';
const smsSentSuccessfullyStatus = 1
const phoneNumberNotExistStatus = -6
const wrongNumberStatus = -17
const util = require('util')
const models = require('../index')
const moment = require('moment')
let compatibleCodeStatus = 1
let codeExpiredStatus = -1
let WrongCodeStatus = -2



/**
 * 
 */
router.post(checkVerificationCodePath, async function (req, res) {
    let isVerified = (await checkVerificationCode(req.query.username, req.query.verificationCode)).toString()
    res.status(200).send(isVerified)
})
/***
 * Get suffix phone number for forgot password
 */
router.get(suffixPhoneNumber, function (req, res) {
    getPhoneNumber(req.query.username).then(number => {
        if (number) {
            res.status(200).send(number.slice(-3))
        } else {
            res.status(401).send(LM.getString("phoneNuberErrMsg"))
        }
    })
});

/***
 * checks matching of phone number in DB with phone number from a query
 */
router.get(checkPhoneNumber, function (req, res) {
    getPhoneNumber(req.query.username).then(async number => {
        if (number) {
            if (number === req.query.phone_number) {
                let verificationCode = generateValidationCode()
                let currentUser = await models.user.model.findOne({ where: { login_name: req.query.username } })
                await models.userPassword.model.update({ verification_code: verificationCode, verification_code_expiry: (moment().add(5, 'minutes')) }, { where: { user_id: currentUser.id } })
                let smsProviderRes = await smsSender.send(number, `Your verification code is: ${verificationCode}`)
                if (smsProviderRes.Status == smsSentSuccessfullyStatus) {
                    res.send('success')
                } else {
                    switch (smsProviderRes.Status) {
                        case phoneNumberNotExistStatus:
                            res.status(400).send({ message: LM.getString("phoneNumberNotExist"), response: util.inspect(smsProviderRes, { showHidden: false, depth: null }) })
                            break;

                        case wrongNumberStatus:
                            break;

                        default:
                            res.status(400).send({ message: LM.getString("smsFailed"), response: util.inspect(smsProviderRes, { showHidden: false, depth: null }) })
                            break;
                    }
                }
            } else {
                res.status(401).send({ message: LM.getString("PhoneDidNotMatchTheNumberRegistered"), response: 'failed' })
            }

        } else {
            res.status(400).send({ message: LM.getString("phoneNuberErrMsg"), response: 'failed' });
        }
    });
});

/***
 * checks if user is in DB from a query
 */
router.get(checkUser, function (req, res) {
    isUserExist(req.query.username).then(user => {
        if (user) {
            res.status(200).send(true);
        } else {
            res.status(401).send(LM.getString("noUserErrMsg"));
        }
    });
});

/**
 * posts new password if varification code has not expiered
 */
router.post(resetWithVarificationCodePath, async function (req, res) {

    await changePasswordWithRycoveryCode(req, res).toString()
})

module.exports = router