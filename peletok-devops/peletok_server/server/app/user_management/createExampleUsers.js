const {createUsers} = require('./user_management_utils');

//TODO: drop this file
/**
 * Creates a number of users for example
 */
function createExampleUsers() {
    createUsers("peletok1", 'pass1', "000000018");
    createUsers("peletok2", 'pass2', "000000026");
    createUsers("peletok3", 'pass3', "000000034");
    createUsers("peletok4", 'pass4', "000000042");
    createUsers("peletok5", 'pass5', "000000059");
}

module.exports = createExampleUsers;
