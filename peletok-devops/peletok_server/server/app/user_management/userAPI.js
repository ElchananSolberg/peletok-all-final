const express = require('express');
const router = express.Router();
const userDocumentRouter = express.Router();
const models = require('../index.js');
const { createUserFromBody } = require('./user_management_utils');
const { updateUser, getPasswordChangHistory, getBalanceUpdateApprovers } = require('./user_management_utils');
const changePassword = require('./user_management_utils').changePassword
const { exist } = require('../utils');
const { GenericGet } = require('../mapping/GenericGet');
const removeAbsolutePath = require("../utils").removeAbsolutePath;
const generalPost = require("../mapping/genericAPI").generalPost
const { Info } = require("../mapping/crudMiddleware");
const logs = require("../logger.js")
const { uploadFile } = require("../utils");
const { getNumStatus } = require("../utils");
const { deleteSessionByUser } = require("../utils");
const { wrapperCreate, wrapperGet, wrapperCreateUser, getMappingPath } = require("../mapping/crudMiddleware");
const { getUserInformation } = require('./user_management_utils')
const path = require("path");
const { changePasswordForConnectedUser, changePasswordForDifferentUser } = require("./user_management_utils");
const { deleteUserDocument } = require("./user_management_utils");
const { checkIfModelFiledExists } = require("./user_management_utils");


const user = models.user;
const userModel = user.model;
const userPassword = models.userPassword
const userAddress = models.userAddress
const address = models.address
const addressModel = address.model
const userPasswordModel = userPassword.model
const userAddressModel = userAddress.model

const allUsersAPI = '/';
const specificUserAPI = '/:id';
const specificUserBaseDetailsAPI = '/details/';
const generalInformationAPI = '/generalInformation';
const userDetails = '/user_details';
const {PELETOK_BUSINESS_ID} = require('../constants')



router.get("/valid_fields/", async (req, res) => {
    let doesExist = await checkIfModelFiledExists(req.query.model, req.query.field, req.query.value);
    res.status(200).send(doesExist)
});

/**
 * Check if user is authenticated
 * @param req: request
 * @param res: response
 * @param next: express next
 * @returns express next or void
 */
function isAuthMiddleware(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    res.send("Unauthorized");
}
// get all user with business balance update permission under user business_id
router.get('/balance_approval/', async function (req, res) {
    await getBalanceUpdateApprovers(req, res)
});
// get password change history
router.get('/password_change_history/', async function (req, res) {
    await getPasswordChangHistory(req, res)
});

// get user general information
router.get(generalInformationAPI, async function (req, res) {
    req.params.id = req.user ? req.user.id : null;
    await wrapperGet(req, res);
});

// Get all users
router.get(allUsersAPI, wrapperGet);

router.get(userDetails, async function (req, res) {
    req.params.id = req.user ? req.user.id : null;
    await getUserInformation(req, res);
});

router.get(specificUserBaseDetailsAPI, async (req, res) => {
    let info = new Info();
    info.result = { first_name: req.user.first_name, last_name: req.user.last_name, login_name: req.user.login_name };
    info.result.phone_number = req.user.UserContact[0] ? req.user.UserContact[0].phone_number : null;
    info.result.last_login = req.user.LoginHistory[0] ? req.user.LoginHistory[0].last_login : null;
    res.customSend(info);
})
    ;

// Create new user
router.post(allUsersAPI, async (req, res) => {
    let info = new Info();
    if (!req.body.business_id) {
        req.body.business_id = req.user.business_id;
    }
    let isCurrentUserIsTheDist =( await models.business.model.count({
        where: {is_distributor: true, id: req.user.business_id},
        include: [
            {
                model: models.distributor.model,
                as: 'Distributor',
                include: [
                    {
                        model: models.business.model,
                        where: {id: req.body.business_id},
                        as: 'Business',
                        required: true
                    }
                ]
        }
        ]
    })) > 0
    if (req.body.business_id == req.user.business_id || req.user.isPtAdmin || req.user.business_id == PELETOK_BUSINESS_ID || isCurrentUserIsTheDist) {
        let currentUserPermission = await models.user.model.findOne({
            include: [{
                model: models.role.model,
                attributes: [],
                include: [{
                    model: models.permission.model,
                    where: {
                        path: 'GET /sensitive_roles',
                    },
                    attributes: [],
                    required: true,
                    through: {
                        attributes: [],
    
                    }
                }],
                required: true
            }],
            where: { id: req.user.id }
        })
        if (!currentUserPermission && req.body.role_id == 1 || !currentUserPermission && req.body.role_id == 2) {
            info.errors.push(new Error('Unauthorized action'))
            res.customSend(info);
        } else {
            let mappingPath = getMappingPath(req);
            let data = req.body
            let info = await wrapperCreateUser(mappingPath, data).catch(e => { info.errors.push(e) })
            if (info.message.message == 'success' && data.role_id) {
                let new_user = await models.user.model.findOne({ where: { id: info.message.id } })
                await new_user.setRoles([data.role_id])
                res.customSend(new_user)
            } else {
                res.customSend(info)
            }
        }
    }
    else {
        info.errors.push(new Error('Unauthorized action'))
        res.customSend(info);
    }
});

// Get user 
router.get(specificUserAPI, wrapperGet);

// Delete user
router.delete(specificUserAPI, function (req, res) {
    userModel.findOne({
        where: { id: req.params.id }
    }).then(u => {
        var found = exist(u, res);
        if (found) {
            u.destroy().then(info => {
                deleteSessionByUser(req.sessionStore.sessions, u.id);
                res.send(info)
            })
        }
    })
});

//Change password ---- Admin
router.put(`/change_password/:id`, async (req, res) => {
    await changePassword(req, res)

});

//change password (user to self)
router.put("/password/", async (req, res) => {
    //  TODO make sure only the parent distributor can change the descendant users password
    if (req.body.user_id) {
        changePasswordForDifferentUser(req, res)
    } else {
        changePasswordForConnectedUser(req, res)
    }
});


// Update user
router.put(specificUserAPI, async (req, res) => {
    let info = new Info();
    info = await updateUser(req, res).catch(e => {
        logs.error(e)
        res.status(401).send("failed")
        return
    })
    if (req.body.role_id) {
        let updated_user = await models.user.model.findOne({where:{id: req.params.id}})
        await updated_user.setRoles([req.body.role_id])
        res.customSend(info);
    } else {
        res.customSend(info);
    }
});

const active = "active";

router.post("/block/:user_id", async (req, res) => {
    const blocked = "blocked";
    const blockedId = getNumStatus(blocked, "User");
    models.user.model.update({ status: blockedId }, { where: { id: req.params.user_id } }).then(() => {
        res.status(200).send("success");
        deleteSessionByUser(req.sessionStore.sessions, Number(req.params.user_id));
    }
    ).catch(err => {
        logs.error(err);
        res.status(401).send("failed");
    })

});

router.post("/frozen/:user_id", async (req, res) => {
    const frozen = "frozen";
    const frozenId = getNumStatus(frozen, "User");
    models.user.model.update({ status: frozenId }, { where: { id: req.params.user_id } }).then(() => {
        res.status(200).send("success");
        deleteSessionByUser(req.sessionStore.sessions, Number(req.params.user_id));
    }
    ).catch(err => {
        logs.error(err);
        res.status(401).send("failed");
    })

});

router.post("/activate/:user_id", async (req, res) => {
    const activeId = getNumStatus(active, "User");
    models.user.model.update({ status: activeId }, { where: { id: req.params.user_id } }).then(() => {
        res.status(200).send("success");
    }
    ).catch(err => {
        logs.error(err);
        res.status(401).send("failed");
    })

});

userDocumentRouter.get("/:id", async (req, res) => {
    await wrapperGet(req, res, null, true, { "user_id": req.params.id });
});

userDocumentRouter.delete("/:id", deleteUserDocument);

userDocumentRouter.post("/:id", async (req, res) => {
    const userImageDir = path.join(__dirname, "../rest/restData/public/user/");
    const fs = require("fs");
    let data = await uploadFile(req, userImageDir);
    data.user_id = req.params.id;
    await wrapperCreate(req, res, null, data);
});

module.exports.userRouter = router;
module.exports.userDocumentRouter = userDocumentRouter;
module.exports.isAuthMiddleware = isAuthMiddleware;
