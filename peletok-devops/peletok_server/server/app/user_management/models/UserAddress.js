var BaseModel = require('../../BaseModel.js');

/**
 * User address model
 */

class UserAddress extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.author = true;
        this.statusField = true;
        this.statusTimestampField = true;
        this.prefix = "USR";
    }

    /**
     * creates associate for user address model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.user.model, { as: 'User' });
        this.model.belongsTo(models.address.model, { as: 'Address' });
    }
}

module.exports = new UserAddress();
