var BaseModel = require('../../BaseModel.js');
const DataTypes = require("sequelize").DataTypes;

/**
 * UserStatusMessage model
 */
class UserStatusMessage extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            message: {
                type: DataTypes.STRING,
                allowNull: true
            }
        };
        this.options["indexes"] = [
            {
                unique: false,
                fields: ['status']
            }
        ];
        this.prefix = 'USR';
        this.statusField = true;
    }
    /**
     * creates associate for userStatusMessage model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.language.model, { as: 'Language' })
    }


}
//exporting instance
module.exports = new UserStatusMessage();