var BaseModel = require('../../BaseModel.js');
const DataTypes = require("sequelize").DataTypes;
const israeliIdValidator = require("../../utils").israeliIdValidator;

/**
 * User model
 */
class UserActivityLog extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            path: {
              type: DataTypes.TEXT,
            },
            full_url: {
              type: DataTypes.TEXT,
            },
            query: {
              type: DataTypes.TEXT,
            },
            params: {
              type: DataTypes.TEXT,
            },
            body: {
              type: DataTypes.TEXT,
            },
            user: {
              type: DataTypes.TEXT,
            },
            ip: {
              type: DataTypes.STRING,
            },
        };
        
        this.statusTimestampField = true;
        this.prefix = 'USR';
    }

    /**
     * creates associate for user model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        
    }
}


module.exports = new UserActivityLog();

