var BaseModel = require('../../BaseModel.js');
const DataTypes = require("sequelize").DataTypes;

/**
 * UserIp model
 */
class UserIp extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            ip_address: {
                type: DataTypes.STRING(24),
                allowNull: true
            }
        };

        this.prefix = 'USR'

    }
     /**
     * creates associate for IP model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.user.model, { as: 'User' });        
    } 


}

module.exports = new UserIp();