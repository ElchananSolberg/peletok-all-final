var BaseModel = require('../../BaseModel.js');
var DataTypes = require('sequelize').DataTypes;
var { addHashWithSalt } = require('../../utils')

/**
 * UserPassword model
 */
class UserPassword extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            password: {
                type: DataTypes.STRING(),
                allowNull: true
            },
            ip: {
                type: DataTypes.STRING(36),
                allowNull: true
            },
            verification_code: {
                type: DataTypes.STRING(6),
                allowNull: true
            },
            verification_code_expiry: {
                type: DataTypes.DATE,
                allowNull: true
            }
        };
        this.options["indexes"] = [{
            unique: false,
            fields: ['user_id']
        }];
        this.statusAuthor = true;
        this.statusField = true;
        this.statusTimestampField = true;
        this.prefix = 'USR';
    }

    /**
     * creates associate for user model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.user.model, { as: 'User' });
    }
}
const userPassword = new UserPassword();

userPassword.model.beforeCreate(async (user) => {
    if(user.password && user.password != ""){
        user.password = await addHashWithSalt(user.password);
        return user 
    }
});

module.exports = userPassword;