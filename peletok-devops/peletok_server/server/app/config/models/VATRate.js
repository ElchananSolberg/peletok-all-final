const BaseModel = require('../../BaseModel');
const DataTypes = require("sequelize").DataTypes;
/**
 * VATRate model
 */
class VATRate extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            value: {
                type: DataTypes.DOUBLE,
                allowNull: true
            },
            start_timestamp: {
                type: DataTypes.DATE,
                allowNull: true
            }
        };
        this.prefix = "CONF";
    }

}

module.exports = new VATRate();