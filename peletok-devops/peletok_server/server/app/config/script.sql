create table mapping
(
  id            serial      not null
    constraint mapping_pk
      primary key,
  path          varchar(32) not null,
  key           varchar(32) not null,
  "table"       varchar(32) not null,
  through       varchar(256),
  role_required integer
);

alter table mapping
  owner to postgres;

create unique index mapping_id_uindex
  on mapping (id);


