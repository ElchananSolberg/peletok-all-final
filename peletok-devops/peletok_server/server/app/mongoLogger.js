const mongoose = require('mongoose');
const {config} = require('./config/config');
const {parse, stringify} = require('flatted/cjs');
const conn = config.mongo_conn || process.env["MONGO_CONN"];

let transactionDataModelModel;



class MongoLogger{
    constructor(){
        this.models =  new Promise((resolve, reject)=>{
            this.connection = mongoose.createConnection(conn, {useNewUrlParser: true, useUnifiedTopology: true})
            const TransactionDataSchema = new mongoose.Schema({
                transactionId: Number,
                supplierId: Number,
                requestParams: mongoose.Schema.Types.Mixed,
                request: mongoose.Schema.Types.Mixed,
                response: mongoose.Schema.Types.Mixed,
                data: mongoose.Schema.Types.Mixed,
            }, { timestamps: true })

            const TransactionData = this.connection.model('TransactionData', TransactionDataSchema)

            const TransactionErrorSchema = new mongoose.Schema({
                transactionId:  Number,
                activityId:  Number,
                userId:  Number,
                businessId:  Number,
                error: mongoose.Schema.Types.Mixed,
            }, { timestamps: true })
            
            const TransactionError = this.connection.model('TransactionError', TransactionErrorSchema)
            

            const ResponseSchema = new mongoose.Schema({
                transactionId:  Number,
                activityId:  Number,
                userId:  Number,
                businessId:  Number,
                body: mongoose.Schema.Types.Mixed,
            }, { timestamps: true })
            
            const Response = this.connection.model('Response', ResponseSchema)

            const ConsoleErrorSchema = new mongoose.Schema({
                env:  String,
                transactionId:  Number,
                activityId:  Number,
                userId:  Number,
                businessId:  Number,
                error: mongoose.Schema.Types.Mixed,
            }, { timestamps: true })
            
            const ConsoleError = this.connection.model('ConsoleError', ConsoleErrorSchema)

            const HttpRequestErrorSchema = new mongoose.Schema({
                request: mongoose.Schema.Types.Mixed,
                response: mongoose.Schema.Types.Mixed,
            }, { timestamps: true })
            
            const HttpRequestError = this.connection.model('HttpRequestError', HttpRequestErrorSchema)
            console.log('connected')

            resolve({TransactionData, TransactionError, Response, ConsoleError, HttpRequestError})
        
        })
    }

    logTransaction(data){
        this.models.then(({TransactionData})=>{
            try {
                (new TransactionData(data)).save(function(err, doc){
                    if (err) {
                        console.log('logTransaction-Error:',err);
                    }
                })
            } catch (e){
                console.log(e)
            }
        })
    }

    logTransactionErrors(data){
        this.models.then(({TransactionError})=>{
            try {
                (new TransactionError(data)).save(function(err, doc){
                    if (err) {
                        console.log('logTransactionErrors-Error:',err);
                    }
                })
            } catch (e){
                console.log(e)
            }
        })
    }

    logResponses(data){
        this.models.then(({Response})=>{
            try {
                (new Response(data)).save(function(err, doc){
                    if (err) {
                        console.log('logResponses-Error:',err);
                    }
                })
            } catch (e){
                console.log(e)
            }
        })
    }

    logConsoleError(data){
        this.models.then(({ConsoleError})=>{
            try {
                (new ConsoleError(data)).save(function(err, doc){
                    if (err) {
                        console.log('logConsoleError-Error:',err);
                    }
                })
            } catch (e){
                console.log(e)
            }
        })
    }
    logHttpRequestError(data){
        this.models.then(({HttpRequestError})=>{
            try {
                (new HttpRequestError(data)).save(function(err, doc){
                    if (err) {
                        console.log('logHttpRequestError-Error:',err);
                    }
                })
            } catch (e){
                console.log(e)
            }
        })
    }
}

let mongoLogger = new MongoLogger;
module.exports.MongoLogger = mongoLogger
