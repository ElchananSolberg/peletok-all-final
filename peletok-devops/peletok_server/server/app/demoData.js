//TODO: Remove this data
let supplier1 = {name: "פלפון", type: "advance", id: 1};
let supplier2 = {name: "סלקום", type: "advance", id: 2};
let supplier3 = {name: "פרטנר", type: "advance", id: 3};

let supplier4 = {name: "טעינת חשמל", type: "payment", id: 4};
let supplier5 = {name: "כביש 6", type: "payment", id: 5};

const suppliers = {
    advance: [supplier1, supplier2, supplier3],
    payment: [supplier4, supplier5]};

let price = " 75 שח";
let prod1 = {name: "virtual " + "טוקמן 1000 דקות שיחה גלישה 150 NB" + price, type: "virtual", id: 1};
let prod2 =  {name: "virtual " + "טוקמן שיחה ללא הגבלה גלישה 100 NB" + price, type: "virtual", id: 2};
let prod3 = {name: "manual " + "טוקמן 1000 דקות שיחה גלישה 150 NB" + price, type: "manual", id: 3};
let prod4 =  {name: "manual " + "טוקמן שיחה ללא הגבלה גלישה 100 NB" + price, type: "manual", id: 4};

const products = {
    virtual: [prod1, prod2],
    manual: [prod3, prod4]};

module.exports.suppliers = suppliers;
module.exports.products = products;

