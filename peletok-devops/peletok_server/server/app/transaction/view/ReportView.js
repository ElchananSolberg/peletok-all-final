const BaseViewModel = require('../../BaseViewModel');
const DataTypes = require("sequelize").DataTypes;
// const { getSyncReportCommand, getDropReportCommand } = require("./sqlQuery");

/**
 * ReportView model
 */
class ReportView extends BaseViewModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            transaction_start_timestamp: {
                type: DataTypes.DATE,
                allowNull: true
            },
            transaction_end_timestamp: {
                type: DataTypes.DATE,
                allowNull: true
            },
            business_name: {
                type: DataTypes.STRING(128),
                allowNull: true
            },
            product_name: {
                type: DataTypes.STRING,
                allowNull: true
            },
            parent_transaction_id: {
                type: DataTypes.INTEGER
            },
            product_id: {
                type: DataTypes.INTEGER
            },
            supplier_id: {
                type: DataTypes.INTEGER
            },
            terminal_id: {
                type: DataTypes.INTEGER
            },
            business_id: {
                type: DataTypes.INTEGER
            },
            user_id: {
                type: DataTypes.INTEGER
            },
            price: {
                type: DataTypes.DOUBLE,
                allowNull: true
            },
            payment: {
                type: DataTypes.DOUBLE,
                allowNull: true
            },
            action: {
                type: DataTypes.STRING(256),
                allowNull: true
            },
            supplier_name: {
                type: DataTypes.STRING(256),
                allowNull: true
            },
            city: {
                type: DataTypes.STRING(256),
                allowNull: true
            },
            branch_business_id: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            cities_item_id: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            bill_number: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            client_id: {
                type: DataTypes.STRING,
                allowNull: true
            },
            contract_number: {
                type: DataTypes.STRING,
                allowNull: true
            },
            phone_number: {
                type: DataTypes.STRING,
                allowNull: true
            },
            item_id: {
                type: DataTypes.STRING,
                allowNull: true
            },
            customer_id: {
                type: DataTypes.STRING,
                allowNull: true
            },
            card_number: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            card_code: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            distributor_name: {
                type: DataTypes.STRING,
                allowNull: true
            }
            ,
            distributor_id: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            invoice_number: {
                type: DataTypes.STRING,
                allowNull: true
            },
            business_status: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            price_not_including_vat: {
                type: DataTypes.DOUBLE,
                allowNull: true
            },
            client_name: {
                type: DataTypes.STRING,
                allowNull: true
            },
            client_address: {
                type: DataTypes.STRING,
                allowNull: true
            },
            contact_identification: {
                type: DataTypes.STRING,
                allowNull: true
            },
            confirmation: {
                type: DataTypes.STRING,
                allowNull: true
            },
            reference_number: {
                type: DataTypes.STRING,
                allowNull: true
            }, city_pay_city: {
                type: DataTypes.STRING,
                allowNull: true

            }, transaction_type: {
                type: DataTypes.INTEGER,
            },
            appeal_id: {
                type: DataTypes.STRING,
                allowNull: true
            },
            distributor_profit: {
                type: DataTypes.DOUBLE,
                allowNull: true
            },
            business_profit: {
                type: DataTypes.DOUBLE,
                allowNull: true
            },
            is_cancelable:{
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: true
            },
            cancel_process_status: {
                // Refers to the transaction that you want to cancel
                // (not necessarily the current transaction)
                type: DataTypes.ENUM(["NO", "AWAITING", "DONE", "REJECT"]),
                allowNull: false,
                defaultValue: "NO"
            },
            business_balance: {
                type: DataTypes.DOUBLE,
                allowNull: true
            },
            distributor_balance: {
                type: DataTypes.DOUBLE,
                allowNull: true
            },
            comment: { // comment for user from external service
                type: DataTypes.TEXT,
                allowNull: true
            },
            external_transaction_id: { // transaction_id from external service (for cancellation)
                type: DataTypes.STRING,
                allowNull: true
            },
            bezek_phone_number: { // phone_number in bezek account
                type: DataTypes.STRING,
                allowNull: true
            },
            bezek_client_id: { // client id in bezek account
                type: DataTypes.STRING,
                allowNull: true
            },
            supplier_balance: {
                type: DataTypes.DOUBLE,
                allowNull: true
            },
            agent_first_name: {
                type: DataTypes.STRING,
                allowNull: true
            },
            agent_last_name: {
                type: DataTypes.STRING,
                allowNull: true
            },
            peletok_price: {
                type: DataTypes.DOUBLE,
                allowNull: true
            },
            business_identifier: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            is_canceled: {
                type: DataTypes.BOOLEAN,
                allowNull: true
            },
            ip: {
                type: DataTypes.STRING,
                allowNull: true
            },
            distributor_price: {
                type: DataTypes.DOUBLE,
                allowNull: true
            },
            business_price: {
                type: DataTypes.DOUBLE,
                allowNull: true
            },
            business_points: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            business_tags_id: {
                type: DataTypes.DECIMAL,
                allowNull: true
            },
            product_type: {
                type: DataTypes.INTEGER,
                allowNull: true
            }

        };
        this.prefix = "REP";
        this.author = true;
        this.statusAuthor = true;
        this.statusField = true;

        // this.getSyncCommand = getSyncReportCommand;
        // this.getDropCommand = getDropReportCommand;


    }
}

module.exports = new ReportView();