const BaseModel = require('../../BaseModel');
const DataTypes = require("sequelize").DataTypes;
/**
 * TransactionLog model
 */
class TransactionLog extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            supplier_response: {
                type: DataTypes.STRING,
                allowNull: true
            },
            description: {
                type: DataTypes.STRING,
                allowNull: true
            },
            request_document_id: {
                type: DataTypes.STRING,
                allowNull: true
            },
            response_document_id: {
                type: DataTypes.STRING,
                allowNull: true
            },
            request_sent_timestamp: {
                type: DataTypes.DATE,
                allowNull: true
            },
            response_receive_timestamp: {
                type: DataTypes.DATE,
                allowNull: true
            }
        };
        this.prefix = "TRNSC";
        this.author = true;
        this.statusAuthor = true;
        this.statusField = true;

    }

    associate(models){
        this.model.belongsTo(models.transaction.model, { as: 'Transaction' })

    }

}

module.exports = new TransactionLog();
