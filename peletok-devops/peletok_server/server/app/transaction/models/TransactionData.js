const BaseModel = require('../../BaseModel');
const DataTypes = require("sequelize").DataTypes;
/**
 * TransactionData model
 */
class TransactionData extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            cities_item_id: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            bill_number: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            client_id: {
                type: DataTypes.STRING,
                allowNull: true
            },
            price: {
                type: DataTypes.DOUBLE,
                allowNull: true
            },
            contract_number: {
                type: DataTypes.STRING,
                allowNull: true
            },
            phone_number: {
                type: DataTypes.STRING,
                allowNull: true
            },
            item_id: {
                type: DataTypes.STRING,
                allowNull: true
            },
            customer_id: {
                type: DataTypes.STRING,
                allowNull: true
            },
            card_number: {
                type: DataTypes.STRING,
                allowNull: true
            },
            card_code: {
                type: DataTypes.STRING,
                allowNull: true
            },
            business_credit: {
                type: DataTypes.DOUBLE,
                allowNull: true
            },
            request_ip: {
                type: DataTypes.STRING,
                allowNull: true
            },
            client_name: {
                type: DataTypes.STRING,
                allowNull: true
            },
            client_address: {
                type: DataTypes.STRING,
                allowNull: true
            },
            contact_identification: {
                type: DataTypes.STRING,
                allowNull: true
            },
            confirmation: {
                type: DataTypes.STRING,
                allowNull: true
            },
            reference_number: {
                type: DataTypes.STRING,
                allowNull: true
            },
            city_pay_city: {
                type: DataTypes.STRING,
                allowNull: true
            },
            city_pay_type: {
                type: DataTypes.STRING,
                allowNull: true
            },
            external_transaction_id: { // transaction_id from external service (for cancellation)
                type: DataTypes.STRING,
                allowNull: true
            },
            comment: { // comment for user from external service
                type: DataTypes.TEXT,
                allowNull: true
            },
            bezek_phone_number: { // phone_number in bezek account
                type: DataTypes.STRING,
                allowNull: true
            },
            bezek_client_id: { // client id in bezek account
                type: DataTypes.STRING,
                allowNull: true
            },
            ip: { 
                type: DataTypes.STRING,
                allowNull: true
            },
            vat: { 
                type: DataTypes.STRING,
                allowNull: true
            },
            points: { 
                type: DataTypes.STRING,
                allowNull: true
            },
            business_identifier: { 
                type: DataTypes.STRING,
                allowNull: true
            },
            cancellation_note: { 
                type: DataTypes.STRING,
                allowNull: true
            }
        };
        this.prefix = "TRNSC";
        this.author = true;
        this.statusAuthor = true;
        this.statusField = true;

    }

    associate(models) {
        this.model.belongsTo(models.transaction.model, { as: 'Transaction' })

    }

}

module.exports = new TransactionData();
