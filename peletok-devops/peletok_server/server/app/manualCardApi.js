const models = require('./index');
const Info = require('../app/resInfo')
const LM = require('../app/config/LanguageManager').LM;


const manualCard = models.manualCard
const manualCardModel = manualCard.model
const productPrice = models.productPrice
const productPriceModel = productPrice.model
const Used = 2
const unUsed = 1

/***
 * Payment manager manual  card
 */
class ManualCardApi {
    constructor(productId) {
        this.productId = productId
        this.info = new Info()
    }


    async getPrice() {


        const product = await productPriceModel.findOne(
            {
                where: { product_id: this.productId }
            })
        if (!product) {
            this.info.setErrMessage(LM.getErrorBasic(), "ID  manual Card  is  null")
            this.info.err_in_productId = true
            return this.info
        }
        return product["fixed_price"]
    }
    /***
     * Payment manual  card
     */
    async pay() {

     

        let card = await manualCardModel.findOne(
            {
                where: { product_id: this.productId, status: unUsed }
            }
        )
        if (!card) {
            this.info.setErrMessage(LM.getString('out_of_manual_cards'), "ManualCard is not available")
            this.info.error.cardNotAvailable = true
            return this.info
        }
   
        const res = {
            "cardNumber": card["card_number"],
            "code": card["code"],
            "expiry_data": card["expiry_date"]
        }
     /***
         * change  in DB status  of manualCard to  used
         */
        card['status'] = Used
        card.save()

        return res

    }
}

module.exports = ManualCardApi 
