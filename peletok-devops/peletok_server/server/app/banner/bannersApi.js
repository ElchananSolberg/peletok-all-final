const models = require('../index.js')

const bannerModel = models.banner.model
const bannerDistributorModel = models.bannerDistributor.model
const bannerLanguageModel = models.bannerLanguage.model
const bannerDisplayModel = models.bannerDisplay.model
const bannerBusinessModel = models.bannerBusiness.model
const userModel = models.user.model;
const bannerTagModel = models.bannerTag.model
const businessTagModel = models.businessTag.model
const Op = require('sequelize').Op;
const logger = require('../logger');

/**
 * Get banners list by filters
 */
async function getBannerList(req, res) {
    if (req.user.business_id && req.user.distributor_id) {

        let banners = await bannerModel.findAll({
            where: {
                [Op.and]:
                    [{
                        [Op.or]:
                            [
                                {'$BannerBusiness.id$': null},
                                {'$BannerBusiness.business_id$': req.user.business_id}]
                    },
                        {
                            [Op.or]:
                                [
                                    {'$BannerLanguage.id$': null},
                                    {'$BannerLanguage.language_code$': req.user.language_code}]
                        }, {
                        [Op.or]:
                            [
                                {'$BannerDistributor.id$': null},
                                {'$BannerDistributor.distributor_id$': req.user['distributor_id']}]
                    },
                        {
                            [Op.or]: [
                                {"$BannerTag.BusinessTag.business_id$": req.user.business_id},
                                {"$BannerTag.id$": null}, {}
                            ]
                        }]
            },
            include: [
                {
                    model: bannerDisplayModel,
                    as: 'BannerDisplay',
                }, {
                    model: bannerBusinessModel,
                    as: 'BannerBusiness',
                    attributes: ['id']
                },
                {
                    model: bannerLanguageModel,
                    as: 'BannerLanguage',
                    attributes: ['id']
                }, {
                    model: bannerDistributorModel,
                    as: 'BannerDistributor',
                    attributes: ['id']
                }, {
                    model: bannerTagModel,
                    as: 'BannerTag',
                    attributes: ['id'],
                    include: [{
                        model: businessTagModel,
                        as: 'BusinessTag',
                        attributes: ['business_id']


                    }]
                }]
        })
        let arrangedBannersList = []
        banners.map((banner) => {
            let arrangedBanner = {}
            arrangedBanner['name'] = banner['name']
            arrangedBanner['banner_url'] = banner['banner_url']
            arrangedBanner['start_date'] = banner['start_date']
            arrangedBanner['end_date'] = banner['end_date']
            arrangedBanner['status'] = banner['status']
            arrangedBanner['duration'] = banner['BannerDisplay'][0]['duration']
            arrangedBanner['num_bounce'] = banner['BannerDisplay'][0]['num_bounce']
            arrangedBanner['order'] = banner['BannerDisplay'][0]['order']
            arrangedBanner['banner_location_id'] = banner['BannerDisplay'][0]['banner_location_id']
            arrangedBannersList.push(arrangedBanner)

        })
        res.send(arrangedBannersList);

    } else {
        res.status(400).send('failed')
    }
}

/***
 * Deletes banner
 * @param req req object
 * @param res res object
 * @return {Promise<void>}
 */
async function deleteBanner(req, res) {
    await bannerModel.destroy({where: {id: req.params.id}, individualHooks:true}).then(()=>{
        res.status(200).send("success");}
    ).catch(e=>{
        //TODO: Add info abuot error
        logger.error(e);
        res.status(500).send("failed");
    })
}

module.exports.getBannerList = getBannerList
module.exports.deleteBanner = deleteBanner;