const DataTypes = require('sequelize').DataTypes;
const BaseModel = require('../../BaseModel');

/**
 * Banner model
 */

class Banner extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            name: {
                type: DataTypes.STRING(1024),
                allowNull: true
            },
            banner_url: {
                type: DataTypes.STRING(1024),
                allowNull: false
            },
            link_url: {
                type: DataTypes.STRING(1024),
                allowNull: true
            },
            start_date: {
                type: DataTypes.DATE,
                allowNull: true
            },
            end_date: {
                type: DataTypes.DATE,
                allowNull: true
            },
        };
        this.statusField = true;
        this.statusTimestampField = true;
        this.author = true;
        this.statusAuthor = true;
        this.prefix = "BAN";
    }

    /**
     * creates associate for Banner model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.hasMany(models.bannerDisplay.model, {as: 'BannerDisplay' });
        this.model.hasMany(models.bannerLanguage.model, {as: 'BannerLanguage' });
        this.model.hasMany(models.bannerTag.model, {as: 'BannerTag' });
        this.model.hasMany(models.bannerBusiness.model, {as: 'BannerBusiness' });
        this.model.hasMany(models.bannerDistributor.model, {as: 'BannerDistributor' });
    }
}

module.exports = new Banner();
