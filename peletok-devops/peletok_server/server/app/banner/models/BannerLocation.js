const DataTypes = require('sequelize').DataTypes;
const BaseModel = require('../../BaseModel');

/**
 * BannerLocation model
 */

class BannerLocation extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            location: {
                type: DataTypes.STRING(128),
                allowNull: true
            },
        };
        this.prefix = "BAN";
    }

    /**
     * creates associate for BannerLocation model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.hasMany(models.bannerDisplay.model, {as: 'BannerDisplay' });
    }
}

module.exports = new BannerLocation();
