const DataTypes = require('sequelize').DataTypes;
const BaseModel = require('../../BaseModel');

/**
 * BannerTag model
 */

class BannerTag extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {};
        this.prefix = "BAN";
    }

    /**
     * creates associate for BannerTag model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.businessTag.model, { as: 'BusinessTag' });
        this.model.belongsTo(models.banner.model, { as: 'Banner' });
    }
}

module.exports = new BannerTag();
