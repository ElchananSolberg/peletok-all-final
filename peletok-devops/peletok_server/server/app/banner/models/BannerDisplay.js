const DataTypes = require('sequelize').DataTypes;
const BaseModel = require('../../BaseModel');

/**
 * BannerDisplay model
 */

class BannerDisplay extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            duration: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            num_bounce: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            order: {
                type: DataTypes.INTEGER,
                allowNull: true
            }
        };
        this.author = true;
        this.prefix = "BAN";
    }

    /**
     * creates associate for BannerDisplay model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.banner.model, { as: 'Banner' });
        this.model.belongsTo(models.bannerLocation.model, { as: 'BannerLocation' });
    }
}

module.exports = new BannerDisplay();
