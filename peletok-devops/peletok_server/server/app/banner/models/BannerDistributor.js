const DataTypes = require('sequelize').DataTypes;
const BaseModel = require('../../BaseModel');

/**
 * BannerDistributor model
 */

class BannerDistributor extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {};
        this.prefix = "BAN";
    }

    /**
     * creates associate for BannerDistributor model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.distributor.model, { as: 'Distributor' });
        this.model.belongsTo(models.banner.model, { as: 'Banner' });
    }
}

module.exports = new BannerDistributor();
