var BaseModel = require('../../BaseModel.js');

/**
 * DocumentHistory model
 */
class DocumentHistory extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.options["indexes"] = [
            {
                fields: ['document_id']
            }
        ];
        this.statusAuthor = true;
        this.statusField = true;
        this.statusTimestampField = true;
        this.prefix = 'DOC';
    }


    /**
     * creates associate for documentHistory model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.document.model, { as: 'Document' });

    }
}

module.exports = new DocumentHistory();