const BaseModel = require('../../BaseModel.js');
const DataTypes = require("sequelize").DataTypes;

/**
 * UserDocument model
 */
class UserDocument extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            type: { // String without validation
                type: DataTypes.STRING(256),
                allowNull: true
            },
            expired_time: {
                type: DataTypes.DATE,
                allowNull: true
            },
            url: {
                type: DataTypes.STRING,
                allowNull: false
            },
            document_by_type: {
                type: new DataTypes.VIRTUAL(DataTypes.JSON, ['type', 'url', 'expired_time']),
                get: function () {
                    return {
                        document_type: this.get("type"),
                        url: this.get('url'),
                        expired_time: this.get("expired_time")
                    };
                },
                set: function (jsonVal) {
                    this.set('type', jsonVal.type);
                    this.set('url', jsonVal.url);
                    this.set('expired_time', jsonVal.expired_time);

                }
            }

        };
        this.prefix = 'DOC';
    }
    /**
     * creates associate for userDocument model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.user.model, { as: 'User' });
    }

}
// exporting instance
module.exports = new UserDocument();