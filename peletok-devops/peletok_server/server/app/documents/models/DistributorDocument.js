const BaseModel = require('../../BaseModel');

/**
 * DistributorDocument model
 */
class DistributorDocument extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.prefix = 'DOC';
    }
    /**
     * creates associate for DistributorDocument model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.distributor.model, { as: 'Distributor' });
        this.model.belongsTo(models.document.model, { as: 'Document' });
    }

}
// exporting instance
module.exports = new DistributorDocument();