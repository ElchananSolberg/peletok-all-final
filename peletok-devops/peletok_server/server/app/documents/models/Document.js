var BaseModel = require('../../BaseModel.js');
const DataTypes = require("sequelize").DataTypes;

/**
 * Document model
 */
class Document extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            type: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            url: {
                type: DataTypes.STRING,
                allowNull: true
            }

        };
        this.statusAuthor = true;
        this.statusTimestampField = true;
        this.statusField = true;
        this.author = true;
        this.prefix = 'DOC';



    }
    /**
     * creates associate for documents model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsToMany(models.supplier.model, { through: models.supplierDocument.model, as: 'Supplier' });
        this.model.hasMany(models.businessDocument.model, {as: 'BusinessDocument' });
        this.model.hasMany(models.documentHistory.model, {as: 'DocumentHistory', });
        this.model.hasMany(models.distributorDocument.model, {as: 'DistributorDocument', });
        this.model.belongsToMany(models.product.model, {through: models.productDocument.model});

    }
}

//exporting instance
module.exports = new Document();