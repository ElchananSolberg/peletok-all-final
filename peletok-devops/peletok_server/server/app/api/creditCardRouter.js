const express = require('express');
const creditCardRouter = express.Router();
const models = require('../index.js');
const Sequelize = require('sequelize');
const Op = Sequelize.Op
const fs = require('fs')
const LM = require('../config/LanguageManager').LM;
const {uploadInvoice} = require('../utils')
const moment = require('moment')
const path = require("path");


creditCardRouter.post('/', async (req, res)=>{
	const params = req.body
	let cards = await models.creditCard.model.create({
		cc_company_name: params.cc_company_name,
		cc_number: params.cc_number,
		cc_holder_name: params.cc_holder_name,
		cc_holder_id: params.cc_holder_id,
		cc_cvv_number: params.cc_cvv_number,
        cc_validity: moment([params.cc_validity_year, params.cc_validity_month - 1 ]),
		description: params.description,
	})
	res.send('success')
})

creditCardRouter.get('/', async (req, res)=>{
	let cards = await models.creditCard.model.findAll({
		attributes: [
			'id',
			'cc_company_name',
			'cc_holder_name',
			'cc_holder_id',
			'cc_cvv_number',
			'description',
			[Sequelize.literal('concat(\'xxxxxxxxxxxx\',substring("CreditCard"."cc_number", char_length("CreditCard"."cc_number") - 3 ,char_length("CreditCard"."cc_number"))) '), 'last_4_numbers'],
			[Sequelize.literal('date_part(\'month\' , cc_validity) + 1'), 'cc_validity_month'],
			[Sequelize.literal('date_part(\'year\' , cc_validity)'), 'cc_validity_year'],
			[Sequelize.literal('concat(date_part(\'month\' , cc_validity) + 1, \'/\', date_part(\'year\' , cc_validity))'), 'cc_validity'],
			],
				 
		raw: true
	})
	res.send(cards)
	
})

creditCardRouter.put('/:card_id',
	async (req, res)=>{
	const params = req.body
	await models.creditCard.model.update(Object.assign({
		cc_company_name: params.cc_company_name,
		cc_holder_name: params.cc_holder_name,
		cc_holder_id: params.cc_holder_id,
        cc_validity: moment([params.cc_validity_year, params.cc_validity_month - 1 ]),
		cc_validity: params.cc_validity,
		description: params.description,
	},params.cc_number ? {cc_number: params.cc_number} : {}),
	{
		where: {id: req.params.card_id}
	})

	res.send('success')
})

creditCardRouter.delete('/:card_id',
	async (req, res)=>{
	await models.creditCard.model.destroy({
		where: {id: req.params.card_id}
	})
	res.send('success')
})


module.exports.creditCardRouter = creditCardRouter;



