const express = require('express');
const Sequelize = require('sequelize');
const Op = Sequelize.Op
const transactionRouter = express.Router();
const cleanRawKeys = require('../utils').cleanRawKeys;
const models = require('../index.js');
const { getUnpaidTransactions, payUnpaidTransactions } = require('../payment/transaction/unPaidTransactions')
const { getReceiptDetails } = require('../payment/transaction/getReceipt')
const { createCancellationTransaction } = require('../payment/transaction/createCancelationTransaction')


transactionRouter.get("/receipt/:transaction_id", async function (req, res) {
  await getReceiptDetails(req, res)
});

transactionRouter.get("/Unpaid", async function (req, res) {
  await getUnpaidTransactions(req, res)
});


transactionRouter.post("/Unpaid", async function (req, res) {
  await payUnpaidTransactions(req, res, req.body['transaction_id'])
});

transactionRouter.get("/cancelation_request/:supplier_id", async function (req, res) {

  let trnasactionsWhere = { transaction_type: 3, cancel_process_status: "AWAITING" }
  if (req.params.supplier_id != 'allSuppliers') {
    trnasactionsWhere.supplier_id = req.params.supplier_id
  }

  let trnasactions = await models.transaction.model.findAll({
    where: trnasactionsWhere,
    attributes: [
      ['parent_transaction_id', 'parentTransactionId'],
      ['phone_number', 'phoneNumber'],
      'action',
      ['price', 'customerPrice'], ['business_price', 'businessPrice'],
      [Sequelize.fn('to_char', Sequelize.col('Transaction.transaction_start_timestamp'), 'HH:MI:SS'), 'requestTime'],
      [Sequelize.fn('to_char', Sequelize.col('Transaction.transaction_start_timestamp'), 'YYYY-MM-DD'), 'requestDate']
    ],
    include: [
      {
        model: models.user.model,
        as: 'Author',
        attributes: [[Sequelize.fn("concat", Sequelize.col("first_name"), ' ', Sequelize.col("last_name")), 'requesterName']]
      },
      {
        model: models.transaction.model,
        as: 'Parent',
        attributes: [
          [Sequelize.fn('to_char', Sequelize.col('Parent.transaction_start_timestamp'), 'HH:MI:SS'), 'transactionTime'],
          [Sequelize.fn('to_char', Sequelize.col('Parent.transaction_start_timestamp'), 'YYYY-MM-DD'), 'transactionDate']
        ]
      },
      {
        model: models.business.model,
        as: 'Business',
        attributes: [['name', 'businessName'], ['business_identifier', 'customerNumber']],
      },
      {
        model: models.product.model,
        as: 'Product',
        attributes: [],
        include: [
          {
            model: models.productPrice.model,
            as: 'ProductPrice',
            attributes: []

          },
          {
            model: models.productDetails.model,
            as: 'ProductDetails',
            where: {
              language_code: 'HE'
            },
            attributes: [['product_name', 'productName']]
          }
        ]
      },
      {
        model: models.supplier.model, as: 'Supplier',
        attributes: [['name', 'supplierName']],
      }
    ],
    raw: true
  })
  let transaciton = trnasactions.map(t => cleanRawKeys(t))
  res.send(transaciton)
});

transactionRouter.post("/cancelation_request/", async function (req, res) {
  await createCancellationTransaction(req, res)
});





module.exports.transactionRouter = transactionRouter;

