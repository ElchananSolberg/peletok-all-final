const BASE_URL = 'https://test-ravkavonline.co';
const AUTH_TOKEN = '8897420b10fc5d4b6895166fe598fa6bd806386c';
let sessionUrl;
let dumpUrl;
let connection;


function connectToReader(activationLink) {
    console.log('connectToReader() activationLink:', activationLink)
    const frame = window.document.createElement('iframe');
    frame.src = activationLink;
    frame.style.display = 'none';
    window.document.body.appendChild(frame);
}

$.ajax({
    url: `${BASE_URL}/api/transaction/session/`,
    type: 'POST',
}).done(result => {
    console.log('api/transaction/session result:', result);
    sessionUrl = result.data.session_url;
    dumpUrl = result.data.dump_url;
    RavKavConnection.connect(sessionUrl, 10000, onConnectStateChange, onConnectSuccess, onConnectFailure);
});

identifyCardDump = cardDumpResult => {
    console.log('identifyCardDump() cardDumpResult:', cardDumpResult)
    $.ajax({
        url: `${BASE_URL}/api/identify/dump-extended/`,
        type: 'POST',
        data: JSON.stringify({ dump: cardDumpResult }),
        contentType: 'application/json',
        dataType: 'json',
        jsonp: false,
        headers: {'X-Ravkav-Location': '32.0796706,34.8023739'},
        processData: false,
    }).done(result => {
        console.log('api/identify/dump-extended result:', result);
        createTransaction(result);
    });
}

createTransaction = recommendedContractsData => {
    console.log('createTransaction() recommendedContractsData:', recommendedContractsData);
    const data = {
        ...recommendedContractsData.contracts[0].transaction,
        is_reserved: false,
    }
    $.ajax({
        url: `${BASE_URL}/api/transaction/transaction/`,
        type: 'POST',
        data: JSON.stringify(data),
        contentType: 'application/json',
        dataType: 'json',
        jsonp: false,
        headers: {'Authorization': `Token ${AUTH_TOKEN}`},
        processData: false,
    }).done(result => {
        console.log('api/transaction/transaction result:', result);
        payWithBalance(result.data.uid)
    });
}

payWithBalance = transactionUid => {
    console.log('payWithBalance() transactionUid:', transactionUid);
    $.ajax({
        url: `${BASE_URL}/api/transaction/${transactionUid}/payment-balance/`,
        type: 'POST',
        headers: {'Authorization': `Token ${AUTH_TOKEN}`},
        contentType: 'application/json',
        dataType: 'json',
        jsonp: false,
        processData: false,
    }).done(result => {
        console.log('api/transaction/${transactionUid}/payment-balance/ result:', result);
        if (result.error === null) {
            createReloading(transactionUid);
        }
    });
}

createReloading = transactionUid => {
    console.log('createReloading() transactionUid:', transactionUid)
    $.ajax({
        url: `${BASE_URL}/api/transaction/${transactionUid}/reloading/`,
        type: 'POST',
        headers: {'Authorization': `Token ${AUTH_TOKEN}`},
        data: JSON.stringify({
            uid: transactionUid,
            personal_id: null,
            start_date: null,
        }),
        contentType: 'application/json',
        dataType: 'json',
        jsonp: false,
        processData: false,
    }).done(result => {
        console.log('api/transaction/${transactionUid}/reloading result:', result);
        if (result.error === null) {
            reloadRavkavCard(result.data.reloading_url)
        }
    });
}

reloadRavkavCard = reloadingUrl => {
    console.log('reloadRavkavCard() reloadingUrl:', reloadingUrl)
    connection.execute(reloadingUrl, 10000, onReloadingExecuteSuccess, onReloadingExecuteFailure)
}

/* reloading execute callbacks */
onReloadingExecuteSuccess = result => {
    console.log('onReloadingExecuteSuccess() result:', result);
}

onReloadingExecuteFailure = error => {
    console.log('onReloadingExecuteFailure() error:', error);
}

/* connect callbacks end */

/* get card dump execute callbacks */
onGetCardDumpExecuteSuccess = cardDump => {
    console.log('onGetCardDumpExecuteSuccess() cardDump:', cardDump);
    identifyCardDump(cardDump.result);
};

onGetCardDumpExecuteFailure = error => {
    console.log('onGetCardDumpExecuteFailure() error:', error);
}
/* get card dump execute callbacks end*/

/* connect callbacks */
onConnectStateChange = state => {
    console.log('onConnectStateChange() state:', state);
    if (state === 'READY') {
        connection.execute(dumpUrl, 10000, onGetCardDumpExecuteSuccess, onGetCardDumpExecuteFailure);
    }
};

onConnectSuccess = ravkavConnection => {
    console.log('onConnectSuccess() ravkavConnection:', ravkavConnection);
    connection = ravkavConnection;
    connectToReader(connection.getDesktopActivationLink())
};

onConnectFailure = error => {
    console.log('onConnectFailure() error:', error);
}
/* connect callbacks end */
