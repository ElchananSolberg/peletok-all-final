const {getStrType} = require('../utils')

module.exports = {
    // fillter object from array if all specified fields don't exist or empty
    filterIfAllObjectsFieldsMissing(arrayOfObjects, fields){
        fields = typeof fields == 'string' ? [fields] : fields
        return arrayOfObjects.filter(e=>fields.filter(f=>e[f]).length > 0)
    },
    // return new object with type name as object [type_name: string]: objects[]
    mapByType(keyOfTypes, data) {
        return data.reduce((newObj, o)=>Object.assign(newObj, {[getStrType(o.type, keyOfTypes)]: [...(newObj[getStrType(o.type, keyOfTypes)] || []), o]}), {})
    },
    isBoolean(val){
    	return val === true || val === false;
    }
}