const BaseModel = require('../../BaseModel');
const DataTypes = require("sequelize").DataTypes;

/**
 * Address model
 */
class SupplierContact extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            name: {
                type: DataTypes.STRING(64),
                allowNull: true
            },
            phone: {
                type: DataTypes.STRING(64),
                allowNull: true
            },
            email: {
                type: DataTypes.STRING(64),
                allowNull: true
            },
            
        };
        this.author = true;
        this.statusAuthor = true;
        this.statusField = true;
        this.statusTimestampField = true;
        this.prefix = "SUPL";
    }

    /**
     * creates associate for user model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.supplier.model, {as: 'Supplier' });
        

    }
}

module.exports = new SupplierContact();
