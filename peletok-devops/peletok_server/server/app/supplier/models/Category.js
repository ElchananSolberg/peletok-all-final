const DataTypes = require('sequelize').DataTypes;
const BaseModel = require('../../BaseModel');

/**
 * Category model
 */
class Category extends BaseModel {

    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            order: {
                type: DataTypes.INTEGER,
                allowNull: true
            }
        };
        this.prefix = 'SUPL';
    }

    /**
     * creates associate for Category model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        //    TODO: Add parent_id FK
        this.model.hasMany(models.categoryName.model, {as: 'CategoryName' });
        this.model.hasMany(models.productCategory.model, {as: 'ProductCategory' });
        this.model.hasMany(models.category.model, {as: 'Child', foreignKey: 'parent_id' });
        this.model.belongsTo(models.category.model, { as: 'Parent', foreignKey: 'parent_id' });

    }
}

module.exports = new Category();
