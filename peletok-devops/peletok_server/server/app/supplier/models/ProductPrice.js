const BaseModel = require('../../BaseModel');
const DataTypes = require("sequelize").DataTypes;

/**
 * ProductPrice model
 */
class ProductPrice extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            fixed_price: {
                type: DataTypes.DOUBLE,
                allowNull: true
            },
            buying_price: {
                type: DataTypes.DOUBLE,
                allowNull: true
            },
            distribution_fee: { 
                type: DataTypes.DOUBLE,
                allowNull: true
            },
            timestamp_start: {
                type: DataTypes.DATE,
                allowNull: true
            },
            timestamp_end: {
                type: DataTypes.DATE,
                allowNull: true
            }
        };
        this.prefix = "SUPL";
        this.author = true;
        this.statusAuthor = true;
        this.statusField = true;
        this.statusTimestampField = true;
    }
    /**
     * creates associate for product model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.product.model, { as: 'Product' });
    }
}


module.exports = new ProductPrice();


