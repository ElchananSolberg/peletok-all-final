const BaseModel = require('../../BaseModel');
const DataTypes = require("sequelize").DataTypes;

/**
 * manualCard model
 */
class ManualCard extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            code: {
                type: DataTypes.STRING(32),
                allowNull: true
            },
            card_number: {
                type: DataTypes.STRING(32),
                allowNull: true
            },
            expiry_date: {
                type: DataTypes.DATEONLY,
                allowNull: true
            },
            insert_date: {
                type: DataTypes.DATE,
                allowNull: true
            },
        };
        this.prefix = "SUPL";
        this.statusField = true;
        this.statusTimestampField = true;
        this.statusAuthor = true;
    }

    /**
     * creates associate for manualCard model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.product.model, { as: 'Product' });
        //Explicit declaration about author_id column
        this.model.belongsTo(models.user.model, {foreignKey: "author_id", as: 'User' });
        this.model.belongsTo(models.transaction.model, { as: 'Transaction' });

    }
}
module.exports = new ManualCard();
