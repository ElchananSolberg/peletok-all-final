const BaseModel = require('../../BaseModel');
const DataTypes = require("sequelize").DataTypes;

/**
 * BusinessSupplier model
 */
class BusinessSupplier extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            id: {
                type: DataTypes.INTEGER,
                autoIncrement: true,
                primaryKey: true
            },
            timestamp_start: {
                type: DataTypes.DATE,
                allowNull: true
            },
            timestamp_end: {
                type: DataTypes.DATE,
                allowNull: true
            },
            description: {
                type: DataTypes.STRING,
                allowNull: true
            },
            is_authorized: {
                type: DataTypes.BOOLEAN,
                allowNull: true
            },
        };
        this.prefix = "SUPL";
        this.author = true;
        this.statusAuthor = true;
        this.statusField = true;
    }
    /**
     * creates associate for supplier model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.user.model, {
            as: 'User',
            foreignKey: "approved_by_id"
        });
    }
}

module.exports = new BusinessSupplier();