const BaseModel = require('../../BaseModel');
const DataTypes = require("sequelize").DataTypes;
const moment = require('moment')
const Business = require('../../business/models/Business').model
const BusinessProduct = require('../../business/models/BusinessProduct').model
const supplier = require('../../supplier/models/Supplier').model
const adminBusinessID = 1
const Op = require('sequelize').Op

/**
 * Product model
 */
class Product extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            id: {
                type: DataTypes.INTEGER,
                autoIncrement: false,
                primaryKey: true
            },
            code_accounting: {
                type: DataTypes.STRING(32),
                allowNull: true
            },
            call_minute: {
                type: DataTypes.INTEGER,
                allowNull: true // null means not limit or not relevant
            },
            sms: {
                type: DataTypes.INTEGER,
                allowNull: true // null means not limit or not relevant
            },
            abroed_price: {
                type: DataTypes.INTEGER,
                allowNull: true // null means not limit or not relevant
            },
            browsing_package: {
                type: DataTypes.STRING(256),
                allowNull: true // null means not limit or not relevant
            },
            call_to_palestinian: {
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: false
            },
            call_terms: {
                type: DataTypes.STRING,
                allowNull: true
            },
            other1: {
                type: DataTypes.STRING,
                allowNull: true
            },
            other2: {
                type: DataTypes.STRING,
                allowNull: true
            },
            more_info_description: {
                type: DataTypes.STRING,
                allowNull: true
            },
            more_info_youtube_url: {
                type: DataTypes.STRING,
                allowNull: true
            },
            more_info_site_url: {
                type: DataTypes.STRING,
                allowNull: true
            },
            type: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            upscale_product_id: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            usage_time_limit: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            order: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            default_is_allow: {
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: true
            },
            cancelable: {
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: true
            },
            profit_model: {
                type: DataTypes.ENUM(["profit", "commission"]),
                allowNull: false,
                defaultValue: "commission"
            },
            is_abstract: { // product for profile
                type: DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: false
            },
            supplier_identity_number: {
                type: DataTypes.STRING,
                allowNull: true
            },
            supplier_color: {
                type: DataTypes.STRING,
                allowNull: true
            },
        };

         this.scopes = {
            BusinessProduct: (businessId) => {
                 return {
                     include: [
                         {
                            attributes: [['product_id', 'id']],
                            model: BusinessProduct,
                            as: 'BusinessProduct',
                            where: {
                                business_id: businessId,
                                is_authorized: true,
                            },
                        },
                     ]
                 }
            } 
        }


        this.prefix = "SUPL";
        this.statusField = true;
        this.statusTimestampField = true;
        this.statusAuthor = true;
        this.attributesToDisplay = ['id', 'name', 'code_supplier', 'status']
    }

    /**
     * creates associate for product model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.supplier.model, { as: 'Supplier' });
        this.model.hasMany(models.productBarcode.model, {as: 'ProductBarcode' });
        this.model.hasMany(models.businessProduct.model, {as: 'BusinessProduct' });
        this.model.hasMany(models.productPrice.model, {as: 'ProductPrice' });
        this.model.hasMany(models.productDetails.model, {as: 'ProductDetails' });
        this.model.hasMany(models.productDetails.model, {as: 'ProductDetailsAr'});
        this.model.hasMany(models.productDetails.model, {as: 'ProductDetailsHe'});
        this.model.hasMany(models.productDetails.model, {as: 'ProductDetailsEn'});
        this.model.hasMany(models.productCategory.model, {as: 'ProductCategory' });
        this.model.hasMany(models.talkPrice.model, {as: 'TalkPrice'});
        this.model.hasMany(models.transaction.model, {as: 'Transaction' });
        this.model.belongsToMany(models.document.model, {through: models.productDocument.model, as: 'Document'});
        this.model.belongsToMany(models.tag.model, {through: models.productTag.model, as: 'Tags'});
        this.model.hasMany(models.manualCard.model, {as: 'ManualCard' });
        this.model.hasMany(models.externalAccountingProductCode.model, {as: 'ExternalAccountingProductCode' });
        this.model.hasMany(models.cost.model, { as: "Cost"})
        this.model.belongsTo(models.product.model, { as: "UpscaleProduct", foreignKey: "upscale_product_id"})

    }
}
const product = new Product();

// Hook for generate id if no given
product.model.beforeCreate((prod, options) => {
    if (prod.id == null) {
        return product.model.max("id", {paranoid: false}).then(maxId => {
            prod.id = maxId ?  maxId + 1 : 1;
        })
    }
});
// Hook for create businessProduct after product is created
product.model.afterCreate( async (product, options)  => {
    let supplierMinPercentageProfit = (await supplier.findOne({where:{id:product.supplier_id},attributes:['min_percentage_profit']})).min_percentage_profit
    let businessProductsToCrate = []
    let businesses
    let productAuthorization = false
    businesses = await Business.findAll({where:{is_abstract:{[Op.or]:[true,false]}}})
    businesses.forEach( async (business)  =>{
        if (business.id == adminBusinessID) {
            productAuthorization = true
        } else{
            productAuthorization = product.default_is_allow
        }
            let businessProduct = {timestamp_start: moment().toDate(),business_id: business.id, product_id: product.id, is_authorized:productAuthorization, final_commission:supplierMinPercentageProfit}
            businessProductsToCrate.push(businessProduct)
    })
    await BusinessProduct.bulkCreate(businessProductsToCrate)
});

// Hook for filter products (only real products)
// To get the products that are past abstract in where object
//     ["is_abstract"] = true;
product.model.beforeFind((options) => {
    options.where = options.where || {};
    if(options.where["is_abstract"]===undefined)
        options.where["is_abstract"] = false;
});
module.exports = product;