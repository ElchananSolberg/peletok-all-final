const BaseModel = require('../../BaseModel');
const DataTypes = require("sequelize").DataTypes;

/**
 * Supplier User model
 */
class SupplierUser extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            type: {
                type: DataTypes.INTEGER,
                allowNull: true
            }
        };
        this.prefix = "SUPL";
        this.statusField = true;
        this.statusTimestampField = true;
    }

    /**
     * creates associate for supplierUser model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.user.model, { as: 'User' });
        this.model.belongsTo(models.supplier.model, { as: 'Supplier' });
    }
}

module.exports = new SupplierUser();
