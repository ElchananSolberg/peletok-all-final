const DataTypes = require('sequelize').DataTypes;
const BaseModel = require('../../BaseModel');
const {isObjectEmpty} = require("../../utils");

/**
 * ProductBarcode model
 */
class ProductBarcode extends BaseModel {

    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            barcode: {
                type: DataTypes.STRING,
                allowNull: false
            },
            barcode_by_type: {
                type: new DataTypes.VIRTUAL(DataTypes.JSON, ['barcode', 'barcode_type_id']),
                get: function () {
                    return {barcode_type_id: this.get("barcode_type_id"), barcode: this.get('barcode')};
                },
                set: function (jsonVal) {
                    this.set('barcode_type_id', jsonVal.barcode_type_id);
                    this.set('barcode', jsonVal.barcode);

                }
            }
        };
        this.prefix = 'SUPL';
        this.author = true;
        this.statusAuthor = true;
        this.statusField = true;
        this.statusTimestampField = true;
    }

    /**
     * creates associate for ProductBarcode model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.barcodeType.model, { as: 'BarcodeType' });
        this.model.belongsTo(models.product.model, { as: 'Product' });
    }
}

module.exports = new ProductBarcode();
