const BaseModel = require('../../BaseModel');
const {DataTypes} = require("sequelize");

/**
 * Cost model
 * Call rates to different countries
 */
class Cost extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            outgoing_call_per_minute: {
                type: DataTypes.DOUBLE,
                allowNull: true
            },
            incoming_call_per_minute: {
                type: DataTypes.DOUBLE,
                allowNull: true
            },
            sms: {
                type: DataTypes.DOUBLE,
                allowNull: true
            }
        };
        this.prefix = "SUPL";
        this.statusField = true;
        this.statusTimestampField = true;
        this.statusAuthor = true;
    }

    /**
     * creates associate for Cost model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.country.model, {as: "Country"})
        this.model.belongsTo(models.product.model, {as: "Product"})
    }
}
module.exports = new Cost();
