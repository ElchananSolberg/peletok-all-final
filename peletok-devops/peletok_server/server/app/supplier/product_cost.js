const path = require("path");
const {create, Info} = require("../mapping/crudMiddleware");

const cost = require("../index").cost.model;
const country = require("../index").country.model;
const {uploadFile} = require("../utils");

// directory to saving cost file
const COST_DIR = path.join(__dirname, '../rest/restData/public/product_cost');

/**
 *
 * @param pathFile path of excel file
 * @return {[Array]} array of rows
 */
function parsCostFile(pathFile) {
    const xlsx = require("xlsx");
    const workSheetsFromFile = xlsx.readFile(pathFile);
    return xlsx.utils.sheet_to_json(workSheetsFromFile.Sheets["Sheet1"],)
}

/**
 * Replaces country string with country id if country string exist in country table
 * @param data {Array} data from excel file
 * @param existCountry {Array} all countries from country table
 * @return {Promise<void>}
 */
async function replaceCountryToCountryId(data, existCountry) {
    for (const countryObject of existCountry){
        for(const constRow of data){
            if(constRow["country"]===countryObject.name){
                delete constRow["country"];
                constRow["country_id"] = countryObject.id;
            }
        }
    }
}

/***
 * Saves product cost in DB (if country not exist its creates new)
 * @param req req object (express)
 * @param res res object (express)
 * @return {Promise<void>}
 */
async function wrapperCreateCost(req, res) {
    let pathFile = (await uploadFile(req, COST_DIR, "cost",
        ["xlsx"], true)).path_cost_file;
    let data = parsCostFile(pathFile);
    let existCountry = await country.findAll({attributes: ["name", "id"]});
    await replaceCountryToCountryId(data, existCountry);
    await cost.destroy({where: {product_id: req.params.product_id}}); // delete old data
    let info = new Info();
    for(const row of data) {
        let mappingPath = "product_cost";
        row["product_id"] = req.params.product_id;
        data.author_id = req.user ? req.user.id : null;
        info.errors.concat((await create(mappingPath, row)).errors);
    }
    res.customSend(info);
}

// console.log(parsCostFile("/home/dev/WebstormProjects/peletok-devops/peletok_server/server/app/rest/restData/local/cityPay/Book1.xlsx"))
module.exports.wrapperCreateCost = wrapperCreateCost;