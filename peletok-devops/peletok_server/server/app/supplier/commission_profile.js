const models = require("../index");
const {getStrType} = require("../utils");
const productModel = models.product.model;
const businessProductModel = models.businessProduct.model;
const businessModel = models.business.model;
const productDetailsModel = models.productDetails.model;
const {getNumStatus} = require('../utils')


/**
 * Returns profile (commission or profit)
 * @param profitModel {String} 'commission' or 'profit'
 * @param language_code {String} language code of user (HE, EN, AR)
 * @param profileId {Number} specific profile
 * @returns {Promise<*>}
 */
async function getProfile(profitModel, language_code, profileId) {
    let whereObj = {is_abstract: true};
    if (profileId) whereObj["id"] = profileId;
    let attributes = ["product_id", "points", "is_authorized", "distribution_fee"];
    if (profitModel === "commission") {
        attributes = attributes.concat(["business_commission", "final_commission"]);
        whereObj["abstract_type"] = "commission profile";
    } else {
        if (profitModel === "profit") {
            attributes.push("percentage_profit");
            whereObj["abstract_type"] = "profit profile";
        } else {
            throw new Error(`profitModel must be 'profit' or 'commission' not ${profitModel}`);
        }
    }
    let businessIncludeRelation = await businessModel.findAll({
        where: whereObj,
        attributes: ["id", "name", "use_distribution_fee"],
        include: [{
            model: businessProductModel,
            as: "BusinessProduct",
            attributes: attributes,
            include: {
                model: productModel,
                as: "Product",
                attributes: ["type", 'supplier_id'],
                where: {profit_model: profitModel},
                required: true,
                where:{status: getNumStatus('active', 'Product')},
                include: [
                    {
                        model: productDetailsModel,
                        as: "ProductDetails",
                        required: false,
                        where: {language_code: language_code},
                        attributes: [["product_name", "name"]]
                    },
                    {
                        model: models.supplier.model,
                        as: 'Supplier',
                        attributes: [['max_percentage_profit', 'business_commission'],'max_percentage_profit', 'min_percentage_profit'],

                    },
                     {
                        model: models.productPrice.model,
                        as: 'ProductPrice',
                        attributes: ['distribution_fee'],

                    }
                ]
            }
        },
        {
            model: models.role.model,
            attributes: ['name']
        }]
    });
    for (const bus of businessIncludeRelation){
        for (const busProd of bus.BusinessProduct){
            busProd.Product.type = getStrType(busProd.Product.type, "product")
        }
    }
    return businessIncludeRelation;


}

module.exports.getProfile = getProfile;