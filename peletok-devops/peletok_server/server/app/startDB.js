const sequelizeInstance = require('./setupSequelize.js');

const storeMap = require("./mapping/insertValFromCsv").storeMap;
const {createAllDemo} = require('./mapping/createDemo');
const logger = require("./logger");
const {getStartDbConfig} = require("./config/config");

const startDbConfig = getStartDbConfig();

/**
 * Creates all tables from models
 * and inserts example users
 * @returns {Promise<void>}
 */
async function startDB() {
    await sequelizeInstance
          .getQueryInterface()
          .dropAllTables();
    //WARNING: Don't change the order of the function calls
    //TODO: Change to force: false
    await sequelizeInstance.sync({force: startDbConfig["force_sync"]}).then(
        () => logger.info("tables created")).catch(
        err => {
            logger.error(err)
        });
    if (startDbConfig["store_map"]) {
        await storeMap().then(
            () => logger.info("map stored")).catch(
            err => {
                logger.error(err)
            });
    }
    if (startDbConfig["demo_data"]) {
        await createAllDemo().then(
            () => logger.info("demo data stored")).catch(
            err => {
                logger.error(err)
            });
    }
    // ----create view---
    const reportView = require("./transaction/view/ReportView");
    await reportView.sync({force: startDbConfig["force_sync_view"]}).then(
        () => logger.info("view table created")).catch(
        err => {
            logger.error(err)
        });

}

startDB().then(() => {logger.info("DB already");
if (process.env.K8S) {
    process.exit(0);
}
// process.exit(0);
}).catch(
    err => {
        logger.error(err);
        // process.exit(1);
    });
