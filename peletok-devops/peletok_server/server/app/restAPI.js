const { getCityPay } = require("./payment/cityPayFromDB");

const express = require('express');
const productRouter = express.Router();
const commissionRouter = express.Router();
const profitRouter = express.Router();
const supplierRouter = express.Router();
const paymentRouter = express.Router();
const giftRouter = express.Router();
const businessRouter = express.Router();
const cityPayRouter = express.Router();
const distributorRouter = express.Router();
const barcodeTypeRouter = express.Router();
const hashavshevetRouter = express.Router()
const tagRouter = express.Router();
const invoiceRouter = express.Router();
const businessProductRouter = express.Router();
const PaymentDetails = require("./payment/paymentDetails")
const bannerRouter = express.Router();
const creditCardsRouter = express.Router();
const Transaction = require('./payment/transaction/transactions.js')
var productsTolls = require('./supplier/findProduct.js').getProducts;
var { wrapperGetProduct, wrapperGetManualCards, wrapperCreateManualCards } = require('./supplier/findProduct.js')
var deleteProduct = require('./supplier/findProduct.js').deleteProduct
var { deleteBarcodeType } = require('./supplier/findProduct.js')
var deleteManualCard = require('./supplier/findProduct.js').deleteManualCard
const { getMappingPath, get, create, wrapperUpdate, wrapperCreateBusiness, update, wrapperCreateDistributor } = require("./mapping/crudMiddleware");
var { wrapperGetSupplierList, wrapperGetSupplier } = require('./supplier/findSupplier.js');
var { wrapperGiftOrder, deleteGift, cancelGiftOrder } = require('./gift/giftsApi')

var getOrderHashavshevetView = require('./hashavshevet/orderHavshevet').getOrderHashavshevetView
var getOrderHashavshevetViewPrivate = require('./hashavshevet/orderHavshevet').getOrderHashavshevetViewPrivate
var getOrderNoByHashavshevet=require('./hashavshevet/orderHavshevet').getOrderNoByHashavshevet
var exportToXml = require('./hashavshevet/orderHavshevet').exportToXml

var getOrderHashavshevetFeeView = require('./hashavshevet/orderHashevetCommissions').getOrderHashavshevetView
var getOrderHashavshevetFeeViewPrivate = require('./hashavshevet/orderHashevetCommissions').getOrderHashavshevetViewPrivate
var exportToXmlFee = require('./hashavshevet/orderHashevetCommissions').exportToXml

var  getHashavshevetByOrder=require('./hashavshevet/orderHavshevet').getHashavshevetByOrder
var  getHashavshevetById=require('./hashavshevet/orderHavshevet').getHashavshevetById
var getHashavshevetByDate=require('./hashavshevet/orderHavshevet').getHashavshevetByDate


var Sequelize = require('sequelize');
var { getNumStatus, uploadInvoice, applyCommissions, getTypeNum } = require('./utils')
const Op = Sequelize.Op;
const moment = require('moment');
const { deleteDistributor } = require('./business/distributorApi.js')
const { deleteBusiness, getBusinessSupplierAuthorizations, updateBusinessSupplier, getBusinessId, getAgents, getBusinessProfiles, isDistributorUseSection20 } = require('./business/businessApi.js')
const { getBannerList } = require('./banner/bannersApi')
const { getTagByNameOrOrder, deleteTag } = require('./tags/tagsApi')
const { hashavshevetImport, showReport } = require('./hashavshevet/importFromXml.js')
const { getBusinessPaymentReports } = require('./hashavshevet/hashavshevetPaymnetReports.js')
const GenericCreate = require("./mapping/GenericCreate");
const {createBusinessActivationNotification} = require('./message/message_utils')

// const payment = require('./utils').payment;
var logs = require("./logger")
const path = require("path");
const suppliersLogosDir = './rest/restData/public/suppliersLogos'
const productsImagesDir = path.join(__dirname, '/rest/restData/public/productsImages')
const giftsImagesDir = './rest/restData/public/gifts'
const distributorsImagesDir = './rest/restData/public/distributors'
const {PtMailer, peletokMail, peletokSecretaryMail} = require('./ptMailer')
const LM = require('./config/LanguageManager').LM;

const { uploadFile, isUserAuthProfiles } = require('./utils.js')
const models = require('./index.js');
const { updateBusinessProduct } = require("./adminApi");
const { multiUpdateBusinessProduct } = require("./adminApi");
const { getProfile } = require("./supplier/commission_profile");
const { getBusinessSupplierProfile, updateBusinessSupplierProfile } = require("./business/bussiness_supplier_profile");
const { copySupplierPermission } = require("./supplier/copy_supplier_data");
const { Info } = require("./mapping/crudMiddleware");
const { copyProfit } = require("./supplier/copy_product_data");
const { wrapperCreateCost } = require("./supplier/product_cost");
const { deleteBanner } = require("./banner/bannersApi");
const supplier = models.supplier;
const supplierModel = supplier.model
const { wrapperCreate, wrapperGet } = require("./mapping/crudMiddleware");
const { updateProfileBusinessProduct } = require('./supplier/profiles_utils')
const peletokBusinessId = 1
let billsPaymentSuppliersType = 2
let prepaidSuppliersType = 1
const {PELETOK_BUSINESS_ID} = require('./constants')




// supplierRouter.post("/supplier", async function (req, res) {
    //     let mappingPath = getMappingPath(req);
    //     req.body.author_id = req.user ? req.user.id : null;
    //     let data = await uploadFile(req, suppliersLogosDir)
    //     data["logo"] = data["fileName"]
    //     let info = await create(mappingPath, data);
    //     res.customSend(info);
    // });
    
    businessRouter.get('/get_locked', async (req, res) => {
        
        const businesses = await models.business.model.findAll({
            attributes: [
                'id',
                ['name', 'businessName'],
                ['business_identifier', 'customerNumber'],
                ['agent_id', 'agentNumber'],
                'status'
            ],
            where: Object.assign({status: [
                getNumStatus('frozen', 'Business'),
                getNumStatus('locked', 'Business'),
                getNumStatus('pending', 'Business'),
            ]}, req.user.business_id != PELETOK_BUSINESS_ID ? {distributor_business_id: req.user.business_id} : {}),
        })
        
        
        res.send(businesses)
    


    })
    //approve business creation (for admin)
    businessRouter.put("/approve/:id", async (req, res) => {
        let info = new Info()
        let mappingPath = 'business'
        let data = { status: getNumStatus('active', 'Business') }
        data.author_id = req.user ? req.user.id : null;
        const filter = req.params.id ? { id: req.params.id } : {};
        info = await update(mappingPath, filter, data);
        if (info.message == 'success') {
            let approvedBusinessDistributorId = (await models.business.model.findOne({ where: { id: req.body.business.id }, attributes: ['id'] })).id
            await createBusinessActivationNotification(req.user.business_id, req.params.id, req.user.language_code, req.body.business)
            await createBusinessActivationNotification(req.user.business_id, approvedBusinessDistributorId, req.user.language_code, req.body.business)
            await PtMailer.send(peletokMail,peletokSecretaryMail, LM.getString('createBusinessApprovalHeader'),`${LM.getString('createBusinessApproval')} ${req.body.business.name} ${LM.getString('businessIdentifier')} ${req.body.business.business_identifier} ${LM.getString('approved')}.`, null, )
        }

        res.customSend(info)
    
    });
    supplierRouter.get("/supplier_list", wrapperGetSupplierList);
    
// Delete supplier
supplierRouter.delete("/supplier/:id", function (req, res) {
    supplierModel.findOne({
        where: { id: req.params.id }
    }).then(sup => {
        if (sup) {
            sup.destroy().then(info => {
                res.send(info)
            })
        } else {
            res.status(400).send('failed')
        }
    })
});



supplierRouter.put("/supplier/:id", async function (req, res) {
    let data = await uploadFile(req, suppliersLogosDir);
    await wrapperUpdate(req, res, null, data);
});


supplierRouter.post("/copy_permission/", async function (req, res) {
    let info = new Info();
    let from = req.query.from;
    let to = req.query.to;
    if (from && to) {
        await copySupplierPermission(req.user, from, to, info);
    }
    else {
        info.errors.push("request query must include 'from' and 'to'")
    }
    res.customSend(info);
});





// productRouter.get("/prepaid_product/:supplier_id", async function (req, res) {

//     // TODO: Add message if supplier not exist
//     // TODO: Check type product in DB
//     var receivedProduct = await productsTolls(req.params.supplier_id);
//     res.send(receivedProduct);

// });



productRouter.get("/product_cost/:product_id", wrapperGet);

productRouter.post("/product_cost/:product_id/", wrapperCreateCost);

productRouter.delete("/product/:product_id", async function (req, res) {
    await deleteProduct(req, res)
});
productRouter.put("/product/:id", async function (req, res) {
    let data = await uploadFile(req, productsImagesDir);
    await wrapperUpdate(req, res, null, data);
});

barcodeTypeRouter.get("/", wrapperGetProduct);
// create product
barcodeTypeRouter.post("/", wrapperCreate);

barcodeTypeRouter.delete("/:id", deleteBarcodeType);

barcodeTypeRouter.put("/:id", async (req, res) => {
    await wrapperUpdate(req, res, null, null, "barcode_type");
});

productRouter.post("/manual_card/", wrapperCreateManualCards);

productRouter.get("/manual_card/:productId", wrapperGetManualCards);

productRouter.put("/manual_card/:id", async function (req, res) {
    await wrapperUpdate(req, res, null, req.body, "manual_card");
})


productRouter.delete("/manual_card/:id", async function (req, res) {
    await deleteManualCard(req, res)
});

productRouter.post("/copy_commission/", async function (req, res) {
    let info = new Info();
    await copyProfit(req, "business_commission").catch(e => {
        info.errors.push(e);
    });
    res.customSend(info);
});

productRouter.post("/copy_percentage_profit/", async function (req, res) {
    let info = new Info();
    await copyProfit(req, "percentage_profit").catch(e => {
        info.errors.push(e);
    });
    res.customSend(info);
});

/***
 * Creates commission (or profit) profile (profile its a fake product)
 * @param req req object
 * @param res res object
 * @param profitModel {string} profit or commission
 * @returns {Promise<void>}
 */
async function createProfile(req, res) {
    let profitModel = req.body.profitModle
    let data = { is_abstract: true, author_id: req.user ? req.user.id : null, profit_model: profitModel, name: req.body.name };
    if (profitModel === "commission") {
        data["abstract_type"] = "commission profile";
    } else {
        if (profitModel === "profit") {
            data["abstract_type"] = "profit profile";
        } else {
            throw new Error(`profitModel must be 'profit' or 'commission' not ${profitModel}`);
        }
    }
    let info = new Info();
    info = await wrapperCreateBusiness('business', data).catch(e => {
        info.errors.push(e);
        res.customSend(info)
    });

    let suppliers = await models.supplier.model.findAll({
        include: [
            {
                model: models.product.model,
                as: 'Product',
                include: [
                    {
                        model: models.businessProduct.model,
                        as: 'BusinessProduct',
                        where: { business_id: info.message.id }
                    },

                ]
            }
        ]
    })

    for (let i = 0; i < suppliers.length; i++) {
        const supplier = suppliers[i];
        for (let index = 0; index < supplier.Product.length; index++) {
            const product = supplier.Product[index];
            for (let j = 0; j < product.BusinessProduct.length; j++) {
                let updateObj = {}
                const businessProduct = product.BusinessProduct[j];

                if (supplier.type == billsPaymentSuppliersType) {
                    updateObj = {final_commission:supplier.min_percentage_profit}
                }
                if (product.default_is_allow) {
                    updateObj.is_authorized = true
                }
                await models.businessProduct.model.update(updateObj,{where:{id:businessProduct.id}})
            }

        }
    }
    res.customSend(info);
}

//create commission profile
commissionRouter.post("/profile/", async (req, res) => {
    let profitModel = "commission";
    await createProfile(req, res, profitModel)
});

//update commission profile
commissionRouter.put("/profile/", async (req, res) => {
    req.body.business_id = req.body.profile_id;
    await multiUpdateBusinessProduct(req, res)
});

//get commission profile
commissionRouter.get("/profile/", async (req, res) => {
    let info = new Info();
    await getProfile("commission", req.user.language_code).then(r => { info.result = r }).catch(err => info.errors.push(err));
    res.customSend(info);
});

commissionRouter.put("/profile/product", async (req, res) => {
    await updateProfileBusinessProduct(req, res)
});

//create profit profile
profitRouter.post("/profile/", async (req, res) => {
    await createProfile(req, res)
}
);
//update profit profile
profitRouter.put("/profile/", async (req, res) => {
    req.body.business_id = req.body.profile_id;
    await multiUpdateBusinessProduct(req, res)
});

profitRouter.put("/profile/product", async (req, res) => {
    await updateProfileBusinessProduct(req, res)
});

//get profit profile
profitRouter.get("/profile/:profitModel", async (req, res) => {
    let info = new Info();
    let profitModel = req.params.profitModel;
    await getProfile(profitModel, req.user.language_code).then(r => { info.result = r }).catch(err => info.errors.push(err));
    res.customSend(info);
});

// get all business profiles
businessRouter.get("/profiles/", async (req, res) => {
    await getBusinessProfiles(req, res)
});
// get all users with agent role
businessRouter.get("/agent/", async (req, res) => {
    await getAgents(req, res)
});
// get all business-supplier profile
businessRouter.get("/profile/", async (req, res) => {
    let info = new Info();
    await getBusinessSupplierProfile().then(result => info.result = result).catch(err => { info.errors.push(err) });
    res.customSend(info);
});

// get one business-supplier profile
businessRouter.get("/profile/:id", async (req, res) => {
    let info = new Info();
    await getBusinessSupplierProfile(req.params.id).then(result => info.result = result).catch(err => { info.errors.push(err) });
    res.customSend(info);
});
businessRouter.get("/tags", async (req, res) => {
    let tags = await models.tag.model.findAll({
        attributes: ['id',['tag_name', 'name']],
        where: {type: getTypeNum('business', 'Tag')},
        order: [['order', 'ASC']],
        raw: true
    })
    res.send(tags)
});




// create business-supplier profile
// body must be json object that includes 'suppliers' key with json object value
// in the  format {<supplier_id>: true or 'true' or false or 'false'
businessRouter.post("/profile/", async (req, res) => {
    let profile_id;
    let info = new Info();
    req.body.is_abstract = true;
    req.body.abstract_type = "supplier profile";
    await new GenericCreate("business", req.body).create().then(async r => {
        const profileId = r.id;
        profile_id = profileId
        const supplierObject = req.body.suppliers;
        await updateBusinessSupplierProfile(supplierObject, profileId, info)
    }).catch(e => { info.errors.push(e) });

     let suppliers = await models.supplier.model.findAll({
         attributes:['id','default_creation']
     })
     for (let index = 0; index < suppliers.length; index++) {
         const supplier = suppliers[index];
         if (supplier.default_creation) {
             await models.businessSupplier.model.update({is_authorized:true},{where:{business_id:profile_id,supplier_id:supplier.id}})
         }
         
     }

    let profile = await models.business.model.findOne({
        where: {
            id: profile_id,
            is_abstract: true
        }
    })
    let roles = await models.role.model.findAll({
        where: {
            name: req.body.roles
        }
    })
    await profile.setRoles(roles)
   
    res.customSend(info);
});

// update business-supplier profile
// body must be json object that includes 'suppliers' key with json object value
// in the  format {<supplier_id>: true or 'true' or false or 'false'
businessRouter.put("/profile/:id", async (req, res) => {
    let info = new Info();
    const profileId = req.params.id;
    const supplierObject = req.body.suppliers;
    await updateBusinessSupplierProfile(supplierObject, profileId, info);
    let profile = await models.business.model.findOne({
        where: {
            id: req.params.id,
            is_abstract: true
        }
    })
    let roles = await models.role.model.findAll({
        where: {
            name: req.body.roles
        }
    })
    await profile.setRoles(roles)
    res.customSend(info);
});

// get all banners
bannerRouter.get("/", wrapperGet);

// get banner
bannerRouter.get("/:id", wrapperGet);

// create banner
bannerRouter.post("/", wrapperCreate);

// delete banner
bannerRouter.delete("/:id", deleteBanner);

// update banner
bannerRouter.put("/:id", async function (req, res) {
    await wrapperUpdate(req, res, null, null, "banner");

});


// get banners list 

bannerRouter.get("/banner_list/", async function (req, res) {
    await getBannerList(req, res)
});


// get all gifts
giftRouter.get("/gift/", wrapperGet);

// get single gift
giftRouter.get("/gift/:id", wrapperGet);

// get ordered gifts
giftRouter.get("/ordered/", async function (req, res) {
    let mappingPath = getMappingPath(req);
    let filter = { "user_id": req.user.id };
    let info = await get(mappingPath, filter);
    res.customSend(info);
});

// create gift
giftRouter.post("/gift", async function (req, res) {
    let mappingPath = getMappingPath(req);
    req.body.author_id = req.user ? req.user.id : null;
    let data = await uploadFile(req, giftsImagesDir)
    data.picture = data.nameAndDate
    let info = await create(mappingPath, data);
    res.customSend(info);
});

// update gift
giftRouter.put("/gift/:id", async function (req, res) {
    let data = await uploadFile(req, giftsImagesDir);
    data.picture = data.nameAndDate
    await wrapperUpdate(req, res, null, data);

});
// cancel gift order
giftRouter.put("/cancel/", async function (req, res) {
    await cancelGiftOrder(req, res)

});

//delete gift
giftRouter.delete("/gift/:id", async function (req, res) {
    await deleteGift(req, res)

});
// order gift process
giftRouter.post("/gift_order/", wrapperGiftOrder);

// get frozen and locked businesses
businessRouter.get("/frozen_business/", async (req, res) => {
    let mappingPath = "business";
    let filter = { [Op.or]: [{ status: getNumStatus('frozen', 'Business') }, { status: getNumStatus('locked', 'Business') }] };
    let info = await get(mappingPath, filter);
    res.customSend(info);
});

// get business ids
//TODO: required admin permission
businessRouter.get("/id", getBusinessId);

// get business
businessRouter.get("/business", wrapperGet);

// get business by distributor id
businessRouter.get("/business_of_distributor/:distributorId", async (req, res) => {
    let mappingPath = "business";
    let filter = !req.user.isPtAdmin ? { distributor_id: req.params.distributorId, id: { [Op.ne]: req.user.business_id } } : { distributor_id: req.params.distributorId };
    let info = await get(mappingPath, filter);
    for (let index = 0; index < info.length; index++) {
        const business = info[index];
        let tags = await models.tag.model.findAll({
            attributes: ['id'],
            include: [
                {
                    model: models.business.model,
                    as: 'Businesses',
                    where: {id: business.id}
                }
            ],
            includeIgnoreAttributes: false,
            raw: true
        })
        business.tags = tags
    }

    res.customSend(info);
});
businessRouter.post("/business_supplier/", async (req, res) => {
    await updateBusinessSupplier(req, res)

});

// get business by id
businessRouter.get("/business/:id", wrapperGet);

// delete business by id
businessRouter.delete("/business/:id", async function (req, res) {
    await deleteBusiness(req, res)
});
// update business
businessRouter.put("/business/:id", async function (req, res) {
    
    if (req.body.section_20) {
        let info = new Info();
        let currentBusinessDistributorId = (await models.business.model.findOne({ where: { id: req.params.id }, attributes: ['distributor_id'] })).distributor_id
        if ((await isDistributorUseSection20(currentBusinessDistributorId))) {
            info.errors.push(new Error('Unauthorized action'))
            res.customSend(info)
            return
        }
    }


    let profileIds = [
        req.body.suppliers_profile_id,
        req.body.profitPercentagesProfile_id,
        req.body.commissionsProfile_id
    ].filter(p=>p)// check if profile selected.

    if(!(await isUserAuthProfiles(req.user.id, profileIds ))){
        return res.status(400).send('Unauthorized profile')
    }

    // save old business identifier for future update
    let oldBusinessIdendifier = null
    if(req.body.business_identifier){
        oldBusinessIdendifier = (await models.business.model.findOne({
            attributes: ['business_identifier'],
            where:{
            id: req.params.id,
        }})).business_identifier
    } else if (req.body.hasOwnProperty('business_identifier')) { 
        let info = new Info();
        info.errors.push(new Error('Business identifier is required'))
        return res.customSend(info)
    }

    let mappingPath = 'business'
    let data = req.body;
    data.author_id = req.user ? req.user.id : null;
    const filter = req.params.id ? { id: req.params.id } : {};
    let info = await update(mappingPath, filter, data);
    if (info.message == 'success') {
        // check if we update business identifier and update transaction data
        if(req.body.business_identifier && oldBusinessIdendifier != req.body.business_identifier){
            models.transactionData.model.update({
                business_identifier: req.body.business_identifier
            }, {
                where: {business_identifier: oldBusinessIdendifier}
            })
        }
        if (req.body.suppliers_profile_id) {
            let suppliersProfile = await models.business.model.findOne({
                where: { id: req.body.suppliers_profile_id, is_abstract: true },
                include: [
                    {
                        model: models.supplier.model,
                        as: 'Suppliers'
                    }
                ]
            })
            let businessSuppliers = []
            suppliersProfile['Suppliers'].map((supplier) => {
                businessSuppliers.push(supplier.BusinessSupplier)
            })
            let authorizedSuppliersIds = []
            let unAuthorizedSuppliersIds = []
            businessSuppliers.forEach((businessSupplier) => {
                if (businessSupplier.is_authorized) {
                    authorizedSuppliersIds.push(businessSupplier.supplier_id)
                } else {
                    unAuthorizedSuppliersIds.push(businessSupplier.supplier_id)
                }
            })
            await models.businessSupplier.model.update({ is_authorized: true, author_id: req.user.id }, { where: { business_id: req.params.id, supplier_id: authorizedSuppliersIds } }).catch(err => {
                info.errors.push(err)
            })
            await models.businessSupplier.model.update({ is_authorized: false, author_id: req.user.id }, { where: { business_id: req.params.id, supplier_id: unAuthorizedSuppliersIds } }).catch(err => {
                info.errors.push(err)
            })
        }
        

        
        if (req.body.tags) {
            let chosenBusiness = await models.business.model.findOne({where:{id:req.params.id},include:[{model: models.tag.model, as: 'Tag', required: false},]})

            await chosenBusiness.setTag(JSON.parse(req.body.tags));
        }
        if (req.body.profitPercentagesProfile_id) {
            let profitPercentagesProfile_id = await models.business.model.findOne({
                where: { id: req.body.profitPercentagesProfile_id, is_abstract: true },
                include: [
                    {
                        model: models.businessProduct.model,
                        as: 'BusinessProduct',
                        attributes: ['product_id', 'percentage_profit', 'points', 'distribution_fee', 'is_authorized']
                    }
                ],
            })
            for (let i = 0; i < profitPercentagesProfile_id.BusinessProduct.length; i++) {
                const businessProduct = profitPercentagesProfile_id.BusinessProduct[i];
                businessProduct['author_id'] = req.user.id
                await models.businessProduct.model.update({
                    percentage_profit: businessProduct.percentage_profit,
                    points: businessProduct.points, distribution_fee: businessProduct.distribution_fee, is_authorized: businessProduct.is_authorized, author_id: req.user.id
                }, { where: { business_id: req.params.id, product_id: businessProduct.product_id } }).catch(err => {
                    info.errors.push(err)
                })
            }

        }

        if (req.body.commissionsProfile_id) {
            let commissionsProfile_id = await models.business.model.findOne({
                where: { id: req.body.commissionsProfile_id, is_abstract: true },
                include: [
                    {
                        model: models.businessProduct.model,
                        as: 'BusinessProduct',
                        attributes: ['product_id', 'business_commission', 'final_commission', 'is_authorized']
                    }
                ],
            })

            for (let i = 0; i < commissionsProfile_id.BusinessProduct.length; i++) {
                const businessProduct = commissionsProfile_id.BusinessProduct[i];
                await models.businessProduct.model.update({
                    business_commission: businessProduct.business_commission,
                    final_commission: businessProduct.final_commission, is_authorized: businessProduct.is_authorized, author_id: req.user.id
                },
                    { where: { business_id: req.params.id, product_id: businessProduct.product_id } }).catch(err => {
                        info.errors.push(err)
                    })
            }

        }
        res.customSend(info)
    } else {
        res.customSend(info)
    }


});



businessRouter.post("/business/", async (req, res) => {
    let newDistributor;
    if (req.body.is_distributor) {
        newDistributor = await models.distributor.model.create({});
        req.body.distributor_id = newDistributor.id
    }

    let profileIds = [
        req.body.suppliers_profile_id,
        req.body.profitPercentagesProfile_id,
        req.body.commissionsProfile_id
    ]
    if(!(await isUserAuthProfiles(req.user.id, profileIds ))){
        return res.status(400).send('Unauthorized profile')
    }

    let info = new Info();
    let mappingPath = getMappingPath(req);
    if (req.body.section_20) {
        if ((await isDistributorUseSection20(req.body.distributor_id))) {
            info.errors.push(new Error('Unauthorized action'))
            res.customSend(info)
            return
        }
    }
    if (req.body.is_distributor && req.user.business_id != peletokBusinessId) {
        info.errors.push(new Error('Unauthorized action'))
        res.customSend(info)
        return
    } else {
        let data = req.body
        data.status = getNumStatus('pending', 'Business')
        info = await wrapperCreateBusiness(mappingPath, data);
        if (info.message.message == 'success') {
            if (req.body.suppliers_profile_id) {
                let suppliersProfile = await models.business.model.findOne({
                    where: { id: req.body.suppliers_profile_id, is_abstract: true },
                    include: [
                        {
                            model: models.supplier.model,
                            as: 'Suppliers'
                        }
                    ]
                })
                let businessSuppliers = []
                suppliersProfile['Suppliers'].map((supplier) => {
                    businessSuppliers.push(supplier.BusinessSupplier)
                })
                let authorizedSuppliersIds = []
                let unAuthorizedSuppliersIds = []
                businessSuppliers.forEach((businessSupplier) => {
                    if (businessSupplier.is_authorized) {
                        authorizedSuppliersIds.push(businessSupplier.supplier_id)
                    } else {
                        unAuthorizedSuppliersIds.push(businessSupplier.supplier_id)
                    }
                })
                await models.businessSupplier.model.update({ is_authorized: true, author_id: req.user.id }, { where: { business_id: info.message.id, supplier_id: authorizedSuppliersIds } }).catch(err => {
                    info.errors.push(err)
                })
                await models.businessSupplier.model.update({ is_authorized: false, author_id: req.user.id }, { where: { business_id: info.message.id, supplier_id: unAuthorizedSuppliersIds } }).catch(err => {
                    info.errors.push(err)
                })
            }
            if (req.body.profitPercentagesProfile_id) {
                let profitPercentagesProfile_id = await models.business.model.findOne({
                    where: { id: req.body.profitPercentagesProfile_id, is_abstract: true },
                    include: [
                        {
                            model: models.businessProduct.model,
                            as: 'BusinessProduct',
                            attributes: ['product_id', 'percentage_profit', 'points', 'distribution_fee', 'is_authorized']
                        }
                    ],
                })
                for (let i = 0; i < profitPercentagesProfile_id.BusinessProduct.length; i++) {
                    const businessProduct = profitPercentagesProfile_id.BusinessProduct[i];
                    businessProduct['author_id'] = req.user.id
                    await models.businessProduct.model.update({
                        percentage_profit: businessProduct.percentage_profit,
                        points: businessProduct.points, distribution_fee: businessProduct.distribution_fee, is_authorized: businessProduct.is_authorized, author_id: req.user.id
                    }, { where: { business_id: info.message.id, product_id: businessProduct.product_id } }).catch(err => {
                        info.errors.push(err)
                    })
                }

                await models.business.model.update({ use_distribution_fee: profitPercentagesProfile_id.use_distribution_fee }, { where: { id: info.message.id } })

            }

            if (req.body.commissionsProfile_id) {
                let commissionsProfile_id = await models.business.model.findOne({
                    where: { id: req.body.commissionsProfile_id, is_abstract: true },
                    include: [
                        {
                            model: models.businessProduct.model,
                            as: 'BusinessProduct',
                            attributes: ['product_id', 'business_commission', 'final_commission', 'is_authorized']
                        }
                    ],
                })

                for (let i = 0; i < commissionsProfile_id.BusinessProduct.length; i++) {
                    const businessProduct = commissionsProfile_id.BusinessProduct[i];
                    await models.businessProduct.model.update({
                        business_commission: businessProduct.business_commission,
                        final_commission: businessProduct.final_commission, is_authorized: businessProduct.is_authorized, author_id: req.user.id
                    },
                        { where: { business_id: info.message.id, product_id: businessProduct.product_id } }).catch(err => {
                            info.errors.push(err)
                        })
                }

            }
            if (req.body.tags) {
                let chosenBusiness = await models.business.model.findOne({where:{id:info.message.id}})

                await chosenBusiness.setTag(JSON.parse(req.body.tags));
            }
            res.customSend(info)
        } else {
            res.customSend(info)
        }
    }

});

businessRouter.post("/lock/:id", async (req, res) => {
    await wrapperUpdate(req, res, null, { status: getNumStatus("locked", "Business") }, "business")
});

businessRouter.get("/business_supplier", async (req, res) => {
    await getBusinessSupplierAuthorizations(req, res)
});



// get all distributors
distributorRouter.get("/distributor", async (req, res) => {
    let mappingPath = "business";
    let filter = !req.user.isPtAdmin ? { id: req.user.business_id } : { is_distributor: true };
    let info = await get(mappingPath, filter);
    res.customSend(info);
});

// get distributor
distributorRouter.get("/distributor/:id", wrapperGet);

// create distributor
distributorRouter.post("/distributor", async function (req, res) {
    let data = req.body
    let info = new Info();
    let mappingPath = 'distributor'
    info = await wrapperCreateDistributor(mappingPath, data)
    res.customSend(info)
});
// delete distributor
distributorRouter.delete("/distributor/:id", async function (req, res) {
    await deleteDistributor(req, res)

});
// update distributor
distributorRouter.put("/distributor/:id", async function (req, res) {
    let name = `distributor${req.params.id}`;
    let data = await uploadFile(req, distributorsImagesDir, name);
    await wrapperUpdate(req, res, null, data);
});

tagRouter.get("/tag", wrapperGet);

tagRouter.get("/tag/by_name_or_order", async function (req, res) {

    await getTagByNameOrOrder(req, res, req.query.name_or_order)
});

tagRouter.delete("/tag/:id", async function (req, res) {

    await deleteTag(req, res)
});

tagRouter.put("/tag/:id", async function (req, res) {

    await wrapperUpdate(req, res, null, req.body, "tag");

});
tagRouter.post("/tag", wrapperCreate);

hashavshevetRouter.post("/import", async function (req, res) {

    await hashavshevetImport(req, res)

});

hashavshevetRouter.post("/show_report", async function (req, res) {
    await showReport(req, res)
});
hashavshevetRouter.get("/business_reports", async function (req, res) {

    await getBusinessPaymentReports(req, res)

});

invoiceRouter.post("/", async function (req, res) {

    await uploadInvoice(req, res);

});

businessProductRouter.put("/", async function (req, res) {

    await applyCommissions(req, res);

});
//get  price of  bill 
paymentRouter.post("/details/:supplierID", async function (req, res) {

    logs.info('details function invoked ,  id  is ' + req.params.supplierID)
    let transaction = new PaymentDetails(req)
    let info = await transaction.getPrice()

    

    if (!info.send) {
        res.status(403).send({ "error": info.error.message })
    }
    else {
        res.send(info.send)
    }


});

//  to get  details for cancel bill/prodact - // step1
paymentRouter.get("/cancelDetails/", async function (req, res) {
    logs.info('paymentCancel function invoked ,  id  is ' + req.query.supplierID)
    let transaction = new PaymentDetails(req)
    let info = await transaction.cancelPaymentDetails()
    if (info.send) {
        res.send(info.send)
    }
    else {
        res.status(403).send({ "error": info.error.message })
    }
});


//  to  pay a bill/prodact 
paymentRouter.post("/payment/:supplierID", async function (req, res) {
    logs.info('payment function invoked ,  id  is ' + req.params.supplierID)
    let transaction = new Transaction(req, res)
    let info = await transaction.pay()
    if (info.send) {
        res.send(info.send)
    }
    else {
        res.status(403).send({ "error": info.error.message })
    }
});
// function which save bill payment confirmation number

paymentRouter.post("/confirmationNumberSave/:supplierID", async function (req, res) {
    let transaction = new PaymentDetails(req)
    let info = await transaction.confirmationSaveHandler()
    if (info.send) {
        res.send(info.send)
    }
    else {
        res.status(403).send({ "error": info.error.message })
    }
});
//  to  revert data from bill/prodact 
paymentRouter.post("/revert/:supplierID", async function (req, res) {
    logs.info('revert function invoked ,  id  is ' + req.params.supplierID)
    let transaction = new PaymentDetails(req)
    let info = await transaction.revert()
    if (info.send) {
        res.send(info.send)
    }
    else {
        res.status(403).send({ "error": info.error.message })
    }
});

//  to  approval payment 
paymentRouter.post("/approval/:supplierID", async function (req, res) {
    logs.info('approval function invoked ,  id  is ' + req.params.supplierID)
    let transaction = new Transaction(req, res)
    let info = await transaction.approval()
    if (info.send) {
        res.send(info.send)
    }
    else {
        res.status(403).send({ "error": info.error.message })
    }
});



//  to cancel a bill/prodact (Api)
paymentRouter.post("/cancel/:supplierID", async function (req, res) {
    logs.info('paymentCancel function invoked ,  id  is ' + req.params.supplierID)
    let transaction = new Transaction(req, res)
    let info = req.body.internal ? await transaction.cancelPaymentInternal(false) : await transaction.cancelPayment()
    if (info.send) {
        res.send(info.send)
    }
    else {
        res.status(403).send({ "error": info.error.message })
    }
});


// rejects the cancel request
paymentRouter.post("/cancel/reject/:supplierID", async function (req, res) {
    logs.info('paymentCancel reject function invoked ,  id  is ' + req.params.supplierID)
    let transaction = new Transaction(req, res)
    let info = await transaction.rejectCancelation()
    if (info.send) {
        res.send(info.send)
    }
    else {
        res.status(403).send({ "error": info.error.message })
    }
});

async function isCommissionExport(exportedOrdertId){
    let hashavshevetExport = await models.hashavshevetExport.model.findOne({
        where: {id: exportedOrdertId},
        attributes: ['meta']
    })
    return JSON.parse(hashavshevetExport.meta).isCommission
}
hashavshevetRouter.get("/order_view/:businessId", async function (req, res) {

    if(req.query.exportedOrdertId){
        req.query.isCommissionExport = await isCommissionExport(req.query.exportedOrdertId)
    }
    await getOrderHashavshevetViewPrivate({businessId: req.params.businessId, ...req.query}).then(r => {
        res.status(200).send(r)
    }).catch(e => {
        logs.error(e);
        res.status(403).send("failed")
    })
})

hashavshevetRouter.get("/order_view", async function (req, res) {
    await getOrderHashavshevetView(req.query).then(r => {
        res.status(200).send(r)
    }).catch(e => {
        logs.error(e);
        res.status(403).send("failed")
    })
})
hashavshevetRouter.post("/export_xml", async function (req, res) {
    if(req.body.exportedOrdertId){
        req.body.isCommissionExport = await isCommissionExport(req.body.exportedOrdertId)
    }
    await exportToXml(req.body).then(fileName => {

        
        res.status(200).download(fileName)
    }).catch(e => {
        logs.error(e);
        res.status(403).send("failed")
    })
})


hashavshevetRouter.get("/order_fee_view", async function (req, res) {
    await getOrderHashavshevetFeeView(req.body.startDate, req.body.endData, req.body.businessId).then(r => {
        res.status(200).send(r)
    }).catch(e => {
        logs.error(e);
        res.status(403).send("failed")
    })
})
hashavshevetRouter.get("/order_fee_view_private",async function (req, res) {
    await getOrderHashavshevetFeeViewPrivate(req.body.startDate,req.body.endData,req.body.businessId).then(r => {
        res.status(200).send(r)
    }).catch(e => {
        logs.error(e);
        res.status(403).send("failed")
    })
})
hashavshevetRouter.get("/export_xml_fee", async function (req, res) {

    await exportToXmlFee(req.body.startDate, req.body.endData, req.body.businessId).then(fileName => {


        res.status(200).download(fileName)
    }).catch(e => {
        logs.error(e);
        res.status(403).send("failed")
    })
})


hashavshevetRouter.get("/get_hashavshevet_by_order",async function (req, res) {
    await  getHashavshevetByOrder(req.body.orderNo).then(r => {
        res.status(200).send(r)
    }).catch(e => {
        logs.error(e);
        res.status(403).send("failed")
    })
})
hashavshevetRouter.get("/get_hashavshevet_by_date",async function (req, res) {
    await getHashavshevetByDate(req.query.startDate,req.query.endDate).then(r => {
        res.status(200).send(r)
    }).catch(e => {
        logs.error(e);
        res.status(403).send("failed")
    })
})

hashavshevetRouter.get("/get_order_by_hashavshevetName",async function (req, res) {
    await getOrderNoByHashavshevet(req.body.hashavshevetName).then(r => {
        res.status(200).send(r)
    }).catch(e => {
        logs.error(e);
        res.status(403).send("failed")
    })
})




hashavshevetRouter.get("/get_hashavshevet_by_id",async function (req, res) {
    await getHashavshevetById(req.body.startDate,req.body.endData,req.body.businessId).then(r => {
        res.status(200).send(r)
    }).catch(e => {
        logs.error(e);
        res.status(403).send("failed")
    })
})

cityPayRouter.get("/", async function (req, res) {
    getCityPay().then(r => {
        res.status(200).send(r)
    }).catch(e => {
        logs.error(e);
        res.status(403).send("failed")
    });

});

creditCardsRouter.get("/", async function (req, res) {
    let creditCards = await models.creditCard.model.findAll({
        attributes: ['id', ['cc_company_name', 'name'], 'cc_number']
    });
    creditCards.forEach((cc) => {
        cc.cc_number = cc.cc_number.slice(-4)
    })
    res.send(creditCards)
})




module.exports.productRouter = productRouter;
module.exports.supplierRouter = supplierRouter;
module.exports.paymentRouter = paymentRouter;
module.exports.giftRouter = giftRouter;
module.exports.businessRouter = businessRouter;
module.exports.cityPayRouter = cityPayRouter;
module.exports.distributorRouter = distributorRouter;
module.exports.bannerRouter = bannerRouter;
module.exports.tagRouter = tagRouter;
module.exports.barcodeTypeRouter = barcodeTypeRouter;
module.exports.hashavshevetRouter = hashavshevetRouter;
module.exports.invoiceRouter = invoiceRouter;
module.exports.businessProductRouter = businessProductRouter;
module.exports.commissionRouter = commissionRouter;
module.exports.profiteRouter = profitRouter;
module.exports.creditCardsRouter = creditCardsRouter;
