const BaseModel = require('../../BaseModel');
const DataTypes = require("sequelize").DataTypes;

/**
 * BusinessProduct model
 */
class BusinessProduct extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            timestamp_start: {
                type: DataTypes.DATE,
                allowNull: true
            },
            timestamp_end: {
                type: DataTypes.DATE,
                allowNull: true
            },
            percentage_profit: {
                type: DataTypes.DOUBLE,
                allowNull: false,
                defaultValue:0
            },
            business_commission: {
                type: DataTypes.DOUBLE,
                allowNull: false,
                defaultValue:0
            },
            final_commission: {
                type: DataTypes.DOUBLE,
                allowNull: false,
                defaultValue:0
            },
            points: {
                type: DataTypes.DOUBLE,
                allowNull: false,
                defaultValue:0
            },
            is_authorized: {
                type: DataTypes.BOOLEAN,
                allowNull: true
            },
            distribution_fee: {
                type: DataTypes.BOOLEAN,
                allowNull: true
            },
            favorite: {
                type: DataTypes.BOOLEAN,
                allowNull: true
            }
        };
        this.prefix = "BSN";
        this.author = true;
        this.statusAuthor = true;
        this.statusField = true;
        this.options["indexes"] = [{unique: true, fields: ["product_id", "business_id"], where: {"deleted_at": null}}]
    }
    /**
     * creates associate for supplier model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.business.model, { as: 'Business' });
        this.model.belongsTo(models.product.model, { as: 'Product' });
        this.model.belongsTo(models.user.model, {
            as: 'User',
            foreignKey: "approved_by_id"
        });
    }
}

module.exports = new BusinessProduct();