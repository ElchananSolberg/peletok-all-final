const DataTypes = require('sequelize').DataTypes;
const BaseModel = require('../../BaseModel');

/**
 * BusinessContact model
 */

class BusinessContact extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            contact_name: {
                type: DataTypes.STRING(128),
                allowNull: true
            },
            contact_phone_number: {
                type: DataTypes.STRING,
                allowNull: true
            },
            contact_mail: {
                type: DataTypes.STRING,
                allowNull: true
            },
            contact_account_mail: {
                type: DataTypes.STRING,
                allowNull: true
            }
        };
        this.prefix = "BSN";
    }

    /**
     * creates BusinessContact for Branch model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.business.model, { as: 'Business' });

    }
}

module.exports = new BusinessContact();
