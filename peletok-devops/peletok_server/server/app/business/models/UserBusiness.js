const DataTypes = require('sequelize').DataTypes;
const BaseModel = require('../../BaseModel');

/**
 * User business model
 */
class UserBusiness extends BaseModel {

    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            position: {
                type: DataTypes.INTEGER,
                allowNull: true
            }
        };
        this.author = true;
        this.statusAuthor = true;
        this.statusField = true;
        this.statusTimestampField = true;
        this.prefix = 'BSN';
    }

    /**
     * creates associate for User business model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.user.model, { as: 'User' });
        this.model.belongsTo(models.business.model, { as: 'Business' });
    }

}

module.exports = new UserBusiness();