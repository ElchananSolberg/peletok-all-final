var BaseModel = require('../../BaseModel.js');
const DataTypes = require("sequelize").DataTypes;

/**
 * Terminal model
 */
class Terminal extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            mac_address: {
                type: DataTypes.STRING(16),
                allowNull: true
            },
            cpu_id: {
                type: DataTypes.STRING(64),
                allowNull: true
            },
            local_token: {
                type: DataTypes.STRING,
                allowNull: true
            }
        };

        this.prefix = 'BSN'

    }
     /**
     * creates associate for terminal model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.branch.model, { as: 'branch' });
        this.model.hasMany(models.transaction.model,{ as:'Transaction'})
    } 


}

module.exports = new Terminal();