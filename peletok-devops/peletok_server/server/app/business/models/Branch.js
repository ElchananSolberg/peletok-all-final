const DataTypes = require('sequelize').DataTypes;
const BaseModel = require('../../BaseModel');

/**
 * Branch model
 */

class Branch extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            name: {
                type: DataTypes.STRING(128),
                allowNull: true
            },
            ip_address: {
                type: DataTypes.STRING(16),
                allowNull: true
            },
            ip_address_required: {
                type: DataTypes.BOOLEAN,
                allowNull: true
            }
        };
        this.prefix = "BSN";
    }

    /**
     * creates associate for Branch model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.business.model, { as: 'Business' });
        this.model.hasMany(models.branchAddress.model, {as: 'BranchAddress' });
        this.model.hasMany(models.terminal.model, {as: 'Terminal' });
        this.model.hasMany(models.userBranch.model, {as: 'UserBranch' });
        this.model.hasMany(models.branchBankAccount.model, {as: 'BranchBankAccount' });
    }
}

module.exports = new Branch();
