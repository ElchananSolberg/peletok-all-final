const DataTypes = require('sequelize').DataTypes;
const BaseModel = require('../../BaseModel');
/**
 * Supplier model
 */
class BusinessAddress extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            type: {
                type: DataTypes.INTEGER,
                allowNull: true
            },

        };
        this.author = true;
        this.statusField = true;
        this.statusTimestampField = true;
        this.prefix = 'BSN';
    }
    /**
     * creates associate for supplier model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.business.model, { as: 'Business' });
        this.model.belongsTo(models.address.model, { as: 'Address' });

    }
}

module.exports = new BusinessAddress();