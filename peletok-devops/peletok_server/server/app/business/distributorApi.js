const models = require('../index.js')
const distributorModel = models.distributor.model


/**
 * delete distributor
 * @param {*} req 
 * @param {*} res 
 */
async function deleteDistributor(req, res) {
    await distributorModel.findOne({
        where: { id: req.params.id },
    }).then(distributor => {
        if (distributor) {
            distributor.destroy().then(info => {
                res.send(info)
            })
        } else {
            res.status(400).send('failed');
        }
    })
}


module.exports.deleteDistributor = deleteDistributor

