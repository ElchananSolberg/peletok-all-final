const models = require("../index");
const businessModel = models.business.model;
const supplierModel = models.supplier.model;
const businessSupplierModel = models.businessSupplier.model;
const {getNumStatus} = require('../utils')

// Methods for management business profiles (suppliers authorized)


/**
 * Returns an abstract business (profile) with information about the suppliers it is authorized to use
 * @param profileId {Number} abstract business id (if null reurns all)
 * @returns {Promise<*>}
 */
async function getBusinessSupplierProfile(profileId) {
    let whereObject = {is_abstract: true, abstract_type: "supplier profile"};
    if(profileId)whereObject["id"] = profileId;
    return await businessModel.findAll({
        where: whereObject,
        attributes: ["name", "id"],
        include: [{
            model: supplierModel,
            as: "Suppliers",
            attributes: ["name", ["id", "supplier_id"]],
            where:{status: getNumStatus('active', 'Supplier')},
            through: {attributes: ["is_authorized"]}
        },
        {model: models.role.model, attributes: ['name']}
        ],

    },)
}

/**
 * Updates abstract business (profile)
 * @param supplierObject {Object} suppliers it is authorized to use, format {supplierID: boolean}
 * @param profileId {Number} abstract business id
 * @param info Info object to save errors
 * @returns {Promise<void>}
 */
async function updateBusinessSupplierProfile(supplierObject, profileId, info) {
    let unauthorized = [];
    let authorized = [];
    Object.keys(supplierObject).map(k => {
        if (supplierObject[k] === false || supplierObject[k] === "false")
            unauthorized.push(k);
        else if (supplierObject[k] === true || supplierObject[k] === "true")
            authorized.push(k);
    });
    await models.businessSupplier.model.update({is_authorized: false}, {
        where: {
            supplier_id: unauthorized,
            business_id: profileId
        }
    }).catch(e => {
        info.errors.push(e)
    });
    await models.businessSupplier.model.update({is_authorized: true}, {
        where: {
            supplier_id: authorized,
            business_id: profileId
        }
    }).catch(e => {
        info.errors.push(e)
    });
}


module.exports.getBusinessSupplierProfile = getBusinessSupplierProfile;
module.exports.updateBusinessSupplierProfile = updateBusinessSupplierProfile;