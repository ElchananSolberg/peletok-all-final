const models = require('../../index')
const sequelize = require('../../setupSequelize')
const request = require('supertest');
const routing = require('../../routing')
const bodyParser = require('body-parser');
var express = require('express');
const moment = require('moment')
const {getTypeNum} = require('../../utils')
const path = require("path");
const {params} = require('./mockSuppliers')

let truncateModel = (model) => {
    return sequelize.getQueryInterface().bulkDelete(model.getTableName(), {}, {
        truncate: true,
        cascade: true,
    }, model)
}

describe('supplierRouterCreate', () => {


    describe('POST /supplier', () => {

        let params;
        let current_user;
        let creditCard;
        let app;

        beforeAll(async (done) => {


            await sequelize.sync({
                force: true,
                logging: false
            });

            current_user = await models.user.model.create({
                login_name: 'admin',
                first_name: 'user',
                last_name: 'family',
            })

            creditCard = await models.creditCard.model.bulkCreate([
                {cc_company_name: 'visa', cc_number: '5000129456783020'}
            ])

            app = express();
            app.use(express.json());
            app.use(express.urlencoded({ extended: false }));
            app.use(bodyParser.urlencoded({ extended: false }));
            // mock req.user
            app.use((req, res, next) => {
                req.user = { id: current_user.id };
                next();
            })
            app.use('/', routing)

            params = {
                name: 'פלאפון1',
                contact_phone_number: '022222222',
                country: 'ישראל',
                city: 'גבעתיים',
                street: 'דרך יצחק רבין',
                number: '33',
                zip_code: '33',
                order: '2',
                contact_name: 'אילן',
                status: '1',
                max_percentage_profit: '12',
                min_percentage_profit: '11',
                max_payment: '12',
                type: '1',
                credit_card_id: '1',
            }

            done()
        })

        beforeEach(async (done) => {
            await truncateModel(models.supplier.model)
            await truncateModel(models.supplierContact.model)
            await truncateModel(models.address.model)
            await truncateModel(models.document.model)
            done()
        })



        it('Should retun status 200', () => {
            return request(app).post('/supplier').then((response) => {
                expect(response.statusCode).toBe(200);
            })
        })

        it('Should create one supplier', () => {
            return request(app)
                .post('/supplier')
                .send({

                })
                .then(async (response) => {
                    let supplier = await models.supplier.model.findAll()
                    expect(supplier.length).toBe(1);
                })
        })

         test('supplier should have this values',  async (done) => {
            await request(app).post('/supplier').send(params)
            let supplier = await models.supplier.model.findOne()
            expect(supplier).toMatchObject({
                name: params.name,
                type: parseInt(params.type),
                credit_card_id: parseInt(params.credit_card_id),
                order: parseInt(params.order),
                status: parseInt(params.status),
                max_percentage_profit: parseInt(params.max_percentage_profit),
                min_percentage_profit: parseInt(params.min_percentage_profit),
                max_payment: parseInt(params.max_payment),
            })
            done()

        } )

        test('address shoud created',  async (done) => {
            await request(app).post('/supplier').send(params)
            let supplier = await models.supplier.model.findOne()
            let address = await supplier.getAddress()
            expect(address).not.toBe(null)
            done()

        } )

        test('address shoud contain fields',  async (done) => {
            await request(app).post('/supplier').send(params)
            let supplier = await models.supplier.model.findOne()
            let address = await supplier.getAddress()
            expect(address).toMatchObject({
                country: 'ישראל',
                city: 'גבעתיים',
                street: 'דרך יצחק רבין',
                number: '33',
                zip_code: '33',
            })
            done()

        } )

        test('supplierContact shoud created',  async (done) => {
            await request(app).post('/supplier').send(params)
            let supplier = await models.supplier.model.findOne()
            let supplierContact = await supplier.getSupplierContact()
            expect(supplierContact).not.toBe(null)
            done()

        } )

        test('supplierContact shoud contain fields',  async (done) => {
            await request(app).post('/supplier').send(params)
            let supplier = await models.supplier.model.findOne()
            let supplierContact = await supplier.getSupplierContact()
            expect(supplierContact).toMatchObject({
                name: params.name,
                phone: params.contact_phone_number,
            })
            done()

        } )
        test('1 document should be exist', async (done) => {
                await request(app).post('/supplier/').attach('image', path.join(__dirname,'wataniya-10.png')).field(params)
                let docs = await models.document.model.findAll();
                expect(docs.length).toBe(1)
                done()
            } )

        test('document should be type logo', async (done) => {
            await request(app).post('/supplier/').attach('image', path.join(__dirname,'wataniya-10.png')).field(params)
            let doc = await models.document.model.findOne();
            expect(doc.type).toBe(getTypeNum('logo', 'Document'))
            

            done()
        } )


         test('document url should match name regex', async (done) => {
                await request(app).post('/supplier/').attach('image', path.join(__dirname,'wataniya-10.png')).field(params)
                let doc = await models.document.model.findOne();
                expect(doc.url).toEqual(
                    expect.stringMatching(/^\/image-wataniya-10-[0-9]{1,4}-[0-9]{1,2}-[0-9]{1,2}T[0-9]{1,2}-[0-9]{1,2}-[0-9]{1,2}\.png$/)
                    )

                done()
            } )

            test('if image field doesn\'t exist or contain empty string document shouldn\'t created', async (done) => {
                // params.image = ''
                await request(app).post('/supplier/').field(params)
                let doc = await models.document.model.findOne();
                expect(doc).toBe(null);
                done()
            } )
    })
})