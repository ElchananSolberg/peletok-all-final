const {getTypeNum, getStrType} = require('../../utils')
const moment = require('moment')
const models = require('../../index')


let params = {
        name: 'פלאפון1',
        contact_phone_number: '022222222',
        country: 'ישראל',
        city: 'גבעתיים',
        street: 'דרך יצחק רבין',
        number: '33',
        zip_code: '33',
        order: '2',
        contact_name: 'אילן',
        status: '1',
        max_percentage_profit: '12',
        min_percentage_profit: '11',
        max_payment: '12',
        type: '1',
        credit_card_id: '1',
        fileName: 'test file url'
     }
async function mockSuppliers(){
        
            let current_user = await models.user.model.create({
                 login_name: 'admin',
                 first_name: 'user',
                 last_name: 'family',   
             })
           
            businesses = await models.business.model.bulkCreate([
                {name: 'business test1', },
                {name: 'business test2', }
            ]) 


            creditCard = await models.creditCard.model.bulkCreate([
                {cc_company_name: 'visa', cc_number: '5000129456783020'},
                {cc_company_name: 'master', cc_number: '5333129456783020'}
            ])

             
             
             let suppliers = []
             for(key in [1,2,3]){
                 let supplier = await models.supplier.model.create({
                            name: params.name + key, 
                            type: params.type, 
                            credit_card_id: 1, 
                            order: params.order, 
                            status: params.status, 
                            max_percentage_profit: params.max_percentage_profit, 
                            min_percentage_profit: params.min_percentage_profit, 
                            max_payment: params.max_payment,
                            Address: {
                                country: params.country,
                                city: params.city,
                                street: params.street,
                                number: params.number,
                                zip_code: params.zip_code,
                            },
                            SupplierContact: {
                                name: params.contact_name,
                                phone: params.contact_phone_number,
                            },
                            Document: [
                                {url: params.fileName + 'background', type: getTypeNum('backgroundImage', 'Document')},
                                {url: params.fileName + 'image', type: getTypeNum('image', 'Document')},
                                {url: params.fileName + 'logo', type: getTypeNum('logo', 'Document')}
                            ],
                            SupplierColor: [{
                              favorite_star_color: '#0072B6',
                              product_background: '#626a7c',
                              chosen_background: '#5ad0f8',
                              logo_background: '#5ad022',
                              font: '#5ad011',
                          }],  
                        },{
                            include:[
                                {model: models.address.model, as: 'Address'},
                                {model: models.supplierContact.model, as: 'SupplierContact'},
                                {model: models.document.model, as: 'Document'},
                                {
                                    model: models.supplierColor.model,
                                    as: 'SupplierColor',
                                },
                            ]  
                 })
                 suppliers.push(supplier);
             }

             return [current_user, suppliers, {id: 1}]
         }

  module.exports = {
      params,
      mockSuppliers,
  }