
const models = require('../../index')
const sequelize = require('../../setupSequelize')
const request = require('supertest');
// const routing = require('../../routing')
const {supplierRouter} = require('../../api/supplierRouter')
const bodyParser = require('body-parser');
var express = require('express');
const {getStrType, getTypeNum} = require('../../utils')
const {truncateModel} = require('./testUtil')
const {params, mockSuppliers} = require('./mockSuppliers')




describe('supplierRouterGetById', () => {

    

    describe('GET /supplier/:supplier_id', () => {

            
            let current_user;
            let app;
            let suppliers;
            let business;

            beforeAll(async  (done) => {

                await sequelize.sync({
                         force: true,
                         logging: false
                });
                
                [suppliers, current_user, business] = await mockSuppliers();
                
                app = express();
                app.use(express.json());
                app.use(express.urlencoded({extended: false}));
                app.use(bodyParser.urlencoded({ extended: false })); 
                // mock req.user
                app.use((req, res, next)=>{      
                    req.user = {
                        id: current_user.id,
                        business_id: business.id,
                        language_code: 'HE',
                        business_id: 1
                    };
                    next();
                })
                app.use('/supplier', supplierRouter)
                // app.use('/', routing)
                
                done()
            } ) 

            it('Should retun status 200', () => {
                return request(app).get('/supplier/1').then((response) => {
                    expect(response.statusCode).toBe(200);
                } )
            } )

            test('response should be ...', async () => {
                        let supplierId = 2;
                        await models.businessSupplier.model.update({is_authorized: false}, {
                            where: {is_authorized: true}
                        })
                        await models.businessSupplier.model.update({is_authorized: true}, {
                             where: {supplier_id: supplierId}
                        })
                        const supplier = await models.supplier.model.findOne({
                            where: {
                                id: supplierId,
                            }
                        })
                        const [images, contact, address, color] = await Promise.all(
                            [    
                                supplier.getDocument({
                                    where: {type: getTypeNum('logo', 'Document')}
                                }),
                                supplier.getSupplierContact(),
                                supplier.getAddress(),
                                supplier.getSupplierColor(),
                            ]
                        )
                        
                        return request(app).get('/supplier/' + supplierId).then(async(response) => {
                            // console.log(response.body)
                            expect(response.body).toMatchObject(
                                {
                                    id: supplierId,
                                    name: supplier.name,
                                    image: images[0].url,
                                    order: supplier.order,
                                    address: address.street,
                                    contact_phone_number: contact.phone,
                                    status: supplier.status,
                                    credit_card_id: supplier.credit_card_id,
                                    max_payment: supplier.max_payment,
                                    max_percentage_profit: supplier.max_percentage_profit,
                                    min_percentage_profit: supplier.min_percentage_profit,
                                    contact_name: contact.name,
                                       
                                }
                            )
                        })
                    
            })


    })
} )