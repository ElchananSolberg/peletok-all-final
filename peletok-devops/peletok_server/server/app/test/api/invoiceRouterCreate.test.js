const models = require('../../index')
const sequelize = require('../../setupSequelize')
const request = require('supertest');
// const routing = require('../../routing')
const {invoiceRouter} = require('../../api/invoiceRouter')
const Product = models.product.model;
const bodyParser = require('body-parser');
var express = require('express');
const {getTypeNum, getNumStatus} = require('../../utils')
const {fileNameFormat} = require('./testUtil')
const moment = require('moment')
const path = require("path");


let truncateModel = (model) => {
       return sequelize.getQueryInterface().bulkDelete(model.getTableName(),{},{
           truncate: true,
           cascade: true,
       }, model)
}


describe('invoiceRouterCreate', () => {





    

    describe(' /incoice', () => {

            let params;
            let current_user;
            let business;
            let app;

            

            beforeAll(async  (done) => {
                
                
                 await sequelize.sync({
                         force: true,
                         logging: false
                     })
                
                 current_user = await models.user.model.create({
                     login_name: 'admin',
                     first_name: 'user',
                     last_name: 'family',   
                 })

                 business = await models.business.model.create({
                     name: 'test',
                     business_identifier: 32,
                 })

                 business2 = await models.business.model.create({
                     name: 'test2',
                     business_identifier: 31,
                 })

                 await models.invoice.model.bulkCreate([{
                    invoice_date:'2020-01-02',
                    invoice_number: 12333,
                    customer_number: business.business_identifier,
                    path : path.join(__dirname,'32Invoice1-2019.pdf')
                },
                {
                    invoice_date:'2020-02-01',
                    invoice_number: 12346,
                    customer_number: business.business_identifier,
                    path : path.join(__dirname,'32Invoice1-2019.pdf')
                },
                {
                    invoice_date:'2020-03-01',
                    invoice_number: 12345,
                    customer_number: business2.business_identifier,
                    path : path.join(__dirname,'32Invoice1-2019.pdf')
                }])

                app = express();
                app.use(express.json());
                app.use(express.urlencoded({extended: false}));
                app.use(bodyParser.urlencoded({ extended: false })); 
                // mock req.user
                app.use((req, res, next)=>{      
                    req.user = {id: current_user.id, business_id: business.id};
                    next();
                })
                // app.use('/', routing)
                app.use('/invoice/', invoiceRouter );                


                
                
                 done()
            } )

            beforeEach(async  (done) => {
               
                done()
            })

            describe('GET /invoice', ()=>{

                it('Should retun status 200', () => {
                    return request(app).get('/invoice/').then((response) => {
                        expect(response.statusCode).toBe(200);
                    } )
                } )

                it('Should retun 2 invoice', () => {
                    return request(app).get('/invoice/').then((response) => {
                        expect(response.body.length).toBe(2);
                    } )
                } )

                it('Should mathc', () => {
                    return request(app).get('/invoice/').then((response) => {
                        expect(response.body).toMatchObject([{
                            id: 1,
                            invoice_date:'2020-01-02T00:00:00.000Z',
                            invoice_number: '12333',
                            customer_number: business.business_identifier,
                            path : path.join(__dirname,'32Invoice1-2019.pdf')
                        },
                        {
                            id: 2,
                            invoice_date:'2020-02-01T00:00:00.000Z',
                            invoice_number: '12346',
                            customer_number: business.business_identifier,
                            path : path.join(__dirname,'32Invoice1-2019.pdf')
                        },]);
                    } )
                } )

                 it('Should return 1 invoice if date specified', () => {
                    return request(app).get('/invoice?year=2020&month=1').then((response) => {
                        expect(response.body.length).toBe(1);
                    } )
                } )

                it('Should mathc', () => {
                    return request(app).get('/invoice?year=2020&month=1').then((response) => {
                        expect(response.body).toMatchObject([{
                            id: 1,
                            invoice_date:'2020-01-02T00:00:00.000Z',
                            invoice_number: '12333',
                            customer_number: business.business_identifier,
                            path : path.join(__dirname,'32Invoice1-2019.pdf')
                        },
                        ]);
                    } )
                } )

                
            })

            describe('POST /invoice/send_email', ()=>{

                it('Should retun status 200', () => {
                    return request(app).post('/invoice/send_email')
                    .send({invoices: [1,2], to: 'chaim@gmail.com'})
                    .then((response) => {
                        expect(response.statusCode).toBe(200);
                    } )
                } )
            })

            describe('POST /invoice/subscribe', ()=>{

                it('Should retun status 200', () => {
                    return request(app).post('/invoice/subscribe')
                    .send({address: 'chaim@gmail.com'})
                    .then((response) => {
                        expect(response.statusCode).toBe(200);
                    } )
                } )

                it('Should update current business invoice_email', () => {
                    return request(app).post('/invoice/subscribe')
                    .send({address: 'chaim@gmail.com'})
                    .then(async (response) => {
                        await business.reload()
                        expect(business.invoice_email).toBe('chaim@gmail.com');
                    } )
                } )

                it('Should update only current business invoice_email', () => {
                    return request(app).post('/invoice/subscribe')
                    .send({address: 'chaim@gmail.com'})
                    .then(async (response) => {
                        await business2.reload()
                        expect(business2.invoice_email).toBe(null);
                    } )
                } )
            })

            describe('POST /invoice/unsubscribe', ()=>{

                it('Should retun status 200', () => {
                    return request(app).post('/invoice/unsubscribe')
                    .send({business_id: business.id})
                    .then((response) => {
                        expect(response.statusCode).toBe(200);
                    } )
                } )

                it('Should update current business invoice_email', () => {
                    return request(app).post('/invoice/unsubscribe')
                    .send({business_id: business.id})
                    .then(async (response) => {
                        await business.reload()
                        expect(business.invoice_email).toBe(null);
                    } )
                } )

                
            })

            describe('POST /invoice/is_subscribe', ()=>{

                it('Should retun status 200', () => {
                    return request(app).post('/invoice/is_subscribe')
                    .send({id: business.id})
                    .then((response) => {
                        expect(response.statusCode).toBe(200);
                    } )
                } )

                it('Should retun false for id', async () => {
                    return request(app).post('/invoice/is_subscribe')
                    .send({id: business.id})
                    .then((response) => {
                        expect(response.body.isSubscribe).toBe(false);
                    } )
                } )

                it('Should retun false for business_identifier', async () => {
                    return request(app).post('/invoice/is_subscribe')
                    .send({business_identifier: business.business_identifier})
                    .then((response) => {
                        expect(response.body.isSubscribe).toBe(false);
                    } )
                } )

                it('Should retun true for id', async () => {
                    await business.update({invoice_email: 'chaim@gmail.com'})
                    return request(app).post('/invoice/is_subscribe')
                    .send({id: business.id})
                    .then((response) => {
                        expect(response.body.isSubscribe).toBe(true);
                    } )
                } )

                it('Should retun true for business_identifier', async () => {
                    await business.update({invoice_email: 'chaim@gmail.com'})
                    return request(app).post('/invoice/is_subscribe')
                    .send({business_identifier: business.business_identifier})
                    .then((response) => {
                        expect(response.body.isSubscribe).toBe(true);
                    } )
                } )

                // it('Should update current business invoice_email', () => {
                //     return request(app).post('/invoice/unsubscribe')
                //     .send({business_id: business.id})
                //     .then(async (response) => {
                //         await business.reload()
                //         expect(business.invoice_email).toBe(null);
                //     } )
                // } )

                
            })

            // it('Should retun status 200', () => {
            //     return request(app).post('/invoice/').then((response) => {
            //         expect(response.statusCode).toBe(200);
            //     } )
            // } )

            // it('Should retun status 200 after get file', () => {
            //     return request(app).post('/invoice/')
            //     .attach('invoice', path.join(__dirname,'32Invoice1-2019.pdf'))
            //     .then((response) => {
            //         expect(response.statusCode).toBe(200);
            //     } )
            // } )

                         


      } )
} )