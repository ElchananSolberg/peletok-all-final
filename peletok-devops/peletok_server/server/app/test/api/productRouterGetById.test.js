const models = require('../../index')
const sequelize = require('../../setupSequelize')
const request = require('supertest');
// const routing = require('../../routing')
const {productRouter} = require('../../api/productRouter')
const Product = models.product.model;
const bodyParser = require('body-parser');
var express = require('express');
const {getTypeNum} = require('../../utils')
const {params, mockProducts} = require('./mockeProducts')


describe('productRouterGet', () => {

    

    describe('GET /product', () => {
            let app;
            let supplier1
            let current_user
            beforeAll(async  (done) => {
                await sequelize.sync({
                         force: true,
                         logging: false
                 });

                [supplier1, current_user] = await mockProducts();
                app = express();
                app.use(express.json());
                app.use(express.urlencoded({extended: false}));
                app.use(bodyParser.urlencoded({ extended: false })); 
                // mock req.user
                app.use((req, res, next)=>{      
                    req.user = {
                        id: current_user.id,
                        language_code: 'HE',
                        business_id: 1
                    };
                    next();
                })
                // app.use('/', routing)
                app.use('/product/', productRouter );                
                
                console.log('here')
                done()
            } )

            it('Should retun status 200', () => {
                return request(app).get('/product/1').then((response) => {
                    expect(response.statusCode).toBe(200);
                } )
            } )

  

            

            test('response should be ...', async () => {
                const product = await Product.findOne({
                    where: {
                        id: 1,
                    }
                })
                const images = await product.getDocument()
              

                const p = await models.product.model.findOne({
                    where: {
                        id: 1
                    }
                })
                await p.addTags([2,4,5])
                const tags = await p.getTags()
                return request(app).get('/product/1').then(async(response) => {
                    let barcodes = await models.productBarcode.model.findAll({
                        where: {
                            product_id: 1
                        }
                    });

                    let talk_prices = await models.talkPrice.model.findAll({
                        where: {
                            product_id: 1
                        }
                    });


                    expect(response.body).toMatchObject({
                                id:1,
                                name: 'he name0',
                                description: params.description_he + 0,
                                price: 23,
                                image: 'image',
                                barcodes: barcodes.map(b=>{
                                    return {
                                        barcode_type_id:b.barcode_type_id,
                                        barcode:b.barcode,
                                    }
                                }),
                                tags: tags.map(t=>t.id),
                                call_terms: 'text',
                                image: (images.find(i=>i.type == getTypeNum('image', 'Document')) || {}).url,
                                more_info_image: (images.find(i=>i.type == getTypeNum('other_image', 'Document')) || {}).url,
                                name: "he name0",
                                name_he: params.name_he + 0,
                                name_en: params.name_en + 0,
                                name_ar: params.name_ar + 0,
                                order: params.order,    
                                profit_model: params.profit_model,
                                status: params.status,
                                supplier_id: supplier1.id,
                                type: 1,
                                // upscale_product_id: 2, //TODO
                                usage_time_limit: params.usage_time_limit,
                                buying_price: params.buying_price,
                                more_info_description: params.more_info_description,
                                more_info_youtube_url: params.more_info_youtube_url,
                                more_info_site_url: params.more_info_site_url,
                                call_minute: params.call_minute,
                                sms: params.sms,
                                browsing_package: params.browsing_package,
                                call_to_palestinian: params.call_to_palestinian,
                                abroed_price: params.abroed_price,
                                other1: params.other1,
                                other2: params.other2,
                                status: params.status,
                                default_is_allow: params.default_is_allow,
                                cancelable: params.cancelable,
                                talk_prices:  talk_prices.map(b=>{
                                    return {
                                        country:b.country,
                                        outcomePrice:b.outcome_price,
                                        incomePrice:b.income_price,
                                        smsPrice:b.sms_price,
                                        
                                    }
                                }),

                    })
                 })
            })

            it('Should return only user\'s business product else return 401', async () => {
                await models.businessProduct.model.update({
                    is_authorized: false
                },{
                    where: {
                        business_id: 1,
                        product_id: 1,
                    }
                })
                return request(app).get('/product/' + supplier1.id).then((response) => {
                    expect(response.statusCode).toBe(401);
                    expect(response.text).toBe('Unauthorized');
                    
                } )
            } )



      } )
} )