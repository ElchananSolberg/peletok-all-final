const models = require('../../index')
const sequelize = require('../../setupSequelize')
const request = require('supertest');
// const routing = require('../../routing')
const {productRouter} = require('../../api/productRouter')
const Product = models.product.model;
const bodyParser = require('body-parser');
var express = require('express');
const {getTypeNum, getNumStatus} = require('../../utils')
const {fileNameFormat} = require('./testUtil')
const moment = require('moment')
const path = require("path");

let truncateModel = (model) => {
       return sequelize.getQueryInterface().bulkDelete(model.getTableName(),{},{
           truncate: true,
           cascade: true,
       }, model)
}


describe('productRouterCreate', () => {

const initialEndDate = '2099-12-31'



    

    describe('POST /product', () => {

            let params;
            let current_user;
            let app;

            

            beforeAll(async  (done) => {
               
                
                 await sequelize.sync({
                         force: true,
                         logging: false
                     })
                
                 current_user = await models.user.model.create({
                      login_name: 'admin',
                     first_name: 'user',
                     last_name: 'family',   
                 })
                app = express();
                app.use(express.json());
                app.use(express.urlencoded({extended: false}));
                app.use(bodyParser.urlencoded({ extended: false })); 
                // mock req.user
                app.use((req, res, next)=>{      
                    req.user = {id: current_user.id};
                    next();
                })
                // app.use('/', routing)
                app.use('/product/', productRouter );                


                 let supplier = await models.supplier.model.create({
                     name: 'supplier name', 
                 })
                 let seller = await models.business.model.create({
                     name: 'seller name', 
                 })
                 let languages = await models.language.model.bulkCreate([
                     {
                        code: 'HE'
                     },
                     {
                        code: 'EN'
                     },
                     {
                        code: 'AR'
                     },
                 ])

                 let tags = await models.tag.model.bulkCreate([
                    {"tag_name":"cancellations","order":null,"type":1,"show_on_product":null,"id":1},
                    {"tag_name":"credits","order":1,"type":2,"show_on_product":null,"id":2},
                    {"tag_name":"המגזר החרדי","order":"1","type":1,"show_on_product":true,"id":3},
                    {"tag_name":"A שטחי","order":"2","type":2,"show_on_product":false,"id":4},
                    {"tag_name":"שטחי הרשות","order":"3","type":3,"show_on_product":true,"id":5}
                 ])

                 let barcodTypes = await models.barcodeType.model.bulkCreate([
                    {
                        name: 'peletok',
                        required: true,
                        status: 2,
                    },
                    {
                        name: 'delek',
                        required: false,
                        status: 2,
                    }
                 ])

                 
                 params = {
                    more_details_link: 'test',
                    video_link: 'test',
                    call_terms: 'text',
                    order: '4',
                    supplier_id: `${supplier.id}`,
                    id: '1',
                    status: '2',
                    // status_author_id: auther.id,
                    usage_time_limit: '5',
                    type: '2',
                    is_abstract: 'false',
                    profit_model: 'profit',
                    description: 'tttttttt',

                    call_minute: 30,
                    sms: 90,
                    browsing_package: '100',
                    
                    call_to_palestinian: true,
                    abroed_price: 12,
                    other1: 'test other1',
                    other2: 'test other1',
                    status: 1,
                    default_is_allow: true,
                    cancelable: true,

                    // productDetails fields
                    seller_id: seller.id, // ?
                    language_id: '',
                    name_he: 'he name',
                    name_en: 'en name',
                    name_ar: 'ar name',
                    product_title: 'product title',
                    description: 'product description',
                    description_he: 'he description',
                    description_en: 'en description',
                    description_ar: 'ar diescription',
                    more_info_description: 'more_info_description',
                    more_info_youtube_url: 'youtube_url',
                    more_info_site_url: 'site_url',
                    title: 'title',
                    title_he: 'he title',
                    title_en: 'en title',
                    title_ar: 'ar title',
                    name: 'product name',
                    name_by_language: '',
                    tags: "[1,2,3]",
                    // price
                    price: '23',
                    buying_price: '22',

                    barcodes: JSON.stringify([
                        {barcode: '1234', barcode_type_id: '1'},
                        {barcode: '1235', barcode_type_id: '2'},
                    ]),
                    talk_prices: JSON.stringify([
                        {country: "ישראל", outcomePrice: 7, incomePrice: 9, smsPrice: 10},
                        {country: "ארהב", outcomePrice: 7, incomePrice: 9, smsPrice: 10},
                        {country: "אוקרינה", outcomePrice: 7, incomePrice: 9, smsPrice: 10},
                        {country: "צרפת", outcomePrice: 7, incomePrice: 9, smsPrice: 10},
                        {country: "תל אביב", outcomePrice: 7, incomePrice: 9, smsPrice: 10},
                    ])
                 }
                
                 done()
            } )

            beforeEach(async  (done) => {
                await truncateModel(Product)
                await truncateModel(models.document.model)
                await truncateModel(models.productDetails.model)
                await truncateModel(models.productPrice.model)
                done()
            })

            

            it('Should retun status 200', () => {
                return request(app).post('/product/').then((response) => {
                    expect(response.statusCode).toBe(200);
                } )
            } )

             it('Should create one product',  () => {
                return request(app)
                    .post('/product/')
                    .send({

                    })
                    .then(async (response) => {
                    let products = await Product.findAll()
                    expect(products.length).toBe(1);
                } )
            } )

             test('product should have this values',  async (done) => {
                await request(app).post('/product/').field(params).then(res=>console.log(res.text))
                let product = await Product.findOne()
                expect(product).toMatchObject({
                    call_terms: params.call_terms,
                    more_info_description: params.more_info_description,
                    more_info_youtube_url: params.more_info_youtube_url,
                    more_info_site_url: params.more_info_site_url,
                    order: parseInt(params.order),
                    supplier_id: parseInt(params.supplier_id),
                    id: parseInt(params.id),
                    status: parseInt(params.status),
                    status_author_id: current_user.id,
                    // status_timestamp: params.status_timestamp,
                    usage_time_limit: parseInt(params.usage_time_limit),
                    type: parseInt(params.type),
                    is_abstract: false,
                    profit_model: params.profit_model,

                    call_minute: params.call_minute,
                    sms: params.sms,
                    browsing_package: params.browsing_package,
                    call_to_palestinian: params.call_to_palestinian,
                    abroed_price: params.abroed_price,
                    other1: params.other1,
                    other2: params.other2,
                    status: params.status ? getNumStatus('active', 'Product') : getNumStatus('inactive', 'Product'),
                    default_is_allow: params.default_is_allow,
                    cancelable: params.cancelable,
                })
                done()
                 
            } )

            test('3 items of productDetials should exist',  async (done) => {
                await request(app).post('/product/').field(params)
                let productDetails = await models.productDetails.model.findAll({ where: {product_id: 1}})
                expect(productDetails.length).toBe(3)
                done()
                 
            } )

            test('5 items of talkPrice should exist',  async (done) => {
                await request(app).post('/product/').field(params)
                let talkPrices = await models.talkPrice.model.findAll({ where: {product_id: 1}})
                expect(talkPrices.length).toBe(5)
                done()
                 
            } )

            test('talkPrice field should be',  async (done) => {
                await request(app).post('/product/').field(params)
                let talkPrice = await models.talkPrice.model.findOne({ 
                    where: {
                                product_id: 1,
                                country: "ישראל"
                            }
                    })
                expect(talkPrice).toMatchObject({
                    country: "ישראל", 
                    outcome_price: 7,
                    income_price: 9,
                    sms_price: 10
                 })
                done()
                 
            } )

            it('should save only tags with type product',  async (done) => {
                await request(app).post('/product/').field(params)
                let product = await Product.findOne()
                let tags = await product.getTags()
                expect(tags.length).toBe(1)
                done()
                 
            } )

            test('productDetials fields ',  async (done) => {
                await request(app).post('/product/').field(params)
                let productDetails = await models.productDetails.model.findAll({ where: {product_id: 1}})
                expect(productDetails.find(p=>p.language_code=='HE')).toMatchObject({
                    product_description: params.description_he,
                    product_name: params.name_he,
                    language_code: 'HE',
                })
                expect(productDetails.find(p=>p.language_code=='EN')).toMatchObject({
                    product_description: params.description_en,
                    product_name: params.name_en,
                    language_code: 'EN',
                })
                expect(productDetails.find(p=>p.language_code=='AR')).toMatchObject({
                    product_description: params.description_ar,
                    product_name: params.name_ar,
                    language_code: 'AR',
                })
                done()
                 
            } )

            test('3 items of productDetials should exist even if some language doesn\'t exist',  async (done) => {
                await request(app).post('/product/').field({})
                let productDetails = await models.productDetails.model.findAll({ where: {product_id: 1}})
                expect(productDetails.length).toBe(3)
                done()
                 
            } )

            // test('If some language doesn\'t exist (name and description) only others should be created', async (done) => {
            //     params.name_he = ''
            //     params.description = ''
            //     await request(app).post('/product/').field(params)
            //     let productDetails = await models.productDetails.model.findAll({ where: {product_id: 1}})
            //     expect(productDetails.length).toBe(2)
            //     done()
            // } )

            test('2 document should be exist', async (done) => {
                await request(app).post('/product/')
                .attach('image', path.join(__dirname,'wataniya-10.png'))
                .attach('more_info_image', path.join(__dirname,'wataniya-10.png'))
                .field(params)
                let docs = await models.document.model.findAll();
                expect(docs.length).toBe(2)
                done()
            } )

            test('documents should be type image and other_image', async (done) => {
                await request(app).post('/product/')
                .attach('image', path.join(__dirname,'wataniya-10.png'))
                .attach('more_info_image', path.join(__dirname,'wataniya-10.png'))
                .field(params)
                let docs = await models.document.model.findAll();
                expect(docs.filter(d=>d.type == getTypeNum('image', 'Document')).length).toBe(1)
                expect(docs.filter(d=>d.type == getTypeNum('other_image', 'Document')).length).toBe(1)
                

                done()
            } )

            test('document url should match name regex', async (done) => {
                await request(app).post('/product/')
                .attach('image', path.join(__dirname,'wataniya-10.png'))
                .attach('more_info_image', path.join(__dirname,'wataniya-10.png'))
                .field(params)
                let docs = await models.document.model.findAll();
                expect((docs.find(d=>d.type == getTypeNum('image', 'Document')) || {}).url).toEqual(
                    expect.stringMatching(fileNameFormat('productsImages', 'image'))
                    )
                expect((docs.find(d=>d.type == getTypeNum('other_image', 'Document')) || {}).url).toEqual(
                    expect.stringMatching(fileNameFormat('productsImages', 'more_info_image'))
                    )

                done()
            } )

            test('if image field doesn\'t exist or contain empty string document shouldn\'t created', async (done) => {
                // params.image = ''
                await request(app).post('/product/').field(params)
                let doc = await models.document.model.findOne();
                expect(doc).toBe(null);
                done()
            } )

            test('1 productPrice should be exist', async (done) => {
                await request(app).post('/product/').field(params)
                let docs = await models.productPrice.model.findAll();
                expect(docs.length).toBe(1)
                done()
            } )

            test('productPrice should contain', async (done) => {
                await request(app).post('/product/').field(params)
                let docs = await models.productPrice.model.findOne();
                expect(docs).toMatchObject({
                    fixed_price: parseInt(params.price),
                    buying_price: parseInt(params.buying_price),
                })
                expect(moment(docs.timestamp_end).isSame(moment(initialEndDate))).toBe(true)
                expect(moment(docs.timestamp_start).isBetween(moment().subtract(2, 'seconds'),moment() )).toBe(true)
                done()
            } )

            test('productPrice shouldn\'t created  if fixed and buing price doesn\'t exist', async (done) => {
                params.price = ''
                params.buying_price = ''
                await request(app).post('/product/').field(params)
                let doc = await models.productPrice.model.findOne();
                expect(doc).toBe(null);
                done()
            } )

            test('productPrice should created only if fixed or buing price exist', async (done) => {
                params.price = 33
                delete params.buying_price
                await request(app).post('/product/').field(params)
                let doc = await models.productPrice.model.findOne();
                expect(doc).not.toBe(null);
                done()
            } )

            it('should create two barcodes', async (done) => {
                
                await request(app).post('/product/').field(params)
                let prodactBarcode = await models.productBarcode.model.findAll();
                expect(prodactBarcode.length).toBe(2);
                done()
            } )

            test('barcodes should match ', async (done) => {
                
                await request(app).post('/product/').field(params)
                let prodactBarcodes = await models.productBarcode.model.findAll();
                expect(prodactBarcodes.filter(b=>b.id==2)).toMatchObject(JSON.parse(params.barcodes).filter(b=>b.id==2).map(b=>{
                    return {
                        barcode: b.barcode,
                        barcode_type_id: parseInt(b.barcode_type_id),
                    }
                }));

                expect(prodactBarcodes.filter(b=>b.id==1)).toMatchObject(JSON.parse(params.barcodes).filter(b=>b.id==1).map(b=>{
                    return {
                        barcode: b.barcode,
                        barcode_type_id: parseInt(b.barcode_type_id),
                    }
                }));
                done()
            } )
            


      } )
} )