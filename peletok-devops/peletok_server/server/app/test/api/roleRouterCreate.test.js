const models = require('../../index')
const sequelize = require('../../setupSequelize')
const request = require('supertest');
// const routing = require('../../routing')
const {rolesRouter} = require('../../api/rolesApi')
const bodyParser = require('body-parser');
var express = require('express');
const {getTypeNum, getNumStatus} = require('../../utils')
const {fileNameFormat} = require('./testUtil')
const moment = require('moment')
const path = require("path");

let truncateModel = (model) => {
       return sequelize.getQueryInterface().bulkDelete(model.getTableName(),{},{
           truncate: true,
           cascade: true,
       }, model)
}


describe('rolesRouter', () => {





    

    describe(' /role', () => {

            let current_user;
            let business;
            let app;

            

            beforeAll(async  (done) => {
                
                
                 await sequelize.sync({
                         force: true,
                         logging: false
                     })
                
                 current_user = await models.user.model.create({
                     login_name: 'admin',
                     first_name: 'user',
                     last_name: 'family',   
                 })

                 business = await models.business.model.create({
                     name: 'test',
                     business_identifier: 32,
                 })


                app = express();
                app.use(express.json());
                app.use(express.urlencoded({extended: false}));
                app.use(bodyParser.urlencoded({ extended: false })); 
                // mock req.user
                app.use((req, res, next)=>{      
                    req.user = {id: current_user.id, business_id: business.id};
                    next();
                })
                // app.use('/', routing)
                app.use('/roles/', rolesRouter );                

                 done()
            } )

            beforeEach(async (done)=>{
                await truncateModel(models.role.model)
                done()
            })

            

            describe('POST /roles', ()=>{

                it('Should retun status 200', () => {
                    return request(app).post('/roles/')
                    .send({name: 'chaim'})
                    .then((response) => {
                        expect(response.statusCode).toBe(200);
                    } )
                } )

                it('Should id and success msg', () => {
                    return request(app).post('/roles/')
                    .send({name: 'chaim'})
                    .then(async (response) => {
                        const role = await models.role.model.findOne()
                        expect(response.body.id).toBe(role.id);
                        expect(response.body.msg).toBe('success');
                    } )
                } )

                // it('Should mathc', () => {
                //     return request(app).get('/invoice/').then((response) => {
                //         expect(response.body).toMatchObject([{
                //             invoice_date:'2019-12-31T22:00:00.000Z',
                //             invoice_number: '12333',
                //             customer_number: business.business_identifier,
                //             path : path.join(__dirname,'32Invoice1-2019.pdf')
                //         },
                //         {
                //             invoice_date:'2019-12-31T22:00:00.000Z',
                //             invoice_number: '12346',
                //             customer_number: business.business_identifier,
                //             path : path.join(__dirname,'32Invoice1-2019.pdf')
                //         },]);
                //     } )
                // } )
            })

            

      } )
} )