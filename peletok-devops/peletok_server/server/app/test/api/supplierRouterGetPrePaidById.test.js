const models = require('../../index')
const sequelize = require('../../setupSequelize')
const request = require('supertest');
const routing = require('../../routing')
const bodyParser = require('body-parser');
var express = require('express');
const {getStrType, getTypeNum} = require('../../utils')
const {truncateModel} = require('./testUtil')
const {params, mockSuppliers} = require('./mockSuppliers')




describe('supplierRouterGetPrepaidById', () => {

    

    describe('GET /supplier/prepaid/:supplier_id', () => {

            
            let current_user;
            let app;
            let suppliers;
            let business;

            beforeAll(async  (done) => {

                await sequelize.sync({
                         force: true,
                         logging: false
                });
                
                [suppliers, current_user, business] = await mockSuppliers();
                
                app = express();
                app.use(express.json());
                app.use(express.urlencoded({extended: false}));
                app.use(bodyParser.urlencoded({ extended: false })); 
                // mock req.user
                app.use((req, res, next)=>{      
                    req.user = {
                        id: current_user.id,
                        business_id: business.id,
                        language_code: 'HE',
                        business_id: 1
                    };
                    next();
                })
                app.use('/', routing)
                
                done()
            } ) 

            it('Should retun status 200', () => {
                return request(app).get('/supplier/prepaid/1').then((response) => {
                    expect(response.statusCode).toBe(200);
                } )
            } )

            test('response should be ...', async () => {
                        let supplierId = 2;
                        await models.businessSupplier.model.update({is_authorized: false}, {
                            where: {is_authorized: true}
                        })
                        await models.businessSupplier.model.update({is_authorized: true}, {
                             where: {id: supplierId}
                        })
                        const supplier = await models.supplier.model.findOne({
                            where: {
                                id: supplierId,
                            }
                        })
                        
                        let images = await supplier.getDocument({
                            where: {type: getTypeNum('backgroundImage', 'Document')}
                        });
                      
                        let color = await supplier.getSupplierColor()
                        return request(app).get('/supplier/prepaid/' + supplierId).then(async(response) => {
                            
                            expect(response.body).toMatchObject(
                                {
                                    id: supplierId,
                                    background_image: images[0].url,
                                    chosen_card_background_color: color[0].chosen_background,
                                    product_card_background_color: color[0].product_background,
                                    favorite_star_color: color[0].favorite_star_color,
                                    logo_background_color:color[0],
                                    logo_background_color: color[0].logo_background,
                                    font_color: color[0].font,
                                       
                                }
                            )
                        })
                    
            })


    })
} )