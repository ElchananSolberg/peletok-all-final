
const models = require('../../index')
const sequelize = require('../../setupSequelize')
const request = require('supertest');
const routing = require('../../routing')
const bodyParser = require('body-parser');
var express = require('express');
const {getStrType, getTypeNum} = require('../../utils')
const {truncateModel} = require('./testUtil')
const {params, mockSuppliers} = require('./mockSuppliers')
const path = require("path");




describe('supplierRouterUpdate', () => {

    

    describe('PUT /supplier/:supplier_id', () => {

            
            let current_user;
            let app;
            let suppliers;
            let business;

            beforeAll(async  (done) => {

                await sequelize.sync({
                         force: true,
                         logging: false
                });
                
                [suppliers, current_user, business] = await mockSuppliers();
                
                app = express();
                app.use(express.json());
                app.use(express.urlencoded({extended: false}));
                app.use(bodyParser.urlencoded({ extended: false })); 
                // mock req.user
                app.use((req, res, next)=>{      
                    req.user = {
                        id: current_user.id,
                        business_id: business.id,
                        language_code: 'HE',
                        business_id: 1
                    };
                    next();
                })
                app.use('/', routing)
                
                done()
            } ) 

            

            it('should\'nt return unautorized supplier', () => {
                return request(app).put('/supplier/2').then((response) => {
                    expect(response.statusCode).toBe(401);
                    expect(response.text).toBe('Unauthorized');
                } )
            } )

            it('Should retun status 200', async() => {
                let supplierId = 2;
                        await models.businessSupplier.model.update({is_authorized: false}, {
                            where: {is_authorized: true}
                        })
                        await models.businessSupplier.model.update({is_authorized: true}, {
                             where: {supplier_id: supplierId}
                        })
                        
                return request(app).put('/supplier/2').then((response) => {
                    expect(response.statusCode).toBe(200);
                } )
            } )

            test('supplier  after apdate should be  ...', async () => {
                        let supplierId = 2;
                        await models.businessSupplier.model.update({is_authorized: false}, {
                            where: {is_authorized: true}
                        })
                        await models.businessSupplier.model.update({is_authorized: true}, {
                             where: {supplier_id: supplierId}
                        })
                       
                        const newParams = {
                            name: 'new name', 
                            type: 8, 
                            credit_card_id: 2, 
                            order: 16, 
                            status: 40, 
                            max_percentage_profit: 777, 
                            min_percentage_profit: 777, 
                            max_payment: 777,
                            
                        }
                        
                        return request(app).put('/supplier/' + supplierId)
                        .send(newParams)
                        .then(async(response) => {
                            const supplier = await models.supplier.model.findOne({
                                where: {
                                    id: supplierId,
                                }
                            })
                            expect(supplier).toMatchObject(newParams)
                        })

                    
            })

            test('supplier and associations after apdate should be  ...', async () => {
                        let supplierId = 2;
                        await models.businessSupplier.model.update({is_authorized: false}, {
                            where: {is_authorized: true}
                        })
                        await models.businessSupplier.model.update({is_authorized: true}, {
                             where: {supplier_id: supplierId}
                        })
                        
                        const newParams = {
                            name: 'new name', 
                            type: 8, 
                            credit_card_id: 2, 
                            order: 16, 
                            status: 40, 
                            max_percentage_profit: 777, 
                            min_percentage_profit: 777, 
                            max_payment: 777,
                            contact_phone_number: '022222777',
                            country: 'ארהב',
                            city: 'ניויורק',
                            street: 'aaa',
                            number: '77',
                            zip_code: '777',
                            contact_name: 'משה',
                        }
                        
                        return request(app).put('/supplier/' + supplierId)
                        .attach('image', path.join(__dirname,'wataniya-10.png')).field(newParams)
                        .then(async(response) => {
                            const supplier = await models.supplier.model.findOne({
                                where: {
                                    id: supplierId,
                                }
                            })
                      
                        const [images, contact, address, color] = await Promise.all(
                            [    
                                supplier.getDocument({
                                    where: {type: getTypeNum('logo', 'Document')}
                                }),
                                supplier.getSupplierContact(),
                                supplier.getAddress(),
                                supplier.getSupplierColor(),
                            ]
                        )
                            expect(supplier).toMatchObject({
                                name: newParams.name,
                                type: newParams.type,
                                credit_card_id: newParams.credit_card_id,
                                order: newParams.order,
                                status: newParams.status,
                                max_percentage_profit: newParams.max_percentage_profit,
                                min_percentage_profit: newParams.min_percentage_profit,
                                max_payment: newParams.max_payment,
                            })
                            
                            expect(contact).toMatchObject({
                               phone: newParams.contact_phone_number,
                               name: newParams.contact_name, 
                            })

                            expect(address).toMatchObject({
                                country: newParams.country,
                                city: newParams.city,
                                street: newParams.street,
                                zip_code: newParams.zip_code,
                            })
                            
                            expect(images[0].type).toBe(getTypeNum('logo', 'Document'))
                            expect(images[0].url).toEqual(
                                expect.stringMatching(/^\/image-wataniya-10-[0-9]{1,4}-[0-9]{1,2}-[0-9]{1,2}T[0-9]{1,2}-[0-9]{1,2}-[0-9]{1,2}\.png$/)
                            )
                        })

                        
                    
            })
            it('Shouldn\'t change if field doesn\'t exist', async () => {
                let supplierId = 1;
                await models.businessSupplier.model.update({is_authorized: false}, {
                    where: {is_authorized: true}
                })
                await models.businessSupplier.model.update({is_authorized: true}, {
                     where: {supplier_id: supplierId}
                })
                
                return request(app).put('/supplier/' + supplierId)
                    .send({})
                    .then(async(response) => {
                        const supplier = await models.supplier.model.findOne({
                            where: {
                                id: supplierId,
                            }
                        })
                  
                    const [images, contact, address, color] = await Promise.all(
                        [    
                            supplier.getDocument({
                                where: {type: getTypeNum('logo', 'Document')}
                            }),
                            supplier.getSupplierContact(),
                            supplier.getAddress(),
                            supplier.getSupplierColor(),
                        ]
                    )
                    expect(supplier).toMatchObject({
                        name: params.name + (supplierId -1),
                    })
                    
                    expect(contact).toMatchObject({
                       phone: params.contact_phone_number,
                       name: params.contact_name, 
                    })

                    expect(address).toMatchObject({
                        country: params.country,
                        city: params.city,
                        street: params.street,
                        zip_code: params.zip_code,
                    })
                    
                    expect(images[0].type).toBe(getTypeNum('logo', 'Document'))
                    expect(images[0].url).toBe(params.fileName + 'logo')
                } )
           }) 

    })
} )