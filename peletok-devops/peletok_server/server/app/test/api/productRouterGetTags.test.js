const models = require('../../index')
const sequelize = require('../../setupSequelize')
const request = require('supertest');
const routing = require('../../routing')
const bodyParser = require('body-parser');
var express = require('express');
const {getTypeNum} = require('../../utils')


describe('productRouterGetTags', () => {

    

    describe('GET /product/tags', () => {
            let app;
            
            beforeAll(async  (done) => {
                await sequelize.sync({
                         force: true,
                         logging: false
                 });
        
                
                app = express();
                app.use(express.json());
                app.use(express.urlencoded({extended: false}));
                app.use(bodyParser.urlencoded({ extended: false })); 
                // mock req.user
                
                app.use('/', routing)
                
                done()
            } )

            it('Should retun status 200', () => {
                return request(app).get('/product/tags/').then((response) => {
                    expect(response.statusCode).toBe(200);
                } )
            } )

  

            

            test('response should be ...', async () => {
                
                let tags = await models.tag.model.bulkCreate([
                    {"tag_name":"cancellations","order":null,"type":1,"show_on_product":null,"id":1},
                    {"tag_name":"credits","order":1,"type":2,"show_on_product":null,"id":2},
                    {"tag_name":"המגזר החרדי","order":"1","type":1,"show_on_product":true,"id":3},
                    {"tag_name":"A שטחי","order":"2","type":2,"show_on_product":false,"id":4},
                    {"tag_name":"שטחי הרשות","order":"3","type":3,"show_on_product":true,"id":5}
                ])
              

                return request(app).get('/product/tags').then(async(response) => {
                    expect(response.body).toMatchObject(tags.filter(t=>t.type==getTypeNum('product', 'Tag')).map(t=>{return {id: t.id, name: t.tag_name}}))
                 })
            })

           



      } )
} )