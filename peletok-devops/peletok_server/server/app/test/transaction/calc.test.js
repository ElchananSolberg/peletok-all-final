const sequelize = require('../../setupSequelize')
const models = require('../../index')
const moment = require('moment')
const Transactions = require('../../payment/transaction/transactions')
const initialEndDate = '2099-12-31'


let business;
let distributor;
let supplier;
let product;
let trx
describe('Transactions', () => {
     beforeAll(async () => {



        await sequelize.sync({
             force: true,
             logging: false
         })

        let VAT = await models.vATRate.model.create({
            value: 17
        })

        const user = await models.user.model.create({
            first_name: 'jon',
            last_name: 'due'
        })

        supplier = await models.supplier.model.create(
            {
                id:95,
                name:'פלאפון',
                type:1,
                max_percentage_profit:3,
                min_percentage_profit:0,
                max_payment:30,
                default_creation: true,
                // credit_card_id:1,
                // author_id:2
            })

        distributor = await models.business.model.create({
            name: 'peletok',
            BusinessFinance: [{
                balance: 2000
            }],
            Distributor: {}

        },{
            include: [
                {model: models.businessFinance.model, as: 'BusinessFinance'},
                {model: models.distributor.model, as: 'Distributor'}
            ]
        })

        
        business = await models.business.model.create({
            name: 'test',
            distributor_id: distributor.id,
            BusinessFinance: [{
                balance: 2000
            }]

        },{
            include: [
                {model: models.businessFinance.model, as: 'BusinessFinance'}
            ]
        })

        
        product = await models.product.model.create({
            profit_model: 'profit',
            supplier_id: supplier.id,
            ProductPrice: [
                {
                    fixed_price: 10,
                    buying_price: 12,
                    distribution_fee: 5,
                    timestamp_start: moment(),
                    timestamp_end: moment(initialEndDate)
                }
            ], 
            
        },{
            include: [
                
                {
                    model: models.productPrice.model, as: 'ProductPrice'
                },
            ]
        })

        trx = new Transactions()
        trx.currentTransaction = await models.transaction.model.create({
            transactionData: [{}]
        },{
           include: [
                {model: models.transactionData.model, as: 'transactionData'}
            ] 
        });
        trx.transactionData = trx.currentTransaction.transactionData[0];
        trx.req = {
            body: {itemId: product.id},
            user: {
                id: user.id,
                business_id: business.id,
            }
        };

        
     } )

     describe('Transactions.endTransaction()', () => {
        let transaction;
        describe('profitModel', () => {
            beforeAll(async ()=>{
                await models.businessProduct.model.update({
                    percentage_profit: 3,
                    business_commission: 3,
                    final_commission: 3,
                    points: 3,
                    distribution_fee: true,
                },{
                    where: {
                        product_id: product.id,
                        business_id: business.id,
                    }
                })

                await models.productPrice.model.update({
                    fixed_price: 3,
                    buying_price: 13,
                    distribution_fee: 3,
                },{
                    where: {
                        product_id: product.id,
                    }
                })

                await models.supplier.model.update({
                    max_percentage_profit: 3,
                    min_percentage_profit: 3,
                    max_payment: 3,
                },{
                    where: {
                        id: supplier.id,
                    }
                })

                await models.business.model.update({
                    use_article_20: true,
                    use_distribution_fee: true,
                    use_points: true,
                },{
                    where: {
                        id: business.id,
                    }
                })


                trx.price = (await product.ProductPrice[0].reload()).buying_price;
                trx.credit = business.BusinessFinance[0]
                trx.distributorCredit = distributor.BusinessFinance[0]
                await trx.endTransaction({
                    isPaid: true,
                    confirmation: true,
                    error: {},
                })
                transaction = await trx.currentTransaction.reload()
            })
            it('transaction.business_price_not_including_vat',()=>{
                expect(transaction.business_price_not_including_vat).toBe(13-0.3)
            })
            it('transaction.business_profit',()=>{
                expect(transaction.business_profit).toBe(0.3)
            })
            it('transaction.business_profit_not_including_vat',()=>{
                expect(transaction.business_profit_not_including_vat).toBe(0.256)
            })
            it('transaction.business_profit_vat',()=>{
                expect(transaction.business_profit_vat).toBe(0.051)
            })
            it('transaction.business_balance', async (done)=>{
                let finance = await business.BusinessFinance[0].reload()
                expect(transaction.business_balance).toBe(finance.balance)
                expect(transaction.business_balance).toBe(finance.balance)
                done()
            })
            it('transaction.distributor_price',()=>{
                expect(transaction.distributor_price).toBe(0.3)
            })
            it('transaction.distributor_price_not_including_vat',()=>{
                expect(transaction.distributor_price_not_including_vat).toBe(0.3)
            })
            it('transaction.distributor_profit',()=>{
                expect(transaction.distributor_profit).toBe(0.3)
            })
            it('transaction.distributor_profit_not_including_vat',()=>{
                expect(transaction.distributor_profit_not_including_vat).toBe(0.3)
            })
            it('transaction.distributor_profit_vat',()=>{
                expect(transaction.distributor_profit_vat).toBe(0.3)
            })
            it('transaction.distributor_balance',()=>{
                expect(transaction.distributor_balance).toBe(0.3)
            })

            
        })
        
        
        
    })
})
