const path = require('path');
const publicDir = path.join(__dirname, './rest/restData/public')
const currency=require('currency.js')
const bcrypt = require('bcryptjs');
const formidable = require('formidable')
const moment = require('moment')
const downloadedInvoicesDir = './rest/restData/public/downloadedInvoices'
const Op = require('sequelize').Op
const {PtMailer, peletokMail} = require('./ptMailer')
const fs = require('fs')
const { promisify } = require("util");
const renameFilePromise = promisify(fs.rename)

function getCurrentTime(start, end) {
    return new Date(new Date().toString().split('GMT')[0] + ' UTC').toISOString().replace(/-/g, '').replace(/:/g, '').slice(start, end)
}

async function isUserAuthProfiles(userId, profileIds){
    // Handle profiles authorization
    const models = require('./index')
    const UserRoles = models.role.model.scope({ method: ['userRoles', userId]});
    const authorizedRoles = (await UserRoles.findAll({attributes: ['id']})).map(r=>r.id)
    return (await models.business.model.count({
        where: {
            id: profileIds,
            is_abstract: true,
        },
        include: {
            model: models.role.model,
            where: {id: authorizedRoles},
            required: true
        }
    })) == profileIds.length
}

/*To exclude from  array by key
 * 
 */
function stringifyExcept(obj, keyToRemove) {

    return JSON.stringify(obj, function (k, v) {
        if (k !== keyToRemove) {
            return v;
        }
    });
}

/*
 * Checks israeli id
 * @param id string of israeli id
 * @returns {boolean}
 */
function israeliIdValidator(id) {
    id = String(id).trim();
    if (id.length > 9 || id.length < 5 || isNaN(id)) return false;

    // Pad string with zeros up to 9 digits
    id = id.length < 9 ? ("00000000" + id).slice(-9) : id;
    var is_valid = Array.from(id, Number)
        .reduce((counter, digit, i) => {
            const step = digit * ((i % 2) + 1);
            return counter + (step > 9 ? step - 9 : step);
        }) % 10 === 0;
    if (!is_valid) throw new Error(
        'israeli id not valid');
}


/**
 * Wrapper for response of DB query
 * @param queryResult result of query (null or object)
 * @param res response manager
 * @returns {boolean}
 */
function exist(queryResult, res) {
    if (queryResult == null) {
        res.status(401);
        res.send("not found");
        return false;
    }
    return true;
}



/***
 * Capitalize string
 * @param str string to capitalize
 * @returns Capitalize string
 */
function capitalize(str) {
    if (!str) return str;
    return str.charAt(0).toUpperCase() + str.slice(1);
}

/***
 * Checks object if empty
 * @param obj object to check
 * @returns boolean true if empty anf false if no
 */
function isObjectEmpty(obj) {
    return !obj || Object.keys(obj).length === 0;
}

/**
 * Checks if object is empty or include only empty objects
 * @param obj object to check
 * @return {boolean}
 */
function isObjectFullyEmpty(obj) {
    let isEmpty = true;
    if (Array.isArray(obj)) {
        Object.values(obj).forEach(o => {
            if (!isObjectEmpty(o)) isEmpty = false;
        });
        return isEmpty;
    }
    return isObjectEmpty(obj);
}


/***
 * changes the path field to name fields only (for example: change {UserAddress.city:
 * Jerusalem} to {city: Jerusalem})
 * @param obj object to change it
 * @param excludeNum num dots to not remove at the end key default 0
 */
function removeAbsolutePath(obj, excludeNum) {

    let excludeNumCopy = excludeNum;
    Object.keys(obj).forEach(originK => {
        excludeNumCopy = excludeNum;
        let k = originK;
        let indexSlice = k.lastIndexOf('.');
        while (excludeNumCopy && excludeNumCopy > 0 && indexSlice >= 0) {
            k = k.slice(0, indexSlice);
            indexSlice = k.lastIndexOf('.');
            excludeNumCopy--;
        }
        let newK = originK.slice(indexSlice + 1);
        if (newK !== originK && !obj[newK]) {
            obj[newK] = obj[originK];
            delete obj[originK];
        }
    });
}


/***
 * Returns null for 'null' and false for undefined and for 0 and for '0' and true for all other case
 * @param strData str boolean
 * @returns true or false or null
 */
function getBoolean(strData) {
    if (strData === "null" || strData === "") return null;
    // noinspection EqualityComparisonWithCoercionJS
    return strData && !(strData == 0);
}

/**
 * Contacts tow objects into new object
 * (If there is an identical key in both of them with different values
 * it will keep all values as array)
 * @param obj1 first object to concat
 * @param obj2 last object to concat
 * @return new object that contains all keys and values
 */
function mergeObjects(obj1, obj2) {
    if (!obj1) return Object.assign({}, obj2);
    if (!obj2) return Object.assign({}, obj1);
    let newObject = {};
    Object.keys(obj1).forEach(k => {
        if (obj1[k] && obj2[k] && obj1[k] !== obj2[k]) {
            newObject[k] = [obj1[k], obj2[k]]
        } else {
            newObject[k] = obj1[k] || obj2[k]
        }
    });
    Object.keys(obj2).forEach(k => {
        if (!newObject[k]) {
            newObject[k] = obj2[k];
        }
    });
    return newObject;
}


/**
 * Checks the specify cell in csv is empty ('""' or "null" are considered empty)
 * @param cell cell to checking
 * @returns boolean if empty and false if no
 */
function isEmptyCell(cell) {
    return !cell || cell === "null" || cell === "" || cell === '""';
}

/**
 * sort the received data(suppliers,products,etc..) by "type" attribute
 * @param {*} keyOfTypes 
 * @param {*} data 
 * @returns object with sorted lists
 *
 */
function mapByType(keyOfTypes, data) {
    let arrangedObj = {}
    const typesList = require("./config/types.json")[keyOfTypes]
    for (let i = 0; i < typesList.length; i++) {
        const type = typesList[i];
        arrangedObj[type] = [];
        for (let index = 0; index < data.length; index++) {
            const instance = data[index];
            // TODO: Check if type column in db zero based
            if (instance['type'] == (i + 1)) {
                arrangedObj[type].push(instance)
            }
        }
    }
    return arrangedObj

}

/**
 * Returns string of status by number
 * @param statusNumber number id of status
 * @param tableName table name
 * @returns {string} of status
 */
function getStrStatus(statusNumber, tableName) {
    const statuses = require("./config/statuses")[tableName];
    return statuses ? statuses[statusNumber - 1] : undefined;
}

/**
 * Returns number of status by string
 * @param statusString string of status
 * @param tableName table name
 * @returns {number} of status
 */
function getNumStatus(statusString, tableName) {
    const statuses = require("./config/statuses")[tableName];
    return statuses.indexOf(statusString) + 1;
}

function getStrType(typeNumber, tableName) {
    const types = require("./config/types")[tableName];
    return types ? types[typeNumber - 1] : undefined;
}


function getTypeNum(typeString, tableName) {
    const types = require("./config/types")[tableName];
    return types.indexOf(typeString) + 1;
}

/**
 * Returns session key by user id
 * @param sessionList list of active sessions
 * @param userId user id to search
 * @return {string} of session key
 */
function getSessionByUser(sessionList, userId) {
    const sessionKeys = Object.keys(sessionList);
    for (const s of sessionKeys) {
        let userBySession = JSON.parse(sessionList[s])["passport"]["user"];
        if (userBySession["id"] === userId) {
            return s;
        }
    }
}

/**
 * Deletes session key by user id
 * @param sessionList list of active sessions
 * @param userId user id to search
 * @return {Boolean}
 */
function deleteSessionByUser(sessionList, userId) {
    const sessionToDelete = getSessionByUser(sessionList, userId);
    if (sessionList) {
        delete sessionList[sessionToDelete]
    }
}

async function addHashWithSalt(userPass) {
    return await bcrypt.hashSync(userPass, await bcrypt.genSaltSync(10));
}


async function compareHash(incomingString, currentHash) {
    isCorrect = await bcrypt.compareSync(incomingString, currentHash);
    return isCorrect
}

/**
 * Checks if object is empty array
 * @param obj object to check
 * @return {boolean} is empty array
 */
function isEmptyArr(obj) {
    return obj instanceof Array && obj.length === 0;
}

/***
 * If one of the objects is an array,
 *  the method also transforms the second object into an array
 * @param obj1 first object
 * @param obj2 second object
 * @return {{obj2: Array, obj1: Array}}
 */
function changeToArrIfOneIsArr(obj1, obj2) {
    if (obj1 instanceof Array && !(obj2 instanceof Array) && obj2 instanceof Object) {
        obj2 = [obj2];
    }
    if (obj2 instanceof Array && !(obj1 instanceof Array) && obj1 instanceof Object) {
        obj1 = [obj1];
    }
    return { "obj1": obj1, "obj2": obj2 }
}
/**
 * calculate product price/payment without VAT
 * @param {*} price 
 */
async function VATReduction(price) {
    const models = require('./index.js')
    let VAT = await models.vATRate.model.findOne({
        order: [
            ['start_timestamp', 'DESC']
        ],
        attributes: ['value'],
        raw: true
    })
    let PercentageOfVAT = parseFloat(VAT.value)

    let PriceWithoutVAT = currency(price).divide(1 + PercentageOfVAT / 100).value

    return PriceWithoutVAT

}

/**
 * handel file transfer from req to server
 * @param {*} req 
 * @param {*} uploadDir 
 * @param {*} optionalFileName 
 */
function uploadFile(req, uploadDir, optionalFileName, receivedFileTypes = [], onlyReceivedTypes = false) {
    let fileTypes = !onlyReceivedTypes ? ['jpg', 'png', 'jpeg', 'svg', 'pdf'].concat(receivedFileTypes) : receivedFileTypes
    var form = new formidable.IncomingForm();
    form.uploadDir = uploadDir;
    return new Promise((resolve, reject) => {
        let data = {};
        form.onPart = function (part) {
            if (part.filename) {
                let fileType = part.filename.split('.').pop().toLowerCase();
                if (!fileTypes.includes(fileType)) {
                    resolve({ error: 'incorrect file type: ' + fileType });
                    return;
                }
            }

            form.handlePart(part);

        }
        form.on('fileBegin', function (name, file) {
            let timestamp = moment().toISOString().replace(":", "-").replace(":", "-");
            timestamp = timestamp.slice(0, timestamp.lastIndexOf("-") + 3);
            const fileName = `/${name}-${optionalFileName ? optionalFileName : file.name.split('/').pop().split('.')[0]}-${timestamp}.${file.name.split('.').pop()}`;
            file.path = form.uploadDir + fileName;
            data[name] = file.path.substring(23, file.path.length);
            data['nameAndDate'] = data[name]
            data["path_" + name] = path.join(form.uploadDir, fileName);
            data.fileName = fileName;
            data['validPath'] = file.path.substring(25, file.path.length);



        });
        form.on("field", function (name, value) {
            data[name] = value;


        });
        form.on('end', function () {

            resolve(data)
        });
        if (req.is("multipart/form-data")) {
            form.parse(req);
        }
        else {
            resolve(req.body)
        }

    }
    )
}

/**
 * handel file transfer from req to server
 * @param {*} req 
 * @param {*} uploadDir 
 * @param {*} optionalFileName 
 */
function extractFormDataAndUploadFiles(req, uploadDir, optionalFileName, receivedFileTypes = [], onlyReceivedTypes = false) {
    let fileTypes = !onlyReceivedTypes ? ['jpg', 'png', 'jpeg', 'svg', 'pdf'].concat(receivedFileTypes) : receivedFileTypes
    var form = new formidable.IncomingForm();
    form.uploadDir = path.join(publicDir, uploadDir);

    return new Promise((resolve, reject) => {
        let data = {};
        form.onPart = function (part) {
            if (part.filename) {
                let fileType = part.filename.split('.').pop().toLowerCase();
                if (!fileTypes.includes(fileType)) {
                    resolve({ error: 'incorrect file type: ' + fileType });
                    return;
                }
            }

            form.handlePart(part);

        }
        form.on('fileBegin', function (name, file) {
            let timestamp = moment().toISOString().replace(":", "-").replace(":", "-");
            timestamp = timestamp.slice(0, timestamp.lastIndexOf("-") + 3);
            const fileName = `/${name}-${optionalFileName ? optionalFileName : file.name.split('/').pop().split('.')[0]}-${timestamp}.${file.name.split('.').pop()}`;
            file.path = form.uploadDir + fileName;
            data[name] = '/' + uploadDir + fileName;

        });
        form.on("field", function (name, value) {
            data[name] = value;


        });
        form.on('end', function () {
            resolve(data)
        });
        if (req.is("multipart/form-data")) {
            form.parse(req);
        }
        else {
            resolve(req.body)
        }

    }
    )
}

/**
 * Apply Commissions and percentage profit on business and distributors
 * @param {*} req 
 * @param {*} res 
 */
async function applyCommissions(req, res) {
    const { Info } = require("./mapping/crudMiddleware");
    let info = new Info()
    if (req.user.is_distributor) {
        const models = require('./index')
        let businessUnder = { distributor_id: req.user.distributor_id }
        let businessWhereObj = {}
        businessProductWhereObj = {}
        let updateObj = {}
        let permittedBusinessIds = []
        if (req.user.is_admin) {
            businessWhereObj = { [Op.and]: [{ [Op.or]: [businessUnder, { is_distributor: true }] }, { [Op.not]: { id: req.user.business_id } }] }
        }
        else {
            businessWhereObj = { [Op.and]: [businessUnder, { [Op.not]: { id: req.user.business_id } }] }
        }
        if (req.body['business_id'])
            businessWhereObj = { [Op.and]: [businessWhereObj, { "id": req.body['business_id'] }] };
        let businesses = await models.business.model.findAll({ where: businessWhereObj, attributes: ['id'], raw: true })
        businesses.map((business) => {
            permittedBusinessIds.push(business.id)
        })
        businessProductWhereObj['business_id'] = permittedBusinessIds
        if (req.body['product_id']) {
            businessProductWhereObj['product_id'] = req.body['product_id']
        }
        if (req.body['business_commission']) {
            updateObj['business_commission'] = req.body['business_commission']
        }
        if (req.body['final_commission']) {
            updateObj['final_commission'] = req.body['final_commission']
        }
        if (req.body['is_authorized']) {
            updateObj['is_authorized'] = req.body['is_authorized']
        }
        if (req.body['percentage_profit']) {
            updateObj['percentage_profit'] = req.body['percentage_profit']
        }
        await models.businessProduct.model.update(updateObj, { where: businessProductWhereObj })
        info.message = 'success'
    } else {
        info.errors.push = 'Unauthorized'
    }
    res.customSend(info)
}
/** checks if action already performed on received number in the last minutes according to its definition in config file
 * 
 * @param {*} contract_number 
 */
async function is_charge_time_limited(contract_number) {
    const Op = require('sequelize').Op
    let models = require('./index.js')
    const { getChargeLimitTime } = require('./config/config')
    const chargeLimitedTime = getChargeLimitTime()
    let lastTransaction = await models.transactionData.model.findOne({
        where: {
            contract_number: contract_number,
            created_at: {
                [Op.between]: [moment().subtract(chargeLimitedTime, 'minutes').toDate(),
                moment().toDate()
                ]
            }
        }
    })
    return Boolean(lastTransaction)
}

// Get object with dot seperated string as keys and return object wirh the  last element as key.
// If removId is true all attributes with  id key will be removed
let cleanRawKeys = (model, removeId = true) => {
    return Object.keys(model)
        .filter(k => !removeId || k.split('.').pop() != 'id')
        .reduce((clean, k) => Object.assign(clean, { [k.split('.').pop()]: model[k] }), {})
}
/**
 * generates 6 digits verification code
 */
function generateValidationCode() { 
    var digits = '0123456789'; 
    let code = ''; 
    for (let i = 0; i < 6; i++ ) { 
        code += digits[Math.floor(Math.random() * 10)]; 
    } 
    return code; 
} 

async function uploadInvoiceAndSendMAil(req, res) {
    const LM = require('./config/LanguageManager').LM;
    const formidable = require('formidable');
    const moment = require("moment");
    const pdf = require('pdf-parse')
    let models = require('./index.js')
    var form = new formidable.IncomingForm();
    form.uploadDir = './';
    form.parse(req);
    const { Info } = require("./mapping/crudMiddleware");
    let info = new Info()
    let invoiceModel;
    return new Promise((resolve, reject) => {
        form.on('file', async (name, file) => {
            const pdfFile = fs.readFileSync(file.path)
            let invoice = await pdf(pdfFile).catch((err)=>{
                info.errors.push(err)
               reject(info)
               return
            })
            if (invoice) {
                let business_identifier = invoice.text.split('מפתח')[1].split('\n')[0];
                if(!business_identifier || !/^\d+$/.test(business_identifier)){
                    info.message = LM.getString('fileNotValid')
                    resolve(info);
                    return;
                }
                let customer = await models.business.model.findOne({
                    where: {business_identifier: business_identifier}
                })
                if(!customer){
                    info.message = LM.getString('customerNotFound')
                    resolve(info);
                    return;
                } else if (customer.invoice_email) {
                    let invoice_number = invoice.text.split('חשבונית מס\n')[1].split('\n')[0];
                    if((await models.invoice.model.count({where: {invoice_number}})) > 0){
                        info.message = LM.getString('invoiceExist')
                        resolve(info);
                        return;
                    }
                    let date = invoice.text.split('סוכן\n')[1].split('\n')[0].split('/').join('-');
                    let time = invoice.text.split('מקור\n')[1].split('\n')[0]
                    let dateAndTime = `${date} ${time}`
                    let momentDate = moment(dateAndTime,'DD-MM-YYYY HH:mm').format("DD/MM/YYYY HH:mm")
                    let finalDate = moment(momentDate, "DD/MM/YYYY HH:mm").toDate()
                    if (!fs.existsSync(__dirname + `/rest/restData/public/invoices/${business_identifier}/`)) {
                        fs.mkdirSync(__dirname + `/rest/restData/public/invoices/${business_identifier}/`);
                    }
                    await renameFilePromise(file.path, __dirname + `/rest/restData/public/invoices/${business_identifier}/${date}-${invoice_number}-${business_identifier}.pdf`).catch((err) => {
                        info.errors.push(err)
                        resolve(info)
                        return
                    })
        
                    invoiceModel = await models.invoice.model.create({
                        invoice_date: finalDate,
                        invoice_number: invoice_number,
                        customer_number: business_identifier,
                        path: `/rest/restData/public/invoices/${business_identifier}/${date}-${invoice_number}-${business_identifier}.pdf`
                    }).catch((err)=>{
                        info.errors.push(err)
                    })
                    if(invoiceModel){
                        let attachments = [invoiceModel.path].map(p=>{return {fileName: p.split('.')[0], path: path.join(__dirname,  p)}})
                        await PtMailer.send(peletokMail, customer.invoice_email, LM.getString('invoice_mail_subject'), LM.getString('invoice_mail_text'), null, attachments )
                        .catch(err=>{
                            info.errors.push(err)
                            reject(info)
                            return
                        })
                    }
                    resolve(info);
                    return;
                
                } else {
                    
                    info.message = LM.getString('customerNotSubscriced')
                    resolve(info);
                    return;
                }
            }else{
                info.errors.push('error : no data in pdf')
                resolve(info)
            }
        })
        

    }
    )
}

function cleanKeys(obj, charsToRemove){
    if(obj instanceof Object == false){
        return obj
    }else if (obj instanceof Array == true){
        obj.map(o=>cleanKeys(o, charsToRemove))
    } else {
        return Object.keys(obj).reduce((n, k) => Object.assign(n, {[charsToRemove.reduce((s,char)=>s.split(char).join(''),k)]: cleanKeys(obj[k], charsToRemove)}),{})
    }
}





module.exports.stringifyExcept = stringifyExcept;
module.exports.cleanRawKeys = cleanRawKeys;
module.exports.israeliIdValidator = israeliIdValidator;
module.exports.exist = exist;
module.exports.capitalize = capitalize;
module.exports.isObjectEmpty = isObjectEmpty;
module.exports.isObjectFullyEmpty = isObjectFullyEmpty;
module.exports.removeAbsolutePath = removeAbsolutePath;
module.exports.getBoolean = getBoolean;
module.exports.mergeObjects = mergeObjects;
module.exports.isEmptyCell = isEmptyCell;
module.exports.mapByType = mapByType;
module.exports.getStrStatus = getStrStatus;
module.exports.getNumStatus = getNumStatus;
module.exports.deleteSessionByUser = deleteSessionByUser;
module.exports.getStrType = getStrType;
module.exports.getTypeNum = getTypeNum;
module.exports.addHashWithSalt = addHashWithSalt;
module.exports.compareHash = compareHash;
module.exports.isEmptyArr = isEmptyArr;
module.exports.changeToArrIfOneIsArr = changeToArrIfOneIsArr;
module.exports.uploadFile = uploadFile;
module.exports.extractFormDataAndUploadFiles = extractFormDataAndUploadFiles;
module.exports.getCurrentTime = getCurrentTime
module.exports.VATReduction = VATReduction
module.exports.uploadInvoiceAndSendMAil = uploadInvoiceAndSendMAil
module.exports.applyCommissions = applyCommissions
module.exports.is_charge_time_limited = is_charge_time_limited
module.exports.generateValidationCode = generateValidationCode
module.exports.isUserAuthProfiles = isUserAuthProfiles
module.exports.cleanKeys = cleanKeys
