'use strict';
const {addColumnsToTable} = require('../dbUtils/addPermission')
module.exports = {
  up: async (queryInterface, Sequelize) => {
  await  addColumnsToTable(queryInterface, 'TRNSC_Transaction', [{
      name: 'supplier_balance',  typeOptions: {
        type: Sequelize.DOUBLE,
        allowNull: true
      }
    }])
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn('TRNSC_Transaction', 'supplier_balance', { transaction: t })
      ])
    })
  }
};