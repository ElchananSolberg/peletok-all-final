'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('BusinessRole',
      {
        business_id: {
          type: Sequelize.INTEGER,
        },
        role_id: {
          type: Sequelize.INTEGER,
        },
        created_at: Sequelize.DATE,
        updated_at: Sequelize.DATE,
        deleted_at: Sequelize.DATE,
        
      }
  )
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('BusinessRole')
  }
};
