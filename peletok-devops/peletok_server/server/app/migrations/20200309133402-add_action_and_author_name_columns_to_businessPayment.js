'use strict';
const {addColumnsToTable} = require('../dbUtils/addPermission')
module.exports = {
  up: async (queryInterface, Sequelize) => {
  await  addColumnsToTable(queryInterface, 'BNK_BusinessPayment', [{
      name: 'action', typeOptions: {
        type: Sequelize.INTEGER,
        allowNull: true
      }
    },
    {
      name: 'author_name', typeOptions: {
        type: Sequelize.STRING,
        allowNull: true
      }
    }])
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn('BNK_BusinessPayment', 'action', { transaction: t }),
        queryInterface.removeColumn('BNK_BusinessPayment', 'author_name', { transaction: t }),
      ])
    })
  }
};