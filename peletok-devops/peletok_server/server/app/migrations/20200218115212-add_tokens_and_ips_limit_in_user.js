'use strict';
const {addColumnsToTable} = require('../dbUtils/addPermission')
module.exports = {
  up: async (queryInterface, Sequelize) => {
  await  addColumnsToTable(queryInterface, 'USR_User', [{
      name: 'num_ip_allowed', typeOptions: {
        type: Sequelize.INTEGER,
        allowNull: true
      }
    },{
      name: 'num_device_allowed', typeOptions: {
        type: Sequelize.INTEGER,
        allowNull: true
      }, 
    }])
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn('User', 'num_ip_allowed', { transaction: t }),
        queryInterface.removeColumn('User', 'num_device_allowed', { transaction: t })
      ])
    })
  }
};