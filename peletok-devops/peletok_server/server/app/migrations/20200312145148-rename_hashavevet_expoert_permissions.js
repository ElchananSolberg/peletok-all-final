'use strict';
const {addPermission, renamePermission} = require('../dbUtils/addPermission');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      renamePermission(queryInterface, '/reports/hashavshevet/exportOrdersToHashavshevetReport', '/reports/hashavshevet/export/profitExport'),
      renamePermission(queryInterface, '/reports/hashavshevet/exportCommissionsToHashavshevetReport', '/reports/hashavshevet/export/CommissionExport'),
      renamePermission(queryInterface, '/reports/hashavshevet/ordersExportedToHashavshevetReport', '/reports/hashavshevet/export/report'),
    ])
  },

  down: (queryInterface, Sequelize) => {
   return Promise.resolve()
  }
};
