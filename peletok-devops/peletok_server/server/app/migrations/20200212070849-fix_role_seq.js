'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.query('ALTER SEQUENCE "PRM_Role_id_seq" RESTART WITH 5')
  },

  down: (queryInterface, Sequelize) => {
    return Promise.resolve()
  }
};
