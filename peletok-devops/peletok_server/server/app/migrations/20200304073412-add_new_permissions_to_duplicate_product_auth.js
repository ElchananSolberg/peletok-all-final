'use strict';
const {addPermission, renamePermission} = require('../dbUtils/addPermission');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      addPermission(queryInterface, 'שכפול הרשאות מוצר', 'api', 'GET /product/clone_porduct_auth', 13),
    ])
  },

  down: (queryInterface, Sequelize) => {
   return Promise.resolve()
  }
};
