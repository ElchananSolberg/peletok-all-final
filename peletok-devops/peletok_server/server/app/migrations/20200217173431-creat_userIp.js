'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('USR_UserIp', {
      id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER
        },
        ip_address: {
        type: Sequelize.STRING(24),
        allowNull: true
        },
        user_id: {
          type: Sequelize.INTEGER,
        },
        created_at: Sequelize.DATE,
        updated_at: Sequelize.DATE,
        deleted_at: Sequelize.DATE,
      
      });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('USR_UserIp');
  }
};
