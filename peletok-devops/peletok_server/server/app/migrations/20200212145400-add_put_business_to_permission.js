'use strict';
const {addPermission} = require('../dbUtils/addPermission');

module.exports = {
  up: (queryInterface, Sequelize) => {
   return addPermission(queryInterface, 'עדכון עסקים', 'api', 'PUT /business/business', 12)
  },

  down: (queryInterface, Sequelize) => {
   return Promise.resolve()
  }
};
