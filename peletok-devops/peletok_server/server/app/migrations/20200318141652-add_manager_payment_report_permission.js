'use strict';
const {addPermission, renamePermission} = require('../dbUtils/addPermission');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      addPermission(queryInterface, 'קבלת דוחות תשלומים', 'api', 'GET /report/managerPayments', 25),
    ])
  },

  down: (queryInterface, Sequelize) => {
   return Promise.resolve()
  }
};
