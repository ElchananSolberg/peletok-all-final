'use strict';
const moment = require('moment');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('MSG_TagOfMessage', [{
      tag_name: "notifications",
      created_at: moment().toDate(),
      updated_at: moment().toDate(),
    }],{})
  },

  down: (queryInterface, Sequelize) => {
    Promise.resolve()
  }
};
