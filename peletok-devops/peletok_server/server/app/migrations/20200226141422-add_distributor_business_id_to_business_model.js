'use strict';
const {addColumnsToTable} = require('../dbUtils/addPermission')
module.exports = {
  up: async (queryInterface, Sequelize) => {
  await  addColumnsToTable(queryInterface, 'BSN_Business', [{
      name: 'distributor_business_id', typeOptions: {
        type: Sequelize.INTEGER,
        allowNull: true
      }
    }])
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn('Business', 'distributor_business_id', { transaction: t }),

      ])
    })
  }
};