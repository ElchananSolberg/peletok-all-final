'use strict';
const {addPermission} = require('../dbUtils/addPermission');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return addPermission(queryInterface, 'חיבור לרב-קו', 'api', 'POST /rav_kav', 11)
   },
 
   down: (queryInterface, Sequelize) => {
    return Promise.resolve()
   }
};
