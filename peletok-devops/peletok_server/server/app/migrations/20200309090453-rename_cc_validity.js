'use strict';
const {renameColumnsIfExist, removeColumnsIfExist} = require('../dbUtils/addPermission');
module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      renameColumnsIfExist(queryInterface, 'BNK_CreditCard', ['cc_validity_month', 'cc_validity']),
      removeColumnsIfExist(queryInterface, 'BNK_CreditCard', ['cc_validity_year'])
    ])
  },

  down: (queryInterface, Sequelize) => {
    return Promise.resolve()
  }
};
