const DataTypes = require('sequelize').DataTypes;
const BaseModel = require('../../BaseModel');
/**
 * BranchBankAccount model
 */

class BranchBankAccount extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            timestamp: {
                type: DataTypes.DATE,
                allowNull: true
            }
        }
        this.author = true;
        this.statusAuthor = true;
        this.statusField = true;
        this.statusTimestampField = true;
        this.prefix = 'BNK';
    }
    /**
     * creates associate for BranchBankAccount model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.branch.model, { as: 'Branch' })
        this.model.belongsTo(models.bankAccount.model, { as: 'BankAccount' })

    }


}

module.exports = new BranchBankAccount();