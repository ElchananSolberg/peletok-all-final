const moment = require('moment');

const logger = require("../../logger");
const BaseModel = require('../../BaseModel');
const { DataTypes } = require('sequelize');

/**
 * BusinessFinance model
 */
class BusinessFinance extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            frame: {
                type: DataTypes.DOUBLE,
                allowNull: false,
                defaultValue: 0
            },
            temp_frame: {
                type: DataTypes.DOUBLE,
                allowNull: false,
                defaultValue: 0
            },
            balance: {
                type: DataTypes.DOUBLE,
                allowNull: false,
                defaultValue: 0
            },
            comment: {
                type: DataTypes.TEXT,
                allowNull: true
            }
        };
        this.author = true;
        this.statusAuthor = true;
        this.statusTimestampField = true;
        this.statusField = true;
        this.prefix = 'BNK';
    }
    /**
     * creates associate for BusinessFinance model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.business.model, { as: 'Business' });
    }
}

let businessFinance = new BusinessFinance();
// businessFinance.model.beforeUpdate(async (instance, options) => {
//     //Warning: The code's working assumption here is that the instance includes all relevant fields from
//     // the instance we want to update (for example, business_id)
//     if (instance._changed.frame) {
//         instance.temp_frame = 0
//     }
//     if (instance._changed.frame || instance._changed.temp_frame) {
//         await instance.destroy(); // soft delete row
//         delete instance.dataValues.id; // new row for updated data with new id
//         instance.dataValues.deleted_at = null; // new row not deleted
//         instance.dataValues.updated_at = null; // updated now and created from old row
//         await businessFinance.model.create(instance.dataValues); // new row
//         instance._changed = {}; // So that it does not call the update method of sequelize
//     }
// });

businessFinance.model.afterSave(async (instance, options)=> {
    const businessFinanceLogModel = require("../../index").businessFinanceLog.model;
    let timestampStart = moment().toDate();
    const dataValues = {...instance.dataValues};
    delete dataValues.id;
    const optionsForOldRow = {
        where: {
            business_id: dataValues.business_id,
            timestamp_end: null
        },
    };
    if (options.transaction) {
        optionsForOldRow.transaction = options.transaction
    }
    await businessFinanceLogModel.update({timestamp_end: timestampStart}, optionsForOldRow).catch(async e => {
        if (options.transaction) {
            logger.error(e);
            await transaction.rollback();
        }
    });
    const optionsForNewRow = {};
    if (options.transaction) {
        optionsForNewRow.transaction = options.transaction
    }
    await businessFinanceLogModel.create({...dataValues, timestamp_start: timestampStart}, optionsForNewRow).catch(async e=>{
        if(options.transaction){
            logger.error(e);
            await transaction.rollback();
        }
    });
});

module.exports = businessFinance;
