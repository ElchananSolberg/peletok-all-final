const BaseModel = require('../../BaseModel');
const { DataTypes } = require('sequelize')

/**
 * BusinessFinanceLog model
 */
class BusinessFinanceLog extends BaseModel {
    /**
     * configs fields and options
     */
    config() {
        this.attributes = {
            frame: {
                type: DataTypes.DOUBLE,
                allowNull: false,
                defaultValue: 0
            },
            temp_frame: {
                type: DataTypes.DOUBLE,
                allowNull: false,
                defaultValue: 0
            },
            balance: {
                type: DataTypes.DOUBLE,
                allowNull: false,
                defaultValue: 0
            },
            timestamp_start: {
                type: DataTypes.DATE,
                allowNull: false
            },
            timestamp_end: {
                type: DataTypes.DATE,
                allowNull: true
            },
            comment: {
                type: DataTypes.TEXT,
                allowNull: true
            }
        };
        this.author = true;
        this.statusAuthor = true;
        this.statusTimestampField = true;
        this.statusField = true;
        this.prefix = 'BNK';
    }
    /**
     * creates associate for BusinessFinanceLog model
     * @param models: objects in format {modelName: model}
     */
    associate(models) {
        this.model.belongsTo(models.business.model, { as: 'Business' });
    }
}

module.exports = new BusinessFinanceLog();
