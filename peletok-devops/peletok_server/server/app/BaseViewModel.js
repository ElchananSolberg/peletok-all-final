const sequelizeInstance = require('./setupSequelize');
const DataTypes = require('sequelize').DataTypes;


/**
 * General configuration for view models
 * A instance represents a model and the instance.model represents Sequelize model
 */
class BaseViewModel {

    /**
     * Constructor
     * Configs the General configuration
     */
    constructor() {
        this.attributes = {}; // fields
        this.options = {'underscored': true, 'paranoid': true}; // base options
        this.prefix = '';
        this.statusTimestampField = false;
        this.statusField = false;
        this.author = false;
        this.statusAuthor = false;

        // Sequelize model name, default is model name
        this.modelName = this.constructor.name;
        this.getDropCommand = null;
        this.getSyncCommand = null;
        this.config();
        this.define();
    }

    /***
     * Configs model
     */
    config() {
    }

    /**
     * defines Sequelize model
     */
    define() {
        if (this.prefix !== '') this.prefix += '_';
        this.addStatusFields();
        this.addAuthorFields();
        this.options["tableName"] = this.prefix + this.modelName;
        this.model = sequelizeInstance.define(
            this.modelName,
            this.attributes,
            this.options)
    }

    /**
     * Throws error if a Sequelize model for this model not defined yet
     */
    isDefined() {
        if (!this.model) {
            throw "a define method must be called before the associate method"
        }
    }


    /**
     * Adds a fields: author_id and status_author_id
     */
    addAuthorFields() {
        if (this.author) {
            this.attributes["author_id"] = {
                type: DataTypes.INTEGER
            }
        }
        if (this.statusAuthor) {
            this.attributes["status_author_id"] = {
                type: DataTypes.INTEGER

            }
        }
    }


    /**
     * Returns all fields of model
     * @returns {string[]}
     */
    getFields() {
        this.isDefined();
        return Object.keys(this.model.attributes)
    }


    /***
     * Adds a fields: status and statusTimestamp
     */
    addStatusFields() {
        if (this.statusField) {
            this.attributes["status"] = {
                type: DataTypes.INTEGER,
                allowNull: true
            }
        }
        if (this.statusTimestampField) {
            this.attributes["status_timestamp"] = {
                type: DataTypes.DATE,
                allowNull: true
            }
        }

    }

    /**
     * Syncs model in DB
     * @param options {force: true} or {force: false}
     * @return {Promise<void>}
     */
    async sync(options = sequelizeInstance.options) {
        if (!this.getSyncCommand) {
            throw Error("sync query not defined")
        }
        if (options["force"]) {
            await this.drop(options);
        }
        const command = this.getSyncCommand(this.options["tableName"]);
        await sequelizeInstance.query(command, {
            logging: options.logging,
        });
    };

    /**
     * Drops model in DB
     * @param options {force: true} or {force: false}
     * @return {Promise<void>}
     */
    async drop(options = sequelizeInstance.options) {
        if (!this.getDropCommand) {
            throw Error("drop query not defined")
        }
        const command = this.getDropCommand(this.options["tableName"]);
        return sequelizeInstance.query(command, {
            logging: options.logging,
        });
    };

}

module.exports = BaseViewModel;
