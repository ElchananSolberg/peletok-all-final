var logs = require('./logger')



/***
 * class for  info/ error 
 */
class ResInfo {
    constructor(req) {
        this.req = req
        this.error = {
            message: false,
            messageForLog: false,
            logingError: false,
            noBills: false,
            err_in_SubscriberNumber:false,
            errId_or_invoiceNumber: false,
            err_AmoutBill_or_billPaid: false,
            err_connection_site: false,
            err_in_Payment: false,
            err_in_productId: false,
            cardNotAvailable: false,
            err_connection_site_step1:false
        };

        this.statusNumbers = {
            logingError: 3,
            noBills: 4,
            errId_or_invoiceNumber: 5,
            err_AmoutBill_or_billPaid: 6,
            err_connection_site: 7,
            err_in_Payment: 8,
            err_in_productId: 9,
            cardNotAvailable: 10,
            err_connection_site: 11,

         
        };

        this.send = false
        this.noConfirmation = false
        this.isPaid = false
        this.confirmation = false
        this.price=false
        this.kod=false
        this.makat=false
        this.details=false
        this.customerId=false
        this.billNumber=false
        this.externalId=false
        this.comment=false
    
    }

    /***
  * To get error  message  and  sent to logger.error + to  info.message  
  * and to client  to  send message  ,  to log messageForLog (if there is a need to separate)
  */
    setErrMessage(message, messageForLog) {
        this.error.message = message;
        this.error.messageForLog = messageForLog || message;
        logs.error(this.error.messageForLog, this.req)
    }

    getStatus() {
        return Object.keys(this.error).map((key) => {
            return this.error[key] && this.statusNumbers[key] ? this.statusNumbers[key] : false
        }).filter(number => number)

    }

}
module.exports = ResInfo;