const fs = require("fs")
const logger= require('../logger')
// const models = require('../../index');
const xmlTemplate = fs.readFileSync(__dirname + "/shamirSmsTemplate.xml")
// const LM = require('../../config/LanguageManager').LM
const axios = require('axios').default;
const utils = require("../utils")
const xml2js = require('xml2js')
const userName = process.env.SHAMIR_USER_NAME || ""
const apiToken = process.env.SHAMIR_API_TOKEN || ""
const senderPhone = process.env.SHAMIR_SENDER_PHONE || ""
const parser = new xml2js.Parser();
var builder = new xml2js.Builder();
const util = require('util')
let xmlObj;
const inforuUrl ='https://uapi.inforu.co.il/SendMessageXml.ashx'
/**
 * adapter class for sending sms messages from 'Shamir systems' service
 */
class ShamirSmsAdapter{
/**
 * Imports xml file.
 * @param {*} xml 
 * @returns {object} xml that converted to obj
 */ 
    async importTemplate(xml) {
        return new Promise(resolve => {
            parser.parseString(
                xml,
                (error, result) => {
                    resolve(result)
                });
        })
    }
    /**
     * Uses to send sms messages via 'Shamir systems' 
     * @param {*} phoneNumber 
     * @param {*} message 
     */
    async send(phoneNumber,message){
        let resResult;
        let baseData = (await this.importTemplate(xmlTemplate))
        baseData['Inforu']['User'][0]['ApiToken'] = apiToken
        baseData['Inforu']['User'][0]['Username'] = userName
        baseData['Inforu']['Content'][0]['Message'] = message
        baseData['Inforu']['Settings'][0]['Sender'] = senderPhone
        baseData['Inforu']['Recipients'][0]['PhoneNumber'][0] = phoneNumber
        let readyXml =builder.buildObject(baseData)
        await axios({
            method: 'post',
            url: inforuUrl,
            params:{InforuXML:readyXml}
        }).then(async res=>{
            let responseData = await this.importTemplate(res.data)
            resResult = responseData.Result
        }).catch(err => {
            logger.error('Failed to send sms message to client'+ err)
            return
        })
        return resResult
    }
    

}    

module.exports = ShamirSmsAdapter;
