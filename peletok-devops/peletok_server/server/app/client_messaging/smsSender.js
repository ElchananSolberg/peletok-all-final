const shamirAdapter = require('./shamirSmsAdapter')


/**
 * Wrapper  class that gets sms service adapter and uses to send sms messages
 */
class SmsSender{
    constructor(adapter) {
        this.adapter = adapter
    }

    async send(phoneNumber,message){
      return  this.adapter.send(phoneNumber,message)
    }
    
}

module.exports = new SmsSender(new shamirAdapter()) ;