import React from 'react';
import './btnUtils.css'

export class CollapseChild extends React.Component {
        

    render(){
        return (
            <svg className="collapse-child"
                onClick={()=>{this.props.onClick();this.toggle()}} 
                height="32" 
                viewBox="0 0 25 50"
                style={{fillRule:'evenodd',clipRule:'evenodd',strokeLinecap:'round',strokeLinejoin:'round',strokeMiterlimit:'1.5'}}
                >
                <circle cx="12.5" cy="25" r="2.5" style={{fill:'none',stroke:'#2bc6c3',strokeWidth:'1px',}}/>
                <path 
                    d="M12.474,0l-0.074,21.869" 
                    style={{fill:'none',stroke:'#808080',strokeWidth:'0.5px',}}
                    />
               { !this.props.last && <path 
                    d="M12.474,28.037l-0.074,21.869" 
                    style={{fill:'none',stroke:'#808080',strokeWidth:'0.5px',}}
                    />}
            </svg>
        )
    }
}



