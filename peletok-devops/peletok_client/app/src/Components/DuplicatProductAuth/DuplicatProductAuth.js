import React, { Component } from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import './DuplicatProductAuth.css';
import InputTypeRadio from '../InputUtils/InputTypeRadio';
import InputTypeSelect from '../InputUtils/InputTypeSelect';
import InputTypeNumber from '../InputUtils/InputTypeNumber';
import Slider from '../InputUtils/Slider';
import { LanguageManager as LM } from '../LanguageManager/Language'
import {
    getSuppliers,
    getProducts,
    postDuplicatProductAout
} from '../DataProvider/DataProvider'
import InputTypeSimpleSelect from '../InputUtils/InputTypeSimpleSelect';
import { Notifications } from '../Notifications/Notifications'
import { Loader } from '../Loader/Loader'


export class DuplicatProductAuth extends Component {
    constructor(props) {
        super(props);

        this.state = {    
            renderFields: true,
            suppliers: [],
            sourceProducts: [],
            targetProducts: [],
            sourceProductsId: null,
            targetProductsId: null,
            targetSelectedServiceId: '0',
            sourceSelectedServiceId: '0',
        }
        this.fields = {}
        
    }

    

    loadSuppliersList(forceUpdate = false) {
        getSuppliers(forceUpdate).then(
            (res) => {
                this.setState({ suppliers: Object.keys(res).reduce((newArr, key) => [...newArr, ...res[key]], []) })
            }
        ).catch(
            (err) => {
                alert("loadSuppliersList failed: " + err)

            }
        )
    }

    productChange = (stateAttr) => {
        return (e) => {
            
            if (e.value) {
                this.setState({
                    [stateAttr]: e.value
                })
            }
        }

    }

    supplierChange = (stateAttr) => {
        return (e) => { 
                getProducts(e.value, {}, true).then(
                    (res) => {
                        this.setState({
                            [stateAttr]: {
                                manual: res.manual ? res.manual.map(p => p.name ? p : Object.assign(p, { name: p.name_he })) : [],
                                virtual: res.virtual ? res.virtual.map(p => p.name ? p : Object.assign(p, { name: p.name_he })) : [],
                                bills_payment: res.bills_payment ? res.bills_payment.map(p => p.name ? p : Object.assign(p, { name: p.name_he })) : [],
                            }
                        })
                        

                    }).catch(
                        (err) => {
                            Notifications.show(err);
                        }
                    )
        }
    }

    sourceServiceChange = (e) => {
        let activeId = e.target.id.split("-")[1]
        this.setState({ targetSelectedServiceId: activeId });
        this.sourceSelectProduct.setValue(null)
    }
    targetServiceChange = (e) => {
        let activeId = e.target.id.split("-")[1]
        this.setState({ sourceSelectedServiceId: activeId });
        this.targetSelectProduct.setValue(null)
    }

    businessTypeChanged = (e) => {
        let activeId = e.target.id.split("-")[1]
        const options = ['all', 'distributor', 'seller']
        this.setState({ selectedBusinessType: options[activeId] });
    }

    productsLoad = (source) => {
        
        if (Object.keys(this.state[source + 'Products']).length !== 0) {
            let virtualProduct = this.state[source + 'Products'].virtual ? this.state[source + 'Products'].virtual.map(p => {
                return { label: p.name, value: p.id, id: p.id, type: p.type }
            }) : [];
            let manualProduct = this.state[source + 'Products'].manual ? this.state[source + 'Products'].manual.map(p => {
                return { label: p.name, value: p.id, id: p.id, type: p.type }
            }) : [];
            let billsPayment = this.state[source + 'Products'].bills_payment ? this.state[source + 'Products'].bills_payment.map(p => {
                return { label: p.name, value: p.id, id: p.id, type: p.type }
            }) : [];
            if (this.state[source + 'SelectedServiceId'] === '0') {
                return virtualProduct.concat(manualProduct, billsPayment);
            }
            if (this.state[source + 'SelectedServiceId'] === '2') {
                return manualProduct;
            }
            if (this.state[source + 'SelectedServiceId'] === '1') {
                return virtualProduct;
            }
            if (this.state[source + 'SelectedServiceId'] === '3') {
                return billsPayment
            }
        }
        return false
    }

    
    buttonSaveClicked = (e) => {
        let sourceValid = this.sourceSelectProduct.runValidation()
        let targetValid = this.targetSelectProduct.runValidation()
        if(sourceValid && targetValid){
            let data = {
                sourceProductId: this.state.sourceProductsId,
                targetProductId: this.state.targetProductsId,
            }
            Loader.show()
            postDuplicatProductAout(data).then((res)=>{
                this.setState({
                    renderFields: false
                }, ()=>{
                    this.setState({
                        renderFields: true
                    }, ()=>{
                        Loader.hide()
                        Notifications.show(LM.getString('productAuthDoplicateSuccessfully'), 'success')
                    })
                })
            })
        } else {
                Loader.hide()
                Notifications.show(LM.getString('targetAndSourceMustSupplied'),'danger')
        }
    }
    

    componentDidMount = async () => {
        
        this.loadSuppliersList();
    
    }
    
    


    render() {
        const header = LM.getString('profitCopy');
        const selectSourceProductHeader = LM.getString('selectSourceProduct');
        const selectTargetProductHeader = LM.getString('selectTargetProduct');
        const buttonSave = LM.getString('save');

        return (

            <Container className='DuplicatProductAuthContainer'>
                <Row >
                    <Col className="DuplicatProductAuthHeader">
                        {header}
                    </Col>
                </Row>
                {this.state.renderFields && <Container className="DuplicatProductAuthSettings">
                     <Row >
                        <Col className="copyProfitPercentagesSubHeader">
                            {selectSourceProductHeader}
                        </Col>
                    </Row>
                    <Row >
                        <Col >
                            <hr className="businessLine" />
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <InputTypeRadio
                                default={LM.getString('productsRadioButtonsRow')[0]}
                                id="CreateProductSelectService-"
                                options={LM.getString('productsRadioButtonsRow')}
                                onChange={this.sourceServiceChange}
                            />
                        </Col>
                    </Row>
                     <Row className="align-items-end">
                        <Col sm='4'>
                            <InputTypeSimpleSelect
                                title={LM.getString("selectSupplier")}
                                id="CreateProductSelectSupplier"
                                required={this.state.creatingNewState}
                                options={this.state.suppliers.map(supplier => {
                                    return { label: supplier.name, value: supplier.id }
                                })} onChange={this.supplierChange('sourceProducts')}
                                ref={(componentObj) => {
                                    this.sourceSelectSupplier = componentObj
                                }}
                            />
                        </Col>
                        <Col sm='4'>
                                <InputTypeSimpleSelect
                                    required={true}
                                    title={LM.getString('selectProductFromList')}
                                    id="CreateProductSelectProduct"
                                    options={this.productsLoad('source') || ['']}
                                    onChange={this.productChange('sourceProductsId')}
                                    ref={(componentObj) => {
                                        this.sourceSelectProduct = componentObj
                                    }}
                                />
                        </Col>
                    </Row>
                    <Row >
                        <Col className="copyProfitPercentagesSubHeader">
                            {selectTargetProductHeader}
                        </Col>
                    </Row>
                    <Row >
                        <Col >
                            <hr className="businessLine" />
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <InputTypeRadio
                                default={LM.getString('productsRadioButtonsRow')[0]}
                                id="CreateProductSelectService-"
                                options={LM.getString('productsRadioButtonsRow')}
                                onChange={this.targetServiceChange}
                            />
                        </Col>
                    </Row>
                    <Row className="align-items-end">
                        <Col sm='4'>
                            <InputTypeSimpleSelect
                                title={LM.getString("selectSupplier")}
                                id="CreateProductSelectSupplier"
                                required={this.state.creatingNewState}
                                options={this.state.suppliers.map(supplier => {
                                    return { label: supplier.name, value: supplier.id }
                                })} onChange={this.supplierChange('targetProducts')}
                                ref={(componentObj) => {
                                    this.targetSelectSupplier = componentObj
                                }}
                            />
                        </Col>
                        <Col sm='4'>
                                <InputTypeSimpleSelect
                                    required={true}
                                    title={LM.getString('selectProductFromList')}
                                    id="CreateProductSelectProduct"
                                    options={this.productsLoad('target') || ['']}
                                    onChange={this.productChange('targetProductsId')}
                                    ref={(componentObj) => {
                                        this.targetSelectProduct = componentObj
                                    }}
                                />
                        </Col>
                    </Row>
                    <Row>
                    <Col sm="auto" >
                        <Button
                            className="copyProfitPercentagesButton"
                            onClick={this.buttonSaveClicked}
                            id="CopyProfitPercentagesSaveButton"
                        >
                            {buttonSave}
                        </Button>
                    </Col>
                </Row>
                </Container>}
            </Container>
        )
    }
}

