import React from 'react';
import {Card, CardFooter, Row, Col, Button} from 'reactstrap'
import ReactSVG from 'react-svg';
import {LanguageManager as LM} from "../LanguageManager/Language"
import headPhones from '../../Assets/Icons/SupportIcons/headPhoneShape49x40.svg';
import shareScreen from '../../Assets/Icons/SupportIcons/ScreenIcon49x40.svg';
import headPhonesSupport from '../../Assets/Icons/SupportIcons/headPhoneShape81x66.svg';
import shareScreenSupport from '../../Assets/Icons/SupportIcons/ScreenIcon81x66.svg';
import './SupportLayout.css'


export class SupportLayout extends React.Component {

    render() {
        return (
            <div>
                <Card className={'px-0 px-md-3 text-center mb-md-3 ' + (this.props.supportLogin ? 'no-border' : '')}>
                    <Row>
                        <Col className={'d-md-block d-none mx-3 my-3 m-md-0 px-1'}>
                            <ReactSVG className={'pt-1 py-md-4'} src={this.props.supportLogin ? shareScreenSupport : shareScreen }/>
                            <div className={'mx-3 mx-md-0'}>
                            <p className={'font-weight-light fs-15 mb-0 mb-md-3 mt-1px'}>{LM.getString('shareScreen')}</p>
                            <Button color="primary" id="clickHere" size="sm" className={'bc-blue-1 border-radius-3 border-unset w-75'} href='https://www.peletop.co.il/support_peletop.exe'>{LM.getString('clickHere')}</Button>
                            </div>
                        </Col>
                        <div className={'border-top d-md-none mx-3 w-100'}/>
                        <Col className={'d-flex flex-md-column mx-3 my-3 m-md-0 px-1'}>
                            <ReactSVG className={'pt-1 py-md-4'} src={this.props.supportLogin ? headPhonesSupport : headPhones}/>
                            <div className={'mx-3 mx-md-0'}>
                                <p className={'font-weight-light fs-15 mb-0 mb-md-3'}>{LM.getString('callUs')}</p>
                                <p className={'fc-blue font-weight-bold fs-22 mb-0'}>{LM.getString('phoneNum')}</p>
                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <Col className="border-top fs-15 mt-md-1 mx-3 px-0 py-3 font-weight-light">
                        {LM.getString('officeHoursST')}<br/>
                        {LM.getString('officeHoursF')}
                        </Col>
                    </Row>
                </Card>

            </div>
        )
    }
}