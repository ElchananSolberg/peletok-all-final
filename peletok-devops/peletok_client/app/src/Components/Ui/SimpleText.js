import React from 'react'

export default function SimpleText({label, text, withSeperator, withBr, end}){
	return text ? <span><span>{(label ? label + ' ' : '') + text}{text && withSeperator && !end && <span style={{margin: '0 10px'}}>|</span>}</span>{text && !end && withBr && <br/>}</span> : <></>
}