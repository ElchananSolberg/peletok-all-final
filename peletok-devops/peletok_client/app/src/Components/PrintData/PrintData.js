import React, { Component } from 'react';
import { Button } from 'reactstrap'
import ReactToPrint from 'react-to-print';
import { ReceiptTemplate } from "../ReceiptTemplate/ReceiptTemplate";
import { getReceiptDetails } from "../DataProvider/Functions/DataGeter";
import { Notifications } from "../Notifications/Notifications";



/**
 * Print component  Should receive a table
 *  and titleOfButton in props
 * And prints the information she received in a table
 */

export class PrintData extends Component {
    constructor(props) {
        super(props);
        this.state = {
            receiptDetails: {}
        }
    }

    async loadReceiptDetails() {
        let res = await getReceiptDetails(this.props.transId).catch(
            (err) => {
                Notifications.show("loadReceiptDetails failed: " + err)
            }
        );
        this.setState({ receiptDetails: res })
    }

    render() {

        return (
            <React.Fragment>
                <ReactToPrint
                    trigger={() => <Button type="button" color={this.props.color}>{this.props.titleOfButton}</Button>}
                    content={() => this.componentRef}
                    onBeforeGetContent={async () => {
                        await this.loadReceiptDetails().catch(
                            (err) => {
                                Notifications.show("loadReceiptDetails failed: " + err)
                            }
                        );
                    }}
                />
                <div style={{ display: "none" }}><ReceiptTemplate {...this.state.receiptDetails} cardNumber={this.props.cardNumber}  codeNumber={this.props.codeNumber} expiryDate={this.props.expiryDate} ref={el => (this.componentRef = el)} /></div>
            </React.Fragment>
        )
    }

}