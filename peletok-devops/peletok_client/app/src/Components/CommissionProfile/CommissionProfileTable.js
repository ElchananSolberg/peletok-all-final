import React from 'react';
import { Button, Table } from 'reactstrap';
import TableRow from './TableRow.js';
import TableHead from './TableHead.js';
import { Scrollbars } from 'react-custom-scrollbars';
import { LanguageManager as LM } from "../LanguageManager/Language";

/**
 * CommissionProfileTable - component that builds the table from the data (this.props.tableRows and this.props.tableHeadersObj) sended by the father component.
 * It use TableHead and TableRow components.
 */
export default class CommissionProfileTable extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            markAll: false,
            unMarkAll: false,
        }
        this.tableRows = {};
    }
    markAllClicked = (e) => {
        this.setState({ markAll: true });
        this.setState({ unMarkAll: false });
    }
    unMarkAllClicked = (e) => {
        this.setState({ markAll: false });
        this.setState({ unMarkAll: true });
    }

    render() {
        let tableRows = this.props.tableRows;
        var trId = 'TableRowID';
        var trArray = tableRows.map((current, index = 0) => {
            return <tr key={index} id={'tr' + index}>
                <TableRow
                    id={trId + index}
                    tableRowObj={current}
                    tableRowsEditable={this.props.tableRowsEditable}
                    checked={this.state.markAll}
                    unMarkAll={this.state.unMarkAll}
                    ref={(componentObj) => { this.tableRows['tableRow' + index] = componentObj }}
                />
            </tr>
        });

        return (
            <React.Fragment>
                <div className={'d-flex w-100 mb-3'}>
                    <div className={(LM.getDirection() === "rtl" ? "mr-auto" : "ml-auto")}>
                        <Button
                            style={{ marginLeft: '1px' }}
                            className="commissionProfileButton CommissionProfileSmallButton"
                            onClick={this.markAllClicked}
                        >
                            {LM.getString('markAll')}
                        </Button>
                        <Button
                            style={{ marginLeft: '1px' }}
                            className="CommissionProfileDarkButton CommissionProfileSmallButton"
                            onClick={this.unMarkAllClicked}
                        >
                            {LM.getString('unMarkAll')}
                        </Button>
                    </div>
                </div>
                <div className={'table-responsive table-css ' + (LM.getDirection() === "rtl" ? "table-rtl" : "")}>
                    <Scrollbars style={{ width: '100%', height: '490px' }}>
                        <Table bordered striped size="sm" >
                            <thead>
                                <tr>
                                    <TableHead tableHeadersObj={this.props.tableHeadersObj} />
                                </tr>
                            </thead>
                            <tbody >
                                {trArray}
                            </tbody>
                            <tfoot >
                                <tr >
                                    <th colSpan="6">
                                        <div style={{ minHeight: '23.5px' }} />
                                    </th>
                                </tr>
                            </tfoot>
                        </Table>
                    </Scrollbars>
                </div>
            </React.Fragment >
        );
    }
}