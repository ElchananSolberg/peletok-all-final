import React, { Fragment } from 'react';
import { Input, Label } from 'reactstrap';
import './CommissionProfile.css';
import { LanguageManager as LM } from "../LanguageManager/Language";

import InputTypeNumber from '../InputUtils/InputTypeNumber';
import ProductTypeEnum from '../../enums/ProductTypeEnum'
import { Notifications } from "../Notifications/Notifications";
import { AccountingEnum } from "../../enums/AccountingEnum";

/**
 * TableRow - component that builds and returns an array of <td> elements from the data sended by CommissionProfileTable component - this.props.tableRowObj .
 */
export default class TableRow extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            checked: this.props.tableRowObj.checkbox,
            useDistributionFee: this.props.tableRowObj.checkbox2,
        }
    }


    rowCheckBoxChange = (e) => {
        e.persist();
        var checked = e.target.checked;
        this.setState({ checked: checked });
    };

    useDistributionFeeChange = (e) => {
        e.persist();
        var checked = e.target.checked;
        this.setState({ useDistributionFee: checked });
    };

    componentDidUpdate(prevProps) {
        if (prevProps !== this.props) {
            let checked = this.props.checked;
            if (!this.props.unMarkAll && this.props.tableRowObj.checkbox) checked = true;
            this.setState({ checked: checked });

            Object.keys(this.props.tableRowObj).forEach((key) => {
                if (this[key]) {
                    if (this.props.tableRowObj[key] !== null && this.props.tableRowObj[key] !== undefined) this[key].setValue(this.props.tableRowObj[key]);
                    else {
                        if (key !== 'checkbox' && key !== 'checkbox2') this[key].setValue('');
                        else this[key].setValue(false);
                    }
                }
            })
        }
    }

    getNunberHandler(key, tableRowObj, objWithFields) {
        let validationObjec = tableRowObj.validationObjec;
        return (e) => {
            let fieldRef = objWithFields[key]
            let max = validationObjec.max_percentage_profit;
            let max_business_commission = validationObjec.business_commission;
            let min = validationObjec.min_percentage_profit;
            switch (key) {
                case 'profitPercentage':
                    if (e.target.value > max) {
                        Notifications.show(
                            `${LM.getString('error')}: ${LM.getString('max')} ${max}`,
                            "danger"
                        );
                        fieldRef.setValue(tableRowObj[key] || max)
                    }
                    break;
                case 'resellerCommission':
                    if (e.target.value > max_business_commission) {
                        Notifications.show(
                            `${LM.getString('error')}: ${LM.getString('max')} ${max_business_commission}`,
                            "danger"
                        );
                        fieldRef.setValue(tableRowObj[key] || min)
                    }
                    break;
                case 'commissionForFinalClient':
                    if (e.target.value < min) {
                        Notifications.show(
                            `${LM.getString('error')}: ${LM.getString('min')} ${min}`,
                            "danger"
                        );
                        fieldRef.setValue(tableRowObj[key] || min)
                    }
                    break;
                default:
                    return;
            }
        }

    }

    getTitle = (key, tableRowObj) => {
        let validationObjec = tableRowObj.validationObjec;
        let max = validationObjec.max_percentage_profit;
        let max_business_commission = validationObjec.business_commission;
        let min = validationObjec.min_percentage_profit;
        switch (key) {

            case 'profitPercentage':
                return `${LM.getString('max')} ${max}`
                break;
            case 'resellerCommission':
                return `${LM.getString('max')} ${max_business_commission}`
                break;
            case 'commissionForFinalClient':
                return `${LM.getString('min')} ${min}`
                break;
            default:
                return;
        }
    }

    componentDidMount() {
        Object.keys(this.props.tableRowObj).forEach((key) => {
            if (this[key]) {
                if (this.props.tableRowObj[key] !== null && this.props.tableRowObj[key] !== undefined) this[key].setValue(this.props.tableRowObj[key]);
                else {
                    if (key !== 'checkbox' && key !== 'checkbox2') this[key].setValue('');
                    else this[key].setValue(false);
                }
            }
        })
    }

    render() {
        let tableRowObj = this.props.tableRowObj;
        let tdArray = [];

        tdArray = Object.keys(tableRowObj).filter(k => k != 'validationObjec').map((key, index = 0) => {
            if (key === 'checkbox' || key === 'checkbox2') {
                return <td key={index} id={this.props.id + '_' + key}>
                    <div
                        className={"custom-control custom-checkbox " + (LM.getDirection() === "rtl" ? "checkbox-rtl" : "")}>
                        <Input
                            className="custom-control-input"
                            type="checkbox"
                            checked={key === 'checkbox' ? this.state.checked : this.state.useDistributionFee}
                            onChange={key === 'checkbox' ? this.rowCheckBoxChange : this.useDistributionFeeChange}
                            id={this.props.id + "CheckBox" + '_' + key}
                            value={this.props.id}
                            ref={(componentObj) => {
                                if (key === 'checkbox') {
                                    this.tableRowCheckBox = componentObj
                                } else {
                                    this.distributionFee = componentObj
                                }
                            }}
                        />
                        <Label className="custom-control-label" for={this.props.id + "CheckBox" + '_' + key} />
                    </div>
                </td>
            } else if (this.props.tableRowsEditable && this.props.tableRowsEditable[index]) {

                return <td
                    title={this.getTitle(key, tableRowObj)}
                    key={index}
                    id={this.props.id + '_' + key}
                >
                    <InputTypeNumber
                        outerStyle={{
                            backgroundColor: 'transparent',
                            maxHeight: '25px',
                            boxShadow: '0 0 0',
                            borderWidth: '0',
                            paddingTop: '0px',
                            paddingBottom: '0px'
                        }}
                        id={this.props.id + "EditableNumber" + '_' + key}
                        disableErrorMessage={true}
                        onChange={this.getNunberHandler(key, tableRowObj, this)}
                        ref={(componentObj) => {
                            this[key] = componentObj
                        }}
                    />
                </td>
            } else if (key == 'profitPerParagraph20') {
                return (<td key={index} id={this.props.id + '_' + key} className={'111111111'}>
                    {(parseFloat(tableRowObj['profitPercentage']) / (parseFloat(1) + parseFloat(AccountingEnum.VAT))).toFixed(2)}
                </td>)
            } else if (key == 'resellerCommissionP20') {
                return (<td key={index} id={this.props.id + '_' + key} className={'111111111'}>
                    {(parseFloat(tableRowObj['resellerCommission']) / (parseFloat(1) + parseFloat(AccountingEnum.VAT))).toFixed(2)}
                </td>)
            }
            else return (
                <td key={index} id={this.props.id + '_' + key} className={'111111111'}>
                    {key === 'cardType' ? ProductTypeEnum[tableRowObj[key]] : tableRowObj[key]}
                </td>
            )
        });

        return (
            <Fragment>
                {tdArray}
            </Fragment>
        );
    }
}