import React, { Component } from 'react';
import './SuppliersProfile.css';

import { Container, Row, Col, Button, Collapse } from 'reactstrap';

import { LanguageManager as LM } from '../LanguageManager/Language';
import { Notifications } from '../Notifications/Notifications';

import CommissionProfileTable from '../CommissionProfile/CommissionProfileTable';

import { getSuppliers,
    getSuppliersProfiles,
    getSuppliersProfile,
    postSuppliersProfile,
    putSuppliersProfile,
    getRole,
     } from '../DataProvider/DataProvider'

import InputTypeSearchList from '../InputUtils/InputTypeSearchList';
import InputTypeText from '../InputUtils/InputTypeText';

/** SuppliersProfile is a main component of SuppliersProfile Page.  
 * SuppliersProfile and its children are working with .css file: SuppliersProfile.css. */
export default class SuppliersProfile extends Component {
    constructor(props) {
        super(props);

        this.state = {
            roles: [],

            suppliers: [],
            /** working variable that will contain an array (of objects) of all the suppliersProfiles */
            suppliersProfiles: [],
            /** working variable that will contain an object of a SELECTED suppliersProfile */
            suppliersProfile: {},

            /** working variable that indicates if a user is on the stage of viewing a Table */
            viewingTable: false,
            /** working variable that indicates if a user is on the stage of creating a new PROFILE */
            isCreatingNew: false,

            /** tableRows - an array of objects for <CommissionProfileTable/> component. The values of the objects are the data of the rows of a table. 
            */
            tableRows: [],
        }
    }

    /** "Select Suppliers Profile" onChange handler.
     * It should UPDATE or Create a suppliers profile
     * TODO: Add functionality */
    suppliersProfileChanged = (e) => {
        this.setState({ viewingTable: true });

        let name = this.selectSuppliersProfile.getValue().value
        let selectedSuppliersProfile = this.state.suppliersProfiles.find(suppliersProfile => (suppliersProfile.name === name));

        getSuppliersProfile(selectedSuppliersProfile.id).then(
            res => {
                this.setState({ suppliersProfile: res[0] });
                this.loadProfileDataIntoTable(res);
            }
        ).catch(
            err => {
                Notifications.show(err, 'danger');
            }
        );
    }
    /** Function that enters a data from a response on getSuppliersProfile() request into the table. */
    loadProfileDataIntoTable = (suppliersProfile) => {
        let tableRows = suppliersProfile[0].Suppliers.map(supplier => { return { 'supplierName': supplier.name, 'checkbox': supplier.BusinessSupplier.is_authorized, } });
        this.setState({ tableRows: tableRows },()=>{
            this.rolesSelect.setValue(this.state.suppliersProfile.Roles ? this.state.suppliersProfile.Roles.map(r=>r.name) : [])
        })
    }

    /** "Save" Button click handler (CREATING or UPDATING of profile) */
    saveButtonClicked = () => {
        let name;
        /** if a user is on a stage of CREATING a new profile */
        if (this.state.isCreatingNew) {
            if (this.profileName.getValue().valid) {
                name = this.profileName.getValue().value;
                let checkOnDoubleNameResult = this.state.suppliersProfiles.find(element => {
                    if(element.name === name) return true
                    else return false
                });
                if (!checkOnDoubleNameResult) {
                    let suppliersList = {};
                    this.state.suppliers.forEach((supplier, index = 0) => {
                        let tableRowCheckBoxChecked = this.commissionProfileTable.tableRows['tableRow' + index].tableRowCheckBox.props.checked;
                        let supplierId = supplier.id.toString();
                        suppliersList[supplierId] = tableRowCheckBoxChecked;
                    });
                    let roles = this.rolesSelect.getValue().value
                    let profileData = {
                        "name": name,
                        "suppliers": suppliersList,
                        roles
                    }
                    postSuppliersProfile(profileData).then(
                        res => {
                            Notifications.show(LM.getString("suppliersProfile") + ' ' + LM.getString('createdSuccessfully'), 'success');
                            this.setState({ tableRows: [] });                            
                            this.cancelButtonClicked();
                            this.launchGetSuppliersProfiles();
                        }
                    ).catch(
                        err => {
                            Notifications.show(err, 'danger');
                        }
                    );
                }
                else Notifications.show(LM.getString("profileName") + " " + LM.getString("isAlreadyExist") + " ", 'warning');
            }
            else Notifications.show(LM.getString("field") + " '" + LM.getString("profileName") + "' " + LM.getString("isNotValid") + " ", 'warning');
        }
        /** else if a user is on a stage of UPDATING a profile */
        else {
            name = this.state.suppliersProfile.name;

            let suppliersList = {};
            this.state.suppliersProfile.Suppliers.forEach((supplier, index = 0) => {
                let tableRowCheckBoxChecked = this.commissionProfileTable.tableRows['tableRow' + index].tableRowCheckBox.props.checked;
                let supplierId = supplier.supplier_id.toString();
                suppliersList[supplierId] = tableRowCheckBoxChecked;
            });
            let roles = this.rolesSelect.getValue().value
            let profileData = {
                "name": name,
                "suppliers": suppliersList,
                roles
            }
            putSuppliersProfile(this.state.suppliersProfile.id, profileData).then(
                res => {
                    Notifications.show(LM.getString("suppliersProfile") + ' ' + LM.getString('updatedSuccessfully'), 'success');
                }
            ).catch(
                err => {
                    Notifications.show(err, 'danger');
                }
            );
        }
    }
    /** "Cancel" Button click handler */
    cancelButtonClicked = () => {
        this.setState({ isCreatingNew: false });
        if (!this.selectSuppliersProfile.getValue().value) this.setState({ viewingTable: false });
        this.profileName.setValue("")
        window.scrollTo({
            top: 0,
            behavior: "smooth"
        });
    }
    /** "Add New Profile" Button click handler */
    addNewProfileButtonClicked = () => {
        this.setState({ isCreatingNew: true, viewingTable: true });
        window.scrollTo({
            top: 0,
            behavior: "smooth"
        });
        this.selectSuppliersProfile.setValue("");
        this.rolesSelect.setValue([])
        
        getSuppliers(true)
            .then(
                res => {
                    let suppliers = Object.keys(res).reduce((newArr, key) => [...newArr, ...res[key]], []);
                    let tableRows = suppliers.map(supplier => { return { 'supplierName': supplier.name, 'checkbox': false, } });
                    this.setState({ suppliers: suppliers, tableRows: tableRows })
                }
            )
            .catch((err) => Notifications.show(err, 'danger'));
    }

    launchGetSuppliersProfiles = () => {
        getSuppliersProfiles(true)
            .then(
                res => {
                    this.setState({ suppliersProfiles: res });
                }
            )
            .catch((err) => Notifications.show(err, 'danger'));
    }

    componentDidMount() {
        getRole().then(res=>{
            this.setState({
                roles: res
            })
        })
        this.launchGetSuppliersProfiles();
    }

    render() {
        /** Screen Header  */
        const header = LM.getString('suppliersProfile');

        /** data for "Select Suppliers Profile" field (InputTypeSearchList InputUtils component) */
        const selectSuppliersProfileProps = {
            title: LM.getString("selectSuppliersProfile") + ':',
            id: "SuppliersProfile_SelectSuppliersProfile",
        };
        /** data for "Profile Name" field (InputTypeText InputUtils component) */
        const profileNameProps = {
            required: true,
            title: LM.getString("profileName") + ':',
            id: "SuppliersProfile_ProfileName",
        };

        /** tableHeadersObj - an object for <CommissionProfileTable/> component. The values of the object are the HEADERS of a table */
        const tableHeadersObj =
        {
            supplierName: LM.getString('supplierName'),
            checkbox: '',
        };

        /** data for "Save" button */
        const saveButtonText = LM.getString("save");
        /** data for "Cancel" button */
        const cancelButtonText = LM.getString('cancel');
        /** data for "Add a New Profile" button */
        const addNewProfileButtonText = LM.getString('addProfile');

        return (
            <Container className="suppliersProfileContainer">
                <Row >
                    <Col className="suppliersProfileHeader">
                        {header + (this.state.isCreatingNew ? ` - ${LM.getString('new')}` : "")}
                    </Col>
                </Row>
                <Container className="suppliersProfileSettings">
                    <Row>
                        <Col sm='4'>
                            <Collapse isOpen={!this.state.isCreatingNew}>
                                <InputTypeSearchList
                                    {...selectSuppliersProfileProps}
                                    onChange={this.suppliersProfileChanged}
                                    options={this.state.suppliersProfiles.map(suppliersProfile => suppliersProfile.name) || ['']}
                                    ref={(componentObj) => { this.selectSuppliersProfile = componentObj }}
                                />
                            </Collapse>
                            <Collapse isOpen={this.state.isCreatingNew}>
                                <InputTypeText
                                    {...profileNameProps}
                                    ref={(componentObj) => { this.profileName = componentObj }}
                                />
                            </Collapse>
                        </Col>
                        <Col sm="auto">
                            <Button
                                className="suppliersProfileButton"
                                id="suppliersProfileAddNewProfileButton"
                                onClick={this.addNewProfileButtonClicked}
                                hidden={this.state.isCreatingNew}
                            >
                                {addNewProfileButtonText}
                            </Button>
                        </Col>
                    </Row>
                    <Collapse isOpen={this.state.viewingTable} >
                        <Row style={{ marginTop: '30px' }}>
                            <Col >
                                <InputTypeSearchList
                                    multi={true}
                                    title={LM.getString('allowedToRole')}
                                    id="allowedToRole"
                                    options={this.state.roles.map(r=>r.name)}
                                    ref={(componentObj) => { this.rolesSelect = componentObj }} />
                            </Col>
                        </Row>
                        <Row style={{ marginTop: '30px' }}>
                            <Col sm='4'>
                                <CommissionProfileTable
                                    tableRows={this.state.tableRows}
                                    tableHeadersObj={tableHeadersObj}
                                    tableFootersObj={{}}
                                    ref={(componentObj) => { this.commissionProfileTable = componentObj }}
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col sm="auto">
                                <Button
                                    className="suppliersProfileButton suppliersProfileButtonSave"
                                    id="suppliersProfileSaveButton"
                                    onClick={this.saveButtonClicked}
                                >
                                    {saveButtonText}
                                </Button>
                            </Col>
                            <Col sm="auto">
                                <Button
                                    className="suppliersProfileButton suppliersProfileDarkButton"
                                    id="suppliersProfileCancelButton"
                                    onClick={this.cancelButtonClicked}
                                    hidden={!this.state.isCreatingNew}
                                >
                                    {cancelButtonText}
                                </Button>
                            </Col>
                        </Row>
                    </Collapse>
                    <Row >
                        <Col >
                            <hr></hr>
                        </Col>
                    </Row>
                    
                </Container>
            </Container>
        )
    }
}