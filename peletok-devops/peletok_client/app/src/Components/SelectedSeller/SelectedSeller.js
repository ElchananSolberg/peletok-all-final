import React from 'react';
import {BehaviorSubject, Subject} from 'rxjs'


/** <SelectedSeller/> is Singleton  */
export class SelectedSeller extends React.Component {

    static singleton = null;
    static currentSeller = new BehaviorSubject({})
    static currentDistributor = new BehaviorSubject({})
    static resetSeller = new Subject
    static deleteSeller = new Subject
    static resetDistributor = new Subject
    static createSellerClicked = new Subject
    static createDistributorClicked = new Subject
    static clearFields = new Subject
    static setHeader = new Subject
    static refreshSellers = new Subject
    static refreshDistributors = new Subject
    static newSellerState = false
    static newDistributorState = false
    static sellerId;
    static distributorId;

    constructor(props) {
        super(props);
        /** here we make Loader to be singleton (to have only 1 instance) */
        if (SelectedSeller.singleton) {
            throw 'Attention, developer! You must not use SelectedSeller as a component.';
        }
        SelectedSeller.singleton = this;

        SelectedSeller.sellerId = window.localStorage.getItem('seller')
        SelectedSeller.distributorId = window.localStorage.getItem('distributor')
        
        
        
        SelectedSeller.resetSeller.subscribe(()=>{
            window.localStorage.removeItem('seller')
            SelectedSeller.currentSeller.next({})
        })

        SelectedSeller.createSellerClicked.subscribe(()=>{
            SelectedSeller.newSellerState = !SelectedSeller.newSellerState
        })

        SelectedSeller.createDistributorClicked.subscribe(()=>{
            SelectedSeller.newDistributorState = !SelectedSeller.newDistributorState
        })

        SelectedSeller.deleteSeller.subscribe(()=>{
            window.localStorage.removeItem('seller')
            SelectedSeller.currentSeller.next({})
        })

        SelectedSeller.resetDistributor.subscribe(()=>{
            // window.localStorage.removeItem('distributor')
            // window.localStorage.removeItem('seller')
            SelectedSeller.currentDistributor.next({})
            SelectedSeller.currentSeller.next({})

        })


        
    }

    static setSeller(seller){
        window.localStorage.setItem('seller', (seller || {}).id)
        SelectedSeller.sellerId = (seller || {}).id
        if ( seller.tags) {
            seller = Object.assign(seller, {tags: seller.tags.constructor.name ==  'Array' ? seller.tags : [seller.tags].filter(t=>t) })
        }else{
            seller.tags = []
        }
        SelectedSeller.currentSeller.next(seller)
    }


    static setDistributor(distributor){
        window.localStorage.setItem('distributor', (distributor || {}).id)
        SelectedSeller.distributorId = (distributor || {}).id
        SelectedSeller.currentDistributor.next(distributor)
    }

    

    

    
    

    render = () => {
        return (
            <>
               
            </>
        );
    }
}