import React from 'react';
import { LanguageManager as LM } from '../LanguageManager/Language';
import InputTypeCheckBox from '../InputUtils/InputTypeCheckBox';
import { putCatalogNumberStatus, getBarcodeTypes } from '../DataProvider/DataProvider';
import { Button } from 'reactstrap';
import './CatalogNumberManagement.css';

const ACTIVE_STATUS = 2;
const INACTIVE_STATUS = 1;

export class TableRow extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            active: this.props.active,
            activeMessage: this.props.activeMessage
        }
    }

    checkOnChange = () => {
        this.setState({ activeMessage: this.active.getValue().value, active: this.active.getValue().value })
        let data = { status: this.active.getValue().value ? ACTIVE_STATUS : INACTIVE_STATUS }
        putCatalogNumberStatus(this.active.props.numberId, data)
    }

    requiredOnChange = () => {
        let data = { required: this.requiredCheckBox.getValue().value }
        putCatalogNumberStatus(this.active.props.numberId, data)
    }

    render() {

        return (
            <React.Fragment>
                <tr>
                    <td>
                        {this.props.businessName}
                    </td>
                    <td  className='checkBoxBorderDisable' style={{ display: 'flex' }}>
                        <InputTypeCheckBox
                            id={this.props.activeCheckBoxId}
                            numberId={this.props.numberId}
                            onChange={this.checkOnChange}
                            checked={this.state.active}
                            ref={(objRef) => { this.active = objRef }}
                        />
                        <div>{this.state.activeMessage ? LM.getString("banersStatusList")[0] : LM.getString("banersStatusList")[1]}</div>
                    </td>
                    <td>
                        <InputTypeCheckBox
                            id={this.props.requiredCheckBoxId}
                            numberId={this.props.numberId}
                            onChange={this.requiredOnChange}
                            checked={this.props.required}
                            ref={(objRef) => { this.requiredCheckBox = objRef }}
                        />
                    </td>
                    <td>
                        <Button onClick={() => this.props.editCatalogNumberFunc(this.props.idFuncArg, this.props.indexProp)} className='regularButton m-1 mx-2'>{LM.getString("edit")}</Button>

                    </td>
                </tr>
            </React.Fragment>
        )
    }
}
