import React, { Component } from 'react';
import { Table, Container, Row, Col, Button } from 'reactstrap';
import './CatalogNumberManagement.css';
import { LanguageManager as LM } from '../LanguageManager/Language';
import { TableRow } from './TableRow';
import { CreateNewBarCodeTypeModalWindow } from './createNewBarCodeTypeModalWindow';
import { getBarcodeTypes, postNewBarcode, putCatalogNumberStatus, putCatalogNumberEdit } from './../DataProvider/DataProvider';
import { ConfirmationModal } from '../ChargeComponent/Prepaid/ConfirmationModal';
import { EditBarCodeTypeModalWindow } from './EditBarCodeTypeModalWindow';
import { Confirm } from '../Confirm/Confirm';


const ACTIVE_STATUS = 2;
const INACTIVE_STATUS = 1;


export class CatalogNumberManagement extends Component {
    constructor(props) {
        super(props);

        this.state = {
            tablerows: [],
            activeMessage: undefined,
            modal: false,
            editModal: false,
            confirmationModal: false,
            editedElement: [],
            index: undefined
        }
        this.rowRef = {};
    }

    loadBarcodeTypes = (forceUpdate = false) => {
        getBarcodeTypes({}, forceUpdate).then(
            (res) => {
                this.setState({ tablerows: res })
            }).catch(
                (err) => alert(err))
    }

    componentDidMount() {
        this.loadBarcodeTypes()
    }

    toggle = () => {
        this.setState({
            modal: !this.state.modal
        })
    }

    editToggle = () => {
        this.setState({
            editModal: !this.state.editModal
        })
    }

    confirmationToggle = () => {
        this.setState({
            confirmationModal: !this.state.confirmationModal
        })
    }


    markAll = () => {
        let data = { status: ACTIVE_STATUS }
        for (let i = 0; i < this.state.tablerows.length; i++) {
            this.rowRef[i].active.setValue(true);
            this.rowRef[i].state.activeMessage = true;
            putCatalogNumberStatus(this.rowRef[i].props.numberId, data)
        }
        this.setState({ activeMessage: true })
    }

    unMarkAll = () => {
        let data = { status: INACTIVE_STATUS }
        for (let i = 0; i < this.state.tablerows.length; i++) {
            this.rowRef[i].active.setValue(false);
            this.rowRef[i].state.activeMessage = false;
            putCatalogNumberStatus(this.rowRef[i].props.numberId, data)
        }
        this.setState({ activeMessage: false })
    }

    saveHandler = () => {
        let validation = this.createBarcodeModal.newBarcodeNameRef.getValue().valid &&
            this.createBarcodeModal.activeRadioBtnRef.getValue().valid &&
            this.createBarcodeModal.requiredRadioBtnRef.getValue().valid
        if (validation) {
            this.confirmationToggle()
        }
    }

    postNewBarcodeHandler = () => {
        let data = {
            name: this.createBarcodeModal.newBarcodeNameRef.getValue().value,
            status: this.createBarcodeModal.activeRadioBtnRef.state.value == LM.getString("active") ? ACTIVE_STATUS : INACTIVE_STATUS,
            required: this.createBarcodeModal.requiredRadioBtnRef.state.value == LM.getString("mandatory")[0] ? true : false
        }
        postNewBarcode(data).then(
            () => {
                this.loadBarcodeTypes(true)
                this.toggle()
            }).catch(
                (err) => alert(err)
            )
        this.confirmationToggle()
    }

    requiredMarkAll = () => {
        let data = { required: true };
        for (let i = 0; i < this.state.tablerows.length; i++) {
            this.rowRef[i].requiredCheckBox.setValue(true);
            putCatalogNumberStatus(this.rowRef[i].props.numberId, data)
        }
    }

    requiredUnMarkAll = () => {
        let data = { required: false };
        for (let i = 0; i < this.state.tablerows.length; i++) {
            this.rowRef[i].requiredCheckBox.setValue(false);
            putCatalogNumberStatus(this.rowRef[i].props.numberId, data)

        }
    }

    editCatalogNumberFunc = (idFuncArg, indexProp) => {


        this.setState({ index: indexProp })
        let catalogArr = [];
        catalogArr.push(this.state.tablerows)
        let catalogElement = catalogArr[0].find(element => element.id == idFuncArg)
        this.editToggle()
        this.setState({ editedElement: catalogElement })


    }

    editHandler = () => {


        if (this.editBarcodeModal.editActiveRadioBtnRef.getValue().valid &&
            this.editBarcodeModal.editRequiredRadioBtnRef.getValue().valid) {

            let editedCatalogNumber = {
                name: this.editBarcodeModal.editBarcodeNameRef.getValue().value || this.state.editedElement.name,
                status: this.editBarcodeModal.editActiveRadioBtnRef.getValue().value == LM.getString("active") ? ACTIVE_STATUS : INACTIVE_STATUS,
                required: this.editBarcodeModal.editRequiredRadioBtnRef.getValue().value == LM.getString("mandatory")[0] ? true : false,
                id: this.state.editedElement.id
            }

            let data = {
                name: editedCatalogNumber.name,
                status: editedCatalogNumber.status,
                required: editedCatalogNumber.required,
            }
            putCatalogNumberEdit(this.state.editedElement.id, data).then(
                () => {
                    this.rowRef[this.state.index].active.setValue(editedCatalogNumber.status == 2 ? true : false)
                    this.rowRef[this.state.index].requiredCheckBox.setValue(editedCatalogNumber.required)

                }).catch(
                    (err) => alert(err))
            this.editToggle()
        }
    }

    render() {

        let rowsArray = this.state.tablerows.map((element, index) => {

            return (
                <TableRow
                    numberId={element.id}
                    businessName={element.name}
                    activeCheckBoxId={element.name}
                    requiredCheckBoxId={`${element.name}index`}
                    active={element.status === ACTIVE_STATUS}
                    required={element.required}
                    activeMessage={element.status === ACTIVE_STATUS}
                    ref={(objRef) => { this.rowRef[index] = objRef }}
                    editCatalogNumberFunc={this.editCatalogNumberFunc}
                    idFuncArg={element.id}
                    indexProp={index}
                />
            )
        })

        return (
            <Container className='catalogManagementContainer'>
                <Row>
                    <Col className='catalogManagementHeader'>
                        {LM.getString("catalogNumberManagement")}
                    </Col>
                </Row>
                <Container className="catalogNumberManagementSettings">
                    <Row >
                        <Col>
                            <div className='statusButtonsMargin'>
                                <Button onClick={this.markAll} className='regularButton m-3' size='sm'>{LM.getString("permissionMarkAll")}</Button>
                                <Button size='sm' onClick={this.unMarkAll}>{LM.getString("permissionClear")}</Button>
                            </div>
                        </Col>
                        <Col className='mandatoryButtonsMargin' >
                            <div>
                                <Button onClick={this.requiredMarkAll} className='regularButton m-3' size='sm'>{LM.getString("permissionMarkAll")}</Button>
                                <Button onClick={this.requiredUnMarkAll} size='sm' >{LM.getString("permissionClear")}</Button>
                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm='10'  >
                            <Table bordered striped size="sm" >
                                <thead className='tableHeaderbackgroundColor'>
                                    <tr>
                                        <th className='tableHeaderWidth' >{LM.getString("catalogNumberName")}</th>
                                        <th className='tableHeaderWidth'>{LM.getString("status")}</th>
                                        <th className='tableHeaderWidth'>{LM.getString("mandatory")[0]}</th>
                                        <th className='tableHeaderWidth'></th>
                                    </tr>
                                </thead>
                                <tbody >
                                    {rowsArray}
                                </tbody>
                            </Table >
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <Button onClick={this.toggle} className='catalogNumberManagementButton'>{LM.getString("new")}</Button>
                        </Col>
                    </Row>
                    <CreateNewBarCodeTypeModalWindow
                        isOpen={this.state.modal}
                        className={this.props.className}
                        saveHandler={this.saveHandler}
                        cancelClick={this.toggle}
                        ref={(objRef) => { this.createBarcodeModal = objRef }} />

                    <EditBarCodeTypeModalWindow
                        isOpen={this.state.editModal}
                        className={this.props.className}
                        editHandler={() => { Confirm.confirm(LM.getString("saveChanges")).then(() => this.editHandler()) }}
                        cancelClick={this.editToggle}
                        defaultName={this.state.editedElement.name}
                        ref={(objRef) => { this.editBarcodeModal = objRef }}
                    />

                    <ConfirmationModal
                        header={LM.getString('createNewCatalogNumber')}
                        message={LM.getString('DoYouWantToCreateNewBarcodeType')}
                        isOpen={this.state.confirmationModal}
                        className={this.props.className}
                        confirmClick={() => this.postNewBarcodeHandler()}
                        cancelClick={this.confirmationToggle} />
                </Container>
            </Container>
        )
    }
}
