import React, { Component } from 'react';
import { Container, Row, Col, Modal, ModalBody, ModalHeader } from 'reactstrap'
import './selectUser.css'
import { HistoryLine } from './HistoryLine';
import { LanguageManager as LM } from '../LanguageManager/Language';



/**
 * A pop-up screen with the user's specific Passwoard Change  History 
 * TODO --All strings will be replaced by languages
 */
export class PasswoardChangeHistory extends Component {

    render() {
        return (
            <Modal  size="lg" isOpen={this.props.isOpen} toggle={this.props.toggle} >
                <ModalHeader toggle={this.props.toggle} >
                    <Container >
                        <Row>
                            <p style={{ width: "100%" ,textAlign: "center",color:'#4864de' , fontWeight :'820' }}>{LM.getString("changePassHistory")}</p>
                        </Row>
                        <Row style={{ width: "100%" ,textAlign: "center",color:'#000' }}>
                            <Col sm="5"><h5>{LM.getString("name")} 
                            {this.props.user.first_name + ' ' + this.props.user.last_name}
                            </h5></Col>
                            <Col sm="4"><h5>{LM.getString("business_name")}</h5></Col>
                            <Col sm="3"><h5>{LM.getString("clientNumber")}</h5></Col>
                        </Row>
                    </Container>
                </ModalHeader>
                <ModalBody>
                    <Container >
                        <Row className="PassChangeHeader">
                            <Col sm="3"><h5>{LM.getString("userNm")}</h5></Col>
                            <Col sm="3"><h5>{LM.getString("user")}</h5></Col>
                            <Col sm="3"><h5>{LM.getString("ip")}</h5></Col>
                            <Col sm="2"><h5>{ LM.getString("date")}</h5></Col>
                            <Col sm="1"><h5>{ LM.getString("time")}</h5></Col>
                        </Row>
                        {
                           this.props.historyDetails.length > 0 ? this.props.historyDetails.map((cur, index) => <HistoryLine key={index} index={index} {...cur} />):<React.Fragment/>
                        }
                    </Container>
                </ModalBody>
            </Modal>
        )
    }
}