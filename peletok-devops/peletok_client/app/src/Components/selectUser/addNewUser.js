import React, { Component } from 'react';
import { Container, Row, Col, Card, CardBody, CardText, Button } from 'reactstrap'
import './selectUser.css'
import { LanguageManager as LM } from '../LanguageManager/Language';
import { UserFieldComponent } from './userFieldComponent'
import InputTypeSimpleSelect from "../InputUtils/InputTypeSimpleSelect";
import InputTypeAddress from '../InputUtils/InputTypeAddress';
import { UsersScreen } from './selectUser';
import { postNewUsersData, getRole, getDoseModeFieldValueExsist } from '../DataProvider/DataProvider';
import { Notifications } from '../Notifications/Notifications'
import { Loader } from '../Loader/Loader'
import Asterisk from '../Ui/Asterisk'
import { SellerRolesEnum } from '../../enums/'

const userFieldsWithID = ["first_name", "last_name", "phone_number", "account_mail", "israeli_id", "id_validity", "username", "password"]
const userFields = ["first_name", "last_name", "phone_number", "account_mail", "username", "password"]
/**
 * 
 * TODO - all strings in the state shuld became from astring file;
 * TODO - The logic that makes the changes in the database
 * TODO -It is clear that all user entries are currently fake and are not connected to the server
 * Component that returns components that return a row for all users
 */
export class AddNewUser extends Component {
    constructor(props) {
        super(props);
        this.state = {
            first_name: "",
            last_name: "",
            israeli_id: "",
            phone_number: "",
            account_mail: "",
            id_validity: "",
            username: "",
            password: "",
            roles: [],
            render: true,

        };
        this.onAdd = this.onAdd.bind(this);
        this.onChangeWraper = props.stateChanger || this.localOnChangeWraper;
        this.onChangeWraper.bind(this);
    }

    localOnChangeWraper(key, validation) {
        var f = (e) => {
            if (e.target && validation(e.target.value)) {
                this.setState({ [key]: e.target.value });
            }
        }
        return f;
    }

    /**
     * validate new userName (value) dose not exist as userName (login_name) of user model (user)
     * validate new userName (value) dose not exist as userName (login_name) of user model (user)
     */
    validateuserNameAndIsraeliId = async (userName, israeliId) => {
        let checkUserNameExist = await getDoseModeFieldValueExsist('user', 'login_name', userName);
        let checkIsraeliIdExist = await getDoseModeFieldValueExsist('user', 'israeli_id', israeliId);
        if (checkUserNameExist) {
            Notifications.show(LM.getString("username") + " " + userName + " " + LM.getString("isAlreadyExist"), "danger")
            return false
        }
        if (checkIsraeliIdExist) {
            Notifications.show(LM.getString("idNumCreditCard") + " " + israeliId + " " + LM.getString("isAlreadyExist"), "danger")
            return false
        } else {
            return true
        }
    }
    /**
     * The function checks whether all fields are filled in and warns if not, and sends them to the server * as a new user
     **/

    validate = async (sellerId) => {
        let isFill = true
        for (let key in userFields) {
            if (!this[userFields[key]].getIsChange()) {
                this[userFields[key]].setValue();
                isFill = false
            }
        }

        let data = {};
        for (var i in userFieldsWithID) {
            if (this.state[userFieldsWithID[i]] != '') {
                data[userFieldsWithID[i]] = this.state[userFieldsWithID[i]];
            }
            else {
            }
        }

        data['business_id'] = sellerId || this.props.currentDistributor;
        data["role_id"] = this.role.getValue().value;
        data["street"] = this.address.getValue().value;
        let is_valid_name_id = await this.validateuserNameAndIsraeliId(data["username"], data["israeli_id"])
        return [isFill, is_valid_name_id, data]

    }
    onAdd = async (sellerId = null) => {

        let [isFill, is_valid_name_id, data] = await this.validate(sellerId)

        if (isFill && is_valid_name_id) {
            Loader.show()
            // TODO add all validation and rolse
            let res = false
            await postNewUsersData(data).then((response) => {
                Loader.hide()
                Notifications.show(LM.getString("UserUpdatedSuccessfully"), "success")
                if (this.props.updateUsers) {
                    this.props.updateUsers()
                }
                this.setState({
                    first_name: "",
                    last_name: "",
                    israeli_id: "",
                    phone_number: "",
                    account_mail: "",
                    id_validity: "",
                    username: "",
                    password: "",
                    render: false

                }, ()=>{
                    this.setState({
                        render: true
                    }, () => {
                        this.role.setValue({});
                        this.address.setValue('');
                        this.props.addUserClick()
                        res = true;
                    
                        })
                })
            }
            ).catch(
                function (error) {
                    // TODO - add real error handling
                    Loader.hide()
                    res = false;
                    console.log(error)
                    Notifications.show(error.response.data.error, "danger");
                }
            );
            return res
        } else {
            Loader.hide()
            return
        }
    }

    componentDidMount() {
        getRole().then((res) => {
            this.setState({ roles: res }, () => {
                if (this.props.creatBusiness) {
                    this.role.setValue(SellerRolesEnum.SELLER_ROLE_ID);                    
                }
            })
        })
    }

    render() {
        
        return (
            this.state.render ? <Container>
                <Row>
                    <p className="edit_user_form headerRowForEditUser1"> {LM.getString("selectUseraddUserHeader")}</p>
                </Row>
                <Row>
                    <UserFieldComponent
                        ref={(componentObj) => { this.first_name = componentObj }}
                        required={true}
                        errorMessage={LM.getString("isRequiredValidationFunctionMsg")}
                        type="text"
                        value={this.state.first_name}
                        size={4}
                        fieldTitle={LM.getString("firstName")}
                        id={"selectUserfirstName"}
                        changeHandler={this.onChangeWraper("first_name", UsersScreen.maxLength(15))}
                    />
                    <UserFieldComponent
                        ref={(componentObj) => { this.last_name = componentObj }}
                        required={true}
                        errorMessage={LM.getString("isRequiredValidationFunctionMsg")}
                        type="text"
                        value={this.state.last_name}
                        size={4}
                        fieldTitle={LM.getString("selectUserlastName")}
                        id={"selectUserlastName"}
                        changeHandler={this.onChangeWraper("last_name", UsersScreen.maxLength(15))}
                    />
                    <UserFieldComponent
                        ref={(componentObj) => { this.israeli_id = componentObj }}
                        type="text"
                        value={this.state.israeli_id}
                        size={4}
                        fieldTitle={LM.getString("selectUseridNum")}
                        id={"selectUseridNum"}
                        changeHandler={this.onChangeWraper("israeli_id", UsersScreen.numberWithMaxLength(9))}
                    />
                </Row>
                <Row>
                    <UserFieldComponent
                        ref={(componentObj) => { this.phone_number = componentObj }}
                        required={true}
                        errorMessage={LM.getString("isRequiredValidationFunctionMsg")}
                        type="text"
                        value={this.state.phone_number}
                        size={4}
                        fieldTitle={LM.getString("selectUserTelNum")}
                        id={"selectUserTelNum"}
                        changeHandler={this.onChangeWraper("phone_number", UsersScreen.numberWithMaxLength(10))}
                    />
                    <UserFieldComponent
                        ref={(componentObj) => { this.account_mail = componentObj }}
                        required={true}
                        errorMessage={LM.getString("isRequiredValidationFunctionMsg")}
                        type="email"
                        value={this.state.account_mail}
                        size={4}
                        fieldTitle={LM.getString("selectUsermail")}
                        id={"selectUsermail"} changeHandler={this.onChangeWraper("account_mail", UsersScreen.always)}
                    />
                    <UserFieldComponent
                        ref={(componentObj) => { this.id_validity = componentObj }}
                        type="date"
                        value={this.state.idCardValidDate}
                        size={4}
                        fieldTitle={LM.getString("selectUseridCardValidDate")}
                        id={"selectUseridCardValidDate"}
                        changeHandler={this.onChangeWraper("id_validity", UsersScreen.maxLength(11))}
                    />
                </Row>
                <Row>
                    <Col sm={12} className={'edit_user_form'}>
                        <Card>
                            <CardBody style={{ padding: "0.04rem 0.8rem" }}>
                                <div className={"card-text"} style={{ textAlign: "start", fontSize: "0.75rem", marginBottom: '0' }}>
                                    {LM.getString("address")}
                                    <Asterisk show={true} />
                                    <InputTypeAddress id={"selectUseraddres"}
                                        required={true}
                                        ref={(componentObj) => { this.address = componentObj }}
                                    >
                                    </InputTypeAddress>
                                </div>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
                <Row>
                    <UserFieldComponent
                        ref={(componentObj) => { this.username = componentObj }}
                        required={true}
                        errorMessage={LM.getString("isRequiredValidationFunctionMsg")}
                        type="text"
                        value={this.state.username}
                        size={4}
                        fieldTitle={LM.getString("username")}
                        id={"selectUserName"}
                        changeHandler={this.onChangeWraper("username", UsersScreen.maxLength(15))}
                    />
                    <UserFieldComponent
                        ref={(componentObj) => { this.password = componentObj }}
                        required={true}
                        errorMessage={LM.getString("isRequiredValidationFunctionMsg")}
                        type="password"
                        value={this.state.password}
                        size={4}
                        fieldTitle={LM.getString("selectUserpass")}
                        id={"selectpassword"}
                        changeHandler={this.onChangeWraper("password", UsersScreen.maxLength(20))}
                    />
                    {/* {this.props.Managment ?//TODO --real options
                        <UserFieldComponent
                        ref={(componentObj) => { this.options = componentObj }}
                        required={true}
                        errorMessage={LM.getString("isRequiredValidationFunctionMsg")}
                        size={4}
                        changeHandler={this.selectChange}
                        datalistID='0'
                        options={this.state.roles ? this.state.roles.map(role => { return { label: role.name, value: role.id } }) : ["no roles"]}
                        select={true}
                        id={"Managment"} fieldTitle={LM.getString("permissions")}
                        />
                        : <div />
                    } */}
                    <Col sm={4} className={'edit_user_form'}>
                        <Card>
                            <CardBody style={{ padding: "0.04rem 0.8rem" }}>
                                <div className={"card-text"} style={{ textAlign: "start", fontSize: "0.75rem", marginBottom: '0' }}>
                                    {LM.getString("permissions")}
                                    <Asterisk show={true} />
                                    <InputTypeSimpleSelect
                                        ref={(componentObj) => { this.role = componentObj }}
                                        sm={4}
                                        required={true}
                                        id={"userPermissionRole"}
                                        className="transparentInputselect" style={{ border: 'none', boxShadow: 'none', minHeight: '1.3rem' }}
                                        options={this.state.roles ? this.state.roles.map(role => { return { label: role.name, value: role.id } }) : []}
                                    >
                                    </InputTypeSimpleSelect>
                                </div>
                            </CardBody>
                        </Card>
                    </Col>
                    {!this.props.creatBusiness && <Col sm={4} className={'edit_user_form'}>
                        <Button className="px-5 btn-lg regularButton" style={{ width: 'max-content', backgroundColor: 'var(--regButton)' }} disabled={false} id="addselectUseraddUser" onClick={() => this.onAdd()}>{LM.getString("selectUseradd")}</Button>
                    </Col>}
                </Row>
            </Container> : <></>
        )
    }
}