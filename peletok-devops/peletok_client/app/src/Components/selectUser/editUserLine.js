import React, { Component } from 'react';
import { Container, Row, Col, Button, Card, CardBody, CardText } from 'reactstrap';
import './selectUser.css'
import { ChangePassword } from './changePass';
import { LoginHistory } from './loginhistory';
import { PasswoardChangeHistory } from './PasswoardChangeHistory';
import { LanguageManager as LM } from '../LanguageManager/Language';
import list from './../../Assets/Icons/MenuIcons/clipboards.svg'
import { UserFieldComponent } from './userFieldComponent';
import { UsersScreen } from './selectUser';
import { UserDocumentsManagement } from './UserDocumentsManagement'
import { putSellerUserData, getRole, deleteCureentUser, freezeUser, defrostUser, getChangePassHistory } from '../DataProvider/DataProvider';
import { faShieldAlt, faPencilAlt, faPlay, faPause } from '@fortawesome/free-solid-svg-icons';
import { Loader } from '../Loader/Loader';
import { Notifications } from '../Notifications/Notifications';
import { Confirm } from '../Confirm/Confirm';
import InputTypeSimpleSelect from "../InputUtils/InputTypeSimpleSelect";
import InputTypeAddress from "../InputUtils/InputTypeAddress";
import Asterisk from '../Ui/Asterisk'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {AuthService} from "../../services/authService";
import {UserSecurityDetails} from "./UserSecurityDetails";



var buttons;
const userFieldsWithID = ["first_name", "last_name", "phone_number", "account_mail", "israeli_id", "id_validity", "username", "role"]
const userFields = ["first_name", "last_name", "phone_number", "account_mail", "username"]

/**
 * 
 * TODO - all strings in the state shuld became from a string file;
 * TODO -It is clear that all user entries are currently fake and are not connected to the server
 * A component that holds information for a particular user
 */
export class EditUserLine extends Component {
    constructor(props) {
        super(props);

        this.state = {
            changePassHistory: [],
            isChangePassword: false,
            open: false,
            first_name: this.props.first_name,
            last_name: this.props.last_name,
            israeli_id: this.props.israeli_id,
            login_history: this.props.login_history ? this.props.login_history : "",
            providers: this.props.providers ? this.props.providers : "",
            id: this.props.id,
            phone_number: this.props.phone_number,
            account_mail: this.props.account_mail,
            id_validity: this.props.id_validity,
            number: this.props.number,
            entrance: this.props.entrance,
            floor: this.props.floor,
            role: this.props.role,
            username: this.props.username,
            password: this.props.password,
            openMLoginHistory: false,
            openMChangePassHistory: false,
            totalSales: this.props.sales,
            roles: [],
            disabled: true,
            showUserSecurityDetails: false
        };
        this.errorMessage = LM.getString("isRequiredValidationFunctionMsg");
        this.didFieldChange = this.didFieldChange.bind(this);
        this.onChangeWraper = props.stateChanger.bind(this);
        this.editPassToggle = this.editPassToggle.bind(this);
        this.save = this.save.bind(this);
    }

    /**
     * Change a Boolean value to get the login list
     */
    openModalLoginHistory = () => {
        this.setState({ openMLoginHistory: !this.state.openMLoginHistory })

    }
    openMChangePassHistory = () => {
        if (!this.state.openMChangePassHistory) {
            getChangePassHistory().then(res => {
                this.setState({ changePassHistory: res }, () => {
                    this.setState({ openMChangePassHistory: !this.state.openMChangePassHistory })
                })
            })
        }
        else {
            this.setState({ openMChangePassHistory: !this.state.openMChangePassHistory })
        }

    }
    /**
     * Change a Boolean value to get a different component later
     */
    more_details = () => {

        this.setState({
            open: !this.state.open,
            isChangePassword: false,
            showUserSecurityDetails: false
        }, ()=>{
            this.address.setValue(this.props.street)
        })
    }
    security_details = () => {
    this.setState({
        open: false,
        isChangePassword: false,
        showUserSecurityDetails: true
    })

    }
    cancel_security_details = () => {
        this.setState({
            open: false,
            isChangePassword: false,
            showUserSecurityDetails: false
        })
    }


    edit = () => {
        this.setState({ disabled: false })
    }

    /**
     * Change a Boolean value toTo receive a password change screen
     */
    editPassToggle = () => {
        this.setState({ isChangePassword: !this.state.isChangePassword })
    }
    cancelation = () => {
        this.setState({ open: !this.state.open });
        this.setState({
            first_name: this.props.first_name,
            last_name: this.props.last_name,
            israeli_id: this.props.israeli_id,
            id: this.props.id,
            phone_number: this.props.phone_number,
            providers: this.props.providers ? this.props.providers : "",
            account_mail: this.props.account_mail,
            id_validity: this.props.id_validity,
            number: this.props.number,
            entrance: this.props.entrance,
            floor: this.props.floor,
            username: this.props.username,
            password: this.props.password,
        })
    }
    /**
     * A function that "frees" the user
     */
    freeze = () => {
        Loader.show()
        freezeUser(this.props.id).then(
            function (response) {
                Loader.hide()
                Notifications.show(LM.getString("UserUpdatedSuccessfully"), "success")
            }
        ).catch(
            function (error) {
                Loader.hide()
                Notifications.show(error, "danger")
            }
        );
        if (this.props.updateUsers) {
            this.props.updateUsers()
        }
    }
    /**
     * save the inputs Fields
     */
    save() {
        for (var i in userFields) {
            if (this.state[userFields[i]] === "") {
                return
            }
        }
        var myObject = {};
        userFieldsWithID.forEach(element => {
            if (this.didFieldChange(element)) {
                if (element == 'role') { myObject["role_id"] = this.state.role.value }
                else { myObject[element] = this.state[element] }
            }
        });
        myObject.street = this.address.getValue().value
        console.log('myObject', myObject);

        if (Object.keys(myObject).length != 0) {
            Loader.show()
            putSellerUserData(this.props.id, myObject).then(
                function (response) {
                    Loader.hide()
                    Notifications.show(LM.getString("UserUpdatedSuccessfully"), "success")
                }
            ).catch(
                function (error) {
                    Loader.hide()
                    Notifications.show(error, "danger")
                }
            );
            if (this.props.updateUsers) {
                this.props.updateUsers()
            }
            this.setState({ open: !this.state.open })
        } else {
            Notifications.show(LM.getString("noChanges"))
        }
    }
    /**
     * Function that cancels the user's freeze
     */
    unFreeze = () => {
        Loader.show()
        defrostUser(this.props.id).then(
            function (response) {
                Loader.hide()
                Notifications.show(LM.getString("UserUpdatedSuccessfully"), "success")
            }
        ).catch(
            function (error) {
                Loader.hide()
                Notifications.show(error.response.data.error, "danger")
            }
        );
        if (this.props.updateUsers) {
            this.props.updateUsers()
        }
    }

    componentDidMount() {
        getRole().then((res) => {
            this.setState({ roles: res })
        })

    }

    didFieldChange(fieldName) {
        return this.state[fieldName] !== this.props[fieldName] && !(!this.state[fieldName] && !this.props[fieldName])
    }

    render() {

        /**
         * Create different buttons
         */
        if (!this.state.open) {
            if (this.props.status === 1 || this.props.status === null) {

                buttons =
                    [

                        <Col className={'buttons justify-content-end ' + (LM.getDirection() === "rtl" ? 'pr-xl-0' : 'pl-xl-0')}>
                            <Button className={'border-radius-3 py-0 px-2 regularButton fs-15 font-weight-bold ' +
                                'line-height-1_3'} size="sm" onClick={this.security_details.bind(this)}
                                id={`editMoreDetails` + this.props.id}>{LM.getString('security')}</Button>
                            <Button className={'border-radius-3 py-0 px-2 regularButton fs-15 font-weight-bold ' +
                                'line-height-1_3 mx-1'} size="sm" onClick={this.more_details.bind(this)}
                                id={`editMoreDetails` + this.props.id}>{LM.getString("more_details")}
                            </Button>
                            <Button className={'border-radius-3 py-0 px-2 regularButton fs-15 font-weight-bold ' +
                                'line-height-1_3'} size="sm" onClick={this.freeze.bind(this)}
                                id={`editselectUserfroze` + this.props.id}>{LM.getString("selectUserfroze")}
                            </Button>
                        </Col>,
                        <Col className={'buttons-icons justify-content-end '
                            + (LM.getDirection() === "rtl" ? 'pr-xl-0' : 'pl-xl-0')}>
                            <a className={'pointer'} onClick={this.security_details.bind(this)}
                                id={`editSecurityDetails` + this.props.id}><FontAwesomeIcon icon={faShieldAlt} size="1x" />
                            </a>
                            <a className={'pointer mx-3'} onClick={this.more_details.bind(this)}
                                id={`editMoreDetails` + this.props.id}><FontAwesomeIcon icon={faPencilAlt} size="1x" />
                            </a>
                            <a className={'pointer'} onClick={this.freeze.bind(this)}
                                id={`editselectUserfroze` + this.props.id}><FontAwesomeIcon icon={faPause} size="1x" />
                            </a>
                        </Col>
                    ]
            } if (this.props.status === 2) {
                buttons =
                    [
                        <Col className={'buttons justify-content-end'}>
                            <Button color="secondary" size="sm" onClick={this.unFreeze.bind(this)}
                                id={`editselectUserCancelFroze` + this.props.id}
                                className={'border-radius-3 py-0 px-2 mx-1 fs-15 font-weight-bold line-height-1_3 ' +
                                    'white-space'}
                            >{LM.getString("selectUserCancelFroze")}</Button>
                        </Col>,
                        <Col className={'buttons-icons justify-content-end'}>
                            <a className={'pointer mx-3'} onClick={this.unFreeze.bind(this)}
                                id={`editselectUserCancelFroze` + this.props.id}>
                                <FontAwesomeIcon icon={faPlay} size="1x" /></a>
                        </Col>
                    ]
            } else if (this.state.showUserSecurityDetails) {
                buttons =
                    [
                        <Col className={'d-flex justify-content-end'}>
                            <Button color="secondary" size="sm" onClick={this.cancel_security_details}
                                    id={`editselectUsercancelation` + this.props.id} className='border-radius-3 py-0 px-2 fs-15 font-weight-bold line-height-1_3'
                            >{LM.getString("cancel")}</Button>
                        </Col>
                    ]
            }
        } else {
            buttons =
                [
                    <Col className={'d-flex justify-content-end'}>
                        {this.state.disabled && AuthService.canEdit() && <Button size="sm" onClick={this.edit.bind(this)} id={`editselectUseredit` + this.props.id}
                                className={'border-radius-3 py-0 px-2 mx-1 regularButton fs-15 font-weight-bold line-height-1_3'}>{LM.getString("selectUseredit")}
                        </Button>}
                        {!this.state.disabled && <Button size="sm" onClick={this.save.bind(this)} id={`editSupplierSave` + this.props.id}
                            className='border-radius-3 py-0 px-2 mx-1 infoButton fs-15 font-weight-bold line-height-1_3'>{LM.getString("save")}
                        </Button>}
                        <Button color="secondary" size="sm" onClick={this.cancelation}
                            id={`editselectUsercancelation` + this.props.id} className='border-radius-3 py-0 px-2 fs-15 font-weight-bold line-height-1_3'
                        >{LM.getString("cancel")}</Button>
                    </Col>
                ]
        }


        /**
         * Return a different component by a valid Boolean value
         */
        let display = this.state.open &&
                <React.Fragment>
                    <Row className={'mt-1'}>
                        <UserFieldComponent disabled={this.state.disabled} required={true} errorMessage={this.errorMessage} type="text" value={this.state.first_name} size={4} fieldTitle={LM.getString("firstName")} id={"selectUserfirstName"} changeHandler={this.onChangeWraper("first_name", UsersScreen.maxLength(15))} />
                        <UserFieldComponent disabled={this.state.disabled} required={true} errorMessage={this.errorMessage} type="text" value={this.state.last_name} size={4} fieldTitle={LM.getString("selectUserlastName")} id={"selectUserlastName"} changeHandler={this.onChangeWraper("last_name", UsersScreen.maxLength(15))} />
                        <UserFieldComponent disabled={this.state.disabled} type="text" value={this.state.israeli_id} size={4} fieldTitle={LM.getString("selectUseridNum")} id={"selectUseridNum"} changeHandler={this.onChangeWraper("israeli_id", UsersScreen.numberWithMaxLength(9))} />
                        <UserFieldComponent disabled={this.state.disabled} required={true} errorMessage={this.errorMessage} type="text" value={this.state.phone_number} size={4} fieldTitle={LM.getString("selectUserTelNum")} id={"selectUserTelNum"} changeHandler={this.onChangeWraper("phone_number", UsersScreen.numberWithMaxLength(10))} />
                        <UserFieldComponent disabled={this.state.disabled} required={true} errorMessage={this.errorMessage} type="email" value={this.state.account_mail} size={4} fieldTitle={LM.getString("selectUsermail")} id={"selectUsermail"} changeHandler={this.onChangeWraper("account_mail", UsersScreen.always)} />
                        <UserFieldComponent disabled={this.state.disabled} required={true} type="date" value={this.state.id_validity} size={4} fieldTitle={LM.getString("selectUseridCardValidDate")} id={"selectUseridCardValidDate"} changeHandler={this.onChangeWraper("id_validity", UsersScreen.maxLength(11))} />
                        <Col sm={12} className={'edit_user_form'}>
                            <Card>
                                <CardBody style={{ padding: "0.04rem 0.8rem" }}>
                                    <div className={"card-text"} style={{ textAlign: "start", fontSize: "0.75rem", marginBottom: '0' }}>
                                        {LM.getString("address")}
                                        <Asterisk show={true} />
                                        <InputTypeAddress
                                            id={"selectUseraddres"}
                                            required={true}
                                            ref={(componentObj) => { this.address = componentObj }}
                                        >
                                        </InputTypeAddress>
                                    </div>
                                </CardBody>
                            </Card>
                        </Col>
                        <UserFieldComponent disabled={this.state.disabled} required={true} errorMessage={this.errorMessage} type="text" value={this.state.username} size={4} fieldTitle={LM.getString("username")} id={"selectUserName"} changeHandler={this.onChangeWraper("username", UsersScreen.maxLength(15))} />
                        {/* TODO -- get the real options */}
                        {/* {this.props.Managment ? <UserFieldComponent errorMessage={this.errorMessage} size={4} changeHandler={this.selectChange} datalistID='0' providers={{ options: ["מזכירה", "מנקה"] }} select={true} id={"EditUserLine"} fieldTitle={LM.getString("permissions")} />
                            : <div />
                        } */}
                        <Col sm={4} className={'edit_user_form'}>
                            <Card>
                                <CardBody style={{ padding: "0.04rem 0.8rem" }}>
                                    <div className={"card-text"} style={{ textAlign: "start", fontSize: "0.75rem", marginBottom: '0' }}>
                                        {LM.getString("permissions")}
                                        <Asterisk show={true} />
                                        <InputTypeSimpleSelect
                                            disabled={this.state.disabled}
                                            required={true}
                                            id={"editUserPermissionRole"}
                                            default={this.state.role ? this.state.role.id : ""}
                                            className="transparentInputselect" style={{ border: 'none', boxShadow: 'none', minHeight: '0.9rem' }}
                                            ref={(componentObj) => { this.role = componentObj }} sm={4}
                                            options={this.state.roles ? this.state.roles.map(role => { return { label: role.name, value: role.id } }) : []}
                                            onChange={(role) => { this.setState({ role: role }, () => { this.onChangeWraper("role", UsersScreen.always) }) }}
                                        >

                                        </InputTypeSimpleSelect>

                                    </div>
                                </CardBody>

                            </Card>
                        </Col>
                        <Col sm={4} className={'edit_user_form'}>
                            <Button disabled={this.state.disabled} className={'btn btn-lg btn-secondary font-weight-bold px-4 regularButton border-radius-3 fs-15'} onClick={this.editPassToggle} id={'editchangePassword'}>{LM.getString("changePassword")}</Button>
                            <img id={'imgHistory' + this.props.id} className="listImg" onClick={this.openMChangePassHistory} src={list} alt="" />
                            <PasswoardChangeHistory historyDetails={this.state.changePassHistory} id={"PasswoardChangeHistory" + this.props.id} user={this.props} isOpen={this.state.openMChangePassHistory} toggle={this.openMChangePassHistory} />
                        </Col>
                        <ChangePassword updateUsers={this.props.updateUsers} userId={this.props.id} toggle={this.editPassToggle}
                            theUser={this.props} isOpen={this.state.isChangePassword} currentSeller={this.props.currentDistributor} />
                    </Row>

                    {false && <React.Fragment>
                        <UserDocumentsManagement updateUsers={this.props.updateUsers} id={this.props.id} onChangeWraper={this.onChangeWraper} />
                    </React.Fragment>}
                </React.Fragment>
            ;
        return (
            <div className={'fs-17 ' + (this.state.open || this.state.showUserSecurityDetails ? "rowsForUsersData " : "rowForEditUser ") + (this.props.status === 2 ? 'rowForEditUserFreeze' : '')}>
                <Row className={'mx-0 white-space'} id={'row' + this.props.id} >
                    <Col id={"index" + this.props.id} xs="1">{this.props.index}</Col>
                    <Col id={"name" + this.props.id} md="2" sm="3" xs="3">{this.state.username != null ? this.state.username : this.props.id}</Col>
                    <Col id={"phone_number" + this.props.id} sm="2" className={'d-none d-md-block'}>{this.state.phone_number}</Col>
                    <Col id={"totalSales" + this.props.id} md="2" xs="3">{this.state.totalSales}</Col>
                    <Col id={"lastLogin" + this.props.id} xl="2" className={'col d-none d-lg-block'}>
                        <img id={'img' + this.props.id} className="listImg" onClick={this.openModalLoginHistory} src={list} alt="" />
                        {this.state.login_history && this.state.login_history[this.state.login_history.length - 1] && this.state.login_history[this.state.login_history.length - 1].timestamp_login.split('T')[0]}
                    </Col>
                    <LoginHistory id={"LoginHistory" + this.props.id} user={this.props} isOpen={this.state.openMLoginHistory} toggle={this.openModalLoginHistory} />
                    {buttons}
                </Row>
                <Container>
                    {display}
                    {this.state.showUserSecurityDetails && <UserSecurityDetails user={this.props.user}/>}
                </Container>
            </div>
        );
    }
}