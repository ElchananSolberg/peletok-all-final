import React, { Component } from 'react';

import { Row, Col } from 'reactstrap';

import { LanguageManager as LM } from '../LanguageManager/Language';
import { Notifications } from '../Notifications/Notifications';

import { getActivitySession } from '../DataProvider/DataProvider';
import {UserSecurityDetailsRow} from "./UserSecurityDetailsRow";


/** UserSecurityDetailsTable component is a part of a ContractInfo page.
 * It creates a Contract Info table. It renders the a table headers and needed amount of table rows with dynamic data from this.state.infos array.
*/
class UserSecurityDetailsTable extends Component {
    constructor(props) {
        super(props);

        this.state = {
            infos: [],
        }
    }

    /** Function that opens the GET html request (getActivitySession from DataProvider).
     *  Saves an array of infos from the server into infos variable. */
    loadIpOrTokenInfoList = (forceUpdate = false) => {
        getActivitySession(this.props.userId, forceUpdate)
            .then(
                (res) => {
                    this.props.isIPSecured && this.setState({ infos: res.ips });
                    this.props.isDeviceSecured && this.setState({ infos: res.tokens });
                }
            ).catch(
                (err) => {
                    Notifications.show(err, 'danger');
                }
            )
    }
    componentDidMount() {
        this.loadIpOrTokenInfoList();
    }

    render() {
        let ipAddress = LM.getString("ipAddress");
        let token = LM.getString("token");
        let status = LM.getString("status");
        let userLogged = LM.getString("userLogged");
        let actions = LM.getString("actions");

        let ipAddressesNotFound = LM.getString("ipAddressesNotFound");
        let tokensNotFound = LM.getString("tokensNotFound");

        let tableRows = this.state.infos;
        let tableRowsRendered = tableRows.map(current => {
            return <UserSecurityDetailsRow {...current} loadIpOrTokenInfoList={this.loadIpOrTokenInfoList} />
        });

        return (
            <>
                {!tableRows[0] &&
                    <Row>
                        <Col>
                            <hr />
                        </Col>
                    </Row>
                }
                <Row>
                    {this.props.isIPSecured && !tableRows[0] &&
                        <Col>
                            {ipAddressesNotFound}
                        </Col>
                    }
                    {this.props.isDeviceSecured && !tableRows[0] &&
                        <Col>
                            {tokensNotFound}
                        </Col>
                    }
                </Row>
                {tableRows[0] &&
                    <div className="d-flex tagsManagementTableHeaders">
                        {this.props.isIPSecured &&
                            <Col xs='3'>
                                {ipAddress}
                            </Col>
                        }
                        {this.props.isDeviceSecured &&
                            <Col xs='3'>
                                {token}
                            </Col>
                        }
                        <Col xs='3'>
                            {actions}
                        </Col>
                    </div>
                }
                {tableRowsRendered}
            </>
        )
    }
}
export default UserSecurityDetailsTable;