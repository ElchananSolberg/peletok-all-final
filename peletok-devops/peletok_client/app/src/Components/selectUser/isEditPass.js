import React, { Component } from 'react';
import { Button, Modal, ModalBody, ModalFooter } from 'reactstrap'
import './selectUser.css'
import checkMark from './img.png'
import { LanguageManager as LM } from '../LanguageManager/Language';




/**
 * Displays a confirmation screen that the password has been changed
 * TODO --All strings will be replaced by languages
 */
export class IsEditPass extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: true,
            Cancel: LM.getString("cancel"),
            messege: LM.getString("selectUserPasswordChanged"),
        }
        this.toggle = this.toggle.bind(this);
    }
    toggle() {
        this.setState({
            modal: !this.state.modal
        });
        this.props.isedit()
    }

    render() {
        return (
            <div>
                <Modal className="changePass" isOpen={this.state.modal} toggle={this.toggle} >
                    <ModalBody>                      
                        <img src={checkMark}alt="" ></img>
                    </ModalBody>
                    <p >{this.state.messege}</p>
                    <ModalFooter>
                        <Button color="secondary" id="selectUserCancel"onClick={this.toggle}>{this.state.Cancel}</Button>
                    </ModalFooter>
                </Modal>
            </div>
        )
    }
}