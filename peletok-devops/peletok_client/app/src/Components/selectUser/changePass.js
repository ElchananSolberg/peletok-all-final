import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, FormGroup, Form, Row } from 'reactstrap';
import './selectUser.css';

import { LanguageManager as LM } from '../LanguageManager/Language';
import { Notifications } from '../Notifications/Notifications';

import InputTypeText from '../InputUtils/InputTypeText';

import { IsEditPass } from './isEditPass';
import { changePass ,putUserPassword } from '../DataProvider/DataProvider'

/** A component that change the password */

export class ChangePassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isEdit: false

        }
        this.changePassfun = this.changePassfun.bind(this);
        this.closeModal = this.closeModal.bind(this);

    }

    closeModal() {
        this.setState({
            isEdit: !this.state.isEdit
        });
        this.props.toggle();
    }
    passwordsNotIdentical = (value) => {
        return value == this.newPass1.getValue().value
    }
    changePassfun() {
        let obj = {};

        if (this.newPass1.getValue().value == this.newPass2.getValue().value && this.oldPass.getValue().value != '' && this.newPass2.getValue().value != '') {
            // obj.old_password = this.oldPass.getValue().value;
            obj.current_password = this.oldPass.getValue().value;            
            obj.new_password = this.newPass1.getValue().value;
            if(this.props.currentDistributor) {obj.user_id = this.props.currentDistributor};
            // changePass(this.props.userId, obj).then(
                putUserPassword(obj).then(
                () => {
                    Notifications.show( LM.getString("UserUpdatedSuccessfully"), 'success');
                    // this.setState({
                    //     isEdit: !this.state.isEdit
                    // });
                    this.closeModal()
                    if (this.props.updateUsers) {
                        this.props.updateUsers()
                    }
                }
            ).catch(
                function (error) {
                    if (error.response && error.response.data) { Notifications.show( error.response.data, 'danger') }
                    else { Notifications.show( LM.getString("unrecognizedError"), 'danger') }
                }
            );

        }
        else if (this.newPass1.getValue().value !== this.newPass2.getValue().value && this.oldPass.getValue().value != '' && this.newPass2.getValue().value != '') {
            Notifications.show( LM.getString("passwordsNotIdentical"), 'warning')
        }
        else if (this.oldPass.getValue().value == '' || this.newPass2.getValue().value == '') {
            Notifications.show( LM.getString("enterAllFields"), 'warning')
        }
    }
    render() {

        let userFullName = this.props.theUser.first_name;
        if (this.props.theUser.last_name) userFullName = userFullName + ' ' + this.props.theUser.last_name;

        let iseditPass =/* !this.state.isEdit ?*/

            <div>
                <Modal isOpen={this.props.isOpen} toggle={this.props.toggle} className={this.props.className + " modal-dialog-centered"}>
                    <ModalHeader toggle={this.props.toggle} charCode="&#215;" className={(LM.getDirection() === "rtl" ? "mr-sm-1 pr-sm-4" : "ml-sm-1 pl-sm-4")}>
                        <p className={'mt-2 mb-0 '}>{LM.getString("changePasswordForUser") + ' ' + userFullName}</p>
                    </ModalHeader>
                    <ModalBody className={'px-sm-4 mx-sm-1'}>
                        <Form>
                            <h3 className={'fs-22 font-weight-light'}>{LM.getString("enterYourCurrentPassword")}</h3>
                            <FormGroup>
                                <InputTypeText
                                    ref={(componentObj) => { this.oldPass = componentObj }}
                                    id="currentPassword"
                                    maxLength='20'
                                    title={LM.getString("currentPassword") + ':'}
                                />
                            </FormGroup>
                            <h3 className={'mb-2 mb-sm-4'}>
                                {LM.getString("enterNewPassword")}
                            </h3>
                            <FormGroup>
                                <InputTypeText
                                    ref={(componentObj) => { this.newPass1 = componentObj }}
                                    id="newPassword"
                                    maxLength='20'
                                    title={LM.getString("newPassword")+':'}
                                />
                            </FormGroup>
                            <FormGroup>
                                <InputTypeText
                                    title={LM.getString("typeNewPasswordAgain")+':'}
                                    ref={(componentObj) => { this.newPass2 = componentObj }}
                                    id="typeNewPasswordAgain"
                                    maxLength='20'
                                    notValidMessage={LM.getString("passwordsNotIdentical")}
                                    validationFunction={this.passwordsNotIdentical}
                                />
                            </FormGroup>
                        </Form>
                        <Row className='mx-auto'>
                            <Button
                                size='lg'
                                onClick={this.changePassfun}
                                id="updatePasswordButton"
                                className={'regularButton px-4 fs-17 border-radius-3 border-unset font-weight-bold'}
                            >
                                {LM.getString("editPass")}
                            </Button>
                            <Button
                                size='lg'
                                color="secondary"
                                id="cancelButton"
                                onClick={this.props.toggle}
                                className={'px-4 fs-17 border-radius-3 border-unset font-weight-bold ' + (LM.getDirection() === "rtl" ? "mr-2" : "ml-2")}
                            >
                                {LM.getString("cancel")}
                            </Button>
                        </Row>
                    </ModalBody>
                </Modal>
            </div> /*: <IsEditPass isedit={this.closeModal} />*/

        return (
            iseditPass
        )
    }
}