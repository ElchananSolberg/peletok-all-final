import React, { Component } from 'react';
import { LanguageManager as LM } from '../LanguageManager/Language';
import './selectUser.css';
import { Container, Row, Col, Button } from 'reactstrap';
import { getUsersDocuments } from '../DataProvider/Functions/DataGeter'
import { deleteDoc } from '../DataProvider/Functions/DataDeleter'
import { Notifications } from '../Notifications/Notifications'



export class Document extends Component {
    constructor(props) {
        super(props);


        this.state = {
            document: this.props.document,
            type: this.props.type,
            expired_time:this.props.expired_time,
            user_id: this.props.user_id,
            id: this.props.id,

        }
    }
   
    delete =()=>{
        deleteDoc(this.state.id).then(
            (res) => {
                { Notifications.show(LM.getString("successDocumentDeleting"), "info", 5000) }
              if (this.props.updateUsers) {
                this.props.updateUsers()
            }
            }).catch(
                (err) => {
                    alert(err);
                }
            )
    }
    render() {
       
        
        return (
            <React.Fragment>
               
                            <Row style={{ backgroundColor: 'var(--grayRow)', margin: '3px' }}>
                                <Col sm='3'>
                                    {this.state.document}
                                </Col>
                                <Col sm='3'>
                                    {this.state.type}
                                </Col>
                                <Col sm='3'>
                                    {this.state.expired_time.split('T')[0]}
                                </Col>
                                <Col sm='3' className={'d-flex justify-content-end'}>
                                    <Button className={'border-radius-3 py-0 px-2 regularButton fs-17 font-weight-bold line-height-1_3'} size="sm" onClick={this.edit} id={`editselectUseredit` + this.props.id}>{LM.getString("selectUseredit")}</Button>

                                    <Button className={'border-radius-3 py-0 px-2 fs-17 font-weight-bold line-height-1_3'} color="secondary" onClick={this.delete} size="sm" id={`editsupplierdelete` + this.props.id} >{LM.getString("delete")}</Button>
                                </Col>

                            </Row>
            </React.Fragment>

        )
    }
}