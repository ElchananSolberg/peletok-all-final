import React, { Component } from 'react';
import { Row, Col } from 'reactstrap'

/**
 * return a row with cols for historiLogin
 */

export class HistoryLine extends Component {



     getDayOfWeek(date) {
        var dayOfWeek = new Date(date).getDay();    
        return isNaN(dayOfWeek) ? null : ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'][dayOfWeek];
      }
    render() {
        
        return (
this.props.type=='history'?
            <Row className="loginHistoryModalLine fs-17 rowForEditUser">
                <Col sm="1" xs='1'>{this.props.user.id}</Col>
                <Col sm="3" xs='3'>{this.props.login_history && this.props.login_history.user_ip}</Col>
                <Col sm="3" xs='5'>{this.props.login_history != null?this.props.login_history.timestamp_login.split('T')[0]:"" }</Col>
                <Col sm="3"  className='d-none d-sm-block'>{this.props.login_history != null?this.getDayOfWeek(this.props.login_history.timestamp_login.split('T')[0] ):""}</Col>
                <Col sm="2" xs='2'>{this.props.login_history != null?this.props.login_history.timestamp_login.split('T')[1].split('Z')[0].split('.')[0]:"" }</Col>
            </Row>
            :
            <Row className="changePassHistoryModalLine">
                <Col sm="3"><h6>{this.props.id}</h6></Col>
                <Col sm="3"><h6>{this.props.login_name}</h6></Col>
                <Col sm="3"><h6>{this.props['UserPassword.ip']}</h6></Col>
                <Col sm="2"><h6>{this.props['UserPassword.deleted_at'].split('T')[0]}</h6></Col>
                <Col sm="1"><h6>{this.props['UserPassword.deleted_at'].split('T')[1].split('Z')[0].split('.')[0]}</h6></Col>
            </Row>

        )
    }

}