import React, { Component } from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import {LanguageManager as LM} from "../LanguageManager/Language";
import InputTypeCheckBox from "../InputUtils/InputTypeCheckBox";
import InputTypeNumber from "../InputUtils/InputTypeNumber";
import UserSecurityDetailsTable from "./UserSecurityDetailsTable";
import {
    postActivitySessionIP,
    postActivitySessionIPCancel,
    postActivitySessionToken, postActivitySessionTokenCancel
} from "../DataProvider/Functions/DataPoster";
import {Notifications} from "../Notifications/Notifications";
import {Confirm} from "../Confirm/Confirm";
import { IpTokenMaxEnum } from '../../enums/'
import {getUser, getUsersDocuments} from "../DataProvider/Functions/DataGeter";
import {Loader} from "../Loader/Loader";

export class UserSecurityDetails extends Component {
    constructor(props){
        super(props);
        this.state = {
            selectedUser: this.props.user,
            isIPSecured: false,
            isDeviceSecured: false,
        };
        this.fields = {};
    }

    fieldsLoad = () => {
        Object.keys(this.fields).forEach((key) => {
            if (this.state.selectedUser[key] !== null && this.state.selectedUser[key] !== undefined) {
                if (key !== "num_ip_allowed" && key !== "num_device_allowed") {
                    this.fields[key].setValue(this.state.selectedUser[key]);
                }
                else {
                    let secured = this.state.selectedUser[key] > 0 ? true : false
                    this.fields[key].setValue(secured);
                    if (key === "num_ip_allowed") {
                        this.setState({ isIPSecured: secured }, () => {
                            secured && this.ipAddressesNumber.setValue(this.state.selectedUser[key]);
                        });
                    }
                    if (key === "num_device_allowed") {
                        this.setState({ isDeviceSecured: secured }, () => {
                            secured && this.tokensNumber.setValue(this.state.selectedUser[key]);
                        });
                    }
                }
            }
            else {
                if (this.fields[key].getValue().value === true) {
                    this.fields[key].setValue(false);

                    if (key === "num_ip_allowed") {
                        this.setState({ isIPSecured: false });

                    }
                    if (key === "num_device_allowed") {
                        this.setState({ isDeviceSecured: false });
                    }
                }
                else this.fields[key].setValue("");
            }
        });
    }

    secureIPChanged = (_e) => {
        this.setState({ isIPSecured: this.fields.num_ip_allowed.getValue().value })
    }
    secureDeviceChanged = (_e) => {
        this.setState({ isDeviceSecured: this.fields.num_device_allowed.getValue().value })
    }

    saveIpAddressesNumberClicked = (e) => {
        let isValid = true;
        if (this.ipAddressesNumber.runValidation) {
            isValid = this.ipAddressesNumber.runValidation() ? isValid : false;
        }
        if (!isValid) {
            Notifications.show(LM.getString('completeFields'), 'danger')
            return
        }
        let newIpAddressesNumber = this.ipAddressesNumber.getValue().value;
        let data = {
            user_id: this.state.selectedUser.id,
            num_ip_allowed: newIpAddressesNumber,
        };
        postActivitySessionIP(data).then(
            (res) => {
                Notifications.show(LM.getString("ipAddressesNumber") + " " + LM.getString("updatedSuccessfully"), 'success');
                this.setState({
                    selectedUser: Object.assign(this.state.selectedUser, {num_ip_allowed: newIpAddressesNumber})
                })
            }
        ).catch(
            (err) => {
                Notifications.show(err.response.data.error, 'danger');
            }
        )
    };
    cancelSecureIpClicked = (e) => {
        Confirm.confirm(LM.getString('confirmUpdating')).then(() => {
            let data = {
                user_id: this.state.selectedUser.id,
            };
            postActivitySessionIPCancel(data).then(
                (res) => {
                    Notifications.show(LM.getString("ipAddressesNumber") + " " + LM.getString("updatedSuccessfully"), 'success');
                    this.setState({
                        selectedUser: Object.assign(this.state.selectedUser, {num_ip_allowed: null}), isIPSecured: false
                    })
                    this.fieldsLoad();
                }
            ).catch(
                (err) => {
                    Notifications.show(err.response.data.error, 'danger');
                }
            )
        }).catch((err) => { })
    };


    saveTokensNumberClicked = () => {
        let isValid = true;
        if (this.tokensNumber.runValidation) {
            isValid = this.tokensNumber.runValidation() ? isValid : false;
        }
        if (!isValid) {
            Notifications.show(LM.getString('completeFields'), 'danger')
            return
        }
        let newTokensNumber = this.tokensNumber.getValue().value;
        let data = {
            user_id: this.state.selectedUser.id,
            num_device_allowed: newTokensNumber,
        };
        postActivitySessionToken(data).then(
        (res) => {
                Notifications.show(LM.getString("tokensNumber") + " " + LM.getString("updatedSuccessfully"), 'success');
                this.setState({
                    selectedUser: Object.assign(this.state.selectedUser, {num_device_allowed: newTokensNumber})
                })
            }
        ).catch(
            (err) => {
                Notifications.show(err.response.data.error, 'danger');
            }
        )
    };


    cancelDeviceSecureClicked = (_e) => {
        Confirm.confirm(LM.getString('confirmUpdating')).then(() => {
            let data = {
                user_id: this.state.selectedUser.id,
            }
            postActivitySessionTokenCancel(data).then(
                (res) => {
                    Notifications.show(LM.getString("tokensNumber") + " " + LM.getString("updatedSuccessfully"), 'success');
                    this.setState({
                        selectedUser: Object.assign({}, this.state.selectedUser, {num_device_allowed: null}), isDeviceSecured: false
                    }, () =>{
                        this.fieldsLoad();
                    })
                }
            ).catch(
                (err) => {
                    Notifications.show(err.response.data.error, 'danger');
                }
            )
        }).catch((err) => { })
    }

    componentDidMount() {
        this.fieldsLoad();
    }


    render() {
        const secureIP = {
            lableText: LM.getString("secureIP"),
            id: "SecureIP",
        };
        const ipAddressesNumberProps = {
            title: LM.getString("ipAddressesNumber") + ":",
            id: "ContractInfo_IPAddressesNumber",
        };
        const deviceSecureProps = {
            lableText: LM.getString("deviceSecure"),
            id: "DeviceSecure",
        };
        const tokensNumberProps = {
            title: LM.getString("tokensNumber") + ":",
            id: "ContractInfo_TokensNumber",
        };

        return(
            <React.Fragment>
                <Row className="justify-content-start my-2">
                    <Col sm='auto'>
                        <InputTypeCheckBox
                            {...secureIP}
                            disabled={this.state.selectedUser.num_ip_allowed}
                            onChange={this.secureIPChanged}
                            ref={(componentObj) => { this.fields.num_ip_allowed = componentObj }}
                        />
                    </Col>
                </Row>
                {this.state.isIPSecured &&
                    <React.Fragment>
                        <Row className='myRow'>
                            <Col sm='3'>
                                <InputTypeNumber
                                    {...ipAddressesNumberProps}
                                    max={IpTokenMaxEnum.LIMIT_MAX}
                                    ref={(componentObj) => { this.ipAddressesNumber = componentObj }}
                                />
                            </Col>
                            <Button
                                className='contractDetailsButton contractDetailsButtonSave'
                                onClick={this.saveIpAddressesNumberClicked}
                                id='contractDetailsSaveIpAddressesNumberButton'
                            >
                                {LM.getString("save")}
                            </Button>
                            <Button
                                className="hashavshevetButton hashavshevetButtonCancel px-4 mx-3"
                                onClick={this.cancelSecureIpClicked}
                                id='contractDetailsCancelSecureIpButton'
                            >
                                {LM.getString("cancelSecureIP")}
                            </Button>
                        </Row>
                        {LM.getString("ipAddresses") + ":"}
                        <UserSecurityDetailsTable
                            userId={this.state.selectedUser.id}
                            isIPSecured={this.state.isIPSecured}
                            ref={(componentObj) => { this.ipAddressesTable = componentObj }}/>
                    </React.Fragment>
                }

                <Row className="justify-content-start my-2">
                    <Col sm='auto'>
                        <InputTypeCheckBox
                            {...deviceSecureProps}
                            disabled={this.state.selectedUser.num_device_allowed}
                            onChange={this.secureDeviceChanged}
                            ref={(componentObj) => { this.fields.num_device_allowed = componentObj }}
                        />
                    </Col>
                </Row>
                {this.state.isDeviceSecured &&
                <React.Fragment>
                    <Row className='myRow'>
                        <Col sm='3'>
                            <InputTypeNumber
                                {...tokensNumberProps}
                                max={IpTokenMaxEnum.LIMIT_MAX}
                                ref={(componentObj) => { this.tokensNumber = componentObj }}
                            />
                        </Col>
                        <Button
                            className='contractDetailsButton contractDetailsButtonSave'
                            onClick={this.saveTokensNumberClicked}
                            id='contractDetailsSaveTokensNumberButton'
                        >
                            {LM.getString("save")}
                        </Button>
                        <Button
                            className="hashavshevetButton hashavshevetButtonCancel px-4 mx-3"
                            onClick={this.cancelDeviceSecureClicked}
                            id='contractDetailsCancelDeviceSecureButton'
                        >
                            {LM.getString("cancelDeviceSecure")}
                        </Button>
                    </Row>
                    {LM.getString("tokens") + ":"}
                    <UserSecurityDetailsTable
                        userId={this.state.selectedUser.id}
                        isDeviceSecured={this.state.isDeviceSecured}
                        ref={(componentObj) => { this.tokensTable = componentObj }}
                    />
                </React.Fragment>
                }
            </React.Fragment>
        );
    }
}
