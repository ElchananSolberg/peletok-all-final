import React, {Component} from 'react';
import {Container, Row, Col, Button, Collapse} from 'reactstrap'
import {EditUserLine} from './editUserLine';
import {AddNewUser} from './addNewUser';
import {LanguageManager as LM} from '../LanguageManager/Language';
import './selectUser.css'
import {Confirm} from '../Confirm/Confirm';


/**
 * TODO - all strings in the state shuld became from astring file;
 * TODO -language settings
 * TODO -It is clear that all user entries are currently fake and are not connected to the server
 * Component that returns components that return a row for all users
 */
export class UsersScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isAdd: false,
            render: true
        }

        this.addUserClick = this.addUserClick.bind(this)
        this.addUserButtonClick = this.addUserButtonClick.bind(this)
    }

    /**
     * Open a component to insert a new user
     */
    addUserClick = () => {
        this.setState({isAdd: !this.state.isAdd})
    }

    addUserButtonClick = () => {
        if (this.state.isAdd) {
            Confirm.confirm(LM.getString("areYouSureLoseData")).then(() => {
                this.setState({isAdd: false}, () => {
                    this.setState({render: false}, () => {
                        this.setState({render: true})
                    })
                })
            })
        } else {
            this.setState({isAdd: !this.state.isAdd})
        }
    }

    /**
     * creates a change handling function to be sent to input components that changes
     * 'key' value in this component's state
     * @param {*} key the key to change(/create) in the state upon calling
     */
    onChangeWraper(key, validation) {
        var f = (e) => {
            if (e.target && validation(e.target.value)) {
                this.setState({[key]: e.target.value});
            }
        }
        return f;
    }

    static isNumber(value) {
        return [...value].every(c => '0123456789'.includes(c));
    }

    static always(value) {
        return true;
    }

    static maxLength(length) {
        return (value) => {
            return value.length <= length;
        }
    }

    static numberWithMaxLength(maxLength) {
        return (value) => UsersScreen.isNumber(value) & UsersScreen.maxLength(maxLength)(value);
    }


    render() {
        return (
            <div
                className="main_border_padding color-gray"> {/** TODO fix container width to be 90% (need to ignore max-width property coming from reactstrat's Container) */}
                {this.props.ourUsers.length > 0 &&
                <React.Fragment>
                    <Row className={'mb-4'}>
                        <Col xs="6" className="headerRowForEditUser1"> {LM.getString("selectUsertitleUsersList")}</Col>
                        <Col xs="6" className="headerRowForEditUser2"> {LM.getString("selectUserTotalSales30")}</Col>
                    </Row>
                    <Row className={'mx-0 white-space'} style={{color: 'var(--tableHeaderText)'}}>
                        <Col xs="1"><h5 className={'font-weight-bold fs-17'}>{LM.getString("selectUserNum")}</h5></Col>
                        <Col md="2" sm="3" xs="4"><h5
                            className={'font-weight-bold fs-17'}>{LM.getString("username")}</h5></Col>
                        <Col sm="2" className={'d-none d-md-block'}><h5
                            className={'font-weight-bold fs-17'}>{LM.getString("selectUserTelNum")}</h5></Col>
                        <Col md="2" sm="3" xs="4"><h5
                            className={'font-weight-bold fs-17'}>{LM.getString("selectUsertotalSales")}</h5></Col>
                        <Col xs="2" className={'d-none d-lg-block'}><h5
                            className={'font-weight-bold fs-17'}>{LM.getString("lastLogin")}</h5></Col>
                    </Row>
                    {this.props.ourUsers.map((cur, index) => {
                            return <EditUserLine
                                user={cur}
                                {...cur}
                                key={index}
                                index={index + 1}
                                stateChanger={this.onChangeWraper}
                                Managment={this.props.Managment}
                                updateUsers={this.props.updateUsers}
                                currentDistributor={this.props.currentDistributor}/>
                        }
                    )
                    }
                </React.Fragment>
                }
                <Button
                    className={'font-weight-bold px-4 fs-17 mt-2 border-radius-3 ' + (!this.state.isAdd ? 'regularButton' : '')}
                    size="lg" onClick={this.addUserButtonClick} id="mainselectUseraddUser"
                >
                    {(!this.state.isAdd ? LM.getString("selectUseraddUser") : LM.getString("cancel"))}
                </Button>
                <Row>
                    <Col>
                        <Collapse isOpen={this.state.isAdd}>
                            {this.state.render ? <AddNewUser
                                    updateUsers={this.props.updateUsers}
                                    addUserClick={this.addUserClick}
                                    stateChanger={this.onChangeWraper}
                                    Managment={this.props.Managment}
                                    currentDistributor={this.props.currentDistributor}
                                />
                                : <></>}
                        </Collapse>
                    </Col>
                </Row>
            </div>
        )
    }
}