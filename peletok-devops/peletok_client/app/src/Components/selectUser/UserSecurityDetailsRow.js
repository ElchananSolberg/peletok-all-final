import React, { Component } from 'react';

import { Button, Row, Col } from 'reactstrap';

import { LanguageManager as LM } from '../LanguageManager/Language'
import { Notifications } from '../Notifications/Notifications';
import { Confirm } from '../Confirm/Confirm';

import { deleteIPAddress, deleteToken } from '../DataProvider/DataProvider'

import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
/** UserSecurityDetailsRow component is a part of a Contract Info page.
 * It renders one row of a infos table using a data from props (object sended by a father component - UserSecurityDetailsTable).
*/
export class UserSecurityDetailsRow extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isExist: true
        };
    }

    /** Button "Delete" onClick handler.*/
    deleteClick = (e) => {
        Confirm.confirm(LM.getString('confirmDeleting')).then(() => {
            let ipOrTokenId = this.props.id;
            if (ipOrTokenId === undefined || ipOrTokenId === null) {
                Notifications.show(LM.getString("idError"), 'warning');

                return;
            }
            else {
                let ip = this.props.ip_address;
                let token = this.props.token;
                
                if (ip) {
                    deleteIPAddress(ipOrTokenId)
                        .then(
                            (res) => {
                                this.setState({
                                    isExist: false
                                })
                                Notifications.show(LM.getString("ipAddress") + LM.getString("successfullyDeleted"), 'success');
                            }
                        )
                        .catch(
                            (err) => {
                                Notifications.show(err.response.data.error, 'danger');
                            }
                        );
                }
                if (token) {
                    deleteToken(ipOrTokenId)
                        .then(
                            (res) => {
                                this.setState({
                                    isExist: false
                                })
                                Notifications.show(LM.getString("token") + " " + LM.getString("successfullyDeleted"), 'success');
                            }
                        )
                        .catch(
                            (err) => {
                                Notifications.show(err.response.data.error, 'danger');
                            }
                        );
                }
            }
        }).catch((err) => { })
    }

    render() {
        let deleteBtn = LM.getString("delete1");

        let ip = this.props.ip_address;
        let token = this.props.token;

        let status = this.props.status ? LM.getString("logged") : LM.getString("loggedOff");
        let userLogged = this.props.userLogged;

        let id = this.props.id;

        return (
            this.state.isExist ? <div className="tagsManagementTableRow align-items-center d-flex flex-wrap">
                {ip &&
                    <Col xs='3'>
                        <>{ip}</>
                    </Col>
                }
                {token &&
                    <Col xs='3'>
                        <>{token}</>
                    </Col>
                }
                {/* <Col xs='2'>
                    <>{status}</>
                </Col>
                <Col xs='2'>
                    <>{userLogged}</>
                </Col> */}

                <Col xs='3' className='d-md-flex d-none'>
                    <Button
                        className="tagsManagementTableBtn tagsManagementTableBtnDelete"
                        id="contractInfoRowBtnDelete"
                        onClick={this.deleteClick}
                    >
                        {deleteBtn}
                    </Button>
                </Col>
                <Col xs='3' className='d-md-none d-flex'>
                    <a
                        className="pointer"
                        id="contractInfoRowBtnDelete"
                        onClick={this.deleteClick}
                    >
                        <FontAwesomeIcon icon={faTrashAlt} size="1x" />
                    </a>
                </Col>
            </div> : <></>
        )
    }
}
