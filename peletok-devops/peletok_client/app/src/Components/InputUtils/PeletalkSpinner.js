import React, { Component } from 'react';
import { Spinner } from 'reactstrap';

import './InputUtils.css';

import { LanguageManager as LM } from '../LanguageManager/Language'

class PeletalkSpinner extends Component {
    render() {
        return (
            <>
                <Spinner type="grow" style={{ height: '100px', width: '100px', background: 'var(--sideBarBack)' }} />
                <div style={{ width: this.props.context === "loader"?'100%':'100px', textAlign: 'center' }}> {LM.getString('pleaseWait')} </div>
            </>
        )
    }
}
export default PeletalkSpinner;

