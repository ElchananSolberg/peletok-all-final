import React, { Component } from 'react';
import './InputUtils.css';
import { Row, Col, CustomInput } from 'reactstrap';
import { Tooltip } from './Tooltip';
import { LanguageManager as LM } from '../LanguageManager/Language';
import Asterisk from '../Ui/Asterisk';


/**
 * InputTypeRadio component renders a row OR a column of radio buttons inside the form with theire text labels.
 * it can render the title above the row of radio buttons.
 * it can render the tooltip to the side of the title.
 * It validates the value was entered (if this.props.required is true).
 * It validates the value is valid to the function sent in  this.props.validationFunction;
 * if there are any validation issues, the div that contains rendered options will change a border color to red;
 * if there are any validation issues bellow the div will be the error message 
 * It can add outer onChange function to checkbox onChange.
 * It can change a view to disabled view due to this.props.disabled.
 */
class InputTypeRadio extends Component {
    constructor(props) {
        super(props);

        this.state = {
            value: this.props.default,
            defaultID: null,
            defaultSet: false,
            validationFuncs: [
                {
                    /**  Validates the value is valid to the function sent in  this.props.validationFunction */
                    f: this.props.validationFunction || (() => (true)),
                    msg: this.props.notValidMessage,
                },
                {
                    f: this.isRequiredValidation,
                    msg: LM.getString("isRequiredValidationFunctionMsg"),
                }
            ],
            errorMessage: '',
        }
    }

    /** can add outer onChange function to checkbox onChange. */
    inputTypeRadioChange = (e) => {
        e.persist();
        this.setState({ value: e.target.value },
            () => {
                if (this.props.onChange) {
                    this.props.onChange(e);
                }
            }
        );
    }

    /** summarizing validation function
    * is running 2 times: when input is loosing focus and when getValue function is running (by programmer-user) 
    */
    runValidation = () => {
        let summarizingValidationResult = (!this.props.required && !this.state.value) || this.state.validationFuncs.every((validation) => validation.f(this.state.value));
        if (!summarizingValidationResult) {
            let messages = [];
            this.state.validationFuncs.forEach((validation) => {
                if (!validation.f(this.state.value)) {
                    messages.push(validation.msg);
                }
            })
            this.setState(({ errorMessage: messages.join(`\n`) }));
        } else {
            this.setState(({ errorMessage: '' }));
        }
        return summarizingValidationResult;
    }

    getValue = () => {
        let summarizingValidationResult = this.runValidation();
        return { valid: summarizingValidationResult, value: this.state.value };
    }

    setValue = (newValue) => {
        if (this.props.options.includes(newValue)) {
            this.setState({ value: newValue });
            this.setState({ defaultID: 'InputUtilsRadio' + this.props.id.replace(/\s+/g, '') + this.props.options.indexOf(newValue) }, () => {
                let elem = document.getElementById(this.state.defaultID);
                if (elem) {
                    elem.setAttribute('checked', true)
                }
            });
            return true;
        }
        return false;
    }

    /** If this.props.required is true validates the value was entered. */
    isRequiredValidation = (value) => {
        if (!value && this.props.required) {
            return false;
        }
        return true;
    }

    componentDidUpdate() {
        if (!this.state.defaultSet) {
            let elem = document.getElementById(this.state.defaultID);
            if (elem) {
                elem.setAttribute('checked', true)
            }
            this.setState({ defaultSet: true });
        }
    }

    render() {
        let disabled = this.props.disabled;
        let id = this.props.id.replace(/\s+/g, '');
        let title = this.props.title;
        let tooltipText = this.props.tooltip;
        let myDefault = this.props.default;

        var index = 0;
        let key = 0;
        let renderedOptions = this.props.options.map((current) => {
            let curID = `InputUtilsRadio${id}${index++}`;
           
            if (current === myDefault && this.state.defaultID !== curID) {
                this.setState({ defaultID: curID });
            }
            return (
                <CustomInput type="radio" key={key++} name="InputUtilsRadio"
                    id={curID} label={current} value={current}
                    disabled={disabled}
                    onChange={this.inputTypeRadioChange}
                    className={"InputUtilsRadio " + (LM.getDirection() === "rtl" ? "radio-rtl" : "")}
                />
            )
        });


        return (
            <>
                <Row>
                    <Col className={"d-flex justify-content-between line-height-1 " + (title ? 'py-1' : '')}>
                        {title ?
                            <div className="fs-17 white-space">{title}
                                <Asterisk show={this.props.required} />
                            </div>
                            : <><Asterisk show={this.props.required} /></>
                        }
                        {tooltipText ?
                            <Tooltip tooltipText={tooltipText} id={id} />
                            : <></>
                        }
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <form onBlur={this.runValidation}>
                            <div className={"my-2 d-flex " + (this.props.vertical ? "flex-column " : "flex-wrap flex-row ") + (this.state.errorMessage ? "invalidDiv" : "")}>
                                {renderedOptions}
                            </div>
                        </form>
                        <div className='ws-pre errorMessageDiv'>{this.state.errorMessage}</div>
                    </Col>
                </Row>

            </>
        );
    }
}

export default InputTypeRadio;
