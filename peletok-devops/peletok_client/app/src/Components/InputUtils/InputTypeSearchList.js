import React, { Component } from 'react';
import './InputUtils.css';
import { Row, Col, Card, CardText } from 'reactstrap';
import { Tooltip } from './Tooltip';
import { LanguageManager as LM } from '../LanguageManager/Language';
import Select, { components } from 'react-select';
import ReactSVG from 'react-svg';
import iconSearchSVG from './ic-search.svg';
import Asterisk from '../Ui/Asterisk';

/**
 * InputTypeSearchList component lets to choose by selection or by typing
 * it looks like mix of <input/> and  <select/>. 
 * If you will type in it, it will filter the list of the options and you will see ONLY the options that have the same letters you typed.
 * If you will select an option you will see ONLY this option. 
 * You will need to DELETE all the letters to see the whole list of options.
 * 
 * it can render the TITLE above the input.
 * it can render the TOOLTIP to the side of the title.
 * It can change a view and functionality to MULTIPLE OPTIONS selection due to this.props.multi.
 * It validates the value was entered (if this.props.required is true).
 * It validates the value is valid to the function sent in  this.props.validationFunction;
 * if there are any validation issues the input will get a prop "invalid" and will change a border color to red;
 * if there are any validation issues bellow the input will be the error message 
 * It can change a view to disabled view due to this.props.disabled.
 */
class InputTypeSearchList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      multiValue: (Array.isArray(this.props.default) ? this.props.default : [this.props.default]).filter(i=>i).map(n=>{return{ label: n, value: n }}),
      value: this.props.default,
      validationFuncs: [
        {
          /**  Validates the value is valid to the function sent in  this.props.validationFunction */
          f: this.props.validationFunction || (() => (true)),
          msg: this.props.notValidMessage
        },
        {
          f: this.isRequiredValidation,
          msg: LM.getString("isRequiredValidationFunctionMsg"),
        }
      ],
      errorMessage: '',
    }
  }

  /** summarizing validation function
  * is running 2 times: when input is loosing focus and when getValue function is running (by programmer-user) 
  * */
  runValidation = () => {
    let summarizingValidationResult = (!this.props.required && !this.state.value) || this.state.validationFuncs.every((validation) => validation.f(this.state.value));
    if (!summarizingValidationResult) {
      let messages = [];
      this.state.validationFuncs.forEach((validation) => {
        if (!validation.f(this.state.value)) {
          messages.push(validation.msg);
        }
      })
      this.setState(({ errorMessage: messages.join(`\n`) }));
    } else {
      this.setState(({ errorMessage: '' }));
    }
    return summarizingValidationResult;
  }

  getValue = () => {
    let summarizingValidationResult = this.runValidation();
    return { valid: summarizingValidationResult, value: this.state.value };
  }

  setValue = (newValue) => {
    if (this.props.multi) {
      let multiValues = newValue.map(current => {
        return { label: current, value: current }
      })
      this.setState({ multiValue: multiValues });
    }

    this.setState({ value: newValue });
  }

  /** If this.props.required is true validates the value was entered. */
  isRequiredValidation = (value) => {
    if ((!value || !value[0]) && this.props.required) {
      return false;
    }
    return true;
  }

  handleMultiChange = (values) => {
    let onlyValues = values.map(current => { return current.value })
    this.setState({ value: onlyValues });

    this.setState({ multiValue: values },
      () => {
        if (this.props.onChange) {
          this.props.onChange();
        }
      }
    );

  }

  render() {
    let disabled = this.props.disabled;
    let placeholder = this.props.placeholder;
    let id = this.props.id.replace(/\s+/g, '');
    let title = this.props.title;
    let tooltipText = this.props.tooltip;

    let reactSelectOptions = [];
    if(this.props.addDefaultToOptions){
      reactSelectOptions.push({value: this.props.default, label: this.props.default});
    }
    this.props.options.map((current) => { reactSelectOptions.push({ value: current, label: current }) });

    let type2 = this.props.type2;

    const DropdownIndicator = (
      props
    ) => {
      return (
        <components.DropdownIndicator {...props}>
          <ReactSVG src={iconSearchSVG} />
        </components.DropdownIndicator>
      );
    };
    const IndicatorSeparator = ({ innerProps }) => {
      return <div {...innerProps} />;
    };

    return (type2 ?
      <>
        <Row >
          <Col >
            <Card className="type2Card">
              <CardText className="type2CardText"  >
                {title}
                <Asterisk show={this.props.required} />
              </CardText>
              <Select options={reactSelectOptions}
                className={"type2Input " + (this.state.errorMessage ? "invalidDiv" : "")}
                id={`InputTypeSearchList${id}`} placeholder={placeholder}
                isDisabled={disabled}
                onChange={
                  ({ value }) => {
                    this.setState({ value: value },
                      () => {
                        if (this.props.onChange) {
                          this.props.onChange(value);
                        }
                      }
                    );
                  }}
                onBlur={this.runValidation}
                value={{ label: this.state.value, value: this.state.value }}
                components={{ DropdownIndicator, IndicatorSeparator }}
              />
              {/** if there are any validation issues bellow the input will be the error message */}
              <div className='ws-pre errorMessageDiv'>{this.state.errorMessage}</div>
            </Card>
          </Col>
        </Row>
      </>
      :
      <>
        <Row>
          <Col className={"d-flex justify-content-between line-height-1 " + (title ? 'py-1' : '')}>
            {title ?
              <div className="fs-17 white-space">{title}
                <Asterisk show={this.props.required} />
              </div>
              : <><Asterisk show={this.props.required} /></>
            }
            {tooltipText ?
              <Tooltip tooltipText={tooltipText} id={id} />
              : <></>
            }
          </Col>
        </Row>
        <Row className='mt-auto'>
          <Col className={'z-index-100'}>
            {/** if there are any validation issues the input will get a prop "invalid" and will change a border color to red */}
            <Select options={reactSelectOptions}
              className={"height_Select " + (this.state.errorMessage ? "invalidDiv" : "")}
              id={`InputTypeSearchList${id}`} placeholder={placeholder}
              isDisabled={disabled}

              onChange={this.props.multi ?
                this.handleMultiChange
                :
                ({ value }) => {
                  this.setState({ value: value },
                    () => {
                      if (this.props.onChange) {
                        this.props.onChange(value);
                      }
                    }
                  );
                }
              }

              onBlur={this.runValidation}
              value={this.props.multi ? this.state.multiValue : { label: this.state.value, value: this.state.value }}
              components={{ DropdownIndicator, IndicatorSeparator }}

              isMulti={this.props.multi}
            />
            {/** if there are any validation issues bellow the input will be the error message */}
            <div className='ws-pre errorMessageDiv'>{this.state.errorMessage}</div>
          </Col>
        </Row>
      </>
    );
  }
}

export default InputTypeSearchList;