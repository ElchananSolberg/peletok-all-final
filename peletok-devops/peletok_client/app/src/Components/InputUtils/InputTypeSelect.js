import React, { Component } from 'react';
import './InputUtils.css';
import { FormFeedback, Input, Row, Col } from 'reactstrap';
import { Tooltip } from './Tooltip';
import { LanguageManager as LM } from '../LanguageManager/Language'
import Asterisk from '../Ui/Asterisk';
/**
 * InputTypeSelect  component lets to select an option
 * it can render the title above the input (if this.props.title is not an empty string).
 * it can render the tooltip to the side of the title (if this.props.tooltip is not an empty string).
 * It validates the value was entered (if this.props.required is true).
 * It validates the value is valid to the function sent in  this.props.validationFunction;
 * if there are any validation issues the input will get a prop "invalid" and will change a border color to red;
 * if there are any validation issues bellow the input will be the error message 
 * It can add outer onChange function to checkbox onChange.
 * It can change a view to disabled view due to this.props.disabled.
 */
class InputTypeSelect extends Component {
    constructor(props) {
        super(props);

        this.state = {
            value: this.props.default,
            validationFuncs: [
                {
                    /**  Validates the value is valid to the function sent in  this.props.validationFunction */
                    f: this.props.validationFunction || (() => (true)),
                    msg: this.props.notValidMessage,
                },
                {
                    f: this.isRequiredValidation,
                    msg: LM.getString("isRequiredValidationFunctionMsg"),
                }
            ],
            errorMessage: '',
        }
    }

    /** can add outer onChange function to onChange. */
    inputTypeSelectChange = (e) => {
        e.persist();
        this.setState({ value: e.target.value },
            () => {
                if (this.props.onChange) {
                    this.props.onChange(e);
                }
            }
        );
    }

    /** summarizing validation function
    * is running 2 times: when input is loosing focus and when getValue function is running (by programmer-user) */
    runValidation = () => {
        let summarizingValidationResult = (!this.props.required && !this.state.value) || this.state.validationFuncs.every((validation) => validation.f(this.state.value));
        if (!summarizingValidationResult) {
            let messages = [];
            this.state.validationFuncs.forEach((validation) => {
                if (!validation.f(this.state.value)) {
                    messages.push(validation.msg);
                }
            })
            this.setState(({ errorMessage: messages.join(`\n`) }));
        } else {
            this.setState(({ errorMessage: '' }));
        }
        return summarizingValidationResult;
    }

    getValue = () => {
        let summarizingValidationResult = this.runValidation();
        return { valid: summarizingValidationResult, value: this.state.value };
    }

    /** If this.props.required is true validates the value was entered. */
    isRequiredValidation = (value) => {
        if (!value && this.props.required) {
            return false;
        }
        return true;
    }

    setValue(newValue) {
        if (this.props.options.includes(newValue)) {
            this.setState({ value: newValue });
            return true;
        }
        return false;
    }

    render() {
        let disabled = this.props.disabled;
        let id = this.props.id.replace(/\s+/g, '');
        let title = this.props.title;
        let tooltipText = this.props.tooltip;
        let myDefault = this.props.default;

        let key = 0;
        let options = this.props.options.map((current) => {
            return <option key={key++} > {current} </option>
        });

        return (
            <>
                <Row>
                    {title ?
                        <Col className="textStart" xs="10">
                            <span className="textStart subHeader" style={{ overflow: "visible" }}>
                                {title}
                                <Asterisk show={this.props.required} />
                            </span>
                        </Col>
                        : <> <Asterisk show={this.props.required} /></>
                    }

                    {tooltipText ?
                        <Col xs="2">
                            <Tooltip tooltipText={tooltipText} id={id} />
                        </Col>
                        : <> </>
                    }
                </Row>
                <Row>
                    <Col>
                        {/** if there are any validation issues the input will get a prop "invalid" and will change a border color to red */}
                        <Input
                            value={this.state.value}
                            type="select" id={`InputUtilsSelect${id}`}
                            disabled={disabled} invalid={this.state.errorMessage.length !== 0}
                            onChange={this.inputTypeSelectChange} onBlur={this.runValidation}
                            defaultValue={myDefault}
                            className={(LM.getDirection() === "rtl" ? "rtl" : "")}
                        >
                            {options}
                        </Input>
                        {/** if there are any validation issues bellow the input will be the error message */}
                        <FormFeedback className='ws-pre'>{this.state.errorMessage}</FormFeedback>
                    </Col>
                </Row>

            </>
        );
    }
}

export default InputTypeSelect;