import React, { Component } from 'react';

import { Container, Row, Col, Button } from 'reactstrap';

import './InputUtils.css';

import { LanguageManager as LM } from '../LanguageManager/Language'

import InputTypeSelect from './InputTypeSelect';
import InputTypeRadio from './InputTypeRadio';
import InputTypeEmail from './InputTypeEmail';
import InputTypeText from './InputTypeText';
import InputTypeCheckBox from './InputTypeCheckBox';
import InputTypeNumber from './InputTypeNumber';
import InputTypeSearchList from './InputTypeSearchList';
import Tag from './Tag';
import ImageUpload from './ImageUpload';
/** an example of uploaded image */
import imgUploaded from './AD.gif';
import InputTypeFile from './InputTypeFile';

import Notice from './Notice';
import Slider from './Slider';

import InputTypeDate from './InputTypeDate';
import InputTypeTime from './InputTypeTime';

import InputTypePhone from './InputTypePhone';

import PeletalkPagination from './PeletalkPagination';

import PeletalkSpinner from './PeletalkSpinner';
import { Loader } from '../Loader/Loader'

class TestScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            inputTypeSelectProps: {
                disabled: false,
                notValidMessage: 'Value ינואר is FORBIDEN ',
                tooltip: "",
                options: LM.getString("monthArray"),
                default: LM.getString("monthArray")[1],
                title: "",
                id: "ChooseMonthId",
                required: true,
            },

            inputTypeRadioProps: {
                disabled: false,
                notValidMessage: 'Value ינואר is FORBIDEN ',
                tooltip: "see this Tooltip",
                options: LM.getString("monthArray"),
                //    default: LM.getString("monthArray")[1],
                title: "Title test:",
                id: "TestRadioId-",
                required: true,
            },
            inputTypeRadioProps1: {
                disabled: true,
                notValidMessage: 'Value ינואר is FORBIDEN ',
                tooltip: "see this Tooltip",
                options: LM.getString("monthArray"),
                default: LM.getString("monthArray")[1],
                title: "Title test:",
                id: "TestRadioId1-",
                required: true,
            },
            inputTypeEmailProps: {
                disabled: false,
                notValidMessage: 'Value 1111 is FORBIDEN ',
                placeholder: 'Example: mymail@google.com',
                tooltip: "",
                title: LM.getString("enterMail") + ":",
                id: "InputMailId",
                required: true,
            },
            inputTypeTextProps: {
                disabled: false,
                password: true,
                notValidMessage: 'Value 1111 is FORBIDEN ',
                maxLength: 5,
                minLength: 1,
                placeholder: 'Type any TEXT',
                tooltip: "show tooltip",
                title: "Test input type text: ",
                id: "InputTextId",
                required: true,
            },
            inputTypeTextType2Props: {
                /** a prop for switching an input to the second type of a view */
                type2: true,
                disabled: false,
                password: false,
                notValidMessage: 'Value 1111 is FORBIDEN ',
                maxLength: 5,
                minLength: 1,
                placeholder: 'Type any TEXT',
                tooltip: "show tooltip",
                title: "Text type2 :",
                id: "TextType2",
                required: true,
            },
            inputTypeTextMultiProps: {
                /** a prop for switching an input to a multi string view */
                multi : true,
                disabled: false,
                notValidMessage: 'Value 1111 is FORBIDEN ',
                minLength: 1,
                placeholder: 'Type any TEXT',
                tooltip: "show tooltip",
                title: "Text MULTI :",
                id: "TextMulti",
                required: true,
            },

            inputTypeCheckBoxProps: {
                disabled: true,
                notValidMessage: 'Value checked (true) is FORBIDEN ',
                checked: true,
                lableText: 'Checkbox text',
                tooltip: "show tooltip",
                title: "Test input type check box :",
                id: "CheckBoxId",
            },
            inputTypeCheckBoxProps1: {
                disabled: false,
                notValidMessage: 'Value checked (true) is FORBIDEN ',
                checked: false,
                lableText: 'Checkbox text 111',
                tooltip: "ttt",
                title: "ttt",
                id: "CheckBoxId1",
            },
            inputTypeNumberProps: {
                money: true,
                radio: false,
                disabled: false,
                notValidMessage: 'Value 1111 is FORBIDEN ',
                minLength: 2,
                maxLength: 4,
                placeholder: 'Type only NUMBERS',
                tooltip: "show tooltip",
                title: "Test input type number:",
                id: "InputNumberId",
                required: true,
            },
            inputTypeNumberRadioProps: {
                money: true,
                radio: true,
                disabled: false,
                notValidMessage: 'Value 1111 is FORBIDEN ',
                minLength: 2,
                maxLength: 4,
                placeholder: 'Type only NUMBERS',
                tooltip: "show tooltip",
                title: "Test input type number:",
                id: "NumberWithRadio",
                required: true,
            },
            inputTypeNumberType2Props: {
                /** a prop for switching an input on second type of a view */
                type2: true,
                disabled: false,
                notValidMessage: 'Value 1111 is FORBIDEN ',
                minLength: 2,
                maxLength: 4,
                placeholder: 'Type only NUMBERS',
                tooltip: "show tooltip",
                title: "Number Type2:",
                id: "InputNumberType2",
                required: true,
            },
            inputTypeSearchListProps: {
                disabled: false,
                notValidMessage: 'Value ' + LM.getString("monthArray")[1] + ' is FORBIDEN ',
                placeholder: 'TYPE or SELECT',
                tooltip: "show tooltip",
                options: LM.getString("monthArray"),
                // default: LM.getString("monthArray")[1],
                title: "Test input type search list:",
                id: "SearchListId",
                required: true,
            },
            inputTypeSearchListMultiProps: {
                multi: true,

                disabled: false,
                notValidMessage: 'Value ' + LM.getString("monthArray")[1] + ' is FORBIDEN ',
                placeholder: 'TYPE or SELECT',
                tooltip: "show tooltip",
                options: LM.getString("monthArray"),
                default: LM.getString("monthArray")[1],
                title: "Test input type search list MULTI:",
                id: "MultiSearchListId",
                required: true,
            },
            inputTypeSearchListType2Props: {
                /** a prop for switching an input on second type of a view */
                type2: true,
                disabled: false,
                notValidMessage: 'Value ' + LM.getString("monthArray")[1] + ' is FORBIDEN ',
                placeholder: 'TYPE or SELECT',
                tooltip: "show tooltip",
                options: LM.getString("monthArray"),
                default: LM.getString("monthArray")[1],
                title: "Input type search list Type2:",
                id: "SearchListType2",
                required: true,
            },
            tagProps: {
                disabled: true,
                notValidMessage: 'Value checked (true) is FORBIDEN ',
                buttonText: 'Favorites',
                tooltip: "show tooltip",
                default: false,
                title: "Test input type search list:",
                id: "TagId",
            },
            tagProps1: {
                disabled: false,
                notValidMessage: 'Value checked (true) is FORBIDEN ',
                buttonText: 'Suggested',
                tooltip: "",
                default: true,
                title: "",
                id: "Tag1Id",
            },
            tagProps2: {
                disabled: false,
                notValidMessage: 'Value checked (true) is FORBIDEN ',
                buttonText: 'to Internet',
                tooltip: "",
                default: true,
                title: "",
                id: "Tag2Id",
            },
            imageUploadProps: {
                default: imgUploaded,
                tooltip: "see this tooltip",
                title: "Test Image Upload",
                id: "ImageUploadId",
                required: true,
            },
            imageUploadProps1: {
                default: '',
                tooltip: "see this tooltip",
                title: "Test Image Upload",
                id: "ImageUploadId1",
                required: true,
            },
            inputTypeFileProps: {
                default: '',
                tooltip: "see this tooltip",
                title: "Test input type FILE",
                id: "InputTypeFile",
                required: true,
                disabled: false,
            },
            noticeProps: {
                noticeText: "Hello! I am a Notice component from InputUtils! Hello! I am a Notice component from InputUtils! Hello! I am a Notice component from InputUtils! Hello! I am a Notice component from InputUtils! Hello! I am a Notice component from InputUtils! Hello! I am a Notice component from InputUtils! Hello! I am a Notice component from InputUtils!",
                tooltip: "show tooltip",
                title: "Test input type number:",
                tooltipId: "TestNotice",
            },
            sliderProps: {
                disabled: false,
                notValidMessage: 'Value 20 is FORBIDEN ',
                /** step , min, max , defaultValue MUST BE A TEXT, NOT A NUMBER */
                step: '10',
                min: '10',
                max: '100',
                defaultValue: '20',
                tooltip: "show tooltip",
                title: "Test slider:",
                id: "SliderId",
            },

            inputTypeDateProps: {
                /** format: YY-MM-DD */
                min: "2019-04-01",
                /** format: YY-MM-DD */
                max: "2019-05-31",
                /** format: YY-MM-DD */
                default: "2019-01-31",
                disabled: false,
                notValidMessage: 'Value 2019-05-01 is FORBIDEN ',
                placeholder: 'Select a Date',
                tooltip: "show tooltip",
                title: "Test input type Date :",
                id: "InputDateId",
                required: true,
            },
            inputTypeDateType2Props: {
                /** a prop for switching an input on second type of a view */
                type2: true,
                /** format: YY-MM-DD */
                min: "2019-04-01",
                /** format: YY-MM-DD */
                max: "2019-05-31",
                /** format: YY-MM-DD */
                default: "2019-01-31",
                disabled: false,
                notValidMessage: 'Value 2019-05-01 is FORBIDEN ',
                placeholder: 'Select a Date',
                tooltip: "show tooltip",
                title: "Date Type2:",
                id: "DateType2",
                required: true,
            },

            inputTypeTimeProps: {
                min: "08:00",
                max: "20:00",
                default: "13:30",
                disabled: false,
                notValidMessage: 'Value 01:00 is FORBIDEN ',
                placeholder: 'Select a Time',
                tooltip: "show tooltip",
                title: "Test input type Time :",
                id: "InputTimeId",
                required: true,
            },
            inputTypeTimeType2Props: {
                /** a prop for switching an input on second type of a view */
                type2: true,
                min: "08:00",
                max: "20:00",
                default: "13:30",
                disabled: false,
                notValidMessage: 'Value 01:00 is FORBIDEN ',
                placeholder: 'Select a Time',
                tooltip: "show tooltip",
                title: "Time Type2:",
                id: "TimeType2",
                required: true,
            },

            inputTypePhoneProps: {
                /** A final id of a prefix input (first one with 3 numbers) will look so:
                 * "InputTypeNumberInputTypePhonePrefix"+id
                 * A final id of a number input (second one with 7 numbers) will look so:
                 * "InputTypeNumberInputTypePhoneNumber"+id
                 */
                title: "Test input type PHONE:",
                tooltip: "show tooltip",
                id: "inputTypePhoneTest",
                required: true,
                /** If you want both inputs to be disabled, send a prop disabled:true  */
                disabled: false,
                /** If you want only one of the inputs to be disabled, send a prop prefixDisabled:true  or numberDisabled:true  */
                prefixDisabled: false,
                numberDisabled: false,
                prefixNotValidMessage: 'Value 072 is FORBIDEN',
                numberNotValidMessage: 'Value 1234567 is FORBIDEN',
            },
            inputTypePhoneTwoDigitsProps: {
                /** A final id of a prefix input (first one with 3 numbers) will look so:
                 * "InputTypeNumberInputTypePhonePrefix"+id
                 * A final id of a number input (second one with 7 numbers) will look so:
                 * "InputTypeNumberInputTypePhoneNumber"+id
                 */
                // If you send a prop prefixTwoDigits with a value: true, a prefix input will allow to enter 2 digits or 3 digits. 
                prefixTwoDigits: true,
                title: "Test input type PHONE:",
                tooltip: "show tooltip",
                id: "inputTypePhoneTwoDigitsTest",
                required: true,
                /** If you want both inputs to be disabled, send a prop disabled:true  */
                disabled: false,
                /** If you want only one of the inputs to be disabled, send a prop prefixDisabled:true  or numberDisabled:true  */
                prefixDisabled: false,
                numberDisabled: false,
                prefixNotValidMessage: 'Value 072 is FORBIDEN',
                numberNotValidMessage: 'Value 1234567 is FORBIDEN',
            },

            paginationProps: {
                /** href="#some-id" or href="//site.com/" or href="//site.com/#some-id"  */
                hrefs: ['#InputTypeNumberInputNumberId', '#InputTypeTextInputTextId', '#InputTypeSearchListSearchListId', '#InputTypeDateInputDateId', '#InputTypeEmailInputMailId'],
                id: 'testPagination'
            },
        }
    }

    outerValidationFunction = (value) => {
        /** value MUST BE COMPARED  WHITH A TEXT, NOT WHITH A NUMBER */
        if (value !== "1111") return true
        return false
    }
    outerChooseValidationFunction = (value) => {
        /** value MUST BE COMPARED WHITH A TEXT, NOT WHITH A NUMBER */
        if (value !== LM.getString("monthArray")[1]) return true
        return false
    }
    outerMultiSearchValidationFunction = (value) => {
        if (value.includes(LM.getString("monthArray")[1])) return false
        return true
    }
    outerCheckedValidationFunction = (value) => {
        return !value
    }
    outerSliderValidationFunction = (value) => {
        /** value MUST BE COMPARED WHITH A TEXT, NOT WHITH A NUMBER */
        if (value !== '20') return true
        return false
    }
    outerDateValidationFunction = (value) => {
        let dateValue = new Date(value);
        /** format: YY-MM-DD */
        let dateValidation = new Date('2019-05-01');
        if (dateValue.getFullYear() !== dateValidation.getFullYear() || dateValue.getMonth() !== dateValidation.getMonth() || dateValue.getDate() !== dateValidation.getDate()) return true
        return false
    }
    outerTimeValidationFunction = (value) => {
        if (value !== '01:00') return true
        return false
    }

    /** value MUST BE COMPARED WHITH A TEXT, NOT WHITH A NUMBER */
    outerPhonePrefixValidationFunction = (value) => {
        if (value !== '072') return true
        return false
    }
    /** value MUST BE COMPARED WHITH A TEXT, NOT WHITH A NUMBER */
    outerPhoneNumberValidationFunction = (value) => {
        if (value !== '1234567') return true
        return false
    }

    outerOnChange = (e) => {
        console.log('outerOnChange');
    }
    numberInnerRadioChange = (e) => {
        console.log(e.target.checked);
    }

    render() {
        return (
            <Container>
                <Row>
                    <Col>
                        <InputTypeNumber {...this.state.inputTypeNumberType2Props} ref={(componentObj) => { this.numberType2 = componentObj }}
                            validationFunction={this.outerValidationFunction} onChange={this.outerOnChange} />
                    </Col>
                </Row>
                <Row >
                    <Col >
                        <InputTypeNumber {...this.state.inputTypeNumberProps} ref={(componentObj) => { this.number = componentObj }}
                            validationFunction={this.outerValidationFunction} onChange={this.outerOnChange} />
                    </Col>
                </Row>
                <Row >
                    <Col >
                        <InputTypeNumber {...this.state.inputTypeNumberRadioProps} ref={(componentObj) => { this.numberRadio = componentObj }}
                            validationFunction={this.outerValidationFunction} onChange={this.outerOnChange}
                            numberInnerRadioChange={this.numberInnerRadioChange}
                        />
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <InputTypeText {...this.state.inputTypeTextType2Props} ref={(componentObj) => { this.textType2 = componentObj }}
                            validationFunction={this.outerValidationFunction} onChange={this.outerOnChange} />
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <InputTypeText {...this.state.inputTypeTextMultiProps} ref={(componentObj) => { this.multi = componentObj }}
                            validationFunction={this.outerValidationFunction} onChange={this.outerOnChange} />
                    </Col>
                </Row>                

                <Row>
                    <Col>
                        <InputTypeText {...this.state.inputTypeTextProps} ref={(componentObj) => { this.inputText = componentObj }}
                            validationFunction={this.outerValidationFunction} onChange={this.outerOnChange} />
                    </Col>
                </Row>

                <Row>
                    <Col >
                        <InputTypeSearchList {...this.state.inputTypeSearchListType2Props} ref={(componentObj) => { this.searchListType2 = componentObj }}
                            validationFunction={this.outerChooseValidationFunction} onChange={this.outerOnChange} />
                    </Col>
                </Row>
                <Row>
                    <Col >
                        <InputTypeSearchList {...this.state.inputTypeSearchListProps} ref={(componentObj) => { this.searchList = componentObj }}
                            validationFunction={this.outerChooseValidationFunction} onChange={this.outerOnChange} />
                    </Col>
                </Row>
                <Row>
                    <Col >
                        <InputTypeSearchList {...this.state.inputTypeSearchListMultiProps} ref={(componentObj) => { this.multiSearchList = componentObj }}
                            validationFunction={this.outerMultiSearchValidationFunction} onChange={this.outerOnChange} />
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <InputTypeDate {...this.state.inputTypeDateType2Props} ref={(componentObj) => { this.dateType2 = componentObj }}
                            validationFunction={this.outerDateValidationFunction} onChange={this.outerOnChange} />
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <InputTypeDate {...this.state.inputTypeDateProps} ref={(componentObj) => { this.inputTypeDate = componentObj }}
                            validationFunction={this.outerDateValidationFunction} onChange={this.outerOnChange} />
                    </Col>
                </Row>

                <Row>
                    <Col sm='4'>
                        <InputTypeTime {...this.state.inputTypeTimeType2Props} ref={(componentObj) => { this.timeType2 = componentObj }}
                            validationFunction={this.outerTimeValidationFunction} onChange={this.outerOnChange} />
                    </Col>
                </Row>
                <Row>
                    <Col sm='4'>
                        <InputTypeTime {...this.state.inputTypeTimeProps} ref={(componentObj) => { this.inputTypeTime = componentObj }}
                            validationFunction={this.outerTimeValidationFunction} onChange={this.outerOnChange} />
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <InputTypeSelect {...this.state.inputTypeSelectProps} ref={(componentObj) => { this.chooseMonth = componentObj }}
                            validationFunction={this.outerChooseValidationFunction} onChange={this.outerOnChange} />
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <InputTypeRadio {...this.state.inputTypeRadioProps} ref={(componentObj) => { this.testRadio = componentObj }}
                            validationFunction={this.outerChooseValidationFunction} onChange={this.outerOnChange} />
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <InputTypeRadio {...this.state.inputTypeRadioProps1} vertical ref={(componentObj) => { this.testRadio1 = componentObj }}
                            validationFunction={this.outerChooseValidationFunction} onChange={this.outerOnChange} />
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <InputTypeEmail {...this.state.inputTypeEmailProps} ref={(componentObj) => { this.inputMail = componentObj }}
                            validationFunction={this.outerValidationFunction} onChange={this.outerOnChange} />
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <InputTypeCheckBox {...this.state.inputTypeCheckBoxProps} ref={(componentObj) => { this.checkBox = componentObj }}
                            validationFunction={this.outerCheckedValidationFunction} onChange={this.outerOnChange} />
                    </Col>
                    <Col>
                        <InputTypeCheckBox {...this.state.inputTypeCheckBoxProps1} ref={(componentObj) => { this.checkBox1 = componentObj }}
                            validationFunction={this.outerCheckedValidationFunction} onChange={this.outerOnChange} />
                    </Col>
                </Row>

                <Row>
                    <Col sm='4'>
                        <Tag {...this.state.tagProps} ref={(componentObj) => { this.tag = componentObj }} validationFunction={this.outerCheckedValidationFunction} />
                    </Col>
                    <Col sm='4'>
                        <Tag {...this.state.tagProps1} ref={(componentObj) => { this.tag1 = componentObj }} validationFunction={this.outerCheckedValidationFunction} />
                    </Col>
                    <Col sm='4'>
                        <Tag {...this.state.tagProps2} ref={(componentObj) => { this.tag2 = componentObj }} validationFunction={this.outerCheckedValidationFunction} />
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <ImageUpload {...this.state.imageUploadProps} ref={(componentObj) => { this.imageUpload = componentObj }} />
                    </Col>
                </Row>
                <Row>
                    <Col sm="8">
                        <ImageUpload {...this.state.imageUploadProps1} ref={(componentObj) => { this.imageUpload1 = componentObj }} />
                    </Col>
                </Row>
                <Row>
                    <Col sm="6">
                        <ImageUpload {...this.state.imageUploadProps1} />
                    </Col>
                </Row>
                <Row>
                    <Col sm="4">
                        <ImageUpload {...this.state.imageUploadProps1} />
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <Notice {...this.state.noticeProps} />
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <Slider {...this.state.sliderProps} ref={(componentObj) => { this.slider = componentObj }}
                            validationFunction={this.outerSliderValidationFunction} onChange={this.outerOnChange} />
                    </Col>
                </Row>

                <Row>
                    <Col xs="4">
                        <InputTypePhone {...this.state.inputTypePhoneProps} ref={(componentObj) => { this.inputTypePhone = componentObj }}
                            prefixValidationFunction={this.outerPhonePrefixValidationFunction} numberValidationFunction={this.outerPhoneNumberValidationFunction}
                        />
                    </Col>
                </Row>
                <Row>
                    <Col xs="4">
                        <InputTypePhone {...this.state.inputTypePhoneTwoDigitsProps} ref={(componentObj) => { this.inputTypePhoneTwoDigits = componentObj }}
                            onChange={this.outerOnChange} numberOnChange={this.outerOnChange}
                        />
                    </Col>
                </Row>
                <Row>
                    <Col xs="4">
                        <InputTypeFile {...this.state.inputTypeFileProps} ref={(componentObj) => { this.inputTypeFile = componentObj }}
                        />
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <Button onClick={(e) => {
                            alert(`select value: ${this.chooseMonth.getValue().value}, select valid: ${this.chooseMonth.getValue().valid};
horisontal radio value:   ${this.testRadio.getValue().value}, horisontal radio valid: ${this.testRadio.getValue().valid};
vertical radio value: ${this.testRadio1.getValue().value} , vertical radio valid: ${this.testRadio1.getValue().valid};
email value: ${this.inputMail.getValue().value}, email valid: ${this.inputMail.getValue().valid};
text input value: ${this.inputText.getValue().value}, text input valid: ${this.inputText.getValue().valid};

checkbox value: ${this.checkBox.getValue().value}, checkbox valid: ${this.checkBox.getValue().valid};
checkbox1 value: ${this.checkBox1.getValue().value}, checkbox1 valid: ${this.checkBox1.getValue().valid};

number input value: ${this.number.getValue().value}, number input valid: ${this.number.getValue().valid};
multiSearchList input value: ${this.multiSearchList.getValue().value}, multiSearchList input valid: ${this.multiSearchList.getValue().valid};
tag value: ${this.tag.getValue().value} , tag valid: ${this.tag.getValue().valid};
tag1 value: ${this.tag1.getValue().value}, tag1 valid: ${this.tag1.getValue().valid};
tag2 value: ${this.tag2.getValue().value}, tag2 valid: ${this.tag2.getValue().valid};
imageUpload value: ${this.imageUpload.getValue().value}, imageUpload valid: ${this.imageUpload.getValue().valid};
slider value: ${this.slider.getValue().value}, slider valid: ${this.slider.getValue().valid};
inputTypeDate value: ${this.inputTypeDate.getValue().value}, inputTypeDate valid: ${this.inputTypeDate.getValue().valid};
inputTypeTime value: ${this.inputTypeTime.getValue().value}, inputTypeTime valid: ${this.inputTypeTime.getValue().valid};
inputTypePhone value: ${this.inputTypePhone.getValue().value}, inputTypePhone valid: ${this.inputTypePhone.getValue().valid} ;
inputTypePhoneTwoDigits value: ${this.inputTypePhoneTwoDigits.getValue().value}, inputTypePhoneTwoDigits valid: ${this.inputTypePhoneTwoDigits.getValue().valid} ;
inputTypeFile value: ${this.inputTypeFile.getValue().value}, inputTypeFile valid: ${this.inputTypeFile.getValue().valid} ;
`)
                        }}>
                            getValue of my Inputs
                        </Button>
                    </Col>
                    <Col>
                        <Button onClick={(e) => {
                            if (this.inputTypeDate.setValue("2012-31-12")) {
                            } else if (this.inputTypeDate.setValue("2012-12-12")) { }

                            if (this.inputTypeTime.setValue("18:50:15")) {
                            } else if (this.inputTypeTime.setValue("23:59")) { }

                            if (this.inputMail.setValue("@gmail.com")) {
                            } else if (this.inputMail.setValue("mail@gmail.com")) { }

                            if (this.testRadio.setValue("hello")) {
                            } else if (this.testRadio.setValue(LM.getString("monthArray")[4])) { }

                            this.searchList.setValue('February');
                            this.multiSearchList.setValue(['פברואר']);

                            if (this.slider.setValue('120')) {
                            } else if (this.slider.setValue('50')) { }

                            if (this.tag.setValue("text")) {
                            } else if (this.tag.setValue(true)) { }

                            this.imageUpload1.setValue(imgUploaded);

                            this.number.setValue(5);
                            this.inputText.setValue("dd5");

                            if (this.chooseMonth.setValue("text")) {
                            } else if (this.chooseMonth.setValue(LM.getString("monthArray")[5])) { }

                            if (this.checkBox1.setValue("text")) {
                            } else if (this.checkBox1.setValue(true)) { }

                            this.numberType2.setValue(555);
                            this.textType2.setValue("type2");
                            this.multi.setValue(`multi
                            string value has been set`);
                            this.dateType2.setValue("2012-12-12");
                            this.timeType2.setValue("23:59")

                            this.inputTypePhone.setValue("0520000000")
                            this.inputTypePhoneTwoDigits.setValue("020000000")
                        }}>
                            setValue
                        </Button>
                    </Col>
                </Row>

                <Row className="mt-3">
                    <PeletalkPagination {...this.state.paginationProps} />
                </Row>
                <Row> <Col> Test PeletalkSpinner </Col> </Row>
                <PeletalkSpinner />
                <Row>
                    <Col>
                        <Button onClick={() => {
                            Loader.show();
                            setTimeout(() => {
                                Loader.hide();
                            }, 2000)
                        }}>
                            Loader.show()
                        </Button>
                    </Col>
                </Row>
            </Container>
        );
    }
}

export default TestScreen;