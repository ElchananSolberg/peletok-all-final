import React, { Component } from 'react';

import { UncontrolledTooltip } from 'reactstrap';

import './InputUtils.css';

import {faInfoCircle} from '@fortawesome/free-solid-svg-icons';

import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";



export class Tooltip extends Component {

  render() {
    let id = 'Tooltip' + this.props.id;
    return (
      <>
        {/*<div className="infoImg" id={id}>*/}
          {/*<b>i</b>*/}
        {/*</div>*/}
        <FontAwesomeIcon icon={faInfoCircle} className={'fs-17 co-gray'} id={id}/>

        <UncontrolledTooltip placement="top" target={id}>
          {this.props.tooltipText}
        </UncontrolledTooltip>

      </>
    );
  }
}
