import React, { Component } from 'react';
import './InputUtils.css';
import { Row, Col } from 'reactstrap';
import { Tooltip } from './Tooltip';
import InputTypeNumber from './InputTypeNumber';
import Asterisk from '../Ui/Asterisk';
/**
 * InputTypePhone component lets to input the PHONE inside TWO INPUTS in next format: 3-digits prefix and 7-digits number (XXX-XXXXXXX). 
 * it can render the title above the inputs (if this.props.title is not an empty string).
 * it can render the tooltip to the side of the title (if this.props.tooltip is not an empty string).
 * 
 * It validates the value was entered (if this.props.required is true).
 * It validates the length of a value of a prefix input is not shorter and not longer than 3.
 * It validates the length of a value of a number input is not shorter and not longer than 7.
 * It validates the value is valid to the functions sent in  this.props.prefixValidationFunction and this.props.numberValidationFunction; 
 * if there are any validation issues the input will get a prop "invalid" and will change a border color to red;
 * if there are any validation issues bellow the input will be the error message 
 * It can change both input to disabled view due to this.props.disabled.
 * It can change one of the inputs to disabled view due to this.props.prefixDisabled  or this.props.numberDisabled.
 * It passes the focus from a prefix input to a number input when 3 digits are input into the prefix input.
 */
class InputTypePhone extends Component {

    prefixOnChange = (e) => {
        if (e.target.value.length >= 3) {
            if (e.target.value.length > 3) {
                let fixedValue = e.target.value.slice(0, 3);
                this.prefix.setValue(fixedValue);
            }
            document.getElementById("InputTypeNumber" + "InputTypePhoneNumber" + this.props.id).focus();
        }
        if (this.props.onChange) this.props.onChange(e);
        if (this.props.prefixOnChange) this.props.prefixOnChange(e);
    }
    numberOnChange = (e) => {
        if (e.target.value.length > 7) this.number.setValue(e.target.value.slice(0, 7));
        if (this.props.onChange) this.props.onChange(e);
        if (this.props.numberOnChange) this.props.numberOnChange(e);
    }

    getValue = () => {
        return { valid: this.prefix.getValue().valid && this.number.getValue().valid, value: this.prefix.getValue().value + this.number.getValue().value };
    }

    setValue = (newValue) => {
        if (newValue.length === 10) {
            let prefixValue = newValue.slice(0, 3);
            let numberValue = newValue.slice(3);
            this.prefix.setValue(prefixValue);
            this.number.setValue(numberValue);
        }
        if (this.props.prefixTwoDigits && newValue.length === 9) {
            let prefixValue = newValue.slice(0, 2);
            let numberValue = newValue.slice(2);
            this.prefix.setValue(prefixValue);
            this.number.setValue(numberValue);
        }
    }

    render() {
        let id = this.props.id.replace(/\s+/g, '');
        let title = this.props.title;
        let tooltipText = this.props.tooltip;

        let prefixProps = {
            minLength: this.props.prefixTwoDigits ? 2 : 3,
            maxLength: 3,
            id: "InputTypePhonePrefix" + this.props.id,
            required: this.props.required,
            disabled: this.props.disabled || this.props.prefixDisabled,
            notValidMessage: this.props.prefixNotValidMessage,
        };
        let numberProps = {
            minLength: 7,
            maxLength: 7,
            id: "InputTypePhoneNumber" + this.props.id,
            required: this.props.required,
            disabled: this.props.disabled || this.props.numberDisabled,
            notValidMessage: this.props.numberNotValidMessage,
        };

        return (
            <>
                <Row>
                    <Col className={"d-flex justify-content-between line-height-1 " + (title ? 'py-1' : '')}>
                        {title ?
                            <div className="fs-17 white-space">
                                {title}
                                <Asterisk show={this.props.required} />
                            </div>
                            : <><Asterisk show={this.props.required} /></>
                        }
                        {tooltipText ?
                            <Tooltip tooltipText={tooltipText} id={id} />
                            : <></>
                        }
                    </Col>
                </Row>
                <Row style={{ direction: "ltr" }}>
                    <Col xs="4">
                        <InputTypeNumber {.../*this.state.*/prefixProps} ref={(componentObj) => { this.prefix = componentObj }}
                            validationFunction={this.props.prefixValidationFunction} onChange={this.prefixOnChange} />
                    </Col>
                    <Col xs="8">
                        <InputTypeNumber {.../*this.state.*/numberProps} ref={(componentObj) => { this.number = componentObj }}
                            validationFunction={this.props.numberValidationFunction} onChange={this.numberOnChange} />
                    </Col>
                </Row>
            </>
        )
    }
}
export default InputTypePhone;
