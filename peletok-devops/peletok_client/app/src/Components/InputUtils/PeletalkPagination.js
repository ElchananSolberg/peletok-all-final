import React, { Component } from 'react';
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap';

import './InputUtils.css';

import { LanguageManager as LM } from '../LanguageManager/Language'

import PeletalkPaginationItem from './PeletalkPaginationItem';

/** All what a programmer-user needs to do is to send an array of valid url-strings into hrefs prop.
 * Possible url-strings: "#some-id" or "//site.com/" or "//site.com/#some-id" or .html file instead of site.com
 */
class PeletalkPagination extends Component {
    constructor(props) {
        super(props);

        this.state = {
            activeIndex : 0,
            data: []
        }
    }

    peletalkPaginationItemClick = (e) => {
        const activeIndex = parseInt(e.target.attributes.index.value);
        if(this.props.handleClick){
            let data = this.props.handleClick(activeIndex+1);
            this.setState({data: data})
        }
        this.setState({ activeIndex : activeIndex });
    }

    nextClick = () =>{
        this.setState({ activeIndex : this.state.activeIndex+1 });
    }
    previousClick = () =>{
        this.setState({ activeIndex : this.state.activeIndex-1 });
    }
    startClick = () =>{
        this.setState({ activeIndex : 0 });
    }
    endClick = () =>{
        this.setState({ activeIndex : (this.props.hrefs.length-1) });
    }

    render() {
        let id=this.props.id.replace(/\s+/g, '');
        let hrefs = this.props.hrefs;
        let peletalkPaginationItems = hrefs.map((current, index) => { 
            return <PeletalkPaginationItem href={current} activeIndex={ this.state.activeIndex} key={index} index={index} id={id} onClick={this.peletalkPaginationItemClick} />
        });

        return (
            <Pagination aria-label="Pages navigation">

                <PaginationItem >                    
                    <PaginationLink first href={hrefs[0]} id={id+'Start'} onClick={this.startClick} className={(LM.getDirection() === "rtl" ? "rtl" : "")}> {LM.getString('start')} </PaginationLink>
                </PaginationItem>

                <PaginationItem hidden={this.state.activeIndex <= 0} >
                    <PaginationLink previous href={hrefs[this.state.activeIndex-1]} id={id+'Previous'} onClick={this.previousClick} />
                </PaginationItem>


                {peletalkPaginationItems}
                {this.state.data?<div>{this.state.data}</div>:<div/>}

                <PaginationItem hidden={this.state.activeIndex >= hrefs.length - 1} >
                    <PaginationLink next href={hrefs[this.state.activeIndex+1]} id={id+'Next'} onClick={this.nextClick} />
                </PaginationItem>

                <PaginationItem>
                    <PaginationLink last href={hrefs[hrefs.length - 1]} id={id+'End'} onClick={this.endClick} className={(LM.getDirection() === "rtl" ? "rtl" : "")}> {LM.getString('end')} </PaginationLink>
                </PaginationItem>

            </Pagination>
        )
    }
}

export default PeletalkPagination;