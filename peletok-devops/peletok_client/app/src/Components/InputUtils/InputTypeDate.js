import React, { Component } from 'react';
import './InputUtils.css';
import { FormFeedback, Input, Row, Col, Card, CardText } from 'reactstrap';

import { Tooltip } from './Tooltip';

import { LanguageManager as LM } from '../LanguageManager/Language'

/**
 * InputTypeDate component lets to input the DATE. 
 * it can render the title above the input (if this.props.title is not an empty string).
 * it can render the tooltip to the side of the title (if this.props.tooltip is not an empty string).
 * 
 * it validates the value is not later than this.props.max .
 * it validates the value is not early than this.props.min .
 * It validates the value was entered (if this.props.required is true).
 * It validates the value is valid to the function sent in  this.props.validationFunction;
 * if there are any validation issues the input will get a prop "invalid" and will change a border color to red;
 * if there are any validation issues bellow the input will be the error message 
 * It can add outer onChange function to input onChange.
 * It can change an input to disabled view due to this.props.disabled.
 */
class InputTypeDate extends Component {
    constructor(props) {
        super(props);
        let innerValidMsg = "";
        if (this.props.min && this.props.min !== -1) {
            innerValidMsg += LM.getString("dateInnerValidationFunctionMsgStart");
            innerValidMsg += LM.getString("dateInnerValidationFunctionMsgNotLess") + this.props.min;
        }
        if (this.props.max && this.props.max !== -1) {
            if (innerValidMsg === '') {
                innerValidMsg += LM.getString("dateInnerValidationFunctionMsgStart");;
            } else {
                innerValidMsg += LM.getString("numberInnerValidationFunctionMsgAnd");
            }
            innerValidMsg += LM.getString("dateInnerValidationFunctionMsgNotMore") + this.props.max;
        }


        this.state = {
            value: this.props.default,
            validationFuncs: [
                {
                    /**  Validates the value is valid to the function sent in  this.props.validationFunction */
                    f: this.props.validationFunction || (() => (true)),
                    msg: this.props.notValidMessage
                },
                {
                    f: this.innerValidationFunction,
                    msg: innerValidMsg,
                },
                {
                    f: this.secondInnerValidationFunction,
                    msg: LM.getString("invalidDate"),
                },
                {
                    f: this.isRequiredValidation,
                    msg: LM.getString("isRequiredValidationFunctionMsg"),
                }
            ],
            errorMessage: '',
        }
    }

    /** can add outer onChange function to input onChange. */
    inputTypeDateChange = (e) => {
        e.persist();
        this.setState({ value: e.target.value },
            () => {
                if (this.props.onChange) {
                    this.props.onChange(e);
                }
            }
        );
    }

    /** summarizing validation function
    * is running 2 times: when input is loosing focus and when getValue function is running (by programmer-user) 
    * */
    runValidation = () => {
        let summarizingValidationResult = (!this.props.required && !this.state.value) || this.state.validationFuncs.every((validation) => validation.f(this.state.value));
        if (!summarizingValidationResult) {
            let messages = [];
            this.state.validationFuncs.forEach((validation) => {
                if (!validation.f(this.state.value)) {
                    messages.push(validation.msg);
                }
            })
            this.setState({ errorMessage: messages.join(`\n`) });
        } else {
            this.setState({ errorMessage: '' });
        }
        return summarizingValidationResult;
    }

    getValue = () => {
        let summarizingValidationResult = this.runValidation();
        return { valid: summarizingValidationResult, value: this.state.value };
    }

    setValue = (newValue) => {
        let dateNewValue = new Date(newValue);
        if (dateNewValue.getFullYear()) {
            this.setState({ value: newValue });
            return true;
        }
        return false;
    }

    /** Sets the today to be a default value.
     * Should be used by programmers-users to clear the input value. 
     */
    setDefault = () => {
        let dateNow = new Date();
        let yearNow = dateNow.getFullYear();
        let monthNow = dateNow.getMonth() + 1;
        let dayNow = dateNow.getDate();

        let stringMonth;        
        if (monthNow.toString().length === 1) stringMonth = "0" + monthNow
        if (monthNow.toString().length === 2) stringMonth = monthNow
        let stringDay;
        if (dayNow.toString().length === 1) stringDay = "0" + dayNow
        else if (dayNow.toString().length === 2) stringDay = dayNow

        this.setValue(yearNow + "-" + stringMonth + "-" + stringDay); /** format: YYYY-MM-DD */
    }

    /** If this.props.required is true validates the value was entered. */
    isRequiredValidation = (value) => {
        if (!value && this.props.required) {
            return false;
        }
        return true;
    }

    /** Validates the value is not less than this.props.min and not bigger then this.props.max */
    innerValidationFunction = (value) => {
        let min = new Date(this.props.min);
        let max = new Date(this.props.max);
        let dateValue = new Date(value);

        if (dateValue.getFullYear()) {
            if (dateValue < min || dateValue > max) {
                return false;
            }
        }
        return true;
    }

    /** Validates the date value is not invalid date */
    secondInnerValidationFunction = (value) => {
        let dateVal = new Date(value);
        if (dateVal.getFullYear()) {
            return true;
        }
        return false;
    }

    componentDidMount() {
        this.setDefault();
    }

    render() {
        let disabled = this.props.disabled;
        let placeholder = this.props.placeholder;
        let id = this.props.id.replace(/\s+/g, '');
        let title = this.props.title;
        let tooltipText = this.props.tooltip;

        let type2 = this.props.type2;

        return (type2 ?
            <>
                <Row >
                    <Col >
                        <Card className="type2Card">
                            <CardText className="type2CardText"  >
                                {title}
                            </CardText>
                            <Input className={"type2Input " + (LM.getDirection() === "rtl" ? "rtl" : "")} type="date"
                                disabled={disabled} invalid={this.state.errorMessage.length !== 0}
                                id={`InputTypeDate${id}`} placeholder={placeholder}
                                onChange={this.inputTypeDateChange} onBlur={this.runValidation}
                                value={this.state.value}
                            />
                            {/** if there are any validation issues bellow the input will be the error message */}
                            <FormFeedback className='ws-pre'>{this.state.errorMessage}</FormFeedback>
                        </Card>
                    </Col>
                </Row>
            </>
            :
            <>
                <Row>
                    <Col className={"d-flex justify-content-between line-height-1 " + (title ? 'py-1' : '') }>
                        {title ?
                            <div className="fs-17 white-space">{title}</div>
                            : <></>
                        }
                        {tooltipText ?
                            <Tooltip tooltipText={tooltipText} id={id} />
                            : <></>
                        }
                    </Col>
                </Row>
                <Row className={'mt-auto'}>
                    <Col >
                        {/** if there are any validation issues the input will get a prop "invalid" and will change a border color to red */}
                        <Input className={(LM.getDirection() === "rtl" ? "rtl" : "")} type="date"
                            disabled={disabled} invalid={this.state.errorMessage.length !== 0}
                            id={`InputTypeDate${id}`} placeholder={placeholder}
                            onChange={this.inputTypeDateChange} onBlur={this.runValidation}
                            value={this.state.value}
                        />
                        {/** if there are any validation issues bellow the input will be the error message */}
                        <FormFeedback className='ws-pre'>{this.state.errorMessage}</FormFeedback>
                    </Col>
                </Row>
            </>

        );
    }
}

export default InputTypeDate;