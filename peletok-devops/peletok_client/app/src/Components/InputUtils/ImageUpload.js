import React, { Component } from 'react';
import './InputUtils.css';
import { Row, Col } from 'reactstrap';
/** an icon of upload */
import uploadImage from './upload_image.svg';
/** an image with only white background */
import whiteBlock from './white_block.png';
import { Tooltip } from './Tooltip';
import { LanguageManager as LM } from '../LanguageManager/Language';
import Asterisk from '../Ui/Asterisk';


/**
 * ImageUpload component lets the user to choose an image on his PC. ImageUpload will show this image inside himself.
 * it can render the title above the image.
 * it can render the tooltip to the side of the title.
 * it renders the starting image with white background. 
 * you can CHANGE the STARTING IMAGE by sending it inside of the prop: default.
 */
class ImageUpload extends Component {
    constructor(props) {
        super(props);

        this.state = {
            /** variable that will be returned by the getValue() function (= sent to the server) */
            img: this.props.default || whiteBlock,

            /** sorce image of <img> that later will show image chosen by the user */
            imagePreviewUrl: this.props.default || whiteBlock,

            validationFuncs: [
                {
                    f: this.innerValidationFunction,
                    msg: LM.getString("imageUploadInnerValidationFunctionMsg"),
                },           
                {
                    f: this.isRequiredValidation,
                    msg: LM.getString("isRequiredValidationFunctionMsg"),
                }
            ],
            errorMessage: '',
        }
    }

    /** click handler of an icon-image "Upload Image". It runs the click() of an input type="file" */
    imgClicked = () => {
        this.refs.fileUploader.click();
    };

    /** Change handler of an input type="file". It will run only if user picked a file.
     * It changes the  sorce image of <img> that should show the image chosen by the user.
     * And the variable that will be returned by the getValue() .
     */
    fileChanged = (e) => {
        e.preventDefault();
        let reader = new FileReader();
        let file = e.target.files[0];

        reader.onloadend = ((object) => {
            return () => {
                object.setState({
                    img: file,
                    imagePreviewUrl: reader.result,
                });
                this.runValidation();
            }
        })(this)

        if (e.target.files[0]) {
            reader.readAsDataURL(e.target.files[0]);
        }        
    }

    /** summarizing validation function
    * is running 3 times: when a user is clicking on the div and when the file was selected and when getValue function is running (by programmer-user) 
    * */
    runValidation = () => {
        let summarizingValidationResult = (!this.props.required && !this.state.imagePreviewUrl) || this.state.validationFuncs.every((validation) => validation.f(this.state.imagePreviewUrl));
        if (!summarizingValidationResult) {
            let messages = [];
            this.state.validationFuncs.forEach((validation) => {
                if (!validation.f(this.state.imagePreviewUrl)) {
                    messages.push(validation.msg);
                }
            })
            this.setState(({ errorMessage: messages.join(`\n`) }));
        } else {
            this.setState(({ errorMessage: '' }));
        }
        return summarizingValidationResult;
    }

    getValue = () => {
        let summarizingValidationResult = this.runValidation();
        return { valid: summarizingValidationResult, value: this.state.img };
    }

    setValue = (newValue) => {
        this.setState({ img : newValue, imagePreviewUrl : newValue });
    }

    /** If this.props.required is true validates the image was selected (it is not whiteBlock). */
    isRequiredValidation = (value) => {
        if (this.props.required && (value === whiteBlock)) {
            return false;
        }
        return true;
    }

    /** Validates the file type is one of image file types  */
    innerValidationFunction = () => {
        if ((this.state.img === this.props.default || this.state.img === whiteBlock) ||
            (typeof this.state.img==="object" && ['image/jpeg','image/jpg','image/gif','image/png','image/bmp', 'image/tif', 'image/svg+xml'].includes(this.state.img.type))||
            (typeof this.state.img==="string" && (['.jpeg','.jpg','.gif','.png','.bmp', '.tif', '.svg'].some((suffix) => {return this.state.img.lastIndexOf(suffix) === (this.state.img.length - suffix.length)})))){
            return true;
        }
        return false;
    }

    render() {
        let id = this.props.id.replace(/\s+/g, '');
        let title = this.props.title;
        let tooltipText = this.props.tooltip;

        return (
            <>
                <Row>
                    <Col className={"d-flex justify-content-between line-height-1 " + (title ? 'py-1' : '') }>
                        {title ?
                            <div className="fs-17 white-space">{title}
                             <Asterisk show={this.props.required}/></div>
                            : <> <Asterisk show={this.props.required}/></>
                        }
                        
                        {tooltipText ?
                            <Tooltip tooltipText={tooltipText} id={id} />
                            : <></>
                        }
                    </Col>
                </Row>
                <Row>
                    <Col>
                        {/** if there are any validation issues the div will change a border color to red */}
                        <div className={"imageUploadDiv " + (this.state.errorMessage ? "invalidDiv" : "")} onClick={this.runValidation}>
                            <img className="imageUploadUploadedPicture" src={this.state.imagePreviewUrl} alt="Uploaded Picture" />
                            <div className="iconDiv" >
                                <img disabled className="iconImage" src={uploadImage} alt="Upload Image" onClick={this.imgClicked} />
                                <input type="file" accept="image/*" style={{ display: 'none' }} id={`ImageUpload${id}`} ref="fileUploader" onChange={this.fileChanged} disabled={this.props.disabled}/>
                            </div>
                        </div>
                        {/** if there are any validation issues bellow the input will be the error message */}
                        <div className='ws-pre errorMessageDiv'>{this.state.errorMessage}</div>
                    </Col>
                </Row>
            </>
        )
    }
}
export default ImageUpload;