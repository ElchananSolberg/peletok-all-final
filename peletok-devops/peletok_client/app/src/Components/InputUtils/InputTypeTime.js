import React, { Component } from 'react';
import './InputUtils.css';
import { FormFeedback, Input, Row, Col, Card, CardText } from 'reactstrap';

import { Tooltip } from './Tooltip';

import { LanguageManager as LM } from '../LanguageManager/Language'

/**
 * InputTypeTime component lets to input the TIME. 
 * it can render the title above the input (if this.props.title is not an empty string).
 * it can render the tooltip to the side of the title (if this.props.tooltip is not an empty string).
 * 
 * it validates the value is not later than this.props.max .
 * it validates the value is not early than this.props.min .
 * It validates the value was entered (if this.props.required is true).
 * It validates the value is valid to the function sent in  this.props.validationFunction;
 * if there are any validation issues the input will get a prop "invalid" and will change a border color to red;
 * if there are any validation issues bellow the input will be the error message 
 * It can add outer onChange function to input onChange.
 * It can change an input to disabled view due to this.props.disabled.
 */
class InputTypeTime extends Component {
    constructor(props) {
        super(props);

        let innerValidMsg = "";
        if (this.props.min && this.props.min !== -1) {
            innerValidMsg += LM.getString("timeInnerValidationFunctionMsgStart");
            innerValidMsg += LM.getString("dateInnerValidationFunctionMsgNotLess") + this.props.min;
        }
        if (this.props.max && this.props.max !== -1) {
            if (innerValidMsg === '') {
                innerValidMsg += LM.getString("timeInnerValidationFunctionMsgStart");
            } else {
                innerValidMsg += LM.getString("numberInnerValidationFunctionMsgAnd");
            }
            innerValidMsg += LM.getString("dateInnerValidationFunctionMsgNotMore") + this.props.max;
        }

        this.state = {
            value: this.props.default || "00:00",
            hour: this.props.default ? this.props.default.slice(0, 2) : '00',
            minute: this.props.default ? this.props.default.slice(3, 6) : '00',
            validationFuncs: [
                {
                    /**  Validates the value is valid to the function sent in  this.props.validationFunction */
                    f: this.props.validationFunction || (() => (true)),
                    msg: this.props.notValidMessage
                },
                {
                    f: this.innerValidationFunction,
                    msg: innerValidMsg,
                },

                {
                    f: this.isRequiredValidation,
                    msg: LM.getString("isRequiredValidationFunctionMsg"),
                }
            ],
            errorMessage: '',
        }
    }

    /** can add outer onChange function to onChange. */
    inputTypeTimeChange = (e) => {
        e.persist();
        if (e.target.id === 'InputTypeTimeHour' + this.props.id) {
            this.setState({ hour: e.target.value }, () => {
                this.setState({ value: (this.state.hour + ":" + this.state.minute) },
                    () => {
                        if (this.props.onChange) this.props.onChange(e);
                    }
                );
            });
        }
        if (e.target.id === 'InputTypeTimeMinute' + this.props.id) {
            this.setState({ minute: e.target.value }, () => {
                this.setState({ value: (this.state.hour + ":" + this.state.minute) },
                    () => {
                        if (this.props.onChange) this.props.onChange(e);
                    }
                );
            });
        }
    }

    /** summarizing validation function
    * is running 2 times: when input is loosing focus and when getValue function is running (by programmer-user)
    * */
    runValidation = () => {
        let summarizingValidationResult = (!this.props.required && !this.state.value) || this.state.validationFuncs.every((validation) => validation.f(this.state.value));
        if (!summarizingValidationResult) {
            let messages = [];
            this.state.validationFuncs.forEach((validation) => {
                if (!validation.f(this.state.value)) {
                    messages.push(validation.msg);
                }
            })
            this.setState(({ errorMessage: messages.join(`\n`) }));
        } else {
            this.setState(({ errorMessage: '' }));
        }
        return summarizingValidationResult;
    }

    getValue = () => {
        let summarizingValidationResult = this.runValidation();
        return { valid: summarizingValidationResult, value: this.state.value };
    }

    setValue = (newValue) => {
        let timeArray = newValue.split(":")
        if (timeArray.length < 2 || timeArray.length > 3) return false;
        let hour = timeArray[0];
        let minute = timeArray[1];
        if (hour.length === 2 && minute.length === 2 && hour <= 23 && minute <= 59 && minute >= 0 && hour >= 0) {
            this.setState({ value: `${hour}:${minute}` });
            return true;
        }
        return false;
    }

    /** If this.props.required is true validates the value was entered. */
    isRequiredValidation = (value) => {
        if (!value && this.props.required) {
            return false;
        }
        return true;
    }

    /** Validates the value is not less than this.props.min and not bigger then this.props.max */
    innerValidationFunction = (value) => {
        let min = this.props.min;
        let max = this.props.max;
        if (value < min || value > max) {
            return false;
        }
        return true;
    }

    render() {
        let disabled = this.props.disabled;
        let placeholder = this.props.placeholder;
        let id = this.props.id.replace(/\s+/g, '');
        let title = this.props.title;
        let tooltipText = this.props.tooltip;
        let defaultValue = this.props.default;
        let hours = ["00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23"].map((current) => { return <option > {current} </option> });
        let minutes = ["00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", '24', '25', '26', '27', '28', '29',
            '30', "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", '54', '55', '56', '57', '58', '59'
        ].map((current) => { return <option > {current} </option> });
        let type2 = this.props.type2;

        return (type2 ?
            <>
                <Row >
                    <Col >
                        <Card className="type2Card">
                            <CardText className="type2CardText"  >
                                {title}
                            </CardText>
                            <Row className={'align-items-center mt-auto ' + (LM.getDirection() === "rtl" ? "direction" : "")} onBlur={this.runValidation} >
                                <Col >
                                    <div className="position">
                                        <Input className="type2Input timeSelectInput"
                                            type="select" id={`InputTypeTimeHour${id}`}
                                            disabled={disabled} invalid={this.state.errorMessage.length !== 0}
                                            onChange={this.inputTypeTimeChange}
                                            defaultValue={defaultValue ? defaultValue.slice(0, 2) : "00"}
                                            value={this.state.value.slice(0, 2)}
                                        >
                                            {hours}
                                        </Input>
                                        <div className="timeSelectIcon" />
                                    </div>
                                </Col>
                                <Col className='p-0' sm='auto'> : </Col>
                                <Col >
                                    <div className="position">
                                        <Input className="type2Input timeSelectInput"
                                            type="select" id={`InputTypeTimeMinute${id}`}
                                            disabled={disabled} invalid={this.state.errorMessage.length !== 0}
                                            onChange={this.inputTypeTimeChange}
                                            defaultValue={defaultValue ? defaultValue.slice(3, 6) : '00'}
                                            value={this.state.value.slice(3, 6)}
                                        >
                                            {minutes}
                                        </Input>
                                        <div className="timeSelectIcon" />
                                    </div>
                                </Col>
                            </Row>
                            <div className='ws-pre errorMessageDiv'>{this.state.errorMessage}</div>
                        </Card>
                    </Col>
                </Row>
            </>
            :
            <>
                <Row>
                    <Col className={"d-flex justify-content-between line-height-1 " + (title ? 'py-1' : '')}>
                        {title ?
                            <div className="fs-17 white-space">{title}</div>
                            : <></>
                        }
                        {tooltipText ?
                            <Tooltip tooltipText={tooltipText} id={id} />
                            : <></>
                        }
                    </Col>
                </Row>
                <Row className={'align-items-center mt-auto ' + (LM.getDirection() === "rtl" ? "direction" : "")} onBlur={this.runValidation} >
                    <Col >
                        <div className="position">
                            <Input className="timeSelectInput"
                                type="select" id={`InputTypeTimeHour${id}`}
                                disabled={disabled} invalid={this.state.errorMessage.length !== 0}
                                onChange={this.inputTypeTimeChange}
                                defaultValue={defaultValue ? defaultValue.slice(0, 2) : "00"}
                                value={this.state.value.slice(0, 2)}
                            >
                                {hours}
                            </Input>
                            <div className="timeSelectIcon" />
                        </div>
                    </Col>
                    <Col className='p-0' sm='auto'> : </Col>
                    <Col >
                        <div className="position">
                            <Input className="timeSelectInput"
                                type="select" id={`InputTypeTimeMinute${id}`}
                                disabled={disabled} invalid={this.state.errorMessage.length !== 0}
                                onChange={this.inputTypeTimeChange}
                                defaultValue={defaultValue ? defaultValue.slice(3, 6) : '00'}
                                value={this.state.value.slice(3, 6)}
                            >
                                {minutes}
                            </Input>
                            <div className="timeSelectIcon" />
                        </div>
                    </Col>
                </Row>
                <div className='ws-pre errorMessageDiv'>{this.state.errorMessage}</div>
            </>
        );
    }
}

export default InputTypeTime;