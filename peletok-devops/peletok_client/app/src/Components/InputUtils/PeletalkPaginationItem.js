import React, { Component } from 'react';
import { PaginationItem, PaginationLink } from 'reactstrap';

import './InputUtils.css';

/** This Component is dynamic PaginationItem.
 * index prop is hand made.
*/
class PeletalkPaginationItem extends Component {
    render() {
        let href = this.props.href;        
        let index = this.props.index;
        let active = this.props.activeIndex === index;
        let id = this.props.id+index;

        return (
            <PaginationItem active={active} >
                <PaginationLink href={href} onClick={this.props.onClick} index={index} id={id}>
                    {index+1}
                </PaginationLink>
            </PaginationItem>
        )
    }
}
export default PeletalkPaginationItem;