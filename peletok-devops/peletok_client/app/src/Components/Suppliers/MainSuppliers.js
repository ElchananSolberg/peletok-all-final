import React, { Component } from 'react';
import './suppliers.css'
import { LanguageManager as LM } from '../LanguageManager/Language';
import {
    getSuppliers, getSupplierTypes, getCreditCardOptions,
    putSuppliersUpdate, postNewSuppliers, deleteSupplier, getSupplier
} from '../DataProvider/DataProvider'
import { config } from '../../utils/Config'
import { Button, Container, Row, Col, Collapse } from 'reactstrap';
import InputTypeText from '../InputUtils/InputTypeText';
import InputTypeSearchList from '../InputUtils/InputTypeSearchList';
import InputTypeNumber from '../InputUtils/InputTypeNumber';
import InputTypeCheckBox from '../InputUtils/InputTypeCheckBox'
import InputTypeSimpleSelect from '../InputUtils/InputTypeSimpleSelect'
import InputTypeColor from '../InputUtils/InputTypeColor'
import ImageUpload from '../InputUtils/ImageUpload'
import whiteBlock from '../InputUtils/white_block.png';
import { Notifications } from '../Notifications/Notifications'
import Select from 'react-select'
import InputTypeAddress from "../InputUtils/InputTypeAddress";
import { Loader } from '../Loader/Loader'
import { objectToFormData } from '../../utilities/utilities'
import { SupplierIdsEnum } from '../../enums'

export class MainSuppliers extends Component {
    constructor(props) {

        super(props);

        this.state = {
            renderFields: true,
            creatingNew: false,
            suppliers: [],
            loadedSupplier: {},
            supplierTypes: [],
            creditCardOptions: [],
            imageChanged: false,
        }
        this.supplierLoader = this.supplierLoader.bind(this);
        this.loadSuppliersList = this.loadSuppliersList.bind(this);
        this.loadCreditCardOptions = this.loadCreditCardOptions.bind(this);
        this.loadSupplierTypes = this.loadSupplierTypes.bind(this);
        this.checkIfSupplierExists = this.checkIfSupplierExists.bind(this);
        this.fields = {}
        this.fromObjFields = {}
    }
    clearImage() {
        this.fields["image"].setValue(whiteBlock);
    }

    checkIfSupplierExists = (name) => {
        let supplierNotExists = false;
        this.state.suppliers.forEach(supplier => {
            if (supplier.name === name) {
                supplierNotExists = false
                return supplierNotExists
            } else {
                supplierNotExists = true
            }
        });
        return supplierNotExists
    }

    save = (e) => {
        let isValid = true;
        // TODO sent array of talk prices instead of xls file
        let data = {};
        Object.keys(this.fields).filter(k=>this.fields[k]).filter(k=>this.fields[k]).forEach((key) => {
            if (this.fields[key].runValidation) {
                isValid = this.fields[key].runValidation() ? isValid : false;
            }

            let value = this.fields[key].getValue();    
            data[key] = value.value == undefined ? null : value.value;           
                    if (this.fields['default_creation'].getValue().value == null) {
                        delete data['default_creation']
                    }
        
                    if (this.fields['cancel_option'].getValue().value == null) {
                        delete data['cancel_option']
                    }
        });
        if (!isValid) {
            Notifications.show(LM.getString('completeFields'), 'danger')
            return
        }
         Loader.show()
        
        if(/^data:image/.test(data.image) || /^http/.test(data.image)){
             data.image = null
        }
        data = objectToFormData(data)
        putSuppliersUpdate(this.state.loadedSupplier.id, data).then(
            res => {
                Loader.hide()
                Notifications.show( this.state.loadedSupplier.name + ' ' + LM.getString('updatedSuccessfully'), 'success')
            }
        ).catch(
            err => {
                Notifications.show(err)
            }
        );
            
    }

    /** TODO  add functionality */

    new = (e) => {
        this.setState({ creatingNew: true });
        window.scrollTo({
            top: 0,
            behavior: "smooth"
        });
        this.cleanPage()
        // this.supplierSelect.setValue("");
        this.supplierLoader("")
    }

    scroll(ref) {
        ref.current.scrollIntoView({ behavior: 'smooth' })
    }

    cancelNew = (e) => {
        // this.setState({ creatingNew: false }, () => { this.clearImage(); });
        this.cleanPage()
        this.setState({ creatingNew: false });
        window.scrollTo({
            top: 0,
            behavior: "smooth"
        });
    }
    cleanPage = (e) => {
        Object.keys(this.fields).filter(k=>this.fields[k]).forEach((key) => {
            try { this.fields[key].setValue(false); this.fields[key].setValue("") }
            catch { this.fields[key].setValue(false) }
        });
        Object.keys(this.fromObjFields).forEach((key) => {

            this.fromObjFields[key].child.setValue('')
        })
    }

    supplierLoader(e) {
        getSupplier(e.value).then((loadedSupplier) => {
            this.setState({
                renderFields: false,
            }, ()=>{

                if (loadedSupplier) {
                    this.setState({
                        loadedSupplier: loadedSupplier,
                        renderFields: true,
                    }, () => {
                        Object.keys(this.fields).filter(k=>this.fields[k]).forEach((key) => {


                            if (key === "status") {
                                if (this.state.loadedSupplier[key] === 1) {
                                    this.fields[key].setValue(true)
                                } else if (this.state.loadedSupplier[key] === 2) {
                                    this.fields[key].setValue(false)
                                }
                            } else {
                                if (key === "image") {
                                    if (this.state.loadedSupplier["image"] === null || this.state.loadedSupplier["image"] === undefined || this.state.loadedSupplier["image"] === "") {
                                    } else {
                                        this.fields["image"].setValue(config.baseUrl + "/" + this.state.loadedSupplier["image"]);
                                    }
                                }
                                else {
                                    if (this.state.loadedSupplier[key] != null || this.state.loadedSupplier[key] != undefined) {
                                        this.fields[key].setValue(this.state.loadedSupplier[key])
                                    } else {
                                        // this.fields[key].setValue(0)
                                    }
                                }
                            }
                        });

                        Object.keys(this.fromObjFields).forEach((key) => {
                            let newVal = "";
                            this.state[this.fromObjFields[key]["listObj"]].forEach(
                                (obj) => {
                                    
                                    if (obj.id === loadedSupplier[key]) {
                                        newVal = obj.name;
                                    }
                                }
                            )
                            this.fromObjFields[key].child.setValue(newVal)
                        })
                    })
                } else {
                    this.setState({
                        loadedSupplier: {}
                    }, () => {
                        Object.keys(this.fields).filter(k=>this.fields[k]).forEach((key) => {
                            this.fields[key].setValue("")
                        })
                        // this.fields["name"].setValue(this.state.loadedSupplier.name)
                    })
                }
            })
        })
    }

    loadSuppliersList(forceUpdate = false) {
        getSuppliers(forceUpdate).then(
            (res) => {
                this.setState({ suppliers: Object.keys(res).reduce((newArr, key) => [...newArr, ...res[key]], []) })
            }
        ).catch(
            (err) => {
                Notifications.show("loadSuppliersList failed: " + err)

            }
        )
    }

    loadCreditCardOptions() {
        getCreditCardOptions().then(
            (res) => {
                this.setState({ creditCardOptions: res })
            }
        ).catch(
            (err) => {
                Notifications.show("loadCreditCardOptions failed: " + err)
            }
        )
    }

    loadSupplierTypes() {
        getSupplierTypes().then(
            (res) => {
                this.setState({ supplierTypes: res })
            }
        ).catch(
            (err) => {
                Notifications.show("loadSupplierTypes failed: " + err)
            }
        )
    }

    componentDidMount() {
        this.loadSupplierTypes();
        this.loadCreditCardOptions();
        this.loadSuppliersList();
    }


    render() {
        return (
            <Container className="suppliersContainer">
                <Row>
                    <Col className="suppliersHeader">
                        {/* <h3 className="suppliersHeader"> */}
                        {LM.getString("suppliers") + (this.state.creatingNew ? ` - ${LM.getString("new")}` : "")}
                        {/* </h3> */}
                    </Col>
                </Row>
                <Container className="suppliersSettings">
                    <Collapse isOpen={!this.state.creatingNew}>
                        <Row>
                            <Col sm="4">
                                <InputTypeSimpleSelect
                                    id="slelctSupplier"
                                    title={LM.getString("supplierSelect")}
                                    options={this.state.suppliers.map(supplier => { return { value: supplier.id, label: supplier.name } })}
                                    onChange={this.supplierLoader}
                                    ref={(child) => this.supplierSelect = child}
                                />
                            </Col>
                        </Row>
                    </Collapse>
                    {this.state.renderFields && <React.Fragment>
                    <Container className="createProductInputBlock" >
                        <Row>
                            <Col sm='4' className='d-flex flex-column'>
                                <InputTypeText
                                    title={LM.getString("supplierName") + ":"}
                                    id="name"
                                    maxLength={20}
                                    default={this.state.loadedSupplier.name}
                                    ref={(child) => { this.fields["name"] = child }}
                                    required={true}
                                />
                            </Col>
                            <Col sm='4' className='d-flex flex-column'>
                                <InputTypeNumber
                                    title={LM.getString("supplierTelnumTitle")}
                                    id='contact_phone_number'
                                    ref={(child) => { this.fields["contact_phone_number"] = child }}
                                    required={true}
                                />
                            </Col>
                            <Col sm='4' className='d-flex flex-column'>
                                <InputTypeAddress
                                    title={LM.getString("supplierAdressTitle")}
                                    id='address'
                                    maxLength={40}
                                    ref={(child) => { this.fields["address"] = child }}
                                    required={true}
                                />
                            </Col>
                        </Row>
                    </Container>
                    <Row>
                        {/* // <Col sm='4' className='d-flex flex-column'>
                        //     <InputTypeText
                        //         title={LM.getString("supplierAdressTitle")}
                        //         id='oldAddress'
                        //         maxLength={40}
                        //         ref={(child) => { this.fields["old_address"] = child }}
                        //         required={true}
                        //     />
                        // </Col> */}

                    </Row>
                    <Row>
                        <Col sm='4' className='d-flex flex-column'>
                            <InputTypeSearchList
                                title={LM.getString("supplierType")}
                                id="type"
                                options={["", ...this.state.supplierTypes.map(obj => obj.name)]}
                                ref={(child) => { this.fromObjFields["type"] = { child: child, listObj: "supplierTypes" } }}
                                required={true}
                            />
                        </Col>
                        <Col sm='4' className='d-flex flex-column'>
                            <InputTypeNumber
                                title={LM.getString("order")}
                                id="order"
                                ref={(child) => { this.fields["order"] = child }}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm='4' className='d-flex flex-column'>
                            <InputTypeText
                                title={LM.getString("suppliercontact")}
                                id="contact_name"
                                maxLength={20}
                                ref={(child) => { this.fields["contact_name"] = child }}
                            />
                        </Col>
                    </Row>
                    <Row  >
                        <Col sm='3' className={'mt-2'}>
                            {/* <InputTypeCheckBox {...this.state.supplieractive} /> */}
                            <InputTypeCheckBox
                                lableText={LM.getString("active")}
                                id="status"
                                ref={(child) => { this.fields["status"] = child }}
                            />
                        </Col>

                        <Col sm='3' className={'mt-2'}>
                            {/* <InputTypeCheckBox {...this.state.supplierDefaultCreation} /> */}
                            <InputTypeCheckBox
                                lableText={LM.getString("supplierDefaultCreation")}
                                id="default_creation"
                                ref={(child) => { this.fields["default_creation"] = child }}
                            />
                        </Col>

                        <Col sm='3' className={'mt-2'}>
                            <InputTypeCheckBox
                                lableText={LM.getString("supplierCancelingPermissions")}
                                id="cancel_option"
                                ref={(child) => { this.fields["cancel_option"] = child }}
                            />
                        </Col>
                    </Row>

                    <Row>
                        <Col sm='8'>
                            <InputTypeSimpleSelect
                                title={LM.getString("suppliersCredidCard")}
                                id="credit_card_id"
                                options={this.state.creditCardOptions.map(obj => { return {label: obj.cc_company_name + " " + obj.last_4_numbers, value: obj.id}})}
                                ref={(child) => { this.fields["credit_card_id"] = child }}
                            />
                        </Col>
                    </Row>
                    <Container className="createProductInputBlock" >
                        <Row>
                            <Col sm='4' className='d-flex flex-column'>
                                <InputTypeNumber
                                    title={LM.getString("supplierMaximumCommission")}
                                    id="max_percentage_profit"
                                    ref={(child) => { this.fields["max_percentage_profit"] = child }}
                                />
                            </Col>
                            {[SupplierIdsEnum.IECBILL,SupplierIdsEnum.IECMATAM].indexOf(this.state.loadedSupplier.id) == -1 && <Col sm='4' className='d-flex flex-column'>
                                <InputTypeNumber
                                    title={LM.getString("supplierMinimumCommission")}
                                    id="min_percentage_profit"
                                    ref={(child) => { this.fields["min_percentage_profit"] = child }}
                                />
                            </Col>}
                            {/* </Row>
                    <Row> */}
                            <Col sm='4' className='d-flex flex-column'>
                                <InputTypeNumber
                                    title={LM.getString("supplierMaximumAmountPayable")}
                                    id="max_payment"
                                    ref={(child) => { this.fields["max_payment"] = child }}
                                />
                            </Col>
                        </Row>
                    </Container>
                    <Row>
                        <Col sm="8">
                            <ImageUpload
                                title={LM.getString("supplierLogo")}
                                id='imageUploadId'
                                onChange={() => { this.setState({ imageChanged: true }) }}
                                ref={(componentObj) => { this.fields["image"] = componentObj }} />
                        </Col>
                    </Row>
                    <Container className="createProductInputBlock" >
                        <Row>
                            <Col className="subHeader">
                                {LM.getString('colors') + ':'}
                            </Col>
                        </Row>
                        <br />
                        <Row>
                            <Col sm="4">
                                <InputTypeColor
                                    id="fontColor"
                                    title={LM.getString("fontColor")}
                                    ref={(componentObj) => { this.fields["font_color"] = componentObj }}
                                />
                            </Col>
                            <Col sm="4">
                                <InputTypeColor
                                    id="logoBackgroundColor"
                                    title={LM.getString("logoBackground")}
                                    ref={(componentObj) => { this.fields["logo_background_color"] = componentObj }}
                                />
                            </Col>
                        </Row>
                        <br />
                        <Row >
                            <Col sm="4">
                                <InputTypeColor
                                    id="productCardBackgroundColor"
                                    title={LM.getString("productCardBackground")}
                                    ref={(componentObj) => { this.fields["product_card_background_color"] = componentObj }}
                                />
                            </Col>
                            <Col sm="4" >
                                <InputTypeColor
                                    id="chosenCardBackgroundColor"
                                    title={LM.getString("chosenCardBackground")}
                                    ref={(componentObj) => { this.fields["chosen_card_background_color"] = componentObj }}
                                />
                            </Col>
                        </Row>
                        <br />
                    </Container>
                    <Row style={{ marginTop: '15px' }} >
                        <Col sm="auto">
                            <Button
                                id='supplierSave'
                                onClick={this.save}
                                className="suppliersButton"
                            >
                                {LM.getString("save")}
                            </Button>
                        </Col>
                        {/*<Col sm="auto">
                            <Button
                                id='supplierNew'
                                onClick={this.new}
                                hidden={this.state.creatingNew}
                                className="suppliersButton suppliersButtonDark"
                            >
                                {LM.getString("new")}
                            </Button>
                        </Col>*/}
                        <Col sm="auto">
                            <Button
                                id='supplierCancelNew'
                                onClick={this.cancelNew}
                                hidden={!this.state.creatingNew}
                                className="suppliersButton suppliersButtonDark"
                            >
                                {LM.getString("cancel")}
                            </Button>
                        </Col>
                    </Row>
                    </React.Fragment>}
                </Container>
            </Container>
        )
    }

}
