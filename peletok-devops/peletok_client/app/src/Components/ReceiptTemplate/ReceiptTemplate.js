import React, { Component } from 'react';
import { LanguageManager as LM } from '../LanguageManager/Language';
import './ReceiptTemplate.css'


export class ReceiptTemplate extends Component {

    render() {

        let transId = this.props.transId || "";
        let transDateTime = this.props.transDateTime || "";
        let operationPerformer = this.props.operationPerformer || "";
        let targetPhone = this.props.targetPhone || "";
        let provider = this.props.provider || "";
        let product = this.props.product || "";
        let price = this.props.price || "";
        let cardNumber = this.props.cardNumber;
        let codeNumber = this.props.codeNumber;
        let expiryDate = this.props.expiryDate;

        return (
            <React.Fragment>
                <div className={"receipt-container"}>
                    <h2 className={"receipt-logo"}>פלאטוק</h2>
                    <h1 className={"receipt-header"}>{LM.getString("successModalMessage")}!</h1>
                    <div className={"receipt-properties"}>
                        <b>{LM.getString("transId")}</b> {transId} <br />
                        <b>{LM.getString("date")}</b> {transDateTime} <br />
                        <b>{LM.getString("operationPerformer")}</b> {operationPerformer} <br />
                        <b>{LM.getString("targetPhone")}</b> {targetPhone}
                    </div>
                    <table>
                        <colgroup>
                            <col className="tbcl smcl" />
                            <col className="tbcl lgcl" />
                            <col className="tbcl smcl" />
                        </colgroup>
                        <thead>
                            <tr>
                                <th>{LM.getString("provider")}</th>
                                <th>{LM.getString("product")}</th>
                                <th>{LM.getString("price")}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{provider}</td>
                                <td>{product}</td>
                                <td>{price}</td>
                            </tr>
                        </tbody>
                    </table>
                    <div className={"receipt-properties"}>
                        <b> {cardNumber ? LM.getString("cardNumber") : null}</b> {cardNumber} <br />
                        <b> {codeNumber ? LM.getString("code") : null}</b> {codeNumber} <br />
                        <b> {expiryDate ? LM.getString("expiryDate") : null}</b> {expiryDate} <br />
                    </div>
                    <div className={"receipt-footer"}>
                        <h4 >{LM.getString("pleaseSaveReceipt")}</h4>
                        <h4 >{LM.getString("thanksAndGB")}</h4>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}