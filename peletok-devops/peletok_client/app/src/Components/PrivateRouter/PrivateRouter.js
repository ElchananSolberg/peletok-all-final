import React from 'react'
import {
  Route,
  Redirect,
} from 'react-router-dom'
import {AuthService} from '../../services/authService'
import { Notifications } from '../Notifications/Notifications'
import { LanguageManager as LM } from '../LanguageManager/Language';

export const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={(props) => (
    AuthService.allowed(props.match.url) === true
      ? <Component {...props} />
      :  Notifications.show(LM.getString('not_allowed'), 'warning') &&
              <Redirect to={{
              pathname: '/main/charge',
              state: { from: props.location }
            }} />
        
  )} />
)