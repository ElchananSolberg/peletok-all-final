import React from 'react'
import {Container, Navbar, NavbarBrand} from 'reactstrap';
import './FooterBar.css'
import { LanguageManager as LM } from '../LanguageManager/Language';


export class FooterBar extends React.Component {
    render() {
        return (
            <Navbar className={'bc-blue-2 p-md-4'}>
                <Container fluid style={{width: "90%", maxWidth: '1300px'}} className={'py-3 fs-12'}>
                <p className={'m-0'}> {LM.getString("support")}</p>
                    <p className={'m-0'}>Created by <a color='white' href="http://ravtech.co.il/">RavTech</a></p>
                </Container>
            </Navbar>
        )
    }
}
