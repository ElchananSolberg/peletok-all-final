import React from 'react';
import { Row, Col, Button, Container } from 'reactstrap';

import './Sellers_Suppliers.css';

import { LanguageManager as LM } from '../LanguageManager/Language';
import { Notifications } from '../Notifications/Notifications';
import { Loader } from '../Loader/Loader';

import { getBusinessSupplierAuthorizationDueToUser, getBusinessSupplierAuthorizationDueToBusiness, postBusinessSuppliers } from '../DataProvider/DataProvider'

import CommissionProfileTable from '../CommissionProfile/CommissionProfileTable';
import { SelectedSeller } from '../SelectedSeller/SelectedSeller';

/**
 * Sellers_Suppliers is a main component of Suppliers Page (in Resellers menu).  Sellers_Suppliers and its children are working with separate .css file: Sellers_Suppliers.css.
 */

export class Sellers_Suppliers extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            /** working variable that will contain an array of suppliers from the server */
            userSuppliers: [],
            businessSuppliers: [],
            /** working variable that will contain a selected seller object */
            selectedSeller: {},
            /** tableRows - an array of objects for <CommissionProfileTable/> component. The values of the objects are the data of the rows of a table */
            tableRows: [],
        };

    }
    /** "Save" button click handler */
    buttonSaveClick = (e) => {
        if (SelectedSeller.currentSeller.getValue().id) {
            let suppliersList = this.state.userSuppliers.filter(s=>this.state.businessSuppliers.find(bs => bs.supplier_id == s.supplier_id)).map((supplier, index = 0) => {
                let tableRowCheckBoxChecked = this.commissionProfileTable.tableRows['tableRow' + index].tableRowCheckBox.props.checked;
                return { "id": supplier.supplier_id, "is_authorized": tableRowCheckBoxChecked };
            })
            let cleanedFromNullSuppliersList = suppliersList.filter(element => element !== null);
            let businessSupplierData = {
                "business_id": this.state.selectedSeller.id,
                "suppliers_list": cleanedFromNullSuppliersList,
            };
            if (cleanedFromNullSuppliersList[0]) {
                Loader.show();
                postBusinessSuppliers(businessSupplierData).then(
                    res => {
                        Loader.hide();
                        Notifications.show(LM.getString("successSellerUpdating") + this.state.selectedSeller.name, 'success');
                    }
                ).catch(
                    err => {
                        Loader.hide();
                        Notifications.show(err, 'danger');
                    }
                );
            }
            else Notifications.show(LM.getString("noChanges"), 'warning');
        }
        else Notifications.show(LM.getString("sellerNotSelectedError"), 'warning');
    }

    /** Function that loads from a server a new data for the table due to selected Seller. */
    fieldsLoad = () => {
        let selectedSellerId = this.state.selectedSeller.id;

        getBusinessSupplierAuthorizationDueToBusiness(selectedSellerId).then(
            (res) => {
                this.setState({ businessSuppliers: res }, () => this.loadSuppliersDataIntoTable());
            }
        ).catch(
            (err) => {
                Notifications.show(LM.getString('loadSuppliersListFailed') + ' ' + err, 'danger');
            }
        )
    }
    /** Function that enters a data into the table. */
    loadSuppliersDataIntoTable = () => {
        let tableRows = this.state.userSuppliers.map(s => { 
            let supplier = this.state.businessSuppliers.find(bs => bs.supplier_id == s.supplier_id)
            return supplier ? { 'supplierName': supplier.supplier_name, 'checkbox': supplier.is_authorized } : false
        }).filter(s=>s);
        this.setState({ tableRows: tableRows })
    }

    componentDidMount() {
        SelectedSeller.setHeader.next(LM.getString('suppliers'));
        if (SelectedSeller.newSellerState) SelectedSeller.createSellerClicked.next();
        if (SelectedSeller.newDistributorState) SelectedSeller.createDistributorClicked.next();
        if (SelectedSeller.currentSeller.getValue().id) this.setState({ selectedSeller: SelectedSeller.currentSeller.getValue() }, () => this.fieldsLoad());
            getBusinessSupplierAuthorizationDueToUser().then(
                (res) => {
                    this.setState({ userSuppliers: res }, () => this.loadSuppliersDataIntoTable());
                }
            ).catch(
                (err) => {
                    Notifications.show(LM.getString('loadSuppliersListFailed') + ' ' + err, 'danger');
                }
            )

        SelectedSeller.currentSeller.subscribe(x => {
            if(x.id){
                this.setState({ selectedSeller: x },() => {
                    this.fieldsLoad()
                })
            }
        });
        
        SelectedSeller.currentDistributor.subscribe(x => {
            if(x.id){
                this.setState({ selectedDistributor: x })
            }
        })
    }

    render() {
        /** tableHeadersObj - an object for <CommissionProfileTable/> component. The values of the object are the HEADERS of a table */
        const tableHeadersObj =
        {
            supplierName: LM.getString('supplierName'),
            authorizedUser: LM.getString('authorizedUser'),
        };

        return (
            <Container className="sellersSuppliersContainer">
                {SelectedSeller.currentSeller.getValue().id ?
                    <Container className="sellersSuppliersSettings">
                        <Row>
                            <Col sm='8'>
                                <CommissionProfileTable
                                    tableRows={this.state.tableRows}
                                    tableHeadersObj={tableHeadersObj}
                                    tableFootersObj={{}}
                                    ref={(componentObj) => {
                                        this.commissionProfileTable = componentObj
                                    }}
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Button
                                    className="sellersSuppliersButton"
                                    id="supplierSave"
                                    onClick={this.buttonSaveClick}
                                >
                                    {LM.getString("save")}
                                </Button>
                            </Col>
                        </Row>
                    </Container>
                    : <></>
                }
            </Container>
        );
    }
}
