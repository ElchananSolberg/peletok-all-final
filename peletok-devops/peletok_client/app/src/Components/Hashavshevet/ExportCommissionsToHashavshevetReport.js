import React, { Component } from 'react';
import './Hashavshevet.css';

import { Container, Row, Col, Button, Collapse, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

import { LanguageManager as LM } from '../LanguageManager/Language';

import { getSellers } from '../DataProvider/DataProvider'

import InputTypeSearchList from '../InputUtils/InputTypeSearchList';
import InputTypeNumber from '../InputUtils/InputTypeNumber';
import InputTypeDate from '../InputUtils/InputTypeDate';
import InputTypeTime from '../InputUtils/InputTypeTime';
import InputTypeText from '../InputUtils/InputTypeText';

import { ReportTableContent } from '../ReportComponent/ReportTableContent';

/** ExportCommissionsToHashavshevetReport is a main component of ExportCommissionsToHashavshevetReport Page.  
 * ExportCommissionsToHashavshevetReport and its children are working with .css file: Hashavshevet.css - common for all Hashavshevet Reports Pages. */
class ExportCommissionsToHashavshevetReport extends Component {
    constructor(props) {
        super(props);

        this.state = {
            /** working variable that will contain an array (of objects) of all the sellers */
            sellers: [],
            /** working variable that indicates if a user is on the stage of viewing a report */
            viewingReport: false,
            /** working variable that indicates if a user is on the stage of viewing a Commission Details table */
            viewingCommissionDetails: false,
            /** Commissions table data - TODO will be CHANGED by a data from server */
            commissionsTableRows: [
                {
                    forExport: 'forExport',
                    sellerName: 'sellerName',
                    terminalNumber: 'terminalNumber',
                    hashavshevetCustomerCode: 'hash.CustomerCode',
                    totalPayment: 'totalPayment',
                    balance: 'balance',
                    vat: 'VAT',
                    earliestDateInCommission: 'earliest DateIn Commission',
                    latestDateInCommission: 'latest DateIn Commission',
                },
                {
                    forExport: 'forExport',
                    sellerName: 'sellerName',
                    terminalNumber: 'terminalNumber',
                    hashavshevetCustomerCode: 'hash.CustomerCode',
                    totalPayment: 'totalPayment',
                    balance: 'balance',
                    vat: 'VAT',
                    earliestDateInCommission: 'earliest DateIn Commission',
                    latestDateInCommission: 'latest DateIn Commission',
                },
            ],
            /** Commission Details table data - TODO will be CHANGED by a data from server */
            commissionDetailsTableRows: [
                {
                    itemName: 'itemName',
                    hashavshevetItemNumber: 'hashavshevetItemNumber',
                    pricePerUnit: 'pricePerUnit',
                    quantity: 'quantity',
                    total: 'total'
                },
            ],
            /** working variable that indicates if a user is on the stage of viewing a Export Commissions Modal window */
            modal: false,
        }
    }

    /** TODO: Add functionality if selecting of a seller should affect other fields */
    sellerChanged = (e) => {

    }

    /** Show Report Button click handler
     * TODO: Add functionality */
    showReportClicked = () => {
        this.setState({ viewingReport: true });
    }

    /** "For Export" checkbox change handler
    * TODO: Add functionality */
    forExportChanged = (e) => {
        // e.target.id;
        // e.target.checked;
    }

    /** Commissions Table Row click handler
     * TODO: Add functionality: 
     *                          - due to e.target.parentElement.id send a GET request
     *                          - fill the Commission Details Table with the data from the response (setState({commissionDetailsTableRows:res}) ).
     */
    trClicked = (e) => {
        if (['TD', 'TR'].includes(e.target.tagName)) {
            // e.target.parentElement.id;

            this.setState({ viewingCommissionDetails: true });
            e.persist();
        }
    }

    /** Export Commissions Button click handler
     * TODO: Add functionality */
    exportCommissionsClicked = () => {
        this.toggleExportCommissionsModal();
        setTimeout(() => {
            this.exportCommissionsFileName.setValue("ExportCommissionsToHashavshevetReport");
        }, 500)
    }
    /** toggle Export Commissions Modal window */
    toggleExportCommissionsModal = () => {
        this.setState({ modal: !this.state.modal });
    }
    /** Export Commissions Button inside Export Commissions Modal window click handler
     * TODO: Add functionality */
    finalExportCommissionsClicked = () => {
        this.toggleExportCommissionsModal();
    }

    componentDidMount() {
        getSellers(true)
            .then(
                res => {
                    this.setState({ sellers: res });
                }
            )
            .catch((err) => alert(err));
    }


    render() {
        /** Payments Header  */
        const header = LM.getString('exportCommissionsToHashavshevetReport');

        /** data for "Select Seller" field (InputTypeSearchList InputUtils component) */
        const selectSellerProps = {
            title: LM.getString("selectSeller"),
            id: "ExportCommissionsToHashavshevetSelectSeller",
        };

        /** data for "Agent" field (InputTypeNumber InputUtils component) */
        const agentProps = {
            title: LM.getString("agent"),
            id: "OrdersExportedToHashavshevet_Agent",
        };
        /** data for "Terminal Number" field (InputTypeNumber InputUtils component) */
        const terminalNumberProps = {
            title: LM.getString("terminalNumber"),
            id: "ExportCommissionsToHashavshevetTerminalNumber",
        };

        /** data for "Starting Date" field (InputTypeDate InputUtils component) */
        const startingDateProps = {
            title: LM.getString("startingDate"),
            id: "ExportCommissionsToHashavshevetStartingDate",
        };
        /** data for "Ending Date" field (InputTypeDate InputUtils component) */
        const endingDateProps = {
            title: LM.getString("endingDate"),
            id: "ExportCommissionsToHashavshevetEndingDate",
        };

        /** data for "Starting Time" field (InputTypeDate InputUtils component) */
        const startingTimeProps = {
            title: LM.getString("startingTime"),
            id: "ExportCommissionsToHashavshevetStartingTime",
        };
        /** data for "Ending Time" field (InputTypeDate InputUtils component) */
        const endingTimeProps = {
            title: LM.getString("endingTime"),
            id: "ExportCommissionsToHashavshevetEndingTime",
        };

        /** data for "File Name" field (InputTypeFile InputUtils component) */
        const fileNameProps = {
            title: LM.getString("fileName") + ":",
            id: "ExportCommissionsToHashavshevetFileName",
        };

        /** Table Header data */
        const tableHeadersObj = {
            forExport: LM.getString('forExport'),
            sellerName: LM.getString('sellerName'),
            terminalNumber: LM.getString('terminalNumber2'),
            hashavshevetCustomerCode: LM.getString('hashavshevetCustomerCode'),
            totalPayment: LM.getString('totalPayment2'),
            balance: LM.getString('balance2'),
            vat: LM.getString('vat'),
            earliestDateInCommission: LM.getString('earliestDateInCommission'),
            latestDateInCommission: LM.getString('latestDateInCommission'),
        };
        /** Commission Details Table Header data */
        const commissionDetailsTableHeadersObj = {
            itemName: LM.getString('itemName'),
            hashavshevetItemNumber: LM.getString('hashavshevetItemNumber'),
            pricePerUnit: LM.getString('pricePerUnit'),
            quantity: LM.getString('quantity'),
            total2: LM.getString('total2'),
        };

        /** data for Show Report button */
        const showReportButtonText = LM.getString("showReport");
        /** data for Export Commissions button */
        const exportCommissionsButtonText = LM.getString("exportCommissions");
        /** data for Cancel button */
        const cancelButtonText = LM.getString("cancel");
        /** data for Commissions sub header */
        const commissionsHeaderText = LM.getString('commissions');
        /** data for Commission Details sub header */
        const commissionDetailsHeaderText = LM.getString('commissionDetails');

        return (
            <Container className="hashavshevetContainer">
                <Row >
                    <Col className="hashavshevetHeader">
                        {header}
                    </Col>
                </Row>
                <Container className="hashavshevetSettings">
                    <Row>
                        <Col sm='4'>
                            <InputTypeSearchList {...selectSellerProps} onChange={this.sellerChanged}
                                options={this.state.sellers.map(seller => seller.name)}
                                ref={(componentObj) => { this.selectSeller = componentObj }} />
                        </Col>
                    </Row>
                    <Row >
                        <Col sm='4'>
                            <InputTypeNumber {...agentProps} ref={(componentObj) => { this.agent = componentObj }} />
                        </Col>
                        <Col sm='4'>
                            <InputTypeNumber {...terminalNumberProps} ref={(componentObj) => { this.terminalNumber = componentObj }} />
                        </Col>
                    </Row>
                    <Row >
                        <Col sm='4'>
                            <InputTypeDate {...startingDateProps} ref={(componentObj) => { this.startingDate = componentObj }} />
                        </Col>
                        <Col sm='4'>
                            <InputTypeTime {...startingTimeProps} ref={(componentObj) => { this.startingTime = componentObj }} />
                        </Col>
                    </Row>
                    <Row >
                        <Col sm='4'>
                            <InputTypeDate {...endingDateProps} ref={(componentObj) => { this.endingDate = componentObj }} />
                        </Col>
                        <Col sm='4'>
                            <InputTypeTime {...endingTimeProps} ref={(componentObj) => { this.endingTime = componentObj }} />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="auto">
                            <Button className="hashavshevetButton" id="hashavshevetShowReportButton" onClick={this.showReportClicked} > {showReportButtonText} </Button>
                        </Col>
                    </Row>
                </Container>
                <Collapse isOpen={this.state.viewingReport} >
                    <Row >
                        <Col className="subHeader">
                            {commissionsHeaderText}
                        </Col>
                    </Row>
                    <Row>
                        <Col >
                            <ReportTableContent
                                tableHeadersObj={tableHeadersObj}
                                tableRows={this.state.commissionsTableRows}
                                reportName={"ExportCommissionsToHashavshevetReport"}
                                trClick={this.trClicked}
                                inputTypeCheckBoxOnChange={this.forExportChanged}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="auto">
                            <Button className="hashavshevetButton" id="hashavshevetExportCommissionsButton" onClick={this.exportCommissionsClicked} > {exportCommissionsButtonText} </Button>
                        </Col>
                        <Modal isOpen={this.state.modal} toggle={this.toggleExportCommissionsModal} >
                            <ModalHeader className="hashavshevetModalHeader" toggle={this.toggle}>{exportCommissionsButtonText}</ModalHeader>
                            <ModalBody className="hashavshevetModalBody" >
                                <Row>
                                    <Col >
                                        <InputTypeText {...fileNameProps} ref={(componentObj) => { this.exportCommissionsFileName = componentObj }} />
                                    </Col>
                                </Row>
                            </ModalBody>
                            <ModalFooter className="hashavshevetModalFooter">
                                <Row>
                                    <Col sm="auto">
                                        <Button className="hashavshevetButton" onClick={this.finalExportCommissionsClicked}>{exportCommissionsButtonText}</Button>{' '}
                                    </Col>
                                    <Col sm="auto">
                                        <Button className="hashavshevetButton hashavshevetButtonCancel" onClick={this.toggleExportCommissionsModal}>{cancelButtonText}</Button>
                                    </Col>
                                </Row>
                            </ModalFooter>
                        </Modal>
                    </Row>
                </Collapse>
                <Collapse isOpen={this.state.viewingReport && this.state.viewingCommissionDetails} >
                    <Row >
                        <Col className="subHeader">
                            {commissionDetailsHeaderText}
                        </Col>
                    </Row>
                    <Row>
                        <Col >
                            <ReportTableContent
                                tableHeadersObj={commissionDetailsTableHeadersObj}
                                tableRows={this.state.commissionDetailsTableRows}
                                reportName={"CommissionDetails"}
                            />
                        </Col>
                    </Row>
                </Collapse>
            </Container>
        )
    }
}

export default ExportCommissionsToHashavshevetReport;