import React, { Component } from 'react';
import './Hashavshevet.css';

import { Container, Row, Col, Button, Collapse } from 'reactstrap';

import { LanguageManager as LM } from '../LanguageManager/Language';

import { getSellers } from '../DataProvider/DataProvider'

import InputTypeSearchList from '../InputUtils/InputTypeSearchList';
import InputTypeNumber from '../InputUtils/InputTypeNumber';
import InputTypeDate from '../InputUtils/InputTypeDate';
import InputTypeTime from '../InputUtils/InputTypeTime';

import { ReportTableContent } from '../ReportComponent/ReportTableContent';

/** OrdersExportedToHashavshevetReport is a main component of OrdersExportedToHashavshevetReport Page.  
 * OrdersExportedToHashavshevetReport and its children are working with .css file: Hashavshevet.css - common for all Hashavshevet Reports Pages. */
class OrdersExportedToHashavshevetReport extends Component {
    constructor(props) {
        super(props);

        this.state = {
            /** working variable that will contain an array (of objects) of all the sellers */
            sellers: [],
            /** working variable that indicates if a user is on the stage of viewing a report */
            viewingReport: false,
            /** working variable that indicates if a user is on the stage of viewing a Order Details table */
            viewingOrderDetails: false,
            /** Orders table data */
            ordersTableRows: [
                {
                    orderCode: 'order Code',
                    sellerName: 'seller Name',
                    terminalNumber: 'terminal Number',
                    hashavshevetCustomerCode: 'hash. CustomerCode',
                    totalPayment: 'total Payment',
                    balance: 'balance',
                    vat: 'VAT',
                    earliestDateInOrder: 'earliest Date',
                    latestDateInOrder: 'latest Date',
                    dateOfExport: 'dateOf Export',
                    invoiceNumber: 'invoice Number',
                    dateOfImport: 'dateOf Import',
                },
                {
                    orderCode: 'order Code',
                    sellerName: 'seller Name',
                    terminalNumber: 'terminal Number',
                    hashavshevetCustomerCode: 'hash. CustomerCode',
                    totalPayment: 'total Payment',
                    balance: 'balance',
                    vat: 'VAT',
                    earliestDateInOrder: 'earliest Date',
                    latestDateInOrder: 'latest Date',
                    dateOfExport: 'dateOf Export',
                    invoiceNumber: 'invoice Number',
                    dateOfImport: 'dateOf Import',
                },
            ],
            /** Order Details table data */
            orderDetailsTableRows: [
                {
                    itemName: 'itemName',
                    hashavshevetItemNumber: 'hashavshevetItemNumber',
                    pricePerUnit: 'pricePerUnit',
                    quantity: 'quantity',
                    total: 'total'
                },
            ],

        }
    }

    /** TODO: Add functionality if selecting of a seller should affect other fields */
    sellerChanged = (e) => {

    }

    /** Show Report Button click handler
     * TODO: Add functionality */
    showReportClicked = () => {
        this.setState({ viewingReport: true });
    }

    /** Orders Table Row click handler
     * TODO: Add functionality: 
     *                          - due to e.target.parentElement.id send a GET request
     *                          - fill the Order Details Table with the data from the response (setState({orderDetailsTableRows:res}) ).
     */
    trClicked = (e) => {
        if (['TD', 'TR'].includes(e.target.tagName)) {
            // e.target.parentElement.id;

            this.setState({ viewingOrderDetails: true });
            e.persist();
        }
    }


    componentDidMount() {
        getSellers(true)
            .then(
                res => {
                    this.setState({ sellers: res });
                }
            )
            .catch((err) => alert(err));
    }


    render() {
        /** Payments Header  */
        const header = LM.getString('ordersExportedToHashavshevetReport');

        /** data for "Select Seller" field (InputTypeSearchList InputUtils component) */
        const selectSellerProps = {
            title: LM.getString("selectSeller"),
            id: "OrdersExportedToHashavshevet_SelectSeller",
        };

        /** data for "Agent" field (InputTypeNumber InputUtils component) */
        const agentProps = {
            title: LM.getString("agent"),
            id: "OrdersExportedToHashavshevet_Agent",
        };
        /** data for "Terminal Number" field (InputTypeNumber InputUtils component) */
        const terminalNumberProps = {
            title: LM.getString("terminalNumber"),
            id: "OrdersExportedToHashavshevet_TerminalNumber",
        };

        /** data for "Starting Date" field (InputTypeDate InputUtils component) */
        const startingDateProps = {
            title: LM.getString("startingDate"),
            id: "OrdersExportedToHashavshevet_StartingDate",
        };
        /** data for "Ending Date" field (InputTypeDate InputUtils component) */
        const endingDateProps = {
            title: LM.getString("endingDate"),
            id: "OrdersExportedToHashavshevet_EndingDate",
        };

        /** data for "Starting Time" field (InputTypeDate InputUtils component) */
        const startingTimeProps = {
            title: LM.getString("startingTime"),
            id: "OrdersExportedToHashavshevet_StartingTime",
        };
        /** data for "Ending Time" field (InputTypeDate InputUtils component) */
        const endingTimeProps = {
            title: LM.getString("endingTime"),
            id: "OrdersExportedToHashavshevet_EndingTime",
        };

        /** Orders table header data */
        const ordersTableHeadersObj = {
            orderCode: LM.getString('orderCode'),
            sellerName: LM.getString('sellerName'),
            terminalNumber: LM.getString('terminalNumber2'),
            hashavshevetCustomerCode: LM.getString('hashavshevetCustomerCode'),
            totalPayment: LM.getString('totalPayment2'),
            balance: LM.getString('balance2'),
            vat: LM.getString('vat'),
            earliestDateInOrder: LM.getString('earliestDateInOrder'),
            latestDateInOrder: LM.getString('latestDateInOrder'),
            dateOfExport: LM.getString('dateOfExport'),
            invoiceNumber: LM.getString('realInvoiceNumber'),
            dateOfImport: LM.getString('dateOfImport'),
        };
        /** Order Details table header data */
        const orderDetailsTableHeadersObj = {
            itemName: LM.getString('itemName'),
            hashavshevetItemNumber: LM.getString('hashavshevetItemNumber'),
            pricePerUnit: LM.getString('pricePerUnit'),
            quantity: LM.getString('quantity'),
            total2: LM.getString('total2'),
        };

        /** data for Show Report button */
        const showReportButtonText = LM.getString("showReport");
        /** data for Orders sub header */
        const ordersHeaderText = LM.getString('orders');
        /** data for Order Details sub header */
        const orderDetailsHeaderText = LM.getString('orderDetails');

        return (
            <Container className="hashavshevetContainer">
                <Row >
                    <Col className="hashavshevetHeader">
                        {header}
                    </Col>
                </Row>
                <Container className="hashavshevetSettings">
                    <Row>
                        <Col sm='4'>
                            <InputTypeSearchList {...selectSellerProps} onChange={this.sellerChanged}
                                options={this.state.sellers.map(seller => seller.name)}
                                ref={(componentObj) => { this.selectSeller = componentObj }} />
                        </Col>
                    </Row>
                    <Row >
                        <Col sm='4'>
                            <InputTypeNumber {...agentProps} ref={(componentObj) => { this.agent = componentObj }} />
                        </Col>
                        <Col sm='4'>
                            <InputTypeNumber {...terminalNumberProps} ref={(componentObj) => { this.terminalNumber = componentObj }} />
                        </Col>
                    </Row>
                    <Row >
                        <Col sm='4'>
                            <InputTypeDate {...startingDateProps} ref={(componentObj) => { this.startingDate = componentObj }} />
                        </Col>
                        <Col sm='4'>
                            <InputTypeTime {...startingTimeProps} ref={(componentObj) => { this.startingTime = componentObj }} />
                        </Col>
                    </Row>
                    <Row >
                        <Col sm='4'>
                            <InputTypeDate {...endingDateProps} ref={(componentObj) => { this.endingDate = componentObj }} />
                        </Col>
                        <Col sm='4'>
                            <InputTypeTime {...endingTimeProps} ref={(componentObj) => { this.endingTime = componentObj }} />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="auto">
                            <Button className="hashavshevetButton" id="hashavshevetShowReportButton" onClick={this.showReportClicked} > {showReportButtonText} </Button>
                        </Col>
                    </Row>
                </Container>
                <Collapse isOpen={this.state.viewingReport} >
                    <Row >
                        <Col className="subHeader">
                            {ordersHeaderText}
                        </Col>
                    </Row>
                    <Row>
                        <Col >
                            <ReportTableContent
                                tableHeadersObj={ordersTableHeadersObj}
                                tableRows={this.state.ordersTableRows}
                                reportName={"OrdersExportedToHashavshevetReport"}
                                trClick={this.trClicked}
                            />
                        </Col>
                    </Row>
                </Collapse>
                <Collapse isOpen={this.state.viewingReport && this.state.viewingOrderDetails} >
                    <Row >
                        <Col className="subHeader">
                            {orderDetailsHeaderText}
                        </Col>
                    </Row>
                    <Row>
                        <Col >
                            <ReportTableContent
                                tableHeadersObj={orderDetailsTableHeadersObj}
                                tableRows={this.state.orderDetailsTableRows}
                                reportName={"OrderDetails"}
                            />
                        </Col>
                    </Row>
                </Collapse>

            </Container>
        )
    }
}

export default OrdersExportedToHashavshevetReport;