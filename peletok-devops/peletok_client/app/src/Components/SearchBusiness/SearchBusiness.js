import React, { Component } from 'react';

import { Button, Collapse, Container, Row, Col, Spinner } from 'reactstrap';

import { LanguageManager as LM } from '../LanguageManager/Language';
import { Notifications } from '../Notifications/Notifications'
import { Loader } from '../Loader/Loader';
import { SelectedSeller } from '../SelectedSeller/SelectedSeller';

import { getDistributors, getSellers, getSellersByDistributorId, getCurrentUserData } from '../DataProvider/DataProvider'

import InputTypeSearchList from '../InputUtils/InputTypeSearchList';
import InputTypeNumber from '../InputUtils/InputTypeNumber';
import InputTypePhone from '../InputUtils/InputTypePhone';
import InputTypeSimpleSelect from '../InputUtils/InputTypeSimpleSelect';


/**
 * Main component of SearchBusiness screen.  
 */
class SearchBusiness extends Component {

    constructor(props) {
        super(props);

        this.state = {
            state: 'loading',
            renderNumbersFields: true,
            /** Screen Header  */
            header: '',
            isEditingClientNumber: false,
            isEditingPhoneNumber: false,
            isEditingAuthorizedDealer: false,

            /** working variable for a "Search" button click handler  */
            searching: false,
            /** working variable that will contain an array of distributors from the server */
            distributors: [],
            /** working variable that will contain a selected distributor object */
            selectedDistributor: {},
            /** working variable that will contain a selected distributor id */
            distributorID: -1,
            /** working variable that will contain an array (of objects) of all the selected distributor sellers */
            sellers: [],
            /** working variable that will contain a selected seller object */
            selectedSeller: {},
            /** working variable that indicates if a user is on the stage of creating a new seller */
            creatingNew: false,
            /** working variable that indicates if a user is on the stage of creating a new distributor */
            creatingNewDistributor: false,
        }
        this.searchFields = {};
    }

    refreshSellers = (newSellerId = false) => {
        getSellersByDistributorId(this.state.distributorID, {}, true).then(
            (res) => {
                this.setState({ sellers: res }, () => {
                    if (newSellerId) {
                        let newCreatedSeller = this.state.sellers.find(seller => (seller.id === newSellerId));
                        this.selectSeller.setValue(newSellerId)
                        this.sellerChange({value: newSellerId})
                    }
                });
            }).catch(
                (err) => {
                    Notifications.show(err, 'danger');
                }
            )
    }

    /** TODO  ADD functionality.
     * "Select Distributor" field onChange handler.
     * Saves the selected distributor id into distributorID variable.
     * Opens the GET html request (get sellers by distributor id from DataProvider).
     * Saves an array from the server (list of sellers of the selected distributor) into sellers variable.
     */
    distributorChange = (e, changeSellersList = true) => {
        return new Promise((resolve, reject)=>{
                
                let selectedDistributor = this.state.distributors.find(d => (d.id == e.value));

                this.setState({ 
                    selectedDistributor,
                    distributorID: selectedDistributor.distributor_id 
                }, () => {        
                SelectedSeller.setDistributor(selectedDistributor);
                this.selectSeller.setValue("")
                if(!e.label){
                    this.selectDistributor.setValue(e.value)
                }

                if(changeSellersList){
                    window.localStorage.setItem('seller', '')
                    getSellersByDistributorId(this.state.distributorID, {}, true).then((res) => {
                        this.setState({ sellers: res });
                        resolve(res)
                    }).catch(
                        (err) => {
                            Notifications.show(err, 'danger');
                            reject(err)
                        }
                    )
                        
                } else {
                    resolve()
                }
            })
        })
    }
    /** TODO  ADD functionality 
     * "Select Seller" field onChange handler 
     * Due to the selected seller name it saves selected seller object into selectedSeller variable.
    */
    sellerChange = (e) => {
        return new Promise((resolve, reject) => {
            let selectedSeller = this.state.sellers.find(s => (s.id ==  e.value));
            if(!e.label){
                this.selectSeller.setValue(e.value)
            }
            if (selectedSeller) {
                this.setState({ selectedSeller: selectedSeller }, () => {
                    SelectedSeller.setSeller(selectedSeller);
                    resolve()
                });
            } else {
                resolve()
            }
        })
    }
    /** 3 Search fields Change handlers */
    clientNumberChange = (e) => {
        if (e.target.value.length > 0) this.setState({ isEditingClientNumber: true });
        else this.setState({ isEditingClientNumber: false });
    }
    phoneNumberChange = (e) => {
        if (e.target.value.length > 0) this.setState({ isEditingPhoneNumber: true });
        else this.setState({ isEditingPhoneNumber: false });
    }
    authorizedDealerChange = (e) => {
        if (e.target.value.length > 0) this.setState({ isEditingAuthorizedDealer: true });
        else this.setState({ isEditingAuthorizedDealer: false });
    }
    /** the end of Search fields Change handlers */

    /** "Search" button click handler 
     * After a click on "Search" button a collapse with many input fields should be opened up */
    searchClick = (e) => {
        this.setState({ searching: true });
        let searchData;
        let searchFuncType;
        if (this.state.isEditingClientNumber) {
            if (this.searchFields["business_id"].getValue().valid) {
                searchData = this.searchFields["business_id"].getValue().value;
                searchFuncType = "0";
            }
        }
        else if (this.state.isEditingPhoneNumber) {
            if (this.searchFields["phone_number"].getValue().valid) {
                searchData = this.searchFields["phone_number"].getValue().value;
                searchFuncType = "1";
            }
        }
        else if (this.state.isEditingAuthorizedDealer) {
            if (this.searchFields["license_number"].getValue().valid) {
                searchData = this.searchFields["license_number"].getValue().value;
                searchFuncType = "2";
            }
        }
        if (searchData && searchData !== "") {
            let params = {};
            switch (searchFuncType) {
                case "0":
                    params.business_identifier = searchData;
                    break;
                case "1":
                    params.phone_number = searchData;
                    break;
                case "2":
                    params.business_license_number = searchData;
                    break;
            }
            Loader.show();
            getSellers({ params }, true)
                .then(
                    res => {
                        Loader.hide();
                        if (res.length > 0) {
                            let seller = res[0];

                            this.setState({ distributorID: seller.distributor_id });

                            let distributor = this.state.distributors.find(element => (element.id == seller.distributor_id));


                            this.setState({ selectedDistributor: distributor }, () => {
                                this.selectDistributor.setValue(distributor.id);
                            });

                            
                            this.distributorChange({value: distributor.id}).then(()=>{
                                this.sellerChange({value: seller.id});
                            });

                            this.selectSeller.setValue(seller.id);
                            this.setState({ selectedSeller: seller }, () => {
                                SelectedSeller.setSeller(seller);
                            });
                            

                        } else Notifications.show(LM.getString("sellerNotFound"), 'danger');
                    }
                )
                .catch(
                    (err) => {
                        Loader.hide();
                        Notifications.show(err, 'danger')
                    }
                )
        }
    }

    /** Function that opens the GET html request (getDistributors from DataProvider).
     *  Saves an array of distributors from the server into distributors variable. */
    loadDistributorsList = (shouldRefreshUserData = true, distributorId = false, forceUpdate = false) => {
        return new Promise((resolve, reject) => {
            getDistributors(forceUpdate).then((distributors) => {
                    this.setState({ distributors }, () => {
                        if (shouldRefreshUserData) {
                                getCurrentUserData(false).then((userData) => {
                                    if(userData.details.distributorId){
                                        this.selectDistributor.setValue(userData.details.distributorId)
                                        this.distributorChange({value: userData.details.distributorId})
                                    }
                                }
                            ).catch(
                                (err) => {
                                    Notifications.show(LM.getString("loadUserDataFailed") + ". " + err, 'warning');
                                }
                            )
                        }
                        if (distributorId) {
                            this.distributorChange({value: distributorId}, false)
                        }
                        resolve(distributors)
                    });
                    
                }
            ).catch((err) => {
                    Notifications.show(LM.getString("loadDistributorsListFailed") + err, 'danger');
                    reject(err)
                }
            )
        })
    }

    async componentDidMount() {

        SelectedSeller.refreshDistributors.subscribe((params) => this.loadDistributorsList(params.shouldRefreshUserData, params.newDistributorId));
        SelectedSeller.refreshSellers.subscribe((newSellerId) => this.refreshSellers(newSellerId));
        SelectedSeller.setHeader.subscribe((newHeader) => this.setState({ header: newHeader }))

        SelectedSeller.resetDistributor.subscribe(() => {
            this.selectDistributor && this.selectDistributor.setValue(null)
            this.setState({ selectedDistributor: {} });
        });

        SelectedSeller.resetSeller.subscribe(() => {
            this.selectSeller &&  this.selectSeller.setValue(null);
            this.setState({ selectedSeller: {} });
        });
        
        SelectedSeller.deleteSeller.subscribe(()=>{
            this.loadDistributorsList()
            this.refreshSellers()
        })

        SelectedSeller.createSellerClicked.subscribe(()=>{
            this.setState({ creatingNew: !this.state.creatingNew })
        })

        // SelectedSeller.createDistributorClicked.subscribe(()=>{
        //     this.setState({ creatingNewDistributor: !this.state.creatingNewDistributor })
        // })


        SelectedSeller.clearFields.subscribe(() => {
            this.searchFields.business_id && this.searchFields.business_id.setValue("");

            this.searchFields.license_number && this.searchFields.license_number.setValue("");
            this.setState({ isEditingClientNumber: false, isEditingPhoneNumber: false, isEditingAuthorizedDealer: false, renderNumbersFields: false, }, () => {
                this.setState({
                    renderNumbersFields: true,
                })
            });
        });
        let distributorId = SelectedSeller.distributorId
        let sellerId = SelectedSeller.sellerId
        let [distributors, sellers] = await Promise.all([
            this.loadDistributorsList(false, false),
            distributorId ? getSellersByDistributorId(distributorId, {}, true) : Promise.resolve([])
        ])
        if(distributorId){
            this.distributorChange({value: distributorId}, false).then(()=>{
                if(sellerId){
                    this.setState({
                        sellers
                    },()=>{
                        this.sellerChange({value: sellerId}).then(()=>{
                            this.setState({
                                state: 'loaded'
                            })
                        })
                    })
                } else {
                    this.setState({
                        sellers,
                        state: 'loaded'
                    })
                }
            })
        }else {
            this.setState({
                state: 'loaded'
            })
        }
        
    }

    componentWillUnmount() {
        SelectedSeller.resetDistributor.next()
    }

    render() {
        /** selectDistributorProps - props for "Select Distributor" field (<InputTypeSearchList/> component) */
        const selectDistributorProps = {
            options: [],
            title: LM.getString('selectDistributor'),
            id: "SearchBusinessSelectDistributor",
        };
        /** data for "Select Seller" field (InputTypeSearchList InputUtils component) */
        const selectSellerProps = {
            options: [],
            title: LM.getString("selectSeller"),
            id: "SearchBusinessSelectSeller",
        };

        /** data for "Client Number" field (InputTypeNumber InputUtils component) */
        const clientNumberProps = {
            title: LM.getString("clientNumber"),
            id: "SearchBusinessClientNumber",
        };
        /** data for "Authorized Dealer" field (InputTypeNumber InputUtils component) */
        const authorizedDealerProps = {
            title: LM.getString("authorizedDealer"),
            id: "SearchBusinessAuthorizedDealer",
        };
        /** data for "Phone Number" field (InputTypePhone InputUtils component) */
        const phoneNumberProps = {
            title: LM.getString("phoneNumber"),
            /** A final id of a prefix input (first one with 3 numbers) will look so:
            * "InputTypeNumberInputTypePhonePrefix"+id
            * A final id of a number input (second one with 7 numbers) will look so:
            * "InputTypeNumberInputTypePhoneNumber"+id
            */
            id: "SearchBusinessPhoneNumber",
        };

        /** data for button Search  */
        const searchText = LM.getString('search');
        const createText = LM.getString('create');

        return (
            <Container className="businessContainer">
                <Row >
                    <Col sm={2} className="businessHeader">
                        {this.state.header}
                    </Col>
                    {this.state.state == 'loading'  && <Col className="loading">
                        <Spinner size="sm" color="primary" />{ /*LM.getString('loading'*/}
                    </Col>}
                </Row>
                <Container className="businessSettings">
                    <Row>
                        <Col sm='4'>
                            <Collapse isOpen={!this.state.creatingNewDistributor}>
                                <InputTypeSimpleSelect
                                    disabled={this.state.state == 'loading'}
                                    {...selectDistributorProps}
                                    options={this.state.distributors.map(distributor => {return {value: distributor.id,label:distributor.name}})}
                                    onChange={this.distributorChange}
                                    ref={(componentObj) => { this.selectDistributor = componentObj }}
                                />
                            </Collapse>
                        </Col>
                        <Col sm='4'>
                            <Collapse isOpen={!this.state.creatingNew}>
                                <InputTypeSimpleSelect
                                    disabled={this.state.state == 'loading'}
                                    {...selectSellerProps}
                                    options={this.state.sellers.map(seller => {return {value: seller.id,label:seller.name}})}
                                    onChange={this.sellerChange}
                                    ref={(componentObj) => { this.selectSeller = componentObj }}
                                />
                            </Collapse>
                        </Col>
                    </Row>
                    <Collapse isOpen={!this.state.creatingNew}>
                        <Row>
                            <Col sm='4'>
                                <InputTypeNumber
                                    {...clientNumberProps}
                                    disabled={this.state.state == 'loading' || (this.state.isEditingPhoneNumber || this.state.isEditingAuthorizedDealer)}
                                    onChange={this.clientNumberChange}
                                    ref={(componentObj) => { this.searchFields["business_id"] = componentObj }}
                                />
                            </Col>
                            <Col sm='4'>
                                {this.state.renderNumbersFields && <InputTypePhone
                                    {...phoneNumberProps}
                                    disabled={this.state.state == 'loading' || (this.state.isEditingClientNumber || this.state.isEditingAuthorizedDealer)}
                                    onChange={this.phoneNumberChange}
                                    ref={(componentObj) => { this.searchFields["phone_number"] = componentObj }}
                                />}
                            </Col>
                            <Col sm='4'>
                                <InputTypeNumber
                                    {...authorizedDealerProps}
                                    disabled={this.state.state == 'loading' || (this.state.isEditingClientNumber || this.state.isEditingPhoneNumber)}
                                    onChange={this.authorizedDealerChange}
                                    ref={(componentObj) => { this.searchFields["license_number"] = componentObj }}
                                />
                            </Col>
                        </Row>
                    </Collapse>
                    {!this.state.creatingNew ?
                        <Row>
                            <Col sm='auto'>
                                <Button className="businessButton businessButtonBig businessButtonBlue"
                                    id="businessButtonSearch" onClick={this.searchClick}>{searchText}</Button>
                            </Col>
                        </Row>
                        : <></>
                    }
                    {!this.state.creatingNew  && <Row >
                        <Col >
                            <hr className="businessLine" />
                        </Col>
                    </Row>}
                </Container>
            </Container>
        )
    }
}
export default SearchBusiness;