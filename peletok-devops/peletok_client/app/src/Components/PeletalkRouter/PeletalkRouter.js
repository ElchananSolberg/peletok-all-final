import React from 'react';
import {
    BrowserRouter as Router,
    Route, Redirect, Switch
} from "react-router-dom";
import { LanguageManager } from "../LanguageManager/Language"
import { Login } from "../LoginComponent/Login";
import { MainScreen } from '../MainScreen/MainScreen';
import '../ColorLoader/ColorLoader.css';
import {config as SERVER_BASE} from "../../utils/Config";
import {AuthService} from '../../services/authService'

var debugLogin = false;

export class PeletalkRouter extends React.Component {
    // This is a Temp Router for all components to be protected by login
    //
    constructor(props) {
        super(props);
        this.state = {
            user: {
                isLoggedIn: debugLogin || false
            },
            authComplete: false
        };
        let langFromCookies = this.props.cookies.get('lang');
        if (!langFromCookies) {
            langFromCookies = 'He';
        }
        LanguageManager.setLang(langFromCookies);
        let root = document.getElementsByTagName('body')[0];
        root.setAttribute('style', 'direction:' + LanguageManager.getDirection());
        this.authenticate = this.authenticate.bind(this);
    };

    // this function is called on login/logout
    authenticate(user) {
        this.setState({
            user: Object.assign({}, this.state.user, { ...user }),
        });
    };

    componentDidMount() {
        AuthService.isAuth().then(res => {
            this.setState({
                user: { ...this.state.user, isLoggedIn: debugLogin || res === true },
                authComplete: true
            })
        }).catch(err => {
            alert(err);
        })

    };

    render() {
        return (
            <Router>
                {
                    this.state.authComplete ?
                        this.state.user.isLoggedIn ?
                            <Switch>
                                <Redirect from={'/login'} to={'/'} />
                                <Route path='/' render={(routeProps) => <MainScreen authenticate={this.authenticate} user={this.state.user} {...routeProps} />} />
                            </Switch>
                            :
                            <Switch>
                                <Route path={'/login'}
                                    render={routerProps =>
                                        <Login {...routerProps}
                                            cookies={this.props.cookies}
                                            authenticate={this.authenticate.bind(this)} />} />
                                <Redirect to='/login' />
                            </Switch>
                        :
                        <React.Fragment />
                }
            </Router>
        )
    }
}