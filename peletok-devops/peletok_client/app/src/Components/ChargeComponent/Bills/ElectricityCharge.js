import React, { Component } from 'react';
import { LanguageManager as LM } from '../../LanguageManager/Language'
import { Button, Container, Row, Col } from 'reactstrap';
import './ElectricityCharge.css';
import InputTypeNumber from '../../InputUtils/InputTypeNumber';
import checkMarkImage from '../../../Assets/Icons/MenuIcons/check-circle.svg';
import IEClogo from '../../../Assets/Logos/BillsLogos/electricCo-logo.svg';
import { paymentIECaccountPost, paymentIECTotalPost, getProducts } from '../../DataProvider/DataProvider';
import { SuccessModalWindow } from './SuccessModalWindow';
import { Notifications } from '../../Notifications/Notifications';
import { Loader } from '../../Loader/Loader';
import InputTypePhone from '../../InputUtils/InputTypePhone';
import { UserDetailsService } from '../../../services/UserDetailsService'






export class ElectricityCharge extends Component {

    /**
     * TODO to add inner onChanges which will work with state
     * and to make a validation process
     *
     */
    constructor(props) {
        super(props)
        this.state = {

            modal: false,
            nextScreen: false,
            name: '',
            address: '',
            total: 0,
            contractNumber: '',
            transactionNumber: undefined,
            billsPaymentObj: undefined,
            transactionId: 0
        }
    }

    componentDidMount() {
        getProducts(this.props.supplierId).then(
            (res) => {
                               
                this.setState({ billsPaymentObj: res });
            }
        ).catch(
            (err) => {
                Notifications.show(err, 'danger');
            }
        )
    }

    toggle = () => {
        this.setState({
            modal: !this.state.modal
        })
    }
    getNextScreen = () => {
        this.setState({
            nextScreen: !this.state.nextScreen
        })
    }

    confirmContract = () => {
        if (this.confirmInput.getValue().valid) {
            Loader.show();
            let contractNum = this.confirmInput.getValue().value;
            paymentIECaccountPost({ billNum: contractNum }).then((response) => {
                Loader.hide();
                this.setState({
                    name: response.success.name, address: response.success.address, total: response.success.price, contractNumber: contractNum
                }, () => {
                    this.getNextScreen()
                })
            }
            ).catch(
                function (error) {
                    Loader.hide();
                    if (error && error.response && error.response.data && error.response.data.error) {
                        Notifications.show(error.response.data.error, 'danger');
                    } else {
                        Notifications.show(LM.getString("unrecognizedError"), 'danger')
                    }
                }
            );
        } else {
            Notifications.show(LM.getString("enterContractNum"), 'warning')
        }
    }

    postPayment = () => {
        let sum = this.state.billsPaymentObj.bills_payment[0].max_payment + " " + LM.getString("NIS")
        if ( this.state.total > this.state.billsPaymentObj.bills_payment[0].max_payment) {
            Notifications.show(LM.getString('thisDealIsLimitedTo') + sum, 'danger')
        }
        else if (this.phoneNumberInput.getValue().valid) {
            Loader.show();
            paymentIECTotalPost({
                itemId: this.state.billsPaymentObj.bills_payment[0].id,
                contractNumber: this.state.contractNumber,
                price: this.state.total, phoneNumber: this.phoneNumberInput.getValue().value
            }
            ).then((response) => {
                              
                Loader.hide();
                this.setState({
                    transactionNumber: response.success.isPaid,
                    transactionId: response.currentTransaction
                })
                UserDetailsService.getUserDetails()
                this.toggle();
            }
            ).catch(
                function (error) {
                    Loader.hide();
                    Notifications.show(error.response.data.error || LM.getString("MustEnterPhoneNumber"), 'danger');
                })
        } else {
            Notifications.show(LM.getString("MustEnterPhoneNumber"), 'warning')
            return
        }
    }

    render() {

        return (

            <div className='main_border_padding ElectricityCharge'>
                <Row className={'justify-content-end'}>
                    <Col xs="auto" className={'mx-auto mx-md-0'}>
                        <img className={'height-logo-img'} src={IEClogo} alt='' />
                    </Col>
                </Row>
                <Row className='headerCol my-3'>
                    {LM.getString("electricHeader")}
                </Row>
                {!this.state.nextScreen ?
                    <div className='mt-4 top-border'>
                        <Row className={'mt-4'}>
                            <Col md='5' className={(LM.getDirection() === 'rtl' ? 'left-border ml-lg-2 pl-lg-4' : 'right-border mr-lg-2 pr-lg-4')}>
                                <Row>
                                    <Col xl='10' md='12' sm='6'>
                                        <InputTypeNumber ref={(componentObj) => { this.confirmInput = componentObj }} title={LM.getString("contractNumber")} id='contractNumberInput'
                                            required='true'
                                        />
                                    </Col>

                                </Row>

                                <div className={'d-sm-flex'}>
                                    <Button id='electricityChargeButton' onClick={this.confirmContract}
                                        className={'border-radius-3 btn-lg font-weight-bold fs-17 px-4 regularButton ' +
                                            'mt-md-3 btn-sm-block'}>{LM.getString("confirm")}</Button>
                                    <Button id='electricityChargeCancellationButton' className={'border-radius-3 btn-lg font-weight-bold fs-17 px-4 mt-md-3 mt-sm-0 mt-2 mx-sm-2 btn-sm-block'}>{LM.getString("cancel")}</Button>
                                </div>
                            </Col>
                            <Col className={'mt-3 mt-md-0'}>
                                <img className='imgSize' src={this.props.image} alt='' />
                            </Col>
                        </Row>
                    </div>
                    : [
                        <Row >
                            <Col sm='6' className='next'>
                                {LM.getString("fullName")}
                            </Col>
                            <Col sm='6' id='fullNameCol' className='next'>
                                {this.state.name}
                            </Col>
                        </Row>,
                        <Row >
                            <Col sm='6' className='next'>
                                {LM.getString("address")}
                            </Col>
                            <Col id='addressCol' sm='6' className='next'>
                                {this.state.address}
                            </Col>
                        </Row>,
                        <Row >
                            <Col sm='6' className='next'>
                                {LM.getString("totalPayment")}
                            </Col>
                            <Col sm='6' id='totalPaymentCol' className='next'>
                                {this.state.total}
                            </Col>
                            <Col sm='6' className='totalAmountFont'>
                                {LM.getString("TotalamountPayment")}
                            </Col>
                            <Col sm='6' id='TotalamountPaymentCol' className='totalAmountFont fontColor'>
                                {this.state.total}
                            </Col>
                        </Row>,
                        <Row >
                            <Col sm='12' className='next'>
                                {LM.getString("Commission0")}
                            </Col>
                        </Row>,
                        <Row>
                            <Col sm='4' className='tel'>
                                <InputTypePhone id='phoneNumberInput'
                                    title={LM.getString("cellularNumber")}
                                    required='true'
                                    ref={(componentObj) => { this.phoneNumberInput = componentObj }}
                                />
                            </Col>
                        </Row>,
                        <Row>
                            <Col sm='4'>
                                <Button id='payButton' onClick={this.postPayment}>
                                    {LM.getString("confirm")}
                                </Button>
                            </Col>
                        </Row>
                    ]
                }
                <p className='BottomCol border-radius-3 p-3 payment-note mt-4 mb-0 fs-26'>
                    {LM.getString("electricBottom")}
                </p>
                <SuccessModalWindow
                    printTransactionId={this.state.transactionId}
                    isOpen={this.state.modal}
                    className={this.props.className}
                    toggle={this.toggle}
                    headerToggle={this.toggle}
                    paidCard={<img className='successModalImg' src={IEClogo} />}
                    validationImage={checkMarkImage}
                    cancelClick={this.toggle}
                    printClick={this.toggle}
                    modalMessage={LM.getString("successModalMessage")}
                    messageDivClassName='fs-20 messageGreenColor'
                    cardDetails={this.state.transactionNumber}
                />
            </div>
        )
    }
}