import React from 'react';
import { Button, Row, Col, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { LanguageManager as LM } from '../../LanguageManager/Language';
import './SuccessModalWindow.css';
import { PrintData } from '../../PrintData/PrintData';

export const SuccessModalWindow = (props) => {
    let paidCard = props.paidCard;
    let cancelClick = props.cancelClick;
    let tranceIdToPrint = props.tranceIdToPrint;
    let modalSuccessMessage = props.modalMessage;
    let validationImage = props.validationImage;

    return (

        <Modal size="lg" isOpen={props.isOpen} toggle={props.toggle} className={props.className}>

            <Row>
                <Col>
                    <ModalHeader toggle={props.headerToggle}></ModalHeader>
                </Col>
            </Row>
            <ModalBody >
                <Row>
                    <Col className='modalImageAndMessage' sm='6' >
                        <div className={props.bottomBorderForRoutesCharge}>{paidCard}</div>
                        <span >{props.additionalInvoices}</span>
                        {props.invoiceButtons}
                    </Col>
                    <Col sm='6' className='text-center'>
                        <img src={validationImage} alt='' />
                        <div className={props.messageDivClassName}>{modalSuccessMessage}</div>
                        <div className='detailBorder'>{props.cardDetails}</div>
                    </Col>
                </Row>
            </ModalBody>
            <ModalFooter className='modalFooter'>
                <Row>
                    <Col>

                        {props.printTransactionId ?
                            <div>
                                <PrintData color="primary" transId={props.printTransactionId} titleOfButton={LM.getString('printButton')}/>
                                <Button id='cancelButton' className='modalBtn' color="secondary" onClick={cancelClick}>{LM.getString('close')}</Button>
                            </div>
                            : <div>
                                <Button id='cancelButton' className='modalBtn' color="secondary" onClick={cancelClick}>{LM.getString('close')}</Button>{' '}
                                {/* <Button id='printButton' className='modalBtn' color="primary" onClick={printClick}>{LM.getString('printButton')}</Button> */}
                                {tranceIdToPrint ? <PrintData transId={tranceIdToPrint} color="primary" cardNumber={props.cardNumber}  codeNumber={props.codeNumber} expiryDate={props.expiryDate} titleOfButton={LM.getString('printButton')}></PrintData>
                                    : <Button id='printButton' className='modalBtn' disabled={true} >{LM.getString('printButton')}</Button>}
                            </div>
                        }
                    </Col>
                </Row>
            </ModalFooter>
        </Modal>)
}

