import React, { Component } from 'react';
import { Button, Container, Row, Col } from 'reactstrap';
import './ElectricityChargeCancelation.css';
import InputTypeNumber from '../../../InputUtils/InputTypeNumber';
import { LanguageManager as LM } from '../../../LanguageManager/Language';
import { backToPreviousScreenHandler } from '../../../../utils/BackToPreviousScreenHandler'
import {postConfirmTheDeal} from "../../../DataProvider/Functions/DataPoster";
import {Notifications} from "../../../Notifications/Notifications";

export class ElectricityChargeCancelationConfirmation extends Component {
    constructor(props) {
        super(props)
        this.state = {
            electricCompanyCharge: false,
            electricityCharge: false,
            idAppeal: undefined,
            businessId: undefined,
            contractNumber: undefined
        }
    }

    electricCompanyCharge = () => {
        this.setState({
            electricCompanyCharge: true,
            electricityCharge: false,
        })
    }
    electricityCharge = () => {
        this.setState({
            electricCompanyCharge: false,
            electricityCharge: true,
        })
    }

    cancel = () => {
        this.setState({
            electricCompanyCharge: false,
            electricityCharge: false,
        })
    }



    confirmDetails = () => {
        if (this.contractNumberInput.getValue().valid &&
            this.businessIdInput.getValue().valid &&
            this.idAppealInput.getValue().valid) {
            let date = {
                contractNumber: this.contractNumberInput.getValue().value,
                businessId: this.businessIdInput.getValue().value,
                idAppeal: this.idAppealInput.getValue().value
            };
            let supplier = this.state.electricCompanyCharge ? 185 : 186;
            postConfirmTheDeal(date, supplier).then(
                res => {
                    Notifications.show(LM.getString("messageSent"), "success");
                    // console.log(res);
                }
            ).catch(
                err => {
                    Notifications.show(err, 'danger');
                }
            );
        }
    }

    render() {
        return (
            <div>
            {backToPreviousScreenHandler(this.props.history)}
            <Container className='border'>
                <Row >
                    <Col className='m-3 fs-17'>
                        {LM.getString('electricityChargeCancelationConfirm')}
                    </Col>
                    <Col className='imageCol'>
                        <img className='ecImage' src={this.props.image} alt='electricityCharge' />
                    </Col>
                </Row>
                <Col>
                    <Row>
                        <Col sm="10">
                            {(this.state.electricCompanyCharge || this.state.electricityCharge) &&
                                <Row>
                                    <Col sm='4'>
                                        <InputTypeNumber id='contractNumberInput'
                                                         title={LM.getString("contractNumber")}
                                                         required={true}
                                                         ref={objReference => this.contractNumberInput = objReference}
                                        />
                                    </Col>
                                    <Col sm='4'>
                                        <InputTypeNumber id='businessIdInput'
                                                         title={LM.getString("businessId")}
                                                         required={true}
                                                         ref={objReference => this.businessIdInput = objReference}/>
                                    </Col>
                                    <Col sm='4'>
                                        <InputTypeNumber id='idAppealInput'
                                                         title={LM.getString('idAppeal')}
                                                         required={true}
                                                         ref={objReference => this.idAppealInput = objReference}/>
                                    </Col>
                                    <Col>
                                        <Button onClick={this.confirmDetails} size='lg' className={'regularButton fs-17 px-4 my-2 ' + (LM.getDirection() === 'rtl' ? ' ml-2' : 'mr-2')}>
                                            {LM.getString("confirm")}
                                        </Button>
                                        <Button onClick={this.cancel} size='lg' className={'fs-17 px-4 my-2'}>
                                            {LM.getString("back")}
                                        </Button>
                                    </Col>
                                </Row>

                            }
                            {!this.state.electricCompanyCharge && !this.state.electricityCharge &&
                                <React.Fragment>
                                    <Button onClick={this.electricCompanyCharge} size='lg' className={'regularButton fs-17 px-4 my-2 ' + (LM.getDirection() === 'rtl' ? ' ml-2' : 'mr-2')}>
                                        {LM.getString("confirm")} {LM.getString("electricCompanyCharge")}
                                    </Button>
                                    <Button onClick={this.electricityCharge} size='lg' className='regularButton fs-17 px-5 my-2'>
                                        {LM.getString("confirm")} {LM.getString("electricityCharge")}
                                    </Button>
                                </React.Fragment>
                            }
                        </Col>
                    </Row>
                </Col>
            </Container>
            </div>
        );
    }
}
