import React, { Component } from 'react';
import '../../Charge.css';
import { Card, CardText, Row, Col } from 'reactstrap';
import { Switch, Route, Link } from 'react-router-dom';
import { LanguageManager as LM } from '../../../LanguageManager/Language';
import { getSuppliers } from '../../../DataProvider/DataProvider';
import { SERVER_BASE } from '../../../DataProvider/DataProvider';
import { CurrentBillCancelation } from './CurrentBillCancelation';

export class BillsLocalPaymentCancelationMainScreen extends Component {

    constructor(props) {
        super(props);
        this.state =
            {
                bills: [],
            };
        this.ChargeMenu = this.ChargeMenu.bind(this);
    }
  
    componentDidMount() {
        getSuppliers().then(
            res => {

                this.setState({ bills: res.bills });

            }).catch(
                (err) => {
                    alert(err);
                })
    }

    cardRenderer(dataArray) {
        let rows = [];
        let cardArray = [];
        for (let j = 0; (j) < dataArray.length; j++) {
            let opacity = 1;
            let pointerEventValue = 'auto';
            if (dataArray[j].available === false) {
                opacity = 0.5;
                pointerEventValue = 'none';
            }
            cardArray[j] =
                <Col className="provider-col col-6 mb-3" key={"col" + dataArray[j].id} xl={2} lg={3} sm={4}>
                    <Link id={`id${dataArray[j].id}Link`} style={{ pointerEvents: `${pointerEventValue}`, textDecoration: "none" }} to={`${this.props.match.url}/chargeCancelOrConfirmation/${dataArray[j].id}`}>
                        <Card className="providerCard providerCardBorderRadius" key={"card" + dataArray[j].id} body inverse
                            style={{ opacity: `${opacity}` }}>
                            <div className='d-flex' style={{ backgroundColor: dataArray[j].logo_background_color }}>
                                <img alt='' className="provider-card-img m-auto" src={`${SERVER_BASE}/${dataArray[j].image}`} />
                            </div>
                            <CardText tag="h1" className="provider-card-text">{dataArray[j].name}</CardText>
                        </Card>
                    </Link>
                </Col>
        }
        rows = (
            <React.Fragment>
                {cardArray}
            </React.Fragment>
        );
        // }
        return (
            <React.Fragment>
                {rows}
            </React.Fragment>
        );
    }
   
    ChargeMenu() {
        return (
            <React.Fragment>
                <h5 className='top-border header pt-3'>{LM.getString("chargeCancelOrConfirmation")}</h5>
                <Row className={'px-2'}>
                    {this.cardRenderer(this.state.bills, 'bills')}
                </Row>
            </React.Fragment>
        )
    }
  
    render() {
        return (
            <React.Fragment>
                <Switch>
                    <Route exact path={`${this.props.match.path}/chargeCancelOrConfirmation`} component={this.ChargeMenu} />
                    <Route path={`${this.props.match.path}/chargeCancelOrConfirmation/:supplierId`} render={(routeProps) => { return <CurrentBillCancelation  {...routeProps} bills={this.state.bills} /> }} />
                </Switch>
            </React.Fragment>
        );
    }
}