import React, { Component } from 'react';
import { Button, Container, Row, Col } from 'reactstrap';
import InputTypeNumber from '../../../InputUtils/InputTypeNumber';
import { LanguageManager as LM } from '../../../LanguageManager/Language';
import { backToPreviousScreenHandler } from '../../../../utils/BackToPreviousScreenHandler'
import { SERVER_BASE } from '../../../DataProvider/DataProvider';
import { Notifications } from '../../../Notifications/Notifications';
import { postCardCancel, postConfirmationNumberSave } from '../../../DataProvider/DataProvider';
import { Confirm } from '../../../Confirm/Confirm';

export class CurrentBillCancelation extends Component {

    constructor(props) {
        super(props)

        this.state = {

            billObj: this.props.bills.find(obj => obj.id == this.props.match.params.supplierId)
        }
    }
    cancelTransaction = () => {
        if (this.contractNumberInput.getValue().valid) {
            let data = {
                transactionId: this.contractNumberInput.getValue().value,
                Internal: true,
            }
            Confirm.confirm(LM.getString('doYouWantToConfirmCancelation')).then(
                () => postCardCancel(data, this.props.match.params.supplierId).then(
                    (res) => Notifications.show(res.success, 'success')).catch(
                        (err) => Notifications.show(err.response.data.error, 'danger')
                    )
            )
        }
    }
    confirmationSaveHandler = () => {
        if (this.confirmationNumberInput.getValue().value != '') {
            let data = {
                transactionId: this.contractNumberInput.getValue().value,
                confirmationNumber: this.confirmationNumberInput.getValue().value
            }
            postConfirmationNumberSave(data, this.props.match.params.supplierId).then(
                (res) => Notifications.show(res.success, 'success')
            ).catch(
                (err) => Notifications.show(err.response.data.error, 'danger')
            )
        } else Notifications.show(LM.getString('wrongInputErrMsg'), 'danger')
    }
    render() {

        return (
            <div>
                {backToPreviousScreenHandler(this.props.history)
                }
                <Container className='border'>
                    <Row >
                        <Col className='imageCol'>
                            <img className='ecImage' src={`${SERVER_BASE}/${this.state.billObj.image}`} alt='electricityCharge' />
                        </Col>
                    </Row>
                    <Col>
                        <Row>
                            <Col sm="7">
                                <Row >
                                    <Col sm='6'>
                                        <InputTypeNumber id='requestIdentification'
                                            title={LM.getString("requestIdentification")}
                                            ref={objReference => this.contractNumberInput = objReference}
                                            required={true}

                                        />
                                    </Col >
                                    <Col sm='6'>
                                        <InputTypeNumber id='confirmationNumberInput'
                                            title={LM.getString("confirmationNumber")}
                                            ref={objReference => this.confirmationNumberInput = objReference} />
                                    </Col>
                                    <p className={'border-radius-3 p-2 payment-note color-gray mx-3'}>
                                        {LM.getString("billCancelationNote")}
                                    </p>
                                </Row>
                                <Button id='saveTransaction' className='regularButton fs-17' onClick={this.confirmationSaveHandler}>{LM.getString("save")}</Button>
                                <Button color='danger' id='cancelTransaction' className='fs-17 mx-4' onClick={this.cancelTransaction}>
                                    {LM.getString("cancelTransaction")}
                                </Button>
                            </Col>
                        </Row>
                    </Col>
                </Container>
            </div >
        );
    }
}
