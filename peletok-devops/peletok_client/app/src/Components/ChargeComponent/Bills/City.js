import React, { Component } from 'react';
import InputTypeSearchList from '../../InputUtils/InputTypeSearchList';
import { Row, Col, Button } from 'reactstrap'
import { LanguageManager as LM } from '../../LanguageManager/Language';
import { Link } from 'react-router-dom';
import './CityPayments.css';

export class City extends Component {

    // TODO to fix the Link

    getCityValue = () => {
        if (this.city.getValue().value != undefined) {
            return this.city.getValue().value
        } return null
    }

    getServiceValue = () => {

        if (this.service.getValue().value != undefined) {
            return this.service.getValue().value
        } return null
    }

    render() {
        return (
            <div className='main_border_padding'>
                <Row>
                    {this.props.header}
                </Row>
                <Row className='mt-2'>
                    <Col lg='4' sm='6'>
                        <InputTypeSearchList
                            title={LM.getString("authority")}
                            id="CitiesSearchList"
                            options={this.props.cities.map(city => city.name_HE)}
                            onChange={this.props.citiesOnChange}
                            ref={(obj) => { this.city = obj }}
                        />
                    </Col >
                    <Col lg='4' sm='6'>
                        <InputTypeSearchList
                            title={LM.getString("paymentType")}
                            id="PaymentsSearchList"
                            options={this.props.services || []}
                            onChange={this.props.serviceOnChange}
                            ref={(obj) => { this.service = obj }}
                        />
                    </Col>
                    <Col lg='8'>
                        <p className={'border-radius-3 p-3 payment-note mt-1 mb-4 color-gray'}>
                            {LM.getString("clientGetsNoInvoice")}
                        </p>
                    </Col>
                </Row>
                <Link style={{ pointerEvents: `${this.props.link}` }} disabled={true} to={`${this.props.match.url}/${this.props.serviceId}`}>
                    <Button disabled={this.props.buttonDisabled} className='border-radius-3 btn-lg font-weight-bold fs-17 px-4 regularButton mt-md-3 btn-sm-block'>
                        {LM.getString("confirm")}
                    </Button>
                </Link>
            </div>
        )
    }
}
