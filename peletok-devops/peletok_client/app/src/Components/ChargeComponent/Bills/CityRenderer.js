import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import { getCities, getProducts } from '../../DataProvider/DataProvider'
import { City } from './City';
import { CityPayProceed } from './CityPayProceed';
import { Notifications } from '../../Notifications/Notifications';


export class CityRenderer extends Component {

    constructor(props) {
        super(props)

        this.state = {
            cities: [],
            selectedCity: [],
            services: [],
            selectedService: '',
            selectedServiceId: [],
            billsPaymentObj: undefined
        }
    }

    citiesOnChange = () => {

        let city = this.cityInput.city.getValue().value
        let cityObj = this.state.cities.find(element => element.name_HE === city);
        let services = cityObj.CityPaymentTypeSite.map((service => service.CityPaymentType.pay_name));
        this.setState({ selectedCity: cityObj, services: services });

    }

    serviceOnChange = () => {
        let selectedArr = [];
        let selectedService = this.cityInput.getServiceValue()
        let selectedServiceInfo = this.state.selectedCity.CityPaymentTypeSite.find(element => { return element.CityPaymentType.pay_name == selectedService })
        selectedArr.push(this.state.selectedCity.CityPaymentTypeSite)
        this.setState({ selectedServiceId: selectedServiceInfo.id, selectedService })
    }

    componentDidMount() {
        getProducts(this.props.match.params.supplierId).then(
            (res) => {
                this.setState({ billsPaymentObj: res })
            }
        ).catch(
            (err) => Notifications.show(err.response.data.error, 'danger')
        )
        getCities().then(
            (res) => {
                this.setState({ cities: res })
            }
        ).catch(
            (err) => {
                Notifications.show(err, 'danger');
            })
    }
    render() {
        return (
            <Switch>
                <Route exact path={this.props.match.path} render={(props) => {
                    return (
                        <City cities={this.state.cities}
                            services={this.state.services}
                            citiesOnChange={this.citiesOnChange}
                            serviceOnChange={this.serviceOnChange}
                            serviceId={this.state.selectedServiceId}
                            link={this.state.selectedService ? 'auto' : 'none'}
                            buttonDisabled={!this.state.selectedService}
                            ref={(objReference) => { this.cityInput = objReference }}
                            {...props} />
                    )
                }} />
                <Route path={`${this.props.match.path}/:id`} render={(props) => {
                    return <CityPayProceed city={this.state.selectedCity.name_HE}
                        service={this.state.selectedService}
                        finalCommission={this.state.billsPaymentObj.bills_payment[0].final_commission}
                        maxPayment={this.state.billsPaymentObj.bills_payment[0].max_payment}
                        productId={this.state.billsPaymentObj.bills_payment[0].id}
                        {...props} />
                }} />
            </Switch>
        )
    }
}
