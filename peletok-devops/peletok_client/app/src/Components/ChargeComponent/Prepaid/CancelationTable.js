import React, { Component } from 'react';
import { Table } from 'reactstrap';
import { LanguageManager as LM } from '../../LanguageManager/Language';
import './CancelationTable.css'

export class CancelationTable extends Component {
    render() {
        return (
            <Table className='cancelationTable' bordered striped  >
                <thead >
                    <tr >
                        <th >{LM.getString('id')}</th>
                        <th >{LM.getString('business_name')} </th>
                        <th >{LM.getString('clientNumber')}</th>
                        <th >{LM.getString('supplier')}</th>
                        <th >{LM.getString('product')}</th>
                        <th >{LM.getString('supplierTelnumTitle')}</th>
                        <th >{LM.getString('price')}</th>
                        <th >{LM.getString('payment')}</th>
                        <th >{LM.getString('date')}</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody >
                    {this.props.tableData}
                </tbody>
            </Table >
        );
    }
}
