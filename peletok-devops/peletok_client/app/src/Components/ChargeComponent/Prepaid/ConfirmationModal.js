import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { LanguageManager as LM } from '../../LanguageManager/Language';

export const ConfirmationModal = (props) => {
    return (
        <div>
            <Modal isOpen={props.isOpen} toggle={props.toggle} className={props.className}>
                <ModalHeader toggle={props.headerToggle}>{props.header}</ModalHeader>
                <ModalBody>
                {props.message}
          </ModalBody>
                <ModalFooter>
                    <Button className ='m-2' color="primary" onClick={props.confirmClick} >{LM.getString("confirm")}</Button>{' '}
                    <Button color="secondary" onClick={props.cancelClick} >{LM.getString("cancel")}</Button>
                </ModalFooter>
            </Modal>
        </div>
    );
}
