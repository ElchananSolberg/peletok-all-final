import React from 'react';
import { Button } from 'reactstrap';
import { LanguageManager as LM } from '../../LanguageManager/Language';

export class CancelationTableRow extends React.Component {

    render() {
        return (
            <tr>
                <td >
                    {this.props.id}
                </td>
                <td >

                    {this.props.business_name}
                </td>
                <td >

                    {this.props.businessId}
                </td>
                <td >

                    {this.props.supplier_name}
                </td>
                <td >

                    {this.props.product_name}
                </td>
                <td >

                    {this.props.phoneNumber}
                </td>
                <td >

                    {this.props.price}
                </td>
                <td >

                    {this.props.amount}
                </td>
                <td >
                    {this.props.transaction_end_timestamp}
                </td>
                {!this.props.disableButton &&
                    <td >
                        <Button onClick={this.props.onClick}>{LM.getString("cancel")}</Button>
                    </td>}
            </tr >)
    }
}
