import React, {Component} from 'react';
import {Route, Link} from 'react-router-dom';
import {Row, Col} from 'reactstrap';
import {Purchase} from './Purchase';
import {PrepaidProduct} from '../Prepaid/PrepaidProduct'
import {Button} from 'react-bootstrap';
import './PrepaidProductGrid.css';
import favoriteStarImage from '../../../Assets/Icons/MenuIcons/favoriteStar.svg';
import { LanguageManager as LM } from '../../LanguageManager/Language';
import { getProducts, favoriteStarPost, getProductTags } from '../../DataProvider/DataProvider';
import { Confirm } from '../../Confirm/Confirm';
import { Notifications } from '../../Notifications/Notifications';

export class PrepaidProductGrid extends Component {

    constructor(props) {
        super(props)
        this.state =
            {
                products: [],
                tags: [],
                currentTab: "favorite",
                productId: undefined
            }

        this.ProductGridRenderFunc = this.ProductGridRenderFunc.bind(this);
        this.loadProducts = this.loadProducts.bind(this);
    }

    ProductGridRenderFunc(productArray, numCellsPerRow, routeProps, color, starColor) {
        let cardArray = [];
        cardArray = productArray.map(product =>
            <Col lg={4} md={6} sm={4} xl={3} className='px-0 mb-4'>
                <div className={'h-100 boxShadow border-r-5 mx-1 '} style={{backgroundColor: color}}>
                    <Link className='prepaidLink' to={routeProps.match.url + `/${product.id}`}>
                        <PrepaidProduct
                            onFavoriteStarClick={this.favoriteStarHandler}
                            toggle={this.toggle}
                            favorite={product.favorite}
                            favoriteStarImage={favoriteStarImage}
                            favoriteStarColor={product.favorite ? starColor : "#00000033"}
                            linkId={product.id}
                            linkFavorite={product.favorite}
                            product={product}/>
                    </Link>
                </div>
            </ Col>
        )
        return (<Row className={'m-0'}>{cardArray}</Row>)

    }

    favoriteStarHandler = (id, inFavorite) => {

        let message = inFavorite ? LM.getString("wantToRemoveFromFavorites") : LM.getString("wantToAddToFavorites")
        Confirm.confirm(message).then(() => {
            favoriteStarPost(id, {favorite: !inFavorite}).then((res) => {
                if (res === 'success') {
                    if (this.state.products.manual == undefined) {
                        let virtual = this.state.products.virtual.map(p => p.id == id ? Object.assign(p, {favorite: !inFavorite}) : p)
                        this.setState({products: Object.assign({}, this.state.products, {virtual})})
                    } else {
                        let virtual = this.state.products.virtual.map(p => p.id == id ? Object.assign(p, {favorite: !inFavorite}) : p)
                        let manual = this.state.products.manual.map(p => p.id == id ? Object.assign(p, {favorite: !inFavorite}) : p)
                        this.setState({products: Object.assign({}, this.state.products, {virtual, manual})})
                    }
                }
            })
        })
    }

    changeFilter(filterType) {
        this.setState({currentTab: filterType});
    }

    cardFilterer(wantedValue) {
        return (obj) => {

            let currentTag = this.state.tags.filter((tag) => this.state.currentTab == tag.name)
            let truthValue = false;
            switch (this.state.currentTab) {
                case "favorite":
                    truthValue = obj.favorite;
                    break;
                case this.state.currentTab:
                    truthValue = obj.tags.find(tag => tag == currentTag[0].id)
                    break;
                default:
                    truthValue = false;
            }
            if (wantedValue) {
                return truthValue;
            } else {
                return !truthValue;
            }
        }
    }

    createButtonsFromTags = (tagArr) => {

        let tagButtons = tagArr.map(
            tag => {
                return (
                    <Button
                        id={tag.name}
                        className={'productGridButtons py-0 my-2 mx-1 ' + (this.state.currentTab === tag.name ? "productGridButtonsSelected" : "")}
                        onClick={() => this.changeFilter(tag.name)}
                    >{tag.name}
                    </Button>
                )
            }
        )
        return tagButtons
    }

    PrepaidLayoutRenderer(productArray, numCellsPerRow, routeProps, color) {
        return (
            <React.Fragment>
                <Row>
                    <Col sm='auto'>
                        <Button
                            id='productGridButton1'
                            className={'productGridButtons py-0 my-2 ' + (this.state.currentTab === "favorite" ? "productGridButtonsSelected " : "") + (LM.getDirection() === "rtl" ? "mr-0 ml-1" : "ml-0 mr-1")}
                            onClick={() => this.changeFilter("favorite")}>{LM.getString("favorite")}</Button>
                        {this.createButtonsFromTags(this.state.tags)}
                    </Col>
                </Row>
                <div style={this.props.match.params.tab == 'cancel' ? {backgroundColor: '#CDEE8F'} : null}>
                    {this.ProductGridRenderFunc(productArray.filter(this.cardFilterer(true)), numCellsPerRow, routeProps, (color.chosenCardBackgroundColor || "#00000033"), color.favoriteStarColor)} {/**choose defalut colors  */}
                    <hr/>
                    <span className='allCardsHeaderSpan'>{LM.getString("otherCards")}</span>
                    {this.ProductGridRenderFunc(productArray.filter(this.cardFilterer(false)), numCellsPerRow, routeProps, (color.productCardBackgroundColor || "#00000033"), color.favoriteStarColor)} {/** choose defalut colors */}
                </div>
            </React.Fragment>
        );
    }

    loadProducts() {
        getProducts(this.props.match.params.supplierId).then(
            (res) => {

                this.setState({products: res});
            }
        ).catch(
            (err) => {
                Notifications.show(err.response.data.error, 'danger');
            }
        )
    }

    componentDidMount() {
        this.loadProducts();
        getProductTags().then(
            (res => this.setState({tags: res})
            )
        )
    }

    render() {

        let productType = this.props.match.params.productType || this.props.productType;
        let products = this.state.products[productType];
        if (!products) {
            products = [];
        }
        return (
            <div>
                <Route exact path={`${this.props.match.path}/`} render={(routeProps) => (this.PrepaidLayoutRenderer(products, 4, routeProps, this.props.color))} />
                <Route exact path={this.props.match.path + '/:productId'} render={(routeProps) => <Purchase {...routeProps} color={this.props.color} products={this.state.products} productType={this.props.match.params.productType || this.props.productType} />} />
            </div>
        )
    }
}
