import React, { Component } from 'react';
import { Button, Container, Row, Col } from 'reactstrap';
import './Purchase.css';
import { PrepaidProduct } from './PrepaidProduct';
import { LanguageManager as LM } from '../../LanguageManager/Language';
import { SuccessModalWindow } from '../Bills/SuccessModalWindow';
import { UpdateCardChargeModalWindow } from './UpdateCardChargeModalWindow';
import favoriteStarImage from '../../../Assets/Icons/MenuIcons/favoriteStar.svg';
import InputTypePhone from '../../InputUtils/InputTypePhone';
import InputTypeNumber from '../../InputUtils/InputTypeNumber';
import InputTypeText from '../../InputUtils/InputTypeText';
import { virtualChargePost, manualChargePost, getPrepaidCancelDetails, postCardCancel } from '../../DataProvider/DataProvider';
import checkMarkImage from '../../../Assets/Icons/MenuIcons/check-circle.svg';
import ReactSVG from 'react-svg';
import arrowsSwitchHorizontal1 from '../../../Assets/images/arrows-switch-horizontal1.svg';
import arrowsSwitchHorizontal2 from '../../../Assets/images/arrows-switch-horizontal2.svg';
import { ConfirmationModal } from '../Prepaid/ConfirmationModal';
import { Loader } from '../../Loader/Loader'
import { CancelationTable } from '../Prepaid/CancelationTable';
import { CancelationTableRow } from '../Prepaid/CancelationTableRow';
import { Notifications } from '../../Notifications/Notifications'
import InputTypeCheckBox from '../../InputUtils/InputTypeCheckBox';
import { favoriteStarPost } from '../../DataProvider/DataProvider';
import { Confirm } from '../../Confirm/Confirm';
import { UserDetailsService } from '../../../services/UserDetailsService';
import { CancelationModal } from '../Prepaid/CancelationModal'

const CELLCOM_ID = 94;
const FREEPHONE = 202;

export class Purchase extends Component {

    constructor(props) {
        super(props);

        this.state = {
            product: {},
            upgrade: undefined,
            paidCard: {},
            modal: false,
            updateModal: false,
            confirmModal: false,
            cancelModal: false,
            modalMessage: LM.getString("successModalMessage"),
            modalWindowImage: checkMarkImage,
            modalMessageClassName: undefined,
            purchasedCardInfoArr: [],
            phoneNumber: undefined,
            cardsCancelationTableData: [],
            cancelationTableToggle: false,
            terminalNumberInput: undefined,
            contractNumber: undefined,
            cardInfo: undefined,
            gotPurchasedCardData: false,
            currentItemObj: {},
            tableRows: []
        }
    }

    toggle = () => {
        this.setState({
            modal: !this.state.modal
        });
    };

    updateToggle = () => {
        this.setState({
            paidCard: this.updatedCard,
            updateModal: !this.state.updateModal
        });
    };
    confirmToggle = () => {
        this.setState({
            confirmModal: !this.state.confirmModal
        })
    }
    cancelToggle = () => {
        this.setState({
            cancelModal: !this.state.cancelModal
        })
    }

    productPurchaseRenderer = () => {
        return (
            this.currentCard = (
                <div className={'border-r-5 pointer boxShadow h-100'} style={{ backgroundColor: this.props.color.chosenCardBackgroundColor || '#00000033' }}>
                    <PrepaidProduct
                        favoriteStarImage={favoriteStarImage}
                        favoriteStarColor={this.state.product.favorite ? (this.props.color.favoriteStarColor || "var(--headerNavLink)") : "#00000033"}
                        onFavoriteStarClick={this.favoriteStarToggle}
                        linkId={this.props.match.params.productId}
                        recommended={this.state.product.recommended ? LM.getString('recommended') : ''}
                        showMoreInfo={false}
                        product={this.state.product}
                        ref={(objRef) => { this.currentCardREf = objRef }}
                    />
                </div>)
        )

    };
    upgradedCardRenderer = () => {
        if (this.state.upgrade != undefined) {
            return (
                this.updatedCard = (
                    <div className={'border-r-5 pointer boxShadow h-100'} style={{ backgroundColor: this.props.color.chosenCardBackgroundColor || '#00000033' }}>
                        <PrepaidProduct
                            favoriteStarImage={favoriteStarImage}
                            favoriteStarColor={this.state.upgrade.favorite ? (this.props.color.favoriteStarColor || 'green') : "#00000033"}
                            onFavoriteStarClick={this.upgradeFavoriteStarToggle}
                            linkId={this.props.match.params.productId}
                            recommended={this.state.upgrade.recommended ? LM.getString('recommended') : ''}
                            showMoreInfo={false}
                            product={this.state.upgrade}
                            updateClick={this.updateToggle} />
                    </div>)
            )
        }
        return null
    };

    favoriteStarToggle = (productId, inFavorite) => {

        let message = inFavorite ? LM.getString("wantToRemoveFromFavorites") : LM.getString("wantToAddToFavorites")
        Confirm.confirm(message).then(() => {
            let newObj = { ...this.state.product }
            newObj.favorite = !this.state.product.favorite
            this.setState({ product: newObj })
            let data = { favorite: newObj.favorite }
            favoriteStarPost(productId, data)
        })
    }

    upgradeFavoriteStarToggle = (productId, inFavorite) => {
        let message = inFavorite ? LM.getString("wantToRemoveFromFavorites") : LM.getString("wantToAddToFavorites")
        Confirm.confirm(message).then(() => {
            let newObj = { ...this.state.upgrade }
            newObj.favorite = !this.state.upgrade.favorite
            this.setState({ upgrade: newObj })
            let data = { favorite: newObj.favorite }
            favoriteStarPost(productId, data)

        })
    }

    currentCardCharge = () => {

        this.setState({ paidCard: this.currentCard })
        let data = {
            itemId: this.state.product.id
        }

        if (this.state.product != undefined && this.state.product.type == 2) {
            this.setState({ paidCard: this.currentCard });
            this.manualChargePostHandler(data)
        }
        else if (this.telephoneNumberInput.getValue().value == this.numberValidationInput.getValue().value &&
            this.state.product != undefined && this.state.product.type == 1) {
            // this.setState({ paidCard: this.currentCard });
            data.phoneNumber = this.telephoneNumberInput.getValue().value
            data.contractNumber = this.props.match.params.supplierId == FREEPHONE ? this.userNumInput.getValue().value : this.telephoneNumberInput.getValue().value;
            this.virtualChargePostHandler(this.props.match.params.supplierId, data)
        }
        else {
            Notifications.show(LM.getString('errorMsg'), 'danger')
        }
    };

    updatedCardCharge = () => {

        let data = {
            itemId: this.state.upgrade.id
        };
        if (this.state.upgrade.type == 2) {
            this.manualChargePostHandler(data)
        }
        else if (this.updatedCardPhoneinput.getValue().value == this.updatedCardNumberValidationinput.getValue().value &&
            this.state.upgrade != undefined && this.state.upgrade.type == 1) {
            data.contractNumber = this.props.match.params.supplierId == FREEPHONE ? this.userNumInput.getValue().value : this.telephoneNumberInput.getValue().value;
            this.virtualChargePostHandler(this.props.match.params.supplierId, data)
        }
        else {
            Notifications.show(LM.getString('errorMsg'), 'danger')
        }
    };

    virtualChargePostHandler = (supplierId, data) => {
        Loader.show()
        virtualChargePost(supplierId, data).then(this.successHandler).catch(this.errorHandler)

    };

    manualChargePostHandler = (data) => {
        Loader.show()
        manualChargePost(data).then(this.successHandler).catch(this.errorHandler)
    };

    successHandler = (res) => {
        Loader.hide()
        if (Object.keys(res)[0] === 'success') {
            if (res.success) {
                this.setState({
                    currentTransaction: res.currentTransaction,
                    modalMessageClassName: true,
                    cardInfo: res.success
                }, () => {
                    this.purchasedCardInfo(res.success)
                })
                UserDetailsService.getUserDetails()
            }
            this.toggle()
        } else if (Object.keys(res)[0] === 'error') {
            Notifications.show(res.error)
        }
    }

    errorHandler = (err) => {
        Loader.hide()
        Notifications.show(err.response.data.error, 'danger')
    }

    cardChargeCancelToggle = () => { return (this.setState({ cancelationTableToggle: !this.state.cancelationTableToggle })) }

    cardChargeCancelDetailsHandler = () => {

        let itemId = this.state.product.id;
        let businessId = this.terminalNumberInput.getValue();
        let supplierId = this.props.match.params.supplierId;
        this.setState({
            terminalNumberInput: this.terminalNumberInput.getValue().value
        })

        if (this.props.productType == 'virtual') {

            let contractNumber = this.numberValidationInput.getValue().value
            this.setState({ contractNumber })
            if (this.numberValidationInput.getValue().valid &&
                this.terminalNumberInput.getValue().valid) {

                getPrepaidCancelDetails(supplierId, contractNumber, itemId, businessId.value).then(
                    (res) => {

                        this.cardChargeCancelToggle()
                        this.setState({
                            cardsCancelationTableData: res
                        }, () => this.tableRowsRender())
                    }
                ).catch((err) => Notifications.show(LM.getString("thereIsNoTransactionsToCancel"), 'danger'))
            }
        }
        else if (this.terminalNumberInput.getValue().valid) {

            let manualUrl = 1;
            getPrepaidCancelDetails(manualUrl, null, itemId, businessId.value).then(
                (res) => {
                    this.cardChargeCancelToggle()
                    this.setState({ cardsCancelationTableData: res, terminalNumberInput: businessId.value }, () => this.tableRowsRender())
                }
            ).catch((err) => Notifications.show(LM.getString("thereIsNoTransactionsToCancel"), 'danger'))
        }
    }

    cardCancelation = () => {

        let itemId = this.state.product.id;
        let manualUrl = 1;
        let businessId = this.state.terminalNumberInput

        if (this.props.productType == 'manual' && this.state.cardsCancelationTableData.length > 0) {

            let data = {
                cancelAmount: this.amountToCancelInput.getValue().value,
                transactionId: this.state.currentItemObj.id || null,
                Internal: this.cancelationInSystemCheckBox.getValue().value || false,
                cancellationNote: this.cancelNoteInput.getValue().value
            }
            Confirm.confirm(LM.getString('doYouWantToConfirmCancelation')).then(() => {

                postCardCancel(data, manualUrl).then(() => {

                    getPrepaidCancelDetails(manualUrl, null, itemId, businessId).then(
                        (res) => {
                            this.setState({ cardsCancelationTableData: res }, () => this.tableRowsRender())
                        }
                    ).catch(this.setState({ cardsCancelationTableData: [] }, () => this.tableRowsRender()),
                        this.cancelToggle()
                    )
                }
                ).catch(err => Notifications.show(err.response.data.error, 'danger')
                )
            })
        }
        else if (this.state.cardsCancelationTableData.length == 0) {
            Notifications.show(LM.getString("noProducts"), 'danger')
        }

        else {

            let data = {
                cancelAmount: this.props.match.params.supplierId == CELLCOM_ID ? false : this.amountToCancelInput.getValue().value,
                transactionId: this.state.currentItemObj.id,
                Internal: this.cancelationInSystemCheckBox.getValue().value || false
            }
            let supplierId = this.props.match.params.supplierId;

            let virtualData = {
                contractNumber: this.state.contractNumber,
                itemId: this.state.product.id,
                businessId: this.state.terminalNumberInput
            }
            Confirm.confirm(LM.getString('areYouSureDelete')).then(() => {
                postCardCancel(data, supplierId).then(() => {
                    getPrepaidCancelDetails(supplierId, virtualData.contractNumber, virtualData.itemId, virtualData.businessId).then(
                        (res) => {
                            this.setState({ cardsCancelationTableData: res }, () => this.tableRowsRender())
                            this.cancelToggle()
                        }).catch(
                            () => {
                                this.setState({ cardsCancelationTableData: [] }, () => this.tableRowsRender())
                                this.cancelToggle()
                            }
                        )
                })
            })
        }
    }
    loadProducts() {

        let prodArr = [];
        if (this.props.match.params.productType == 'manual' || !this.props.products.virtual) {
            this.props.products.manual.forEach(value => prodArr.push(value))
        }
        else {
            this.props.products.virtual.forEach(value => prodArr.push(value));
        }
        prodArr.forEach((value) => {

            if (value.id == this.props.match.params.productId) {

                this.setState({ product: value });

                if (value.upscale_product_id) {
                    var update = prodArr.find(obj => { return obj.id == value.upscale_product_id });
                    this.setState({ upgrade: update })
                }
            }
        })
    }
    componentDidMount = () => {
        this.loadProducts();
    }

    purchasedCardInfo = (obj) => {

        let purchasedCardInfoArr = [];
        purchasedCardInfoArr.push(obj)
        purchasedCardInfoArr = purchasedCardInfoArr.map(element => {
            if (this.props.match.params.supplierId == FREEPHONE) {
                return (<Container className='m-1 fs-26'>
                    <Row>
                        <Col>
                            {}
                            {LM.getString("priceWithColon")}
                            {element.price ? element.price : obj}
                        </Col>
                    </Row>

                    <Row>
                        {element.user_id ?
                            <Col>
                                {LM.getString("userNmWithColon")}
                                {element.user_id}
                            </Col> : null}
                    </Row>
                    <Row>
                        {element.user_password ?
                            <Col>
                                {LM.getString("passwordWithColon")}
                                {element.user_password}
                            </Col> : null}
                    </Row>
                </Container>)
            }
            return (<Container className='m-1 fs-26'>
                <Row>
                    <Col>
                        {}
                        {LM.getString("cardNumber")}
                        {element.cardNumber ? element.cardNumber : obj}
                    </Col>
                </Row>

                <Row>
                    {element.code ?
                        <Col>
                            {LM.getString("code")}
                            {element.code}
                        </Col> : null}
                </Row>
                <Row>
                    {element.expiry_data ?
                        <Col>
                            {LM.getString("expiryDate")}
                            {element.expiry_data}
                        </Col> : null}
                </Row>
            </Container>)
        }
        )

        return this.setState({ purchasedCardInfoArr, gotPurchasedCardData: true })
    }
    confirmationHandler = (e) => {

        let input;
        if (e.target.id == "InputTypeNumberInputTypePhoneNumbertelephoneNumberInput" || e.target.id == "InputTypeNumberInputTypePhonePrefixtelephoneNumberInput") {
            input = this.telephoneNumberInput;

        } else {
            input = this.updatedCardPhoneinput;
        }

        if (input.getValue().value.length == 10 && input.getValue().valid) {

            this.setState({ phoneNumber: input.getValue().value })
            this.confirmToggle()
        }
    }

    modalCancelClick = () => {

        if (this.state.updateModal == true) {

            this.updatedCardPhoneinput.setValue('          ')

        } else {

            this.telephoneNumberInput.setValue('          ')
        }
        this.confirmToggle()

    }
    modalConfirmClick = () => {
        if (this.state.updateModal == true) {

            this.updatedCardNumberValidationinput.setValue(this.updatedCardPhoneinput.getValue().value)

        } else {

            this.numberValidationInput.setValue(this.telephoneNumberInput.getValue().value)
        }
        this.confirmToggle()
    }

    tableRowsRender = () => {
        let cancelItemArr = [];
        let tableRows = this.state.cardsCancelationTableData.map((element) => {
            return (
                <CancelationTableRow {...element} onClick={(e) => {
                    this.setState({ currentItemObj: element })
                    Object.values(element).forEach(key => cancelItemArr.push(key))
                    this.cancelToggle()
                }
                }
                />
            )
        })
        this.setState({ tableRows })
    }

    telephoneNumberValidationFunc = (value) => {

        if (value != '       ') return true
        return false

    }
    prefixValidationFunc = (value) => {

        if (value != '   ') return true
        return false
    }

    render() {


        if (this.state.cancelationTableToggle == false) {
            return (
                <div>

                    <div>
                        <Row className='prodDescripRow'>
                            <span className='productDescriptionSpan'>
                                {LM.getString('productDescription')}
                            </span>
                        </Row>
                        <div style={this.props.match.params.tab == 'cancel' ? { backgroundColor: '#CDEE8F' } : null} className={'d-flex flex-column flex-sm-row'}>
                            <Col lg={4} sm={4} xl={3} className='border-r-5 px-0' style={{ backgroundColor: `${this.props.color.chosenCardBackgroundColor}` }}>
                                {this.productPurchaseRenderer()}
                            </Col>
                            {this.upgradedCardRenderer() && this.props.match.params.tab == 'main' ?
                                <div className={'mx-auto mx-sm-2 my-auto'}>
                                    <p className='mb-0 text-center'>{LM.getString('upgrade')}</p>
                                    <ReactSVG className='d-none d-sm-block mb-0' src={arrowsSwitchHorizontal1} />
                                    <ReactSVG className='d-none d-sm-block mb-0' src={arrowsSwitchHorizontal2} />
                                    <p className='d-sm-none m-2' style={{ fontSize: '60px', lineHeight: 0.6 }}>&#8645;</p>
                                </div>
                                : null}
                            {this.upgradedCardRenderer() && this.props.match.params.tab == 'main' ?
                                <Col lg={4} sm={4} xl={3} className='border-r-5 px-0' style={{ backgroundColor: `${this.props.color.chosenCardBackgroundColor}` }}>
                                    {this.upgradedCardRenderer()}
                                </Col>
                                : null}
                        </div>
                        <div>
                            <div className='notesRow p-3 my-2'>
                                <Col>
                                    {this.props.match.params.supplierId == FREEPHONE ? LM.getString('freePhoneWarning') : LM.getString('chargeExpirationNote')}
                                </Col>

                                <Col className='productDescriptionSpan'>
                                    {this.props.match.params.supplierId != FREEPHONE ? LM.getString('chargeInvoiceNote') : null}
                                </Col>
                            </div>
                            {this.props.match.params.tab == 'main' && this.state.product.type == 1 || this.props.match.params.tab == 'cancel' && this.state.product.type == 1 ?
                                <div>
                                    <Col sm='4'>
                                        <InputTypePhone id="telephoneNumberInput"
                                            title={LM.getString("supplierTelnumTitle")}
                                            maxLength='10'
                                            minLength='10'
                                            required='true'
                                            numberOnChange={this.confirmationHandler}
                                            prefixOnChange={this.confirmationHandler}
                                            numberValidationFunction={this.telephoneNumberValidationFunc}
                                            prefixValidationFunction={this.prefixValidationFunc}
                                            ref={(objReference) => { this.telephoneNumberInput = objReference }}
                                        />
                                    </Col>

                                    <div className={'d-flex '}>
                                        <Col sm='4'>
                                            <InputTypePhone id="numberValidationInput"
                                                title={LM.getString("numberValidation")}
                                                maxLength='10'
                                                minLength='10'
                                                required='true'
                                                ref={(objReference) => { this.numberValidationInput = objReference }}
                                            />
                                        </Col>
                                        {this.props.match.params.supplierId == FREEPHONE ?

                                            <Col sm='3' className='mx-3'>
                                                <InputTypeNumber id="contactNumberIdInput"
                                                    title={LM.getString("userNm")}
                                                    ref={(objReference) => { this.userNumInput = objReference }}
                                                />
                                            </Col>
                                            :
                                            <></>
                                        }
                                        {this.props.match.params.tab == 'main' ?
                                            <Col >
                                                <Button id="chargeButton" className="buttonTopMargin regularButton btn-lg px-4 border-radius-3"
                                                    disabled={this.state.buttonToggle} color='primary' onClick={this.currentCardCharge}>
                                                    {LM.getString("charge")}
                                                </Button>
                                            </Col>
                                            :
                                            <div>
                                                <Row>
                                                    <Col sm='5' className='mx-3'>
                                                        <InputTypeNumber id="clientIdInput"
                                                            title={LM.getString("clientNumber")}
                                                            required='true'
                                                            ref={(objReference) => { this.terminalNumberInput = objReference }}
                                                        />
                                                    </Col>
                                                    <Col sm='4' >
                                                        <Button id="virtualCardChargeCancelButton" className="buttonMarginRight buttonTopMargin regularButton btn-lg px-3 border-radius-3 fs-17"
                                                            color='primary' onClick={this.cardChargeCancelDetailsHandler}>
                                                            {LM.getString("chargeCancel")}
                                                        </Button>
                                                    </Col>

                                                </Row>
                                            </div>}
                                    </div>
                                </div>
                                :
                                this.props.match.params.tab == 'main' ?
                                    <Col>
                                        <Button id="manualCardChargeButton" className="buttonMarginRight regularButton btn-lg px-3 my-4 border-radius-3"
                                            disabled={this.state.buttonToggle} color='primary' onClick={this.currentCardCharge}>
                                            {LM.getString("charge")}
                                        </Button>
                                    </Col>
                                    :
                                    <Row>
                                        <Col sm='3' className='mx-3'>
                                            <InputTypeNumber id="clientIdInput"
                                                title={LM.getString("clientNumber")}
                                                required='true'
                                                ref={(objReference) => { this.terminalNumberInput = objReference }}
                                            />
                                        </Col>
                                        <Col >
                                            <Button id="manualCardChargeCancelButton" className="buttonTopMargin regularButton btn-lg px-4 border-radius-3 fs-18"
                                                color='primary' onClick={this.cardChargeCancelDetailsHandler}>
                                                {LM.getString("chargeCancel")}
                                            </Button>
                                        </Col>
                                    </Row>
                            }

                        </div>
                        <ConfirmationModal
                            isOpen={this.state.confirmModal}
                            toggle={this.confirmToggle}
                            headerToggle={this.confirmToggle}
                            message={`${LM.getString("supplierTelnumTitle")} ${this.state.phoneNumber}`}
                            confirmClick={this.modalConfirmClick}
                            cancelClick={this.modalCancelClick}
                        />
                        <SuccessModalWindow
                            isOpen={this.state.modal}
                            className={this.props.className}
                            toggle={this.toggle}
                            headerToggle={this.toggle}
                            paidCard={this.state.paidCard}
                            validationImage={this.state.modalWindowImage}
                            modalMessage={this.state.modalMessage}
                            cardDetails={this.state.purchasedCardInfoArr}
                            messageDivClassName={this.state.modalMessageClassName ? 'messageGreenColor' : 'messageRedColor'}
                            cancelClick={this.toggle}
                            tranceIdToPrint={this.state.currentTransaction}
                            cardNumber={this.state.gotPurchasedCardData ? this.state.cardInfo.cardNumber : null}
                            codeNumber={this.state.gotPurchasedCardData ? this.state.cardInfo.code : null}
                            expiryDate={this.state.gotPurchasedCardData ? this.state.cardInfo.expiry_data : null}
                        />
                        <UpdateCardChargeModalWindow
                            isOpen={this.state.updateModal}
                            className={this.props.className}
                            updateHeaderToggle={this.updateToggle}
                            updatedCard={this.updatedCard}
                            updatedCardCancelClick={this.updateToggle}
                            updatedCardCharge={this.updatedCardCharge}
                            // updateDescription={LM.getString("updateDescription")}
                            inputs={this.state.product.type == 1 ? <React.Fragment>
                                <InputTypePhone id="updatedCardPhoneinput"
                                    title={LM.getString("supplierTelnumTitle")}
                                    maxLength='10'
                                    minLength='10'
                                    required='true'
                                    numberValidationFunction={this.telephoneNumberValidationFunc}
                                    prefixValidationFunction={this.prefixValidationFunc}
                                    numberOnChange={this.confirmationHandler}
                                    ref={(objReference) => { this.updatedCardPhoneinput = objReference }} />
                                <InputTypePhone id="updatedCardNumberValidationinput"
                                    title={LM.getString("numberValidation")}
                                    maxLength='10'
                                    minLength='10'
                                    required='true'
                                    ref={(objReference) => { this.updatedCardNumberValidationinput = objReference }} />
                            </React.Fragment>
                                : null}
                        />
                    </div>
                </div>)
        }
        else return (
            <Container>
                <CancelationModal
                    isOpen={this.state.cancelModal}
                    header={this.state.currentItemObj.product_name}
                    className={'modalSize'}
                    toggle={this.cancelToggle}
                    table={<CancelationTable tableData={<CancelationTableRow disableButton
                        {...this.state.cardsCancelationTableData.find((element) => element.id == this.state.currentItemObj.id)} />} />}
                    disableAmountInput={this.props.match.params.supplierId == CELLCOM_ID && this.props.productType == 'virtual'}
                    amountInput={<InputTypeNumber
                        id='amountToCancel'
                        title={LM.getString("amountToCancel")}
                        ref={(objReference) => { this.amountToCancelInput = objReference }}
                    />}
                    inSystemCheckbox={<InputTypeCheckBox
                        id={"cancelationInSystem"}
                        lableText={LM.getString("cancelationInSystem")}
                        ref={(objReference) => { this.cancelationInSystemCheckBox = objReference }}
                    />}
                    note={<InputTypeText
                        id={"cancelNote"}
                        title={LM.getString("note")}
                        ref={(objReference) => this.cancelNoteInput = objReference}
                    />
                    }
                    confirmClick={this.cardCancelation}
                    cancelClick={this.cancelToggle}
                />
                <Row>
                    <Col>
                        <CancelationTable tableData={this.state.tableRows} />
                    </Col>
                </Row>
                <Row>
                </Row>
            </Container >)
    }
}
