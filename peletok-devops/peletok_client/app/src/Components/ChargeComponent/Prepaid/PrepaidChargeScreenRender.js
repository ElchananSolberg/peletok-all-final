import React, { Component } from 'react';
import './PrepaidChargeScreenRender.css'
import { PrepaidChargeScreen } from './PrepaidChargeScreen';
import { Route } from 'react-router-dom';
import { PrepaidProductGrid } from './PrepaidProductGrid';
import { getPrepaidSupplier, getProducts } from '../../DataProvider/DataProvider'
import { backToPreviousScreenHandler } from '../../../utils/BackToPreviousScreenHandler'
import { LanguageManager as LM } from '../../LanguageManager/Language';

// TODO send product via route props and change it in prepaid product grid comp
export class PrepaidChargeScreenRender extends Component {

    constructor(props) {
        super(props)

        this.state = {
            view: false,
            prePaidScreen: {},
            virtual: undefined,
            manual: undefined
        }
    }

    componentDidMount = async () => {
        let [supplier, products] = await Promise.all([getPrepaidSupplier(this.props.match.params.supplierId), getProducts(this.props.match.params.supplierId)])

        this.setState({
            prePaidScreen: supplier,
            virtual: (products.virtual || []).length,
            manual: (products.manual || []).length,
            view: true
        });
    }
    render() {

        {
            if (this.state.view) {
                return (
                    <div>
                        <div className='bottom-border'>
                            {backToPreviousScreenHandler(this.props.history)}
                        </div>
                        <div>
                            {this.state.virtual && this.state.manual ?
                                <div >
                                    <Route exact path={this.props.match.path} render={
                                        (routeProps) =>
                                            <PrepaidChargeScreen
                                                {...routeProps}
                                                {...this.state.prePaidScreen}
                                                operationTypeName={this.props.operationType == 'charge' ? LM.getString("charge") : LM.getString('chargeCancel')}
                                                virtual={this.state.virtual}
                                                manual={this.state.manual}
                                                chargeCancelBackgroundStyle={this.props.operationType == 'cancel' ?
                                                    { backgroundColor: '#CDEE8F', border: '1px solid #CDEE8F', borderRadius: '5px', padding: '10px' } : null}
                                            />} />
                                    <Route path={this.props.match.path + '/:productType'} render={(routeProps) =>
                                        <PrepaidProductGrid
                                            {...routeProps}
                                            color={{
                                                productCardBackgroundColor: this.state.prePaidScreen.product_card_background_color,
                                                chosenCardBackgroundColor: this.state.prePaidScreen.chosen_card_background_color,
                                                favoriteStarColor: this.state.prePaidScreen.favorite_star_color
                                            }} />
                                    } />
                                </div>
                                : <Route path={this.props.match.path} render={(routeProps) =>
                                    <PrepaidProductGrid
                                        productType={this.state.virtual ? 'virtual' : 'manual'}
                                        {...routeProps}
                                        color={{
                                            productCardBackgroundColor: this.state.prePaidScreen.product_card_background_color,
                                            chosenCardBackgroundColor: this.state.prePaidScreen.chosen_card_background_color,
                                            favoriteStarColor: this.state.prePaidScreen.favorite_star_color
                                        }} />}
                                />}
                        </div>
                    </div>
                )
            }
            return null
        }
    }
}
