import React from 'react';
import { Container, Row, Col, Card, CardBody } from 'reactstrap';
import './Gifts.css';
import { LanguageManager as LM } from "../LanguageManager/Language";
import { config } from '../../utils/Config'
import InputTypeCheckBox from '../InputUtils/InputTypeCheckBox';

/**
 * A component that displays gift information and the option to select it
* TODO -- making user information from the database and checking how many points it has
 */
export class Gifts extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            checked: false,
        }
    }
    outerOnChange = async (e) => {
        await this.setState({ checked: this.checkBoxGift.getValue().value });

        if (this.props.onChange) { this.props.onChange(this.state.checked, this.props) }

        if (this.props.setPoints) { this.props.setPoints(this.props.points_required, this.state.checked) }
    }
    getSelectedGift = () => {
        return { checked: this.state.checked, value: this.props }
    }



    render() {
        let enabled = this.state.checked || this.props.enabled
        return (
            <Card className={"p-2 giftContainer " + (enabled ? 'View' : 'Clear')}>
                <CardBody className="p-1">
                    <Container fluid>
                        <Row>
                            <Col sm='5' className="d-flex flex-column">
                                <img id='giftImg' src={config.baseUrl + '/' + this.props.picture} alt="" className="fullImg" />
                            </Col>
                            <Col sm='7' className="d-flex flex-column">
                                <Row className='title'>
                                    <Col sm='10' style={{ textAlign: "start" }}>
                                        <span id='giftName' style={{ fontWeight: 'normal', fontSize: '29px' }}> {this.props.name}</span>
                                    </Col>
                                    <Col sm='2'>
                                    <InputTypeCheckBox checked={this.state.checked} disabled={!enabled} id={"id"+this.props.id.toString()} onChange={this.outerOnChange} toolcdtip='' ref={(componentObj) => { this.checkBoxGift = componentObj }} />
                                    </Col>
                                </Row>
                                <Row className='mb-auto'>
                                    <Col>
                                        <span id='giftData' className='mb-auto' style={{ fontWeight: 'normal', fontSize: '17px' }}> {this.props.description}</span>
                                    </Col>
                                </Row>
                                <Row >
                                    <Col id='points' style={{ textAlign: 'end', color: 'var(--headerNavLinkSelected)', fontWeight: 'normal', fontSize: '21px' }}>
                                        {this.props.points_required}  {LM.getString('points')}
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </Container>
                </CardBody>
            </Card>
        );
    }
}
