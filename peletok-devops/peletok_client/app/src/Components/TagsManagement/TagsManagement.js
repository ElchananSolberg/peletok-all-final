import React, { Component } from 'react';
import './TagsManagement.css';

import { Button, Container, Row, Col } from 'reactstrap';

import { LanguageManager as LM } from '../LanguageManager/Language'
import { Notifications } from '../Notifications/Notifications'

import { postTag } from '../DataProvider/DataProvider'

import InputTypeText from '../InputUtils/InputTypeText'
import InputTypeNumber from '../InputUtils/InputTypeNumber';
import InputTypeSearchList from '../InputUtils/InputTypeSearchList';

import TagsTable from './TagsTable.js';

/** Main component of Tags Management page.  
 * TagsManagement and its children are working with separate .css file: TagsManagement.css.
 */
class TagsManagement extends Component {
    constructor(props) {
        super(props);

        this.state = {
            renderAddFields: true,
            /** Tags Management Header  */
            tagsManagementHeader: LM.getString("tagsManagement"),
            /** Tag Creation Sub header  */
            tagsCreationHeader: LM.getString("tagCreation"),
            /** Tag Edition Sub header  */
            tagsEditionHeader: LM.getString("tagEdition"),

            /** data for "Tag Name" field (InputTypeText InputUtils component) */
            tagNameProps: {
                title: LM.getString("tagName") + ":",
                id: "TagsManagementTagName",
                required: 'true'
            },
            /** data for "Search Tag by Name" field (InputTypeText InputUtils component) */
            searchTagByProps: {
                title: LM.getString("searchTagBy"),
                id: "TagsManagementSearchTagBy",
            },

            /** data for "Select Type" field (InputTypeSelect InputUtils component) */
            selectTypeProps: {
                options: LM.getString("selectTypeArray"),
                title: LM.getString("selectType"),
                id: "TagsManagementSelectType",
                required: 'true'
            },

            /** data for "Order" field (InputTypeNumber InputUtils component) */
            orderProps: {
                title: LM.getString("order"),
                id: "TagsManagementOrder",
            },

            /** button "Add" data  */
            buttonAdd: LM.getString("add"),
            /** button "Search" data  */
            buttonSearch: LM.getString("search"),

            /** working variable that will contain a value of Search Tag by Name field */
            tagName: '',
            ordersIncludes: null,
        }
        /** working variable that will contain an object of all the refs of ADDITION input fields */
        this.addFields = {};
    }

    /** "Order" field change handler */
    orderChange = (e) => {
        let tags = this.tagsTable.getTags();
        let orders = tags.map(current => { return current.order });
        if (orders.includes(e.target.value)) this.setState({ ordersIncludes: true });
        else this.setState({ ordersIncludes: false });
    }

    /** Button "Add" onClick handler. */
    addClick = (e) => {
        let data = {};
        let requestValid = true;

        Object.keys(this.addFields).forEach((key) => {
            if (this.addFields[key].runValidation) {
                requestValid = this.addFields[key].runValidation() ? requestValid : false;
            }
            let value = this.addFields[key].getValue();
            if (!value.valid) requestValid = false;
            else {
                if (value.value === "") {
                    data[key] = null;
                }
                else if (key !== "type") data[key] = value.value;
                else data[key] = LM.getString("selectTypeArray").findIndex((element) => { if (element === value.value) return true }) + 1;
            }
        });
        if (!requestValid) {
            Notifications.show(LM.getString("notValidField"), 'danger')
            return;
        }
        postTag(data)
            .then(
                res => {
                    this.tagsTable.loadTagsList(true);
                    Notifications.show(LM.getString("successTagAdding") + data["name"], 'success')
                    this.setState({
                        renderAddFields: false
                    },()=>{
                        this.setState({
                            renderAddFields: true
                        })
                    })
                }
            ).catch(
                err => {
                    Notifications.show(err.response.data.error, 'danger')
                }
            );

    }
    /** Button "Search" onClick handler.
     * TODO: Add functionality */
    searchClick = (e) => {
        let tagName = this.searchField.getValue().value;
        this.setState({ tagName: tagName });

    }

    render() {
        let warningMsg = LM.getString("warningOrderExist");

        return (
            <Container className="tagsManagementContainer">
                <Row >
                    <Col className='tagsManagementHeader'>
                        {this.state.tagsManagementHeader}
                    </Col>
                </Row>
                <Container className="tagsManagementSettings">
                    <Row >
                        <Col className="tagsManagementSubHeader">
                            {this.state.tagsCreationHeader}
                        </Col>
                    </Row>
                    { this.state.renderAddFields && <Row className="align-items-end">
                        <Col sm='4'>
                            <InputTypeText {...this.state.tagNameProps} ref={(componentObj) => { this.addFields["name"] = componentObj }} />
                        </Col>
                        <Col sm='4'>
                            <InputTypeNumber {...this.state.orderProps} onChange={this.orderChange} ref={(componentObj) => { this.addFields["order"] = componentObj }} />

                        </Col>

                        <Col sm='4'>
                            <InputTypeSearchList {...this.state.selectTypeProps} ref={(componentObj) => { this.addFields["type"] = componentObj }} />
                        </Col>

                        <Col sm='4' className='mx-auto'>
                            <div className="tagsManagementWarningMsg">
                                {this.state.ordersIncludes ? warningMsg : ''}
                            </div>
                        </Col>
                    </Row>}
                    <Row >
                        <Col sm='auto'>
                            <Button className="tagsManagementButton" id="tagsManagementButtonAdd" onClick={this.addClick} >{this.state.buttonAdd}</Button>
                        </Col>
                    </Row>
                    <Row >
                        <Col >
                            <hr className="tagsManagementLine" />
                        </Col>
                    </Row>

                    <Row >
                        <Col className="tagsManagementSubHeader">
                            {this.state.tagsEditionHeader}
                        </Col>
                    </Row>
                    <Row className="align-items-end">
                        <Col sm='4'>
                            <InputTypeText {...this.state.searchTagByProps} ref={(componentObj) => { this.searchField = componentObj }} />
                        </Col>
                        <Col sm='auto'>
                            <Button className="tagsManagementButton" id="tagsManagementButtonSearch" onClick={this.searchClick} >{this.state.buttonSearch}</Button>
                        </Col>
                    </Row>
                    <Row>
                        <Col lg='12'>
                            <TagsTable tagNameForSearch={this.state.tagName} ref={(componentObj) => { this.tagsTable = componentObj }} />
                        </Col>
                    </Row>
                </Container>
            </Container>
        )
    }
}
export default TagsManagement;