import React, { Component } from 'react';
import { Button, Row, Col, Collapse } from 'reactstrap';

import { LanguageManager as LM } from "../LanguageManager/Language"
import {getDistributors, getSellers, getReportRes, getBusinessTags, getSuppliers} from '../DataProvider/DataProvider'
import { ReportTableContent } from "../ReportComponent/ReportTableContent"
import InputTypeRadio from '../InputUtils/InputTypeRadio'
import InputTypeSelect from '../InputUtils/InputTypeSelect'
import InputTypeText from '../InputUtils/InputTypeText'
import InputTypeNumber from '../InputUtils/InputTypeNumber';
import InputTypeSearchList from '../InputUtils/InputTypeSearchList'
import InputTypeDate from '../InputUtils/InputTypeDate';
import InputTypeTime from '../InputUtils/InputTypeTime';
import {Loader} from "../Loader/Loader";
import { ManagerPaymentsEnum } from '../../enums'
import {Notifications} from "../Notifications/Notifications";
import {ReportManagerPagination} from "../ReportComponent/ReportManagerPagination";


export class TransactionReports extends Component {
    constructor(props) {
        super(props);
        this.state = {
            transactionStatus: {},
            busnissStatus: {},
            distributors: [],
            business: [],
            agents: [],
            tableRows: [],
            tableHeadersObj: {},
            tableFootersObj: {},
            openReport: false,
            reportID: props.match.params.type,
            header: '',
            tags: [],
            render: true,
            pagination: false,
            filteredLIst: []
        };
        this.getDistributorsNames = this.getDistributorsNames.bind(this);
        this.getBusinessNames = this.getBusinessNames.bind(this);
        this.getAgentNames = this.getAgentNames.bind(this);
        this.fields = {};
    }

    getDistributorsNames() {
        getDistributors().then(
            (res) => {
                this.setState({ distributors: res });
            }
        ).catch(
            (err) => {
                alert("Failed loading Distributors list: " + err)

            }
        )
    }

    getBusinessNames() {
        getSellers().then(
            (res) => {
                this.setState({ business: res });
            }
        ).catch(
            (err) => {
                alert("Failed loading business list: " + err)

            }
        )
    }

    getAgentNames() {
        // This is mock we need to get it from the server
        // TODO add server call for agents
        this.setState({
            agents: [
                {
                    name: "סמי",
                    id: 1,
                },
                {
                    name: "רונן",
                    id: 2,
                },
                {
                    name: "דליה",
                    id: 3,
                },
            ]
        })
    }

    getTags() {
        getBusinessTags().then(res => {
            this.setState({
                tags: res
            });
        })
    }

    componentDidMount() {
        this.getDistributorsNames();
        this.getBusinessNames();
        this.getAgentNames();
        this.getTags();
        this.onReportHeader();
    }

    componentDidUpdate() {
        if (this.state.reportID !== this.props.match.params.type) {
            this.onReportIDChanged();
        }
    }

    onReportIDChanged() {

        this.setState({
            reportID: this.props.match.params.type,
            render:false,
            openReport: false,
            pagination: false
        },() => {
            this.fields = {};
            this.setState({
                render:true
            })
            this.onReportHeader()
        })
    }

    onReportHeader() {
        let header = '';
        switch (this.state.reportID) {
            case ManagerPaymentsEnum.TRANSACTION:
                header = "transactions";
                break;
            case ManagerPaymentsEnum.PAYMENT:
                header = "payments";
                break;
            case ManagerPaymentsEnum.ACTION:
                header = "actions";
                break;
            case ManagerPaymentsEnum.ACTIONANDPAYMENT:
                header = "actionsAndPayments";
                break;
            case ManagerPaymentsEnum.DISTRIBUTIONACTIVITIES:
                header = "distributionActivities";
                break;
            case ManagerPaymentsEnum.OBLIGO:
                header = "obligo";
                break;
            case ManagerPaymentsEnum.MANUALCARDS:
                header = "manualCards";
                break;
            case ManagerPaymentsEnum.CARDSASSEMBLAGE:
                header = "cardsAssemblage";
                break;
            case ManagerPaymentsEnum.EMAILREPORT:
                header = "emailReport";
                break;
            default:
                header = "transactions";
        }
        this.setState({
            header
        })
    }

    showReportClick = () => {
        Loader.show();

        let params = {};
        let requestValid = true;
        let validetions = []

        // this is a list of valid params for generating the report
        // let validParams = ['startDate', "startTime", "endDate", "endTime", "distributor", "distributor_id", 'busines', 'agent', "agent_id", "business_id", "phone_number", "transaction_number", "by_distributor"];
        Object.keys(this.fields).forEach((key) => {
            if(this.fields[key].getValue().value){
                if(this.fields[key].runValidation){
                    validetions.push(this.fields[key].runValidation())
                }
                params[key] = this.fields[key].getValue().value;
            }
        })
        params['startDate'] = `${this.startDate.getValue().value} ${this.startTime.getValue().value}`;
        params['endDate'] = `${this.endDate.getValue().value} ${this.endTime.getValue().value}`;
        if (params['status']){
            params['status'] = params['status'].map(s => LM.getString("generalStatus").indexOf(s) + 1);
        }
        if (params['business_status']){
            params['business_status'] = params['business_status'].map(s => LM.getString("generalStatus").indexOf(s) + 1);
        }
        if (params['service_type']){
            params['service_type'] = params['service_type'].map(s => LM.getString("serviceTypeOptions").indexOf(s) + 1);
        }
        if (params['business_tags_id']){
            params['business_tags_id'] = this.state.tags.filter(i => params['business_tags_id'].find(name => name == i.name)).map(i => i.id);
        }
        console.log(params)

        if (validetions.filter(v=>!v).length === 0) {
            getReportRes(this.state.reportID, params ).then(
                (res) => {
                    Loader.hide();

                    if (res === "No reports found") {
                        this.setState({
                            pagination: false,
                            openReport: false
                        });
                        Notifications.show(LM.getString("noReportErrMsg"), 'danger')
                    }
                    else {
                        this.setState({
                            tableFootersObj: res['sum'],
                            tableHeadersObj: res.headers.reduce((n, k) => Object.assign(n, {[k]:LM.getString(k)}), {}),
                            tableRows: res['data'],
                            pagination: false,
                            openReport: false,
                            sumOfProfit: res['sum'].priceSum + LM.getString('moneySymbol')
                        },() => {
                            this.setState({
                                pagination: true,
                            })
                        })
                    }
                }
            ).catch(
                (err) => {
                    alert(err);
                })
        }
        else {
            alert(LM.getString("wrongInputErrMsg"))
        }
    }

    getFilteredList = (filteredLIst) => {
        this.setState({
            filteredLIst: filteredLIst,
            openReport: false
        }, ()=> {
            this.setState({openReport: true})
        })
    }


    render() {
        let time_now = new Date()
        let nowHour = time_now.getHours() + "";
        let nowMinute = time_now.getMinutes() + "";
        if (nowHour.length < 2) { nowHour = "0" + nowHour }
        if (nowMinute.length < 2) { nowMinute = "0" + nowMinute }
        return (
            this.state.render ?  <div>
                <h3 className={'color-page-header fs-26 font-weight-light'}>{LM.getString(this.state.header)}</h3>
                <div className="main_border_padding" >
                    <Row>
                        <Col sm="4">
                            <InputTypeSearchList
                                title={LM.getString("business_name")}
                                id='selectBusiness'
                                options={this.state.business.map(busines => busines.name)}
                                onChange={() => {
                                    let theName = this.fieldsBusiness.state.value;
                                    let businesObj = this.state.business.filter((el) => { return el.name == theName })[0];
                                    this.fields["business_identifier"].setValue(businesObj.business_identifier);
                                }}
                                ref={(componentObj) => { this.fieldsBusiness = componentObj }}
                            />
                        </Col>
                        <Col sm="4">
                            <InputTypeNumber
                                title={LM.getString("customNum")}
                                id="costumerNumber"
                                ref={(componentObj) => { this.fields["business_identifier"] = componentObj }} />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="4">
                            <InputTypeSearchList
                                title={LM.getString("selectDistributor")}
                                id='selectDistributors'
                                options={this.state.distributors.map(distributor => distributor.name)}
                                onChange={
                                    () => {
                                        let theName = this.fieldsDistributor.state.value;
                                        let distObj = this.state.distributors.filter((el) => { return el.name == theName })[0]
                                        this.fields['distributor_id'].setValue(distObj.id)
                                    }
                                }
                                ref={(componentObj) => { this.fieldsDistributor = componentObj }}
                            />
                        </Col>
                        <Col sm="4">
                            <InputTypeNumber
                                title={LM.getString("distributorNum")}
                                id="distributorNumber"
                                ref={(componentObj) => { this.fields["distributor_id"] = componentObj }} />
                        </Col>
                    </Row>
                    <Row>

                    </Row>
                    <Row>
                        <Col sm="4">
                            <InputTypeSearchList
                                title={LM.getString("agentSearch")}
                                id='selectAgent'
                                options={this.state.agents.map(agent => agent.name)}
                                onChange={
                                    () => {
                                        let theName = this.fieldsAgent.state.value;
                                        let agenObj = this.state.agents.filter((el) => { return el.name == theName })[0]
                                        this.fields['agent_id'].setValue(agenObj.id)
                                    }
                                }
                                ref={(componentObj) => { this.fieldsAgent = componentObj }}
                            />
                        </Col>
                        <Col sm="4">
                            <InputTypeNumber
                                title={LM.getString("agentNum")}
                                id="agentNumber"
                                ref={(componentObj) => { this.fields["agent_id"] = componentObj }} />
                        </Col>
                    </Row>
                    <Row>
                        {this.state.reportID === ManagerPaymentsEnum.TRANSACTION &&
                            <Col sm="4">
                                <InputTypeSearchList
                                    multi={true}
                                    title={LM.getString('transactionsStatusTitle')}
                                    id="transactionsStatus"
                                    options={LM.getString("generalStatus")}
                                    ref={(componentObj) => {
                                        this.fields['status'] = componentObj
                                    }}/>
                            </Col>
                        }
                        {this.state.reportID === ManagerPaymentsEnum.PAYMENT &&
                            <Col sm="4">
                                <InputTypeSearchList
                                    disabled
                                    multi={true}
                                    title={LM.getString('actionType')}
                                    id="actionType"
                                    options={LM.getString("actionTypeOptions")}
                                    ref={(componentObj) => { this.fields['action_type'] = componentObj }} />
                            </Col>
                        }
                        <Col sm="4">
                            <InputTypeSearchList
                                multi={true}
                                title={LM.getString('businessStatus')}
                                id="businessStatus"
                                options={LM.getString("generalStatus")}
                                ref={(componentObj) => { this.fields['business_status'] = componentObj }} />
                        </Col>
                    </Row>
                    <Row>
                        {(this.state.reportID == ManagerPaymentsEnum.TRANSACTION || this.state.reportID == ManagerPaymentsEnum.ACTION || this.state.reportID == ManagerPaymentsEnum.ACTIONANDPAYMENT) &&
                            <Col sm="4">
                                <InputTypeNumber
                                    title={LM.getString('phoneNumber')}
                                    id="phoneNumber"
                                    ref={(componentObj) => {
                                        this.fields['phone_number'] = componentObj
                                    }}/>
                            </Col>
                        }
                        {this.state.reportID == ManagerPaymentsEnum.PAYMENT &&
                            <Col sm="4">
                                <InputTypeNumber
                                    title={LM.getString('receiptNumber')}
                                    id="receiptNumber"
                                    ref={(componentObj) => { this.fields['receipt_number'] = componentObj }} />
                            </Col>
                        }
                        {!this.state.reportID == ManagerPaymentsEnum.DISTRIBUTIONACTIVITIES &&
                        <Col sm="4">
                            <InputTypeNumber
                                title={LM.getString('transactionNumber')}
                                id="transactionNumber"
                                ref={(componentObj) => {
                                    this.fields['id'] = componentObj
                                }}/>
                        </Col>
                        }
                    </Row>
                    <Row>
                        <Col>
                            <InputTypeRadio
                                options={[LM.getString("byDistributor")]}
                                id="byDistributor"
                                ref={(componentObj) => { this.fields["by_distributor"] = componentObj }} />
                        </Col>
                    </Row>
                    {(this.state.reportID == ManagerPaymentsEnum.ACTION
                        || this.state.reportID == ManagerPaymentsEnum.ACTIONANDPAYMENT
                        || this.state.reportID == ManagerPaymentsEnum.DISTRIBUTIONACTIVITIES) &&
                        <Row>
                            <Col sm="4">
                                <InputTypeSearchList
                                    disabled
                                    multi={true}
                                    title={LM.getString('ServiceTypeMessage')}
                                    id="ServiceTypeMessage"
                                    options={LM.getString("serviceTypeOptions")}
                                    ref={(componentObj) => { this.fields["service_type"] = componentObj }} />
                            </Col>
                            <Col sm="4">
                                <InputTypeSearchList
                                    // TODO Add tags
                                    multi={true}
                                    title={LM.getString('tags')}
                                    id="tags"
                                    options={this.state.tags.map(tag => tag.name)}
                                    ref={(componentObj) => { this.fields["business_tags_id"] = componentObj }} />
                            </Col>
                        </Row>
                    }
                    <Row>
                        <Col sm="4">
                            <InputTypeDate
                                title={LM.getString("startingDate") + ":"}
                                id="startingDate"
                                ref={(componentObj) => { this.startDate = componentObj }} />
                        </Col>
                        <Col sm="4">
                            <InputTypeTime
                                title={LM.getString("startingTime") + ":"}
                                id="startingTime"
                                ref={(componentObj) => { this.startTime = componentObj }} />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="4">
                            <InputTypeDate
                                title={LM.getString("endingDate") + ":"}
                                id="endingDate"
                                ref={(componentObj) => { this.endDate = componentObj }} />
                        </Col>
                        <Col sm="4">
                            <InputTypeTime
                                title={LM.getString("endingTime") + ":"}
                                id="endingTime"
                                default={nowHour + ':' + nowMinute}
                                ref={(componentObj) => { this.endTime = componentObj }} />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="auto">
                            <Button className="paymentsButton" id="paymentsShowReportButton" onClick={this.showReportClick} > {LM.getString("showReport")} </Button>
                        </Col>
                    </Row>
                </div>
                {this.state.openReport &&
                    <ReportTableContent
                        reportName={LM.getString("transactions")}
                        tableRows={this.state.filteredLIst}
                        tableHeadersObj={this.state.tableHeadersObj}
                        tableFootersObj={this.state.tableFootersObj} paginationClient={true} />
                }
                {this.state.pagination &&
                    <ReportManagerPagination
                        tableRows={this.state.tableRows}
                        getFilteredList={this.getFilteredList}
                    />
                }
            </div> : <></>
        )
    }

}
