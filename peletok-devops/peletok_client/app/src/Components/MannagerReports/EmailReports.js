import React, { Component } from 'react';
import { Button, Row, Col, Collapse } from 'reactstrap';

import { LanguageManager as LM } from "../LanguageManager/Language"
import { getReportRes } from '../DataProvider/DataProvider'
import { ReportTableContent } from "../ReportComponent/ReportTableContent"
import InputTypeNumber from '../InputUtils/InputTypeNumber';
import InputTypeDate from '../InputUtils/InputTypeDate';
import InputTypeTime from '../InputUtils/InputTypeTime';

export class EmailReports extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tableRows: [],
            tableHeadersObj: {},
            tableFootersObj: {},
            openReport: false,

        }
        this.fields = {};
        this.reportID = "15";
    }


    showReportClick = () => {
        let params = {};
        let requestValid = true;
        // this is a list of valid params for generating the report
        let validParams = ['startDate', "startTime", "endDate", "endTime", "terminal_number"];
        Object.keys(this.fields).forEach((key) => {
            let isValid = validParams.indexOf(key) != -1
            if (this.fields[key] !== null && isValid) {
                let result = this.fields[key].getValue();
                if (!result.valid) {
                    requestValid = false;
                }
                params[key] = result.value;
            }
        })
        if (requestValid) {
            getReportRes(this.reportID, { params }).then(
                (res) => {
                    if (res === "No reports found") {
                        alert(LM.getString("noReportErrMsg"))
                    }
                    else {
                        let headers = [];
                        Object.values(res['headers']).map((value) => {
                            return headers.push(LM.getString(value))
                        });
                        this.setState({
                            tableFootersObj: res['sum'],
                            tableHeadersObj: headers,
                            tableRows: res['data'],
                            openReport: true,
                            sumOfProfit: res['sum'].priceSum + LM.getString('moneySymbol')
                        }
                        )
                    }
                }
            ).catch(
                (err) => {
                    alert(err);
                })
        }
        else {
            alert(LM.getString("wrongInputErrMsg"))
        }

    }

    render() {
        let time_now = new Date()
        let nowHour = time_now.getHours() + "";
        let nowMinute = time_now.getMinutes() + "";
        if (nowHour.length < 2) { nowHour = "0" + nowHour }
        if (nowMinute.length < 2) { nowMinute = "0" + nowMinute }

        return (
            <div>
                <h3 className={'color-page-header fs-26 font-weight-light'}>{LM.getString("emailReport")}</h3>
                <div className="main_border_padding" >
                    <Row>
                        <Col sm="4">
                            <InputTypeNumber
                                title={LM.getString('terminalNumber')}
                                id="terminalNumber"
                                ref={(componentObj) => { this.fields['terminal_number'] = componentObj }} />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="4">
                            <InputTypeDate
                                title={LM.getString("startingDate") + ":"}
                                id="startingDate"
                                ref={(componentObj) => { this.fields["startDate"] = componentObj }} />
                        </Col>
                        <Col sm="4">
                            <InputTypeTime
                                title={LM.getString("startingTime") + ":"}
                                id="startingTime"
                                ref={(componentObj) => { this.fields["startTime"] = componentObj }} />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="4">
                            <InputTypeDate
                                title={LM.getString("endingDate") + ":"}
                                id="endingDate"
                                ref={(componentObj) => { this.fields["endDate"] = componentObj }} />
                        </Col>
                        <Col sm="4">
                            <InputTypeTime
                                title={LM.getString("endingTime") + ":"}
                                id="endingTime"
                                default={nowHour + ':' + nowMinute}
                                ref={(componentObj) => { this.fields["endTime"] = componentObj }} />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="auto">
                            <Button className="paymentsButton" id="paymentsShowReportButton" onClick={this.showReportClick} > {LM.getString("showReport")} </Button>
                        </Col>
                    </Row>
                </div>
                <Collapse isOpen={this.state.openReport}>
                    <ReportTableContent reportName={LM.getString("emailReport")} tableRows={this.state.tableRows} tableHeadersObj={this.state.tableHeadersObj} tableFootersObj={this.state.tableFootersObj} />
                </Collapse>
            </div>
        )
    }

}
