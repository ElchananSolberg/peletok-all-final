import React from 'react';
import './Loader.css'

import PeletalkSpinner from '../InputUtils/PeletalkSpinner';

/** <Loader/> is Singleton  */
export class Loader extends React.Component {

    static singleton;
    static index = 0;

    constructor(props) {
        super(props);
        /** here we make Loader to be singleton (to have only 1 instance) */
        if (Loader.singleton) {
            throw 'Attention, developer! You must not use Loader as a component, you should use show() and hide() interface.';
        }
        Loader.singleton = this;

        this.state = {
            show: false,
        }
    }

    static show = () => {
        if(Loader.index > 5) throw 'It seems that Loader component does not exist in a render tree.';
        
        if(!Loader.singleton){
            Loader.index++;
            setTimeout(() => {
                Loader.show();
            }, 300 )
        } else Loader.singleton.setState({ show: true });    
    }
    static hide = () => {
        if(Loader.index > 5) throw 'It seems that Loader component does not exist in a render tree.';
        
        if(!Loader.singleton){
            Loader.index++;
            setTimeout(() => {
                Loader.hide();
            }, 300 )
        } else Loader.singleton.setState({ show: false });    
    }

    render = () => {
        return (
            <>
                {this.state.show &&
                    <div className='loader' style={{ height: window.innerHeight, paddingTop: window.innerHeight / 4 }}>
                        <PeletalkSpinner context="loader"/>
                    </div>
                }
            </>
        );
    }
}