import React, { Component } from 'react';
import './CopyPermissions.css';
import { Button, Container, Row, Col } from 'reactstrap';

import { LanguageManager as LM } from '../LanguageManager/Language';

import { getSuppliers } from '../DataProvider/DataProvider'

import InputTypeSearchList from '../InputUtils/InputTypeSearchList';
import { Notifications } from '../Notifications/Notifications'

/**
 * Main component of CopyPermissions screen.  
 */
export default class CopyPermissions extends Component {
    constructor(props) {
        super(props);

        this.state = {
            /** working variable that will contain an array of suppliers from the server */
            suppliers: [],

            /** working variable that will contain an id of selected source supplier */
            currentSourceSupplierId: {},
            /** working variable that will contain an id of selected target source supplier  */
            currentTargetSupplierId: {},
        }
    }

    /** Source  "Select Supplier" field onChange handler
    */
    sourceSupplierChanged = (e) => {
        let value = this.sourceSelectSupplier.getValue().value;
        let selectedSupplier = this.state.suppliers.find(element => (element.name === value));
        this.setState({ currentSourceSupplierId: selectedSupplier.id });
    }
    /** Target "Select Supplier" field onChange handler
    */
    targetSupplierChanged = (e) => {
        let value = this.targetSelectSupplier.getValue().value;
        let selectedSupplier = this.state.suppliers.find(element => (element.name === value));
        this.setState({ currentTargetSupplierId: selectedSupplier.id });
    }

    /** TODO: add functionality 
     * button "Save" onClick handler
    */
    buttonSaveClicked = (e) => {

    }

    componentDidMount() {
        let forceUpdate = false;
        getSuppliers(forceUpdate).then(
            (res) => {
                this.setState({ suppliers: Object.keys(res).reduce((newArr, key) => [...newArr, ...res[key]], []) })
            }
        ).catch(
            (err) => {
                Notifications.show("loadSuppliersList failed: " + err, 'danger')
            }
        )
        // getSuppliersList(forceUpdate).then(
        //     (res) => {
        //         this.setState({ suppliers: res });
        //     }
        // ).catch(
        //     (err) => {
        //         alert(LM.getString("loadSuppliersListFailed") + ": " + err)

        //     }
        // )
    }

    render() {
        /** Screen Header text */
        const header = LM.getString('copyPermissionsSupplierToSupplier');
        /** "Source Supplier" sub header text */
        const sourceSupplierSubHeader = LM.getString('sourceSupplier');
        /** "Target Supplier" sub header text */
        const targetSupplierSubHeader = LM.getString('targetSupplier');

        /** data for "Select Supplier" field (InputTypeSearchList InputUtils component) */
        const selectSupplierProps = {
            title: LM.getString("selectSupplier"),
            options: [],
        };

        /** data for button "Save"  */
        const buttonSave = LM.getString('save');

        return (
            <Container className="copyPermissionsContainer">
                <Row >
                    <Col className="copyPermissionsHeader">
                        {header}
                    </Col>
                </Row>
                <Container className="copyPermissionsSettings">
                    <Row >
                        <Col className="copyPermissionsSubHeader">
                            {sourceSupplierSubHeader}
                        </Col>
                    </Row>
                    <Row>
                        <Col sm='4'>
                            <InputTypeSearchList
                                {...selectSupplierProps}
                                onChange={this.sourceSupplierChanged}
                                options={this.state.suppliers.map(supplier => supplier.name)}
                                ref={(componentObj) => { this.sourceSelectSupplier = componentObj }}
                                id="CopyPermissionsSourceSelectSupplier"
                            />
                        </Col>
                    </Row>

                    <Row >
                        <Col className="copyPermissionsSubHeader">
                            {targetSupplierSubHeader}
                        </Col>
                    </Row>
                    <Row>
                        <Col sm='4'>
                            <InputTypeSearchList
                                {...selectSupplierProps}
                                onChange={this.targetSupplierChanged}
                                options={this.state.suppliers.map(supplier => supplier.name)}
                                ref={(componentObj) => { this.targetSelectSupplier = componentObj }}
                                id="CopyPermissionsTargetSelectSupplier"
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="auto" >
                            <Button
                                className="copyPermissionsButton"
                                onClick={this.buttonSaveClicked}
                                id="CopyPermissionsSaveButton"
                            >
                                {buttonSave}
                            </Button>
                        </Col>
                    </Row>

                </Container>
            </Container>
        )
    }
}