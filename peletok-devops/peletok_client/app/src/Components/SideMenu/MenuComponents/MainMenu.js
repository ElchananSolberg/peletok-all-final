import React, { Component } from 'react';
import { LanguageManager as LM } from "../../LanguageManager/Language";
import { MenuLargeButton } from "../Utils/MenuLargeButton";
import { getReportList } from '../../DataProvider/DataProvider'
import IconInvoice from '../../../Assets/Icons/MenuIcons/invoice.svg'
import IconCharge from '../../../Assets/Icons/MenuIcons/charge.svg'
import IconStatus from '../../../Assets/Icons/MenuIcons/acount-status.svg'
import IconGift from '../../../Assets/Icons/MenuIcons/gift.svg'
import {UserDetailsService} from "../../../services/UserDetailsService";

export class MainMenu extends Component {
    constructor(props) {
        super(props);
        this.state =
            {
                reports: [],
                use_point: false
            };
    }

    componentDidMount() {
        getReportList().then(
            (res) => {
                let resList = [];
                Object.keys(res).forEach((key) => {
                    resList[key] = {reportName: res[key].reportName,
                                    name: res[key].reportName,
                                    id: res[key].id,
                                    data: res[key].data
                                };    
                });
                this.setState({ reports: resList })                
            }
        ).catch(
            (err) => {
                alert("getReportList failed: " + err)

            }
        );
        UserDetailsService.usePoint.subscribe(usePoint => {
            this.setState({ use_point: usePoint})
        });
        UserDetailsService.getUserDetails()
    }

    render() {
        return (
            <div className="rounded">
                <MenuLargeButton
                    path={`${this.props.match.url}/charge`}
                    icon={IconCharge}
                    text={LM.getString("charge")}
                    location={this.props.location}
                    toggle={this.props.toggle}
                />
                <MenuLargeButton id='sideMenuReportBtn'
                    list={this.state.reports}
                    path={`${this.props.match.url}/report`}
                    icon={IconStatus}
                    text={LM.getString("accountStatus")}
                    location={this.props.location}
                    toggle={this.props.toggle}
                />
                <MenuLargeButton id='sideMenuInvoiceBtn'
                    path={`${this.props.match.url}/invoice`}
                    icon={IconInvoice}
                    text={LM.getString("showInvoice")}
                    location={this.props.location}
                    toggle={this.props.toggle}
                />
                {this.state.use_point &&
                    <MenuLargeButton id='sideMenuGiftBtn'
                         list={[{
                             name: LM.getString("pointUsage"),
                             id: "pointUsage"
                         }, {name: LM.getString("cancelGift"), id: "cancelGift"}]}
                         path={`${this.props.match.url}/gifts`}
                         icon={IconGift}
                         text={LM.getString("gifts")}
                         location={this.props.location}
                         toggle={this.props.toggle}
                    />
                }
            </div>
        )
    }
}
