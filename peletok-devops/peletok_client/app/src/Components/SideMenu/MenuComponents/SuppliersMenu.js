import React, {Component} from 'react';
import {LanguageManager as LM} from "../../LanguageManager/Language";
import {MenuLargeButton} from "../Utils/MenuLargeButton";
import {MenuListButton} from "../Utils/MenuListButton";
import IconCharge from "../../../Assets/Icons/MenuIcons/charge.svg";
// TODO get updated SVGs
import IconGift from "../../../Assets/Icons/MenuIcons/gift.svg";

export class SuppliersMenu extends Component {
    render() {
        let mainConfigList = [
            {name: LM.getString("suppliers"), id: "suppliers"},
            {name: LM.getString("products"), id: "products"},
            {name: LM.getString("suppliersProfile"), id: "suppliersProfile"},
            {name: LM.getString("profitPercentageProfile"), id: "profitPercentageProfile/profit"},
            {name: LM.getString("commissionProfile"), id: "profitPercentageProfile/commission"},
            {name: LM.getString("catalogNumberManagement"), id:"catalogNumberManagement"},
            {name: LM.getString("manualCards"), id: "manualCards"},
        ];
        return (
            <div className="rounded" style={{overflow: "hidden"}}>
            <MenuLargeButton
                // list={mainConfigList}
                path={`${this.props.match.url}/suppliersAndProducts`}
                icon={IconCharge}
                text={LM.getString("suppliersAndProducts")}
                location={this.props.location}
                toggle={this.props.toggle}
            />
                {mainConfigList.map((obj) => <MenuListButton key={obj.id}
                            id={obj.id}
                            toggle={this.props.toggle}
                            {...obj}
                            path={`${this.props.match.url}/suppliersAndProducts`}
                            location={this.props.location} />)}
                <MenuListButton
                    path={`${this.props.match.url}/gifts`}
                    // icon={IconGift}
                    id="gifts"
                    name={LM.getString("gifts")}
                    location={this.props.location}
                    toggle={this.props.toggle}
                />
            </div>
        )
    }
}
