import React, { Component } from 'react';
import { LanguageManager as LM } from "../../LanguageManager/Language";
import { MenuLargeButton } from "../Utils/MenuLargeButton";
import { MenuListButton } from "../Utils/MenuListButton";
import IconCharge from "../../../Assets/Icons/MenuIcons/charge.svg";
import IconReports from '../../../Assets/Icons/MenuIcons/acount-status.svg'
// TODO get updated SVGs

export class ResellersMenu extends Component {
    render() {
        let menuItemList = [
            { name: LM.getString("business"), id: "business" },
            { name: LM.getString("contractInfo"), id: "contractInfo" },
            { name: LM.getString("suppliers"), id: "suppliers" },
            { name: LM.getString("profitPercentages"), id: "profitPercentages" },
            { name: LM.getString("commissions"), id: "commissions" },
            { name: LM.getString("userManagement"), id: "userManagement" }
        ];
       
        return (
            <div className="rounded" style={{ overflow: "hidden" }}>
                <MenuLargeButton
                    // list={menuItemList}
                    path={`${this.props.match.url}/sellersAndDistributors`}
                    icon={IconCharge}
                    text={LM.getString("sellersAndDistributors")}
                    location={this.props.location}
                    toggle={this.props.toggle}
                />
                {menuItemList.map((obj) => <MenuListButton key={obj.id}
                            id={obj.id}
                            toggle={this.props.toggle}
                            {...obj}
                            path={`${this.props.match.url}/sellersAndDistributors`}
                            location={this.props.location} />)}
              
                <MenuListButton
                    path={`${this.props.match.url}/locked`}
                    // icon={IconCharge}
                    id="locked"
                    name={LM.getString("locked")}
                    location={this.props.location}
                    toggle={this.props.toggle}
                />
            </div>
        );
    }
}