import React, { Component } from 'react';
import { LanguageManager as LM } from "../../LanguageManager/Language";
import { MenuLargeButton } from "../Utils/MenuLargeButton";
import { MenuListButton } from "../Utils/MenuListButton";
import IconCharge from "../../../Assets/Icons/MenuIcons/charge.svg";
import IconReports from '../../../Assets/Icons/MenuIcons/acount-status.svg'
// TODO get updated SVGs

export class CancelMenu extends Component {
    render() {
        let electricityCompanyList = [
            { name: LM.getString("electricityCompany") + " " + LM.getString("confirmDetails"), id: "confirmDetails" },
            { name: LM.getString("electricityCompany") + " " + LM.getString("retrieveDetails"), id: "retrieveDetails" },
        ];
        let localPaymentEndList = [
            { name: LM.getString("chargeCancelOrConfirmation"), id: "chargeCancelOrConfirmation" },
        ];
        return (
            <div className="rounded" style={{ overflow: "hidden" }}>
                <MenuLargeButton
                    path={`${this.props.match.url}/chargeCancel`}
                    icon={IconCharge}
                    text={LM.getString("chargeCancel")}
                    location={this.props.location}
                    toggle={this.props.toggle}
                />
                {electricityCompanyList.map((obj) => <MenuListButton key={obj.id}
                            id={obj.id}
                            toggle={this.props.toggle}
                            {...obj}
                            path={`${this.props.match.url}/electricityCompany`}
                            location={this.props.location} />)}
                {/* <MenuLargeButton
                    list={electricityCompanyList}
                    path={`${this.props.match.url}/electricityCompany`}
                    icon={IconCharge}
                    text={LM.getString("electricityCompany")}
                    location={this.props.location}
                    toggle={this.props.toggle}
                /> */}
                {localPaymentEndList.map((obj) => <MenuListButton key={obj.id}
                            id={obj.id}
                            toggle={this.props.toggle}
                            {...obj}
                            path={`${this.props.match.url}/endLocalPayment`}
                            location={this.props.location} />)}
                {/* <MenuLargeButton
                    list={localPaymentEndList}
                    path={`${this.props.match.url}/endLocalPayment`}
                    icon={IconCharge}
                    text={LM.getString("endLocalPayment")}
                    location={this.props.location}
                    toggle={this.props.toggle}
                /> */}
                <MenuListButton
                    path={`${this.props.match.url}/UnpaidPayments`}
                    // path={this.props.path}
                    // icon={IconCharge}
                    id="UnpaidPayments"
                    name={LM.getString("UnpaidPayments")}
                    location={this.props.location}
                    toggle={this.props.toggle}
                />
                <MenuListButton
                    path={`${this.props.match.url}/CancelTransactions`}
                    // path={this.props.path}
                    // icon={IconCharge}
                    id="CancelTransactions"
                    name={LM.getString("CancelTransactions")}
                    location={this.props.location}
                    toggle={this.props.toggle}
                />
            </div>
        );
    }
}