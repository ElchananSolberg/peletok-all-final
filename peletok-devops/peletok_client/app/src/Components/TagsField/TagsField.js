import React, { Component } from 'react';
import Tag from '../InputUtils/Tag';
import { Row, Col } from 'reactstrap';
class TagsField extends Component {

    constructor(props) {
        super(props);

        this.state = {
            renderTag: true,
            tags: []
        }
       
    }
    setValue = (value) => {
        this.setState({
            renderTag: false,
            tags: value || []
        },()=>{
            this.setState({
                renderTag: true
            })
        })
    }

    getValue = () => {
        return { valid: true, value: JSON.stringify(this.state.tags) };
    }

    getAddHandler = (tagId) => {
        return (e) => {
            const isExist = this.state.tags.find((t) => t == tagId);
            if(!isExist){
                this.setState({
                    tags: [...this.state.tags, tagId]
                })
            }
        }
    }

    getRemoveHandler = (tagId) => {
        return (e) => {
            this.setState({
                tags:  this.state.tags.filter(t=>t!=tagId)
            })
        }
    }

    render(){
        return (
            <Row className="align-items-end">
                {this.props.tags.map((tag, i) => {
                    return <Col sm='auto' key={i}>
                             {this.state.renderTag && <Tag
                                 buttonText={tag.name}
                                 id={`tag-${i}`}
                                 disabled={this.props.disabled} 
                                 default={this.state.tags.find(t=>t==tag.id)}
                                 onClick={this.getAddHandler(tag.id)}
                                 onBatgeClick={this.getRemoveHandler(tag.id)}
                                 />}
                            </Col>
                })}
            </Row>)
    }
}

export default TagsField

                        