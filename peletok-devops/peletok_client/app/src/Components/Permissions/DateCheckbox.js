import React from 'react';
import './Permissions.css'
import DatePicker from "react-datepicker";
import InputTypeCheckBox from '../InputUtils/InputTypeCheckBox';
import PropTypes from 'prop-types'
import moment from 'moment'

export default class DateCheckbox extends React.Component {

    span = null;
    checkBox = null;

    constructor(props) {
        super(props);


        this.state = {
            showDate: false
        };
    }

    static propTypes = {
      onChange: PropTypes.func.isRequired,
      checked: PropTypes.bool,
      id: PropTypes.string
    };

    resetCheckboxAndCloseDatePicker = (e)=>{
        if(e.target.id == 'layer'){
            this.checkBox.setValue(false);
            this.setState({showDate: false})
        }
        
    }

    render() {
        return (
            <span className="DateCheckbox" ref={span=>this.span=span}>
            <InputTypeCheckBox 
                checked={this.props.checked} 
                id={this.props.id}
                ref={obj=>this.checkBox=obj} 
                onChange={
                    (e)=>{
                        if(e.target.checked){
                            document.getElementsByTagName('BODY')[0].style.overflow = 'hidden';
                            this.setState({showDate: true})
                        }else{
                            this.props.onChange(e)
                        }
                    }
                } 
            />
              { this.state.showDate &&
                
                    
                    <div id="layer" onClick={this.resetCheckboxAndCloseDatePicker}>
                        <div style={{
                                position: 'absolute',
                                top: this.span.getBoundingClientRect().top + 'px',    
                                left: this.span.getBoundingClientRect().right + 'px'
                            }}>
                            <DatePicker
                                inline
                                minDate={moment().toDate()}
                                onChange={
                                    (date)=>{
                                        this.props.onChange(date)
                                        document.getElementsByTagName('BODY')[0].style.overflow = 'scroll';
                                        this.setState({showDate: false})
                                    }
                                }
                            />
                        </div>                                  
                    </div> 
                
            }
            </span>
        );
    }
}
