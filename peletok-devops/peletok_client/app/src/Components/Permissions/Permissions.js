import React from 'react';
import { Button, Row, Col, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

import './Permissions.css';

import { LanguageManager as LM } from '../LanguageManager/Language';

import InputTypeSearchList from '../InputUtils/InputTypeSearchList';
import InputTypeText from '../InputUtils/InputTypeText';

import PermissionsTable from './PermissionsTable'
import { PermissionsEnum } from '../../enums'
import { PermissionsProvider, PermissionsContext } from './permissionsContext';

export class Permissions extends React.Component {
  constructor(props) {
    super(props);
    
    this.state = {
      display: true,
      inputTypeTextProps: {
        tooltip: '',
        title: LM.getString("permissionProfileName"),
        id: "permissionProfileName",
        required: true,
        maxLength: 30,
      },
      /** working variable that indicates if a user is on the stage of viewing an add role modal window */
      isModalOpen: false,
    };
    this.fields = {};
  }



  addRoleClicked = () => {
    this.toggleAddRoleModal();
  }
  /** toggle Add Role Modal window */
  toggleAddRoleModal = () => {
    this.setState({ isModalOpen: !this.state.isModalOpen });
  }

  render() {
    /** data for "Role Name" field (InputTypeText InputUtils component) */
    const roleNameProps = {
      required: true,
      title: LM.getString("roleName") + ":",
      id: "PermissionsRoleName",
    };

    return (
      <PermissionsProvider>
        <PermissionsContext.Consumer>
          {({ roles,
            setRole,
            roleId,
            permissionActions,
            permissions,
            save,
            permissionGroups,
            permissionLableCellWidth,
            permissionTempEditCellWidth,
            loading,
            saveRole,
            registerToggleAddRoleModal,
            linkedRoles,
            linkRoleSelectChange
          }) => (
              !loading &&
              <div className="firstDivP">
                <h3 className="firstDivString">  {LM.getString("permissionManagement")}</h3>
                <div className="workers" >
                  <Row className="align-items-center">
                    <Col sm="4">
                      <InputTypeSearchList
                        title={LM.getString("permissionProfileName")}
                        options={roles.map(role => role.name)}
                        default={roles.find(role => role.id == roleId).name}
                        onChange={(e) => {
                          setRole(e).then((roleId) => {
                            this.setState({
                              display: false
                            }, () => {
                              this.setState({
                                display: true
                              })
                            })
                          })
                        }}
                        id="roles"
                      />
                    </Col>
                    <Col sm="auto" className="permissionsAddRole" onClick={this.addRoleClicked}>
                      {LM.getString('addRole')}
                    </Col>
                    
                    <Modal isOpen={this.state.isModalOpen} toggle={this.toggleAddRoleModal} >
                      <ModalHeader className="hashavshevetModalHeader"> {LM.getString('roleAddition')} </ModalHeader>
                      <ModalBody className="hashavshevetModalBody" >
                        <Row>
                          <Col >
                            <InputTypeText
                              {...roleNameProps}
                              ref={(componentObj) => { this.fields.roleName = componentObj }}
                            />
                          </Col>
                        </Row>

                      </ModalBody>
                      <ModalFooter className="hashavshevetModalFooter">
                        <Row>
                          <Col sm="auto">
                            <Button
                              className="hashavshevetButton"
                              onClick={saveRole(this.fields)}
                            >
                              {LM.getString('save')}
                            </Button>
                            {' '}
                          </Col>
                          <Col sm="auto">
                            <Button
                              className="hashavshevetButton hashavshevetButtonCancel"
                              onClick={(()=>{registerToggleAddRoleModal(this.toggleAddRoleModal); return this.toggleAddRoleModal})()}
                            >
                              {LM.getString('cancel')}
                            </Button>
                          </Col>
                        </Row>
                      </ModalFooter>
                    </Modal>
                  </Row>
                  {this.state.display && <Row style={{ marginTop: '30px' }}>
                      <Col >
                          <InputTypeSearchList
                              multi={true}
                              default={linkedRoles.length > 0 ? linkedRoles : null}
                              title={LM.getString('allowedToRole')}
                              id="allowedToRole"
                              options={roles.map(r=>r.name)}
                              ref={(componentObj) => { this.fields['rolesSelect'] = componentObj }}
                              onChange={linkRoleSelectChange(this.fields)} 
                              />
                      </Col>
                  </Row>}
                  <hr width="95%" />
                  {this.state.display &&
                    <PermissionsTable
                      actionNames={[{ style: { width: permissionLableCellWidth }, title: LM.getString('permissions') },
                      ...Object.keys(permissionActions).map(key => { return { style: { width: parseInt(key) === PermissionsEnum.ACTION_TEMP_EDIT ? permissionTempEditCellWidth : '' }, title: permissionActions[key], } })]}
                      permissions={permissions}
                      permissionGroups={permissionGroups}
                    >

                    </PermissionsTable>
                  }
                  <Row>
                    <Col lg="2" id={"payCheckedInvoices"} style={{ marginTop: '15px', }}>
                      <Button style={{ width: '100%', overflow: 'hidden' }} className="commisionProfileButton " onClick={save}> {LM.getString('save')} </Button>
                    </Col>
                  </Row>
                </div>
              </div>
            )}
        </PermissionsContext.Consumer>
      </PermissionsProvider>



    );
  }
}