// Dependencies
import React, { Component, createContext } from 'react';
import { arrayToObject } from '../../utilities/utilities'
import { LanguageManager as LM } from '../LanguageManager/Language';
import { PermissionsEnum } from '../../enums'
import {
  getPermissionRoles, getPermissions, getRole,
  postRole, postPermissionRoles
} from '../DataProvider/DataProvider'
import { Notifications } from '../Notifications/Notifications'
import { Loader } from '../Loader/Loader'
// Context
export const PermissionsContext = createContext();

export class PermissionsProvider extends Component {

  // Class attributes
  permissionRoles = [];
  roles = [];
  permissionActions = {};
  permissions = [];
  permissionLableCellWidth = '50%';
  permissionTempEditCellWidth = '25%';
  permissionGroups = {};
  toggleAddRoleModal = null;
  rolesWithAllowd = []

  state = {
    roleId: null,
    groupsCollapse: {},
    permissionsRolesForm: {},
    loading: true,
    linkedRoles: [],
  }


  save = () => {
    let data = {
      roleId: this.state.roleId,
      permissionRolesForm: this.state.permissionsRolesForm,
      roles: this.state.linkedRoles
    }
    Loader.show()
    postPermissionRoles(data).then((res) => {
      if (res == 'success') {
        Notifications.show(LM.getString('saved_successfully'), 'success')
        this.initData(this.state.roleId)
      } else {
        Notifications.show(LM.getString('error_while_saving'), 'danger')
      }
      Loader.hide()
    })
  }

  // Manage collapse state of all group in context children
  // Return promise that resolved after setState will happened
  setGroupsCollapse = (groupName) => {
    return new Promise((resolve) => {
      this.setState({
        groupsCollapse: Object.assign(this.state.groupsCollapse, { [groupName]: !this.state.groupsCollapse[groupName] })
      }, () => { resolve(groupName) })
    })
  }

  registerToggleAddRoleModal = (func) => {
    this.toggleAddRoleModal = func;
  }

  linkRoleSelectChange = (fields) => {
    return ()=>{
        this.setState({
            linkedRoles: fields.rolesSelect.getValue().value
        })
    }
  }

  saveRole = (fields) => {
    return () => {

      if (fields.roleName.getValue().valid) {
        const newName = fields.roleName.getValue().value;
        if (!this.roles.find(t => (t.name === newName))) {
          Loader.show();
          postRole({
            name: newName
          }).then(
            res => {
              Loader.hide();
              Notifications.show(LM.getString('successProductRoleAdding') + newName, 'success');
              this.toggleAddRoleModal();
              this.initData(res.id)
            }
          ).catch(
            err => {
              Loader.hide();
              Notifications.show(err, 'danger');
            }
          );
        }
        else {
          Notifications.show(LM.getString('roleName') + " " + LM.getString('isAlreadyExist'), 'warning')
        }
      }
    }
  }

  // Change the current edited role
  setRole = (name) => {
    return new Promise((resolve) => {
      let roleId = this.roles.find(role => role.name == name).id
      let roleWithAllowed = this.rolesWithAllowd.find(r=>r.id == roleId)
      this.setState({
        roleId: roleId,
        permissionsRolesForm: this.permissionRoles[roleId] ? arrayToObject(this.permissionRoles[roleId], 'permissionId') : {},
        linkedRoles: roleWithAllowed ? roleWithAllowed.AllowedRoles.map(r=>r.name) : []
      })
      resolve(roleId)
    })
  }


  updateForm = (actionId, permissionId, groupName) => {
    let newForm;
    return (e) => {
      if (e.target && e.target.checked || (!e.target && e instanceof Date)) {
        newForm = [...this.state.permissionsRolesForm[permissionId] || []]
        newForm = this._addPermissionRole(permissionId, actionId, newForm, e instanceof Date ? e : null);

      } else {
        newForm = this.state.permissionsRolesForm[permissionId].filter(pr => !PermissionsEnum.mapPermisionRoleToAction(pr, actionId));
      }
      this.setState({
        permissionsRolesForm: Object.assign(this.state.permissionsRolesForm, { [permissionId]: newForm })
      })
      // Force update by toggle collapse, ugly but work
      this.setGroupsCollapse(groupName).then((name) => { this.setGroupsCollapse(name) })

    }
  }

  // Update specific action for all permissions in the group
  updateAll = (permissionIds, actionId, groupName) => {
    let newForm = {};
    permissionIds.forEach(id => {
      let tempNewForm = [...this.state.permissionsRolesForm[id] || []]
      newForm[id] = this._addPermissionRole(id, actionId, tempNewForm)
    });

    this.setState({
      permissionsRolesForm: Object.assign(this.state.permissionsRolesForm, newForm)
    })
    // Force update by toggle collapse, ugly but work
    this.setGroupsCollapse(groupName).then((name) => { this.setGroupsCollapse(name) })


  }

  _addPermissionRole = (permissionId, actionId, clonedForm, date) => {
    // If action doesn't exist in the group we should add it
    if (!clonedForm.find(p => PermissionsEnum.mapPermisionRoleToAction(p, actionId))) {

      // If permission exist for current role we sould delete and create 
      // it again to prevent existence of two permission with difrent action.
      clonedForm = clonedForm.filter(p => p.permissionId != permissionId);

      clonedForm.push(
        {
          roleId: this.state.roleId,
          permissionId: permissionId,
          action: date ? PermissionsEnum.ACTION_EDIT : parseInt(actionId),
          date: date
        });


    }
    return clonedForm;
  }

  // Delete specific action for all permissions in the group
  deleteAll = (permissionIds, actionId, groupName) => {
    let newForm = {};
    permissionIds.forEach(id => {
      let tempNewForm = this.state.permissionsRolesForm[id].filter(pr => !PermissionsEnum.mapPermisionRoleToAction(pr, actionId))
      newForm[id] = tempNewForm;
    });

    this.setState({
      permissionsRolesForm: Object.assign(this.state.permissionsRolesForm, newForm)
    })
    // Force update by toggle collapse, ugly but work
    this.setGroupsCollapse(groupName).then((name) => { this.setGroupsCollapse(name) })


  }


  async componentDidMount() {
    this.permissionActions = {
      [PermissionsEnum.ACTION_SHOW]: LM.getString('show'),
      [PermissionsEnum.ACTION_EDIT]: LM.getString('edit'),
      [PermissionsEnum.ACTION_TEMP_EDIT]: LM.getString('temp_edit'),
    }
    await this.initData()

  }
  
  initData = async (roleId = null) => {
    const [permissionsResponse, permissionGroupsResponse, rolesResponse] = await Promise.all([
      getPermissions(),
      getPermissionRoles(),
      getRole()
    ]);

    this.permissionGroups = permissionsResponse.permissionGroups.reduce((prevPgs, pg) => {
      prevPgs[pg.id] = pg.name;
      return prevPgs;
    }, {})
    this.rolesWithAllowd = permissionsResponse.roles
    this.permissions = arrayToObject(permissionsResponse.permissions, 'permissionGroupId')

    permissionGroupsResponse.forEach(pr => pr.action = PermissionsEnum.permissionTypeTextToNumber(pr.action))
    this.permissionRoles = arrayToObject(permissionGroupsResponse, 'roleId');

    this.roles = rolesResponse;
    let roleWithAllowed = this.rolesWithAllowd.find(r=>r.id == (roleId || this.roles[0].id))
    this.setState({
      loading: false,
      roleId: roleId || this.roles[0].id,
      permissionsRolesForm: arrayToObject(this.permissionRoles[roleId || this.roles[0].id], 'permissionId'),
      linkedRoles: roleWithAllowed ? roleWithAllowed.AllowedRoles.map(r=>r.name) : []
    })
  }


  render() {
    const { children } = this.props;

    return (
      <PermissionsContext.Provider
        value={{
          registerToggleAddRoleModal: this.registerToggleAddRoleModal,
          saveRole: this.saveRole,
          linkRoleSelectChange: this.linkRoleSelectChange,
          save: this.save,
          setGroupsCollapse: this.setGroupsCollapse,
          permissionActions: this.permissionActions,
          setRole: this.setRole,
          updateAll: this.updateAll,
          deleteAll: this.deleteAll,
          updateForm: this.updateForm,
          roles: this.roles,
          permissionGroups: this.permissionGroups,
          permissions: this.permissions,
          permissionLableCellWidth: this.permissionLableCellWidth,
          permissionTempEditCellWidth: this.permissionTempEditCellWidth,
          ...this.state
        }}
      >
        {children}
      </PermissionsContext.Provider>
    );
  }
}
