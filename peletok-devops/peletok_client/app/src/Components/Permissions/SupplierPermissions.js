import React from 'react';
import { Button, Row, Col, Container } from 'reactstrap';
import './Permissions.css';
import { LanguageManager as LM } from '../LanguageManager/Language';
import InputTypeSelect from '../InputUtils/InputTypeSelect';
import InputTypeRadio from '../InputUtils/InputTypeRadio';
  

/**
 * Component that handles the suppliers' permissions
 * TODO --Produce all functionality
 */
export class SupplierPermissions extends React.Component {
    constructor(props) {
        super(props);


        /**
         *  TODO -- The   providers list will come from the database and will not remain as a hard code
         */
        this.state = {

            providersSupplier: {

                title: LM.getString("supplierSelect"),
                id: "supplierSelect",
                options: ['All', 'avi', 'achmad', 'gabi', 'alex', 'noy', 'david', ' muchamaad'],
                default: "All",
                placeholder: 'Type or Select',
                tooltip: ''

            },
            providersSector: {

                title: LM.getString("providersSector"),
                id: "providersSector",
                options: ['All', 'avi', 'achmad', 'gabi', 'alex', 'noy', 'david', ' muchamaad'],
                default: "All",
                placeholder: 'Type or Select',
                tooltip: ''
            },
            providersStoreType: {

                title: LM.getString("providersStoreType"),
                default: "All",
                id: "providersStoreType",
                options: ['All', 'avi', 'achmad', 'gabi', 'alex', 'noy', 'david', ' muchamaad'],
                tooltip: '',
                placeholder: 'Type or Select',

            },

            PermissionsMarketer: {
                lableText: LM.getString("PermissionsMarketer"),
                id: 'PermissionsMarketer',
                maxLength: "int",
                tooltip: "",
                placeholder: 'Type or Select',

            },

            PermissionsDistributor1: {
                lableText: LM.getString("PermissionsDistributor"),
                id: 'PermissionsDistributor',
                maxLength: "int",
                tooltip: "",
                placeholder: 'Type or Select',

            },
            inputTypeRadioProps: {
                tooltip: "",
                options: [LM.getString("PermissionsDistributor"), LM.getString("PermissionsMarketer"), LM.getString("permissionAll")],
                default: LM.getString("permissionAll"),
                title: LM.getString("PermissionsSelectCustomerType"),
                id: "testRadioId-",
                required: true,
            },

            PermissionsDistributor: {
                lableText: LM.getString("PermissionsDistributor"),
                id: 'PermissionsDistributor',
                maxLength: "int",
                tooltip: "",
                placeholder: 'Type or Select',

            },

        };
    }


    /** TODO  add functionality */

    permissionChange = (e) => {
      

        e.persist();

    }

    /** TODO  add functionality */

    MarkAll = (e) => {
      

        e.persist();

    }
    /** TODO  add functionality */

    Clear = (e) => {
     

        e.persist();

    }
    /** TODO  add functionality */

    save = (e) => {
      

        e.persist();

    }
    /** TODO  add functionality */

    providersSupplierChange = (e) => {
      

        e.persist();

    }
    /** TODO  add functionality */

    providersSectorChange = (e) => {
      

        e.persist();

    }
    /** TODO  add functionality */

    providersStoreTypeChange = (e) => {
      

        e.persist();

    }
    /** TODO  add functionality */

    radioMarketer = (e) => {
       

        e.persist();

    }
    /** TODO  add functionality */

    radioAll = (e) => {
     

        e.persist();

    }
    /** TODO  add functionality */

    radioDistributor = (e) => {
      

        e.persist();

    }


    render() {
        return (<div className="firstDivP">
            <p style={{ fontSize: "1.5rem" }} >  {LM.getString("SupplierPermissions")}</p>

            <div className="secendDiv" >

                <Container>
                    <Row>

                        <Col style={{ marginTop: '15px' }} sm='4'>
                            <InputTypeSelect {...this.state.providersSupplier} />
                        </Col>
                    </Row>
                    <Row>
                        <Col style={{ marginTop: '15px' }} sm='4'>
                            <InputTypeSelect {...this.state.providersSector} />
                        </Col>
                        <Col style={{ marginTop: '15px' }} sm='4'>
                            <InputTypeSelect {...this.state.providersStoreType} />
                        </Col>
                    </Row>
                </Container>
                <InputTypeRadio  {...this.state.inputTypeRadioProps} />

                <div >

                    <Button onClick={this.save} id="PermissionsApplyPermission" className="PermissionsApplyPermission"
                        style={{ marginTop: '15px', marginLeft: '20px', width: "max-content", backgroundColor: "var(--selectorBackground)", color: "#16577d", borderColor: "#48dedb" }}
                    >
                        {LM.getString("PermissionsApplyPermission")}
                    </Button>
                </div>
                <div>
                    <Button id='PermissionsUndoPermission' color="secondary" onClick={this.save} style={{ marginTop: '15px', marginLeft: '20px', width: "max-content" }} >
                        {LM.getString("PermissionsUndoPermission")}
                    </Button>
                </div>
            </div>
        </div>
        );
    }
}