import React, { Component } from 'react';
import './Config_Commissions.css';
import { Button, Container, Row, Col } from 'reactstrap';

import { LanguageManager as LM } from '../LanguageManager/Language';
import { Notifications } from '../Notifications/Notifications'

import { getSuppliers, getProducts } from '../DataProvider/DataProvider'

import InputTypeRadio from '../InputUtils/InputTypeRadio';
import InputTypeSearchList from '../InputUtils/InputTypeSearchList';
import InputTypeNumber from '../InputUtils/InputTypeNumber';
import Slider from '../InputUtils/Slider';

/**
 * Main component of Config_Commissions screen.  
 */
export default class Config_Commissions extends Component {
    constructor(props) {
        super(props);

        this.state = {
            /** working variable that contain end of a selected service radio button id */
            selectedServiceId: '0',
            /** working variable that will contain an array of suppliers from the server */
            suppliers: [],
            /** working variable that will contain an object (object of arrays of objects) of all the selected supplier products */
            currentSupplierProducts: {},
            /** working variable that will contain a selected product object */
            selectedProduct: {},
        }
    }

    /** "Select Service Type" field onChange handler.
    * It saves the END OF a selected service radio button ID into selectedServiceId variable. 
    * At the end it clears a value of the 'Select Product' field.
    */
    serviceTypeChanged = (e) => {
        this.setState({ selectedServiceId: e.target.id.split("-")[1] });
        this.selectProduct.setValue("");
    }

    /** "Select Supplier" field onChange handler
    */
    supplierChanged = (e) => {
        let value = this.selectSupplier.getValue().value;
        let selectedSupplier = this.state.suppliers.find(element => (element.name === value));

        getProducts(selectedSupplier.id, {}, true)
            .then(
                (res) => {
                    this.setState({ currentSupplierProducts: res });
                    this.selectProduct.setValue("");
                })
            .catch(
                (err) => {
                    Notifications.show(err, 'danger')
                }
            )

    }
    /** "Select Product" field onChange handler */
    productChanged = (e) => {
        let value = this.selectProduct.getValue().value;
        let selectedProduct = this.state.currentSupplierProducts.bills_payment.find(element => (element.name === value))
            ||
            this.state.currentSupplierProducts.bills_payment_commission_varies.find(element => (element.name === value));
        if (selectedProduct) {
            this.setState({ selectedProduct: selectedProduct });
        }
    }

    /** Function that prepares (according to the selected service) a list of product names for the "Select Product" fields */
    productsLoad = (selectedServiceId, currentSupplierProducts) => {
        if (Object.keys(currentSupplierProducts).length !== 0) {
            let billsPayment = currentSupplierProducts.bills_payment ? currentSupplierProducts.bills_payment.map(product => product.name) : [];

            let billPaymentCommissionVaries = currentSupplierProducts.bills_payment_commission_varies ? currentSupplierProducts.bills_payment_commission_varies.map(product => product.name) : [];


            if (selectedServiceId === '0') {
                return billsPayment.concat(billPaymentCommissionVaries);
            }
            if (selectedServiceId === '1') {
                return billsPayment;
            }
            if (selectedServiceId === '2') {
                return billPaymentCommissionVaries;
            }
        }
        return false
    }

    componentDidMount() {
        let forceUpdate = false;
        getSuppliers(forceUpdate).then(
            (res) => {
                this.setState({ suppliers: Object.keys(res).reduce((newArr, key) => [...newArr, ...res[key]], []) })
            }
        ).catch(
            (err) => {
                Notifications.show("loadSuppliersList failed: " + err, 'danger')
            }
        )
        // getSuppliersList(forceUpdate).then(
        //     (res) => {
        //         this.setState({ suppliers: res });
        //     }
        // ).catch(
        //     (err) => {
        //         Notifications.show(LM.getString("loadSuppliersListFailed") + ": " + err, 'danger')
        //     }
        // )
    }

    render() {
        /** Screen Header text */
        const header = LM.getString('commissions');

        /** data for "ServiceType" field (<InputTypeRadio/> component) */
        const serviceTypeProps = {
            title: LM.getString('serviceType') + ":",
            options: LM.getString('serviceTypeOptions1'),
            default: LM.getString('serviceTypeOptions1')[0],
            id: "Config_CommissionsServiceType-",
        };
        /** data for 'Client Type' field (<InputTypeRadio/> component) */
        const clientTypeProps = {
            title: LM.getString("clientType"),
            options: LM.getString("clientTypeOptions"),
            id: "Config_CommissionsClientType-",
        };

        /** data for "Select Supplier" field (InputTypeSearchList InputUtils component) */
        const selectSupplierProps = {
            title: LM.getString("selectSupplier"),
            options: [],
            id: "Config_CommissionsSelectSupplier",
        };
        /** data for "Select Product" field (InputTypeSearchList InputUtils component) */
        const selectProductProps = {
            title: LM.getString("selectProductFromList"),
            options: [],
            id: "Config_CommissionsSelectProduct",
        };

        /** data for "Commission" field (InputTypeNumber InputUtils component) */
        const commissionProps = {
            title: LM.getString("commission")+":",
            id: "Config_CommissionsCommission",
        };
        /** data for "Final Commission" field (InputTypeNumber InputUtils component) */
        const finalCommissionProps = {
            title: LM.getString("finalCommission") + ':',
            id: "Config_CommissionsFinalCommission",
        };

        const costRangeProps = {
            id: 'Config_CommissionsCostRange',
            title: LM.getString("costRange") + ':',
            defaultValue: '0',
            min: '0',
            max: '999',
        };

        /** data for "Spread Commission On All Sellers" button */
        const buttonSpreadCommissionOnAllSellers = LM.getString('spreadCommissionOnAllSellers');
        /** data for "Spread Final Commission On All Sellers" button */
        const buttonSpreadFinalCommissionOnAllSellers = LM.getString('spreadFinalCommissionOnAllSellers');
        /** data for "Spread permission to use products from resellers */
        const buttonSpreadPermissionOnSellers = LM.getString("spreadPermission");
        /** data for "Spread Final Commission On All Sellers" button */
        const buttonRemovePermissionForSellers = LM.getString("removePermission")

        return (
            <Container className="config_CommissionsContainer" >
                <Row >
                    <Col className="config_CommissionsHeader">
                        {header}
                    </Col>
                </Row>
                <Container className="config_CommissionsSettings">
                    <Row >
                        <Col lg="8">
                            <InputTypeRadio
                                {...serviceTypeProps}
                                onChange={this.serviceTypeChanged}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm='4'>
                            <InputTypeSearchList
                                {...selectSupplierProps}
                                onChange={this.supplierChanged}
                                options={this.state.suppliers.map(supplier => supplier.name)}
                                ref={(componentObj) => { this.selectSupplier = componentObj }}
                            />
                        </Col>
                        <Col sm='4'>
                            <InputTypeSearchList
                                {...selectProductProps}
                                onChange={this.productChanged}
                                options={this.productsLoad(this.state.selectedServiceId, this.state.currentSupplierProducts) || ['']}
                                ref={(componentObj) => { this.selectProduct = componentObj }}
                            />
                        </Col>
                    </Row>
                    <Row >
                        <Col sm='4'>
                            <InputTypeNumber
                                {...commissionProps}
                                ref={(componentObj) => { this.commission = componentObj }}
                            />
                        </Col>
                        <Col sm='4'>
                            <InputTypeNumber
                                {...finalCommissionProps}
                                ref={(componentObj) => { this.finalCommission = componentObj }}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col  >
                            <InputTypeRadio
                                {...clientTypeProps}
                                onChange={this.clientTypeChanged}
                                ref={(componentObj) => { this.clientType = componentObj }}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm='8'>
                            <Slider
                                {...costRangeProps}
                                ref={(componentObj) => { this.costRange = componentObj }}
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="auto" >
                            <Button
                                className="config_CommissionsButton"
                                onClick={this.buttonSpreadCommissionOnAllSellersClicked}
                                id="Config_Commissions_ButtonSpreadCommissionOnAllSellers"
                            >
                                {buttonSpreadCommissionOnAllSellers}
                            </Button>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="auto" >
                            <Button
                                className="config_CommissionsButton"
                                onClick={this.buttonSpreadFinalCommissionOnAllSellersClicked}
                                id="Config_Commissions_ButtonSpreadFinalCommissionOnAllSellers"
                            >
                                {buttonSpreadFinalCommissionOnAllSellers}
                            </Button>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="auto" >
                            <Button
                                className="config_CommissionsButton"
                                onClick={this.buttonSpreadPermissionOnSellersClicked}
                                id="Config_Commissions_ButtonSpreadPermissionOnSellers"
                            >
                                {buttonSpreadPermissionOnSellers}
                            </Button>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="auto" >
                            <Button
                                className="config_CommissionsButton config_CommissionsButtonDark"
                                onClick={this.buttonRemovePermissionForSellersClicked}
                                id="Config_Commissions_ButtonRemovePermissionForSellers"
                            >
                                {buttonRemovePermissionForSellers}
                            </Button>
                        </Col>
                    </Row>
                </Container>
            </Container>
        )
    }

}