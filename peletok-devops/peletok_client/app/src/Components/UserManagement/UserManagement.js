import React, { Component } from 'react';
import { Container } from 'reactstrap'
import { UsersScreen } from '../selectUser/selectUser';
import { LanguageManager as LM } from '../LanguageManager/Language';
import { getUsersForManager } from "../DataProvider/Functions/DataGeter";
import { SelectedSeller } from '../SelectedSeller/SelectedSeller';




/**
 * Print component  Should receive a table
 *  and titleOfButton in props
 * And prints the information she received in a table
 */

export class UserManagement extends Component {

    constructor(props) {
        super(props);
        this.state = {
            value: [],
            /** working variable that will contain a selected distributor id */
            distributorID: -1,
            /** working variable that will contain a selected distributor object */
            selectedDistributor: {},
            /** working variable that will contain a selected seller object */
            selectedSeller: {},
            /** render is a sloppy way to make the page rerender */
            render: true
        };
    }

    prepareFilterAndLoadUsers = () => {
        if (this.state.distributorID && this.state.distributorID !== -1) {
            let filter = {
                distributor: this.state.distributorID,
                business: this.state.selectedSeller.id,
            };
            getUsersForManager(filter).then(res => {
                let users = res.result
                this.setState({ value: users }, () => {
                    this.setState({ render: false }, () => {
                        this.setState({ render: true })
                    })
                })
            })
        }
    }

    componentDidMount() {
        SelectedSeller.setHeader.next(LM.getString('userManagement'));
        if (SelectedSeller.newSellerState) SelectedSeller.createSellerClicked.next();
        if (SelectedSeller.newDistributorState) SelectedSeller.createDistributorClicked.next();
        if (SelectedSeller.currentSeller.getValue().id && SelectedSeller.currentDistributor.getValue().id)
            this.setState({ selectedSeller: SelectedSeller.currentSeller.getValue(), selectedDistributor: SelectedSeller.currentDistributor.getValue(), distributorID: SelectedSeller.currentDistributor.getValue().distributor_id },
                () => this.prepareFilterAndLoadUsers()
            );
        SelectedSeller.currentSeller.subscribe(x => {
            if(x.id){
                this.setState({ selectedSeller: x }, () => this.prepareFilterAndLoadUsers())
            }
        });
        SelectedSeller.currentDistributor.subscribe(x => {
            if(x.id){
                this.setState({ selectedDistributor: x, distributorID: x.distributor_id, value: [] })
            }
        });
    }

    render() {
        return (
            <Container className="businessContainer">
                {SelectedSeller.currentSeller.getValue().id && SelectedSeller.currentDistributor.getValue().id && this.state.render && this.state.value?
                    <UsersScreen Managment={true} ourUsers={this.state.value} updateUsers={this.prepareFilterAndLoadUsers}
                        currentDistributor={this.state.selectedSeller.id} />
                    : <></>
                }
            </Container>
        )
    }

}