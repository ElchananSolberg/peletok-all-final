import React, { Component } from 'react';
import { CustomInput, FormGroup } from 'reactstrap';
import { LanguageManager as LM, LanguageManager } from "../LanguageManager/Language"


export class LangButtons extends Component {
    // This class handles all languages
    // TODO - change the buttons to be able to handel more then three languages
    constructor(props) {
        super(props);

        this.state = {
            rSelected: LanguageManager.getLang(),
            lang: 'He'
        };

        this.props.cookies.set('lang', this.state.rSelected, { path: '/' });
        this.onRadioBtnClick = this.onRadioBtnClick.bind(this);
    }

    onRadioBtnClick(rSelected) {
        this.setState({lang:rSelected})
        this.props.cookies.set('lang', rSelected, { path: '/' });
        LanguageManager.setLang(rSelected);
        this.setState({ rSelected });
        let root = document.getElementsByTagName('body')[0];
        root.setAttribute('style', 'direction:' + LanguageManager.getDirection());
    }

    render() {
        return (
            <FormGroup style={{ direction: "ltr" }} className={'text-center'}>
                <div className={'form-check-inline ' + (LM.getDirection() === "rtl" ? "form-check-inline-rtl" : "")}>
                    <CustomInput type="radio" id="radioBtnEn" name="customRadio" label={LM.getString('english')}
                        onClick={() => this.onRadioBtnClick('En')} defaultChecked={this.state.rSelected === 'En'} />
                    <CustomInput type="radio" id="radioBtnAr" name="customRadio" label={LM.getString('arabic')}
                        onClick={() => this.onRadioBtnClick('Ar')} defaultChecked={this.state.rSelected === 'Ar'} />
                    <CustomInput type="radio" id="radioBtnHe" name="customRadio" label={LM.getString('hebrew')}
                        onClick={() => this.onRadioBtnClick('He')} defaultChecked={this.state.rSelected === 'He'} />
                </div>
            </FormGroup>
        );
    }
}

