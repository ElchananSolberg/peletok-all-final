import React, { Component } from 'react';
import {
    Container, Col, Row,
    Navbar
} from 'reactstrap';
import './Login.css';
import { Switch, Route, Link } from 'react-router-dom';
import { LangButtons } from './LangButtons';
import { LoginForm } from './LoginForm';
import { FooterBar } from "../FooterBar/FooterBar";
import { SupportLayout } from "../SupportLayout/SupportLayout";
import { LanguageManager as LM } from "../LanguageManager/Language"
import { ForgotPasswordManager } from '../ForgotPasswordComponents/ForgotPasswordManager'
import { SupportButton } from "../HeaderBar/SupportButton";

let forSupport = LM.getString("supportButton");


class LoginComponent extends Component {
    render() {
        return (
            <Container color={'light'}>
                <h5 className={'border-bottom font-weight-light fs-26 pb-md-3 pt-md-4 py-2 text-center'}>{LM.getString("welcomeMessage")}</h5>
                <Row className={'pt-md-4 justify-content-around'}>
                    <Col md="5" sm="12" className={'fs-17'}>
                        {/*<LangButtons cookies={this.props.cookies}/> is saving the lang to cookies */}
                        <LangButtons cookies={this.props.cookies} />
                        {/*<LoginForm {...this.props}/> is passing all the props to the login*/}
                        <LoginForm {...this.props} />

                    </Col>
                    <Col md="6" sm="12">
                        <SupportLayout />
                    </Col>
                </Row>
            </Container>
        )
    }
}

export class Login extends Component {
    /*
     TODO - add baner for advertisement
     The Login page contains header and footer...
     Renders the LoginForm, LangButtons and SupportLayout
     */

    render() {
        return (
            <div className={'temp_background login_background'}>
                <div className={"main-header"}>

                    <Navbar color="light" light expand="md">
                        {/* TODO - get Logo src */}
                        <Container fluid style={{ width: "90%", maxWidth: '1300px' }}>
                            <Link to={'/'} className="peletokLogo">{LM.getString("peletalk")}</Link> {/* TODO - get Logo src */}
                            <div className="my-auto d-flex d-md-none" style={{ alignSelf: "end" }}>
                                <SupportButton buttonLabel={forSupport} />
                            </div>
                        </Container>
                    </Navbar>
                </div>
                <Container>
                    <div className={'fillScreen'}>
                        <div className={"main-body my-2 my-md-0 pt-md-5"}>
                            <div className={"form border-radius-4 login " + (LM.getDirection() === "rtl" ? "login-rtl" : "")}>
                                <Switch>
                                    <Route exact path={`${this.props.match.path}/`} render={(routePros) => { return <LoginComponent {...routePros} {...this.props} /> }} />
                                    <Route exact path={`${this.props.match.path}/forgotPassword/:username`} component={ForgotPasswordManager} />
                                </Switch>
                            </div>
                        </div>
                    </div>
                </Container>
                <div className={"main-footer footer-login"}>
                    <FooterBar />
                </div>
            </div>
        );
    }
}
