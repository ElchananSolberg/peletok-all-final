import React, { Component } from 'react';
import {
    Form, FormGroup, Button, Label, Input, Container,
    Row, Col, CustomInput, FormFeedback
} from 'reactstrap';
import axios from 'axios';
import { Link, Redirect } from "react-router-dom";
import { getUser, reloadData } from "../DataProvider/DataProvider"
import { LanguageManager as LM } from "../LanguageManager/Language"
import { AuthService } from '../../services/authService'
// TODO change URL and proxy
// const proxyUrl = "https://cors-anywhere.herokuapp.com/";
import { config as SERVER_BASE } from "../../utils/Config";

const serverUrl = `${SERVER_BASE.baseUrl}/auth/login`;

export class LoginForm extends Component {
    // TODO -  add clearCredentials if did not login
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            isLoggedIn: false,
            userStatus: false
        };
        this.handlePassChange = this.handlePassChange.bind(this);
        this.handleUserChange = this.handleUserChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.checkUser = this.checkUser.bind(this);
    }

    handleSubmit(evt) {
        evt.preventDefault();
        const userAndPass = {
            username: this.state.username,
            password: this.state.password,
            lang: this.props.cookies.cookies.lang
        };
        if (!this.state.username) {
            return this.setState({ usernameErrMsg: LM.getString("isRequiredValidationFunctionMsg") });
        }
        if (!this.state.password) {
            return this.setState({ passwordErrMsg: LM.getString("isRequiredValidationFunctionMsg") });
        }

        AuthService.login(userAndPass).then(res => {
            if (res === "success") {
                reloadData();
                this.setState({
                    isLoggedIn: true
                }, () => {
                    this.props.authenticate({ isLoggedIn: this.state.isLoggedIn });
                });
            }
        }).catch(err => {

            switch (err.response.data.message) {
                case 'Ip or token limit exceeded':
                    this.setState({
                        usernameErrMsg: LM.getString("errorIpTokenLimit")
                    });
                    break;
                case 'User blocked':
                    this.setState({
                        usernameErrMsg: LM.getString("errorUserBlocked")
                    });
                    break;
                case 'User frozen':
                    this.setState({
                        usernameErrMsg: LM.getString("errorUserFrozen")
                    });
                    break;
                case 'Business locked':
                    this.setState({
                        usernameErrMsg: LM.getString("errorUserBlocked")
                    });
                    break;
                case 'Business pending':
                    this.setState({
                        usernameErrMsg: LM.getString("errBusinessPending")
                    });
                    break;
                case "Incorrect credentials.":
                    this.setState({
                        usernameErrMsg: LM.getString("wrongUserNameOrPassword")
                    });
                    break;
                default:
                    this.setState({
                        usernameErrMsg: LM.getString("errorCheckInputFields")
                    })
            }
        });
    }

    handleUserChange(evt) {
        this.setState({
            username: evt.target.value,
            usernameErrMsg: "",
            usernameInvalid: false
        });
    };

    handlePassChange(evt) {
        this.setState({
            password: evt.target.value,
        });
    };

    checkUser() {
        if (this.state.username && !this.state.userStatus) {
            getUser(this.state.username).then((res) => {
                this.setState({ userStatus: res },
                    () => { this.props.history.push(`${this.props.match.url}/forgotPassword/${this.state.username}`) })

            }).catch(err => {
                this.setState({
                    usernameErrMsg: LM.getString("userNotExistErrMsg"),
                    usernameInvalid: true
                })
            })
        } else {
            this.setState({
                usernameErrMsg: LM.getString("userErrMsg"),
                usernameInvalid: true
            })
        }
    };

    render() {
        return (this.state.isLoggedIn ?
            <Redirect to={"/"} /> :
            <Form onSubmit={this.handleSubmit}>
                <FormGroup>
                    <Label className={'pt-md-3'} for='usernameId'>{LM.getString('username')}</Label>
                    <Input invalid={this.state.usernameInvalid} type="username" name="username"
                        id="usernameId" placeholder="username"
                        onChange={this.handleUserChange} />
                    <FormFeedback size='sm'>{this.state.usernameErrMsg}</FormFeedback>
                    <Label for="passwordId">{LM.getString('password')}</Label>
                    <Input type="password" name="password" id="passwordId" placeholder="********"
                        onChange={this.handlePassChange} />
                    <FormFeedback size='sm'>{this.state.passwordErrMsg}</FormFeedback>
                    <Container className={'mb-2 mb-md-4 mt-3'}>
                        <Row>
                            <Col className={"p-0 " + (LM.getDirection() === "rtl" ? "col-checkbox-rtl" : "")}>
                                <CustomInput type="checkbox" id="exampleCustomCheckbox"
                                    className={(LM.getDirection() === "rtl" ? "checkbox-rtl" : "")}
                                    label={LM.getString("rememberMe")} />
                            </Col>
                            <Button className={'p-0'} tag={Link} color="link" onClick={this.checkUser} to={`${this.props.match.url}`}>
                                {LM.getString("forgotPassword")}
                            </Button>
                        </Row>
                    </Container>
                    <Col md="5" sm="12" className='px-0'>
                        <Button block size='lg' className={'fs-17 border-radius-3 bc-blue-1 border-unset'}
                            type="submit" id="btnLogin"
                            value="Log In"
                        >{LM.getString("submit")}</Button>
                    </Col>
                </FormGroup>
            </Form>
        )
    }
}
