import React, { Component } from 'react';
import { Container, Row, Col, Button } from 'reactstrap'
import { LanguageManager as LM } from '../LanguageManager/Language';
import InputTypeSearchList from '../InputUtils/InputTypeSearchList';
import UnpaidPaymentsTable from './UnpaidPaymentsTable'
import { getSuppliers } from '../DataProvider/Functions/DataGeter'

/**
 * 
 */
export class UnpaidPayments extends Component {
    constructor(props) {
        super(props);
        this.state = {

            listSuppliersNamesAndIDs: [],
            supplierSelectListProps: {
                disabled: false,
                options: [],
                default: LM.getString("AllSuppliers"),
                title: LM.getString("supplierSelect"),
                id: "supplierSelect",
            },
            tableHeadersObj: {
                supplierName: LM.getString("supplierName"),
                orderNumber: LM.getString("orderNumber"),
                date: LM.getString("date"),
                AmountIncludingVAT: LM.getString("AmountIncludingVAT"),
                chekbox: LM.getString("MultiplePayment"),
                button: LM.getString("SinglePayment"),
            },
            tableRows: [
                {
                    supplierName: 'aaa', orderNumber: "0125", date: '00/21/87', AmountIncludingVAT: '45.2'
                },

                {
                    supplierName: 'aaa', orderNumber: "01525", date: '00/21/87', AmountIncludingVAT: '45.2'
                },

                {
                    supplierName: 'aaa', orderNumber: "045125", date: '00/21/87', AmountIncludingVAT: '45.2'
                },

                {
                    supplierName: 'aaa', orderNumber: "0845", date: '00/21/87', AmountIncludingVAT: '45.2'
                },
                {
                    supplierName: 'aaa', orderNumber: "02255", date: '00/21/87', AmountIncludingVAT: '45.2'
                },
                {
                    supplierName: 'aaa', orderNumber: "09585", date: '00/21/87', AmountIncludingVAT: '45.2'
                },
                {
                    supplierName: 'aaa', orderNumber: "09525", date: '00/21/87', AmountIncludingVAT: '45.2'
                },
                {
                    supplierName: 'aaa', orderNumber: "02425", date: '00/21/87', AmountIncludingVAT: '45.2'
                },
                {
                    supplierName: 'aaa', orderNumber: "02125", date: '00/21/87', AmountIncludingVAT: '45.2'
                },
                {
                    supplierName: 'aaa', orderNumber: "3125", date: '00/21/87', AmountIncludingVAT: '45.2'
                },
                {
                    supplierName: 'aaa', orderNumber: "4125", date: '00/21/87', AmountIncludingVAT: '45.2'
                },
                {
                    supplierName: 'aaa', orderNumber: "0165", date: '00/21/87', AmountIncludingVAT: '45.2'
                },
                {
                    supplierName: 'aaa', orderNumber: "0925", date: '00/21/87', AmountIncludingVAT: '45.2'
                },
                {
                    supplierName: 'aaa', orderNumber: "0425", date: '00/21/87', AmountIncludingVAT: '45.2'
                },
            ]


        };
    }
    componentDidMount() {
        getSuppliers().then(
            (res) => {
                var listSuppliersNames = [];
                var listSuppliersNamesAndIDs = [];
                var listSuppliers = res.prepaid;

                listSuppliers.forEach(element => {
                    listSuppliersNamesAndIDs.push({ 'name': [element.name][0], 'id': [element.id][0] })
                    listSuppliersNames.push([element.name][0])
                });

                let supplierSelectListProps = Object.assign({}, this.state.supplierSelectListProps);
                supplierSelectListProps.options = listSuppliersNames;
                this.setState({ supplierSelectListProps }, () => {
                    this.setState({ listSuppliersNamesAndIDs });

                });
            }
        ).catch(
            (err) => {
                alert(err);
            }
        )
    }

    render() {

        return (<div >
            <h3 className="firstDivString">  {LM.getString("UnpaidPayments")}</h3>

            <div className="locked" >
                <Row >
                    <Col style={{ maxWidth: '36%' }} >
                        <InputTypeSearchList {...this.state.supplierSelectListProps} ref={(componentObj) => { this.supplierSelect = componentObj }}
                            validationFunction={this.outerChooseValidationFunction} onChange={this.outerOnChange} />                        </Col>

                </Row>
                <hr />
                <Row style={{ marginTop: '30px' }}>
                    <Col >
                    <UnpaidPaymentsTable WithTheMarkAllButton={true} onChecked={this.onChecked} tableRows={this.state.tableRows} tableHeadersObj={this.state.tableHeadersObj} tableFootersObj={this.state.tableFootersObj} />
                    </Col>
                </Row>


            </div>
        </div>


        );
    }
}
