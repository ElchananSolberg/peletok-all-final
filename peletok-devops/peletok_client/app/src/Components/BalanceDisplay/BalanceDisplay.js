import React from 'react';
import {
    Container, Row, Col
} from 'reactstrap';
import './BalanceDisplay.css';
import {LanguageManager as LM} from "../LanguageManager/Language";

/**
 * brealks a number to integer part (string representation) with commas where needed
 * and a fractional part (up to 2 digit persition) also in a string representation (with the '.' in place)
 * @param {*} x a double value
 */
function numberWithCommas(x) {
    x = "" + x;
    let stringParts = x.toString().split('.');
    let integerPart = stringParts[0].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    let floatPart = (stringParts.length > 1 ? "." + stringParts[1] : ".00");
    floatPart = (floatPart + "00").slice(0, 3);
    return {
        integerPart: integerPart,
        floatPart: floatPart
    };
}

/**
 displays a single balance display box with a name and a value
 */
function BalanceBox(props) {
    let number = numberWithCommas(props.value);
    return (
        <Row className={"mx-1 my-2 balanceBoxRow"}>
            <Col
                className={"col-3 px-1 balanceBoxTitle d-flex " +
                (LM.getDirection() === "ltr" ? "leftBorderRadius-3" : "rightBorderRadius-3")}>
                <span className={"mx-auto"}>{props.message}</span>
            </Col>
            <Col className={"col-9 py-1 px-2 balanceBoxValue  " +
            (LM.getDirection() === "ltr" ? "rightBorderRadius-3" : "leftBorderRadius-3")}>
                <span className={"direction-ltr"}>
                    <span id="sum">{number.integerPart}</span>
                    <span className={"my-auto balanceBoxRemainder"}>{number.floatPart}</span>
                </span>
            </Col>
        </Row>
    )
}

/**
 * creates a full display box for the balance properties.
 * @param props data.{balance, credit, remainder} attribute holds the names and values to display
 * @returns React functional component of the entire display box
 */
export function BalanceDisplay(props) {
    // let {balance, credit, remainder} = props.data;
    return (
        <Container className="p-0 mt-auto">
            {props.values.map((value)=>
            <BalanceBox message={value.strRepr} value={value.value}/>)}
        </Container>
    );
}
