import React, { Fragment } from 'react';

/**
 * LockTableHead - component that builds and returns an array of <th> elements from the data sended by LockTableHead component  . 
 */
export default class LockTableHead extends React.Component {
    
  render() {
    var mapKey = 0;
    var thArray = [];
    var tableHeadersObj = this.props.tableHeadersObj;
    thArray = Object.keys(tableHeadersObj).map((key) => {
        
                          return <th className={'fs-12 pr-3 py-2'} key={mapKey++}>{tableHeadersObj[key]}</th>
    });
    return (
      <Fragment>
        {thArray}
      </Fragment>


    );
  }
}