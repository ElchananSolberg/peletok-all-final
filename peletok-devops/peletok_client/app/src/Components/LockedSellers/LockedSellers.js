import React, { Component } from 'react';
import { Row, Col } from 'reactstrap'
import './LockedSellers.css'
import { LanguageManager as LM } from '../LanguageManager/Language';
import { getSelersStatus } from '../DataProvider/Functions/DataGeter';
import  LockTable  from './LockTable'

/**
 * Component that manages the locking of users in the system.
 */
export class LockedSellers extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: []
        };

    }
    componentDidMount() {
        getSelersStatus().then(
            (response) => {
                this.setState({ data: response })
            }
        ).catch(
            function (error) {
                alert(error)
            }
        );
    }

    render() {

        return (<div >
            <h3 className="firstDivString">  {LM.getString("locked")}</h3>
            <div className="locked" >
                {<LockTable tableRows={this.state.data} />}
              
            </div>
        </div>


        );
    }
}
