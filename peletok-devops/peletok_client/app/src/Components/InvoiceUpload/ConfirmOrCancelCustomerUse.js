import React, { Component } from 'react';
import { LanguageManager as LM } from '../LanguageManager/Language';
import InputTypeSimpleSelect from '../InputUtils/InputTypeSimpleSelect';
import { Notifications } from '../Notifications/Notifications';
import { Loader } from '../Loader/Loader';
import { Confirm } from '../Confirm/Confirm';
import InputTypeNumber from '../InputUtils/InputTypeNumber';
import { Container, Row, Col, Button } from 'reactstrap';
import './InvoiceUpload.css';
import { getDistributors, getSellersByDistributorId, postIsSubscribe, postUnsubscribe } from '../DataProvider/DataProvider'

export class ConfirmOrCancelCustomerUse extends Component {

    constructor(props) {
        super(props);
        this.state = {
            distributors: [],
            sellers: [],
            currentSeller: null,
            isSubscribe: false,
            render: true,
        }
    }

    componentDidMount() {
        getDistributors().then((res) => {
            this.setState({
                distributors: res
            })
        }).catch((err) => {
            Notifications.show(err.response.data.error, 'danger')
        })
    }
    
    distributorsChangeHandler = (e) => {
        getSellersByDistributorId(e.value).then((res) => {
            this.setState({
                sellers: res
            })
        }).catch((err) => {
            Notifications.show(err.response.data.error, 'danger')
        })
    }

    sellersChangeHandler = (e) => {

        this.setState({
            currentSeller: e.value
        })

    }

    search = () => {
        let data = this.customerIdentityField.getValue().value ?  { business_identifier: this.customerIdentityField.getValue().value } : { id: this.state.currentSeller }
        if(!data.business_identifier || !data.id){
            Loader.show()
            postIsSubscribe(data).then((res) => {
                this.setState({
                    isSubscribe: res.isSubscribe,
                    currentSeller: res.seller_id,

                })
                if(!res.isSubscribe){
                    Notifications.show(LM.getString('subscibtionHasNotFound'))
                }
                Loader.hide()
            }).catch((err) => {
                Notifications.show(err.response.data.error, 'danger')
                Loader.hide()
            })
        }else{
            Notifications.show(LM.getString('selerhasntSelected'), 'danger')
        }
    }
    unSubscribe = () => {
        Confirm.confirm(LM.getString('doYouWantToContinue')).then(
            () => postUnsubscribe({ business_id: this.state.currentSeller }).then((res) => {
                Notifications.show(LM.getString('serviceCanceledSuccessfully'), 'success')
                this.setState({ isSubscribe: false })
                this.clearFields()
            }).catch((err) => {
                Notifications.show(err.response.data.error, 'danger')
            }))
    }

    clearFields = (withRender = true) => {
        this.setState({
            render: !withRender
        }, () => {
            this.setState({
                render: true
            })
        })
    }

    render() {

        return (
            <Container>
                <Row >
                    <Col className="invoiceUploadSubHeader">
                        {LM.getString("confirmCancelCustomerUse")}
                    </Col>
                </Row>
                {this.state.render && <Row>
                    <Col sm='4'>
                        <InputTypeSimpleSelect
                            id='ChooseAgent'
                            options={this.state.distributors.map((d) => { return { value: d.id, label: d.name } })}
                            title={LM.getString("selectDistributor")}
                            onChange={this.distributorsChangeHandler}
                        />
                    </Col>
                    <Col sm='4'>
                        <InputTypeSimpleSelect
                            id='ChooseAgent'
                            options={this.state.sellers.map((d) => { return { value: d.id, label: d.name } })}
                            title={LM.getString("selectSeller")}
                            onChange={this.sellersChangeHandler}
                        />
                    </Col>
                    <Col sm='3'>
                        <InputTypeNumber
                            id='TerminalNumber'
                            ref={(obj) => { this.customerIdentityField = obj }}
                            title={LM.getString("typeTerminalNumber")}
                        />
                    </Col>
                </Row>}
                <Row>
                    <Col sm='2'>
                        <Button
                            className='invoiceUploadButton invoiceUploadButtonSearch invoiceUploadSearchButtonMargin'
                            size='sm'
                            id="invoiceUploadSearch"
                            onClick={this.search}
                        >
                            {LM.getString("search")}
                        </Button>
                    </Col>
                
                {this.state.isSubscribe && 
                    <Col sm="3">
                        <Button
                            style={{marginRight: '10px'}}
                            className='invoiceUploadButton invoiceUploadButtonSearch'
                            id="invoiceUploadCancel"
                            onClick={this.unSubscribe}
                        >
                            {LM.getString("confirmCancelSubsciption")}
                        </Button>
                    </Col>}
                </Row>
            </Container >
        )
    }
}