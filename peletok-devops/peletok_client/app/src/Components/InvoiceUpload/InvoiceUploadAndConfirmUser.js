import React, { Component } from 'react';
import { LanguageManager as LM } from '../LanguageManager/Language';
import InputTypeFile from '../InputUtils/InputTypeFile';
import InputTypeSelect from '../InputUtils/InputTypeSelect';
import InputTypeNumber from '../InputUtils/InputTypeNumber';
import { Container, Row, Col, Button } from 'reactstrap';
import './InvoiceUpload.css';
import {InvoiceUpload} from './InvoiceUpload'
import {ConfirmOrCancelCustomerUse} from './ConfirmOrCancelCustomerUse'

export class InvoiceUploadAndConfirmUser extends Component {
  
    render() {
        let arr = ['Peletalk', 'Cellcom'];
        /** Screen Header text */
        const header = LM.getString('invoiceUpload');

        return (
            <Container className="invoiceUploadContainer">
                <InvoiceUpload/>
                <ConfirmOrCancelCustomerUse/>   
            </Container>
        );
    }
}
