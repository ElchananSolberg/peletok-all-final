import React, { Component } from 'react';
import {
    Button, Input, FormFeedback,
    Label, FormGroup, Col
} from 'reactstrap';
import { LanguageManager as LM } from "../LanguageManager/Language"


export class ForgotPasswordEnterPhone extends Component {
    render() {
        return (
            <FormGroup>
                <Label>
                    {LM.getString("enterPhoneWithThreeDigit")}: {this.props.lastDigits}*******
            </Label>
                <Label size='sm'>{LM.getString('enterPhoneNum')}</Label>
                <Input invalid={this.props.errMsg.length !== 0} size='sm' type="phone" name="phone" id="phoneNumber"
                    onChange={this.props.onChange} />
                <FormFeedback id='enterPhoneNumErrMsg' size='sm'>{this.props.errMsg}</FormFeedback>
                <Col md="5" sm="12" className='px-0 mt-2 mt-md-4'>
                    <Button block size='lg' className={'fs-17 border-radius-3 bc-blue-1 border-unset'} id="confirmPhone" onClick={this.props.onSubmit}>
                    {LM.getString("confirm")}
                </Button>
                </Col>
        </FormGroup>
        )
    }
}

