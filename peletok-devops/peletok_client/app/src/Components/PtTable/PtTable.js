import React from 'react';
import { LanguageManager as LM } from '../LanguageManager/Language';
import { Table} from 'reactstrap'
import { Scrollbars } from 'react-custom-scrollbars';


function PtTable ({children, width, height}) {       
        return (
                <div className={'table-responsive table-css ' + (LM.getDirection() === "rtl" ? "table-rtl" : "")}>
                    <Scrollbars style={{ width: width || '100%', height: height || '490px' }}>
                        <Table bordered striped size="sm" >
                            {children}
                        </Table>
                    </Scrollbars>
                </div>
        );  
}

export default PtTable;