import React from 'react';

/**
 * UnpaidTableHead - component that builds and returns an array of <th> elements from the data sended by CommissionProfileTable component  . 
 */
 function TableHead ({children}){
    return (
      <thead>
          <tr>
              {children}
          </tr>
      </thead>
    );
}

export default TableHead;

