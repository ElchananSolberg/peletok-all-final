import React from 'react';

/**
 * UnpaidTableHead - component that builds and returns an array of <th> elements from the data sended by CommissionProfileTable component  . 
 */
export default class TableHead extends React.Component {
  render() {
    return (
      <tbody>
              {this.props.children}
      </tbody>
    );
  }
}