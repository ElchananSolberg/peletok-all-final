import React from 'react';

/**
 * UnpaidTableHead - component that builds and returns an array of <th> elements from the data sended by CommissionProfileTable component  . 
 */
export default class TableHead extends React.Component {
  render() {
    var mapKey = 0;
    return (   
          <tr>
              {this.props.children}
          </tr>
    );
  }
}