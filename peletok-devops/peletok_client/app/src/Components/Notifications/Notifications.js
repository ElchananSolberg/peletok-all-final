import React from 'react';
import { Alert } from 'reactstrap';
import './Notifications.css'


/** <Notifications/> is Singleton  */
/** The component receives a message to display, and also receives the color of the message */
/** To display a message you must import the component and then use the desired location as this example :
 *               {Notifications.show("הגעת ליעד","info",5000)}
 * 
*/
export class Notifications extends React.Component {
    
    static singleton;
    static index = 0;

    static colors = [
            'primary',
            'secondary',
            'success',
            'danger',
            'warning',
            'info',
            'light',
            'dark',
        ]

    static timeOuts = []

    constructor(props) {
        super(props);

        
        /** here we make Notifications to be singleton (to have only 1 instance) */
        if (Notifications.singleton) {
            throw 'Attention, developer! You must not use Notifications as a component, you should use show()  interface.';
        }
        Notifications.singleton = this;

        this.state = {
            show: false,
            message : '',
            color:"primary",
            notifications: []
        }

    }


    static show = (massege, color, time, stillOpen = false) => {
       

        if (Notifications.index > 5) throw 'It seems that Notifications component does not exist in a render tree.';

        if (!Notifications.singleton) {
            Notifications.index++;
            setTimeout(() => {
                Notifications.show();
            }, 300)
        } else {

            color = color || Notifications.singleton.state.color 
            if(Notifications.colors.indexOf(color) < 0){
                color = "primary"
                console.error('Color must be part of reactstrp alert\'s colors')
            }
           

            let id = Math.floor(Math.random() * 1000000000);
            Notifications.singleton.setState({ notifications: [...Notifications.singleton.state.notifications, new Notif(id, massege, color || 'primary', stillOpen, time)]});
            if(!stillOpen){
                let timeOut = setTimeout(() => {
                    Notifications.singleton.filterNotif(id);
                }, time ? time :3000 );
                Notifications.timeOuts.push(timeOut);
            }

        }
        // Return treu enable to call notification in tinery condition and then call ui element in render
        return true;
    }

    filterNotif(id){
        Notifications.singleton.setState({notifications: Notifications.singleton.state.notifications.filter(n=>n.id != id)})
    }

    componentWillUnmount() {
        Notifications.timeOuts.forEach((to) => {
            clearTimeout(to);
        } );
    }


    render = () => {
        return (
            <>
                {this.state.notifications.length > 0 &&
                    <div>

                        {this.state.notifications.map((n, i) => {
                                return (
                                       !n.stillOpen ?
                                           <Alert key={i} className="myAlert" color={n.color} style={{top: (75 * (i + 1)) + 'px'}}>
                                              <h5>{n.meassege}</h5>
                                           </Alert> :
                                           <Alert key={i} className="myAlert" isOpen={n.stillOpen} toggle={()=>{this.filterNotif(n.id)}} color={n.color} style={{top: (75 * (i + 1)) + 'px'}}>
                                              <h5>{n.meassege}</h5>
                                           </Alert>
                                       )
                              } )
                        }
                    </div>
                }
            </>
        );
    }
}

class Notif{
    constructor(id, meassege, color, stillOpen = false, time){
        this.id       = id;      
        this.meassege = meassege;            
        this.color    = color;         
        this.time     = time;        
        this.stillOpen    = stillOpen;        
    }
}
