import React, {Component} from 'react';
import {Table} from 'reactstrap';
import TableRow from './tableRow.js';
import TableHead from './tableHead.js';
import {Scrollbars} from 'react-custom-scrollbars';
import {LanguageManager as LM} from "../LanguageManager/Language";
import {ExportExcel} from '../ExportExcel/ExportExcel'

export class ReportTableContent extends Component {

    render() {
        var key = 0;
        // var tableRows = this.props.tableRows;
        var trId = 'tr';
        var index = 0;
        var tableHeadersObj = this.props.tableHeadersObj;
        var trArray = this.props.tableRows.map((current) => {
            //TODO: Get cancelType number from server
            const cancelType = 2;
            return <tr
                className={(current.transaction_type === cancelType ? "reportCancelRow " : "")}
                id={trId + index} key={current.transactionId} onClick={(e)=>{
                        if(this.props.trClick){
                            this.props.trClick(e)
                        }

                        if(this.props.trClickedWithObject){
                            this.props.trClickedWithObject(e, current)
                        }
                    }
                }>
                <TableRow
                    id={trId + index++}
                    tableHeadresObj={tableHeadersObj}
                    tableRowObj={current}
                    cancelationClick={this.props.cancelationClick}
                    isCanceleType={current.transaction_type === cancelType}
                    isCanceled={current.is_canceled}
                    cancelable={current.is_cancelable}
                    printable={current.is_printable!==false} // default true
                    cancelProcessStatus={current.cancel_process_status}
                    printClick={this.props.printClick}
                    inputTypeCheckBoxOnChange={(e)=>{
                        if(this.props.inputTypeCheckBoxOnChange){
                            this.props.inputTypeCheckBoxOnChange(e)
                        }
                        if(this.props.cehckeBoxChangedWithObject){
                            this.props.cehckeBoxChangedWithObject(e, current)
                        }
                    }}
                />
            </tr>
        });

        /** TODO when we will change the component to work whith 3 languages, the text in <th> with colSpan attribute should be changed.
         * <tfoot> needs to be changed  if there will be more than one footer - this.props.tableFootersObj object will be the part of ARRAY tableFootersArray and it will be
         * needed to make MAP() on this array (same as we got trArray upper, but here will be needed to add also <th> with colSpan into each  <tr></tr>) */
        let sumObj = this.props.sumObj;
        let tableFootersObj = this.props.tableFootersObj;
        let sumProfitObj = this.props.sumProfitObj;
        let sumPaymentObj = this.props.sumPaymentObj;
        let currentBalanceObj = this.props.currentBalanceObj;
        return (
            <React.Fragment>

                <div className={'table-responsive table-css ' + (LM.getDirection() === "rtl" ? "table-rtl" : "")} align={'center'}>
                    <Scrollbars style={{width: '95%', height: 490}}>

                        {trArray.length > 0 ? <Table bordered striped size="sm">
                            <thead>
                            <tr>
                                <TableHead tableHeadersObj={tableHeadersObj}/>
                            </tr>
                            </thead>
                            <tbody>
                            {trArray}
                            </tbody>
                            <tfoot>
                            {
                                sumObj ?
                                    <tr>
                                        <TableHead tableHeadersObj={tableHeadersObj} tableDataObj={sumObj}/>
                                    </tr>
                                    : <></>
                            }
                            {
                                tableFootersObj ?
                                    <tr>
                                        <th colSpan={this.props.tableFootersColSpan}>{LM.getString("total")}</th>
                                        <TableHead tableHeadersObj={tableFootersObj}/>
                                    </tr>
                                    : <></>
                            }
                            {
                                sumProfitObj ?
                                    <tr>
                                        <TableHead tableHeadersObj={tableHeadersObj} tableDataObj={sumProfitObj}/>
                                    </tr>
                                    : <></>
                            }
                            {
                                sumPaymentObj ?
                                    <tr>
                                        <TableHead tableHeadersObj={tableHeadersObj} tableDataObj={sumPaymentObj}/>
                                    </tr>
                                    : <></>
                            }
                            {
                                currentBalanceObj ?
                                    <tr>
                                        <TableHead tableHeadersObj={tableHeadersObj} tableDataObj={currentBalanceObj}/>
                                    </tr>
                                    : <></>
                            }
                            </tfoot>
                        </Table> : <span>{LM.getString('noResultFound')}</span>}
                    </Scrollbars>
                </div>
                {this.props.useReportExport!==false&&<ExportExcel
                    data={this.props.tableRows} fileName={this.props.reportName}
                    tableHeadersObj={this.props.excelHeadersObj||tableHeadersObj}/>}
            </React.Fragment>
        );
    }

}
