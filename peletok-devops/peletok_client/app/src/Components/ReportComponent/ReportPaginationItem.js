import React, {Component} from 'react';
import {PaginationItem, PaginationLink} from 'reactstrap';

import '../InputUtils/InputUtils.css';


class ReportPaginationItem extends Component {


    render() {
        let index = this.props.index;
        let active = this.props.activeIndex === index;
        let id = this.props.id + index;
        return (
                <PaginationItem active={active}>
                    <PaginationLink onClick={this.props.onClick} index={index} id={id}>
                        {index + 1}
                    </PaginationLink>
                </PaginationItem>);
    }
}

export default ReportPaginationItem;