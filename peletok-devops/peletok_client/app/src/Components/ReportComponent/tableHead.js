import React, { Fragment } from 'react';

/**
 * TableHead - component that builds and returns an array of <th> elements from the data sended by MyTable component (he got it from ReportContainer) - this.props.tableHeadersObj . 
 */
export default class TableHead extends React.Component {
  render() {
    var mapKey = 0;
    var thArray = [];
    var tableHeadersObj = this.props.tableHeadersObj;
    var tableDataObj = this.props.tableDataObj||tableHeadersObj;
    thArray = Object.keys(tableHeadersObj).map((key) => {
      if (tableDataObj[key]===undefined || tableDataObj[key] === "NaN")return <th id={key} key={mapKey++}/>;
      return <th id={key} key={mapKey++}>{tableDataObj[key]}</th>
    });

    return (
      <Fragment>
        {thArray}
      </Fragment>


    );
  }
}