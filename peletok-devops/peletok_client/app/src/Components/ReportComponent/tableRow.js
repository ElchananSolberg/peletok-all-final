import React, {Fragment} from 'react';
import {Button} from 'reactstrap';
import {LanguageManager as LM} from "../LanguageManager/Language";
import {PrintData} from '../PrintData/PrintData'

import InputTypeCheckBox from '../InputUtils/InputTypeCheckBox';
import {postCancelTransaction} from "../DataProvider/Functions/DataPoster";
import {Confirm} from "../Confirm/Confirm";
import { Notifications } from '../Notifications/Notifications'

/**
 * TableRow - component that builds and returns an array of <td> elements from the data sended by MyTable component - this.props.tableRowObj (he got it from the array this.props.tableRows sended by ReportContainer ).
 this.props.cancelationClick and this.props.printClick are function written in reportContainer.js
 */
export default class TableRow extends React.Component {

    constructor(props) {
        super(props);
        this.state = {cancelable: this.props.cancelable, cancelProcessStatus: this.props.cancelProcessStatus};
    }

    cancelationClick = (e) => {
        e.persist();
        Confirm.confirm(LM.getString('areYouSureCancel')).then(() => {
            postCancelTransaction(this.props.tableRowObj.transactionId).then(r => {
                if (r !== "success") {
                    throw new Error(r)
                }
                Notifications.show(LM.getString("cancelRequestSucceed", 'success'));
                this.setState({cancelProcessStatus: "AWAITING"});
            }).catch(e => {
                Notifications.show(LM.getString("cancelRequestFailed"))
            })
        })

    };

    render() {
        // var tableRowObj = this.props.tableRowObj;
        var tableHeadresObj = this.props.tableHeadresObj;
        var tdArray = [];
        var mapKey = 0;
        tdArray = Object.keys(tableHeadresObj).map((key) => {
            if (key === 'forExport') {
                return <td id={key} key={mapKey++}>
                    <InputTypeCheckBox
                        checked={true}
                        id={"ForExportCheckbox_" + this.props.id.slice(2)}
                        onChange={this.props.inputTypeCheckBoxOnChange}
                    />
                </td>
            }

            /** TODO for now, we render <Button> with cancelationClick or with printClick depending of the value of tableRowObj[key]
             * but when we will change the component to work whith 3 languages, it seems to me, we will need to change the text in ifs conditions
             */
            if (this.props.tableRowObj[key] === LM.getString('mailToCancel')) { /** 'Cancellation Request' - to be changed in 3 languages mode*/
             return <td id={key} key={mapKey++}>
                    <Button
                        onClick={this.cancelationClick}
                        disabled={(!this.state.cancelable)||this.state.cancelProcessStatus === "AWAITING"}
                        color="secondary">{
                        this.props.isCanceled ? LM.getString("transactionCanceled") : (
                            this.props.cancelable ? (
                                    this.state.cancelProcessStatus === "AWAITING" ? LM.getString("requestSubmitted") : this.props.tableRowObj[key])
                                : LM.getString("isNotCancelable"))}
                    </Button>
                </td>
            } else if (this.props.tableRowObj[key] === LM.getString('print')) { /** 'Print' - to be changed in 3 languages mode */
                return <td id={key} key={mapKey++}>
                    {/*<Button onClick={this.printClick} color="secondary">{this.props.tableRowObj[key]}</Button>*/}
                    {this.props.tableRowObj[key]&&this.props.printable ?
                        <PrintData transId={this.props.tableRowObj.transactionId} titleOfButton={this.props.tableRowObj[key]}/>
                        : <Button onClick={this.printClick} disabled={true}
                                  color="secondary">{this.props.tableRowObj[key]}</Button>}
                </td>
            } else return <td id={key} key={mapKey++}>{this.props.tableRowObj[key]}</td>
        });
        return (
            <Fragment>
                {tdArray}
            </Fragment>

        );
    }
}