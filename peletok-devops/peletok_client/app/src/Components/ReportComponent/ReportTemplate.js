import React, {Component} from 'react';
import {Badge, Button, Col, Collapse, Row} from 'reactstrap';
import {LanguageManager as LM} from '../LanguageManager/Language';
import InputTypeText from '../InputUtils/InputTypeText'
import InputTypeSearchList from '../InputUtils/InputTypeSearchList'
import InputTypeNumber from '../InputUtils/InputTypeNumber'
import InputTypeRadio from '../InputUtils/InputTypeRadio'
import InputTypeDate from '../InputUtils/InputTypeDate';
import InputTypeTime from '../InputUtils/InputTypeTime'
import {getReportRes, getSuppliers, SERVER_BASE} from '../DataProvider/DataProvider'
import {ReportTableContent} from "./ReportTableContent";
import {Notifications} from '../Notifications/Notifications'
import {Loader} from "../Loader/Loader";
import {postCancelTransaction} from "../DataProvider/Functions/DataPoster";
import {Confirm} from "../Confirm/Confirm";
import {getReportLength} from "../DataProvider/Functions/DataGeter";
import ReportPagination from "./ReportPagination";
import {ExportSellerReportToExcel} from "../ExportExcel/ExportSellerReportToExcel";


export class ReportTemplate extends Component {
    /**
     *
     * @state {
     *      reportName: name of report displaed
            reportID: id of report refrenced in the URL
            reportURL: full URL with ID
            tableComponent: componet holding the report based on the ID
            loadedReport: the current report loaded by ID 
            fieldRefs: holeds all the field's
            suppliers: list of suppliers for filtering
            tableRows: To be filled by reportGenerator getting the report frome server
            tableHeadersObj: To be filled by reportGenerator getting the report frome server
            tableFootersObj: To be filled by reportGenerator getting the report frome server
     * } state
     */
    constructor(props) {
        super(props);
        this.state = {
            reportName: "",
            reportID: "",
            reportURL: "",
            reportLength: 0,
            tableComponent: null,
            // loadedReport: null,
            fieldRefs: {},
            supplier_id: '',
            city_id: '',
            excelHeadersObj: {},
            tableFootersObj: {},
            openReport: false,
            key: 0,
            reportDate: "",
            manual: false,
            virtual: false,
            bills: false,
            sumConsumerPrice: 0,
            sumPayment: 0,
            currentBalance: null,
            sumBusinessPrice: 0,
            sumProfit: 0,
            params: {}
        };
        this.fieldRowParser = this.fieldRowParser.bind(this);
        this.reportGenerator = this.reportGenerator.bind(this);
        this.fieldParser = this.fieldParser.bind(this);
        this.onChangeWrapper = this.onChangeWrapper.bind(this);
        this.suppliers = [];
        this.prepaidSuppliers = [];
        // this.citiesList = [];
    }


    fieldParser(field) {
        let fieldComponent;
        switch (field.fieldType) {
            case 'String':
                fieldComponent = (<Col md="4" sm="6">
                    <InputTypeText {...field}
                        ref={(componentObj) => { this.state.fieldRefs[field.id] = componentObj }} />
                </Col>);
                break;
            case 'Number':
                fieldComponent = (<Col md="4" sm="6">
                    <InputTypeNumber {...field}
                        ref={(componentObj) => { this.state.fieldRefs[field.id] = componentObj }} />
                </Col>);
                break;
            case 'supplierList':
                fieldComponent = (<Col md="4" sm="6">
                    <InputTypeSearchList {...field}
                                         addDefaultToOptions={true}
                                         ref={(componentObj) => {
                                             this.state.fieldRefs[field.id] = componentObj
                                         }}
                                         onChange={this.onChangeWrapper(field.id)}
                                         options={this.suppliers.map(supplier => supplier.name)}
                    />
                </Col>);
                break;
            case 'perpaidList':
                fieldComponent = (<Col md="4" sm="6">
                    <InputTypeSearchList {...field}
                                         addDefaultToOptions={true}
                                         ref={(componentObj) => {
                                             this.state.fieldRefs[field.id] = componentObj
                                         }}
                                         onChange={this.onChangeWrapper(field.id)}
                                         options={this.prepaidSuppliers.map(supplier => supplier.name)}
                    />
                </Col>);
                break;
            // case 'authoritiesList':
            //     fieldComponent = (<Col md="4" sm="6">
            //         <InputTypeSearchList {...field}
            //                                     addDefaultToOptions={true}
            //                                     ref={(componentObj) => { this.state.fieldRefs[field.id] = componentObj }}
            //             options={this.citiesList.map(city => city.name_HE)}
            //             onChange={this.onChangeWraper(field.id)} />
            //     </Col>);
            //     break;
            case 'RadioButton':
                fieldComponent = (<Col>
                    <InputTypeRadio {...field}
                        ref={(componentObj) => { this.state.fieldRefs[field.id] = componentObj }}
                        default={this.getDefault(field.id)}
                        onChange={this.onChangeWrapper(field.id)} />
                </Col>);
                break;
            case 'Time':
                fieldComponent = (<Col md="4" sm="6">
                    <InputTypeTime {...field}
                        ref={(componentObj) => { this.state.fieldRefs[field.id] = componentObj }} />
                </Col>);
                break;
            case 'Date':
                fieldComponent = (<Col md="4" sm="6">
                    <InputTypeDate {...field}
                        ref={(componentObj) => { this.state.fieldRefs[field.id] = componentObj }} />
                </Col>);
                break;
            // 
            case 'yearList':
                fieldComponent = (<Col md="4" sm="6">
                    <InputTypeSearchList {...field}
                                         addDefaultToOptions={true}
                                         ref={(componentObj) => {
                                             this.state.fieldRefs[field.id] = componentObj
                                         }}
                                         options={["2019", "2018", "2017", "2016", "2015", "2014", "2013", "2012",
                                             "2011", "2010", "2009", "2008", "2007", "2006"]}/>
                </Col>);
                break;
            default:
                fieldComponent = (<div style={{ textAlign: "start" }}>Component not found</div>)
        }
        return (
            <React.Fragment>
                {fieldComponent}
            </React.Fragment>
        )
    }
    getDefault(id) {
        if(id==="service_type"){
            return LM.getString("any")
        }
    }


    onChangeWrapper(fieldID) {

        return (e) => {
            // let validParams = ['startDate', "endDate", "phoneNumber", 'service_type', 'authorities'];
            if (this.state.fieldRefs[fieldID]) {
                switch (fieldID) {
                    case "provider": {
                        let selected = this.suppliers.find((obj) => obj.name === this.state.fieldRefs[fieldID].getValue().value);
                            this.setState({
                                supplier_id: selected?selected.id:''
                            })
                    }
                        break;
                    case "service_type":
                        this.handelServiceType();
                        break;
                    // case 'authorities':
                    //     {
                    //         let selected = this.citiesList.find((obj) => obj.name_HE === this.state.fieldRefs[fieldID].getValue().value)
                    //         this.setState({
                    //             city_id: selected.city_id
                    //         })
                    //     }
                    //     break;
                    default:
                        break;
                }
            }
        };
    }
    handelServiceType() {
        let selected = this.state.fieldRefs['service_type'].getValue().value;
        switch (selected) {
            case LM.getString("any"):
                this.setState({
                    manual: false,
                    virtual: false,
                    bills: false,
                });
                break;
            case LM.getString('manualCard'):
                this.setState({
                    manual: true,
                    virtual: false,
                    bills: false,
                })
                break;
            case LM.getString('virtualCard'):
                this.setState({
                    manual: false,
                    virtual: true,
                    bills: false,
                })
                break;
            case LM.getString('billPayment'):
                this.setState({
                    manual: false,
                    virtual: false,
                    bills: true,
                })
                break;

            default:
                break;
        }
    }

    fieldRowParser(fieldRow) {
        return (
            <Row className={'mt-2'}>
                {fieldRow.map(this.fieldParser)}
            </Row>
        )
    }

    reportTemplateConstructor(props) {
        return (
            <React.Fragment>
                {props.map(this.fieldRowParser)}
            </React.Fragment>
        );
    }


    componentDidMount() {
        if (this.props.location&&this.props.location.data) {
            this.loadOptions().then(
                (res) => {
                    const data = this.props.location.data;
                    if (data.reportName) {
                        this.setState({
                            reportName: data.reportName,
                            reportID: data.reportID,
                            reportURL: `${SERVER_BASE}/report/${data.reportID}`,
                            tableComponent: this.reportTemplateConstructor(data.fields)
                        })
                    }
                }
            ).catch(
                (err) => {
                    Notifications.show("loadSuppliersList failed: " + err, 'danger')
                }
            );
        }
// getCities().then(res=>{this.citiesList=res})
    }

    async loadOptions() {
        let suppNeddedFlag = false;
        this.props.location.data.fields.map(row=>{
            row.map(field=>{
                if(field.id==="provider"){
                    suppNeddedFlag=true;
                }
            })
        });
        if(suppNeddedFlag) {
            let res = await getSuppliers();
            this.suppliers = Object.keys(res).reduce((newArr, key) => [...newArr, ...res[key]], []);
            this.prepaidSuppliers = res['prepaid'];
        }
    }

    reportGenerator(e) {
        let requestValid = true;
        let validParams = ['startDate', "startDateTime", "endDate", "endDateTime", "phoneNumber", 'supplier_id', 'manual', 'virtual', 'bills', 'transactionId', 'transaction', 'contract', 'date', 'payment_method', 'dateTime'];
        let params = {};
        Object.keys(this.state.fieldRefs).forEach((key) => {
            let isValid = validParams.indexOf(key) != -1
            if (this.state.fieldRefs[key] !== null && isValid) {
                let result = this.state.fieldRefs[key].getValue();
                if (!result.valid) {
                    requestValid = false;
                }
                params[key] = result.value;
            }
        })
        console.log(params)

        if (this.state.supplier_id && this.state.reportID == 0 | 1 | 3 | 4 | 5 | 6 | 8 | 9 | 10) { params['supplier_id'] = this.state.supplier_id }
        if (this.state.manual) { params['manual'] = this.state.manual }
        if (this.state.virtual) { params['virtual'] = this.state.virtual }
        if (this.state.bills) { params['bills'] = this.state.bills }
        if (requestValid) {
            // alert(JSON.stringify(params));
            Loader.show();
            getReportLength(this.state.reportID, {params: params}).then(
                (res) => {
                    Loader.hide();
                    // noinspection EqualityComparisonWithCoercionJS
                    if (res.length == '0') {
                        this.noData();
                    }
                    else {
                        this.setState({
                            reportLength: res.length,
                            sumBusinessPrice: res.business_price || 0,
                            sumConsumerPrice: res.consumer_price || 0,
                            sumPayment: res.sum_payment || 0,
                            currentBalance: res.currentBalance,
                            sumProfit: res.sum_profit,
                            openReport: true,
                            params: params,
                            key: this.state.key+1
                        });
                    }
                }
            ).catch(
                (err) => {
                    Loader.hide();
                    Notifications.show(err, 'danger')
                })
        }
        else {
            Notifications.show(LM.getString("wrongInputErrMsg"), 'danger')
        }


    };

    noData() {
        Notifications.show(LM.getString("noReportErrMsg"), 'danger');
        this.setState({
            openReport: false,
            key: this.state.key+1
        });
    }

    /***
     * Loads all rows from server (for excel)
     * @returns {Promise<{data: *, columns: {}}>}
     */
    loadData = async () => {
        let res = await getReportRes(this.state.reportID, {params: this.state.params});
        let columnName = {};
        Object.values(res['headers']).map((value) => {
            return columnName[value] = LM.getStringWithoutColon(value)
        });
        return {columns: columnName, data: res["data"]}
    };
    
    render() {
        let ReportTemplateComponent = <Badge color="secondary">Loading...</Badge>; // TODO replace this with a real loading icon
        if (this.state.tableComponent) {
            ReportTemplateComponent = this.state.tableComponent;
        }
        return (
            <div>
                <h3 className={'color-page-header fs-26 font-weight-light'}>{this.state.reportName}</h3>
                <div className="main_border_padding" >
                    {ReportTemplateComponent}
                    <Button id='showReport' className={'border-radius-3 btn btn-lg btn-md-block btn-secondary font-weight-bolder' +
                        ' fs-17 px-4 regularButton'} onClick={this.reportGenerator}>
                        {LM.getString("showReport")}
                    </Button>
                </div>
                    <Collapse isOpen={this.state.openReport}>
                        <React.Fragment>
                            <ReportPagination reportLength={this.state.reportLength}
                                              sumProfit={this.state.sumProfit}
                                              sumBusinessPrice={this.state.sumBusinessPrice}
                                              sumConsumerPrice={this.state.sumConsumerPrice}
                                              sumPayment={this.state.sumPayment}
                                              currentBalance={this.state.currentBalance}
                                              id={this.state.reportName}
                                              params={this.state.params} reportID={this.state.reportID}
                                              reportData={this.props.location.data} key={this.state.key}/>

                            <ExportSellerReportToExcel
                                loadData={this.loadData} fileName={this.state.reportName}
                            />
                        </React.Fragment>
            </Collapse>
            </div>
        )
    }
}