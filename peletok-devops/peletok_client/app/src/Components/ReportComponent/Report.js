import React, {Component} from 'react';
// import './Report.css'
import {Switch, Route, Redirect} from 'react-router-dom';
import {ReportTemplate} from './ReportTemplate';

export class Report extends Component {

    /**
     * constructs the Charge page component
     * TODO - change data mockup to real server data
     * @param props
     */
    constructor(props) {
        super(props);
    }


    /**
     * renders the correct page according to the route specified
     */
    render() {
        const pathname = this.props.location.pathname;
        const reportId = this.props.location.data ? this.props.location.data.reportID : pathname.substring(pathname.lastIndexOf("/") + 1);
        return (
            <Switch>
                <Route path={`${this.props.match.path}/:reportId`}
                       render={props => <ReportTemplate
                           key={reportId} {...props}
                       />}
                />
                <Redirect to={'/'}/>
            </Switch>
        );
    }

}

