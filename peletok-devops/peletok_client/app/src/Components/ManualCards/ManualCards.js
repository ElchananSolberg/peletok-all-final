import React from 'react';
import { Button, Container, Row, Col } from 'reactstrap';
import './ManualCards.css';
import { LanguageManager as LM } from '../LanguageManager/Language';
import { Notifications } from '../Notifications/Notifications';
import { Loader } from '../Loader/Loader';
import InputTypeCheckBox from '../InputUtils/InputTypeCheckBox';
import InputTypeSearchList from '../InputUtils/InputTypeSearchList';
import InputTypeDate from '../InputUtils/InputTypeDate'
import { getSuppliers, getProducts, getManualCards } from '../DataProvider/Functions/DataGeter';
import { Rows } from './Rows';
import ManualCardsForm from './ManualCardsForm';


export class ManualCards extends React.Component {
    constructor(props) {
        super(props);


        this.state = {
            choice: "",
            new: false,
            myCards: [],
            productlist: [],
            listSuppliersNamesAndIDs: [],
            SupplierID: null,
            disabled: true,
            chosenProduct: '',
            ManualCardsSupplierList: {
                options: [],
                title: LM.getString("supplierSelect"),
                id: "ProviderListId",
                required: true,
            },
            ManualCardsListOfProducts: {
                options: [],
                title: LM.getString("ListOfProducts"),
                id: "ListOfProductsId",
                required: true,
            },
            ActiveCardsCheckBoxProps: {
                lableText: LM.getString("ActiveCards"),
                id: "ActiveCardsCheckBoxId",
            },
            TicketsUsedCheckBoxProps: {
                lableText: LM.getString("TicketsUsed"),
                id: "TicketsUsedCheckBoxId",
            },
            cardsThatAreGoingToExpireCheckBoxProps: {
                lableText: LM.getString("cardsThatAreGoingToExpire"),
                id: "cardsThatAreGoingToExpireCheckBoxId",
            },
            ExpiredAndUnusedCardsCheckBoxProps: {
                lableText: LM.getString("ExpiredAndUnusedCards"),
                id: "ExpiredAndUnusedCardsCheckBoxId",
            },
            inputTypeStartDate: {
                title: LM.getString("insertDate"),
                id: "StartDateInputTextId",
                required: true,
            },
            inputTypeEndDate: {
                title: LM.getString("endDate"),
                id: "EndDateInputTextId",
                required: true,
            },
            isProductFound: false,
            manualCardsList: []
        };
    }
    componentDidMount() {
        getSuppliers().then(
            (res) => {
                var listSuppliersNames = [];
                var listSuppliersNamesAndIDs = [];
                var listSuppliers = res.prepaid;

                listSuppliers.forEach(element => {
                    listSuppliersNamesAndIDs.push({ 'name': [element.name][0], 'id': [element.id][0] })
                    listSuppliersNames.push([element.name][0])
                });

                let ManualCardsSupplierList = { ...this.state.ManualCardsSupplierList };
                ManualCardsSupplierList.options = listSuppliersNames;
                this.setState({ ManualCardsSupplierList })
                this.setState({ listSuppliersNamesAndIDs })

                // Updating enddate
                let dateNow = new Date();
                let yearNow = dateNow.getFullYear();
                let monthNow = dateNow.getMonth() + 1;
                let dayNow = dateNow.getDate() + 1;
                let stringDay;
                if (dayNow.toString().length === 1) {
                    stringDay = "0" + dayNow
                }
                if (dayNow.toString().length === 2) {
                    stringDay = dayNow
                }
                let stringMonth;
                if (monthNow.toString().length === 1) {
                    stringMonth = "0" + monthNow
                }
                if (monthNow.toString().length === 2) {
                    stringMonth = monthNow
                }
                let relevantDate = yearNow + '-' + stringMonth + '-' + stringDay;

                this.EndDate.setValue(relevantDate)
            }

        ).catch(
            (err) => {
                Notifications.show(err, 'danger');
            }
        )
    }

    getProduct = () => {

        this.ListOfProducts.setValue("")
        var list = [];
        var productlist = [];
        var selected = this.chooseSupplier.getValue().value;
        this.state.listSuppliersNamesAndIDs.forEach(element => {
            if (element['name'] == selected) {
                this.setState({ SupplierID: element['id'] }, () => {
                    getProducts(this.state.SupplierID).then(
                        (res) => {
                            for (let i in res.manual) {
                                productlist.push({ "name": res.manual[i].name, "id": res.manual[i].id })
                                list.push(res.manual[i].name)
                                this.setState({ productlist });
                            };
                            let ManualCardsListOfProducts = { ...this.state.ManualCardsListOfProducts }
                            ManualCardsListOfProducts.options = list;
                            this.setState({ ManualCardsListOfProducts }, () => {

                            });
                        }).catch(
                            (err) => {
                                Notifications.show(err, 'danger');
                            }
                        )
                }

                )
            }
        });
    }
    getManunualCardsById = () => {

        this.setState({ myCards: [] }, () => {
            var productId = this.getProductId();
            var params = {
                startDate: this.StartDate.getValue().value,
                endDate: this.EndDate.getValue().value,
                isUsed: this.TicketsUsed.getValue().value,
                isActive: this.checkBoxActiveCard.getValue().value,
                aboutToExpire: this.GoingToExpire.getValue().value,
                expiredAndUnused: this.ExpiredAndUnused.getValue().value,
            }
            Loader.show();
            getManualCards(productId, params).then(
                (res) => {
                    setTimeout(() => {
                        Loader.hide();
                    }, 1000)
                    if (res.length === 0) {
                        Loader.hide();
                        this.setState({ manualCardsList: <Col>{LM.getString("noCards")}</Col> })
                    }
                    else {
                        this.setState({ myCards: [] }, () => {
                            var myCards = [];
                            for (let i in res) {
                                var myCard = {
                                    card_number: res[i].card_number,
                                    status: res[i].status,
                                    expiry_date: res[i].expiry_date,
                                    insert_date: res[i].insert_date ? res[i].insert_date.split('T')[0] : '',
                                    code: res[i].code,
                                    Time_of_income: res[i].insert_date ? res[i].insert_date.split('T')[1].split('Z')[0].split('.')[0] : '',
                                    product_id: res[i].product_id,
                                    author: res[i].author,
                                    id: res[i].id
                                }
                                if (myCard.card_number != undefined && myCard.card_number != null) {
                                    myCards.push(myCard)
                                }
                            }
                            this.setState({ myCards }, () => {
                                if (this.state.myCards.length > 0) {
                                    let manualCards = this.state.myCards.map((card, index) =>
                                        <Rows id={index} getCards={this.getManunualCardsById} cardID={card.id} {...card} ProductId={this.getProductId()} />)
                                    this.setState({ manualCardsList: manualCards })
                                }
                            })
                        });
                    }
                }).catch(
                    (err) => {
                        setTimeout(() => {
                            Loader.hide();
                        }, 1000)
                        Notifications.show(err, 'danger');
                    }
                )
        })
    }

    supliersOnSelect = (e) => {

        this.setState({ myCards: [] }, () => {
            this.getProduct();
            this.setState({ new: false }, () => {
                if (!this.ListOfProducts.getValue().valid || this.state.chosenProduct == this.ListOfProducts.getValue().value) {
                    this.setState({ disabled: true })
                }
            }
            )
        })
    }

    productOnSelect = (e) => {
        this.setState({ myCards: [], isProductFound: true, chosenProduct: this.ListOfProducts.getValue().value }, () => this.setState({ disabled: false }))
    }

    getProductId = () => {
        var id
        for (let i in this.state.productlist) {
            if (this.ListOfProducts && this.state.productlist[i].name === this.ListOfProducts.getValue().value) {
                id = this.state.productlist[i].id;
                return id
            }
        }
    }
    onChange = (e) => {

        var count = 0;

        if (this.checkBoxActiveCard.getValue().value) {
            this.ExpiredAndUnused.setValue(false)

            count++;
        };
        if (this.TicketsUsed.getValue().value) {
            this.ExpiredAndUnused.setValue(false)
            count++;
        };
        if (this.GoingToExpire.getValue().value) {
            this.ExpiredAndUnused.setValue(false)
            count++;
        };
        if (count > 1) {
            this.setState({ choice: !this.state.choice })
        }
        else if (count == 1) {
            this.setState({ choice: !this.state.choice })
        }
        else if (count == 0) {
            this.setState({ choice: !this.state.choice })
        }

    }

    expiredAndUnusedCardsCheckBoxIdOnChange = () => {
        this.checkBoxActiveCard.setValue(false)
        this.TicketsUsed.setValue(false)
        this.GoingToExpire.setValue(false)
    }

    render() {

        var listOfCards;
        listOfCards = this.state.myCards;

        return (
            <Container className="manualCardsContainer">
                <Row>
                    <Col className="suppliersHeader">
                        {LM.getString("manualCards")}
                    </Col>
                </Row>
                <Container className="manualCardsSettings" >
                    <Row >
                        <Col sm="4">
                            <InputTypeSearchList onChange={this.supliersOnSelect} {...this.state.ManualCardsSupplierList} ref={(componentObj) => { this.chooseSupplier = componentObj }} />
                        </Col>
                        <Col sm="4">
                            <InputTypeSearchList onChange={this.productOnSelect}{...this.state.ManualCardsListOfProducts} ref={(componentObj) => { this.ListOfProducts = componentObj }} />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm='auto'>
                            <InputTypeCheckBox
                                {...this.state.ActiveCardsCheckBoxProps}
                                onChange={this.onChange}
                                ref={(componentObj) => { this.checkBoxActiveCard = componentObj }} />
                        </Col>
                        <Col sm='auto'>
                            <InputTypeCheckBox
                                {...this.state.TicketsUsedCheckBoxProps}
                                onChange={this.onChange}
                                ref={(componentObj) => { this.TicketsUsed = componentObj }} />

                        </Col>
                        <Col sm='auto'>
                            <InputTypeCheckBox
                                {...this.state.cardsThatAreGoingToExpireCheckBoxProps}
                                onChange={this.onChange}
                                ref={(componentObj) => { this.GoingToExpire = componentObj }} />
                        </Col>
                    </Row>
                    <Row>
                        <Col className='mt-4'>
                            <InputTypeCheckBox
                                {...this.state.ExpiredAndUnusedCardsCheckBoxProps}
                                onChange={this.expiredAndUnusedCardsCheckBoxIdOnChange}
                                ref={(componentObj) => { this.ExpiredAndUnused = componentObj }} />
                        </Col>
                    </Row>
                    <Row className='mt-4'>
                        <Col sm="4" >
                            <InputTypeDate
                                {...this.state.inputTypeStartDate}
                                ref={(componentObj) => { this.StartDate = componentObj }}
                            />
                        </Col>
                        <Col sm="4">
                            <InputTypeDate
                                {...this.state.inputTypeEndDate}
                                ref={(componentObj) => { this.EndDate = componentObj }}
                            />
                        </Col>
                        <Col sm="auto">
                            <Button
                                className="manualCardsButton manualCardsButtonSearch"
                                onClick={this.getManunualCardsById}
                                id='supplierSearch'
                                disabled={this.state.disabled}
                            >
                                {LM.getString("search")}
                            </Button>
                        </Col>
                    </Row>
                    <Row >
                        <Col >
                            <hr className="manualCardsLine" />
                        </Col>
                    </Row>

                    {listOfCards.length > 0 ?
                        <Row className="mainRow" >
                            <Col > {LM.getString("cardNumber")}</Col>
                            <Col > {LM.getString("used")}</Col>
                            <Col > {LM.getString("DateAndTimeOfIncome")}</Col>
                            <Col > {LM.getString("ExpiryDate")}</Col>
                            <Col > {LM.getString("cardCode")}</Col>
                            <Col></Col>
                        </Row>
                        : <React.Fragment />}

                    {this.state.manualCardsList}

                    <ManualCardsForm
                        selectedProductId={this.getProductId()}
                        isProductFound={this.state.isProductFound}
                        getCards={this.getManunualCardsById}
                    />
                </Container>
            </Container>
        );
    }
}