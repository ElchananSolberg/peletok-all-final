import axios from 'axios';

import { DEFAULT_REFRESH_TIME, fixBusinessId, loadedData, SERVER_BASE, shouldRefetch } from './../DataProvider'
import { Notifications } from '../../Notifications/Notifications';
import { LanguageManager as LM } from '../../LanguageManager/Language';
import { AuthService } from '../../../services/authService';

/**
 * general get function that stores its data and returns it if up to date
 * @param {*} url the url to get
 * @param {*} reqData 
 * @param {*} reqOptions 
 * @param {*} forceReload should a request be forced
 */
function dataGetter(url, config = {}, forceReload = false, reloadTime = DEFAULT_REFRESH_TIME) {
    fixBusinessId(config);
    if (config instanceof Object) {
        fixBusinessId(config.params)
        config["withCredentials"] = true;
    }
    
    if (forceReload || (typeof loadedData[url] === "undefined") || shouldRefetch(loadedData[url].refreshTime)) {
        return new Promise(function (resolve, reject) {
            axios.get(SERVER_BASE + AuthService.addQueryParams(url), Object.assign({ withCredentials: true }, config)).then(
                function (response) {
                    loadedData[url] = {
                        data: response.data,
                        refreshTime: new Date((new Date()).getTime() + reloadTime * 60000)
                    }
                    resolve(loadedData[url].data);
                }
            ).catch(
                function (error) {
                    // TODO - add real error handling
                    reject(error);
                }
            );
        });
    } else {
        // if no request is needed and data exists - resolves it
        return new Promise(function (resolve, reject) {
            resolve(loadedData[url].data);
        });
    }
}

/*
 * ########################################
 *       ONLY WRITE BELLOW THIS LINE       
 * ########################################
 */
function appendParams(params){
    return Object.keys(params).reduce((n, k)=>`${n}${n=='?' ? '' : '&'}${k + '=' + params[k]}`,'?')
}

export function getTansaction(supplierId, config = {}, forceReload = true) {
    return dataGetter(`/transaction/cancelation_request/${supplierId}`, config, forceReload, 0);
}

export function getProductsSupplierMax(productId, config = {}, forceReload = true) {
    return dataGetter(`/product/supplier_details/${productId}`, config, forceReload, 0);
}

export function getHashavshevetOrders(params, config = {}, forceReload = true) {
    return dataGetter(`/hashavshevet/order_view/${appendParams(params)}`, config, forceReload, 0);
}

export function getHashavshevetByDate(params, config = {}, forceReload = true) {
    return dataGetter(`/hashavshevet/get_hashavshevet_by_date/${appendParams(params)}`, config, forceReload, 0);
}

export function getHashavshevetOrdersByID(businessId, params, config = {}, forceReload = true) {
    return dataGetter(`/hashavshevet/order_view/${businessId}${appendParams(params)}`, config, forceReload, 0);
}

export function getVat( config = {}, forceReload = true) {
    return dataGetter(`/config/vat/`, config, forceReload, 0);
}

export function getLockedBusiness( config = {}, forceReload = true) {
    return dataGetter(`/business/get_locked`, config, forceReload, 0);
}

export function getBusinessProfiles(config = {}, forceReload = true) {
    return dataGetter(`/business/profiles`, config, forceReload, 0);
}

export function getProductDetails(productId, config = {}, forceReload = true) {
    return dataGetter(`/product/${productId}`, config, forceReload, 0);
}

export function getMoreInfo(productId, config = {}, forceReload = true) {
    return dataGetter(`/product/more_info/${productId}`, config, forceReload, 0);
}

export function getProductTags(config = {}, forceReload = true) {
    return dataGetter(`/product/tags`, config, forceReload, 0);
}
export function getBusinessTags(config = {}, forceReload = true) {
    return dataGetter(`/business/tags`, config, forceReload, 0);
}

export function getPermissions(config = {}, forceReload = true) {
    return dataGetter('/permissions', config, forceReload, 0);
}

export function getRole(config = {}, forceReload = true) {
    return dataGetter('/roles', config, forceReload, 0);
}

export function getPermissionRoles(productId, config = {}, forceReload = true) {
    return dataGetter('/permission_roles', config, forceReload, 0);
}

export function getSuppliers(config = {}, forceReload = true, filterNoneActive = false) {
    return dataGetter(`/supplier${filterNoneActive ? '?filter=true' : ''}`, config, forceReload, 0);
}

export function getPrepaidSupplier(supplierId, config = {}, forceReload = true) {
    return dataGetter(`/supplier/prepaid/${supplierId}`, config, forceReload, 0);
}

export function getSupplier(supplierId, config = {}, forceReload = true) {
    return dataGetter(`/supplier/${supplierId}`, config, forceReload, 0);
}

export function getUsersDocuments(userId, config = {}, forceReload = true) {
    return dataGetter(`/user_doc/${userId}`, config, forceReload, 0);
}

export function getSuppliersList(config = {}, forceReload = true) {
    return dataGetter("/supplier/supplier_list", config, forceReload, 0);
}
export function getProducts(supplierId, config = {}, forceReload = true) {
    return dataGetter(`/product/products/${supplierId}`, config, forceReload);
}
export function getChangePassHistory(config = {}, forceReload = true) {
    return dataGetter(`/user/password_change_history`, config, forceReload);
}
export function getManualCards(productId, params, config = {}, forceReload = true) {
    return dataGetter(`/product/manual_card/${productId}/?startDate=${params.startDate}&endDate=${params.endDate}&isUsed=${params.isUsed}&isActive=${params.isActive}&aboutToExpire=${params.aboutToExpire}&expiredAndUnused=${params.expiredAndUnused}`, config, forceReload, 0);
}
export function getUserMessages(config = {}, forceReload = true) {
    return dataGetter(`/message/`, config, forceReload);
}
export function getOrderedGifts(forceReload = true) {
    return dataGetter("/gift/ordered", {}, forceReload, 1);
}

export function getReportList(config = {}, forceReload = true) {
    return dataGetter("/report/", config, forceReload);
}

export function getReportLength(reportId, config = {}, forceReload = true) {
    return dataGetter(`/report/report_length/${reportId}`, config, forceReload);
}

export function getManagerReportLength(reportId, config = {}, forceReload = true) {
    return dataGetter(`/admin/report/report_length/${reportId}`, config, forceReload);
}

export function getReportRes(reportId, params, config = {}, forceReload = true) {
    return dataGetter(`/report/report_data/${reportId}${appendParams(params)}`, config, forceReload);
}
export function getManagerReportRes(reportId, config = {}, forceReload = true) {
    return dataGetter(`/admin/report/report_data/${reportId}`, config, forceReload);
}
export function getAllGifts(forceReload = true) {
    return dataGetter("/gift/gift", {}, forceReload, 0);
}
export function getAllUnpaid(forceReload = true) {
    return dataGetter("/transaction/Unpaid", {}, forceReload, 1);
}
export function getAllBanners(forceReload = false) {
    return dataGetter("/banner", {}, forceReload);
}
export function getCurrentUserData(forceReload = true) {
    return dataGetter("/user/user_details", "currentUserData", {}, forceReload, 1);
}
export function getMyDetails(forceReload = true) {
    return dataGetter("/user/generalInformation", {}, forceReload, 1);
}
export function getSellerUsersData(forceReload = true) {
    return dataGetter("/user", {}, forceReload, 1);
}
export function getCreditCardOptions(forceReload = true) {
    // TODO - change this to get real data from server when implemented
    return dataGetter('/credit_card/', {}, forceReload)
}

export function getCreditCards(forceReload = true) {
    // TODO - change this to get real data from server when implemented
    return dataGetter('/credit_card/', {}, forceReload)
}

export function getSupplierTypes(forceReload = true) {
    // TODO - change this to get real data from server when implemented
    return new Promise((res, rej) => {
        res([
            {
                name: "prepaid",
                id: 1
            },
            {
                name: "bills",
                id: 2
            }
        ])
    })
}

export function getReceiptDetails(transaction_id, forceReload = true) {
    if (transaction_id == undefined) {
        Notifications.show(LM.getString("transactionIdErr"), "danger");
        return new Promise(function (resolve, reject) {
            resolve(null);
        })
    }
    return dataGetter(`/transaction/receipt/${transaction_id}`, {}, forceReload, 1);
    // TODO - change this to get real data from server when implemented
    // return new Promise((res, rej) => {
    //     res({
    //         "transId": "23456678",
    //         "transDateTime": "05:21:44 08/10/2018",
    //         "operationPerformer": "יוסי נייד מוכרן 3478",
    //         "targetPhone": "052-2222222",
    //         "provider": "סלקום",
    //         "product": "טוקמן בלה בלה בלה בלה בלה בלה בלה בלה בלה בלה בלה בלה בלה בלה בלה בלה בלה בלה בלה בלה בלה בלה",
    //         "price": "55 ש\"ח"
    //     })
    // })
}



export function getSelersStatus(forceReload = true) {
    return dataGetter("/business/frozen_business");
}
export function getCities(config = {}, forceReload = true) {
    return dataGetter('/city_pay', config, forceReload, 0)
}
export function getDistributors(config = {}, forceReload = true) {
    return dataGetter("/distributor/distributor", config, forceReload, 0);
}
export function getSellers(config = {}, forceReload = true) {
    return dataGetter("/business/business", config, forceReload);
}
export function getSellersByDistributorId(distributorId, config = {}, forceReload = true) {
    return dataGetter(`/business/business_of_distributor/${distributorId}`, config, forceReload);
}
export function getTags(config = {}, forceReload = true) {
    return dataGetter("/tag/tag", config, forceReload, 0);
}
export function getBarcodeTypes(config = {}, forceReload = true) {
    return dataGetter("/barcode_type", config, forceReload, 0)
}
export function getUsersForManager(filter = {}, config = {}, forceReload = true) {
    let url = "/admin/user";
    let filterStr = "";
    for (const k of Object.keys(filter)) {
        if (filter[k] && filter[k] !== "") filterStr += `&${k}=${filter[k]}`
    }
    if (filterStr !== "") url += "?" + filterStr.slice(1); // slice(1) for remove first '&'
    return dataGetter(url, config, forceReload, 0)
}

export function getDoseModeFieldValueExsist(model, field, value,  forceReload = true) {
    return dataGetter(`/user/valid_fields/?model=${model}&field=${field}&value=${value}`, {}, forceReload, 0)
}

export function getSuffixPhoneNumber(username, forceReload = true) {
    return dataGetter(`/reset/suffix_phone/?username=${username}`, forceReload);
}
export function getUser(username, forceReload = true) {
    return dataGetter(`/reset/check_user/?username=${username}`, forceReload);
}
export function getPhoneNumberForUser(username, phoneNumber, forceReload = true) {
    return dataGetter(`/reset/check_phone/?username=${username}&phone_number=${phoneNumber}`, forceReload);
}
export function getBusinessSupplierAuthorizationDueToUser(config = {}, forceReload = true) {
    return dataGetter(`/admin/business_supplier/authorization`, config, forceReload, 0);
}
export function getBusinessSupplierAuthorizationDueToBusiness(businessId, config = {}, forceReload = true) {
    return dataGetter(`/admin/business_supplier/business_authorization/${businessId}`, config, forceReload, 0);
}
export function getBusinessProductDueToUser(config = {}, forceReload = true) {
    return dataGetter(`/admin/business_product/authorization`, config, forceReload, 0);
}
export function getBusinessProductAuthorizationDueToBusiness(businessId, config = {}, forceReload = true) {
    return dataGetter(`/admin/business_product/business_authorization/${businessId}`, config, forceReload, 0);
}
export function getBusinessProductCommissionDueToUser(config = {}, forceReload = true) {
    return dataGetter(`/admin/business_product/commission_authorization`, config, forceReload, 0);
}
export function getBusinessProductCommissionAuthorizationDueToBusiness(businessId, config = {}, forceReload = true) {
    return dataGetter(`/admin/business_product/commission_business_authorization/${businessId}`, config, forceReload, 0);
}
export function getSuppliersProfiles(config = {}, forceReload = true) {
    return dataGetter(`/business/profile/`, config, forceReload, 0);
}
export function getSuppliersProfile(profileId, config = {}, forceReload = true) {
    return dataGetter(`/business/profile/${profileId}`, config, forceReload, 0);
}
export function getPrepaidCancelDetails(supplierID, contractNumber, itemId, businessId, config = {}, forceReload = true) {
    return dataGetter(`/payment/cancelDetails/?supplierID=${supplierID}&contractNumber=${contractNumber}&itemId=${itemId}&businessId=${businessId}`, config, forceReload, 0)
}
export function getProfitPercentageProfiles(profitModel, config = {}, forceReload = true) {
    return dataGetter(`/profit/profile/${profitModel}`, config, forceReload, 0);
}
export function getUserForPersonalArea(config = {}, forceReload = false) {
    return dataGetter(`/user/details/`, config, forceReload, 0);
}
export function getBalanceApprovals(config = {}, forceReload = false) {
    return dataGetter(`/user/balance_approval`, config, forceReload, 0)
}
export function getAgentsResponsible(config = {}, forceReload = true) {
    return dataGetter("/business/agent/", config, forceReload, 0);
}
export function getActivitySession(businessId, config = {}, forceReload = true) {
    return dataGetter(`/activity_session/${businessId}`, config, forceReload, 0);
}
export function getInvoice(params={} ,config = {}, forceReload = true) {
    return dataGetter(`/invoice?${Object.keys(params).map(k=>`${k}=${params[k]}`).join('&')}`, config, forceReload, 0);
}