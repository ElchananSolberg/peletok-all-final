import axios from 'axios';
import { AuthService } from '../../../services/authService';
import {
    loadedData,
    DEFAULT_REFRESH_TIME,
    SERVER_BASE,
    shouldRefetch, fixBusinessId
} from './../DataProvider'

/**
 * general delete function that stores its data and returns it if up to date
 * @param {*} url the url to the data to delete
 * @param {*} reqData 
 * @param {*} reqOptions 
 * @param {*} forceReload should a request be forced
 */
function dataDeleter(url, config = {}) {
    return new Promise(function (resolve, reject) {
        // check axios.delete API
        // TODO check if we need to add withCredentials: true
        fixBusinessId(config);
        if(config instanceof Object)fixBusinessId(config.params);
        config.withCredentials = true
        axios.delete(SERVER_BASE + AuthService.addQueryParams(url), config).then(
            function (response) {
                
                resolve(response.data);
            }
        ).catch(
            function (error) {
                // TODO - add real error handling
                reject(error);
            }
        );
    });
}

/*
 * ########################################
 *       ONLY WRITE BELLOW THIS LINE
 * ########################################
 */

export function deleteCreditCards(id) {
    return dataDeleter(`/credit_card/${id}`);
}
export function deleteSupplier(supplierId, config = {}) {
    return dataDeleter(`/supplier/supplier/${supplierId}`, config);
}
export function deleteManualCard(cardID, config = {}) {
    return dataDeleter(`/product/manual_card/${cardID}`, config);
}
export function deleteProduct(productId, config = {}) {
    return dataDeleter(`/product/product/${productId}`, config);
}
export function deleteGift(giftId, config = {}) {
    return dataDeleter(`/gift/gift/${giftId}`, config);
}
export function deleteDoc(docID, config = {}) {
    return dataDeleter(`/user_doc/${docID}`, config);
}
export function deleteMessage(messageId, config = {}) {
    return dataDeleter(`/message/${messageId}`, config);
}
export function deleteSeller(sellerId, config = {}) {
    return dataDeleter(`/business/business/${sellerId}`, config);
}
export function deleteTag(tagId, config = {}) {
    return dataDeleter(`/tag/tag/${tagId}`, config);
}
export function deleteDistributor(distributorId, config = {}) {
    return dataDeleter(`/distributor/distributor/${distributorId}`, config);
}
export function deleteCatalogNumber(catalogNumberId, config = {}) {
    return dataDeleter(`/barcode_type/${catalogNumberId}`, config)
}

export function deleteIPAddress(ipAddressId, config = {}) {
    return dataDeleter(`/activity_session/ip/${ipAddressId}`, config);
}
export function deleteToken(tokenId, config = {}) {
    return dataDeleter(`/activity_session/token/${tokenId}`, config);
}