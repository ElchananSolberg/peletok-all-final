import axios from 'axios';
import { Notifications } from '../Notifications/Notifications'
import {config} from "../../utils/Config";
const SERVER_BASE = config.baseUrl;

export {SERVER_BASE};

export const DEFAULT_REFRESH_TIME = 5; // in minutes

export var loadedData = {};

export function reloadData() {
    getBusinessInfo().then(r=> {
        mapById = r.mapById;
        mapByIdentifier = r.mapByIdentifier;
    });
    getTagsOfMessage().then(r => {
        tagsOfMessageByName = r;
    });
    getBusinessTagsOfMessage().then(r => {
        businessTagOfMessageByName = r;
    });
    loadedData = {}
}

var eventLiseners = {
    BALANCE:[],
    "CURRENT_USER":[],
    "":[],
}

export function shouldRefetch(refreshTime) {
    return (new Date()) > refreshTime;
}

//TODO: this method should be called only when the site loads and when a business changed
/** Returns Object for mapping businesses
 * @returns {{mapByIdentifier: {Object}, mapById: {Object}}}
 */
async function getBusinessInfo() {
    let mapById = {};
    let mapByIdentifier = {};
    await axios.get(SERVER_BASE + "/business/id", { withCredentials: true }).then(result=>{
        if(result.status!==200){throw new Error(result)}
        for(const business of result.data){
            mapById[business.id] = business.business_identifier;
            mapByIdentifier[business.business_identifier] = business.id;
        }
    }).catch(e=>{
        Notifications.show(e);
    });
    return {mapById: mapById, mapByIdentifier: mapByIdentifier};
}
let mapById=null
let mapByIdentifier=null;

/**
 * Saves tags of message information from server
 * @returns {Promise<{}>}
 */
async function getTagsOfMessage() {
    let messageTagsById = {};
    let messageTagsByName = {};
    await axios.get(SERVER_BASE + "/message/tag", {withCredentials: true}).then(result => {
        if (result.status !== 200) {
            throw new Error(result)
        }
        for (const tag of result.data) {
            messageTagsById[tag.id] = tag.tag_name;
            messageTagsByName[tag.tag_name] = tag.id;
        }
    }).catch(e => {
        Notifications.show(e);
    });
    return messageTagsByName;

}

/**
 * Saves business tags of message information from server
 * @returns {Promise<{}>}
 */
async function getBusinessTagsOfMessage() {
    let messageTagsById = {};
    let messageTagsByName = {};
    await axios.get(SERVER_BASE + "/message/business_tag", {withCredentials: true}).then(result => {
        if (result.status !== 200) {
            throw new Error(result)
        }
        for (const tag of result.data) {
            messageTagsById[tag.id] = tag.tag_name;
            messageTagsByName[tag.tag_name] = tag.id;
        }
    }).catch(e => {
        Notifications.show(e);
    });
    return messageTagsByName;

}

//TODO: Check loading
let tagsOfMessageByName = {};

//TODO: Check loading
let businessTagOfMessageByName = {};

/**
 * Returns business id instead of business identifier
 * @param {Number || String}businessIdentifier business identifier
 * @returns {Number || String}
 */
export function getBusinessId(businessIdentifier) {
    return mapByIdentifier[businessIdentifier]||-1;
}

/**
 * Changes value of business id from business identifier to business id
 * @param data {Object}
 */
export function fixBusinessId(data) {
    if(!(data instanceof Object))return;
    if (data["business_id"]) {
        data["business_id"] = getBusinessId(data["business_id"]);
    }
    if (data["businessId"]) {
        data["businessId"] = getBusinessId(data["businessId"]);
    }
    if (data["business_identifier"]) {
        data["id"] = getBusinessId(data["business_identifier"]);
        delete data["business_identifier"]
    }
    if (data["businessIdentifier"]) {
        data["id"] = getBusinessId(data["businessIdentifier"]);
        delete data["businessIdentifier"]
    }
}
/**
 * Converts data to form data if image sending
 * @param sentData {Object} data to send
 * @param config {Object} config
 * @param replaceWithBusinessId {Boolean}  replace business identifier with business id or no
 * @return {Object} data to send as correct format
 */
export function getData(sentData, config, replaceWithBusinessId=true) {
    let sendAsFormData = false;
    let formData = new FormData();
    if(replaceWithBusinessId)fixBusinessId(sentData);
    for (const k of Object.keys(sentData)) {
        if (sentData[k] instanceof File) {
            sendAsFormData = true;
            formData.append(k, sentData[k]);
            config['Content-Type'] = 'multipart/form-data/';
        }
    }
    if (sendAsFormData) {
        for (const k of Object.keys(sentData)) {
            if (sentData[k]) formData.set(k, sentData[k]);
        }
    }
    return sendAsFormData ? formData : sentData;
}



export * from './Functions/DataGeter';
export * from './Functions/DataPuter';
export * from './Functions/DataPoster';
export * from './Functions/DataDeleter';
export {tagsOfMessageByName};
export {businessTagOfMessageByName};