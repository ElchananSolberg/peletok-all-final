import {BehaviorSubject} from 'rxjs'
import {getCurrentUserData} from '../Components/DataProvider/DataProvider'
import { LanguageManager as LM } from '../Components/LanguageManager/Language';

export class UserDetailsService{
	static balance = new BehaviorSubject([])
	static currentUserStatus = new BehaviorSubject([])
	static usePoint = new BehaviorSubject([])
	static userDetails = null


	static getUserDetails(){
		if(UserDetailsService.userDetails){
			let pointToUse = LM.getString("pointToUse");
			let balance = UserDetailsService.userDetails.details.use_point ? UserDetailsService.userDetails.balance : UserDetailsService.userDetails.balance.filter(i => i.strRepr !== pointToUse);
			UserDetailsService.balance.next(balance);
			UserDetailsService.currentUserStatus.next(UserDetailsService.userDetails.details.status);
			UserDetailsService.usePoint.next(UserDetailsService.userDetails.details.use_point)
		} else {
			getCurrentUserData().then((res) => {
				let pointToUse = LM.getString("pointToUse");
				let balance = res.details.use_point ? res.balance : res.balance.filter(i => i.strRepr !== pointToUse);
				UserDetailsService.balance.next(balance);
				UserDetailsService.currentUserStatus.next(res.details.status);
				UserDetailsService.usePoint.next(res.details.use_point)
				UserDetailsService.userDetails = res;
				setTimeout(()=>{
					UserDetailsService.userDetails = null
				}, 5000)
			})
		}
	}

}