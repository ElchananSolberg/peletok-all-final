import React, { Component } from 'react';
import { LanguageManager as LM } from '../Components/LanguageManager/Language'

export const backToPreviousScreenHandler = (history) => {
    return (
        <h3 className={'pointer color-page-header fs-26 font-weight-light d-inline-block'}
            id='backToPreviousScreenBtn'
            onClick={() => {
                {
                    history.go(-1)
                }
            }}> {LM.getString("selectUserBack")}
        </h3>)
}


