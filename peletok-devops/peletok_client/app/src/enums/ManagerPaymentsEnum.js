
export class ManagerPaymentsEnum {
    static TRANSACTION            = '11';
    static PAYMENT                = '12';
    static ACTION                 = '13';
    static ACTIONANDPAYMENT       = '14';
    static DISTRIBUTIONACTIVITIES = '15';
    static OBLIGO                 = '16';
    static MANUALCARDS            = '17';
    static CARDSASSEMBLAGE        = '18';
    static EMAILREPORT            = '19';
}