from django.urls import include, path
from django.contrib import admin
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token
from rest_framework.urlpatterns import format_suffix_patterns
# from app import views
# from pay_bill import views
# from my_bills import views
# from road_tzafon import views


urlpatterns = [
    path(r'api/mast/', include('mast.urls')),
    path(r'api/city4u/', include('city4u.urls')),
    path(r'api/pay_bill/', include('pay_bill.urls')),
    path(r'api/my_bills/', include('my_bills.urls')),
    path(r'api/road_tzafon/', include('road_tzafon.urls')),
    path(r'api/hagihon/', include('hagihon.urls')),
    path(r'api/police/', include('police.urls')),
    path(r'api/six_road/', include('six_road.urls')),
    path(r'api/jawwal/', include('jawwal.urls')),
    path(r'api/api-token-auth/', obtain_jwt_token),
    path(r'api/api-token-refresh/', refresh_jwt_token),
    path('api/api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('admin/', admin.site.urls),
]
