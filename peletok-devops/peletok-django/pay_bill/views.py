from __future__ import unicode_literals
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import BasePermission
from rest_framework.decorators import api_view, permission_classes
from selenium.common.exceptions import TimeoutException, NoAlertPresentException
from utils.elements_selectors import PayBill
from utils.main_functions import MainFunctions

site_url = 'https://www.paybill.co.il'


@api_view(['POST'])
@permission_classes([BasePermission])
def post_get_price(request):
    """
    {
    "uri": "/Payment/WaterPayment?p=58",
    "payment_info": {
        "city_name": "פלגי מוצקין -דלית אל כרמל",
        "client_id": "15633308",
        "bill": "1151423"
        }
    }
    :param request:
    :return {status: <str> 'success' / 'error', price: <float> || error_message: <str>}
    """
    payment_handler = PayBillsPayment(site_url + str(request.data.get("uri")),
                                      request.data.get("payment_info"))
    res = payment_handler.get_price()
    return Response(res, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes([BasePermission])
def post_pay(request):
    """
    {
    "url": "https://www.paybill.co.il/Payment/WaterPayment?p=58",
    "payment_info": {
        "city_name": "פלגי מוצקין -דלית אל כרמל",
        "client_id": "15633308",
        "bill": "1151423",
        "full_name": "פלאטוק",
        "phone": "0548408908",
        "email": "mendyb@ravtech.co.il",
        "card_num": "4580110705790910",
        "card_cvv": "969",
        "card_valid_month": "06",
        "card_valid_year": "2023",
        "personalId": "012163234"
        }
    }
    :param request:
    :return {status: <str> 'success' / 'error', confirmation: <str> || error_message: <str>}
    """
    payment_handler = PayBillsPayment(site_url + str(request.data.get("uri")),
                                      request.data.get("payment_info"))
    res = payment_handler.pay()
    return Response(res, status=status.HTTP_200_OK)


class PayBillsPayment:
    def __init__(self, url: str, payment_info: dict):
        self.payment_info = payment_info
        self.url = url
        self.driver = MainFunctions(self.url)

    def is_alert_present(self):
        try:
            alert_text = self.driver.get_alert_text()
            self.driver.dismiss_alert()
            return alert_text
        except NoAlertPresentException and TimeoutException:
            return False

    def first_steps(self):
        return_obj = {}
        try:
            self.driver.select_by_text(PayBill.idMunicipalityNameInput,
                                       self.payment_info['city_name'])
            self.driver.click_button(PayBill.idNextButton1)
            self.driver.send_keys(PayBill.idClearinghouseInput, self.payment_info['client_id'])
            self.driver.send_keys(PayBill.idHesbonInput, self.payment_info['bill'])
            return True
        except TimeoutException:
            return_obj["status"] = 'error'
            return_obj["error_message"] = "המערכת לא הצליחה להתחבר, אנא נסה שוב מאוחר יותר."
            return return_obj

    def get_price(self):
        return_obj = {}
        begin = self.first_steps()
        if type(begin) is dict:
            return begin
        elif begin:
            self.driver.click_button(PayBill.idAmountInput)
            payed = self.is_alert_present()
            if payed:
                return_obj["status"] = 'error'
                return_obj["error_message"] = payed
            else:
                price_sum = self.driver.get_value_from_element(PayBill.idAmountInput)
                return_obj["status"] = 'success'
                return_obj["price"] = price_sum
        self.driver.driver.close()
        return return_obj

    def pay(self):
        return_obj = {}
        begin = self.first_steps()
        if type(begin) is dict:
            return begin
        elif begin:
            self.driver.click_button(PayBill.idAmountInput)
            payed = self.is_alert_present()
            if payed:
                return_obj["status"] = 'error'
                return_obj["error_message"] = payed
            else:
                self.driver.click_button(PayBill.idNextButton2)
                self.driver.send_keys(PayBill.idEmailInput, self.payment_info['email'])
                self.driver.click_button(PayBill.idRegularPayButton)
                self.driver.send_keys(PayBill.idFullNameInForm, self.payment_info['full_name'])
                self.driver.send_keys(PayBill.idPhoneNumberInForm, self.payment_info['phone'])
                self.driver.send_keys(PayBill.idCreditCardNumberInForm,
                                      self.payment_info['card_num'])
                self.driver.select_from_selector(PayBill.idMonthExpInForm,
                                                 self.payment_info['card_valid_month'])
                self.driver.select_from_selector(PayBill.idYearExpInForm,
                                                 self.payment_info['card_valid_year'])
                self.driver.send_keys(PayBill.idCvvNumInForm, self.payment_info['card_cvv'])
                self.driver.send_keys(PayBill.idPersonalIdInForm, self.payment_info['personalId'])
                self.driver.click_button(PayBill.idPayButtonInForm)
                confirmation_number = self.driver.get_text_from_element(
                    PayBill.idConfirmationNumber)
                return_obj['status'] = 'success'
                return_obj['confirmation'] = confirmation_number
        return return_obj
