from __future__ import unicode_literals
import datetime
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import BasePermission
from rest_framework.response import Response
from selenium.common.exceptions import NoSuchElementException, TimeoutException
from utils.elements_selectors import Mast
from utils.main_functions import MainFunctions

site_url = 'https://www.pay24.co.il'


@api_view(['POST'])
@permission_classes([BasePermission])
def post_get_price(request):
    """
    {
    "uri":"/payDesign/SearchPay/SearchPay?ReshutMast=12&payOrigin=MAST_Web",
    "payment_info": {
        "client_id": "190945298",
        "bill": "51395028"
          }
    }
    :param request:
    :return {status: <str> 'success' / 'error', price: <float> || error_message: <str>}
    """
    payment_handler = MastPayment(site_url + str(request.data.get("uri")),
                                  request.data.get("payment_info"))
    res = payment_handler.get_price()
    return Response(res, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes([BasePermission])
def post_pay(request):
    """
    {
    "uri":"/payDesign/SearchPay/SearchPay?ReshutMast=12&payOrigin=MAST_Web",
    "payment_info": {
        "client_id": "190945298",
        "bill": "51395028",
        "full_name": "פלאטוק",
        "phone": "0548408908",
        "email": "mendyb@ravtech.co.il",
        "card_num": "4580110705790910",
        "card_cvv": "969",
        "card_valid_month": "06",
        "card_valid_year": "2023",
        "personalId": "012163234"
        }
    }
    :param request:
    :return {status: <str> 'success' / 'error', confirmation: <str> || error_message: <str>}
    """
    payment_handler = MastPayment(site_url + str(request.data.get("url")),
                                  request.data.get("payment_info"))
    res = payment_handler.pay(request.data.get("cc_info"))
    return Response(res, status=status.HTTP_200_OK)


class MastPayment:

    def __init__(self, url: str, payment_info: dict):
        self.payment_info = payment_info
        self.url = url
        self.driver = MainFunctions(self.url)

    def first_steps(self):
        try:
            self.driver.send_keys(Mast.idMeshalemInput, self.payment_info["client_id"])
            self.driver.send_keys(Mast.idSefachInput, self.payment_info["bill"])
            self.driver.click_button(Mast.idNextButton)
            return True
        except TimeoutException:
            return False

    def get_price(self):
        begin = self.first_steps()
        return_obj = {}
        if begin:
            try:
                error_message = self.driver.get_text_from_element(Mast.infoMessage)
                return_obj["status"] = 'error'
                return_obj["error_message"] = error_message
            except NoSuchElementException:
                price_sum = self.driver.get_value_from_element(Mast.idPriceValue)
                return_obj["status"] = 'success'
                return_obj["price"] = price_sum
        else:
            return_obj["status"] = 'error'
            return_obj["error_message"] = "המערכת לא הצליחה להתחבר, אנא נסה שוב מאוחר יותר."
        self.driver.driver.close()
        return return_obj

    def pay(self, cc_info):
        return_obj = {}
        self.first_steps()
        try:
            error_message = self.driver.get_text_from_element(Mast.infoMessage)
            return_obj["status"] = 'error'
            return_obj["error_message"] = error_message
        except NoSuchElementException or TimeoutException:

            self.driver.send_keys(Mast.payAccount, cc_info["cc_num"])
            self.driver.send_keys(Mast.payCvv, cc_info["cvv"])
            self.driver.send_keys(Mast.ddMonth, cc_info["ex_date"]["month"])
            self.driver.send_keys(Mast.ddYear, cc_info["ex_date"]["year"])
            self.driver.send_keys(Mast.payerDetails, cc_info["payer_name"])
            self.driver.send_keys(Mast.payerIdNum, cc_info["payer_id_num"])
            self.driver.send_keys(Mast.payerPhone, cc_info["payer_phone_num"])
            self.driver.send_keys(Mast.payerEmail, cc_info["email"])
            self.driver.click_button(Mast.validButton)
            try:
                return_obj["status"] = 'error'
                return_obj["error_message"] = self.driver.get_text_from_element(
                    Mast.errorMessage)
            except TimeoutException:
                return_obj["status"] = 'success'
                return_obj["confirmation"] = self.driver.get_text_from_element(
                    Mast.successNum)
                return_obj["full_response"] = self.driver.get_text_from_element(
                    Mast.fullResponse)
                date = str(datetime.date.today()).replace("-", "_")
                screen_shot_file_name = str(self.payment_info["bill"]) + f"__{date}"
                self.driver.driver.save_screenshot(f"{screen_shot_file_name}.png")
                self.driver.driver.close()
                return_obj["screen_shot_file_name"] = screen_shot_file_name
        return return_obj
