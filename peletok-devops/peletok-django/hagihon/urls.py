from django.urls import path
from . import views

urlpatterns = [
    path(
        r'get_price/',
        views.post_get_price,
        name='get_price'
    ),
    path(
        r'pay/',
        views.post_pay,
        name='pay'
    )
]
