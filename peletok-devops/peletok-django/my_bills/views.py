from selenium.common.exceptions import TimeoutException
from rest_framework.permissions import BasePermission
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from time import sleep
from utils.elements_selectors import MyBills
from utils.main_functions import MainFunctions

site_url = 'https://www.mybills.co.il'


@api_view(['POST'])
@permission_classes([BasePermission])
def post_get_price(request):
    """
    {
    "uri": "/payments/154/%d7%aa%d7%9c_%d7%90%d7%91%d7%99%d7%91_%d7%97%d7%
    a0%d7%99%d7%94_%d7%97%d7%a0%d7%99%d7%94_%d7%90%d7%99%d7%a0%d7%98%d7%a8%d7%a0%d7%98.html",
    "payment_info": {
        "client_id": "73418301",
        "bill": "2000568881"
        }
    }
    :param request:
    :return {status: <str> 'success' / 'error', price: <float> || error_message: <str>}
    """
    payment_handler = MyBillsPayment(site_url + str(request.data.get("uri")),
                                     request.data.get("payment_info"))
    res = payment_handler.get_price()
    return Response(res, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes([BasePermission])
def post_pay(request):
    """
    {
    "uri": "/payments/154/%d7%aa%d7%9c_%d7%90%d7%91%d7%99%d7%91_%d7%97%d7%
    a0%d7%99%d7%94_%d7%97%d7%a0%d7%99%d7%94_%d7%90%d7%99%d7%a0%d7%98%d7%a8%d7%a0%d7%98.html",
    "payment_info": {
        "client_id": "73418301",
        "bill": "2000568881",
        "full_name": "פלאטוק",
        "phone": "0548408908",
        "email": "mendyb@ravtech.co.il",
        "card_num": "4580110705790910",
        "card_cvv": "969",
        "card_valid_month": "06",
        "card_valid_year": "2023",
        "personalId": "012163234"
        }
    }
    :param request:
    :return {status: <str> 'success' / 'error', confirmation: <str> || error_message: <str>}
    """
    payment_handler = MyBillsPayment(site_url + str(request.data.get("uri")),
                                     request.data.get("payment_info"))
    res = payment_handler.pay()
    return Response(res, status=status.HTTP_200_OK)


class MyBillsPayment:

    def __init__(self, url: str, payment_info: dict):
        self.payment_info = payment_info
        self.url = url
        self.driver = MainFunctions(self.url)

    def first_steps(self):
        return_obj = {}
        try:
            self.driver.send_keys(MyBills.idClientIdInput, self.payment_info["client_id"])
            self.driver.send_keys(MyBills.idBillIdInput, self.payment_info["bill"])
            self.driver.click_button(MyBills.idAmountButton)
            if self.driver.check_if_element_exits(MyBills.idErrorPaymentPaid)[0]:
                msg_txt = self.driver.get_text_from_element(MyBills.idErrorPaymentPaid)
                return msg_txt
            else:
                price = float(self.driver.get_value_from_element(MyBills.idFieldAmount))
                return price
        except TimeoutException:
            return "המערכת לא הצליחה להתחבר, אנא נסה שוב מאוחר יותר."

    def get_price(self):
        begin = self.first_steps()
        return_obj = {}
        if type(begin) is float:
            return_obj["status"] = 'success'
            return_obj["price"] = begin
        elif type(begin) is str:
            return_obj["status"] = 'error'
            return_obj["error_message"] = begin
        self.driver.driver.close()
        return return_obj

    def pay(self):
        return_obj = {}
        begin = self.first_steps()
        if isinstance(begin, str):
            return_obj["status"] = 'error'
            return_obj["error_message"] = begin
            return return_obj
        else:
            self.driver.click_button(MyBills.idConfirmButton)
            self.driver.send_keys(MyBills.idFirstNameField, self.payment_info["full_name"])
            self.driver.send_keys(MyBills.idLastNameField, self.payment_info["full_name"])
            self.driver.send_keys(MyBills.idPhoneField, self.payment_info["phone"])
            self.driver.send_keys(MyBills.idEmailField, self.payment_info["email"])
            self.driver.send_keys(MyBills.idIdNumberField, self.payment_info["personalId"])
            self.driver.send_keys(MyBills.idCardNumberField, self.payment_info["card_num"])
            self.driver.select_by_text(MyBills.idYearValid, self.payment_info["card_valid_year"])
            self.driver.select_by_text(MyBills.idMonthValid,
                                       self.payment_info["card_valid_month"])
            self.driver.click_button(MyBills.idSubmitButton)

            # check if the sum is the same.
            # amount = self.driver.get_text_from_element(id_total_amount)
            # print(amount)
            # print(type(amount))
            # print('****')
            # print(begin)
            # print(type(begin))
            # if amount != begin:
            #     return_obj["error"] = 'Error 2'

            self.driver.select_by_index(MyBills.idDealTypeSelector, 1)
            sleep(0.5)
            self.driver.select_by_index(MyBills.idNumOfPaySelector, 1)
            self.driver.click_button(MyBills.idConfirmPayButton)

            # payments_details = driver.get_text_from_element(id_payments_details)
            # return payments_details
            print('***********')
            print(self.driver.driver.find_element_by_tag_name("body").text)
            print('***********')

            payments_details = self.driver.get_text_in_elements(MyBills.classPaymentsDetails, 1)
            print(payments_details)
            return_obj["Reference No"] = payments_details

            # self.driver.driver.close()
            sleep(180)
            print(return_obj)
            return return_obj
