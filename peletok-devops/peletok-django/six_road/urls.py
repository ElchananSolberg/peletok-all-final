from django.urls import path
from . import views


urlpatterns = [
    path(
        r'check_availability/',
        views.post_check_availability,
        name='get_price'
    ),
    path(
        r'pay/',
        views.post_pay,
        name='pay'
    )
]