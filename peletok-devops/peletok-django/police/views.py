from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import BasePermission
from rest_framework.decorators import api_view, permission_classes
from selenium.common.exceptions import NoSuchFrameException, TimeoutException
from utils.elements_selectors import Police
from utils.main_functions import MainFunctions


@api_view(['POST'])
@permission_classes([BasePermission])
def post_get_price(request):
    """
    {
    "uri": "https://ecom.gov.il/voucherspa/input/318",
    "payment_info": {
        "bill": "90515945278"
        }
    }
    :return {status: <str> 'success' / 'error', price: <float> || error_message: <str>}
    """
    payment_handler = PolicePayment(str(request.data.get("uri")), request.data.get("payment_info"))
    res = payment_handler.get_price()
    return Response(res, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes([BasePermission])
def post_pay(request):
    """
    {
    "uri": "https://ecom.gov.il/voucherspa/input/318",
    "payment_info": {
        "bill": "90515945278",
        "full_name": "פלאטוק",
        "phone": "0548408908",
        "email": "gurvicheli@gmail.com",
        "card_num": "4580110705790910",
        "card_cvv": "969",
        "card_valid_month": "06",
        "card_valid_year": "2023",
        "personalId": "012163234"
        }
    }
    :return {status: <str> 'success' / 'error', confirmation: <int> || error_message: <str>}
    """
    payment_handler = PolicePayment(str(request.data.get("uri")), request.data.get("payment_info"))
    res = payment_handler.pay()
    return Response(res, status=status.HTTP_200_OK)


class PolicePayment:

    def __init__(self, url: str, payment_info: dict):
        self.url = url
        self.payment_info = payment_info
        self.driver = MainFunctions(self.url)

    def first_steps(self):
        try:
            self.driver.wait_for_element(Police.reportRadioButton)
            self.driver.click_button_by_doc(Police.reportRadioButton[1])
            self.driver.send_keys(Police.reportInput, self.payment_info['bill'])
            try:
                self.driver.driver.switch_to.frame(0)
                self.driver.click_button(Police.notRobot)
            except NoSuchFrameException:
                pass
            self.driver.click_button(Police.submitButton)
            self.driver.wait_for_element_until_not_displayed(Police.loading)
            try:
                self.driver.wait_for_element(Police.selectReport)
                class_name = self.driver.driver.find_elements(*Police.selectReport)[
                    1].get_attribute("class")
                if 'ui-state-active' not in class_name:
                    self.driver.click_button_in_elements(Police.selectReport, 1)
                price = [x.text for x in self.driver.driver.find_elements(*Police.dataForm)][-2]
                return float(price)
            except TimeoutException:
                message = self.driver.get_text_from_element(Police.description)
                return message
        except TimeoutException:
            return "המערכת לא הצליחה להתחבר, אנא נסה שוב מאוחר יותר."

    def get_price(self):
        begin = self.first_steps()
        return_obj = {}
        if isinstance(begin, str):
            return_obj["status"] = 'error'
            return_obj["error_message"] = begin
        elif isinstance(begin, float):
            return_obj["status"] = 'success'
            return_obj["price"] = begin
        return return_obj

    def pay(self):
        begin = self.first_steps()
        return_obj = {}
        if isinstance(begin, str):
            return_obj["status"] = 'error'
            return_obj["error_message"] = begin
            return return_obj
        elif isinstance(begin, float):
            try:
                self.driver.click_button_in_elements(Police.ccPay, 0)
                self.driver.send_keys(Police.nameInput, self.payment_info['full_name'])
                self.driver.send_keys(Police.emailInput, self.payment_info['email'])
                self.driver.send_keys(Police.phoneInput, self.payment_info['phone'])
                self.driver.send_keys(Police.cardNumberInput, self.payment_info['card_num'])
                self.driver.select_by_value(Police.yearsInput,
                                            self.payment_info['card_valid_year'])
                self.driver.select_by_value(Police.monthsInput,
                                            self.payment_info['card_valid_month'])
                self.driver.send_keys(Police.idInput, self.payment_info['personalId'])
                self.driver.send_keys(Police.cvvInput, self.payment_info['card_cvv'])
                self.driver.click_button(Police.checkbox)
                if not self.driver.check_element_is_enabled(Police.submit):
                    return_obj["status"] = 'error'
                    return_obj["error_message"] = 'לא הוכנסו כל הנתונים הנדרשים.'
                    return return_obj
                else:
                    self.driver.click_button(Police.submit)
                    self.driver.click_button_in_elements(Police.confirmButton2, 1)
                    confirmation_number = self.driver.get_text_in_elements(
                        Police.confirmationNumber, 1)
                    confirmation_number = ''.join(
                        [x for x in confirmation_number if x.isnumeric()])
                    return_obj["status"] = 'success'
                    return_obj["confirmation"] = confirmation_number
                    return return_obj
            except TimeoutException:
                return_obj["status"] = 'error'
                return_obj["error_message"] = "המערכת לא הצליחה להתחבר, אנא נסה שוב מאוחר יותר."
                return return_obj
