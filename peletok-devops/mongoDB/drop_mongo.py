import pymongo
import os

mongo_client = os.environ.get('MONGODB_CONN_STRING_ADMIN')


db_name = "peletok-staging"
col_name = "responses"


def drop_mongodb_collection(conn_string, db_name, collection_name):
    mongo_client = pymongo.MongoClient(conn_string)
    db_name = mongo_client[db_name]
    col_name = db_name[collection_name]
    col_name.drop()

drop_mongodb_collection(mongo_client, db_name, col_name)
