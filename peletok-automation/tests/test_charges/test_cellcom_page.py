from tests.base_project_test_class import *


class TestCellcomPage(BaseProjectTestClass):

    def login_and_get_to_cellcom(self):
        self.login()
        self.driver.tools.wait_and_click(HomePage.cellcom_icon)
        self.wait_and_click_if_virtual()

    def test_cellcom_click_on_back_button(self):
        self.login_and_get_to_cellcom()
        self.click_on_back_botton()
        self.after_the_click_back_button()
        self.remove_logs_dir = True

    def test_check_cellcom_page(self):
        self.login_and_get_to_cellcom()
        self.remove_logs_dir = True

    def test_cellcom_talkman_25(self):
        self.login_and_get_to_cellcom()
        self.pay_for_calling_card_vc(25, "94/virtual/1030")
        self.remove_logs_dir = True

    def test_cellcom_talkman_49(self):
        self.login_and_get_to_cellcom()
        self.pay_for_calling_card_vc(49, "94/virtual/1031")
        self.remove_logs_dir = True

    def test_cellcom_talkman_59(self):
        self.login_and_get_to_cellcom()
        self.pay_for_calling_card_vc(59, "94/virtual/1032")
        self.remove_logs_dir = True

    def test_check_alert_favorite_tag_and_wait_btn_annul_cellcom(self):
        self.login_and_get_to_cellcom()
        self.click_favorite_tag_and_wait_btn_annul()
        self.remove_logs_dir = True

    def test_check_alert_favorite_tag_and_wait_btn_confirm_cellcom(self):
        self.login_and_get_to_cellcom()
        self.click_favorite_tag_and_wait_btn_confirm()
        self.remove_logs_dir = True

    def test_check_if_add_favorite_cards_cellcom(self):
        self.login_and_get_to_cellcom()
        self.click_favorite_tag_and_check_if_add_moadafim()
        self.remove_logs_dir = True
