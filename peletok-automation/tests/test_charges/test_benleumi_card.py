from tests.base_project_test_class import *


class TestBenleumiCards(BaseProjectTestClass):

    def login_and_get_to_benleumi(self):
        self.login()
        self.driver.tools.wait_and_click(HomePage.benleumi_icon)

    def test_benleumi_click_on_back_button(self):
        self.login_and_get_to_benleumi()
        self.click_on_back_botton()
        self.after_the_click_back_button()
        self.remove_logs_dir = True

    def test_benleumi_13(self):
        self.login_and_get_to_benleumi()
        self.pay_for_calling_card_mc(13, "124/103")
        self.remove_logs_dir = True

    def test_check_alert_favorite_tag_and_wait_btn_annul_benleumi(self):
        self.login_and_get_to_benleumi()
        self.click_favorite_tag_and_wait_btn_annul()
        self.remove_logs_dir = True

    def test_check_alert_favorite_tag_and_wait_btn_confirm_benleumi(self):
        self.login_and_get_to_benleumi()
        self.click_favorite_tag_and_wait_btn_confirm()
        self.remove_logs_dir = True

    @pytest.mark.skip("development problem, for it to work you have to press the back button and press the card again")
    def test_check_if_add_favorite_cards_benleumi(self):
        self.login_and_get_to_benleumi()
        self.click_favorite_tag_and_check_if_add_moadafim()
        self.remove_logs_dir = True
