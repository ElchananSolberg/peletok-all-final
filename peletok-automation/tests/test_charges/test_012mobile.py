from tests.base_project_test_class import *


class Test012mobileCards(BaseProjectTestClass):

    def login_and_get_to_012mobile(self):
        self.login()
        self.driver.tools.wait_and_click(HomePage.zero_one_two_mobile_icon)
        self.remove_logs_dir = True

    def test_012mobile_click_on_back_button(self):
        self.login_and_get_to_012mobile()
        self.click_on_back_botton()
        self.after_the_click_back_button()
        self.remove_logs_dir = True

    def test_012mobile_callingcard_25(self):
        self.login_and_get_to_012mobile()
        self.pay_for_calling_card_vc(25, "201/1506")
        self.remove_logs_dir = True

    def test_012mobile_callingcard_45(self):
        self.login_and_get_to_012mobile()
        self.pay_for_calling_card_vc(45, "201/1500")
        self.remove_logs_dir = True

    def test_012mobile_callingcard_50(self):
        self.login_and_get_to_012mobile()
        self.pay_for_calling_card_vc(50, "201/1501")
        self.remove_logs_dir = True

    def test_check_alert_favorite_tag_and_wait_btn_annul_012mobile(self):
        self.login_and_get_to_012mobile()
        self.click_favorite_tag_and_wait_btn_annul()
        self.remove_logs_dir = True

    def test_check_alert_favorite_tag_and_wait_btn_confirm_012mobile(self):
        self.login_and_get_to_012mobile()
        self.click_favorite_tag_and_wait_btn_confirm()
        self.remove_logs_dir = True

    def test_check_if_add_favorite_cards_012mobile(self):
        self.login_and_get_to_012mobile()
        self.click_favorite_tag_and_check_if_add_moadafim()
        self.remove_logs_dir = True
