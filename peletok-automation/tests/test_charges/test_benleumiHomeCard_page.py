from tests.base_project_test_class import *


class TestBenleumiHomeCardCards(BaseProjectTestClass):

    def login_and_get_to_benleumiHomeCard(self):
        self.login()
        self.driver.tools.wait_and_click(HomePage.benleumiHomeCard_icon)

    def test_BenleumiHomeCard_click_on_back_button(self):
        self.login_and_get_to_benleumiHomeCard()
        self.click_on_back_botton()
        self.after_the_click_back_button()
        self.remove_logs_dir = True

    def test_check_BenleumiHomeCard_page(self):
        self.login_and_get_to_benleumiHomeCard()
        self.remove_logs_dir = True

    def test_benleumiHomeCard_talkman_20(self):
        self.login_and_get_to_benleumiHomeCard()
        self.pay_for_calling_card_vc(20, "211/2002")
        self.remove_logs_dir = True

    def test_benleumiHomeCard_talkman_30(self):
        self.login_and_get_to_benleumiHomeCard()
        self.pay_for_calling_card_vc(30, "211/2003")
        self.remove_logs_dir = True

    def test_benleumiHomeCard_talkman_74(self):
        self.login_and_get_to_benleumiHomeCard()
        self.pay_for_calling_card_vc(74, "211/2026")
        self.remove_logs_dir = True

    def test_check_alert_favorite_tag_and_wait_btn_annul_benleumiHomeCard(self):
        self.login_and_get_to_benleumiHomeCard()
        self.click_favorite_tag_and_wait_btn_annul()
        self.remove_logs_dir = True

    def test_check_alert_favorite_tag_and_wait_btn_confirm_benleumiHomeCard(self):
        self.login_and_get_to_benleumiHomeCard()
        self.click_favorite_tag_and_wait_btn_confirm()
        self.remove_logs_dir = True

    def test_check_if_add_favorite_cards_benleumiHomeCard(self):
        self.login_and_get_to_benleumiHomeCard()
        self.click_favorite_tag_and_check_if_add_moadafim()
        self.remove_logs_dir = True

