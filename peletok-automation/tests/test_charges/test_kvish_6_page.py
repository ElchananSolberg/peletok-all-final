from tests.base_project_test_class import *


class TestKvish6(BaseProjectTestClass):

    def login_and_click_kvish_6_icone(self):
        self.login()
        self.driver.tools.wait_and_click(HomePage.kvish_6)
        self.driver.wait.wait_for_element_to_be_present(VerificationPage.img_logo_kvish_6)

    def test_login_kvish6(self):
        self.login_and_click_kvish_6_icone()
        self.remove_logs_dir = True

    def test_kvish6_click_on_back_button(self):
        self.login_and_click_kvish_6_icone()
        self.click_on_back_botton()
        self.after_the_click_back_button()
        self.remove_logs_dir = True

    def set_form_kvish_6(self):
        self.driver.tools.set_text(FormKvish6.input_id_kvish_6, User.id_user)
        self.driver.tools.set_text(FormKvish6.input_last_six_digits_of_invoice_kvish_6, User.last_six_digits_of_invoice)
        self.driver.tools.set_text(FormKvish6.invoice_amount_kvish_6, Invoice.price_invoice)
        self.driver.tools.set_text(FormKvish6.pre_phone_number, User.phone[:3])
        self.driver.tools.set_text(FormKvish6.phone_number, User.phone[3:])

    def click_btn_charging_kvish_6_and_verify(self):
        self.driver.tools.wait_and_click(FormKvish6.btn_charging_kvish_6)
        self.driver.wait.wait_for_element_to_be_present(VerificationPage.alert_success_kvish_6)

    def check_total_amount_to_pay_kvish_6(self):
        assert Invoice.price_invoice + 7 == Invoice.total_invoice_amount_value

    def test_login_and_fill_form_and_click_btn_charging(self):
        self.login_and_click_kvish_6_icone()
        self.set_form_kvish_6()
        self.check_total_amount_to_pay_kvish_6()
        self.click_btn_charging_kvish_6_and_verify()
        self.remove_logs_dir = True