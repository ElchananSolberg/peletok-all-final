from tests.test_suppliers_and_products.base_suppliers_and_products_test_class import *
import pytest


class TestSuppliersProfile(BaseSuppliersAndProductsTestClass):

    data_of_test_supplier_profile = {"סלקום": {
            "name_supplier": "סלקום",
            "InputTypeTextname":                    ["סלקום", "סלקום  99"],
            "InputTypeNumbercontact_phone_number":  ["0526136468", "9999999999"],
            "InputTypeAddressaddress":              ["3453450", "0"],
            "InputTypeSearchListtype":              ["prepaid", "bills"],
            "InputTypeNumberorder":                 ["2", "1"],
            "InputTypeTextcontact_name":            ["999999999", "1111111111"],
            "InputTypeSearchListcredit_card_id":    ["ויזה xxxxxxxxxxxx3090", "אמריקן אקספרס xxxxxxxxxxxx3020"],
            "InputTypeNumbermax_percentage_profit": ["3", "9"],
            "InputTypeNumbermin_percentage_profit": ["155", "999"],
            "maximum_amount_payable":               ["1005", "9999"],
            "credit_limitation":                    ["1111", "9999"]}
            }

    def click_on_suppliers_tab(self):
        self.login_and_get_in_suppliers_and_products()
        self.click_on_a_tab("suppliers")

    def replace_database_in_inputs(self, locator, data_of_replace):
        self.driver.tools.clear_value(locator)
        self.driver.tools.set_text(locator, data_of_replace)
        self.driver.tools.wait_and_click(SuppliersPage.supplier_Save)
        self.driver.wait.wait_for_element_to_be_present(SuppliersPage.alert_success)

    def login_and_check_database_in_inputs(self, selector, name_supplier, type_attribute_the_test,
                                           data_of_check):
        self.click_on_suppliers_tab()
        self.click_on_input_and_choose_option(input_selector=SuppliersPage.input_type_search_list_slelct_supplier,
                                              choice_name=name_supplier)
        assert self.driver.tools.wait_and_get_attribute_when_present(
            selector, type_attribute_the_test, raise_exception=False) == data_of_check, "not find element"

    def template_tests_inputs(self, selector, name_input=None, name_supplier=None):
        if not name_input:
            name_input = selector[1]
        for data_of_supplier_current in self.data_of_test_supplier_profile.values():
            self.login_and_check_database_in_inputs(selector, data_of_supplier_current["name_supplier"], "value",
                                                    data_of_supplier_current[name_input][0])
            self.replace_database_in_inputs(selector, data_of_supplier_current[name_input][1])
            self.restart_driver()
            if not name_supplier:
                name_supplier = data_of_supplier_current["name_supplier"]
            self.login_and_check_database_in_inputs(selector, name_supplier, "value", data_of_supplier_current[name_input][1])
            self.replace_database_in_inputs(selector, data_of_supplier_current[name_input][0])

    def template_tests_react_select(self, selector):
        for data_of_supplier_current in self.data_of_test_supplier_profile.values():
            self.click_on_suppliers_tab()
            self.click_on_input_and_choose_option(input_selector=SuppliersPage.input_type_search_list_slelct_supplier,
                                                  choice_name=data_of_supplier_current["name_supplier"])
            assert data_of_supplier_current[selector[1]][0].replace(" ", "") in \
                   self.driver.tools.get_text_from_element(selector).replace(" ", ""), 'your choice is not in the input'
            self.click_on_input_and_choose_option(input_selector=selector,
                                                  choice_name=data_of_supplier_current[selector[1]][1])
            self.driver.tools.wait_and_click(SuppliersPage.supplier_Save)
            self.driver.wait.wait_for_element_to_be_present(SuppliersPage.alert_success)
            self.restart_driver()
            self.click_on_suppliers_tab()
            self.click_on_input_and_choose_option(input_selector=SuppliersPage.input_type_search_list_slelct_supplier,
                                                  choice_name=data_of_supplier_current["name_supplier"])
            assert data_of_supplier_current[selector[1]][1].replace(" ", "") in \
                   self.driver.tools.get_text_from_element(selector).replace(" ", ""), 'your choice is not in the input'
            self.click_on_input_and_choose_option(input_selector=selector,
                                                  choice_name=data_of_supplier_current[selector[1]][0])
            self.driver.tools.wait_and_click(SuppliersPage.supplier_Save)
            self.driver.wait.wait_for_element_to_be_present(SuppliersPage.alert_success)

    def template_tests_checkboxes(self, selector_checkbox, selector_label_of_checkbox):
        for data_of_supplier_current in self.data_of_test_supplier_profile.values():
            self.login_and_check_database_in_inputs(selector_checkbox, data_of_supplier_current["name_supplier"], "checked",
                                          "true")
            self.driver.tools.wait_and_click(selector_label_of_checkbox)
            self.driver.tools.wait_and_click(SuppliersPage.supplier_Save)
            self.driver.wait.wait_for_element_to_be_present(SuppliersPage.alert_success)
            self.restart_driver()
            self.login_and_check_database_in_inputs(
                selector_checkbox, data_of_supplier_current["name_supplier"], "checked", False)
            self.driver.tools.wait_and_click(selector_label_of_checkbox)
            self.driver.tools.wait_and_click(SuppliersPage.supplier_Save)
            self.driver.wait.wait_for_element_to_be_present(SuppliersPage.alert_success)

    def test_suppliers_input_type_search_list_slelct_supplier(self):
        self.click_on_suppliers_tab()
        self.driver.wait.wait_for_element_to_be_present(SuppliersPage.input_type_search_list_slelct_supplier)
        self.remove_logs_dir = True

    def test_suppliers_tab_input_type_text_name(self):
        self.template_tests_inputs(selector=SuppliersPage.input_type_text_name, name_supplier="סלקום  99")
        self.remove_logs_dir = True

    def test_suppliers_tab_input_type_number_contact_phone_number(self):
        self.template_tests_inputs(selector=SuppliersPage.input_type_number_contact_phone_number)
        self.remove_logs_dir = True

    def test_suppliers_tab_input_type_address_address(self):
        self.template_tests_inputs(selector=SuppliersPage.input_type_address_address)
        self.remove_logs_dir = True

    def test_suppliers_tab_input_type_number_order(self):
        self.template_tests_inputs(selector=SuppliersPage.input_type_number_order)
        self.remove_logs_dir = True

    def test_suppliers_tab_input_type_text_contact_name(self):
        self.template_tests_inputs(selector=SuppliersPage.input_type_text_contact_name)
        self.remove_logs_dir = True

    def test_suppliers_tab_input_type_check_box_status(self):
        self.template_tests_checkboxes(selector_checkbox=SuppliersPage.input_type_check_box_status,
                                       selector_label_of_checkbox=SuppliersPage.label_of_input_type_check_box_status)
        self.remove_logs_dir = True

    def test_suppliers_tab_input_type_check_box_default_creation(self):
        self.template_tests_checkboxes(selector_checkbox=SuppliersPage.input_type_check_box_default_creation,
                                       selector_label_of_checkbox=SuppliersPage.
                                       label_of_input_type_check_box_default_creation)
        self.remove_logs_dir = True

    def test_suppliers_tab_input_type_check_box_cancel_option(self):
        self.template_tests_checkboxes(selector_checkbox=SuppliersPage.input_type_check_box_cancel_option,
                                       selector_label_of_checkbox=SuppliersPage.
                                       label_of_input_type_check_box_cancel_option)
        self.remove_logs_dir = True

    def test_suppliers_tab_input_type_number_max_percentage_profit(self):
        self.template_tests_inputs(selector=SuppliersPage.input_type_number_max_percentage_profit)
        self.remove_logs_dir = True

    def test_suppliers_tab_input_type_number_min_percentage_profit(self):
        self.template_tests_inputs(selector=SuppliersPage.input_type_number_min_percentage_profit)
        self.remove_logs_dir = True

    def test_suppliers_tab_maximum_amount_payable(self):
        self.template_tests_inputs(selector=SuppliersPage.maximum_amount_payable, name_input="maximum_amount_payable")
        self.remove_logs_dir = True

    def test_suppliers_tab_credit_limitation(self):
        self.template_tests_inputs(selector=SuppliersPage.credit_limitation, name_input="credit_limitation")
        self.remove_logs_dir = True

    @pytest.mark.skip("Different value cannot be saved, The bug is known")
    def test_input_type_search_list_type(self):
        self.template_tests_react_select(SuppliersPage.input_type_search_list_type)
        self.remove_logs_dir = True

    def test_input_type_search_list_credit_card_id(self):
        self.template_tests_react_select(SuppliersPage.input_type_search_list_credit_card_id)
        self.remove_logs_dir = True
