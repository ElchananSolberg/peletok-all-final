from tests.test_suppliers_and_products.base_suppliers_and_products_test_class import *


class TestProfileAmlot(BaseSuppliersAndProductsTestClass):

    def get_into_prfile_amlot(self):
        self.login_and_get_in_suppliers_and_products()
        self.driver.tools.wait_and_click(ProfileAmlot.profile_amlot_link)
        self.driver.wait.wait_for_element_to_be_present(ProfileAmlot.add_now_profile_btn)

    def get_last_option_from_select(self, select_path):
        last_option = self.get_list_of_options(select_path)[-1]
        self.driver.tools.wait_and_click(select_path)
        return last_option

    def click_add_new_profile_btn(self):
        self.driver.wait.wait_for_element_to_be_present(ProfileAmlot.add_now_profile_btn)
        self.driver.tools.wait_and_click(ProfileAmlot.add_now_profile_btn)
        self.driver.wait.wait_for_element_to_be_present(ProfileAmlot.popup)

    def click_x_btn_in_btn_popup(self, btn_locator, x_btn_locator):
        self.driver.tools.wait_and_click(btn_locator)
        self.driver.wait.wait_for_element_to_be_present(x_btn_locator)
        self.driver.tools.wait_and_click(x_btn_locator)
        self.driver.wait.wait_for_element_to_be_present(btn_locator)

    def click_cancel_btn_in_btn_popup(self, btn_locator, cancel_btn_locator):
        self.driver.tools.wait_and_click(btn_locator)
        self.driver.wait.wait_for_element_to_be_present(cancel_btn_locator)
        self.driver.tools.wait_and_click(cancel_btn_locator)
        self.driver.wait.wait_for_element_to_be_present(btn_locator)

    def check_if_got_string_written(self, locator):
        self.driver.wait.wait_for_element_to_be_present(locator)
        text = self.driver.tools.get_text_from_element(locator)
        assert type(text) == str and text != ""

    def check_if_got_number_written_and_bigger_from_zero(self, locator):
        self.driver.wait.wait_for_element_to_be_present(locator)
        string_number = self.driver.tools.wait_and_get_attribute_when_present(locator, "value")
        number = float(string_number)
        assert number > 0, "Number not greater than 0"

    def return_if_checkbox_checked_or_not(self, checkbox_selector):
        check = self.driver.tools.wait_for_element_and_get_attribute(checkbox_selector, 'checked')
        return check

    def click_checkbox_and_verify(self, input_selector, checkbox_label_locator):
        self.driver.wait.wait_for_element_to_be_present(input_selector)
        first_checked = self.return_if_checkbox_checked_or_not(input_selector)
        self.driver.tools.click_on_web_element_by_js(self.driver.wait.wait_for_element_to_be_present(
            checkbox_label_locator))
        second_checked = self.return_if_checkbox_checked_or_not(input_selector)
        self.driver.tools.click_on_web_element_by_js(self.driver.wait.wait_for_element_to_be_present(
            checkbox_label_locator))
        third_checked = self.return_if_checkbox_checked_or_not(input_selector)
        assert first_checked != second_checked and first_checked == third_checked

    def click_mark_all_and_clean_btn_and_verify(self, mark_btn, clean_btn, checkbox_locator):
        counter = 0
        while counter < 2:
            check = self.driver.tools.wait_for_element_and_get_attribute(checkbox_locator, 'checked')
            if check:
                self.driver.tools.wait_and_click(clean_btn)
                no_checked = self.driver.tools.wait_for_element_and_get_attribute(checkbox_locator, 'checked')
                assert check != no_checked, "not matching"
                counter += 1
            else:
                self.driver.tools.wait_and_click(mark_btn)
                checked = self.driver.tools.wait_for_element_and_get_attribute(checkbox_locator, 'checked')
                assert check != checked, "not matching"
                counter += 1

    def choose_supplier_and_enter_commission(self):
        self.click_on_input_and_choose_option(ProfileAmlot.select_provider_list, "תשלום חשבון חשמל")
        self.insert_to_input_and_verify(ProfileAmlot.commission_input, 1)
        self.driver.tools.wait_and_click(ProfileAmlot.apply_commission_on_products_btn)
        self.driver.wait.wait_for_element_to_be_present(ProfileAmlot.confirm)
        self.driver.tools.wait_and_click(ProfileAmlot.confirm_approve_btn)
        self.insert_to_input_and_verify(ProfileAmlot.final_commission_input, 1)
        self.driver.tools.wait_and_click(ProfileAmlot.apply_final_commission_on_products_btn)
        self.driver.wait.wait_for_element_to_be_present(ProfileAmlot.confirm)
        self.driver.tools.wait_and_click(ProfileAmlot.confirm_approve_btn)

    def verify_table_first_column(self):
        self.driver.wait.wait_for_element_contain_text(ProfileAmlot.table_product)
        first_column = self.driver.tools.get_text_from_element(ProfileAmlot.table_product)
        assert first_column == "תשלום חשבון חשמל באשראי" or first_column == "תשלום חשבון חשמל במזומן",\
            f"{first_column} not תשלום חשבון חשמל במזומן or תשלום חשבון חשמל באשראי"

        self.check_if_got_number_written_and_bigger_from_zero(ProfileAmlot.table_commission)

        number = self.driver.tools.get_text_from_element(ProfileAmlot.table_commission_section_20)
        assert float(number.strip()) > 0, "Number not greater than 0"

        self.check_if_got_number_written_and_bigger_from_zero(ProfileAmlot.table_final_commission)
        self.click_checkbox_and_verify(ProfileAmlot.table_licensed_to_css_selector,
                                       ProfileAmlot.table_licensed_to_label)

    def test_login_and_get_into_profile_amlot_link(self):
        self.get_into_prfile_amlot()
        self.driver.wait.wait_for_element_to_be_present(ProfileAmlot.profile_select)
        self.remove_logs_dir = True

    def test_login_and_check_if_got_any_option_in_profile_select_and_click_it(self):
        self.get_into_prfile_amlot()
        self.click_on_input_and_choose_option(ProfileAmlot.profile_select,
                                              self.get_last_option_from_select(ProfileAmlot.profile_select))
        self.remove_logs_dir = True

    def test_click_add_new_profile_btn(self):
        self.get_into_prfile_amlot()
        self.click_add_new_profile_btn()
        self.remove_logs_dir = True

    def test_x_btn_on_add_new_profile_btn_popup(self):
        self.get_into_prfile_amlot()
        self.click_x_btn_in_btn_popup(ProfileAmlot.add_now_profile_btn, ProfileAmlot.popup_x_btn)
        self.remove_logs_dir = True

    def test_cancel_btn_on_add_new_profile_btn_popup(self):
        self.get_into_prfile_amlot()
        self.click_cancel_btn_in_btn_popup(ProfileAmlot.add_now_profile_btn, ProfileAmlot.popup_cancel_btn)
        self.remove_logs_dir = True

    def test_save_btn_on_add_new_profile_btn_popup_when_profile_is_empty(self):
        self.get_into_prfile_amlot()
        self.click_add_new_profile_btn()
        self.driver.tools.wait_and_click(ProfileAmlot.popup_save_btn)
        assert self.driver.wait.wait_for_element_to_be_present(ProfileAmlot.alert_warning), "no alert"
        self.remove_logs_dir = True

    def test_insert_name_to_profile_input_on_add_new_profile_btn_popup_and_verify(self):
        self.get_into_prfile_amlot()
        self.insert_new_profile(ProfileAmlot.profile_select, ProfileAmlot.add_now_profile_btn,
                                ProfileAmlot.popup_insert_profile_name)
        self.remove_logs_dir = True

    def test_insert_name_to_profile_input_on_add_new_profile_btn_popup_and_save(self):
        self.get_into_prfile_amlot()
        new_profile = self.insert_new_profile(
            ProfileAmlot.profile_select, ProfileAmlot.add_now_profile_btn, ProfileAmlot.popup_insert_profile_name)
        self.driver.tools.wait_and_click(ProfileAmlot.popup_save_btn)
        assert self.driver.wait.wait_for_element_to_be_present(ProfileAmlot.alert_success), "no save alert"
        all_profiles = self.get_list_of_options(ProfileAmlot.profile_select)
        assert new_profile in all_profiles, "new profile is didn't saved"
        self.remove_logs_dir = True

    def test_select_provider_form_list(self):
        self.get_into_prfile_amlot()
        self.click_on_input_and_choose_option(ProfileAmlot.select_provider_list, "תשלום חשבון חשמל")
        self.remove_logs_dir = True

    def test_insert_to_start_commission_input(self):
        self.get_into_prfile_amlot()
        self.insert_to_input_and_verify(ProfileAmlot.commission_input, 1)
        self.remove_logs_dir = True

    def test_start_commission_btn_with_empty_input(self):
        self.get_into_prfile_amlot()
        self.driver.tools.wait_and_click(ProfileAmlot.apply_commission_on_products_btn)
        assert self.driver.wait.wait_for_element_to_be_present(ProfileAmlot.alert_warning), "no alert came up"
        self.remove_logs_dir = True

    def test_start_commission_btn_with_field_input_without_to_select_profile(self):
        self.get_into_prfile_amlot()
        self.insert_to_input_and_verify(ProfileAmlot.commission_input, 1)
        self.driver.tools.wait_and_click(ProfileAmlot.apply_commission_on_products_btn)
        assert self.driver.wait.wait_for_element_to_be_present(ProfileAmlot.alert_warning), "no alert show up"
        self.remove_logs_dir = True

    def test_start_commission_btn_with_field_input_with_select_profile_and_cancel(self):
        self.get_into_prfile_amlot()
        self.click_on_input_and_choose_option(ProfileAmlot.profile_select,
                                              self.get_last_option_from_select(ProfileAmlot.profile_select))
        self.insert_to_input_and_verify(ProfileAmlot.commission_input, 1)
        self.driver.tools.wait_and_click(ProfileAmlot.apply_commission_on_products_btn)
        self.driver.wait.wait_for_element_to_be_present(ProfileAmlot.confirm)
        self.driver.tools.wait_and_click(ProfileAmlot.confirm_cancel_btn)
        self.driver.wait.wait_for_element_to_be_present(ProfileAmlot.apply_commission_on_products_btn)
        self.remove_logs_dir = True

    def test_start_commission_btn_with_field_input_with_select_profile_and_approve(self):
        self.get_into_prfile_amlot()
        self.click_on_input_and_choose_option(ProfileAmlot.profile_select,
                                              self.get_last_option_from_select(ProfileAmlot.profile_select))
        self.insert_to_input_and_verify(ProfileAmlot.commission_input, 1)
        self.driver.tools.wait_and_click(ProfileAmlot.apply_commission_on_products_btn)
        self.driver.wait.wait_for_element_to_be_present(ProfileAmlot.confirm)
        self.driver.tools.wait_and_click(ProfileAmlot.confirm_approve_btn)
        assert self.driver.wait.wait_for_element_to_be_present(ProfileAmlot.alert_success), "no alert show up"
        self.remove_logs_dir = True

    def test_insert_to_final_commission_input(self):
        self.get_into_prfile_amlot()
        self.insert_to_input_and_verify(ProfileAmlot.final_commission_input, 1)
        self.remove_logs_dir = True

    def test_final_commission_btn_with_empty_input(self):
        self.get_into_prfile_amlot()
        self.driver.tools.wait_and_click(ProfileAmlot.apply_final_commission_on_products_btn)
        assert self.driver.wait.wait_for_element_to_be_present(ProfileAmlot.alert_warning), "no alert came up"
        self.remove_logs_dir = True

    def test_final_commission_btn_with_field_input_without_to_select_profile(self):
        self.get_into_prfile_amlot()
        self.insert_to_input_and_verify(ProfileAmlot.final_commission_input, 1)
        self.driver.tools.wait_and_click(ProfileAmlot.apply_final_commission_on_products_btn)
        assert self.driver.wait.wait_for_element_to_be_present(ProfileAmlot.alert_warning), "no alert show up"
        self.remove_logs_dir = True

    def test_final_commission_btn_with_field_input_with_select_profile_and_cancel(self):
        self.get_into_prfile_amlot()
        self.click_on_input_and_choose_option(ProfileAmlot.profile_select,
                                              self.get_last_option_from_select(ProfileAmlot.profile_select))
        self.insert_to_input_and_verify(ProfileAmlot.final_commission_input, 1)
        self.driver.tools.wait_and_click(ProfileAmlot.apply_final_commission_on_products_btn)
        self.driver.wait.wait_for_element_to_be_present(ProfileAmlot.confirm)
        self.driver.tools.wait_and_click(ProfileAmlot.confirm_cancel_btn)
        self.driver.wait.wait_for_element_to_be_present(ProfileAmlot.apply_final_commission_on_products_btn)
        self.remove_logs_dir = True

    def test_final_commission_btn_with_field_input_with_select_profile_and_approve(self):
        self.get_into_prfile_amlot()
        self.click_on_input_and_choose_option(ProfileAmlot.profile_select,
                                              self.get_last_option_from_select(ProfileAmlot.profile_select))
        self.insert_to_input_and_verify(ProfileAmlot.final_commission_input, 1)
        self.driver.tools.wait_and_click(ProfileAmlot.apply_final_commission_on_products_btn)
        self.driver.wait.wait_for_element_to_be_present(ProfileAmlot.confirm)
        self.driver.tools.wait_and_click(ProfileAmlot.confirm_approve_btn)
        assert self.driver.wait.wait_for_element_to_be_present(ProfileAmlot.alert_success), "no alert show up"
        self.driver.wait.wait_for_element_to_be_present(ProfileAmlot.apply_final_commission_on_products_btn)
        self.remove_logs_dir = True

    def test_checkbox_save_for_default(self):
        self.get_into_prfile_amlot()
        self.click_checkbox_and_verify(ProfileAmlot.checkbox_save_for_default_id,
                                       ProfileAmlot.checkbox_save_for_default_label)
        self.remove_logs_dir = True

    def test_select_approved_for(self):
        self.get_into_prfile_amlot()
        self.click_on_input_and_choose_option(ProfileAmlot.approve_for_input_select, "מנהל")
        self.remove_logs_dir = True

    def test_select_approved_for_click_x_btn(self):
        self.get_into_prfile_amlot()
        self.click_on_input_and_choose_option(ProfileAmlot.approve_for_input_select, "מנהל")
        self.driver.tools.wait_and_click(ProfileAmlot.approve_for_input_select_x_btn)
        self.remove_logs_dir = True

    def test_table_profile_amlot(self):
        self.get_into_prfile_amlot()
        self.click_on_input_and_choose_option(ProfileAmlot.profile_select,
                                              self.get_last_option_from_select(ProfileAmlot.profile_select))
        self.choose_supplier_and_enter_commission()
        self.verify_table_first_column()
        self.remove_logs_dir = True

    def test_mark_all_btn_and_clean_btn_in_table_and_verify(self):
        self.get_into_prfile_amlot()
        self.click_on_input_and_choose_option(ProfileAmlot.profile_select,
                                              self.get_last_option_from_select(ProfileAmlot.profile_select))
        self.click_on_input_and_choose_option(ProfileAmlot.select_provider_list, "תשלום חשבון חשמל")
        self.insert_to_input_and_verify(ProfileAmlot.commission_input, 1)
        self.driver.tools.wait_and_click(ProfileAmlot.apply_commission_on_products_btn)
        self.driver.wait.wait_for_element_to_be_present(ProfileAmlot.confirm)
        self.driver.tools.wait_and_click(ProfileAmlot.confirm_approve_btn)
        self.insert_to_input_and_verify(ProfileAmlot.final_commission_input, 1)
        self.driver.tools.wait_and_click(ProfileAmlot.apply_final_commission_on_products_btn)
        self.driver.wait.wait_for_element_to_be_present(ProfileAmlot.confirm)
        self.driver.tools.wait_and_click(ProfileAmlot.confirm_approve_btn)
        self.driver.wait.wait_for_element_contain_text(ProfileAmlot.table_product)
        self.click_mark_all_and_clean_btn_and_verify(ProfileAmlot.mark_all_btn, ProfileAmlot.clean_btn,
                                                     ProfileAmlot.table_licensed_to_css_selector)
        self.remove_logs_dir = True

    def test_save_btn_of_all_page(self):
        self.get_into_prfile_amlot()
        assert self.driver.wait.wait_for_element_to_be_invisible(ProfileAmlot.save_btn), "save btn on page"
        self.click_on_input_and_choose_option(ProfileAmlot.profile_select,
                                              self.get_last_option_from_select(ProfileAmlot.profile_select))
        assert self.driver.wait.wait_for_element_to_be_visible(ProfileAmlot.save_btn), "save btn not on page"
        self.driver.tools.wait_and_click(ProfileAmlot.save_btn)
        assert self.driver.wait.wait_for_element_to_be_present(ProfileAmlot.alert_success), "no alert success show up"
        self.remove_logs_dir = True
