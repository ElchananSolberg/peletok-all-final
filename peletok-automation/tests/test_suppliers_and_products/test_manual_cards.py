from tests.test_suppliers_and_products.base_suppliers_and_products_test_class import *
from datetime import datetime, date


class TestManualCards(BaseSuppliersAndProductsTestClass):


    def click_on_checkbox_and_check_if_checked(self, checkbox_name, attribute, is_checked=None):
        self.driver.tools.wait_and_click((ManualCardsPage.checkbox_click[0], ManualCardsPage.checkbox_click[1].format(checkbox_name)))
        self.check_if_checkbox_is_checked(checkbox_name=checkbox_name, attribute=attribute, is_checked=is_checked)

    def check_if_checkbox_is_checked(self, checkbox_name, attribute, is_checked=None):
        assert is_checked == self.driver.tools.wait_for_element_and_get_attribute((ManualCardsPage.checkbox_check[0],
                                                                                   ManualCardsPage.checkbox_check[1].format(checkbox_name)),
                                                                                  attribute, timeout=2), f'{checkbox_name} is not {is_checked}'

    def login_and_get_in_my_tab_and_choose_options(self):
        self.login_and_get_in_suppliers_and_products()
        self.click_on_a_tab('manualCards')
        self.click_on_input_and_choose_option(ManualCardsPage.selector_suppliers, 'סלקום  QA')
        self.click_on_input_and_choose_option(ManualCardsPage.selector_items, 'בדיקה')

    def amount_of_rows_in_table(self):
        self.driver.tools.wait_and_click(ManualCardsPage.search_btn)
        if self.driver.wait.wait_for_element_to_be_present(ManualCardsPage.no_cards_report, timeout=8,
                                                           raise_exception=False):
            return 0
        else:
            return len(self.driver.wait.wait_for_elements_to_be_present(ManualCardsPage.card_row))

    def check_if_any_card_in_db(self):
        self.driver.tools.wait_and_click(ManualCardsPage.search_btn)
        if self.driver.wait.wait_for_element_to_be_present(ManualCardsPage.no_cards_report, timeout=8,
                                                           raise_exception=False):
            return False
        else:
            return True

    def check_parameter_in_db(self, column_in_db: int, against_parameter: str, flag=True):
        for i in range(len(self.driver.wait.wait_for_elements_to_be_present(ManualCardsPage.card_row))):
            card_parameter = self.driver.tools.get_text_from_element((ManualCardsPage.cards_in_db[0],
                                                                      ManualCardsPage.cards_in_db[1].format(i + 7,
                                                                                                            column_in_db)))
            if card_parameter == against_parameter:
                flag = not flag
                break
        assert flag, 'Unexpectedly item returned from db'

    def add_new_manual_card(self, card_number, card_code, expected_or_different_date_func, months=0, days=0):
        self.driver.tools.wait_and_click(ManualCardsPage.new_manual_card_btn)
        self.driver.tools.set_text(ManualCardsPage.card_number_form_manual_cards, card_number)
        self.driver.tools.set_text(ManualCardsPage.card_code_form_manual_cards, card_code)
        expected_or_different_date_func(ManualCardsPage.expiration_date, months=months, days=days)
        assert 'לא' == self.driver.tools.wait_for_element_and_get_attribute(ManualCardsPage.card_in_use_input, 'value')
        self.driver.tools.wait_and_click(ManualCardsPage.save_btn)
        self.driver.wait.wait_for_element_to_be_present(ManualCardsPage.alert_successful)

    def test_manual_cards_login_and_get_in_manual_cards(self):
        self.login_and_get_in_suppliers_and_products()
        self.click_on_a_tab('manualCards')
        self.remove_logs_dir = True

    def test_manual_cards_login_and_check_active_cards_checkbox(self):
        self.login_and_get_in_my_tab_and_choose_options()
        self.click_on_checkbox_and_check_if_checked('ActiveCards', 'checked', is_checked='true')
        self.click_on_checkbox_and_check_if_checked('ActiveCards', 'checked')
        self.remove_logs_dir = True

    def test_manual_cards_login_and_check_tickets_used_checkbox(self):
        self.login_and_get_in_my_tab_and_choose_options()
        self.click_on_checkbox_and_check_if_checked('TicketsUsed', 'checked', is_checked='true')
        self.click_on_checkbox_and_check_if_checked('TicketsUsed', 'checked')
        self.remove_logs_dir = True

    def test_manual_cards_login_and_check_cards_that_are_going_to_expire_checkbox(self):
        self.login_and_get_in_my_tab_and_choose_options()
        self.click_on_checkbox_and_check_if_checked('cardsThatAreGoingToExpire', 'checked', is_checked='true')
        self.click_on_checkbox_and_check_if_checked('cardsThatAreGoingToExpire', 'checked')
        self.remove_logs_dir = True

    def test_manual_cards_login_and_check_expired_and_unused_cards_checkbox(self):
        self.login_and_get_in_my_tab_and_choose_options()
        self.click_on_checkbox_and_check_if_checked('ActiveCards', 'checked', is_checked='true')
        self.click_on_checkbox_and_check_if_checked('TicketsUsed', 'checked', is_checked='true')
        self.click_on_checkbox_and_check_if_checked('cardsThatAreGoingToExpire', 'checked', is_checked='true')
        self.click_on_checkbox_and_check_if_checked('ExpiredAndUnusedCards', 'checked', is_checked='true')
        self.check_if_checkbox_is_checked('ActiveCards', 'checked')
        self.check_if_checkbox_is_checked('TicketsUsed', 'checked')
        self.check_if_checkbox_is_checked('cardsThatAreGoingToExpire', 'checked')
        self.click_on_checkbox_and_check_if_checked('ExpiredAndUnusedCards', 'checked')
        self.remove_logs_dir = True

    def test_manual_cards_login_and_check_dates(self):
        self.login_and_get_in_my_tab_and_choose_options()
        self.date_expected_from_input(ManualCardsPage.income_date)
        self.date_expected_from_input(ManualCardsPage.end_date, months=0, days=1)
        self.remove_logs_dir = True

    def test_manual_cards_login_and_check_add_and_cancel_btn(self):
        self.login_and_get_in_my_tab_and_choose_options()
        self.driver.tools.wait_and_click(ManualCardsPage.new_manual_card_btn)
        self.driver.wait.wait_for_element_to_be_visible(ManualCardsPage.card_number_form_manual_cards)
        self.driver.tools.wait_and_click(ManualCardsPage.new_manual_card_btn)
        self.driver.wait.wait_for_element_to_be_invisible(ManualCardsPage.card_number_form_manual_cards)
        self.remove_logs_dir = True

    def test_manual_cards_login_and_add_new_manual_card(self):
        self.login_and_get_in_my_tab_and_choose_options()
        self.insert_different_date_to_input_and_check(ManualCardsPage.income_date, months=0, days=0)
        next_card_num = self.amount_of_rows_in_table()
        self.add_new_manual_card(next_card_num, next_card_num + 1, self.date_expected_from_input, months=12)
        self.check_parameter_in_db(column_in_db=1, against_parameter=str(next_card_num), flag=False)
        self.remove_logs_dir = True

    def test_manual_cards_login_and_search_if_only_used_cards_in_db(self):
        self.login_and_get_in_my_tab_and_choose_options()
        self.click_on_checkbox_and_check_if_checked('TicketsUsed', 'checked', is_checked='true')
        self.insert_different_date_to_input_and_check(ManualCardsPage.income_date)
        if self.check_if_any_card_in_db():
            self.check_parameter_in_db(column_in_db=2, against_parameter='לא')
        self.remove_logs_dir = True

    def test_manual_cards_login_and_search_if_only_expired_cards_in_db(self):
        self.login_and_get_in_my_tab_and_choose_options()
        self.add_new_manual_card(1, 2, self.insert_different_date_to_input_and_check, months=0)
        self.click_on_checkbox_and_check_if_checked('cardsThatAreGoingToExpire', 'checked', is_checked='true')
        self.driver.tools.wait_and_click(ManualCardsPage.search_btn)
        self.check_parameter_in_db(column_in_db=4,
                                   against_parameter=self.driver.tools.wait_for_element_and_get_attribute(ManualCardsPage.income_date, "value"),
                                   flag=False)
        self.remove_logs_dir = True

    def test_manual_cards_login_and_search_if_expired_and_unused_cards_in_db(self):
        self.login_and_get_in_my_tab_and_choose_options()
        self.click_on_checkbox_and_check_if_checked('ExpiredAndUnusedCards', 'checked', is_checked='true')
        current_date = date.today().strftime('%Y-%m-%d')
        self.driver.tools.set_text(ManualCardsPage.income_date, '03012020')
        self.driver.tools.wait_and_click(ManualCardsPage.search_btn)
        for i in range(len(self.driver.wait.wait_for_elements_to_be_present(ManualCardsPage.card_row))):
            is_card_in_use = self.driver.tools.get_text_from_element((ManualCardsPage.cards_in_db[0],
                                                                      ManualCardsPage.cards_in_db[1].format(i + 7, 2)))
            card_date = self.driver.tools.get_text_from_element((ManualCardsPage.cards_in_db[0],
                                                                 ManualCardsPage.cards_in_db[1].format(i + 7, 4)))
            if is_card_in_use == 'כן' or str(datetime.strptime(card_date, '%Y-%m-%d')) > current_date:
                assert False, 'Unexpectedly item returned from db'
        self.remove_logs_dir = True
