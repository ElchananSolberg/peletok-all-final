from tests.base_project_test_class import *
import time


class TestLogin(BaseProjectTestClass):

    def test_login_success(self):
        time.sleep(60)
        self.login()
        self.driver.wait.wait_for_element_to_be_present(HomePage.cellcom_icon)
        self.driver.wait.wait_for_element_to_be_present(VerificationPage.print_btn)
        self.remove_logs_dir = True

    def test_false_login_password(self):
        self.login_with_different_username_or_password(User.username, User.false_password)
        self.driver.wait.wait_for_element_to_be_present(VerificationPage.login_error_msg)
        self.driver.wait.wait_for_element_to_be_present(VerificationPage.print_btn)
        self.remove_logs_dir = True

    def test_false_login_username(self):
        self.login_with_different_username_or_password(User.false_username, User.password)
        self.driver.wait.wait_for_element_to_be_present(VerificationPage.login_error_msg)
        self.remove_logs_dir = True

    def test_false_login_username_and_password(self):
        self.login_with_different_username_or_password(User.false_username, User.false_password)
        self.driver.wait.wait_for_element_to_be_present(VerificationPage.login_error_msg)
        self.remove_logs_dir = True

    def test_log_out(self):
        self.login()
        self.driver.tools.wait_and_click(VerificationPage.user_area_drop)
        self.driver.tools.wait_and_click(VerificationPage.btn_log_out)
        self.driver.wait.wait_for_element_to_be_present(LoginPage.submit_btn)
        self.remove_logs_dir = True