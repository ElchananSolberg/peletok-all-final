from tests.test_reports.base_reports_test_class import *


class TestBalanceByDate(BaseReportsTestClass):

    def login_and_get_into_balance_by_date_page(self):
        self.login_and_get_in_report_manager_area("remainderByDate", BalanceByDatePage.show_reports_btn)

    def test_balance_by_date_page_name_of_business_select(self):
        self.login_and_get_into_balance_by_date_page()
        self.check_if_got_options_in_which_select(GeneralElementsInReportsAreaManager.business_name_input)
        self.remove_logs_dir = True

    def test_balance_by_date_page_reseller_select(self):
        self.login_and_get_into_balance_by_date_page()
        self.check_if_got_options_in_which_select(GeneralElementsInReportsAreaManager.reseller_select)
        self.remove_logs_dir = True

    def test_balance_by_date_choose_payment_select(self):
        self.login_and_get_into_balance_by_date_page()
        self.check_if_got_options_in_which_select(GeneralElementsInReportsAreaManager.choose_payment_select)
        self.remove_logs_dir = True

    def test_balance_by_date_exists_customer_number_input(self):
        self.login_and_get_into_balance_by_date_page()
        self.driver.tools.wait.wait_for_element_to_be_visible(GeneralElementsInReportsAreaManager.customer_number_input)
        self.remove_logs_dir = True

    def test_balance_by_date_exists_reseller_number_input(self):
        self.login_and_get_into_balance_by_date_page()
        self.driver.tools.wait.wait_for_element_to_be_visible(
            GeneralElementsInReportsAreaManager.distributer_number_input)
        self.remove_logs_dir = True

    def test_balance_by_date_time_hour(self):
        self.login_and_get_into_balance_by_date_page()
        self.insert_hour_and_check(BalanceByDatePage.time_in_hours_input, hour=2)
        self.remove_logs_dir = True

    def test_balance_by_date_time_minute(self):
        self.login_and_get_into_balance_by_date_page()
        self.insert_minute_and_check(BalanceByDatePage.time_in_minuts_input, minute=2)
        self.remove_logs_dir = True

    def test_balance_by_date_page_show_reports_btn(self):
        self.login_and_get_into_balance_by_date_page()
        self.insert_different_date_to_input_and_check(BalanceByDatePage.date_input)
        self.click_on_show_reports_btn_in_reports_manager_area_and_check(BalanceByDatePage.show_reports_btn, GeneralElementsInReportsAreaManager.no_reports_alert)
        self.remove_logs_dir = True

    def test_balance_by_date_export_report_to_excel(self):
        self.login_and_get_into_balance_by_date_page()
        self.export_report_to_excel_button(BalanceByDatePage.show_reports_btn)
        self.remove_logs_dir = True
