from tests.test_reports.base_reports_test_class import *


class TestActions(BaseReportsTestClass):

    def login_and_get_into_actions_page(self):
        self.login_and_get_in_report_manager_area("13", GeneralElementsInReportsAreaManager.view_report_button)

    def test_actions_name_of_business_select(self):
        self.login_and_get_into_actions_page()
        self.check_if_got_options_in_which_select(GeneralElementsInReportsAreaManager.business_name_input)
        self.remove_logs_dir = True

    def test_actions_reseller_select(self):
        self.login_and_get_into_actions_page()
        self.check_if_got_options_in_which_select(GeneralElementsInReportsAreaManager.reseller_select)
        self.remove_logs_dir = True

    def test_actions_search_agent_by_name_select(self):
        self.login_and_get_into_actions_page()
        self.check_if_got_options_in_which_select(GeneralElementsInReportsAreaManager.find_agent_by_name_select)
        self.remove_logs_dir = True

    def test_actions_status_business_select(self):
        self.login_and_get_into_actions_page()
        self.check_if_got_options_in_which_select(GeneralElementsInReportsAreaManager.business_status_select)
        self.remove_logs_dir = True

    def test_actions_exists_phone_number_input(self):
        self.login_and_get_into_actions_page()
        self.driver.tools.wait.wait_for_element_to_be_visible(GeneralElementsInReportsAreaManager.phone_number_input)
        self.phone_number_input()
        self.remove_logs_dir = True

    def test_actions_exists_customer_number_input(self):
        self.login_and_get_into_actions_page()
        self.driver.tools.wait.wait_for_element_to_be_visible(GeneralElementsInReportsAreaManager.customer_number_input)
        self.remove_logs_dir = True

    def test_actions_exists_reseller_number_input(self):
        self.login_and_get_into_actions_page()
        self.driver.tools.wait.wait_for_element_to_be_visible(
            GeneralElementsInReportsAreaManager.distributer_number_input)
        self.remove_logs_dir = True

    def test_actions_exists_agent_number_input(self):
        self.login_and_get_into_actions_page()
        self.driver.tools.wait.wait_for_element_to_be_visible(GeneralElementsInReportsAreaManager.agent_number_input)
        self.remove_logs_dir = True

    def test_actions_view_by_marketers_radio_button(self):
        self.login_and_get_into_actions_page()
        self.view_by_marketers_radio_button_reports_manager_general()
        self.remove_logs_dir = True

    def test_actions_tags_select(self):
        self.login_and_get_into_actions_page()
        self.check_if_got_options_in_which_select(GeneralElementsInReportsAreaManager.tags_select)
        self.remove_logs_dir = True

    def test_actions_export_report_to_excel(self):
        self.login_and_get_into_actions_page()
        self.export_report_to_excel_button(GeneralElementsInReportsAreaManager.view_report_button)
        self.remove_logs_dir = True

    def test_actions_input_start_date(self):
        self.login_and_get_into_actions_page()
        self.check_if_current_date_in_input(GeneralElementsInReportsAreaManager.from_date)
        self.insert_different_date_to_input_and_check(GeneralElementsInReportsAreaManager.from_date)
        self.remove_logs_dir = True

    def test_actions_input_end_date(self):
        self.login_and_get_into_actions_page()
        self.check_if_current_date_in_input(GeneralElementsInReportsAreaManager.to_date)
        self.insert_different_date_to_input_and_check(GeneralElementsInReportsAreaManager.to_date)
        self.remove_logs_dir = True

    def test_actions_start_hour(self):
        self.login_and_get_into_actions_page()
        self.insert_hour_and_check(GeneralElementsInReportsAreaManager.start_time_in_hours_select, hour=2)
        self.remove_logs_dir = True

    def test_actions_start_minute(self):
        self.login_and_get_into_actions_page()
        self.insert_minute_and_check(GeneralElementsInReportsAreaManager.start_time_in_minuts_select, minute=2)
        self.remove_logs_dir = True

    def test_actions_end_hour(self):
        self.login_and_get_into_actions_page()
        self.insert_hour_and_check(GeneralElementsInReportsAreaManager.end_time_in_hours, hour=2)
        self.remove_logs_dir = True

    def test_actions_end_minute(self):
        self.login_and_get_into_actions_page()
        self.insert_minute_and_check(GeneralElementsInReportsAreaManager.end_time_in_minuts, minute=2)
        self.remove_logs_dir = True

    def test_action_page_show_reports_btn(self):
        self.login_and_get_into_actions_page()
        self.insert_different_date_to_input_and_check(GeneralElementsInReportsAreaManager.from_date)
        self.check_if_current_date_in_input(GeneralElementsInReportsAreaManager.to_date)
        self.click_on_show_reports_btn_in_reports_manager_area_and_check(
            GeneralElementsInReportsAreaManager.view_report_button, GeneralElementsInReportsAreaManager.no_reports_alert)
        self.remove_logs_dir = True
