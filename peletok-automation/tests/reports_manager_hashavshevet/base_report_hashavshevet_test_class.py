from tests.test_reports.base_reports_test_class import *


class BaseReportsHashavshevetTestClass(BaseReportsTestClass):

    def click_on_checkbox_input_and_check_if_checked(self, checkbox_name, attribute, is_checked=None):
        self.driver.tools.wait_and_click(GeneralElementsInReportsAreaManagerHashavshevet.article_20_checkbox_click)
        self.check_if_checkbox_input_is_checked(checkbox_name=checkbox_name, attribute=attribute, is_checked=is_checked)

    def check_if_checkbox_input_is_checked(self, checkbox_name, attribute, is_checked=None):
        assert is_checked == self.driver.tools.wait_for_element_and_get_attribute(
            GeneralElementsInReportsAreaManagerHashavshevet.article_20_checkbox_check, attribute,
            timeout=50), f'{checkbox_name} is not {is_checked}'
