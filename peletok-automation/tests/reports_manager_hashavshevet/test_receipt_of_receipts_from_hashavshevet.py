from tests.test_reports.base_reports_test_class import *


class TestReceiptOfReceiptsHashavshevet(BaseReportsTestClass):

    def login_and_get_into_Receipt_of_receipts_from_hashavshevet_page(self):
        self.login_and_get_in_report_manager_area_hashavshevet("importReceiptsFromHashavshevet",
                                                               GeneralElementsInReportsAreaManagerHashavshevet.view_report_button)

    def click_on_checkbox_and_check_if_checked(self, checkbox_name, attribute, is_checked=None):
        self.driver.tools.wait_and_click(
            (GeneralElementsInReportsAreaManagerHashavshevet.checkbox_click[0],
             GeneralElementsInReportsAreaManagerHashavshevet.checkbox_click[1].format(checkbox_name)))
        self.check_if_checkbox_is_checked(checkbox_name=checkbox_name, attribute=attribute, is_checked=is_checked)

    def check_if_checkbox_is_checked(self, checkbox_name, attribute, is_checked=None):
        assert is_checked == self.driver.tools.wait_for_element_and_get_attribute(
            (GeneralElementsInReportsAreaManagerHashavshevet.checkbox_check[0],
             GeneralElementsInReportsAreaManagerHashavshevet.checkbox_check[
                 1].format(checkbox_name)),
            attribute,
            timeout=2), f'{checkbox_name} is not {is_checked}'

    def test_receipt_of_receipts_hashavshevet_page_login_and_check_credit_checkbox(self):
        self.login_and_get_into_Receipt_of_receipts_from_hashavshevet_page()
        self.click_on_checkbox_and_check_if_checked('ImportReceiptsFromHashavshevet_Credit', 'checked',
                                                    is_checked='true')
        self.click_on_checkbox_and_check_if_checked('ImportReceiptsFromHashavshevet_Credit', 'checked')
        self.remove_logs_dir = True

    def test_receipt_of_receipts_hashavshevet_page_login_and_check_delek_user_checkbox(self):
        self.login_and_get_into_Receipt_of_receipts_from_hashavshevet_page()
        self.click_on_checkbox_and_check_if_checked('ImportReceiptsFromHashavshevet_DelekUser', 'checked',
                                                    is_checked='true')
        self.click_on_checkbox_and_check_if_checked('ImportReceiptsFromHashavshevet_DelekUser', 'checked')
        self.remove_logs_dir = True

    def test_receipt_of_receipts_hashavshevet_page_show_reports_btn(self):
        self.login_and_get_into_Receipt_of_receipts_from_hashavshevet_page()
        self.driver.wait.wait_for_element_to_be_present(
            GeneralElementsInReportsAreaManagerHashavshevet.view_report_button)
        self.remove_logs_dir = True
