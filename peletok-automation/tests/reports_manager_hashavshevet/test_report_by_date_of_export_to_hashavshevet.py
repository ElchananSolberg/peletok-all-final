from tests.test_reports.base_reports_test_class import *


class TestReportByDateOfExportToHashavshevet(BaseReportsTestClass):

    def login_and_get_into_report_by_date_of_export_to_hashavshevet_page(self):
        self.login_and_get_in_report_manager_area_hashavshevet("export/orderByDate",
                                                               GeneralElementsInReportsAreaManagerHashavshevet.view_report_button)

    def test_report_by_date_of_export_to_hashavshevet_page_input_start_date(self):
        self.login_and_get_into_report_by_date_of_export_to_hashavshevet_page()
        self.check_if_current_date_in_input(GeneralElementsInReportsAreaManagerHashavshevet.from_date)
        self.insert_different_date_to_input_and_check(GeneralElementsInReportsAreaManagerHashavshevet.from_date)
        self.remove_logs_dir = True

    def test_report_by_date_of_export_to_hashavshevet_page_input_end_date(self):
        self.login_and_get_into_report_by_date_of_export_to_hashavshevet_page()
        self.check_if_current_date_in_input(GeneralElementsInReportsAreaManagerHashavshevet.to_date)
        self.insert_different_date_to_input_and_check(GeneralElementsInReportsAreaManagerHashavshevet.to_date)
        self.remove_logs_dir = True

    def test_report_by_date_of_export_to_hashavshevet_page_start_hour(self):
        self.login_and_get_into_report_by_date_of_export_to_hashavshevet_page()
        self.insert_hour_and_check(GeneralElementsInReportsAreaManagerHashavshevet.start_time_in_hours_select, hour=2)
        self.remove_logs_dir = True

    def test_report_by_date_of_export_to_hashavshevet_page_start_minute(self):
        self.login_and_get_into_report_by_date_of_export_to_hashavshevet_page()
        self.insert_minute_and_check(GeneralElementsInReportsAreaManagerHashavshevet.start_time_in_minuts_select,
                                     minute=2)
        self.remove_logs_dir = True

    def test_report_by_date_of_export_to_hashavshevet_page_end_hour(self):
        self.login_and_get_into_report_by_date_of_export_to_hashavshevet_page()
        self.insert_hour_and_check(GeneralElementsInReportsAreaManagerHashavshevet.end_time_in_hours, hour=2)
        self.remove_logs_dir = True

    def test_report_by_date_of_export_to_hashavshevet_page_end_minute(self):
        self.login_and_get_into_report_by_date_of_export_to_hashavshevet_page()
        self.insert_minute_and_check(GeneralElementsInReportsAreaManagerHashavshevet.end_time_in_minuts, minute=2)
        self.remove_logs_dir = True

    def test_report_by_date_of_export_to_hashavshevet_page_show_reports_btn(self):
        self.login_and_get_into_report_by_date_of_export_to_hashavshevet_page()
        self.insert_different_date_to_input_and_check(GeneralElementsInReportsAreaManagerHashavshevet.from_date)
        self.check_if_current_date_in_input(GeneralElementsInReportsAreaManagerHashavshevet.to_date)
        self.click_on_show_reports_btn_in_reports_manager_area_and_check(
            GeneralElementsInReportsAreaManagerHashavshevet.view_report_button, GeneralElementsInReportsAreaManagerHashavshevet.no_results_found_text)
        self.remove_logs_dir = True

    def test_report_by_date_of_export_to_hashavshevet_page_export_report_to_excel(self):
        self.login_and_get_into_report_by_date_of_export_to_hashavshevet_page()
        self.export_report_to_excel_button(GeneralElementsInReportsAreaManagerHashavshevet.view_report_button)
        self.remove_logs_dir = True
