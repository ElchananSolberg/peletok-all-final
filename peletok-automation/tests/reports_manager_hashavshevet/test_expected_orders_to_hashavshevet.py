from tests.reports_manager_hashavshevet.base_report_hashavshevet_test_class import *


class TestExpectedOrdersToHashavshevet(BaseReportsHashavshevetTestClass):

    def login_and_get_into_expected_orders_to_hashavshevet_page(self):
        self.login_and_get_in_report_manager_area_hashavshevet("export/report",
                                                               GeneralElementsInReportsAreaManagerHashavshevet.view_report_button)

    def test_expected_orders_to_hashavshevet_page_input_start_date(self):
        self.login_and_get_into_expected_orders_to_hashavshevet_page()
        self.check_if_current_date_in_input(GeneralElementsInReportsAreaManagerHashavshevet.from_date)
        self.insert_different_date_to_input_and_check(GeneralElementsInReportsAreaManagerHashavshevet.from_date)
        self.remove_logs_dir = True

    def test_expected_orders_to_hashavshevet_page_input_end_date(self):
        self.login_and_get_into_expected_orders_to_hashavshevet_page()
        self.check_if_current_date_in_input(GeneralElementsInReportsAreaManagerHashavshevet.to_date)
        self.insert_different_date_to_input_and_check(GeneralElementsInReportsAreaManagerHashavshevet.to_date)
        self.remove_logs_dir = True

    def test_expected_orders_to_hashavshevet_page_start_hour(self):
        self.login_and_get_into_expected_orders_to_hashavshevet_page()
        self.insert_hour_and_check(GeneralElementsInReportsAreaManagerHashavshevet.start_time_in_hours_select, hour=2)
        self.remove_logs_dir = True

    def test_expected_orders_to_hashavshevet_page_start_minute(self):
        self.login_and_get_into_expected_orders_to_hashavshevet_page()
        self.insert_minute_and_check(GeneralElementsInReportsAreaManagerHashavshevet.start_time_in_minuts_select,
                                     minute=2)
        self.remove_logs_dir = True

    def test_expected_orders_to_hashavshevet_page_end_hour(self):
        self.login_and_get_into_expected_orders_to_hashavshevet_page()
        self.insert_hour_and_check(GeneralElementsInReportsAreaManagerHashavshevet.end_time_in_hours, hour=2)
        self.remove_logs_dir = True

    def test_expected_orders_to_hashavshevet_page_end_minute(self):
        self.login_and_get_into_expected_orders_to_hashavshevet_page()
        self.insert_minute_and_check(GeneralElementsInReportsAreaManagerHashavshevet.end_time_in_minuts, minute=2)
        self.remove_logs_dir = True

    def test_expected_orders_to_hashavshevet_page_show_reports_btn(self):
        self.login_and_get_into_expected_orders_to_hashavshevet_page()
        self.insert_different_date_to_input_and_check(GeneralElementsInReportsAreaManagerHashavshevet.from_date)
        self.check_if_current_date_in_input(GeneralElementsInReportsAreaManagerHashavshevet.to_date)
        self.click_on_show_reports_btn_in_reports_manager_area_and_check(
            GeneralElementsInReportsAreaManagerHashavshevet.view_report_button,
            GeneralElementsInReportsAreaManagerHashavshevet.no_results_found_text)
        self.remove_logs_dir = True

    def test_expected_orders_to_hashavshevet_page_export_report_to_excel(self):
        self.login_and_get_into_expected_orders_to_hashavshevet_page()
        self.export_report_to_excel_button(GeneralElementsInReportsAreaManagerHashavshevet.view_report_button)
        self.remove_logs_dir = True

    def test_expected_orders_to_hashavshevet_page_reseller_select(self):
        self.login_and_get_into_expected_orders_to_hashavshevet_page()
        self.check_if_got_options_in_which_select(GeneralElementsInReportsAreaManagerHashavshevet.reseller_select)
        self.remove_logs_dir = True

    def test_expected_orders_to_hashavshevet_page_agent_select(self):
        self.login_and_get_into_expected_orders_to_hashavshevet_page()
        self.check_if_got_options_in_which_select(GeneralElementsInReportsAreaManagerHashavshevet.agent_select)
        self.remove_logs_dir = True

    def test_expected_orders_to_hashavshevet_page_exists_terminal_number_input(self):
        self.login_and_get_into_expected_orders_to_hashavshevet_page()
        self.driver.tools.wait.wait_for_element_to_be_visible(
            GeneralElementsInReportsAreaManagerHashavshevet.terminal_number_select)
        self.remove_logs_dir = True

    def test_expected_orders_to_hashavshevet_page_article_20_check_box(self):
        self.login_and_get_into_expected_orders_to_hashavshevet_page()
        self.driver.wait.wait_for_element_to_be_visible(
            GeneralElementsInReportsAreaManagerHashavshevet.article_20_checkbox_click)
        self.click_on_checkbox_input_and_check_if_checked(
            GeneralElementsInReportsAreaManagerHashavshevet.article_20_checkbox_click,
            'checked', is_checked='true')
        self.click_on_checkbox_input_and_check_if_checked(
            GeneralElementsInReportsAreaManagerHashavshevet.article_20_checkbox_click,
            'checked')
        self.remove_logs_dir = True
