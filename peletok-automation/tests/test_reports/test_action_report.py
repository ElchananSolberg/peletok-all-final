from tests.test_reports.base_reports_test_class import *


class TestActionReport(BaseReportsTestClass):

    def test_action_report_login_and_get_in_action_report(self):
        self.login_and_get_in_report("0", 'פעילות')
        self.remove_logs_dir = True

    def test_action_report_supplier_list_input(self):
        self.login_and_get_in_report("0", 'פעילות')
        self.check_if_got_options_in_which_select(GeneralReportsPage.suppliers_input)
        self.remove_logs_dir = True

    def test_action_report_phone_number_input(self):
        self.login_and_get_in_report("0", 'פעילות')
        self.phone_number_input()
        self.remove_logs_dir = True

    def test_action_report_radio_btn(self):
        self.login_and_get_in_report("0", 'פעילות')
        self.check_radio_buttons(GeneralReportsPage.radio_button)
        self.remove_logs_dir = True

    def test_action_report_check_date_inputs(self):
        self.login_and_get_in_report("0", 'פעילות')
        self.check_if_current_date_in_input(GeneralReportsPage.start_date_input)
        self.check_if_current_date_in_input(GeneralReportsPage.end_date_input)
        self.insert_different_date_to_input_and_check(GeneralReportsPage.start_date_input)
        self.remove_logs_dir = True

    def test_action_report_hour_and_minute(self):
        self.login_and_get_in_report("0", 'פעילות')
        self.insert_hour_and_check(GeneralReportsPage.start_hours_select)
        self.insert_minute_and_check(GeneralReportsPage.start_minutes_select)
        self.insert_hour_and_check(GeneralReportsPage.end_hours_select)
        self.insert_minute_and_check(GeneralReportsPage.end_minutes_select)
        self.remove_logs_dir = True

    def test_action_report_show_report_btn(self):
        self.login_and_get_in_report("0", 'פעילות')
        self.click_on_show_reports_btn_and_check()
        self.remove_logs_dir = True

    def test_action_report_no_report_alert(self):
        self.login_and_get_in_report("0", 'פעילות')
        self.insert_different_date_to_input_and_check(GeneralReportsPage.start_date_input, months=2)
        self.click_on_show_reports_btn_and_check()
        self.driver.wait.wait_for_element_to_be_present(GeneralReportsPage.no_report_alert)
        self.remove_logs_dir = True

    def test_action_report_spans_on_top_of_table(self):
        self.login_and_get_in_report("0", 'פעילות')
        self.check_if_got_options_in_which_select(GeneralReportsPage.suppliers_input)
        self.click_on_show_reports_btn_and_check()
        self.check_dates_report_span()
        self.check_total_sales_span('דוחפעילות')
        self.remove_logs_dir = True

    def test_all_action_report(self):
        self.login_and_get_in_report("0", 'פעילות')
        self.check_if_got_options_in_which_select(GeneralReportsPage.suppliers_input)
        self.phone_number_input()
        self.check_radio_buttons(GeneralReportsPage.radio_button)
        self.insert_different_date_to_input_and_check(GeneralReportsPage.start_date_input)
        self.insert_hour_and_check(GeneralReportsPage.start_hours_select)
        self.insert_minute_and_check(GeneralReportsPage.start_minutes_select)
        self.insert_hour_and_check(GeneralReportsPage.end_hours_select)
        self.insert_minute_and_check(GeneralReportsPage.end_minutes_select)
        self.click_on_show_reports_btn_and_check()
        self.check_dates_report_span()
        self.check_total_sales_span('דוחפעילות')
        self.remove_logs_dir = True
