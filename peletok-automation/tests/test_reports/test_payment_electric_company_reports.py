from tests.test_reports.base_reports_test_class import *


class TestPaymentElectricCompanyReports(BaseReportsTestClass):

    def test_login_and_get_in_payment_electric_company_reports(self):
        self.login_and_get_in_report('9', 'דוח טעינה לחברת חשמל לישראל')
        self.remove_logs_dir = True

    def test_payment_electric_company_reports_date_inputs(self):
        self.login_and_get_in_report('9', 'דוח טעינה לחברת חשמל לישראל')
        self.check_if_current_date_in_input(GeneralReportsPage.start_date_input)
        self.check_if_current_date_in_input(GeneralReportsPage.end_date_input)
        self.insert_different_date_to_input_and_check(GeneralReportsPage.start_date_input)
        self.remove_logs_dir = True

    def test_payment_electric_company_reports_hour_start(self):
        self.login_and_get_in_report('9', 'דוח טעינה לחברת חשמל לישראל')
        self.insert_hour_and_check(GeneralReportsPage.start_hours_select)
        self.remove_logs_dir = True

    def test_payment_electric_company_reports_hour_end(self):
        self.login_and_get_in_report('9', 'דוח טעינה לחברת חשמל לישראל')
        self.insert_hour_and_check(GeneralReportsPage.end_hours_select)
        self.remove_logs_dir = True

    def test_payment_electric_company_reports_minute_start(self):
        self.login_and_get_in_report('9', 'דוח טעינה לחברת חשמל לישראל')
        self.insert_minute_and_check(GeneralReportsPage.start_minutes_select)
        self.remove_logs_dir = True

    def test_payment_electric_company_reports_minute_end(self):
        self.login_and_get_in_report('9', 'דוח טעינה לחברת חשמל לישראל')
        self.insert_minute_and_check(GeneralReportsPage.end_minutes_select)
        self.remove_logs_dir = True

    def test_payment_electric_company_reports_show_report_btn(self):
        self.login_and_get_in_report('9', 'דוח טעינה לחברת חשמל לישראל')
        self.insert_different_date_to_input_and_check(GeneralReportsPage.start_date_input)
        self.click_on_show_reports_btn_and_check()
        self.remove_logs_dir = True

    def test_payment_electric_company_reports_no_reports_alert(self):
        self.login_and_get_in_report('9', 'דוח טעינה לחברת חשמל לישראל')
        self.insert_different_date_to_input_and_check(GeneralReportsPage.start_date_input, days=3)
        self.click_on_show_reports_btn_and_check()
        self.remove_logs_dir = True
