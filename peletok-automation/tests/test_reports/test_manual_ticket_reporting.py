from tests.test_reports.base_reports_test_class import *


class TestManualTicketReporting(BaseReportsTestClass):

    def test_login_and_get_in_report_manual_tickets(self):
        self.login_and_get_in_report('3', 'דוח כרטיסים ידניים')
        self.remove_logs_dir = True

    def test_manual_tickets_suppliers_select(self):
        self.login_and_get_in_report('3', 'דוח כרטיסים ידניים')
        self.check_if_got_options_in_which_select(GeneralReportsPage.suppliers_input)
        self.remove_logs_dir = True

    def test_manual_tickets_date_inputs(self):
        self.login_and_get_in_report('3', 'דוח כרטיסים ידניים')
        self.check_if_current_date_in_input(GeneralReportsPage.start_date_input)
        self.check_if_current_date_in_input(GeneralReportsPage.end_date_input)
        self.insert_different_date_to_input_and_check(GeneralReportsPage.start_date_input)
        self.remove_logs_dir = True

    def test_manual_tickets_hours_and_minutes(self):
        self.login_and_get_in_report('3', 'דוח כרטיסים ידניים')
        self.check_hour_minute_in_input()
        self.insert_hour_and_check(GeneralReportsPage.start_hours_select)
        self.insert_hour_and_check(GeneralReportsPage.end_hours_select)
        self.insert_minute_and_check(GeneralReportsPage.start_minutes_select)
        self.insert_minute_and_check(GeneralReportsPage.end_minutes_select)
        self.remove_logs_dir = True

    def test_manual_tickets_show_report_btn(self):
        self.login_and_get_in_report('3', 'דוח כרטיסים ידניים')
        self.insert_different_date_to_input_and_check(GeneralReportsPage.start_date_input)
        self.click_on_show_reports_btn_and_check()
        self.remove_logs_dir = True

    def test_manual_tickets_no_reports_alert(self):
        self.login_and_get_in_report('3', 'דוח כרטיסים ידניים')
        self.insert_different_date_to_input_and_check(GeneralReportsPage.start_date_input, days=1)
        self.click_on_show_reports_btn_and_check()
        self.remove_logs_dir = True
