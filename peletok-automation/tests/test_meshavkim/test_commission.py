from tests.test_meshavkim.base_meshavkim_test_class import *
from tests.test_charges.test_kvish_6_page import *


class TestCommission(BaseMeshavkimTestClass):

    def login_and_get_in_commission_in_meshavkim(self):
        self.login_and_get_in_meshavkim()
        self.click_on_side_menu_options('עמלות')

    def login_and_click_kvish_6_icone(self):
        self.login()
        self.driver.tools.wait_and_click(HomePage.kvish_6)
        self.driver.wait.wait_for_element_to_be_present(VerificationPage.img_logo_kvish_6)

    def choose_marketer_and_distributor(self):
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.filter_behirat_mefit, 'peletok')
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.filter_behirat_meshavek, 'peletok')
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.btn_search)

    def choose_supplier_and_product(self):
        self.click_on_input_and_choose_option(CommissionPage.choose_supplier_input, 'כביש 6')
        self.click_on_input_and_choose_option(CommissionPage.choose_product_input, 'תשלום חשבון כביש 6')

    def restart_commission_inputs(self):
        self.driver.tools.clear_value_and_set_text(CommissionPage.commission_input, '3.5')
        self.driver.tools.wait_and_click(CommissionPage.start_commission_btn)
        self.driver.tools.wait_and_click(CommissionPage.confirm_btn_in_alert)
        self.driver.wait.wait_for_element_to_be_present(GeneralMeshavkimPage.success_pop_up_alert)
        self.driver.tools.clear_value_and_set_text(CommissionPage.final_commission_input, '7')
        self.driver.tools.wait_and_click(CommissionPage.start_final_commission_btn)
        self.driver.tools.wait_and_click(CommissionPage.confirm_btn_in_alert)
        self.driver.wait.wait_for_element_to_be_present(GeneralMeshavkimPage.success_pop_up_alert)

    def change_commission_and_save(self):
        self.driver.tools.clear_value_and_set_text(CommissionPage.commission_input, '2')
        self.driver.tools.wait_and_click(CommissionPage.start_commission_btn)
        self.driver.tools.wait_and_click(CommissionPage.confirm_btn_in_alert)
        self.driver.wait.wait_for_element_to_be_present(GeneralMeshavkimPage.success_pop_up_alert)
        self.driver.tools.clear_value_and_set_text(CommissionPage.final_commission_input, '8')
        self.driver.tools.wait_and_click(CommissionPage.start_final_commission_btn)
        self.driver.tools.wait_and_click(CommissionPage.confirm_btn_in_alert)
        self.driver.wait.wait_for_element_to_be_present(GeneralMeshavkimPage.success_pop_up_alert)
        assert self.driver.tools.wait_for_element_and_get_attribute(CommissionPage.commission_column, 'value') in\
            self.driver.tools.wait_for_element_and_get_attribute(CommissionPage.commission_input, 'value'),\
            'The value did not update'
        assert self.driver.tools.wait_for_element_and_get_attribute(CommissionPage.final_commission_column, 'value') in\
            self.driver.tools.wait_for_element_and_get_attribute(CommissionPage.final_commission_input, 'value'),\
            'The value did not update'
        assert self.driver.tools.wait_for_element_and_get_attribute(CommissionPage.allowed_to_use_column, 'checked') ==\
            'true', 'Checkbox is not checked'

    def check_if_supplier_name_is_checked(self):
        return set([x.is_selected() for x in self.driver.wait.wait_for_elements_to_be_present(
            CommissionPage.all_checkbox)])

    def check_all_checkbox_options(self):
        self.driver.tools.wait_and_click(CommissionPage.clear_btn)
        assert self.check_if_supplier_name_is_checked() == {False}, 'there is still sum checkbox that are checked'
        self.driver.tools.wait_and_click(CommissionPage.checkall_btn)
        assert self.check_if_supplier_name_is_checked() == {True}, 'not all checkbox are checked'
        self.driver.tools.click_on_web_element_by_js(self.driver.wait.wait_for_element_to_be_present(
            CommissionPage.allowed_to_use_column))
        assert self.driver.tools.wait_for_element_and_get_attribute(CommissionPage.allowed_to_use_column, 'checked') !=\
            'true', 'Checkbox is checked'
        self.driver.tools.click_on_web_element_by_js(self.driver.wait.wait_for_element_to_be_present(
            CommissionPage.allowed_to_use_column))
        assert self.driver.tools.wait_for_element_and_get_attribute(CommissionPage.allowed_to_use_column, 'checked') ==\
            'true', 'Checkbox is not checked'

    def login_and_arrange_all_inputs(self):
        self.login_and_get_in_commission_in_meshavkim()
        self.choose_marketer_and_distributor()
        self.choose_supplier_and_product()

    def test_unit_of_commission(self):
        self.login_and_arrange_all_inputs()
        self.change_commission_and_save()
        self.check_all_checkbox_options()
        self.restart_commission_inputs()
        self.remove_logs_dir = True

    def test_integration_on_commission(self):
        self.login_and_arrange_all_inputs()
        self.change_commission_and_save()
        self.restart_driver()
        self.login_and_click_kvish_6_icone()
        assert "8" in self.driver.tools.get_text_from_element(CommissionPage.amount_of_commission),\
            "The amount of commission did not chang"
        self.restart_driver()
        self.login_and_arrange_all_inputs()
        self.restart_commission_inputs()
        self.remove_logs_dir = True
