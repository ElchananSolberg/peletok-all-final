from tests.test_meshavkim.base_meshavkim_test_class import *


class TestMeshavkimPertayEssek(BaseMeshavkimTestClass):

    def login_and_get_to_meshavkim_pirtey_essec(self):
        self.login_and_get_in_meshavkim()
        self.click_on_side_menu_options("פרטי עסק")

    def test_login_and_get_to_meshavkim_pirtey_essec(self):
        self.login_and_get_to_meshavkim_pirtey_essec()
        self.remove_logs_dir = True

    def test_filter_by_mefitz_and_meshavek(self):
        self.login_and_get_to_meshavkim_pirtey_essec()
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.distributor_input, 'peletok')
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.marketers_input, 'ידידיה נעול')
        self.verify_name_business()
        self.remove_logs_dir = True

    def test_filter_by_phone_number(self):
        self.login_and_get_to_meshavkim_pirtey_essec()
        self.input_filter_phone_number_and_click_btn_search()
        self.verify_phone_number_business()
        self.remove_logs_dir = True

    def test_filter_by_authorized_dealer_num(self):
        self.login_and_get_to_meshavkim_pirtey_essec()
        self.input_number_business_authorized_dealer_and_click_btn_search()
        self.verify_number_business_authorized_dealer()
        self.remove_logs_dir = True

    def test_autocomplete_address_business(self):
        self.login_and_get_to_meshavkim_pirtey_essec()
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.distributor_input, 'peletok')
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.marketers_input, 'ידידיה נעול')
        self.verify_autocomplete_address('רחבת הכ', 'רחבת הכותל, ירושלים')
        self.remove_logs_dir = True

    def test_verify_if_mail_address_exist(self):
        self.login_and_get_to_meshavkim_pirtey_essec()
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.distributor_input, 'peletok')
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.marketers_input, 'ידידיה נעול')
        self.verify_if_address_mail_exist()
        self.remove_logs_dir = True

    def test_verify_email_business_invoice(self):
        self.login_and_get_to_meshavkim_pirtey_essec()
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.distributor_input, 'peletok')
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.marketers_input, 'ידידיה נעול')
        self.verify_email_business_invoice()
        self.remove_logs_dir = True

    def test_verify_if_exist_date_create(self):
        self.login_and_get_to_meshavkim_pirtey_essec()
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.distributor_input, 'peletok')
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.marketers_input, 'ידידיה נעול')
        self.verify_if_address_mail_exist()
        self.verify_if_exist_date_create_business()
        self.remove_logs_dir = True

    def test_supplier_profile(self):
        self.login_and_get_to_meshavkim_pirtey_essec()
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.distributor_input, 'peletok')
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.marketers_input, 'ידידיה נעול')
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.search_list_business_supplier_profile)
        self.driver.wait.wait_for_element_to_be_present(GeneralMeshavkimPage.menu_profiles)
        self.remove_logs_dir = True

    def test_percentage_profite_profile(self):
        self.login_and_get_to_meshavkim_pirtey_essec()
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.distributor_input, 'peletok')
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.marketers_input, 'ידידיה נעול')
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.search_list_business_percentage_profile)
        self.driver.wait.wait_for_element_to_be_present(GeneralMeshavkimPage.menu_profiles)
        self.remove_logs_dir = True

    def test_commission_profile(self):
        self.login_and_get_to_meshavkim_pirtey_essec()
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.distributor_input, 'peletok')
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.marketers_input, 'ידידיה נעול')
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.search_list_business_commission_profile)
        self.driver.wait.wait_for_element_to_be_present(GeneralMeshavkimPage.menu_profiles)
        self.remove_logs_dir = True

    def test_btn_save(self):
        self.login_and_get_to_meshavkim_pirtey_essec()
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.distributor_input, 'peletok')
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.marketers_input, 'ידידיה נעול')
        self.verify_if_changes_have_been_saved()
        self.remove_logs_dir = True