from tests.test_meshavkim.base_meshavkim_test_class import *


class TestMeshavkimSapakim(BaseMeshavkimTestClass):

    def login_and_get_to_meshavkim_sapakim(self):
        self.login_and_get_in_meshavkim()
        self.click_on_side_menu_options("ספקים")

    def test_login_and_get_to_meshavkim_sapakim(self):
        self.login_and_get_to_meshavkim_sapakim()
        self.remove_logs_dir = True

    def test_filter_by_mefitz_and_meshavek(self):
        self.login_and_get_to_meshavkim_sapakim()
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.distributor_input, 'peletok')
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.marketers_input, 'ידידיה נעול')
        self.verify_authentication_sapakim()
        self.remove_logs_dir = True

    def test_filter_by_phone_number(self):
        self.login_and_get_to_meshavkim_sapakim()
        self.input_filter_phone_number_and_click_btn_search()
        self.verify_authentication_sapakim()
        self.remove_logs_dir = True

    def test_filter_by_authorized_dealer_num(self):
        self.login_and_get_to_meshavkim_sapakim()
        self.input_number_business_authorized_dealer_and_click_btn_search()
        self.verify_authentication_sapakim()
        self.remove_logs_dir = True

    def test_btn_clear(self):
        self.login_and_get_to_meshavkim_sapakim()
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.distributor_input, 'peletok')
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.marketers_input, 'ידידיה נעול')
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.btn_clear_all)
        assert self.return_if_supplier_name_is_checked() == {False}, "not all is clear"
        self.remove_logs_dir = True

    def test_btn_select_all(self):
        self.login_and_get_to_meshavkim_sapakim()
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.distributor_input, 'peletok')
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.marketers_input, 'ידידיה נעול')
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.btn_select_all)
        assert self.return_if_supplier_name_is_checked() == {True}, "not all is select"
        self.remove_logs_dir = True

    def test_check_box_suppliers(self):
        self.login_and_get_to_meshavkim_sapakim()
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.distributor_input, 'peletok')
        self.click_on_input_and_choose_option(GeneralMeshavkimPage.marketers_input, 'ידידיה נעול')
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.btn_clear_all)
        len_checkbox = len(
            self.driver.wait.wait_for_elements_to_be_present(SuppliersProfilePage.all_check_box_in_table))
        for supplier in range(len_checkbox):
            self.driver.tools.click_on_web_element_by_js(
                self.driver.wait.wait_for_element_to_be_present((GeneralMeshavkimPage.check_one_checkbox[0],
                                                                 GeneralMeshavkimPage.check_one_checkbox[1].format(
                                                                     str(supplier)))))
            assert self.driver.tools.wait_for_element_and_get_attribute(
                (GeneralMeshavkimPage.attribute_of_one_checkbox[0],
                 GeneralMeshavkimPage.attribute_of_one_checkbox[1].
                 format(str(supplier))), "checked") == 'true', \
                "this not checked"
        self.driver.tools.wait_and_click(GeneralMeshavkimPage.btn_save_supplier)
        self.driver.wait.wait_for_element_to_be_present(GeneralMeshavkimPage.alert_success)
        self.remove_logs_dir = True