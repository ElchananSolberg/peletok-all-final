from tests.base_project_test_class import *

@pytest.mark.skip
class TestTextVoiceTalkmanVirtualCardsCancelBtns(BaseProjectTestClass):

    def login_and_get_to_text_voice(self):
        self.login()
        self.driver.tools.wait_and_click(HomePage.text_voice_icon)

    def test_cancel_btn_text_voice_talkman_29(self):
        self.login_and_get_to_text_voice()
        self.pay_for_calling_card_vc(164, "196/3", cancel_payment=True)
        self.remove_logs_dir = True

    def test_cancel_btn_text_voice_talkman_164(self):
        self.login_and_get_to_text_voice()
        self.pay_for_calling_card_vc(29, "196/1", cancel_payment=True)
        self.remove_logs_dir = True
