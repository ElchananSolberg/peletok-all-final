from tests.base_project_test_class import *

@pytest.mark.skip
class TestCellcomPageCancelBtns(BaseProjectTestClass):

    def login_and_get_to_cellcom(self):
        self.login()
        self.driver.tools.wait_and_click(HomePage.cellcom_icon)

    def test_cancel_btn_cellcom_talkman_25(self):
        self.login_and_get_to_cellcom()
        self.pay_for_calling_card_vc(25, "94/virtual/1030", cancel_payment=True)
        self.remove_logs_dir = True

    def test_cancel_btn_cellcom_talkman_49(self):
        self.login_and_get_to_cellcom()
        self.pay_for_calling_card_vc(49, "94/virtual/1031", cancel_payment=True)
        self.remove_logs_dir = True

    def test_cancel_btn_cellcom_talkman_59(self):
        self.login_and_get_to_cellcom()
        self.pay_for_calling_card_vc(59, "94/virtual/1032", cancel_payment=True)
        self.remove_logs_dir = True


