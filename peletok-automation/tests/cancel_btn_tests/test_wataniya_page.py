from tests.base_project_test_class import *

class TestWataniyaTalkmanCardsCancelBtns(BaseProjectTestClass):


    def login_and_get_to_wataniya(self):
        self.login()
        self.driver.tools.wait_and_click(HomePage.wataniya_icon)

    @pytest.mark.skip
    def test_cancel_btn_wataiya_talkman_10(self):
        self.login_and_get_to_wataniya()
        self.pay_for_calling_card_vc(10, "209/1020", cancel_payment=True)
        self.remove_logs_dir = True

    @pytest.mark.skip
    def test_cancel_btn_wataniya_talkman_17(self):
        self.login_and_get_to_wataniya()
        self.pay_for_calling_card_vc(17, "209/1021", cancel_payment=True)
        self.remove_logs_dir = True

    def test_cancel_btn_wataniya_talkman_25(self):
        self.login_and_get_to_wataniya()
        self.pay_for_calling_card_vc(25, "209/1022", cancel_payment=True)
        self.remove_logs_dir = True
