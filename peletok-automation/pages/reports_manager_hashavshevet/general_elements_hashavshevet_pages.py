from selenium.webdriver.common.by import By


class GeneralElementsInReportsAreaManagerHashavshevet:
    from_date = (By.ID, 'InputTypeDateExportOrdersToHashavshevetStartingDate')
    to_date = (By.ID, 'InputTypeDateExportOrdersToHashavshevetEndingDate')
    start_time_in_minuts_select = (By.ID, 'InputTypeTimeMinuteExportOrdersToHashavshevetStartingTime')
    start_time_in_hours_select = (By.ID, 'InputTypeTimeHourExportOrdersToHashavshevetStartingTime')
    end_time_in_minuts = (By.ID, 'InputTypeTimeHourExportOrdersToHashavshevetEndingTime')
    end_time_in_hours = (By.ID, 'InputTypeTimeHourExportOrdersToHashavshevetEndingTime')
    view_report_button = (By.ID, 'hashavshevetShowReportButton')
    report_buttons_by_href_string = (By.XPATH,
                                     '//div[@class="temp_background main-body pt-5"]//a[@href="/reports/hashavshevet/{}"]')
    first_row_in_table = (By.ID, 'tr0')
    no_results_found_text = (By.XPATH, ".//*[@class='temp_background main-body pt-5']"
                                       "//span[.='לא נמצאו תוצאות']")
    reseller_select = (By.ID, 'InputTypeSearchListExportOrdersToHashavshevetSelectSeller')
    article_20_checkbox_click = (By.XPATH, '//label[@for="InputTypeCheckBoxExportOrdersToHashavshevetsection20"]')
    article_20_checkbox_check = (By.ID, "InputTypeCheckBoxExportOrdersToHashavshevetsection20")
    checkbox_click = (By.XPATH, '//label[@for="InputTypeCheckBox{}"]')
    checkbox_check = (By.ID, 'InputTypeCheckBox{}')
    agent_select = (By.ID, 'InputTypeSearchListOrdersExportedToHashavshevet_Agent')
    terminal_number_select = (By.ID, 'InputTypeNumberExportOrdersToHashavshevetTerminalNumber')
    radio_button = (By.XPATH, '//label[@for="InputTypeCheckBoxImportReceiptsFromHashavshevet_DelekUser{}"]')
    input_upload_file = (By.XPATH, '//input[@id="inputTypeFileImportReceiptsFromHashavshevet_SelectFile"]')
