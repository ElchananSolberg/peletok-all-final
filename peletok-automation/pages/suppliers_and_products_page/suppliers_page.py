from selenium.webdriver.common.by import By


class SuppliersPage:

    input_type_search_list_slelct_supplier = (By.ID, 'InputTypeSearchListslelctSupplier')
    input_type_text_name = (By.ID, 'InputTypeTextname')
    input_type_number_contact_phone_number = (By.ID, 'InputTypeNumbercontact_phone_number')
    input_type_address_address = (By.ID, 'InputTypeAddressaddress')
    input_type_search_list_type = (By.ID, 'InputTypeSearchListtype')
    input_type_number_order = (By.ID, 'InputTypeNumberorder')
    input_type_text_contact_name = (By.ID, 'InputTypeTextcontact_name')
    input_type_check_box_status = (By.ID, 'InputTypeCheckBoxstatus')
    label_of_input_type_check_box_status = (By.XPATH, '//label[@for="InputTypeCheckBoxstatus"]')
    input_type_check_box_default_creation = (By.ID, 'InputTypeCheckBoxdefault_creation')
    label_of_input_type_check_box_default_creation = (By.XPATH, '//label[@for="InputTypeCheckBoxdefault_creation"]')
    input_type_check_box_cancel_option = (By.ID, 'InputTypeCheckBoxcancel_option')
    label_of_input_type_check_box_cancel_option = (By.XPATH, '//label[@for="InputTypeCheckBoxcancel_option"]')
    input_type_search_list_credit_card_id = (By.ID, 'InputTypeSearchListcredit_card_id')
    input_type_number_max_percentage_profit = (By.ID, 'InputTypeNumbermax_percentage_profit')
    input_type_number_min_percentage_profit = (By.ID, 'InputTypeNumbermin_percentage_profit')
    maximum_amount_payable = (By.XPATH, '//input[@id="InputTypeNumbermax_payment"][1]')
    credit_limitation = (By.XPATH, '//div[@class="row"]/div[4]//input[@id="InputTypeNumbermax_payment"]')
    supplier_Save = (By.ID, "supplierSave")
    alert_success = (By.CLASS_NAME, "myAlert")
