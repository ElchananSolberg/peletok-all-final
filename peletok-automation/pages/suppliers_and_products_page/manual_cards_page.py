from selenium.webdriver.common.by import By


class ManualCardsPage:
    selector_suppliers = (By.ID, 'InputTypeSearchListProviderListId')
    selector_items = (By.ID, 'InputTypeSearchListListOfProductsId')
    checkbox_click = (By.XPATH, '//label[@for="InputTypeCheckBox{}CheckBoxId"]')
    checkbox_check = (By.ID, 'InputTypeCheckBox{}CheckBoxId')
    income_date = (By.ID, 'InputTypeDateStartDateInputTextId')
    end_date = (By.ID, 'InputTypeDateEndDateInputTextId')
    search_btn = (By.ID, 'supplierSearch')
    no_cards_report = (By.CSS_SELECTOR, 'div.manualCardsSettings.container>div.col')
    new_manual_card_btn = (By.ID, 'manualCardsFormNewButton')
    card_number_form_manual_cards = (By.ID, 'InputTypeTextManualCardsFormCardNumber')
    card_code_form_manual_cards = (By.ID, 'InputTypeTextManualCardsFormCardCode')
    expiration_date = (By.ID, 'InputTypeDateManualCardsFormExpiryDate')
    card_in_use_input = (By.ID, 'InputTypeTextManualCardsFormInUse')
    save_btn = (By.ID, 'manualCardsFormSaveButton')
    alert_successful = (By.CSS_SELECTOR, 'div.myAlert.alert.alert-success.fade.show')
    card_row = (By.CLASS_NAME, 'cardRow')
    cards_in_db = (By.CSS_SELECTOR, 'div.manualCardsSettings.container>div:nth-child({})>div:nth-child({})')



