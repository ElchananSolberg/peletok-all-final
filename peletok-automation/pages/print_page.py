from selenium.webdriver.common.by import By


class PrintPage:
    save_btn = (By.XPATH, '//*[@class="action-button"]')
    info = (By.XPATH, "//div[@class='receipt-properties']")
    body = (By.XPATH, '//body')
    supplier = (By.XPATH, '//table//tbody/tr/td[1]')
    product = (By.XPATH, '//table//tbody/tr/td[2]')
    price = (By.XPATH, '//table//tbody/tr/td[3]')
