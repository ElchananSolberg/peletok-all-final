from selenium.webdriver.common.by import By


class ProfitPercentages:
    btn_save_distribution_fee = (By.ID, "sellers_ProfitPercentages_ButtonSaveUseDistributionFee")
    checkbox_use_distribution_fee = (By.XPATH, "//label["
                                               "@for='InputTypeCheckBoxSellers_ProfitPercentages__UseDistributionFee']")
    attribute_of_checkbox = (By.ID, "InputTypeCheckBoxSellers_ProfitPercentages__UseDistributionFee")
    alert_success = (By.CLASS_NAME, "myAlert")
    radio_btn = (By.XPATH, "//label[@for='InputUtilsRadioSellers_ProfitPercentages_ServiceType-{}']")
